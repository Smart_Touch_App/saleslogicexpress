using Telerik.WebAii.Controls.Xaml.Wpf;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;
using ArtOfTest.WebAii.Wpf;

namespace SalesLogicExpress
{
    public class DailyStop : BaseWebAiiTest
    {
        #region [ Dynamic Applications Reference ]

        private Applications _applications;

        /// <summary>
        /// Gets the Applications object that has references
        /// to all the elements, windows or regions
        /// in this project.
        /// </summary>
        public Applications Applications
        {
            get
            {
                if (_applications == null)
                {
                    _applications = new Applications(Manager.Current);
                }
                return _applications;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        //[CodedStep(@"LeftClick on Item0Textboxview")]
        //public void Order_value_check_CodedStep()
        //{
            //// LeftClick on Item0Textboxview
            //Applications.SalesLogicExpressexe.UserLogin.Item0Textboxview.User.Click(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 29, 11, ArtOfTest.Common.OffsetReference.TopLeftCorner, ArtOfTest.Common.ActionPointUnitType.Percentage, ((System.Windows.Forms.Keys)(0)));
            
        //}
    
        //[CodedStep(@"radwatermarktextbox: Type 'RSR173' into LoginIDRadwatermarktextbox")]
        //public void Order_value_check_CodedStep1()
        //{
            //// radwatermarktextbox: Type 'RSR173' into LoginIDRadwatermarktextbox
            //Applications.SalesLogicExpressexe.UserLogin.LoginIDRadwatermarktextbox.SetText(true, "RSR173", 10, 100, false, true);
            
        //}
    
        //[CodedStep(@"LeftClick on Item2Textboxview")]
        //public void Order_value_check_CodedStep2()
        //{
            //// LeftClick on Item2Textboxview
            //Applications.SalesLogicExpressexe.UserLogin.Item2Textboxview.User.Click(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 53, 68, ArtOfTest.Common.OffsetReference.TopLeftCorner, ArtOfTest.Common.ActionPointUnitType.Percentage, ((System.Windows.Forms.Keys)(0)));
            
        //}
    
        //[CodedStep(@"radwatermarktextbox: Type 'FBM173' into RouteRadwatermarktextbox")]
        //public void Order_value_check_CodedStep3()
        //{
            //// radwatermarktextbox: Type 'FBM173' into RouteRadwatermarktextbox
            //Applications.SalesLogicExpressexe.UserLogin.RouteRadwatermarktextbox.SetText(true, "FBM173", 10, 100, false, true);
            
        //}
    
        //[CodedStep(@"Click LoginRadbutton")]
        //public void Order_value_check_CodedStep4()
        //{
            //// Click LoginRadbutton
            //Applications.SalesLogicExpressexe.UserLogin.LoginRadbutton.User.Click(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 66, 15, ArtOfTest.Common.OffsetReference.TopLeftCorner, ArtOfTest.Common.ActionPointUnitType.Pixel);
            
        //}
    
        //[CodedStep(@"Click ServiceRouteRadbutton")]
        //public void Order_value_check_CodedStep5()
        //{
            //// Click ServiceRouteRadbutton
            //Applications.SalesLogicExpressexe.RouteHome.ServiceRouteRadbutton.User.Click(ArtOfTest.WebAii.Core.MouseClickType.LeftClick, 51, 12, ArtOfTest.Common.OffsetReference.TopLeftCorner, ArtOfTest.Common.ActionPointUnitType.Pixel);
            
        //}
    
        //[CodedStep(@"RightClick on SelectedVisualBorder")]
        //public void Order_value_check_CodedStep6()
        //{
            //// RightClick on SelectedVisualBorder
            //Applications.SalesLogicExpressexe.ServiceRoute.SelectedVisualBorder.User.Click(ArtOfTest.WebAii.Core.MouseClickType.RightClick, 30, 71, ArtOfTest.Common.OffsetReference.TopLeftCorner, ArtOfTest.Common.ActionPointUnitType.Percentage, ((System.Windows.Forms.Keys)(0)));
            
        //}
    
        //[CodedStep(@"RightClick on SelectedVisualBorder")]
        //public void Order_value_check_CodedStep7()
        //{
            //// RightClick on SelectedVisualBorder
            //Applications.SalesLogicExpressexe.ServiceRoute.SelectedVisualBorder.User.Click(ArtOfTest.WebAii.Core.MouseClickType.RightClick, 32, 45, ArtOfTest.Common.OffsetReference.TopLeftCorner, ArtOfTest.Common.ActionPointUnitType.Percentage, ((System.Windows.Forms.Keys)(0)));
            
        //}
    
        //[CodedStep(@"radlistbox: selecting item '03 M R  Customer Prospect  (N30) Net 30 Days     Rescheduled Date:   Original Date:   NO ACTIVITY ACTIVITY 0 CREDIT HOLD'")]
        //public void Order_value_check_CodedStep8()
        //{
            //// radlistbox: selecting item '03 M R  Customer Prospect  (N30) Net 30 Days     Rescheduled Date:   Original Date:   NO ACTIVITY ACTIVITY 0 CREDIT HOLD'
            //Applications.SalesLogicExpressexe.ServiceRoute.DailyStopListCustomradlistbox.SelectItem("03 M R  Customer Prospect  (N30) Net 30 Days     Rescheduled Date:   Original Dat" +
                            //"e:   NO ACTIVITY ACTIVITY 0 CREDIT HOLD");
            
        //}
    
        //[CodedStep(@"Desktop command: LeftDoubleClick on DailyStopListCustomradlistbox")]
        //public void Order_value_check_CodedStep9()
        //{
            //// Desktop command: LeftDoubleClick on DailyStopListCustomradlistbox
            //Applications.SalesLogicExpressexe.ServiceRoute.DailyStopListCustomradlistbox.User.Click(ArtOfTest.WebAii.Core.MouseClickType.LeftDoubleClick, 0, 0, ArtOfTest.Common.OffsetReference.AbsoluteCenter, ArtOfTest.Common.ActionPointUnitType.Percentage);
            
        //}
    }
}
