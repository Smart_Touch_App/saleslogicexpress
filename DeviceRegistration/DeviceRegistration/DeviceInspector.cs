﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeviceRegistration
{
    public partial class DeviceInspector : Form
    {
        public DeviceInspector()
        {
            InitializeComponent();
        }
        void GetSystemDetails() {
            
            System.Management.SelectQuery query = new System.Management.SelectQuery(@"Select * from Win32_ComputerSystem");

            //initialize the searcher with the query it is supposed to execute
            using (System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query))
            {
                //execute the query
                foreach (System.Management.ManagementObject process in searcher.Get())
                {
                    //print system info
                    process.Get();
                    Console.WriteLine("/*********Operating System Information ***************/");
                    Console.WriteLine("{0}{1}", "System Manufacturer:", process["Manufacturer"]);
                    lblManufacturer.Text = process["Manufacturer"].ToString();
                    lblModel.Text = process["Model"].ToString();
                    Console.WriteLine("{0}{1}", " System Model:", process["Model"]);


                }
            }

            System.Management.ManagementObjectSearcher searcher1 = new System.Management.ManagementObjectSearcher("SELECT * FROM Win32_BIOS");
            System.Management.ManagementObjectCollection collection = searcher1.Get();


            foreach (ManagementObject obj in collection)
            {
                if (((string[])obj["BIOSVersion"]).Length > 1)
                    Console.WriteLine("BIOS VERSION: " + ((string[])obj["BIOSVersion"])[0] + " - " + ((string[])obj["BIOSVersion"])[1]);
                else
                    Console.WriteLine("BIOS VERSION: " + ((string[])obj["BIOSVersion"])[0]);
            }

            ManagementObjectSearcher searchers = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where IPEnabled=true");
            IEnumerable<ManagementObject> objects = searchers.Get().Cast<ManagementObject>();
            string mac = (from o in objects orderby o["IPConnectionMetric"] select o["MACAddress"].ToString()).FirstOrDefault();
             Console.WriteLine("MAC : " +  mac);
             lblDeviceID.Text = DeviceManager.Device.GenerateDeviceID();// mac.Replace(":", "");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetSystemDetails();
        }

        private void BtnCopyToClipboard_Click(object sender, EventArgs e)
        {
            string message = "DeviceID : {0}, Model : {1}, Manufacturer: {2}";
            message = string.Format(message, lblDeviceID.Text, lblModel.Text, lblManufacturer.Text);
            Clipboard.SetText(message);
        }
    }
}
