﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class PickManager
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.PickManager");

        public PickManager()
        {

        }

        public int UpdateManualPickReasonAndCountForPick(PickOrderItem pickItem)
        {
            return DbEngine.ExecuteNonQuery("update BUSDTA.PickOrder set reason_code_id=" + pickItem.LastManualPickReasonCode + "," +
            " ManuallyPickCount= ( SELECT max(isnull(ManuallyPickCount,0))+1  from BUSDTA.PickOrder where Item_Number='" + pickItem.ItemNumber + "' and Order_Id=" + Order.OrderId + ")" +
            " where Item_Number='" + pickItem.ItemNumber + "' and Order_Id=" + Order.OrderId);
        }

        public int UpdateManualPickReasonAndCountForException(PickOrderItem exceptionItem)
        {
            return DbEngine.ExecuteNonQuery("update BUSDTA.PickOrder_Exception set ManualPickReasonCode=" + exceptionItem.LastManualPickReasonCode + "," +
            " ManuallyPickCount= ( SELECT max(isnull(ManuallyPickCount,0))+1  from  BUSDTA.PickOrder_Exception where Item_Number='" + exceptionItem.ItemNumber + "' and Order_Id=" + Order.OrderId + ")" +
            " where Item_Number='" + exceptionItem.ItemNumber + "' and Order_Id=" + Order.OrderId);
        }


        public ObservableCollection<ReasonCode> GetReasonListForManualPick()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();

            try
            {

                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Manual Pick'").Tables[0];

                ReasonCode ReasonCode = new ReasonCode();


                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetReasonListForManualPick  Message: " + e.StackTrace);
                throw;
            }

            return reasonCodeList;

        }

        public bool IsPickListCreated(string OrderId)
        {
            try
            {
                int countPickItems = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.PickOrder where order_id = '" + OrderId + "'"));
                int itemExCount = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.pickOrder_exception where order_id='" + OrderId + "'"));
                if (countPickItems == 0 && itemExCount == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, IsPickListCreated(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public string GetPickListStatus(string OrderId)
        {
            try
            {
                string IsOnHold = DbEngine.ExecuteScalar("select distinct IsOnHold from busdta.PickOrder where order_id = '" + OrderId + "'");

                if (IsOnHold == "0" || IsOnHold == "")
                {
                    return "NotHold";

                }
                else
                {
                    return "Hold";
                }
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetPickListStatus(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int DeletePickList(string OrderId)
        {
            try
            {
                return DbEngine.ExecuteNonQuery("delete from busdta.PickOrder where order_id = '" + OrderId + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, DeletePickList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }
        public int DeletePickedItem(string OrderId, string ItemNumber)
        {
            try
            {
                return DbEngine.ExecuteNonQuery("delete from busdta.PickOrder where order_id = '" + OrderId + "' and item_number='" + ItemNumber + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, DeletePickedItem(" + OrderId + ", " + ItemNumber + ")  Message: " + e.StackTrace);
                throw;
            }
        }
        public void UpdateItemForUMChange(string OrderId, PickOrderItem PreviousItem, PickOrderItem CurrentItem, string RouteID)
        {
            log.Info("[SalesLogicExpress.Application.Managers][PickManager][Start:UpdateItemForUMChange]");
            try
            {
                /* Possible Scenarios
                 * Case 1 : Item was not picked
                 *      a. Just update the UM and OrderQty in Pick Order and return.
                 * Case 2 : Item was partially/overpicked picked
                 *      a. Add item in pick_exception table with the pickqty as exception qty and status as incorrect item.
                 *      b. Remove the item from PickOrder table. 
                 * Case 3 : Item was Fully/Over picked.
                 *      a. Add item in pick_exception table with the pickqty as exception qty and status as incorrect item.
                 *      b. Remove the item from PickOrder table.
                 */
                string query = string.Empty;
                query = @"
                        select Item_Number
                        ,case when isnull(Picked_Qty_Primary_UOM,0) >0 and isnull(Picked_Qty_Primary_UOM,0) <= Order_Qty then 1 else 0 end UnderPicked
                        ,case when isnull(Picked_Qty_Primary_UOM,0) =0 then 1 else 0 end NotPicked
                        ,case when isnull(Picked_Qty_Primary_UOM,0) > Order_Qty then 1 else 0 end OverPicked
                        ,CAST(isnull(Picked_Qty_Primary_UOM,0) AS VARCHAR) + '|'+ CAST(Order_Qty AS VARCHAR) PickVsOrdered, Order_Qty, ORDER_UOM,Primary_UOM, order_id from busdta.PickOrder  where order_id = '{0}' and item_number ='{1}'
                        ";
                query = string.Format(query, OrderId, PreviousItem.ItemNumber.Trim());
                DataSet dsItemDetails = DbEngine.ExecuteDataSet(query);
                if (dsItemDetails.HasData())
                {
                    bool itemUnderPicked = Convert.ToBoolean(Convert.ToInt32(dsItemDetails.Tables[0].Rows[0]["UnderPicked"].ToString()));
                    bool itemOverPicked = Convert.ToBoolean(Convert.ToInt32(dsItemDetails.Tables[0].Rows[0]["OverPicked"].ToString()));
                    if (itemUnderPicked)
                    {
                        /* If the item is underpicked,  then it will not be present in the PickOrder_Exception table
                         hence add the item in PickOrder_Exception table */
                        query = " insert into busdta.PickOrder_Exception (RouteId,order_id,Item_number,Exception_Qty,UOM,Exception_Reason) values(" + RouteID + ",'" + OrderId + "','" + PreviousItem.ItemNumber + "'," + PreviousItem.PickedQuantityInPrimaryUOM + ",'" + PreviousItem.UM + "','" + PreviousItem.ExceptionReason + "')";
                        DbEngine.ExecuteNonQuery(query);
                    }
                    if (itemOverPicked)
                    {
                        /* If the item is already overpicked,  then it will be present in the PickOrder_Exception table
                         so just update the item in the Exception List. */
                        query = " update busdta.PickOrder_Exception set Exception_Qty = '{0}',UOM= '{1}',Exception_Reason= '{2}' where order_id = '{3}' and item_number='{4}'";
                        query = string.Format(query, PreviousItem.OrderQty, PreviousItem.UM, PreviousItem.ExceptionReason, OrderId, PreviousItem.ItemNumber);
                        DbEngine.ExecuteNonQuery(query);
                    }
                    /* Update the item in the PickOrder table for the updated qty and UOM */
                    query = "update busdta.PickOrder set  Order_Qty={0} , Order_UOM='{1}',order_qty_primary_UOM='{2}' where order_id = '{3}' and item_number='{4}'";
                    query = string.Format(query, CurrentItem.OrderQty, CurrentItem.UM, CurrentItem.OrderQtyInPrimaryUOM, OrderId, CurrentItem.ItemNumber);
                    DbEngine.ExecuteNonQuery(query);

                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][PickManager][UpdateItemForUMChange][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            log.Info("[SalesLogicExpress.Application.Managers][PickManager][End:UpdateItemForUMChange]");
        }
        public int UpdatePickedItem(string OrderId, object Pickeditem, string OldUM = "")
        {
            try
            {
                PickOrderItem item = Pickeditem as PickOrderItem;
                string query = "Update busdta.PickOrder set  Order_Qty=" + item.OrderQty + " , Order_UOM='" + item.UM + "',Picked_qty_primary_UOM='" + item.PickedQuantityInPrimaryUOM + "',order_qty_primary_UOM=" + item.OrderQtyInPrimaryUOM + " where order_id = '" + OrderId + "' and item_number='" + item.ItemNumber + "' and Order_UOM='" + (string.IsNullOrEmpty(OldUM) ? item.UM : OldUM) + "'";
                int result = DbEngine.ExecuteNonQuery(query);
                return result;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, UpdatePickedItem(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }
        public int InsertPickList(string OrderId, ObservableCollection<PickOrderItem> pickedItems, string routeId)
        {
            try
            {
                foreach (PickOrderItem item in pickedItems)
                {
                    InsertPickItem(OrderId, item, routeId);
                }

                return 1;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, InsertPickList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int InsertPickItem(string OrderId, PickOrderItem item, string routeId)
        {
            try
            {
                return DbEngine.ExecuteNonQuery(
                        @"insert into  BUSDTA.PickOrder 
                    (RouteId, Order_ID,Item_Number,Order_Qty,Order_UOM,Picked_Qty_Primary_UOM,Primary_UOM,Order_Qty_Primary_UOM) 
                    values (" + routeId + "," + OrderId + ",'" + item.ItemNumber + "'," + item.OrderQty + ",'" + item.UM + "','" + item.PickedQuantityInPrimaryUOM + "','" + item.PrimaryUM + "','" + item.OrderQtyInPrimaryUOM + "')"
                        );
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, InsertPickItem(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public ObservableCollection<PickOrderItem> GetPickItemsFromDB(string OrderId)
        {
            try
            {
                ObservableCollection<PickOrderItem> pickItems = new ObservableCollection<PickOrderItem>();

                DataTable dt = DbEngine.ExecuteDataSet(@"select po.Item_Number,IMDSC1 as ItemDescription ,LFIVI,
                po.Order_Qty,po.Order_UOM,Picked_Qty_Primary_UOM,Primary_UOM,Order_Qty_Primary_UOM,IMITM as ItemId,ISNULL(od.UnitPriceAmt,0) Unit_Price,
                ISNULL(od.ExtnPriceAmt,0) Extn_Price,isnull(od.IsTaxable,'0') as IsTaxable,
                isnull(OnHandQuantity,0) as OnHandQty,isnull(CommittedQuantity,0) as CommittedQty,
                isnull(HeldQuantity,0) as HeldQty, (isnull(OnHandQuantity,0)-isnull(CommittedQuantity,0)-isnull(HeldQuantity,0)) as AvailableQty 
                from BUSDTA.PickOrder po left join busdta.F4101
                on po.Item_Number = imlitm 
                join  busdta.F4102 on BUSDTA.f4102.IBLITM = BUSDTA.f4101.IMLITM
                join Busdta.F40205 on Busdta.F40205.LFLNTY = BUSDTA.f4102.IBLNTY    
                left join busdta.ORDER_HEADER oh on po.Order_ID = oh.OrderID left join 
                busdta.Order_Detail od on oh.OrderID=od.OrderID and od.ItemId=imitm 
                left join busdta.inventory inv on po.Item_Number = inv.ItemNumber where po.order_id = '" + OrderId + "'").Tables[0];

                Random RandomInt = new Random();

                PickOrderItem pickItem = new PickOrderItem();

                foreach (DataRow dr in dt.Rows)
                {
                    pickItem = new PickOrderItem();
                    pickItem.ItemNumber = dr["Item_Number"].ToString().Trim();
                    pickItem.ItemDescription = dr["ItemDescription"].ToString().Trim();
                    pickItem.OrderQty = Convert.ToInt32(dr["Order_Qty"].ToString());
                    pickItem.UM = dr["Order_UOM"].ToString().Trim();
                    pickItem.PickedQuantityInPrimaryUOM = Convert.ToInt32(dr["Picked_Qty_Primary_UOM"].ToString());
                    pickItem.PrimaryUM = dr["Primary_UOM"].ToString().Trim();
                    pickItem.OrderQtyInPrimaryUOM = Convert.ToInt32(dr["Order_Qty_Primary_UOM"].ToString());
                    //To be removed coz of temparary dynamic value it should come from some other source
                    pickItem.AvailableQty = Convert.ToInt32(dr["AvailableQty"].ToString().Trim());
                    pickItem.QtyOnHand = Convert.ToInt32(dr["OnHandQty"].ToString().Trim());
                    pickItem.ActualQtyOnHand = RandomInt.Next(pickItem.AvailableQuantity, pickItem.AvailableQuantity + 100);
                    pickItem.ItemId = dr["ItemId"].ToString().Trim();
                    // pickItem.UMConversionFactor = new Managers.PricingManager().jdeUOMConversion(pickItem.UM, pickItem.PrimaryUM, Convert.ToInt32(pickItem.ItemId));

                    // code added to replace pricing manager with uom manager
                    if (UoMManager.ItemUoMFactorList != null)
                    {
                        var item = pickItem;
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == item.ItemId.Trim()) && (x.FromUOM == item.UM.ToString()) && (x.ToUOM == item.PrimaryUM));
                        pickItem.UMConversionFactor = itemUomConversion == null ? UoMManager.GetUoMFactor(item.UM.ToString(), item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim()) : itemUomConversion.ConversionFactor;
                    }
                    else
                    {
                        pickItem.UMConversionFactor = UoMManager.GetUoMFactor(pickItem.UM, pickItem.PrimaryUM, Convert.ToInt32(pickItem.ItemId.Trim()), pickItem.ItemNumber.Trim());
                    }
                    pickItem.UnitPrice = Convert.ToDecimal(dr["Unit_Price"].ToString());
                    pickItem.ExtendedPrice = Convert.ToDecimal(dr["Extn_Price"].ToString());

                    if (dr["IsTaxable"].ToString() != "")
                    {
                        pickItem.IsTaxable = Convert.ToBoolean(Convert.ToInt32(dr["IsTaxable"].ToString().Trim()));
                    }
                    pickItem.StkType = dr["LFIVI"].ToString();
                    pickItems.Add(pickItem);
                }
                return pickItems;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetPickItemsFromDB(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int UpdatePickQty(string OrderId, string ItemNumber, string Picked_Qty_Primary_UOM, string UM = "")
        {
            try
            {
                return DbEngine.ExecuteNonQuery(" update busdta.PickOrder set Picked_Qty_Primary_UOM ='" + Picked_Qty_Primary_UOM + "' where order_id='" + OrderId + "' and Item_number = '" + ItemNumber + "' and Order_UOM='" + UM + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, UpdatePickQty(" + OrderId + "," + Picked_Qty_Primary_UOM + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int HoldUnHoldPickList(string OrderId, bool IsOnHold)
        {
            try
            {
                int result = 0;

                if (IsOnHold)
                {
                    result = DbEngine.ExecuteNonQuery(" update busdta.PickOrder set IsOnHold ='1' where order_id='" + OrderId + "'");
                }
                else
                {
                    result = DbEngine.ExecuteNonQuery(" update busdta.PickOrder set IsOnHold =NULL where order_id='" + OrderId + "'");
                }
                return result;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, HoldPickList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int SaveException(string OrderId, PickOrderItem Item, string RouteId)
        {
            try
            {
                bool IsExceptionExists = DbEngine.ExecuteScalar(" select count(*) from busdta.PickOrder_Exception pe where pe.Order_ID='" + OrderId + "' and pe.Item_Number='" + Item.ItemNumber + "' and pe.UOM='" + Item.UM + "'") == "0" ? false : true;
               
                int result;

                if (IsExceptionExists)
                {
                    //Update
                    result = DbEngine.ExecuteNonQuery(" update busdta.PickOrder_Exception pe  set pe.exception_qty=" + Item.ExceptionQtyInOrderUOM + ",pe.UOM='" + Item.UM + "',pe.exception_reason='" + Item.ExceptionReason + "' where pe.Order_ID='" + OrderId + "' and pe.Item_Number='" + Item.ItemNumber + "' and pe.UOM='" + Item.UM + "'");
                }
                else
                {
                    //Insert
                    result = DbEngine.ExecuteNonQuery(" insert into busdta.PickOrder_Exception (RouteId,order_id,Item_number,Exception_Qty,UOM,Exception_Reason) values(" + RouteId + ",'" + OrderId + "','" + Item.ItemNumber + "'," + Item.ExceptionQtyInOrderUOM + ",'" + Item.UM + "','" + Item.ExceptionReason + "')");
                }
                return result;

            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, SaveException(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public int DeleteException(string OrderId, PickOrderItem Item)
        {
            try
            {
                return DbEngine.ExecuteNonQuery(" delete from busdta.PickOrder_Exception pe  where pe.Order_ID='" + OrderId + "' and pe.Item_Number='" + Item.ItemNumber + "' and pe.UOM='" + Item.UM + "'");
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, DeleteException(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

        public ObservableCollection<PickOrderItem> GetExceptionList(string OrderId)
        {
            try
            {
                ObservableCollection<PickOrderItem> ExceptionItems = new ObservableCollection<PickOrderItem>();

                DataTable dt = DbEngine.ExecuteDataSet(@"select pe.Item_Number,IMDSC1 as ItemDescription ,isnull(Order_Qty,0) Order_Qty,pe.UOM Order_UOM,
isnull(Picked_Qty_Primary_UOM,0) Picked_Qty_Primary_UOM,isnull(Primary_UOM,imuom1) Primary_UOM ,isnull(Order_Qty_Primary_UOM,0) Order_Qty_Primary_UOM,IMITM as ItemId ,
isnull(pe.Exception_Qty,0) Exception_Qty,pe.Exception_Reason Exception_Reason
from busdta.PickOrder_Exception  pe join busdta.F4101
            on Item_Number = imlitm left join BUSDTA.PickOrder po on po.Order_ID = pe.Order_Id and po.Item_Number=pe.Item_Number 
 where pe.order_id = '" + OrderId + "'").Tables[0];


                PickOrderItem pickItem = new PickOrderItem();

                foreach (DataRow dr in dt.Rows)
                {
                    pickItem = new PickOrderItem();
                    pickItem.ItemNumber = dr["Item_Number"].ToString();
                    pickItem.ItemDescription = dr["ItemDescription"].ToString();
                    pickItem.OrderQty = Convert.ToInt32(dr["Order_Qty"].ToString());
                    pickItem.UM = dr["Order_UOM"].ToString();
                    pickItem.PickedQuantityInPrimaryUOM = Convert.ToInt32(dr["Picked_Qty_Primary_UOM"].ToString());
                    pickItem.PrimaryUM = dr["Primary_UOM"].ToString();
                    pickItem.OrderQtyInPrimaryUOM = Convert.ToInt32(dr["Order_Qty_Primary_UOM"].ToString());
                    pickItem.ItemId = dr["ItemId"].ToString();
                    //  pickItem.UMConversionFactor = new Managers.PricingManager().jdeUOMConversion(pickItem.UM, pickItem.PrimaryUM, Convert.ToInt32(pickItem.ItemId));

                    // Code added to replace pricing manager call with uom manager
                    if (UoMManager.ItemUoMFactorList != null)
                    {
                        var item = pickItem;
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == item.ItemId.Trim()) && (x.FromUOM == item.UM.ToString()) && (x.ToUOM == item.PrimaryUM));
                        pickItem.UMConversionFactor = itemUomConversion == null ? UoMManager.GetUoMFactor(item.UM.ToString(), item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim()) : itemUomConversion.ConversionFactor;
                    }
                    else
                    {
                        pickItem.UMConversionFactor = UoMManager.GetUoMFactor(pickItem.UM, pickItem.PrimaryUM, Convert.ToInt32(pickItem.ItemId.Trim()), pickItem.ItemNumber.Trim());
                    }
                    pickItem.ExceptionQtyInPrimaryUOM = Convert.ToInt32(Convert.ToInt32(dr["Exception_Qty"].ToString()) * pickItem.UMConversionFactor);
                    pickItem.ExceptionQtyInOrderUOM = Convert.ToInt32(dr["Exception_Qty"].ToString());
                    pickItem.ExceptionReason = dr["Exception_Reason"].ToString();
                    ExceptionItems.Add(pickItem);
                }
                return ExceptionItems;
            }
            catch (Exception e)
            {
                log.Error("SalesLogicExpress.Application.Managers.PickManager, GetExceptionList(" + OrderId + ")  Message: " + e.StackTrace);
                throw;
            }
        }

    }
}
