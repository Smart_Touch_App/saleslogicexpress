﻿using System;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using System.Data;
using System.Collections.Generic;
using System.Linq;
namespace SalesLogicExpress.Application.Managers
{

    public class ItemManager
    {
        public static string customerNO = string.Empty;
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ItemManager");
        public ItemManager()
        {

        }

        /// <summary>
        /// Search for items
        /// </summary>
        /// <param name="searchTerm">term to search</param>
        /// <returns>ObservableCollection of Item class object</returns>
        /// <remarks>Search on items in made on itemNumber and item description</remarks>
        public ObservableCollection<Item> SearchItem(string searchTerm, string items, bool blackList)
        {
            return SearchItem(searchTerm, null, true);
        }
        public ObservableCollection<Item> SearchItem(string searchTerm, string CustomerNo)
        {
            string getParentQuery = "select mapa8 as parentNo from BUSDTA.F0150 P where maan8 = " + CustomerNo;
            string ParentId = Helpers.DbEngine.ExecuteScalar(getParentQuery);
            // Check for restrictions on search.
            List<Item> restrictedItems = ItemRestrictions.CustomerRestrictions.ContainsKey(ParentId) ? ItemRestrictions.CustomerRestrictions[ParentId].Items : new List<Item>();
            string restrictedType = ItemRestrictions.CustomerRestrictions.ContainsKey(ParentId) ? ItemRestrictions.CustomerRestrictions[ParentId].Type : "";

            ObservableCollection<Item> itemCollection = new ObservableCollection<Item>();
            Managers.ItemRestrictionManager manager = new ItemRestrictionManager();
            ObservableCollection<ItemRestriction> restrictedItem = manager.GetItemRestrictionList(CustomerNo);
            try
            {
                string[] supportedKeyword = new string[] { "IMLITM", "IMDSC1" };
                string[] searchTermkeywords = searchTerm.Trim().Replace("  ", " ").Split(' ');
                string conditionQuery = string.Empty, queryPlaceholder = string.Empty;
                foreach (string keyword in supportedKeyword)
                {
                    conditionQuery = conditionQuery + "(";
                    foreach (string term in searchTermkeywords)
                    {
                        conditionQuery += keyword + " like '%" + term + "%' ";
                        if (searchTermkeywords[searchTermkeywords.Length - 1] != term)
                        {
                            conditionQuery += " or ";
                        }
                    }
                    if (supportedKeyword[supportedKeyword.Length - 1] != keyword)
                    {
                        conditionQuery += " ) or ";
                    }
                    else
                    {
                        conditionQuery += " ) or ";
                    }
                }
                conditionQuery = conditionQuery + "(0=1) ";
                if (searchTermkeywords.Length == 1)
                {
                    queryPlaceholder = conditionQuery;
                    queryPlaceholder = queryPlaceholder.Replace(") and (", ") or (");
                    conditionQuery = "(" + conditionQuery + ") or (" + queryPlaceholder + ")";
                }

                string searchQuery = "select RTRIM(LTRIM(IMLITM)) as Item#, IMDSC1 as ItemDescription, IMLITM ,1 as OrderQty,1 as PreOrderQty, 1 as UsualQty, iu.UOM as UM, IMUOM4 as UM_Price, " +
                    "IMLNTY as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5, dateformat(today(), 'mm/dd/yyyy') as EffectiveDate, dateformat(today(), 'mm/dd/yyyy') as ExpiredDate,  IMITM as ItemId,imuom1 as PrimaryUOM,IMSRP4 as SalesCat4, " +
                    "isnull(OnHandQuantity,0) as OnHandQty, isnull(CommittedQuantity,0) as CommittedQty,isnull(HeldQuantity,0) as HeldQty, (isnull(OnHandQuantity,0)-isnull(CommittedQuantity,0)-isnull(HeldQuantity,0)) as AvailableQty" +
                    " from busdta.F4101 join busdta.F4102 on BUSDTA.F4101.IMLITM = busdta.f4102.IBLITM JOIN BUSDTA.F40205 ON IBLNTY=LFLNTY JOIN BUSDTA.Inventory inv ON BUSDTA.F4101.IMITM = inv.itemId join busdta.ItemConfiguration ic on imitm = ic.itemId JOIN busdta.ItemUoMs iu on ic.itemid=iu.itemid " +
                    "where IBLITM = IMLITM and ic.RouteEnabled = 1 and ic.allowSearch=1 and iu.cansell=1 and iu.DisplaySeq=(select min(DisplaySeq) from busdta.itemUOMs where itemid=ic.itemid and cansell=1)  and ( " + conditionQuery + " ) ORDER BY 1";
                //searchQuery = searchQuery + "Union all ";
                //searchQuery = searchQuery + "select RTRIM(LTRIM(IMLITM)) as Item#, IMDSC1 as ItemDescription, IMLITM ,1 as OrderQty,1 as PreOrderQty, 1 as UsualQty, iu.UOM as UM, IMUOM4 as UM_Price, " +
                //    "Case LFIVI when 'Y' then 'S' else 'N' end as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5, dateformat(today(), 'mm/dd/yyyy') as EffectiveDate, dateformat(today(), 'mm/dd/yyyy') as ExpiredDate,  IMITM as ItemId,imuom1 as PrimaryUOM,IMSRP4 as SalesCat4, " +
                //    "isnull(OnHandQuantity,0) as OnHandQty, isnull(CommittedQuantity,0) as CommittedQty,isnull(HeldQuantity,0) as HeldQty, (isnull(OnHandQuantity,0)-isnull(CommittedQuantity,0)-isnull(HeldQuantity,0)) as AvailableQty" +
                //    " from busdta.F4101 join busdta.F4102 on BUSDTA.F4101.IMLITM = busdta.f4102.IBLITM JOIN BUSDTA.F40205 ON IBLNTY=LFLNTY and LFIVI<>'Y' JOIN BUSDTA.Inventory inv ON BUSDTA.F4101.IMITM = inv.itemId join busdta.ItemConfiguration ic on imitm = ic.itemId JOIN busdta.ItemUoMs iu on ic.itemid=iu.itemid " +
                //    "where IBLITM = IMLITM and ic.RouteEnabled = 1 and ic.allowSearch=1 and iu.cansell=1 and iu.DisplaySeq=(select min(DisplaySeq) from busdta.itemUOMs where itemid=ic.itemid and cansell=1)  and ( " + conditionQuery + " ) ";//ORDER BY 1";

                DataSet searchQueryResults = Helpers.DbEngine.ExecuteDataSet(searchQuery);
                DataSet newSearchResult = null;

                if (searchQueryResults.HasData())
                {
                    Item item;
                    foreach (DataRow dr in searchQueryResults.Tables[0].Rows)
                    {
                        #region UOM Collection
                        string uomQuery = "SELECT UOM as 'UMCode', 1 as 'UMMultipler' FROM BUSDTA.ItemUoMs  WHERE ITEMID='" + dr["ItemId"].ToString().Trim() + "' and  cansell=1 order by DisplaySeq";
                        List<QtyUOMClass> qtyUOM = new List<QtyUOMClass>();
                        DataSet resultUOM = DbEngine.ExecuteDataSet(uomQuery);
                        if (resultUOM.HasData())
                        {
                            qtyUOM = resultUOM.GetEntityList<QtyUOMClass>();

                        }

                        
                        #endregion

                        // E. Exclude List. Derived from the Item Restriction Logic (F03012.AIEDF2 is E for the customer). 
                        // Do not include these items in search result.
                        if (("E".Equals(restrictedType) || "e".Equals(restrictedType)) && restrictedItems.Count(i => i.ItemNumber == dr["Item#"].ToString()) > 0)
                        {
                            continue;
                        }
                        // I. Include List. Derived from the Item Restriction Logic (F03012.AIEDF2 is I for the customer). 
                        // Include these items in search result.
                        else if (("I".Equals(restrictedType) || "i".Equals(restrictedType)))
                        {
                            if (restrictedItems.Count(i => i.ItemNumber == dr["Item#"].ToString()) > 0)
                            {
                                item = new Item();
                                item.ItemNumber = dr["Item#"].ToString();
                                item.ItemDescription = dr["ItemDescription"].ToString();
                                item.UM = dr["UM"].ToString();
                                item.UMPrice = dr["UM_Price"].ToString();
                                item.SalesCat1 = dr["SalesCat1"].ToString();
                                item.SalesCat4 = dr["SalesCat4"].ToString();
                                item.SalesCat5 = dr["SalesCat5"].ToString();
                                item.ItemId = dr["ItemId"].ToString();
                                item.StkType = dr["StkType"].ToString();
                                item.PrimaryUM = dr["PrimaryUOM"].ToString();
                                item.AvailableQty = Convert.ToInt32(dr["AvailableQty"].ToString().Trim());
                                item.ActualQtyOnHand = Convert.ToInt32(dr["OnHandQty"].ToString().Trim());
                                item.CommittedQty = Convert.ToInt32(dr["CommittedQty"].ToString().Trim());
                                item.QtyOnHand = Convert.ToInt32(dr["OnHandQty"].ToString().Trim());
                                item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);

                                #region Set Conversion Factor
                                try
                                {
                                    //Set selected item in dropdown
                                    QtyUOMClass objUM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUM.Trim());
                                    if (objUM != null)
                                    {
                                        item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUM.Trim());
                                        item.UM = item.PrimaryUM.Trim();
                                    }
                                    else
                                    {
                                        item.SelectedQtyUOM = item.QtyUOMCollection[0];
                                        item.UM = item.QtyUOMCollection[0].UMCode;
                                    }

                                    //Set conversion factor
                                    List<QtyUOMClass> objRemove = new List<QtyUOMClass>();

                                    foreach (QtyUOMClass x in item.QtyUOMCollection)
                                    {
                                        x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUM, x.UMCode, item.ItemId, item.ItemNumber);
                                        if (x.UMMultipler > item.AvailableQty)
                                        {
                                            objRemove.Add(x);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.Error("[SalesLogicExpress.Application.Managers][ItemManager][SearchItem][Set Conversion Factor Region][ExceptionStackTrace = " + ex.StackTrace + "]");
                                    
                                }
                                
                                #endregion
                                itemCollection.Add(item);
                            }
                            continue;
                        }
                        //If there are no restrictions (F03012.AIEDF2 is blank), display all the values.
                        else
                        {
                            item = new Item();
                            item.ItemNumber = dr["Item#"].ToString();
                            item.ItemDescription = dr["ItemDescription"].ToString();
                            item.UM = dr["UM"].ToString();
                            item.UMPrice = dr["UM_Price"].ToString();
                            item.SalesCat1 = dr["SalesCat1"].ToString();
                            item.SalesCat4 = dr["SalesCat4"].ToString();
                            item.SalesCat5 = dr["SalesCat5"].ToString();
                            item.ItemId = dr["ItemId"].ToString();
                            item.StkType = dr["StkType"].ToString();
                            item.PrimaryUM = dr["PrimaryUOM"].ToString();
                            item.AvailableQty = Convert.ToInt32(dr["AvailableQty"].ToString().Trim());
                            item.QtyOnHand = Convert.ToInt32(dr["OnHandQty"].ToString().Trim());
                            item.QtyUOMCollection = new ObservableCollection<QtyUOMClass>(qtyUOM);
                            item.ActualQtyOnHand = Convert.ToInt32(dr["OnHandQty"].ToString().Trim());
                            item.CommittedQty = Convert.ToInt32(dr["CommittedQty"].ToString().Trim());
                            #region Set Conversion Factor
                            try
                            {
                                //Set selected item in dropdown
                                QtyUOMClass objUM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUM.Trim());
                                if (objUM != null)
                                {
                                    item.SelectedQtyUOM = item.QtyUOMCollection.FirstOrDefault(x => x.UMCode.Trim() == item.PrimaryUM.Trim());
                                    item.UM = item.PrimaryUM.Trim();
                                }
                                else
                                {
                                    item.SelectedQtyUOM = item.QtyUOMCollection[0];
                                    item.UM = item.QtyUOMCollection[0].UMCode;
                                }

                                //Set conversion factor
                                List<QtyUOMClass> objRemove = new List<QtyUOMClass>();

                                foreach (QtyUOMClass x in item.QtyUOMCollection)
                                {
                                    x.UMMultipler = new TemplateManager().SetConversionFactorForItem(item.PrimaryUM, x.UMCode, item.ItemId, item.ItemNumber);
                                    if (x.UMMultipler > item.AvailableQty)
                                    {
                                        objRemove.Add(x);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("[SalesLogicExpress.Application.Managers][ItemManager][SearchItem][Set Conversion Factor Region][ExceptionStackTrace = " + ex.StackTrace + "]");

                            }

                            #endregion
                            itemCollection.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return itemCollection;
        }

        public List<string> GetAppliedUMs(string ItemNumber)
        {
            List<string> AppliedUms = new List<string>();

            DataTable dt = DbEngine.ExecuteDataSet(@"select distinct(UMUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + ItemNumber + "')" +
           " union select distinct(UMRUM) UMUM from BUSDTA.F41002 where UMITM = (select imitm from BUSDTA.F4101 where IMLITM = '" + ItemNumber + "')").Tables[0];

            foreach (DataRow item in dt.Rows)
            {
                AppliedUms.Add(item["UMUM"].ToString());
            }
            return AppliedUms;
        }

        public Boolean ValidateNonStockItem (string ItemNumber)
        {
            Boolean IsNonStockItem = false;

            DataTable dt = DbEngine.ExecuteDataSet(@"select IBLITM,LFIVI,Case LFIVI when 'Y' then 'STOCK' else 'NON-STOCK' end case from busdta.F4102 join busdta.F40205 on IBLNTY=LFLNTY where IBLITM = '" + ItemNumber + "'").Tables[0];

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][2].ToString() == "STOCK")
                {
                    return IsNonStockItem =  false;
                }else
                {
                    return IsNonStockItem = true;
                }
            }
            return IsNonStockItem;
        }

        public ObservableCollection<Item> GetSuggestedItems(string customerID)
        {
            customerNO = customerID;
            ObservableCollection<Item> itemCollection = new ObservableCollection<Item>();

            try
            {
                //TODO : Map suggested items specific to customer
                string suggestedItemQuery = "select IMLITM as Item#, IMDSC1 as ItemDescription, IMLITM ,1 as OrderQty,1 as PreOrderQty, 1 as UsualQty, IMUOM4 as UM, IMUOM4 as UM_Price, " +
                    "IMLNTY as StkType,IMSRP1  as SalesCat1, IMSRP5 as SalesCat5, dateformat(today(), 'mm/dd/yyyy') as EffectiveDate, dateformat(today(), 'mm/dd/yyyy') as ExpiredDate,  IMITM as ItemId " +
                    "from busdta.F4101 left outer join busdta.F4102 on BUSDTA.F4101.IMLITM = busdta.f4102.IBLITM   " +
                    "where IMLITM like '%7801727%' or IMLITM like '%5881729%' or IMLITM like '%7851212%' or IMLITM like '%08111%' ";
                DataSet suggestedItems = Helpers.DbEngine.ExecuteDataSet(suggestedItemQuery);
                if (suggestedItems.HasData())
                {
                    Item item;
                    foreach (DataRow dr in suggestedItems.Tables[0].Rows)
                    {
                        item = new Item();
                        item.ItemNumber = dr["Item#"].ToString().Trim();
                        item.ItemDescription = dr["ItemDescription"].ToString();
                        item.UM = dr["UM"].ToString();
                        item.UMPrice = dr["UM_Price"].ToString();
                        item.SalesCat1 = dr["SalesCat1"].ToString();
                        item.SalesCat5 = dr["SalesCat5"].ToString();
                        item.ItemId = dr["ItemId"].ToString();
                        itemCollection.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return itemCollection;
        }

        public static ItemRestriction ItemRestrictions
        {
            get;
            set;
        }
        public static ItemRestriction getItemRestrictionList()
        {
            ItemRestriction itemRestriction = new ItemRestriction();

            string restrictionFlagQuery = "select AIAN8, AIEDF2 from busdta.F03012 " +
                "where AIAN8 in (select P.MAPA8 Parent from " +
                "BUSDTA.F0150 P join BUSDTA.F0101 B on P.MAAN8=B.ABAN81 and P.MAOSTP='   ' " +
                "join BUSDTA.F03012 C on B.ABAN8=C.AIAN8 " +
                ") and AIEDF2 <> ''";

            itemRestriction.CustomerRestrictions = new Dictionary<string, ItemRestriction.Restriction>();
            DataSet restrictionFlag = Helpers.DbEngine.ExecuteDataSet(restrictionFlagQuery);
            if (restrictionFlag.HasData())
            {
                SalesLogicExpress.Domain.ItemRestriction.Restriction restriction;
                foreach (DataRow dr in restrictionFlag.Tables[0].Rows)
                {
                    restriction = new ItemRestriction.Restriction();

                    if (dr["AIEDF2"].ToString() != null && !dr["AIEDF2"].ToString().Equals(""))
                    {
                        string restrictionQuery = "select TRIM(IMLITM) as ItemNumber from BUSDTA.F4013 " +
                             "JOIN BUSDTA.F4101 on CAST(SXXRVT as INT)=IMITM where SXXRTC='IR' and SXXRVF=" + dr["AIAN8"].ToString();
                        DataSet ds = DbEngine.ExecuteDataSet(restrictionQuery);
                        if (null != ds)
                        {
                            restriction.Type = dr["AIEDF2"].ToString();
                            restriction.Items = ds.GetEntityList<Item>();
                            itemRestriction.CustomerRestrictions.Add(dr["AIAN8"].ToString(), restriction);
                        }
                    }

                }
            }
            ItemRestrictions = itemRestriction;
            return itemRestriction;
        }
    }
}
