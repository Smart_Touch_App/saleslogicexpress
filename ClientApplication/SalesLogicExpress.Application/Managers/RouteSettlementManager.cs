﻿using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using log4net;


namespace SalesLogicExpress.Application.Managers
{
    public class RouteSettlementManager
    {
        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.RouteSettlementManager");

        public static ObservableCollection<RouteSettlementModel> GetTransactions(bool showAllActivities)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetTransactions]");
            StringBuilder objQueryBuilder = new StringBuilder();
            string strVerifyCodeId = DbEngine.ExecuteScalar("SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VERF'");
            
            objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry', 'PaymentVoid', 'VoidCreditMemo', 'VoidExpense','VoidMoneyOrder') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            objQueryBuilder.Append("WHERE ACT.TDTYP in ('Order','CreditMemo','OrderReturned','Expense','MoneyOrder','Quote','InvAdjustment') " + (showAllActivities ? "" : "AND ISNULL(RS.Status,0)<>" + strVerifyCodeId) + " AND ACT.TDPNTID=0 ");
            objQueryBuilder.Append("UNION ");
            objQueryBuilder.Append("SELECT  ACT.TDPNTID, ACT.TDID AS ActivityID, ltrim(RTRIM(ACT.TDSTTLID)) AS  SettlementID, ACT.TDROUT AS  RouteID, ACT.TDTYP AS  ActivityType, ACT.TDCLASS AS  ActivityDetailClass, ");
            objQueryBuilder.Append("ACT.TDDTLS AS  ActivityDetails, ACT.TDSTRTTM AS  ActivityStart, ACT.TDENDTM AS  ActivityEnd, ACT.TDSTID AS  StopInstanceID, ACT.TDAN8 AS  CustomerID, ");
            objQueryBuilder.Append("ACT.TDPNTID AS  ActivityHeaderID, (CASE RS.Status WHEN " + strVerifyCodeId + " THEN (CASE WHEN ACT.TDSTAT IN ('VoidAtCashCollection', 'VoidAtDeliverCustomer', 'VoidAtOrderEntry','PaymentVoid', 'VoidCreditMemo','VoidExpense','VoidMoneyOrder') THEN 'VOIDED / SETTLED' ELSE 'SettlementVerified' END) ");
            objQueryBuilder.Append("ELSE ACT.TDSTAT END) AS  ActivityStatus FROM busdta.M50012 ACT LEFT OUTER JOIN BUSDTA.Route_Settlement RS ON ACT.TDSTTLID=RS.SettlementId ");
            objQueryBuilder.Append("WHERE ACT.TDTYP in ('Payment','ReturnOrder','HeldReturn') " + (showAllActivities ? "" : "AND ISNULL(RS.Status,0)<>" + strVerifyCodeId) + " ORDER BY ActivityEnd DESC");

            List<RouteSettlementModel> activities = new List<RouteSettlementModel>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(objQueryBuilder.ToString());
                if (result.HasData())
                {
                    activities = result.GetEntityList<RouteSettlementModel>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetTransactions][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetTransactions]");

            return new ObservableCollection<RouteSettlementModel>(activities);
        }

        public static ObservableCollection<RouteSettlementSettlementsModel> GetAllSettlements()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetAllSettlements]");

            // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
            string query = @"select SettlementID ,SettlementNo,LTRIM(RTRIM(d.StatusTypeDESC)) as  [Status] , SettlementDateTime as 'Date',
                             Originator,Verifier,SettlementAmount,ExceptionAmount,[Comment] 
                             ,b.App_User as'OriginatorName', c.App_User as 'VerifierName' 
                             from busdta.Route_Settlement a LEFT join busdta.user_master b ON a.Originator =b.App_user_id 
                            LEFT join busdta.user_master c ON a.Verifier =c.App_user_id 
                            LEFT JOIN BUSDTA.Status_Type d ON a.Status=d.StatusTypeID order by SettlementDateTime DESC";
            List<RouteSettlementSettlementsModel> activities = new List<RouteSettlementSettlementsModel>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<RouteSettlementSettlementsModel>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetAllSettlements][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetAllSettlements]");

            return new ObservableCollection<RouteSettlementSettlementsModel>(activities);
        }

        public static ObservableCollection<RouteSettlementModel> GetTransactionsSettlementID(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][Start:GetTransactionsSettlementID][SettlementID='" + settlementID + "']");

            // TransactionID as ActivityID, RouteID,TransactionType as ActivityType,TransactionDetailClass as ActivityDetailClass,TransactionDetails as ActivityDetails,TransactionStart as ActivityStart,TransactionEnd as ActivityEnd,StopInstanceID,CustomerID,SettlementID,ParentTransactionID as ActivityHeaderID, TransactionStatus as ActivityStatus
            string query = "select TDID as ActivityID,TDROUT as RouteID,TDTYP as ActivityType, " +
                                          "  TDCLASS as ActivityDetailClass,TDDTLS as ActivityDetails," +
                                           " TDSTRTTM as ActivityStart,TDENDTM as ActivityEnd," +
                                           " TDSTID as StopInstanceID, TDAN8 as CustomerID," +
                                           " ltrim(rtrim(TDSTTLID)) as SettlementID,TDPNTID as ActivityHeaderID," +
                                           " TDSTAT as ActivityStatus from busdta.M50012 " +
                                           " where  ActivityType in ('Order','Payment','MoneyOrder') and TDSTTLID= " + settlementID + "  " +
                                           " ORDER BY TDENDTM DESC";
            List<RouteSettlementModel> activities = new List<RouteSettlementModel>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<RouteSettlementModel>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][RouteSettlementManager][GetTransactionsSettlementID][SettlementID='" + settlementID + "'][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][RouteSettlementManager][End:GetTransactionsSettlementID][SettlementID='" + settlementID + "']");

            return new ObservableCollection<RouteSettlementModel>(activities);
        }
    }
}
