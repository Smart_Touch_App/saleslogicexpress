﻿using iAnywhere.MobiLink.Client;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.SLEStatService;
using SalesLogicExpress.Application.ViewModels;
using System.Configuration;

namespace SalesLogicExpress.Application.Managers
{
    public class SyncManager
    {
        #region Fields and Properties

        //private readonly ILog Logger = LogManager.GetLogger(typeof(SyncManager));
        private readonly ILog Logger = LogManager.GetLogger("SyncLogger");

        DbmlsyncClient _syncClient = null;
        DBSC_StartType dbStartType;
        UInt32 syncHandle;
        public List<UInt32> syncHandles = new List<UInt32>();
        public SyncUpdateType SyncState { get; set; }

        #endregion

        #region Constructor/Destructor

        public SyncManager()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncManager-Constructor]");
            try
            {
                ResourceManager.NetworkManager.InternetStateChanged += NetworkStateChanged;
                ResourceManager.WebServiceManager.InitiateQueryForDataFromServer();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SyncManager-Constructor][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SyncManager-Constructor]");
        }
        public void ListenToQueue()
        {
            ResourceManager.QueueManager.QueueChanged += SyncQueueChanged;
        }
        ~SyncManager()
        {
            ShutDownSyncClient();
        }

        #endregion

        #region Methods

        public bool IsSynching { get; set; }
        public void ShutDownSyncClient()
        {
            if (SyncClient != null)
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:ShutDownSyncClient]");
                try
                {
                    SyncClient.ShutdownServer(DBSC_ShutdownType.DBSC_SHUTDOWN_ON_EMPTY_QUEUE);
                    SyncClient.WaitForServerShutdown(10000);
                    SyncClient.Disconnect();
                    SyncClient.Fini();
                    _syncClient = null;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][ShutDownSyncClient][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                finally
                {
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:ShutDownSyncClient]");
            }
        }
        public DbmlsyncClient SyncClient
        {
            get
            {
                if (_syncClient == null)
                {
                    Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncClient]");
                    string sqlAnywherePath = ConfigurationManager.AppSettings["SqlAnywhereInstallationPath"].ToString();
                    try
                    {
                        _syncClient = DbmlsyncClient.InstantiateClient();
                        _syncClient.Init();
                        // Setting the "server path" is usually required on Windows
                        // Mobile/CE. In other environments the server path is usually
                        // not required unless you SA install is not in your path or
                        // you have multiple versions of the product installed
                        _syncClient.SetProperty("server path", sqlAnywherePath);
                        _syncClient.StartServer(3426, "-q -c SERVER=remote_eng;DBN=remote_db;UID=dba;PWD=sql", 60000, out dbStartType);
                        _syncClient.Connect(null, 3426, "dba", "sql");
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SyncClient][ExceptionStackTrace = " + ex.StackTrace + "]");
                        throw;
                    }
                    Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SyncClient]");

                }
                return _syncClient;
            }
        }
        public QueueManager queueManager;
        async void ProcessQueue(QueueManager.SyncQueueChangedEventArgs e)
        {
            await Task.Run(() =>
            {
                bool useAppService = Convert.ToBoolean(ConfigurationManager.AppSettings["userAppService"].ToString());
                if (useAppService)
                {
                    ProcessQueueWithWebServiceCheck(e);
                }
                else
                {
                    ProcessQueueWithoutWebServiceCheck(e);
                }
            });
        }
        private void ProcessQueueWithoutWebServiceCheck(QueueManager.SyncQueueChangedEventArgs e)
        {
            try
            {
                IsSynching = true;
                ResourceManager.Synchronization.NotifyUpdate(null, SyncUpdateType.Initiated);
                if (ResourceManager.NetworkManager.IsInterNetConnected())
                {
                    ProcessSyncQueue(e);
                }
                else
                {
                    NotifyUpdate(SyncUpdateType.ServerNotReachable);
                }
            }
            catch (Exception ex)
            {
                IsSynching = false;
            }
            IsSynching = false;
            NotifyUpdate(SyncUpdateType.Idle);
        }
        private void ProcessQueueWithWebServiceCheck(QueueManager.SyncQueueChangedEventArgs e)
        {
            try
            {
                IsSynching = true;
                ResourceManager.Synchronization.NotifyUpdate(null, SyncUpdateType.Initiated);
                if (ResourceManager.NetworkManager.IsInterNetConnected() && ResourceManager.WebServiceManager.IsServerReachable())
                {
                    HealthResponse response = ResourceManager.WebServiceManager.InvokeWebRequestOnSync();
                    if (response != null)
                    {
                        InvokeSyncOnWebServiceResponse(response, e);
                    }
                }
                else
                {
                    NotifyUpdate(SyncUpdateType.ServerNotReachable);
                }
            }
            catch (Exception ex)
            {
                IsSynching = false;
            }
            IsSynching = false;
            NotifyUpdate(SyncUpdateType.Idle);
        }

        private void ProcessSyncQueue(QueueManager.SyncQueueChangedEventArgs e)
        {
            try
            {
                switch (e.QueueType)
                {
                    case QueueManager.SyncQueueType.Immediate:
                        if (!e.IsDataInQueue) break;
                        SyncEverything("prof_everything", null, e.ImmediateQueue);
                        //SynchronizeDatabase("prof_everything", null, e.ImmediateQueue);
                        break;
                    case QueueManager.SyncQueueType.Priority:
                        if (!e.IsDataInQueue) break;
                        SynchronizeDatabase("prof_immediate", null, e.PriorityQueue);
                        break;
                    case QueueManager.SyncQueueType.Normal:
                        SyncEverything("prof_everything", null, e.NormalQueue);
                        //SynchronizeDatabase("prof_everything", null, e.NormalQueue);
                        break;
                    case QueueManager.SyncQueueType.All:
                        // When we do a mannual sync, then we explicitly pass the queue being processed as null
                        // as there might be items in other items as well.
                        SyncEverything("prof_everything", null, null);
                        //SynchronizeDatabase("prof_everything", null, null);
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }
        private bool SyncEverything(String publication, Domain.TransactionSyncDetail syncActivity, Queue<Domain.TransactionSyncDetail> queue)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SyncEverything][publication=" + publication + "]");
            bool isSyncSuccessful = true;
            try
            {
                if (publication == "prof_everything")
                {
                    bool syncEverything = true, syncSysEverything = true;
                    syncEverything = SynchronizeDatabase("prof_everything", null, queue);
                    syncSysEverything = SynchronizeDatabase("prof_SystemDB_everything", null, null);

                    isSyncSuccessful = syncEverything && syncSysEverything;
                }
                else
                {
                    isSyncSuccessful = SynchronizeDatabase(publication, syncActivity, queue);
                }
                return isSyncSuccessful;
            }
            catch (Exception ex)
            {
                isSyncSuccessful = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SynchronizeDatabase][publication=" + publication + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return isSyncSuccessful;
            }
        }

        private bool SynchronizeDatabase(String publication, Domain.TransactionSyncDetail syncActivity, Queue<Domain.TransactionSyncDetail> queue)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:SynchronizeDatabase][publication=" + publication + "]");
            DBSC_Event dbEvent;
            bool isSyncSuccessful = true;
            //// Setting the "server path" is usually required on Windows
            //// Mobile/CE. In other environments the server path is usually
            //// not required unless you SA install is not in your path or
            //// you have multiple versions of the product installed
            syncHandle = SyncClient.Sync(publication, "");
            syncHandles.Add(syncHandle);
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][SynchronizeDatabase][publication=" + publication + " [SYNC_MSG = START SYNC]");
                while (SyncClient.GetEvent(out dbEvent, DbmlsyncClient.DBSC_INFINITY)
                      == DBSC_GetEventRet.DBSC_GETEVENT_OK)
                {
                    Logger.Info("[publication=" + publication + " [SYNC_MSG = " + dbEvent.str1+ "]");
                    if (dbEvent.hdl == syncHandle)
                    {
                        Console.WriteLine("Event Type : {0}", dbEvent.type);
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                            Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SynchronizeDatabase][publication=" + publication + "][DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG = " + dbEvent.str1 + "]");
                            ResourceManager.Synchronization.NotifyUpdate(queue, SyncUpdateType.SyncFailed);
                            isSyncSuccessful = false;
                            break;
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_CANCEL)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][SynchronizeDatabase][publication=" + publication + "][DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG = " + dbEvent.str1 + "]");
                            ResourceManager.Synchronization.NotifyUpdate(queue, SyncUpdateType.SyncFailed);
                            isSyncSuccessful = false;
                            break;
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG)
                        {
                            if (dbEvent.str1.Contains("Network Error"))
                            {
                                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SynchronizeDatabase][publication=" + publication + "][DBSC_EventType.DBSC_EVENTTYPE_ERROR_MSG = " + dbEvent.str1 + "]");
                                ResourceManager.Synchronization.NotifyUpdate(queue, SyncUpdateType.SyncFailed);
                                break;
                            }
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_FIRST_INTERNAL)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_INFO_MSG)
                        {
                            Console.WriteLine("Info : {0}", dbEvent.str1);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_UPLOAD_COMMITTED)
                        {
                            //syncActivity.Synced = true;
                            //syncActivity.SyncTimestamp = DateTime.Now;
                            //NotifyUpdate(syncActivity, SyncUpdateType.UploadComplete);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_SYNC_DONE)
                        {
                            //syncActivity.Synced = true;
                            //syncActivity.SyncTimestamp = DateTime.Now;
                            //NotifyUpdate(syncActivity, SyncUpdateType.SyncComplete);
                        }
                        if (dbEvent.type == DBSC_EventType.DBSC_EVENTTYPE_DOWNLOAD_COMMITTED)
                        {
                            NotifyUpdate(queue, SyncUpdateType.DownloadComplete);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSyncSuccessful = false;
                ResourceManager.Synchronization.NotifyUpdate(SyncUpdateType.SyncFailed);
                NotifyUpdate(queue, SyncUpdateType.SyncFailed);
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][SynchronizeDatabase][publication=" + publication + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            NotifyUpdate(SyncUpdateType.Idle);
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:SynchronizeDatabase][publication=" + publication + "]");
            CommonNavInfo.IsMandatorySync = CommonNavInfo.IsUpdateAvailable = !isSyncSuccessful;
            return isSyncSuccessful;
        }
        public async void CancelSync()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:CancelSync]");
            await Task.Run(() =>
            {
                try
                {
                    foreach (UInt32 handle in syncHandles)
                        SyncClient.CancelSync(handle);
                    syncHandles.Clear();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][CancelSync][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            });
            Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:CancelSync]");
        }
        public void Initialize()
        {

        }
        public Dictionary<string, DateTime?> GetSyncLastTimes()
        {
            var syncDetails = new Dictionary<string, DateTime?>();

            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:GetSyncLastTimes]");


                var query = " select (SELECT top 1 last_download_time FROM SYSSYNC WHERE subscription_name <> ''  order by last_download_time desc), ";
                query += "(SELECT top 1 last_upload_time FROM SYSSYNC WHERE subscription_name <> ''  order by last_upload_time desc)";

                var dbset = DbEngine.ExecuteDataSet(query);
                if (dbset.HasData())
                {
                    var lastDownloadTime = Convert.ToDateTime(dbset.Tables[0].Rows[0]["last_download_time"].ToString());
                    var lastUploadTime = Convert.ToDateTime(dbset.Tables[0].Rows[0]["last_upload_time"].ToString());

                    syncDetails.Add("LastDownloadTime", lastDownloadTime);
                    syncDetails.Add("LastUploadTime", lastUploadTime);
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:GetSyncLastTimes]");

                return syncDetails;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][GetSyncLastTimes][ExceptionStackTrace = " + ex.StackTrace + "]");
                syncDetails.Add("LastDownloadTime", null);
                syncDetails.Add("LastUploadTime", null);
                return null;
            }
        }
        #endregion

        #region Event Handlers and Supporting Enums/Classes
        void InvokeSyncOnWebServiceResponse(HealthResponse healthResponse, QueueManager.SyncQueueChangedEventArgs e)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][Start:InvokeSyncOnWebServiceResponse][healthResponse=" + healthResponse.SerializeToJson() + "]");
                // Scenarios
                // 1. Healthresponse contains Truncation details but ready-to-sync is false.
                // 2. Healthresponse contains Truncation details and ready-to-sync is true.
                // 3. Healthresponse contains Mandatory Sync Flag set to true but ready-to-sync is false.
                // 4. Healthresponse contains Mandatory Sync Flag set to true and ready-to-sync is true.
                // 5. Healthresponse contains Optional Sync Flag set to true and ready-to-sync is true and Sync Queue is empty.
                // 6. Healthresponse contains Optional Sync Flag set to true and ready-to-sync is true and Sync Queue has items.

                #region Table Truncation Response from WebService
                bool truncationStatus = true;
                List<Constants.WebServiceManager.ApplicationGroup> appGroupToSyncList = new List<Constants.WebServiceManager.ApplicationGroup>();

                // Iterate for ApplicationGroups which has IsMandatory flag set to true
                foreach (var deltaChange in (healthResponse.DeltaChangeDetails).Where(deltaChange => deltaChange.IsMandatorySync))
                {
                    // Map ApplicationGroup in response to ApplicationGroup enum.
                    // Add ApplicationGroup to synclist
                    appGroupToSyncList.AddRange(Enum.GetValues(typeof(Constants.WebServiceManager.ApplicationGroup)).Cast<Constants.WebServiceManager.ApplicationGroup>().Where(appGpItem => string.Equals(deltaChange.ApplicationGroup.ToLower(), appGpItem.ToString())));
                }
                // Get Table list to truncate, if tables to truncate is not empty.
                if (healthResponse.TruncatedTables != null && healthResponse.TruncatedTables.Count() != 0)
                {
                    var truncateList = healthResponse.TruncatedTables.ToList();
                    using (var dbMgr = new Managers.DBManager())
                        //Call to DBManager to truncate table list
                        truncationStatus = dbMgr.TruncateDBTables(truncateList);
                }
                #endregion
                if (healthResponse != null && healthResponse.ReadyToSync)
                {
                    ProcessSyncQueue(e);
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][SyncManager][End:InvokeSyncOnWebServiceResponse][healthResponse=" + healthResponse.SerializeToJson() + "]");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SyncManager][InvokeSyncOnWebServiceResponse][healthResponse=" + healthResponse.SerializeToJson() + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }
        void SyncQueueChanged(object sender, QueueManager.SyncQueueChangedEventArgs e)
        {
            ProcessQueue(e);
        }
        void NetworkStateChanged(object sender, NetworkManager.InternetStateChangedEventArgs e)
        {
            //// If internet connection is available, process the queue
            //if (e.State == InternetState.Connected && (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueImmediate) || queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueEverything)))
            //{
            //    System.Diagnostics.Debug.WriteLine("Internet available and queue pending");
            //    System.Diagnostics.Debug.WriteLine("Process Queue");
            //    if (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueImmediate))
            //    {
            //        ProcessQueue(queueManager.SyncQueueImmediate);
            //    }
            //    if (queueManager.IsTransactionInSyncQueue(queueManager.SyncQueueEverything))
            //    {
            //        ProcessQueue(queueManager.SyncQueueEverything);
            //    }
            //}
            //// If internet connection is not available, persist the Sync Queue in the database
            //if (e.State == InternetState.Disconnected)
            //{
            //    System.Diagnostics.Debug.WriteLine("Internet not available return..");
            //}
        }
        public event EventHandler<SyncUpdatedEventArgs> SyncProgressChanged;
        public void NotifyUpdate(SyncUpdateType update)
        {
            SyncUpdatedEventArgs args = new SyncUpdatedEventArgs();
            args.State = update;
            args.StateChangedTime = DateTime.Now;
            OnSyncProgress(args);
        }
        public void NotifyUpdate(Queue<Domain.TransactionSyncDetail> queue, SyncUpdateType update)
        {
            SyncUpdatedEventArgs args = new SyncUpdatedEventArgs();
            args.Queue = queue;
            args.State = update;
            args.StateChangedTime = DateTime.Now;
            OnSyncProgress(args);
        }
        protected virtual void OnSyncProgress(SyncUpdatedEventArgs e)
        {
            EventHandler<SyncUpdatedEventArgs> handler = SyncProgressChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class SyncUpdatedEventArgs : EventArgs
        {
            public SyncUpdatedEventArgs()
            {
                StateChangedTime = DateTime.SpecifyKind(StateChangedTime, DateTimeKind.Utc);
            }
            public Domain.TransactionSyncDetail SyncActivity { get; set; }
            public SyncUpdateType State { get; set; }
            public Queue<Domain.TransactionSyncDetail> Queue { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        public enum SyncUpdateType
        {
            Initiated,
            Idle,
            UploadComplete,
            DownloadComplete,
            SyncComplete,
            SyncFailed,
            ServerNotReachable
        }
        #endregion

    }
}