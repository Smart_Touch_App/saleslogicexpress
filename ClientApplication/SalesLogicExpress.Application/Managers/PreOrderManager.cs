﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.ViewModels;
using System.Globalization;
using System.Configuration;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;


namespace SalesLogicExpress.Application.Managers
{
    public class PreOrderManager
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.PreOrderManager");

        string _baseDate = ConfigurationManager.AppSettings["BaseDate"].ToString();
        ///<summary>
        ///Gets preorder datea collection 
        ///</summary>
        ///<param name="customerID">Customer Number</param>
        public ObservableCollection<TemplateItem> GetPreOrderItems(string customerID, DateTime date)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][PreOrderManager][Start:GetPreOrderItems][customerID=" + customerID + "][date=" + date + "]");

            ObservableCollection<TemplateItem> result = new ObservableCollection<TemplateItem>();
            try
            {
                //string QueryPreOrder = "select OPORTP as Template_Name , OPAN8 as Invoice#, OPOSEQ as Sequence#, OPLITM as Item# , IMDSC1 as ItemDescription, IMITM as ItemId," +
                //"OPQTYU/10000 as PreOrderQty,OTQTYU/10000 as UsualQty,OPUOM as UM,OPLNTY as StkType,Dateformat(BUSDTA.DateG2J(OTEFTJ),'mm/dd/yyyy') as " +
                //"EffectiveDate, Dateformat(BUSDTA.DateG2J(OTEXDJ),'mm/dd/yyyy') as ExpiredDate ,SRP1  as SalesCat1," +
                //"SRP5 as SalesCat5 from busdta.M4016 ,busdta.F4015, BUSDTA.f4101 where OPAN8=" + customerID + " and OPORTP='S" + customerID + "'" + " and ltrim(rtrim(oplitm)) = ltrim(rtrim(imlitm)) order by OPOSEQ";

                //String Query = QueryTemplate + "right outer join" + QueryPreOrder;

                string QueryPreOrder = " select POORTP as Template_Name , POAN8 as Invoice#, POOSEQ as Sequence#, POLITM as Item# , (inv.OnHandQuantity - inv.CommittedQuantity -  inv.HeldQuantity) as AvailableQty, IMDSC1 as ItemDescription, IMITM as ItemId, POQTYU/10000 as PreOrderQty, POQTYU/10000" +
                                       " as UsualQty,POUOM as UM, POLNTY as StkType, Dateformat(BUSDTA.DateG2J(OTEFTJ),'mm/dd/yyyy') as EffectiveDate, Dateformat(BUSDTA.DateG2J(OTEXDJ),'mm/dd/yyyy') as ExpiredDate ,POSRP1" +
                                       " as SalesCat1, POSRP5 as SalesCat5, POSTDT AS 'StopDate' from  busdta.M4016 po left join BUSDTA.f4101 im ON po.POLITM = im.IMLITM left join" +
                                       " busdta.F4015 ot on po.POAN8 = ot.otan8 and po.POLITM= ot.otlitm left join busdta.ItemConfiguration ic on imitm = ic.itemId join busdta.Inventory inv on inv.ItemId = im.IMITM where POAN8=" + customerID + " and POORTP='S" + customerID + "' and ltrim(rtrim(politm)) = ltrim(rtrim(imlitm)) " +
                                       " and POSTDT='" + date.ToString("yyyy-MM-dd") + "' and ic.RouteEnabled = 1 and ic.allowSearch=1  GROUP BY POLITM, POORTP, POAN8,AvailableQty, POOSEQ, IMDSC1, IMITM, POQTYU, OTQTYU, POUOM, POLNTY, OTEFTJ, OTEXDJ, POSRP1, POSRP5, POSTDT" +
                                       " order by POOSEQ";

                DataSet templateItems = Helpers.DbEngine.ExecuteDataSet(QueryPreOrder);

                ItemManager itemManager = new ItemManager();

                Random qtyOnHand = new Random();

                if (templateItems.HasData())
                {
                    // Dictionary<string, List<string>> templateUms = ResourceManager.GetItemUMList;
                    Dictionary<string, List<string>> templateUms = UoMManager.GetItemUMList;
                    foreach (DataRow templateItem in templateItems.Tables[0].Rows)
                    {
                        TemplateItem order = new TemplateItem();
                        order.UsualQty = Convert.ToInt32(templateItem["PreOrderQty"]);
                        order.PreOrderQty = Convert.ToInt32(templateItem["PreOrderQty"]);
                        order.UM = templateItem["UM"].ToString();
                        order.SeqNo = Convert.ToInt32(templateItem["Sequence#"].ToString());
                        order.ItemNumber = templateItem["Item#"].ToString().Trim();
                        order.ItemDescription = templateItem["ItemDescription"].ToString().Trim();
                        order.ItemId = templateItem["ItemId"].ToString().Trim();
                        if (templateItem["EffectiveDate"].ToString() == null || templateItem["EffectiveDate"].ToString() == "")
                            order.EffectiveFrom = DateTime.Now.ToString("MM/dd/yyyy");
                        else
                            order.EffectiveFrom = templateItem["EffectiveDate"].ToString();
                        if (templateItem["ExpiredDate"].ToString() == null || templateItem["ExpiredDate"].ToString() == "")
                            order.EffectiveThru = DateTime.Now.ToString("MM/dd/yyyy");
                        else
                            order.EffectiveThru = templateItem["EffectiveDate"].ToString();
                        order.StkType = templateItem["StkType"].ToString();
                        order.SalesCat1 = templateItem["SalesCat1"].ToString();
                        order.SalesCat5 = templateItem["SalesCat5"].ToString();
                        order.InclOnTmplt = false;
                        order.AvailableQty = Convert.ToInt32(templateItem["AvailableQty"].ToString().Trim());
                        order.AppliedUMS = templateUms.ContainsKey(order.ItemNumber.Trim()) ? templateUms[order.ItemNumber.Trim()] : new List<string>() { order.UM };
                        order.ActualQtyOnHand = qtyOnHand.Next(5, 50);

                        result.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreOrderManager][GetTemplateItemsForCustomer][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        /// <summary>
        /// Save PreOrder for the given customer
        /// </summary>
        /// <param name="preOrderItems">List of Template Items</param>
        /// <param name="customerNumber">Invoice Number</param>
        public void SavePreOrder(ObservableCollection<TemplateItem> preOrderItems, string customerNumber, DateTime StopDate)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][PreOrderManager][Start:SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "]");
            try
            {
                //DateTime dtStop = Convert.ToDateTime(StopDate);
                //string stopdate = dtStop.ToString("yyyy-MM-dd");
                string checkPreOrder = " SELECT COUNT(1) FROM BUSDTA.M4016 WHERE POORTP = 'S" + customerNumber + "' and POAN8 = " + customerNumber + " and POSTDT='" + StopDate.ToString("yyyy-MM-dd") + "'";
                string count = DbEngine.ExecuteScalar(checkPreOrder);
                if (Convert.ToInt32(count) > 0)
                {
                    string cmdDeletePreOrder = "Delete from busdta.M4016 where POORTP = 'S" + customerNumber + "' and POAN8 = " + customerNumber + " and POSTDT='" + StopDate.ToString("yyyy-MM-dd") + "'";
                    DbEngine.ExecuteNonQuery(cmdDeletePreOrder);
                }
                foreach (TemplateItem templateItem in preOrderItems)
                {

                    if (templateItem.PreOrderQty > 0)
                    {
                        string cmdSavePreOrder = "insert into busdta.M4016 (POORTP, POAN8, POOSEQ, POITM, POLITM, POQTYU, POUOM, POLNTY, POSRP1, POSRP5, POCRBY, POCRDT, POUPBY, POUPDT, POSTDT) " +
                            "values " +
                            "('S" + customerNumber + "', " + customerNumber + ", " + templateItem.SeqNo + ", " + templateItem.ItemId + ", '" + templateItem.ItemNumber + "' " +
                            ", " + templateItem.PreOrderQty * 10000 + ", '" + templateItem.UM + "', 'S', " + "'" + templateItem.SalesCat1 + "'" +
                            "," + "'" + templateItem.SalesCat5 + "'" + "," + "'" + CommonNavInfo.UserName + "'" + "," + "'" + DateTime.Now.ToString("yyyy-MM-dd") + "'" + "," + "'" + CommonNavInfo.UserName + "'" + "," + "'" + DateTime.Now.ToString("yyyy-MM-dd") + "'," + "'" + StopDate.ToString("yyyy-MM-dd") + "'" + ");";
                        DbEngine.ExecuteNonQuery(cmdSavePreOrder);
                    }
                }
                // ResourceManager.Transaction.AddTransactionInQueueForSync(Transaction.SaveTemplate, SyncQueueManager.Priority.everything);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreOrderManager][SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][PreOrderManager][End:SavePreOrder][customerNumber=" + customerNumber + ",templateItems=" + preOrderItems.ToString() + "]");

        }


        public ObservableCollection<PreOrderStops> GetFutureStopDates(string CustomerID, DateTime stopDate)
        {
            DateTime baseDate = Convert.ToDateTime(_baseDate);
            ObservableCollection<PreOrderStops> stopDates = new ObservableCollection<PreOrderStops>();
            Logger.Info("[SalesLogicExpress.Application.Managers][PreOrderManager][Start:GetFutureStopDates][customerNumber=" + CustomerID + ",stopDate=" + stopDate.ToString("yyyy-MM-dd hh:mm") + "]");
            try
            {
                int index = 0;
                //string query = "CALL BUSDTA.getPreOrderDatesForCustomer(CustomerNum =" + CustomerID + ", SelectedDate ='" + stopDate.ToString("yyyy-MM-dd") + "', BaseDate='" + baseDate.ToString("yyyy-MM-dd") + "')";
                string query = "select TOP 5 RPSTDT from busdta.m56m0004 where RPSTTP <> 'Moved' AND RPSTTP <> 'Unplanned' and RPSTDT > cast(getdate() as date) and RPAN8 = " + CustomerID + " order by 1";
                DataSet preOrderDates = DbEngine.ExecuteDataSet(query);
                DateTime tempDate = new DateTime();
                if (preOrderDates.HasData())
                {
                    foreach (DataRow stopRow in preOrderDates.Tables[0].Rows)
                    {
                        PreOrderStops stop = new PreOrderStops();
                        tempDate = Convert.ToDateTime(stopRow["RPSTDT"]);
                        stop.Weekday = tempDate.DayOfWeek.ToString();
                        stop.Stopdate = tempDate.ToString("MM'/'dd'/'yyyy ");
                        stopDates.Add(stop);
                        index++;
                        if (index > 4)
                        {
                            break;
                        }
                    }
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][PreOrderManager][End:GetFutureStopDates][customerNumber=" + CustomerID + ",stopDate=" + stopDate.ToString("yyyy-MM-dd hh:mm") + "]");
                return stopDates;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreOrderManager][GetFutureStopDates][customerNumber=" + CustomerID + ",templateItems=" + stopDate.ToString("yyyy-MM-dd hh:mm") + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }

        public DateTime CheckPreOrderForSameDate(string customerNumber, DateTime StopDate)
        {
            bool isPreOrderExist = false;
            DateTime activityDate = default(DateTime);//DateTime.Now;
            try
            {
                string checkPreOrder = " SELECT COUNT(1) FROM BUSDTA.M4016 WHERE POORTP = 'S" + customerNumber + "' and POAN8 = " + customerNumber + " and DATEFORMAT( POSTDT, 'yyyy-dd-mm' )='" + StopDate.ToString("yyyy-dd-MM") + "'";
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(checkPreOrder));
                if (count > 0)
                {
                    isPreOrderExist = true;
                }
                else
                    ViewModelPayload.PayloadManager.PreOrderPayload.IsPreOrderForAnotherDate = true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PreOrderManager][CheckPreOrderForSameDate][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }

            if (isPreOrderExist)
            {
                string query = string.Format("select * FROM busdta.M50012 where TDTYP = 'PreOrder' AND TDAN8='" + customerNumber + "' ");
                DataSet activity = DbEngine.ExecuteDataSet(query);

                if (activity.HasData())
                {
                    for (int i = 0; i < activity.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = activity.Tables[0].Rows[i];
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ViewModelPayload.PreOrderPayload));
                        MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(dr["TDDTLS"].ToString()));
                        //PreOrderStops preOrderStops = (PreOrderStops)serializer.ReadObject(stream);

                        ViewModelPayload.PreOrderPayload preorderpayload = new ViewModelPayload.PreOrderPayload();
                        preorderpayload = (ViewModelPayload.PreOrderPayload)serializer.ReadObject(stream);
                        if (preorderpayload.StopDate.Date == StopDate.Date)
                        {
                            activityDate = Convert.ToDateTime(dr["TDENDTM"]);
                            break;
                        }
                    }
                }

            }

            return activityDate;
            //throw new NotImplementedException();
        }
    }
}
