﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using System.Data;
using SalesLogicExpress.Application.Helpers;


namespace SalesLogicExpress.Application.Managers
{
    public class CustomerStopSequencingManager
    {
        public static  readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerStopSequencingManager");

        public static  ObservableCollection<CustomerStopSequencingModel> GetCustomerStopSequencingForWeek(int dayNo, string day)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][Start:GetCustomerStopSequencingForWeek]");
            ObservableCollection<CustomerStopSequencingModel> customerStopList = new ObservableCollection<CustomerStopSequencingModel>();
            try
            {
                string baseDate = System.Configuration.ConfigurationManager.AppSettings["BaseDate"].ToString().Replace("-", "");
                /*Following query selects Route Standard sequence for customers present on route*/
                string query = @"select r.RSAN8 as 'CustomerId', a.ABALPH as 'CustomerName' ,d.DCDDC as 'DeliveryDayCode', (select DMDDCD FROM BUSDTA.M56M0001 where DMDDC =(d.DCDDC)) as 'DDCDescription'
                                    ,r.RSWN as 'WeekNumber', 
                                    r.RSDN as 'DayNumber', r.RSSN as 'SequenceNumber'
                                    from BUSDTA.F03012 c join BUSDTA.M56M0003 r
                                    on c.aian8 = r.RSAN8 join BUSDTA.M56M0002 d
                                    on d.DCDDC = c.AISTOP join BUSDTA.F0101 a
                                    on a.ABAN8 = c.AIAN8 join BUSDTA.Customer_Route_Map crm
                                    on aian8 = crm.CustomerShipToNumber
                                    where   r.RSDN =" + dayNo + " and crm.IsActive = 'Y' and crm.RelationshipType = 'AN85' ORDER BY r.RSSN";
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                CustomerStopSequencingModel cssModel;
                if (result!=null)
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        cssModel = new CustomerStopSequencingModel();
                        cssModel.CustomerCode = row["CustomerId"].ToString().Trim();
                        cssModel.Name = row["CustomerName"].ToString().Trim();
                        cssModel.IdCode = row["DeliveryDayCode"].ToString().Trim();
                        cssModel.IdLable = row["DDCDescription"].ToString().Trim();
                        cssModel.ListID = row["WeekNumber"].ToString().Trim();
                        cssModel.DayOfWeek = day.Trim();
                        cssModel.DayNo = row["DayNumber"].ToString().Trim();
                        //cssModel.DayOfWeek = row["DayNumber"].ToString().Trim();
                        customerStopList.Add(cssModel);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][GetCustomerStopSequencingForWeek][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][End:GetCustomerStopSequencingForWeek]");
            return customerStopList;
        }

        public static bool UpadateSequenceNo(string dayNo, string customerID, string weekNo, string descCode,string sequenceNO)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][Start:UpadateSequenceNo]");
            bool flag = false;
            try
            {
                string baseDate = System.Configuration.ConfigurationManager.AppSettings["BaseDate"].ToString().Replace("-", "");
                string query = @" UPDATE BUSDTA.M56M0003 R SET R.RSSN='" + sequenceNO + @"'
                                from BUSDTA.F03012 c join BUSDTA.M56M0003 r
                                on c.aian8 = r.RSAN8 join BUSDTA.M56M0002 d
                                on d.DCDDC = c.AISTOP join BUSDTA.F0101 a
                                on a.ABAN8 = c.AIAN8
                                WHERE R.RSWN = " + weekNo + @" AND 
                                R.RSDN = " + dayNo + @" AND 
                                R.RSAN8='" + customerID + @"'";
                               
                int i= DbEngine.ExecuteNonQuery(query);
                if (i>0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][UpadateSequenceNo][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][End:UpadateSequenceNo]");
            return flag;
        }

        public static DataTable GetDatesForDayOfWeek(int dayIndex)
        {
            DataSet dsReturn = new DataSet();
            DataTable dtReturn = new DataTable();
            dtReturn.Columns.Add("Date");
            dtReturn.Columns.Add("WeekNo");
            dtReturn.Columns.Add("CustCount");
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][End:GetDatesForDayOfWeek][dayIndex "+dayIndex+"]");
            string baseDate = System.Configuration.ConfigurationManager.AppSettings["BaseDate"].ToString().Replace("-", "");
            try
            {
                string query = @"SELECT SUM(RPCACT+RPPACT),  RPSTDT as 'RPSTDT' FROM BUSDTA.M56M0004  GROUP BY RPSTDT 
                        having RPSTDT >= convert( date, getdate(),110) AND SUM(RPCACT+RPPACT)=0 
                        AND datepart(dw,RPSTDT)= " + dayIndex + @" ORDER BY RPSTDT ";
                dsReturn = DbEngine.ExecuteDataSet(query);
                if (dsReturn.Tables[0].Rows.Count>0)
                {
                    for (int i = 0; i < dsReturn.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = dtReturn.NewRow();
                        string calDate = Convert.ToDateTime(dsReturn.Tables[0].Rows[i]["RPSTDT"].ToString()).Date.ToString("yyyy-MM-dd");
                        dr["Date"] =calDate;
                        string queryCustCount = "SELECT count(*) FROM BUSDTA.M56M0004  WHERE RPSTDT ='" + calDate + "'";
                        int custCount = Convert.ToInt32(DbEngine.ExecuteScalar(queryCustCount));
                        dr["CustCount"] = custCount;
                        string weekNoForDate = string.Empty;
                        string queryForWeekNo = "SELECT BUSDTA.GetWeekForDate('"+calDate+"','"+baseDate+"')";
                        weekNoForDate=DbEngine.ExecuteScalar(queryForWeekNo);
                        dr["WeekNo"] = weekNoForDate.Trim();
                        dtReturn.Rows.Add(dr);
                    }
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][End:GetDatesForDayOfWeek][dayIndex " + dayIndex + "]");
                return dtReturn;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][GetDatesForDayOfWeek][dayIndex " + dayIndex + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                
                throw;
            }
        }
        public static bool UpadateSequenceNoForM56M0004(string dateParameter, string customerID, string sequenceNO)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][Start:UpadateSequenceNoForM56M0004][dateParameter=" + dateParameter + "] [customerID  =" + customerID + "] [sequenceNO = " + sequenceNO + "]");
            bool flag = false;
            try
            {
                string query = @"update BUSDTA.M56M0004  set RPSN =" + sequenceNO + " where RPAN8='" + customerID + "' AND RPSTTP in('Planned','Moved')  AND RPSTDT IN ( '" + dateParameter + "') ";
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][UpadateSequenceNoForM56M0004][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][End:UpadateSequenceNoForM56M0004][dateParameter=" + dateParameter + "] [customerID  =" + customerID + "] [sequenceNO = " + sequenceNO + "]");
            return flag;
        }

        public static bool UpadateReschduledAndUnplanned(string dateParameter)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][Start:UpadateSequenceNoForM56M0004][dateParameter=" + dateParameter + "]");
            bool flag = false;
            try
            {
                string queryForRescCuct = " SELECT RPAN8 FROM BUSDTA.M56M0004  WHERE RPSTDT ='"+dateParameter+"' and RPSTTP   in ('Rescheduled') order by RPCRDT";
                string queryForPlannedCust = "SELECT RPAN8 FROM BUSDTA.M56M0004  WHERE RPSTDT ='"+dateParameter+"' and RPSTTP   in ('Unplanned') order by RPCRDT";
                DataTable dtRescCust = new DataTable();
                DataTable dtUnplannedCust = new DataTable();
                dtRescCust = DbEngine.ExecuteDataSet(queryForRescCuct).Tables[0];
                dtUnplannedCust = DbEngine.ExecuteDataSet(queryForPlannedCust).Tables[0];
                string customerID = string.Empty;
                int sequenceNO = 1;
                for (int i = 0; i < dtRescCust.Rows.Count; i++)
                {
                    customerID = dtRescCust.Rows[i]["RPAN8"].ToString();
                    string query = @"update BUSDTA.M56M0004  set RPSN =" + sequenceNO + " where RPAN8='" + customerID + "'   AND RPSTDT IN ( '" + dateParameter + "') ";
                    DbEngine.ExecuteNonQuery(query);
                    sequenceNO++;
                }
                for (int i = 0; i < dtUnplannedCust.Rows.Count; i++)
                {
                    customerID = dtUnplannedCust.Rows[i]["RPAN8"].ToString();
                    string query = @"update BUSDTA.M56M0004  set RPSN =" + sequenceNO + " where RPAN8='" + customerID + "'  AND RPSTDT IN ( '" + dateParameter + "') ";
                    DbEngine.ExecuteNonQuery(query);
                    sequenceNO++;
                }

                
                
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][UpadateSequenceNoForM56M0004][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerStopSequencingManager][End:UpadateSequenceNoForM56M0004][dateParameter=" + dateParameter + "]");
            return flag;
        }
    }
}
