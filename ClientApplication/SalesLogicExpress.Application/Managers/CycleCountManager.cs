﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System.Data;
using System.Globalization;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    class CycleCountManager : IDisposable
    {
        int TransactionTypeID = 0;

        public CycleCountManager()
        {
            SetTransactionTypeID();
        }

        private void SetTransactionTypeID()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CycleCountManager");
        public TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        public ObservableCollection<CycleCount> GetCycleCountList(int routeID)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:GetCycleCountList]");
                ObservableCollection<CycleCount> cycleCountRequestCollection = new ObservableCollection<CycleCount>();
                // query to get list of cycle count request.

                string query = "select c.*, s.StatusTypeDESC,s.StatusTypeID,s.StatusTypeCD, u.Name, '4057732548' as Contact , (select StatusTypeCD from busdta.Status_Type where StatusTypeID = c.CycleCountTypeId) as RequestTypeStatusCD from BUSDTA.Cycle_Count_Header c JOIN BUSDTA.Status_Type s ";
                query = query + " on s.StatusTypeID = c.StatusId join BUSDTA.user_master u";
                query = query + " on u.App_user_id = c.InitiatorId where c.RouteId = '" + routeID + "'";
                query = query + " order by c.UpdatedDatetime desc";

                DataSet result = DbEngine.ExecuteDataSet(query);
                Dictionary<string, int> itemsInfo = new Dictionary<string, int>();
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        CycleCount cycleCount = new CycleCount();

                        cycleCount.CycleCountID = Convert.ToInt32(row["CycleCountID"]);
                        cycleCount.CycleCountStatus = row["StatusTypeDESC"].ToString().Trim().ToLower();

                        cycleCount.CycleCountStatusID = Convert.ToInt32(row["StatusTypeID"].ToString().Trim().ToLower());
                        cycleCount.CycleCountStatusCD = row["StatusTypeCD"].ToString().Trim().ToLower();
                        string name = row["Name"].ToString().Trim().ToLower();
                        cycleCount.CustomerContactName = textInfo.ToTitleCase(name);
                        cycleCount.CustomerContactNumber = row["Contact"].ToString().Trim().ToLower();
                        cycleCount.CycleCountDate = (row["CycleCountDatetime"].ToString() != null) ? Convert.ToDateTime(row["CycleCountDatetime"].ToString()) : new DateTime();
                        cycleCount.IsCountInitiated = Convert.ToInt32(row["IsCountInitiated"].ToString().Trim().ToLower()) == 0 ? false : true;
                        cycleCount.RequestTypeStatusCD = row["RequestTypeStatusCD"].ToString().Trim().ToLower();
                        itemsInfo = GetItemsCount(cycleCount.CycleCountStatusCD, cycleCount.CycleCountID, cycleCount.CycleCountStatusID, cycleCount.RequestTypeStatusCD);

                        cycleCount.ItemsToCount = itemsInfo["ItemsToCount"];
                        cycleCount.ItemsCounted = itemsInfo["ItemsCounted"];
                        if (itemsInfo["ItemsToCount"] == 0 && itemsInfo["ItemsCounted"] == 0 && cycleCount.RequestTypeStatusCD.ToLower() == StatusTypesEnum.PHYSCL.ToString().ToLower())
                            cycleCount.IsCountEnable = false;
                        //int itemsCountedAfterRecount = 0;
                        if (cycleCount.CycleCountStatus.ToLower() == "submitted")
                        {
                            cycleCount.ItemsCountedAfterRecount = GetInfoForSubmittedStatus(cycleCount.CycleCountID);
                        }
                        // Recount number will always be less by 1, as default value is 1
                        cycleCount.CycleCountRevision = Convert.ToInt32(DbEngine.ExecuteScalar("(select isnull(max(ReCountNumber),1) from busdta.Cycle_Count_Detail where CycleCountID = " + cycleCount.CycleCountID + ")")) - 1;

                        cycleCountRequestCollection.Add(cycleCount);
                    }
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:GetCycleCountList]");

                return cycleCountRequestCollection;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CycleCountManager][GetCycleCountList][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return null;
            }
        }
        public int GetInfoForSubmittedStatus(int countID)
        {
            int itemsCountedAfterRecount = 0;
            List<CycleCountItems> allItems = new List<CycleCountItems>();
            var maxRecount = DbEngine.ExecuteScalar("select isnull(max(ReCountNumber),0) from busdta.Cycle_Count_Detail where CycleCountID = " + countID + "");
            if (maxRecount != null)
                if (Convert.ToInt32(maxRecount.ToString().Trim()) > 1)
                {
                    string query = "select * from busdta.Cycle_Count_Detail ccd where ccd.CycleCountID = " + countID;
                    query = query + " and ccd.ReCountNumber = (select max(ReCountNumber) from busdta.Cycle_Count_Detail where CycleCountID = " + countID + ")";
                    query = query + " AND ccd.ItemStatus = 1";

                    DataSet result = DbEngine.ExecuteDataSet(query);
                    if (result.HasData())
                    {
                        itemsCountedAfterRecount = result.Tables[0].Rows.Count;
                    }
                }

            return itemsCountedAfterRecount;
        }

        public Dictionary<string, int> GetItemsCount(string countStatusCD, int countID, int requestStatusID, string requestTypeStatusCD)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:GetCycleCountItemsCount]");
            Dictionary<string, int> itemsCount = new Dictionary<string, int>();
            ObservableCollection<CycleCountItems> allItems = new ObservableCollection<CycleCountItems>();
            ObservableCollection<CycleCountItems> recountItems = new ObservableCollection<CycleCountItems>();
            ObservableCollection<CycleCountItems> newList = new ObservableCollection<CycleCountItems>();
            int itemsToCount = 0, itemsCounted = 0;
            try
            {
                allItems = GetItemDetailForRequest(countID, requestStatusID);

                if (countStatusCD.ToLower() == StatusTypesEnum.SUBMIT.ToString().ToLower() || countStatusCD.ToLower() == StatusTypesEnum.ACCEPT.ToString().ToLower())
                {
                    for (int i = 0; i < allItems.Count; i++)
                    {
                        //if (allItems[i].ItemStatus)
                        //{
                        itemsCounted++;
                        //}
                    }
                }
                else if (countStatusCD.ToLower() == StatusTypesEnum.RECNT.ToString().ToLower())
                {
                    recountItems = GetRecountItemsList(countID);
                    itemsToCount = recountItems.Count;
                    //Logic bypassed as we have to show all request items as 'items counted'
                    //for (int i = 0; i < allItems.Count; i++)
                    //{
                    //    if (allItems[i].ItemStatus)
                    //    {
                    //        itemsCounted++;
                    //    }
                    //}
                    itemsCounted = allItems.Count();
                }
                else if (countStatusCD.ToLower() == StatusTypesEnum.VOID.ToString().ToLower())
                {
                    for (int i = 0; i < allItems.Count; i++)
                    {
                        // In void we have to show total item to coount
                        //if (allItems[i].ItemStatus)
                        //{
                        //    itemsCounted++;
                        //}
                        //else
                        //    itemsToCount++;
                        itemsToCount = allItems.Count();

                    }
                }
                else
                {
                    // Code required if, we have to show 'ItemCounted' and 'ItemsToCount' on cycle count tab.
                    /* for (int i = 0; i < allItems.Count; i++)
                     {
                         if (allItems[i].ItemStatus == 1)
                         {
                             itemsCounted++;
                         }
                         else
                             itemsToCount++;
                     }*/
                    itemsToCount = allItems.Count();
                }
                if (requestTypeStatusCD.ToLower() == StatusTypesEnum.PHYSCL.ToString().ToLower() && allItems.Count() == 0)
                    itemsCounted = itemsToCount = 0;
                itemsCount["ItemsCounted"] = itemsCounted;
                itemsCount["ItemsToCount"] = itemsToCount;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CycleCountManager][GetCycleCountItemsCount][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            return itemsCount;
        }
        public ObservableCollection<CycleCountItems> GetItemDetailForRequest(int requestId, int requestStatusID)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:GetItemDetailForRequest]");

                var recountItemsList = GetRecountItemsList(requestId);

                var getAllCCItems = "select ccd.UpdatedDatetime,ccd.UpdatedBy,ccd.CreatedDatetime,ccd.CreatedBy,ccd.CountedQty,ccd.ItemId,ccd.ReCountNumber,ccd.CycleCountDetailID,ccd.CycleCountID, ccd.ItemStatus, " +
                                    "im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMLNTY as StkType, im.IMSRP1  as SalesCat1, im.IMSRP5 as SalesCat5, im.IMSRP4 as SalesCat4, iconfig.PrimaryUM as PrimaryUM, " +
                                    "iconfig.TransactionUM as TransactionUM from busdta.Cycle_Count_Detail ccd inner join busdta.F4101 im on ccd.itemid = im.imitm inner join busdta.Itemconfiguration iconfig on ccd.itemid = iconfig.itemid" +
                                    " where ccd.CycleCountID =" + requestId + " and ccd.ReCountNumber = (select max(ReCountNumber) from busdta.Cycle_Count_Detail where itemid = ccd.itemid and CycleCountID =" + requestId + ") and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 ";//and ccd.ItemStatus = 1";
                var CCItemsSet = DbEngine.ExecuteDataSet(getAllCCItems);
                var allListItems = new ObservableCollection<CycleCountItems>();

                if (CCItemsSet.HasData())
                {

                    foreach (DataRow dataRow in CCItemsSet.Tables[0].Rows)
                    {
                        var CCItem = new CycleCountItems();
                        CCItem.CountedQuantity = Convert.ToInt32(dataRow["CountedQty"].ToString().Trim());
                        CCItem.CycleCountDetailID = Convert.ToInt32(dataRow["CycleCountDetailID"].ToString().Trim());
                        CCItem.ReCountNumber = Convert.ToInt32(dataRow["ReCountNumber"].ToString().Trim());
                        CCItem.ItemId = dataRow["ItemId"].ToString().Trim();
                        CCItem.ItemId = CCItem.ItemId;

                        CCItem.CycleCountID = Convert.ToInt32(dataRow["CycleCountID"].ToString().Trim());
                        CCItem.ItemStatus = Convert.ToInt32(dataRow["ItemStatus"]) == 0 ? false : true;
                        CCItem.ItemDescription = dataRow["ItemDescription"].ToString().Trim();
                        CCItem.StkType = dataRow["StkType"].ToString().Trim();
                        CCItem.SalesCat1 = dataRow["SalesCat1"].ToString().Trim();
                        CCItem.SalesCat4 = dataRow["SalesCat4"].ToString().Trim();
                        CCItem.PrimaryUM = dataRow["PrimaryUM"].ToString().Trim();
                        CCItem.ItemNumber = dataRow["ItemNumber"].ToString().Trim();
                        // CCItem.CountedQtyUoM = dataRow["TransactionUM"].ToString().Trim();
                        CCItem.TransactionUOM = dataRow["TransactionUM"].ToString().Trim();
                        CCItem.UM = CCItem.TransactionUOM;
                        CCItem.TransactionQty = 9999;

                        CCItem.IsForCount = recountItemsList != null && recountItemsList.Count() != 0 ? false : true;
                        var factor = 0.0m;
                        if (UoMManager.ItemUoMFactorList != null)
                        {
                            var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == CCItem.ItemId.Trim()) && (x.FromUOM == CCItem.PrimaryUM.ToString()) && (x.ToUOM == CCItem.UM));
                            factor = itemUomConversion == null ? Convert.ToDecimal(UoMManager.GetUoMFactor(CCItem.PrimaryUM, CCItem.UM, Convert.ToInt32(CCItem.ItemId.Trim()), CCItem.ItemNumber.Trim())) : Convert.ToDecimal(itemUomConversion.ConversionFactor);
                        }
                        else
                        {
                            factor = Convert.ToDecimal(UoMManager.GetUoMFactor(CCItem.PrimaryUM, CCItem.UM, Convert.ToInt32(CCItem.ItemId.Trim()), CCItem.ItemNumber.Trim()));
                        }
                        // UoM conversion done as we store countedquantity in primary uom and have to show in transaction uom.
                        CCItem.CountedQuantity = Convert.ToInt32(CCItem.CountedQuantity * factor);
                        allListItems.Add(CCItem);
                    }
                }

                if (recountItemsList.Any())
                {
                    var CC_list = allListItems.ToList();

                    foreach (CycleCountItems CCitem in recountItemsList)
                    {
                        if (CC_list.Any(x => x.ItemId == CCitem.ItemId))
                        {
                            //for (int index = 0; index < allListItems.Count(); index++)
                            //    if (allListItems[index].ItemId == CCitem.ItemId)
                            //        allListItems.RemoveAt(index);
                            CC_list.RemoveAll(x => x.ItemId == CCitem.ItemId);
                        }
                        CC_list.Add(CCitem);
                    }
                    allListItems.Clear();
                    allListItems = CC_list.ToObservableCollection();
                }

                #region Check for check depending on CycleCountStatusId scanner behaviour
                // Recount Items 
                if (ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any())
                    ResourceManager.StatusTypes = new InventoryManager().GetStatusTypes();
                var typeName = string.Empty;
                var statusType = ResourceManager.StatusTypes.FirstOrDefault(x => x.ID == requestStatusID);
                if (statusType != null)
                {
                    typeName = statusType.StatusCode.Contains('-') ? statusType.StatusCode.Replace("-", "").ToLower().Trim() : statusType.StatusCode.ToLower().Trim();
                }
                #endregion
                if (typeName.ToLower().Trim() == StatusTypesEnum.VOID.ToString().ToLower() ||
                    typeName.ToLower().Trim() == StatusTypesEnum.SUBMIT.ToString().ToLower() ||
                    typeName.ToLower().Trim() == StatusTypesEnum.ACCEPT.ToString().ToLower())
                {
                    foreach (var ccItem in allListItems)
                    {
                        ccItem.IsForCount = false;
                    }
                }

                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:GetItemDetailForRequest]");
                return allListItems;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CycleCountManager][GetItemDetailForRequest][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return null;
            }
        }

        public ObservableCollection<CycleCountItems> GetRecountItemsList(int requestId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:GetRecountItemsList]");

                var recountItemquery = "select ccd.UpdatedDatetime,ccd.UpdatedBy,ccd.CreatedDatetime,ccd.CreatedBy,ccd.CountedQty,ccd.ItemId,ccd.ReCountNumber,ccd.CycleCountDetailID,ccd.CycleCountID, ccd.ItemStatus, " +
                                    "im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMLNTY as StkType, im.IMSRP1  as SalesCat1, im.IMSRP5 as SalesCat5, im.IMSRP4 as SalesCat4, iconfig.PrimaryUM as PrimaryUM, iconfig.TransactionUM as TransactionUM" +
                                    " from busdta.Cycle_Count_Detail ccd inner join busdta.F4101 im on ccd.itemid = im.imitm inner join busdta.Itemconfiguration iconfig on ccd.itemid = iconfig.itemid" +
                                    " where ccd.CycleCountID = " + requestId + " and ccd.ReCountNumber = (select isnull(max(ReCountNumber),0) from busdta.Cycle_Count_Detail where CycleCountID = " + requestId + ") and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 ";//and ccd.CountedQty = 0";
                var recountItemsSet = DbEngine.ExecuteDataSet(recountItemquery);
                var recountItemsList = new ObservableCollection<CycleCountItems>();
                if (recountItemsSet.HasData())
                {
                    foreach (DataRow dataRow in recountItemsSet.Tables[0].Rows)
                    {
                        var CCItem = new CycleCountItems();
                        CCItem.CountedQuantity = Convert.ToInt32(dataRow["CountedQty"].ToString().Trim());
                        CCItem.CycleCountDetailID = Convert.ToInt32(dataRow["CycleCountDetailID"].ToString().Trim());
                        CCItem.ReCountNumber = Convert.ToInt32(dataRow["ReCountNumber"].ToString().Trim());
                        CCItem.ItemId = dataRow["ItemId"].ToString().Trim();
                        CCItem.ItemId = CCItem.ItemId;

                        CCItem.CycleCountID = Convert.ToInt32(dataRow["CycleCountID"].ToString().Trim());
                        CCItem.ItemStatus = Convert.ToInt32(dataRow["ItemStatus"]) == 0 ? false : true;
                        CCItem.ItemDescription = dataRow["ItemDescription"].ToString().Trim();
                        CCItem.StkType = dataRow["StkType"].ToString().Trim();
                        CCItem.SalesCat1 = dataRow["SalesCat1"].ToString().Trim();
                        CCItem.SalesCat4 = dataRow["SalesCat4"].ToString().Trim();
                        CCItem.PrimaryUM = dataRow["PrimaryUM"].ToString().Trim();
                        CCItem.ItemNumber = dataRow["ItemNumber"].ToString().Trim();
                        CCItem.CountedQtyUoM = CCItem.PrimaryUM;
                        CCItem.IsForCount = true;

                        CCItem.TransactionUOM = dataRow["TransactionUM"].ToString().Trim();
                        CCItem.UM = CCItem.TransactionUOM;
                        CCItem.TransactionQty = 9999; /// TO BE USED IN PICK MANAGER

                        var factor = 0.0m;
                        if (UoMManager.ItemUoMFactorList != null)
                        {
                            var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == CCItem.ItemId.Trim()) && (x.FromUOM == CCItem.PrimaryUM.ToString()) && (x.ToUOM == CCItem.UM));
                            factor = itemUomConversion == null ? Convert.ToDecimal(UoMManager.GetUoMFactor(CCItem.PrimaryUM, CCItem.UM, Convert.ToInt32(CCItem.ItemId.Trim()), CCItem.ItemNumber.Trim())) : Convert.ToDecimal(itemUomConversion.ConversionFactor);
                        }
                        else
                        {
                            factor = Convert.ToDecimal(UoMManager.GetUoMFactor(CCItem.PrimaryUM, CCItem.UM, Convert.ToInt32(CCItem.ItemId.Trim()), CCItem.ItemNumber.Trim()));
                        }
                        // UoM conversion done as we store countedquantity in primary uom and have to show in transaction uom.
                        CCItem.CountedQuantity = Convert.ToInt32(CCItem.CountedQuantity * factor);
                        recountItemsList.Add(CCItem);
                    }
                }


                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:GetRecountItemsList]");

                return recountItemsList;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CycleCountManager][GetRecountItemsList][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return null;
            }
        }

        /// <summary>
        /// UpdateCycleCountItems() is used to upadate items, when we submit or hold cycle count request.
        /// </summary>
        /// <param name="itemsToUpdate"></param>
        public bool UpdateCycleCountItems(ObservableCollection<CycleCountItems> itemsToUpdate, int requestStatusID, string requestStausCode)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:UpdateItemsCycleCount]");
                var itemList = itemsToUpdate;
                var phyclCount = 0;
                ObservableCollection<CycleCountItems> itemsSorted = new ObservableCollection<CycleCountItems>();

                // Recount Items filtering
                if (ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any())
                    ResourceManager.StatusTypes = new InventoryManager().GetStatusTypes();
                var typeName = string.Empty;
                var statusType = ResourceManager.StatusTypes.FirstOrDefault(x => x.ID == requestStatusID);
                if (statusType != null)
                {
                    //\/\/typeName = statusType.StatusDesc.Replace("-", "");
                    typeName = statusType.StatusCode.ToString().ToLower();
                }
                if (typeName.ToLower().Trim() == StatusTypesEnum.RECNT.ToString().ToLower())
                {
                    foreach (var sortedItem in itemsToUpdate)
                    {
                        if (sortedItem.IsForCount)
                        {
                            itemsSorted.Add(sortedItem);
                        }
                    }
                    itemList = itemsSorted;
                }
                if (requestStausCode.ToLower() == StatusTypesEnum.PHYSCL.ToString().ToLower())
                {
                    string phyclCountResult = DbEngine.ExecuteScalar("select count (1) from busdta.Cycle_Count_Detail where CycleCountID = " + itemsToUpdate[0].CycleCountID + "");
                    phyclCount = string.IsNullOrEmpty(phyclCountResult) ? 0 : Convert.ToInt32(phyclCountResult);
                }
                foreach (var cycleItem in itemList)
                {
                    if (!string.Equals(cycleItem.CountedQtyUoM, cycleItem.PrimaryUM, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == cycleItem.ItemId) && (x.FromUOM == cycleItem.CountedQtyUoM) && (x.ToUOM == cycleItem.PrimaryUM));
                        double conversionFactor = 1;
                        conversionFactor = itemUomConversion != null ? itemUomConversion.ConversionFactor : UoMManager.GetUoMFactor(cycleItem.CountedQtyUoM, cycleItem.PrimaryUM, Convert.ToInt32(cycleItem.ItemId.Trim()), cycleItem.ItemNumber.Trim());
                        cycleItem.CountedQuantity = Convert.ToInt32(cycleItem.CountedQuantity * conversionFactor);
                    }
                    if (requestStausCode.ToLower() == StatusTypesEnum.PHYSCL.ToString().ToLower() && phyclCount == 0)
                    {

                        var itemStatus = cycleItem.ItemStatus ? "1" : "0";
                        var query = @"INSERT INTO BUSDTA.Cycle_Count_Detail (CycleCountID,CycleCountDetailID,ReCountNumber,ItemId,CountedQty,ItemStatus,OnHandQty,
                                      HeldQty,CountAccepted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) 
                            VALUES(" + cycleItem.TransactionID + ",isnull((select ( max(CycleCountDetailID) + 1) from BUSDTA.Cycle_Count_Detail),1),1," + cycleItem.ItemId + "," + cycleItem.CountedQuantity + "," +
                               " '" + itemStatus + "',(select OnHandQuantity from busdta.inventory where itemid = " + cycleItem.ItemId + "),(select HeldQuantity from busdta.inventory where itemid = " + cycleItem.ItemId + "),'0'," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                        DbEngine.ExecuteNonQuery(query);
                    }
                    else
                    {
                        var query = "UPDATE BUSDTA.Cycle_Count_Detail SET CountedQty=" + cycleItem.CountedQuantity + ",OnHandQty=(select OnHandQuantity from busdta.inventory where itemid = " + cycleItem.ItemId + "),HeldQty=(select HeldQuantity from busdta.inventory where itemid = " + cycleItem.ItemId + "),UpdatedBy=" + UserManager.UserId + ",UpdatedDatetime= getdate(), ItemStatus = 1 " +
                                       "WHERE CycleCountID=" + cycleItem.CycleCountID + " AND CycleCountDetailID = " + cycleItem.CycleCountDetailID + " AND ReCountNumber=" + cycleItem.ReCountNumber + " " +
                                       "AND ItemId=" + cycleItem.ItemId;
                        DbEngine.ExecuteNonQuery(query);
                    }

                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:UpdateItemsCycleCount]");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CycleCountManager][UpdateItemsCycleCount][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return false;
            }
        }

        public void UpdateCycleCountRequest(StatusTypesEnum activityType, int cycleCountID, int reasonID = 0)
        {
            try
            {
                if (ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any())
                    ResourceManager.StatusTypes = new InventoryManager().GetStatusTypes();
                var statusCode = 0;
                var statusType = ResourceManager.StatusTypes.FirstOrDefault(x => x.StatusCode.ToLower().Trim() == activityType.ToString().ToLower().Trim());
                if (statusType != null)
                {
                    //typeName = statusType.StatusDesc.Replace("-", "");
                    statusCode = statusType.ID;
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:UpdateCycleCountRequest]");
                switch (activityType)
                {
                    case StatusTypesEnum.HOLD:
                        string holdQuery = string.Empty;

                        DbEngine.ExecuteNonQuery(holdQuery);
                        break;
                    case StatusTypesEnum.VOID:
                        string Voidquery = "update BUSDTA.Cycle_Count_Header set ReasonCodeId = '" + reasonID + "', ";
                        Voidquery = Voidquery + " StatusId = " + statusCode + ", IsCountInitiated = 0,";
                        Voidquery = Voidquery + " UpdatedBy=" + UserManager.UserId + ",UpdatedDatetime= getdate() ";
                        Voidquery = Voidquery + " WHERE CycleCountID = '" + cycleCountID + "'";

                        int result = DbEngine.ExecuteNonQuery(Voidquery);

                        break;
                    case StatusTypesEnum.SUBMIT:
                        string submitQuery = string.Empty;
                        submitQuery = "update BUSDTA.Cycle_Count_Header set ";
                        submitQuery = submitQuery + " StatusId = " + statusCode + ", IsCountInitiated = 1,";
                        submitQuery = submitQuery + " UpdatedBy=" + UserManager.UserId + ",UpdatedDatetime= getdate() ";
                        submitQuery = submitQuery + " WHERE CycleCountID = '" + cycleCountID + "'";
                        DbEngine.ExecuteNonQuery(submitQuery);
                        break;

                }

                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:UpdateCycleCountRequest]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CycleCountManager][UpdateItemsCycleCount][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return;
            }
        }
        public void Dispose()
        {
        }

        public Dictionary<int, string> GetReasonCode()
        {
            Dictionary<int, string> reasonCodeList = new Dictionary<int, string>();
            try
            {
                string query = "select ReasonCodeId, ReasonCode, ReasonCodeDescription from busdta.ReasonCodeMaster";
                query = query + " where ReasonCodeType = 'Manual Pick'";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        int reasonCodeID = Convert.ToInt32(row["ReasonCodeId"]);
                        string reasonCode = row["ReasonCode"].ToString();
                        string reasonDescription = row["ReasonCodeDescription"].ToString();

                        reasonCodeList.Add(reasonCodeID, reasonDescription);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPaymentDetails " + ex.StackTrace + "]");
            }
            return reasonCodeList;
        }


        public ObservableCollection<ReasonCode> GetReasonCodeForVoid()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();

            try
            {

                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Cycle Count'").Tables[0];

                ReasonCode ReasonCode = new ReasonCode();


                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception e)
            {
                Logger.Error("SalesLogicExpress.Application.Managers.PickManager, GetReasonListForManualPick  Message: " + e.StackTrace);
                throw;
            }

            return reasonCodeList;

        }
        public int SaveOnHold(ObservableCollection<CycleCountItems> itemsList, string requestStausCode)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:SaveOnHold]");

            try
            {
                if (ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any())
                    ResourceManager.StatusTypes = new InventoryManager().GetStatusTypes();
                var statusCode = 0;
                var statusType = ResourceManager.StatusTypes.FirstOrDefault(x => x.StatusCode.ToLower().Trim() == StatusTypesEnum.HOLD.ToString().ToLower().Trim());
                if (statusType != null)
                {
                    //typeName = statusType.StatusDesc.Replace("-", "");
                    statusCode = statusType.ID;
                }

                foreach (var cycleItem in itemsList)
                {
                    if (!string.Equals(cycleItem.CountedQtyUoM, cycleItem.PrimaryUM, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == cycleItem.ItemId) && (x.FromUOM == cycleItem.CountedQtyUoM) && (x.ToUOM == cycleItem.PrimaryUM));
                        double conversionFactor = 1;
                        conversionFactor = itemUomConversion != null ? itemUomConversion.ConversionFactor : UoMManager.GetUoMFactor(cycleItem.CountedQtyUoM, cycleItem.PrimaryUM, Convert.ToInt32(cycleItem.ItemId.Trim()), cycleItem.ItemNumber.Trim());
                        cycleItem.CountedQuantity = Convert.ToInt32(cycleItem.CountedQuantity * conversionFactor);
                    }
                    if (requestStausCode.ToLower() != StatusTypesEnum.PHYSCL.ToString().ToLower())
                    {
                        var itemStatus = cycleItem.ItemStatus ? "1" : "0";
                        string query = "UPDATE BUSDTA.Cycle_Count_Detail SET CountedQty=" + cycleItem.CountedQuantity + ", ItemStatus = " + itemStatus + " , UpdatedBy=" + UserManager.UserId + ",UpdatedDatetime= getdate() " +
                                       "WHERE CycleCountID=" + cycleItem.CycleCountID + " AND CycleCountDetailID = " + cycleItem.CycleCountDetailID + " AND ReCountNumber=" + cycleItem.ReCountNumber + " " +
                                       "AND ItemId=" + cycleItem.ItemId;

                        result = DbEngine.ExecuteNonQuery(query);
                    }
                    else if (requestStausCode.ToLower() == StatusTypesEnum.PHYSCL.ToString().ToLower())
                    {

                        var itemStatus = cycleItem.ItemStatus ? "1" : "0";
                        var query = @"INSERT INTO BUSDTA.Cycle_Count_Detail (CycleCountID,CycleCountDetailID,ReCountNumber,ItemId,CountedQty,ItemStatus,OnHandQty,
                                      HeldQty,CountAccepted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) 
                            VALUES(" + cycleItem.TransactionID + ",isnull((select ( max(CycleCountDetailID) + 1) from BUSDTA.Cycle_Count_Detail),1),1," + cycleItem.ItemId + "," + cycleItem.CountedQuantity + "," +
                               " '" + itemStatus + "',0,0,'0'," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";
                        DbEngine.ExecuteNonQuery(query);
                    }
                }

                string statusIDQuery = "update busdta.Cycle_Count_Header set  IsCountInitiated = 1 ";//StatusId = " + statusCode + ",
                statusIDQuery = statusIDQuery + " where CycleCountID = '" + itemsList[0].CycleCountID + "'";

                int queryResult = DbEngine.ExecuteNonQuery(statusIDQuery);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][SaveOnHold " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:SaveOnHold]");
            return result;
        }

        public Dictionary<string, bool> GetPendingCycleCountStatus()
        {
            Dictionary<string, bool> statusDict = new Dictionary<string, bool>();
            try
            {
                var countPartialLock = 0;
                var countFullLock = 0;
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:GetPendingCycleCountStatus]");

                var query = "select c.*,(select StatusTypeCD FROM busdta.Status_Type where StatusTypeID = StatusID) as StatusTypeCD from busdta.Cycle_Count_Header c where (select StatusTypeCD FROM busdta.Status_Type where StatusTypeID = StatusID) in ('RELS', 'RECNT', 'SUBMIT')"; // and IsCountInitiated = 1";

                DataTable dt = DbEngine.ExecuteDataSet(query).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in dt.Rows)
                    {
                        var datePNDG = string.IsNullOrEmpty(dataRow["CycleCountDatetime"].ToString()) ? new DateTime() : Convert.ToDateTime(dataRow["CycleCountDatetime"]);
                        var isCountInitiated = string.IsNullOrEmpty(dataRow["IsCountInitiated"].ToString()) ? String.Empty : Convert.ToString(dataRow["IsCountInitiated"]);
                        var flgIsCountInitiated = isCountInitiated == "1" ? true : false;
                        var StatusTypeCD = string.IsNullOrEmpty(dataRow["StatusTypeCD"].ToString()) ? String.Empty : Convert.ToString(dataRow["StatusTypeCD"].ToString().Trim());

                        if (flgIsCountInitiated)
                            countFullLock++;
                        else
                            countPartialLock++;

                        if (datePNDG.Date < DateTime.Now.Date && StatusTypeCD.ToLower() == StatusTypesEnum.RELS.ToString().ToLower())
                        {
                            countFullLock++;
                        }
                        //else 

                    }
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:GetPendingCycleCountStatus]");

                statusDict.Add("PartialLock", countPartialLock > 0);
                statusDict.Add("FullLock", countFullLock > 0);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPendingCycleCountStatus " + ex.StackTrace + "]\n[Message " + ex.Message + "]");
            }
            return statusDict;
        }
        public void UpdateInitiationStatus(int requestId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:UpdateInitiationStatus]");

                #region Update CycleCount Request Status
                DbEngine.ExecuteNonQuery("update busdta.Cycle_Count_Header set IsCountInitiated = 1 where CycleCountID=" + requestId + "");
                #endregion
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][end:UpdateInitiationStatus]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][UpdateInitiationStatus " + ex.StackTrace + "]\n[Message " + ex.Message + "]");
            }
        }


        public bool UpdatePickDetailsOnReset(ObservableCollection<CycleCountItems> itemsToUpdate, string transactioID, string TransactionTypeID)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][Start:UpdatePickDetailsOnReset]");

                foreach (var cycleItem in itemsToUpdate)
                {
                    if (cycleItem.IsForCount)
                    {
                        string query = string.Empty;

                        query = @"Update [BUSDTA].[PICK_DETAIL] set 
                                [IsInException]='0',
                                [ExceptionReasonCodeId]='1',
                                [PickedQty]='0',
                                [PickQtyPrimaryUOM]='0' ,
                                [TransactionQty]='9999',
                                [TransactionQtyPrimaryUOM]='9999',
                                [ManuallyPickCount]='0',
                                [ReasonCodeId] = '0',
                                [LastScanMode]='0',
                                [PickAdjusted]='0'
                                where RouteID='" + CommonNavInfo.RouteID + "' and TransactionID='" + transactioID + "' and TransactionTypeID='" + TransactionTypeID + "' and ItemID='" + cycleItem.ItemId.Trim() + "'  and TransactionDetailID='0' ";
                        int cnt = DbEngine.ExecuteNonQuery(query);
                    }
                }
                Logger.Info("[SalesLogicExpress.Application.Managers][CycleCountManager][End:UpdatePickDetailsOnReset]");
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CycleCountManager][UpdatePickDetailsOnReset][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return false;
            }
        }
    }
}
