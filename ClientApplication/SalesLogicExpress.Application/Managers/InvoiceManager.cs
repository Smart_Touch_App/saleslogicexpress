﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class InvoiceManager
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.InvoiceManager");

        public int CreateInvoice(string InvoicePaymentType, string RouteId, string InvoiceNumber, string CustomerId, DateTime InvoiceDate, decimal InvoiceAmt, decimal OpenAmt, string OrderId)
        {
            int InvoiceId = 0;
            try
            {
                string strOpenStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='OPEN'");
                string strUnknownStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='UNKNOWN'");

                string IsInvoiceExists = DbEngine.ExecuteScalar("select TRIM(ISNULL(DeviceInvoiceNumber,'')) DeviceInvoiceNumber from BUSDTA.Invoice_Header where DeviceInvoiceNumber=" + InvoiceNumber);

                if (string.IsNullOrEmpty(IsInvoiceExists))
                {
                    InvoiceId = Convert.ToInt32(DbEngine.ExecuteScalar(
                          @" Insert into BUSDTA.Invoice_Header (InvoicePaymentType,DeviceStatusID,ARStatusID,OrderId,RouteId,DeviceInvoiceNumber,CustomerId,DeviceInvoiceDate,GrossAmt) "
                          + " values ('" + InvoicePaymentType + "'," + strOpenStatusId + "," + strUnknownStatusId + "," + OrderId + "," + RouteId + "," + InvoiceNumber + "," + CustomerId + ",'" + InvoiceDate.ToString("yyyy-MM-dd") + "'," + InvoiceAmt.ToString() + ")"
                          + " SELECT @@Identity"));
                    //MapInvoiceOrder(RouteId, InvoiceId.ToString(), CustomerId, OrderId, StatusId);
                    InsertInvoiceInCustomerLedger(RouteId, InvoiceId, CustomerId, InvoiceAmt);
                }

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][InvoiceManager][CreateInvoice][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return InvoiceId;
        }

        public void InsertInvoiceInCustomerLedger(string routeId, int InvoiceId, string customerId, decimal invoiceOpenAmt)
        {
            DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Customer_Ledger (InvoiceId,CustomerId,RouteId,InvoiceGrossAmt,InvoiceOpenAmt,IsActive) 
                                     values(" + InvoiceId.ToString() + "," + customerId + "," + routeId + "," + invoiceOpenAmt.ToString() + "," + invoiceOpenAmt.ToString() + ",'1')");
        }

        public ObservableCollection<PaymentARModel.InvoiceDetails> GetInvoices(string CustomerId)
        {
            ObservableCollection<PaymentARModel.InvoiceDetails> InvoiceDetailsList = new ObservableCollection<PaymentARModel.InvoiceDetails>();
            StringBuilder queryBuilder = new StringBuilder();
            try
            {
                string voidStatusId = DbEngine.ExecuteScalar("SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");

                queryBuilder.Append("select ih.invoiceid,TRIM(ISNULL(ih.DeviceInvoiceNumber,'')) InvoiceNumber,ih.DeviceInvoiceDate InvoiceDate,");
                queryBuilder.Append("\n(case lower(InvoicePaymentType) when 'return' then (-1)*ih.GrossAmt else ih.GrossAmt end) GrossAmount,");
                queryBuilder.Append("\n(case lower(InvoicePaymentType) when 'return' then (-1)*SUM(isnull(cl.InvoiceOpenAmt,0) + (SELECT (-1)*ISNULL(SUM(ISNULL(receiptappliedamt,0)),0) AS Amount FROM BUSDTA.Customer_Ledger ");
                queryBuilder.Append("\nWHERE ReceiptID=ih.OrderId AND ISNULL(InvoiceId,0)<>0 AND RouteId=ih.RouteId)) else sum(isnull(cl.InvoiceOpenAmt,0)) - sum(isnull(cl.ConcessionAmt,0)) - sum(isnull(cl.receiptappliedamt,0)) end)  OpenAmount,");
                queryBuilder.Append("\nDATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) AS Age ,InvoicePaymentType");
                queryBuilder.Append("\nfrom BUSDTA.Customer_Ledger cl join busdta.Invoice_Header ih on cl.InvoiceId=ih.InvoiceID");
                queryBuilder.Append("\nwhere IH.CustomerId=" + CustomerId + " and cl.isactive='1' and ih.DeviceStatusId<>"+(string.IsNullOrEmpty(voidStatusId)?"0":voidStatusId));
                queryBuilder.Append("\ngroup by ih.invoiceid,ih.DeviceInvoiceNumber,ih.DeviceInvoiceDate, ih.GrossAmt,InvoicePaymentType Order by ih.DeviceInvoiceDate desc ");
                DataSet result = DbEngine.ExecuteDataSet(queryBuilder.ToString());

                for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                {
                    PaymentARModel.InvoiceDetails obj = new PaymentARModel.InvoiceDetails();
                    obj.AgingInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["Age"].ToString());
                    obj.GrossAmountInvDtl = Convert.ToDecimal(result.Tables[0].Rows[i]["GrossAmount"].ToString());
                    obj.OpenAmountInvDtl = Convert.ToDecimal(result.Tables[0].Rows[i]["OpenAmount"].ToString());
                    obj.DateInvDtl = Convert.ToDateTime(result.Tables[0].Rows[i]["InvoiceDate"]).ToString("MM/dd/yyyy");
                    obj.InvoiceNoInvDtl = result.Tables[0].Rows[i]["InvoiceNumber"].ToString();
                    obj.InvoiceIdInvDtl = Convert.ToInt32(result.Tables[0].Rows[i]["invoiceid"].ToString());
                    obj.InvoicePaymentType = result.Tables[0].Rows[i]["InvoicePaymentType"].ToString();
                    InvoiceDetailsList.Add(obj);
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][InvoiceManager][GetOpenInvoices][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return InvoiceDetailsList;
        }




    }
}
