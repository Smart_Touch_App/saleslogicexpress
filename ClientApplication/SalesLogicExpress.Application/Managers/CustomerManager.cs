﻿using System;
using System.Linq;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.Helpers;
using System.Collections.Generic;
using log4net;
using System.Configuration;
namespace SalesLogicExpress.Application.Managers
{
    public class CustomerManager
    {
        public readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerManager");
        string baseDate = ConfigurationManager.AppSettings["BaseDate"].ToString();
        public ObservableCollection<Customer> GetCustomersForRoute(string routeID)
        {
            DateTime timer = DateTime.Now;
            DateTime timer2 = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetCustomersForRoute][routeID=" + routeID + "]");
            log.Info("[CustomerManager][Start:GetCustomersForRoute][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][GetCustomersForRoute][QueryStart:]\t" + DateTime.Now + "");
            ObservableCollection<Customer> customersCollection = new ObservableCollection<Customer>();

            try
            {
                #region Implementation
                string customerByRouteQuery = "";
                customerByRouteQuery = customerByRouteQuery + "SELECT AIZON,AISTOP as DeliveryCode,(SELECT DMDDCD from busdta.M56M0001 where DMDDC = cs.AISTOP) as DeliveryCodeDesc,a.aban8 AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type, ";
                customerByRouteQuery = customerByRouteQuery + "(SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = a.aban8 AND RPSTTP <> 'Moved' AND RPSTDT >= cast(getdate() as date) order by 1) as NextStop,";//" isnull(BUSDTA.getNextStopDate(A.ABAN8,GETDATE(),'" + baseDate + "',1),NULL) as NextStop,  ";
                customerByRouteQuery = customerByRouteQuery + "(SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = a.aban8 AND RPSTTP <> 'Moved' AND RPSTDT < cast(getdate() as date) order by 1 desc) as LastStop,"; //" BUSDTA.getLastStopDate(A.ABAN8,GETDATE(),'" + baseDate + "',1) as LastStop,  ";
                customerByRouteQuery = customerByRouteQuery + " a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , cs.AIEXHD as CreditHold,  ";
                customerByRouteQuery = customerByRouteQuery + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, BUSDTA.GetDefaultPhone(a.aban8) AS Phone# , ";
                customerByRouteQuery = customerByRouteQuery + "(rtrim(d.aladd1) + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip,AITRAR as TransMode,AICO Company ";
                customerByRouteQuery = customerByRouteQuery + " ,cs.aitrar as 'Payment Code',f.PNPTD  as 'Payment Desc'";
                customerByRouteQuery = customerByRouteQuery + "FROM busdta.f0101 AS a ";
                customerByRouteQuery = customerByRouteQuery + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
                customerByRouteQuery = customerByRouteQuery + "JOIN busdta.f0101 AS b ON a.aban81 =b.aban8 ";
                //customerByRouteQuery = customerByRouteQuery + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
                customerByRouteQuery = customerByRouteQuery + " join busdta.F0116 as d on a.aban8 =d.alan8  ";
                customerByRouteQuery = customerByRouteQuery + " LEFT OUTER JOIN busdta.F0014 f ON cs.aitrar =f.pnptc ";
                customerByRouteQuery = customerByRouteQuery + " LEFT OUTER JOIN BUSDTA.Customer_Route_Map crm on aian8 = crm.CustomerShipToNumber ";
                customerByRouteQuery = customerByRouteQuery + " where AIZON>' ' and AISTOP>' ' and cs.AISTOP <> '7' ";
                customerByRouteQuery = customerByRouteQuery + " and crm.IsActive = 'Y' and crm.RelationshipType = 'AN85' ";

                DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerByRouteQuery);
                log.Info("[CustomerManager][GetCustomersForRoute][QueryEnd:]" + (DateTime.Now - timer) + "");
                timer2 = DateTime.Now;

                log.Info("[CustomerManager][GetCustomersForRoute][DataInitializationStart:]" + DateTime.Now + "");
                if (queryResult.HasData())
                {
                    string[] deliveryCodes = new string[] { "11", "1A", "1E" };
                    Random randomCode = new Random();
                    Customer customer;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                        customer = new Customer();
                        customer.CustomerNo = customerDataRow["CustomerNo"].ToString();
                        customer.Phone = customerDataRow["Phone#"].ToString();
                        customer.Address = customerDataRow["Address"].ToString();
                        customer.City = customerDataRow["City"].ToString();
                        customer.Name = customerDataRow["Name"].ToString();
                        customer.Zip = customerDataRow["Zip"].ToString();
                        customer.State = customerDataRow["State"].ToString();
                        customer.Shop = customerDataRow["Bill_To_Name"].ToString();
                        //customer.SequenceNo = index;
                        if (customerDataRow["CreditHold"].ToString() == "y" || customerDataRow["CreditHold"].ToString() == "Y")
                        {
                            customer.CreditHold = true;
                        }
                        if (customerDataRow["CreditHold"].ToString() == "n" || customerDataRow["CreditHold"].ToString() == "N")
                        {
                            customer.CreditHold = false;
                        }
                        customer.DeliveryCode = customerDataRow["DeliveryCode"].ToString().Trim();
                        customer.DeliveryCodeDescription = customerDataRow["DeliveryCodeDesc"].ToString().Trim();
                        customer.Route = customerDataRow["Route"].ToString();
                        customer.RouteBranch = customerDataRow["Route_Branch"].ToString();
                        customer.BillType = customerDataRow["Billto_Type"].ToString();
                        customer.IsCustomerOnAccount = !string.IsNullOrEmpty(customerDataRow["TransMode"].ToString()) && customerDataRow["TransMode"].ToString().Trim() == "CSH" ? false : true;
                        customer.Company = customerDataRow["Company"].ToString();
                        if (!(customerDataRow["NextStop"].ToString() == null || customerDataRow["NextStop"].ToString() == ""))
                        {
                            DateTime next = Convert.ToDateTime(customerDataRow["NextStop"]);
                            customer.NextStop = next.ToString("MM'/'dd'/'yyyy ");
                        }
                        else
                        {
                            customer.NextStop = "None";
                        }
                        if (!(customerDataRow["LastStop"].ToString() == null || customerDataRow["LastStop"].ToString() == ""))
                        {
                            DateTime next = Convert.ToDateTime(customerDataRow["LastStop"]);
                            customer.PreviousStop = next.ToString("MM'/'dd'/'yyyy ");
                        }
                        else
                        {
                            customer.PreviousStop = "None";
                        }
                        customer.PaymentModeForList = customerDataRow["Payment Code"].ToString().Trim();
                        customer.PaymentModeDescriptionForList = customerDataRow["Payment Desc"].ToString().Trim();
                        customer.PaymentModeDescriptionForList = "(" + customer.PaymentModeForList + ") " + customer.PaymentModeDescriptionForList + "";
                        customer.StopCodeDesc = customerDataRow["DeliveryCodeDesc"].ToString();
                        customer.VisiteeType = "Cust";
                        customersCollection.Add(customer);
                    }
                    var orderedlist = customersCollection.OrderBy(x => x.CustomerNo);

                    ObservableCollection<Customer> OrderList = new ObservableCollection<Customer>(orderedlist);

                    //for (var stopNo = 1; stopNo <= OrderList.Count; stopNo++)
                    //{
                    //    OrderList[stopNo - 1].SequenceNo = stopNo;
                    //}
                    customersCollection = OrderList;
                    customersCollection.ElementAt(0).SaleStatus = "SALE";
                    customersCollection.ElementAt(0).SaleStatusReason = "SALE";
                    customersCollection.ElementAt(1).SaleStatus = "NOSALE";
                    customersCollection.ElementAt(1).SaleStatusReason = "NO SALE - CREDIT HOLD";
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetCustomersForRoute][routeID=" + routeID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            log.Info("[CustomerManager][GetCustomersForRoute][DataInitializationEnd:]\t" + (DateTime.Now - timer2) + "");
            log.Info("[CustomerManager][GetCustomersForRoute][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetCustomersForRoute][routeID=" + routeID + "]");
            return customersCollection;
        }
        public ObservableCollection<CustomerContact> GetCustomerContact(string customerID)
        {
            try
            {

                log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetCustomerContact][customerID=" + customerID + "]");

                string customerContactQuery = "select ABAN8 'Customer#','' as Title,'' as ContactName, ABALPH 'Customer Name',WPPHTP 'Phone Type' ";
                customerContactQuery = customerContactQuery + " ,WPPH1 'Phone#', EAEMAL 'Email' ";
                customerContactQuery = customerContactQuery + " from BUSDTA.F0101, BUSDTA.F0115, BUSDTA.F01151 ";
                customerContactQuery = customerContactQuery + " where WPAN8 = ABAN8 and WPAN8 = EAAN8 and WPAN8 in (select ABAN8 from  ";
                customerContactQuery = customerContactQuery + " BUSDTA.F0101 where ABAN8 like '%" + customerID + "%')";
                DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerContactQuery);

                ObservableCollection<CustomerContact> customerContactCollection = new ObservableCollection<CustomerContact>();
                if (queryResult.HasData())
                {
                    Random rd = new Random();
                    string[] contactNames = new string[] { "David Pinto", "Willam Johnsons", "Jacob Smith", "Logan White", "Jayden Lee" };
                    string[] contactTitles = new string[] { "Manager", "Stores", "Front Office" };
                    CustomerContact customerContact = null;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                        //customerContact = new CustomerContact
                        //{
                        //    Name = contactNames[rd.Next(0, contactNames.Length - 1)],
                        //    Title = contactTitles[rd.Next(0, contactTitles.Length - 1)],
                        //    Phone = customerDataRow["Phone#"].ToString(),
                        //    PhoneType = customerDataRow["Phone Type"].ToString(),
                        //    Email = customerDataRow["Email"].ToString(),
                        //    //         EmailType = customerDataRow]["Email Type"].ToString()
                        //};
                        //customerDataRow["ContactName"] = customerContact.Name;
                        //customerDataRow["Title"] = customerContact.Title;
                        //customerContactCollection.Add(customerContact);
                    }
                }
                log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetCustomerContact][customerID=" + customerID + "]");

                return customerContactCollection;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetCustomerContact][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
        }
        public void AddNewContactDetails()
        {
            string AddContactQuery = "INSERT INTO BUSDTA.M0111(CDAN8,CDIDLN,CDRCK7,CDCNLN,CDPHTP,CDAR1,CDPH1,CDEXTN1,CDDFLTPH1,";
            AddContactQuery = AddContactQuery + "CDAR2,CDPH2,CDEXTN2,CDDFLTPH2,CDAR3,CDPH3,CDEXTN3,CDDFLTPH3,CDAR4,CDPH4,CDEXTN4,";
            AddContactQuery = AddContactQuery + "CDDFLTPH4,CDETP,CDEMAL1,CDDFLTEM1,CDEMAL2,CDDFLTEM2,CDEMAL3,CDDFLTEM3,CDFNAME,";
            AddContactQuery = AddContactQuery + "CDLNAME,CDTITL,CDACTV,CDDFLT,CDREFLN)";
            AddContactQuery = AddContactQuery + "values(1108911,0,2,0,NULL,,4057212466,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,";
            AddContactQuery = AddContactQuery + "NULL,NULL,NULL,NULL,NULL,NULL,,NULL,NULL,NULL,NULL,NULL,'Johnny','Dre',NULL,NULL,NULL,NULL)";
        }
        public CustomerInformation GetCustomerInformation(string customerID)
        {
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetCustomerInformation]");
            log.Info("[CustomerManager][Start:GetCustomerInformation][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][GetCustomerInformation][QueryStart:]\t" + DateTime.Now + "");
            CustomerInformation customerInformation = new CustomerInformation();
            try
            {
                #region  Implementation
                string customerByRouteQuery =
                            @"
                                            SELECT
                                            a.aban8 as 'CustomerId',
                                            a.abalph as 'CustomerName',
                                            rtrim(d.aladd1) as 'AddressLine1',
                                            rtrim(d.aladd2) as 'AddressLine2',
                                            rtrim(d.aladd3) as 'AddressLine3',
                                            rtrim(d.aladd4) as 'AddressLine4',
                                            rtrim(d. alcty1) as 'City',
                                            rtrim(d.aladds) as 'State',
                                            rtrim(d. aladdz) as 'Zip',
                                            '' as 'MailingName',
                                            a.ABTXCT as 'TaxExemptCertificate',
                                            a.ABAT1 as 'SearchType', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = 'ST' and ltrim(DRKY) = a.ABAT1) as 'SearchTypeDesc',
                                            o.MAPA8 as 'Parent', busdta.GetABADescriptionFromABAId(o.MAPA8) as 'ParentDesc',
                                            a.aban81 as 'BillTo', busdta.GetABADescriptionFromABAId(a.aban81) as 'BillToDesc',
                                            a.ABAN85 as 'RemitTo', busdta.GetABADescriptionFromABAId(a.ABAN85) as 'RemitToDesc',
                                            c.AIAC12 as 'KAMNAMStreet', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '12' and ltrim(DRKY) = c.AIAC12) as 'KAMNAMStreetDesc',
                                            c.AIAC14 as 'KindOfBizAcct', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '14' and ltrim(DRKY) = c.AIAC14) as 'KindOfBizAcctDesc',
                                            c.AIFRTH as 'FreightHandlingCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '42' and DRRT = 'FR' and ltrim(DRKY) = c.AIAC14) as 'FreightHandlingDesc',
                                            c.AIAC02 as 'OperatingUnitCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '02' and ltrim(DRKY) = c.AIAC02) as 'OperatingUnitDesc',
                                            c.AIAC06 as 'RegionCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '06' and ltrim(DRKY) = c.AIAC06) as 'RegionDesc',
                                            c.AIAC05 as 'DistrictCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '05' and ltrim(DRKY) = c.AIAC05) as 'DistrictDesc',
                                            c.AIAC04 as 'BranchCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '04' and ltrim(DRKY) = c.AIAC04) as 'BranchDesc',
                                            c.AIAC08 as 'ChainCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '08' and ltrim(DRKY) = c.AIAC08) as 'ChainDesc',
                                            c.AIAC03 as 'RouteCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '03' and ltrim(DRKY) = c.AIAC03) as 'RouteDesc',
                                            c.AISTOP as 'StopCodeCd', (select DRDL01 from BUSDTA.F0005 where  DRSY = '42' and DRRT = 'SP' and ltrim(DRKY) = c.AISTOP) as 'StopCodeDesc'
                                            FROM busdta.F0101 a left join busdta.F0116 d ON a.aban8 =d.alan8
                                            left join busdta.F0150 o ON a.ABAN8 = o.MAAN8
                                            left  join busdta.F03012 c ON a.ABAN8 = c.AIAN8
                                            WHERE a.aban8 ='" + customerID + "'";

                DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerByRouteQuery);
                customerInformation.CustomerNo = customerID;

                if (queryResult.HasData())
                {
                    string[] deliveryCodes = new string[] { "11", "1A", "1E" };
                    Random randomCode = new Random();
                    customerInformation = new CustomerInformation
                    {
                        CustomerNo = queryResult.Tables[0].Rows[0]["CustomerId"].ToString().ToUpper(),
                        Address1 = queryResult.Tables[0].Rows[0]["AddressLine1"].ToString().ToUpper(),
                        Address2 = queryResult.Tables[0].Rows[0]["AddressLine2"].ToString().ToUpper(),
                        Address3 = queryResult.Tables[0].Rows[0]["AddressLine3"].ToString().ToUpper(),
                        Address4 = queryResult.Tables[0].Rows[0]["AddressLine4"].ToString().ToUpper(),
                        CustInfoCity = queryResult.Tables[0].Rows[0]["City"].ToString().ToUpper(),
                        CustInfoState = queryResult.Tables[0].Rows[0]["State"].ToString().ToUpper(),
                        CustInfoZip = queryResult.Tables[0].Rows[0]["Zip"].ToString().ToUpper(),
                        MailingName = queryResult.Tables[0].Rows[0]["MailingName"].ToString().ToUpper(),
                        TaxExemptCertificate = queryResult.Tables[0].Rows[0]["TaxExemptCertificate"].ToString().ToUpper(),
                        SearchTypeCode = queryResult.Tables[0].Rows[0]["SearchType"].ToString().ToUpper(),
                        SearchTypeDesc = queryResult.Tables[0].Rows[0]["SearchTypeDesc"].ToString().ToUpper(),
                        ParentCode = queryResult.Tables[0].Rows[0]["Parent"].ToString().ToUpper(),
                        ParentDesc = queryResult.Tables[0].Rows[0]["ParentDesc"].ToString().ToUpper(),
                        BillToCode = queryResult.Tables[0].Rows[0]["BillTo"].ToString().ToUpper(),
                        BillToDesc = queryResult.Tables[0].Rows[0]["BillToDesc"].ToString().ToUpper(),
                        RemitToCode = queryResult.Tables[0].Rows[0]["RemitTo"].ToString().ToUpper(),
                        RemitToDesc = queryResult.Tables[0].Rows[0]["RemitToDesc"].ToString().ToUpper(),
                        KAMNAMStreetCode = queryResult.Tables[0].Rows[0]["KAMNAMStreet"].ToString().ToUpper(),
                        KAMNAMStreetDesc = queryResult.Tables[0].Rows[0]["KAMNAMStreetDesc"].ToString().ToUpper(),
                        KindOfBusinessAccountCode = queryResult.Tables[0].Rows[0]["KindOfBizAcct"].ToString().ToUpper(),
                        KindOfBusinessAccountDesc = queryResult.Tables[0].Rows[0]["KindOfBizAcctDesc"].ToString().ToUpper(),
                        FreightHandlingCode = queryResult.Tables[0].Rows[0]["FreightHandlingCd"].ToString().ToUpper(),
                        FreightHandlingCodeDesc = queryResult.Tables[0].Rows[0]["FreightHandlingDesc"].ToString().ToUpper(),
                        DistrictCode = queryResult.Tables[0].Rows[0]["DistrictCd"].ToString().ToUpper(),
                        DistrictDesc = queryResult.Tables[0].Rows[0]["DistrictDesc"].ToString().ToUpper(),
                        BranchCode = queryResult.Tables[0].Rows[0]["BranchCd"].ToString().ToUpper(),
                        BranchDesc = queryResult.Tables[0].Rows[0]["BranchDesc"].ToString().ToUpper(),
                        ChainCode = queryResult.Tables[0].Rows[0]["ChainCd"].ToString().ToUpper(),
                        ChainDesc = queryResult.Tables[0].Rows[0]["ChainDesc"].ToString().ToUpper(),
                        RouteCode = queryResult.Tables[0].Rows[0]["RouteCd"].ToString().ToUpper(),
                        RouteDesc = queryResult.Tables[0].Rows[0]["RouteDesc"].ToString().ToUpper(),
                        StopCode = queryResult.Tables[0].Rows[0]["StopCodeCd"].ToString().ToUpper(),
                        StopCodeDesc = queryResult.Tables[0].Rows[0]["StopCodeDesc"].ToString().ToUpper()
                    };
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetCustomerInformation][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
            log.Info("[CustomerManager][GetCustomerInformation][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetCustomerInformation]");
            return customerInformation;

        }
        public List<string> GetStateCodes()
        {
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetStateCodes]");
            log.Info("[CustomerManager][Start:GetStateCodes][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][GetStateCodes][QueryStart:]\t" + DateTime.Now + "");
            List<string> stateCodeList = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("select distinct(RTRIM(ad.ALADDS)) cd from busdta.f0116 ad WHERE ad.ALADDS !=''");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        stateCodeList.Add(dataRow["cd"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStateCodes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            log.Info("[CustomerManager][GetStateCodes][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetStateCodes]");
            return stateCodeList;
        }


        public List<string> GetAccSegmentType()
        {
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetStateCodes]");
            log.Info("[CustomerManager][Start:GetStateCodes][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][GetStateCodes][QueryStart:]\t" + DateTime.Now + "");
            List<string> AccSegmentTypeList = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("select distinct(RTRIM(ad.ALADDS)) cd from busdta.f0116 ad WHERE ad.ALADDS !=''");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        AccSegmentTypeList.Add(dataRow["cd"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStateCodes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            log.Info("[CustomerManager][GetStateCodes][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetStateCodes]");
            return AccSegmentTypeList;
        }
        public DateTime GetPreviousStop(string customerID)
        {
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetPreviousStop]");
            log.Info("[CustomerManager][Start:GetPreviousStop][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][GetPreviousStop][QueryStart:]\t" + DateTime.Now + "");
            DateTime previousStop = DateTime.Now;
            try
            {
                #region Implementation
                DateTime now = DateTime.Now;
                string query = "";

                query = "SELECT RPSTDT as lastdate FROM BUSDTA.M56M0004 where RPAN8 =" + customerID + " and RPSTDT < cast('" + now.ToString("yyyyMMdd") + "' as date) order by RPSTDT desc; set rowcount 0;";

                DataSet lastStop = DbEngine.ExecuteDataSet(query);
                if (lastStop.HasData())
                {
                    foreach (DataRow lastST in lastStop.Tables[0].Rows)
                    {
                        if (lastST["lastdate"].ToString() == null || lastST["lastdate"].ToString() == "")
                        {
                            previousStop = DateTime.Now;
                        }
                        else
                        {
                            previousStop = Convert.ToDateTime(lastST["lastdate"].ToString());
                        }
                    }

                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetPreviousStop][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            log.Info("[CustomerManager][GetPreviousStop][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][GetPreviousStop][End:GetStopDateDescription]");
            return previousStop;
        }
        public string GetStopDateDescription(string stopCode)
        {
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetStopDateDescription]");
            log.Info("[CustomerManager][Start:GetStopDateDescription][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][GetStopDateDescription][QueryStart:]\t" + DateTime.Now + "");
            string stopCodeDescription = "";
            try
            {
                #region Implementation
                string query = "select DRDL01 from BUSDTA.F0005 where  DRSY = '42' and DRRT = 'SP' and ltrim(DRKY) = '" + stopCode + "'";
                string result = DbEngine.ExecuteScalar(query);
                if (!string.IsNullOrEmpty(result))
                {
                    stopCodeDescription = result.Trim();
                }
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStopDateDescription][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            log.Info("[CustomerManager][GetStopDateDescription][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetStopDateDescription]");
            return stopCodeDescription;
        }
        public int SaveCustomerInformation(CustomerInformation customerInformation)
        {
            int result = -1;
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:SaveCustomerInformation]");
            log.Info("[CustomerManager][Start:SaveCustomerInformation][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][SaveCustomerInformation][QueryStart:]\t" + DateTime.Now + "");
            
            try
            {
                #region Implementation
                //
                int ID = 1;
                string Value = "select ISNULL(max(TransID),0) from [BUSDTA].[F0116_Audit]";
                ID = Convert.ToInt32(DbEngine.ExecuteScalar(Value)) + 1;

                //Insert Customer Info into F0116 Audit Table//
                string AuditInsert = "INSERT INTO [BUSDTA].[F0116_Audit](TransID,TransType,ALAN8,ALEFTB,ALEFTF,ALADD1,ALADD2,";
                AuditInsert = AuditInsert + " ALADD3,ALADD4,ALADDZ,ALCTY1,ALCOUN,ALADDS,";
                AuditInsert = AuditInsert + " WSValidation,Status,RejReason,CreatedDatetime)";
                AuditInsert = AuditInsert + " values (" + ID + ",'AddressbookTransaction','" + customerInformation.CustomerNo + "',0,NULL,'" + customerInformation.Address1 + "','" + customerInformation.Address2 + "','" + customerInformation.Address3 + "',";
                AuditInsert = AuditInsert + " '" + customerInformation.Address4 + "','" + customerInformation.CustInfoZip + "','" + customerInformation.CustInfoCity + "','" + customerInformation.CustInfoState + "',";
                AuditInsert = AuditInsert + " '" + customerInformation.State + "','No',NULL,NULL,GETDATE())";
                DbEngine.ExecuteNonQuery(AuditInsert);

                //Insert Customer Info into F0101 Audit Table//
                string AuditInsert1 = "INSERT INTO [BUSDTA].[F0101_Audit](TransID,TransType,ABAN8,ABALKY,ABTAX,ABALPH,ABMCU,ABSIC,ABLNGP,ABAT1,";
                AuditInsert1 = AuditInsert1 + " ABCM,ABTAXC,ABAT2,ABAN81,ABAN82,ABAN83,ABAN84,ABAN86,ABAN85,ABAC01,ABAC02,ABAC03,";
                AuditInsert1 = AuditInsert1 + " ABAC04,ABAC05,ABAC06,ABAC07,ABAC08,ABAC09,ABAC10,ABAC11,ABAC12,ABAC13,ABAC14,ABAC15,";
                AuditInsert1 = AuditInsert1 + " ABAC16,ABAC17,ABAC18,ABAC19,ABAC20,ABAC21,ABAC22,ABAC23,ABAC24,ABAC25,ABAC26,ABAC27,ABAC28,ABAC29,ABAC30,";
                AuditInsert1 = AuditInsert1 + " ABRMK,ABTXCT,ABTX2,ABALP1,WSValidation,Status,RejReason,CreatedDatetime)";
                AuditInsert1 = AuditInsert1 + " values (" + ID + ",'AddressbookTransaction','" + customerInformation.CustomerNo + "','','','','','','','', ";
                AuditInsert1 = AuditInsert1 + " '','','','','','','','','','','','', ";
                AuditInsert1 = AuditInsert1 + " '','','','','','','','','','','','', ";
                AuditInsert1 = AuditInsert1 + " '','','','','','','','','','','','','','','',";
                AuditInsert1 = AuditInsert1 + " '','" + customerInformation.TaxExemptCertificate.Trim().SqlStringEscaping() + "','','','No',NULL,NULL,GETDATE())";
                DbEngine.ExecuteNonQuery(AuditInsert1);
                //
                string updateQuery =
                                   @"update  busdta.F0116 set 
                                     aladd1=?," +
                                   " aladd2=?," +
                                   " aladd3=?," +
                                   " aladd4=?," +
                                   " alcty1=?," +
                                   " aladds=?," +
                                   " aladdz='" + customerInformation.CustInfoZip + "'" +
                                   " where alan8='" + customerInformation.CustomerNo + "'";
                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(updateQuery);
                iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.ParameterName = "Address1";
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = customerInformation.Address1;
                command.Parameters.Add(parm);

                parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.ParameterName = "Address2";
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = customerInformation.Address2;
                command.Parameters.Add(parm);

                parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.ParameterName = "Address3";
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = customerInformation.Address3;
                command.Parameters.Add(parm);

                parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.ParameterName = "Address4";
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = customerInformation.Address4;
                command.Parameters.Add(parm);

                parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.ParameterName = "City";
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = customerInformation.CustInfoCity;
                command.Parameters.Add(parm);

                parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.ParameterName = "State";
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = customerInformation.CustInfoState;
                command.Parameters.Add(parm);

                UpdateTaxExemptCertificate(customerInformation);

                result = new DB_CRUD().InsertUpdateData(command);
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][SaveCustomerInformation][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
            log.Info("[CustomerManager][SaveCustomerInformation][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:SaveCustomerInformation]");
            return result;

        }
        public int UpdateTaxExemptCertificate(CustomerInformation customerInformation)
        {
            try
            {
                string updateQuery =
                                 @"update  busdta.F0101 set 
                                  ABTXCT='" + customerInformation.TaxExemptCertificate.Trim().SqlStringEscaping() + "'" +
                                 " where aban8='" + customerInformation.CustomerNo + "'";

                return DbEngine.ExecuteNonQuery(updateQuery);
            }
            catch (Exception ex)
            {
                log.Error("Error in UpdateTaxExemptCertificate CustomerNo = " + customerInformation.CustomerNo + " Error = " + ex.Message);
                throw ex;
            }

        }

        public bool CloseActivity(string stopID)
        {
            bool flag = false;
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:CloseActivityCount][StopID= " + stopID + "]");
            log.Info("[CustomerManager][Start:CloseActivityCount][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][CloseActivityCount][QueryStart:]\t" + DateTime.Now + "");
            #region Implementation
            try
            {
                string query = string.Empty;                   
                query = "UPDATE BUSDTA.M56M0004 SET RPPACT=1, RPCACT =0 WHERE RPSTID ='" + stopID + "'";
                int i = DbEngine.ExecuteNonQuery(query);
                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][UpdateActivityCount][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            #endregion
            log.Info("[CustomerManager][ClosectivityCount][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:CloseActivityCount][StopID= " + stopID + "]");
            return flag;
        }
        public bool UpdateActivityCount(string stopID, bool isPendingAct, bool isIncrement)
        {
            bool flag = false;
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:UpdateActivityCount][StopID= " + stopID + "]");
            log.Info("[CustomerManager][Start:UpdateActivityCount][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][UpdateActivityCount][QueryStart:]\t" + DateTime.Now + "");
            #region Implementation
            try
            {
                string query = string.Empty;
                if (isPendingAct)
                {
                    if (isIncrement)
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPPACT=RPPACT+1  WHERE RPSTID ='" + stopID + "'";
                    }
                    else
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPPACT=RPPACT-1  WHERE RPSTID ='" + stopID + "' and RPPACT!=0";
                    }
                }
                else
                {
                    if (isIncrement)
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPCACT=RPCACT+1  WHERE RPSTID ='" + stopID + "' ";
                    }
                    else
                    {
                        query = "UPDATE BUSDTA.M56M0004 SET RPCACT=RPCACT-1  WHERE RPSTID ='" + stopID + "' and RPCACT !=0";
                    }
                }
                int i = DbEngine.ExecuteNonQuery(query);
                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][UpdateActivityCount][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            #endregion
            log.Info("[CustomerManager][UpdateActivityCount][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:UpdateActivityCount][StopID= " + stopID + "]");
            return flag;
        }
        public int GetPendingAtivityForStop( string stopId)
        {
            int count = 0;
            DateTime timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetPendingAtivityForStop][StopID= "+stopId+"]");
            log.Info("[CustomerManager][Start:GetPendingAtivityForStop][FunctionStartTime:]\t" + DateTime.Now + "");
            log.Info("[CustomerManager][GetPendingAtivityForStop][QueryStart:]\t" + DateTime.Now + "");
            try
            {
                #region Implementation
                string query = "SELECT RPPACT FROM BUSDTA.M56M0004 WHERE RPSTID='" + stopId + "'";
                count = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetPendingAtivityForStop][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            log.Info("[CustomerManager][GetPendingAtivityForStop][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetPendingAtivityForStop][StopID= " + stopId + "]");
            return count;
        }
        public void UpdateSequenceNo(string stopID, string stopDate)
        {
            DateTime timer = DateTime.Now;
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:UpdateSequenceNo]");
                log.Info("[CustomerManager][Start:UpdateSequenceNo][FunctionStartTime:]\t" + DateTime.Now + "");
                log.Info("[CustomerManager][UpdateSequenceNo][QueryStart:]\t" + DateTime.Now + "");
                #region Implementation
                //Check for activity for date
                string queryCustForDate = "SELECT RPSN as 'SeuenceNo', RPSTID as 'StopID' ,RPAN8 as 'CustNo', RPCACT as 'CompletedAct' ,RPPACT  as 'PendingAct' " +
                                            " , RPSTTP as 'StopType' FROM BUSDTA.M56M0004 WHERE RPSTDT='" + stopDate + "' ORDER BY RPSN ";
                DataSet dsResult = DbEngine.ExecuteDataSet(queryCustForDate);
                DataTable dtAllCust = dsResult.HasData() ? dsResult.Tables[0] : new DataTable();

                List<SequenceClass> custListForSeq = new List<SequenceClass>();
                if (dtAllCust.Rows.Count > 0)
                {
                    custListForSeq = dtAllCust.GetEntityList<SequenceClass>();
                    
                    int sequenceNoForCust = 0;
                    bool isAnyActivityPerformedBefore = custListForSeq.Where(x => x.PendingAct > 0 || x.CompletedAct > 0).Count() > 0 ? true : false;
                    if (!isAnyActivityPerformedBefore)
                    {
                        //Remove input stopID from List
                        SequenceClass inputCust = custListForSeq.FirstOrDefault(x => x.StopID.ToString().Trim() == stopID);
                        custListForSeq.Remove(inputCust);

                        for (int i = 0; i < custListForSeq.Count; i++)
                        {
                            string tempStopType = custListForSeq[i].StopType.Trim().ToLower();
                            if (tempStopType == "rescheduled" || tempStopType == "unplanned")
                            {
                                sequenceNoForCust = i;
                                continue;
                            }
                            else
                            {
                                sequenceNoForCust = i;
                                break;
                            }
                        }
                        custListForSeq.Insert(sequenceNoForCust, inputCust);
                    }
                    else
                    {
                        // Check wether stop ID has activity, if it has ,then dont update sequence no
                        bool isServiced = custListForSeq.Where(x => x.StopID.ToString().Trim() == stopID && (x.CompletedAct > 0 || x.PendingAct > 0)).Count() > 0 ? true : false;
                        if (!isServiced)
                        {
                            //Remove input stopID from List 
                            SequenceClass inputCust = custListForSeq.FirstOrDefault(x => x.StopID.ToString().Trim() == stopID);
                            custListForSeq.Remove(inputCust);
                            SequenceClass lastActCust = custListForSeq.OrderByDescending(x => x.SeuenceNo).FirstOrDefault(x => x.PendingAct > 0 || x.CompletedAct > 0);
                            int dropIndex = custListForSeq.IndexOf(lastActCust) + 1;
                            custListForSeq.Insert(dropIndex, inputCust);
                        }
                    }
                    // Now update sequence no in DB as per custListForSeq 
                    for (int i = 1; i <= custListForSeq.Count; i++)
                    {
                        string queryToUpdateSeqNo = "UPDATE BUSDTA.M56M0004 SET RPSN='" + i + "' WHERE RPSTID='" + custListForSeq[i - 1].StopID.ToString().Trim() + "'";
                        DbEngine.ExecuteNonQuery(queryToUpdateSeqNo);
                    }
                }
                #endregion
                log.Info("[CustomerManager][UpdateSequenceNo][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
                log.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:UpdateSequenceNo]");
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CustomerManager][UpdateSequenceNo][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }
        public class SequenceClass
        {
            private int seuenceNo;
            private int stopID;
            private string custNo;
            private int pendingAct;
            private int completedAct;
            private string stopType;

            public string StopType
            {
                get
                {
                    return this.stopType;
                }
                set
                {
                    this.stopType = value;
                }
            }

            public int CompletedAct
            {
                get
                {
                    return this.completedAct;
                }
                set
                {
                    this.completedAct = value;
                }
            }

            public int PendingAct
            {
                get
                {
                    return this.pendingAct;
                }
                set
                {
                    this.pendingAct = value;
                }
            }

            public string CustNo
            {
                get
                {
                    return this.custNo;
                }
                set
                {
                    this.custNo = value;
                }
            }

            public int StopID
            {
                get
                {
                    return this.stopID;
                }
                set
                {
                    this.stopID = value;
                }
            }

            public int SeuenceNo
            {
                get
                {
                    return this.seuenceNo;
                }
                set
                {
                    this.seuenceNo = value;
                }
            }
        }
    }
}
