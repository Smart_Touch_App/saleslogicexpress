﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Managers;
namespace SalesLogicExpress.Application.Managers
{
    class WebServiceRequestAndResponse
    {
        public DoHealthCheck DoHealthCheck;
        public DoHealthCheckResult DoHealthCheckResult;
        public bool IsServerReachable = true;

        public DoHealthCheckResult MDoHealthCheck(Rq pRq)
        {
            DoHealthCheckResult = new DoHealthCheckResult();
            DoHealthCheckResult.Status.Code = 0;
            DoHealthCheckResult.Status.Description = "Success";
            DoHealthCheckResult.ReadyToSync = true;
            DoHealthCheckResult.ServerStats.ServerInfo.CpuUtilization = 6;
            DoHealthCheckResult.ServerStats.ServerInfo.NumOfDBConnections = 4;
            DoHealthCheckResult.ServerStats.ServerInfo.NumOfServerConnections = 64;
            DoHealthCheckResult.ServerStats.ServerInfo.Server = "Sql Server";
            DoHealthCheckResult.DeltaChangeDetails.DeltaInfo = new List<DeltaInfo>();
            var deltaInfo = new DeltaInfo
            {
                ApplicationGroup = "UserDefineCodes",
                DeltaRows = 8,
                IsMandatorySync = false
            };
            DoHealthCheckResult.DeltaChangeDetails.DeltaInfo.Add(deltaInfo);
            deltaInfo = new DeltaInfo
            {
                ApplicationGroup = "Customer",
                DeltaRows = 6,
                IsMandatorySync = false
            };
            DoHealthCheckResult.DeltaChangeDetails.DeltaInfo.Add(deltaInfo);
            deltaInfo = new DeltaInfo
            {
                ApplicationGroup = "Pricing",
                DeltaRows = 5,
                IsMandatorySync = true
            };
            DoHealthCheckResult.DeltaChangeDetails.DeltaInfo.Add(deltaInfo);
            deltaInfo = new DeltaInfo
            {
                ApplicationGroup = "Product",
                DeltaRows = 2,
                IsMandatorySync = false
            };
            DoHealthCheckResult.DeltaChangeDetails.DeltaInfo.Add(deltaInfo);
            DoHealthCheckResult.didSchemaChange = false;
            DoHealthCheckResult.NumSqlQueryQueue = 1;
            DoHealthCheckResult.IsMandatorySync = true;
            return DoHealthCheckResult;

        }
    }

    #region Request
    public class DoHealthCheck
    {
        public Rq RqObj;
    }

    public class SyncSubscription
    {
        public DateTime LastDownload;
        public DateTime LastUpload;
        public PublicationName PubName;
        public enum PublicationName
        {
            pub_validate_user = 0,
            pub_everything,
            pub_immediate
        }

    }


    public class RemoteSubscription
    {
        public List<SyncSubscription> SyncSubscriptionList;

        public RemoteSubscription()
        {
            SyncSubscriptionList = new List<SyncSubscription>();
            for (var i = 0; i <= 2; i++)
            {
                SyncSubscription ss = new SyncSubscription();
                ss.LastDownload = new DateTime(2015, 05, 25);
                ss.LastUpload = new DateTime(2015, 05, 25);
                SyncSubscriptionList.Add(ss);
            }
            SyncSubscriptionList[0].PubName = SyncSubscription.PublicationName.pub_everything;
            SyncSubscriptionList[1].PubName = SyncSubscription.PublicationName.pub_immediate;
            SyncSubscriptionList[2].PubName = SyncSubscription.PublicationName.pub_validate_user;

        }
    }

    public class DeviceDeatil
    {
        public string DeviceID;
        public string DeviceName;
        public string ScriptVersion;
        public int Latitude;
        public int Longitude;
        public DateTime LastSchemaUpdate;
        public RemoteSubscription RtSubscription = new RemoteSubscription();
    }

    public class Rq
    {
        public bool IsReadyToSync;
        public bool ShowSchemaChanges;
        public bool ShowDeltaChanges;
        public bool ShowSqlServerStatus;
        public bool ShowConcurrentDevices;
        public bool ShowSqlServerQueue;
        public DeviceDeatil DeviceDetail = new DeviceDeatil();
    }
    #endregion

    #region Response

    public class DeltaInfo
    {
        public string ApplicationGroup;
        public int DeltaRows;
        public bool IsMandatorySync;
    }

    public class DeltaChangeDetails
    {
        public List<DeltaInfo> DeltaInfo;
    }

    public class ServerInfo
    {
        public int CpuUtilization;
        public int NumOfDBConnections;
        public int NumOfServerConnections;
        public string Server;
    }

    public class ServerStats
    {
        public ServerInfo ServerInfo = new ServerInfo();
    }

    public class Status
    {
        public int Code;
        public string Description;
    }

    public class DoHealthCheckResult
    {
        public Status Status = new Status();
        public bool ReadyToSync;
        public ServerStats ServerStats = new ServerStats();
        public DeltaChangeDetails DeltaChangeDetails = new DeltaChangeDetails();
        public bool didSchemaChange;
        public object SchemaChangeDetails;
        public int NumSqlQueryQueue;
        public bool IsMandatorySync;
    }


    #endregion
}



