﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class CustomerNotesManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerNotesManager");

        public ObservableCollection<CustomerNote> GetCustomerNotes(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerNotesManager][Start:GetCustomerNotes][CustomerID = " + customerID + "]");
            ObservableCollection<CustomerNote> customerNoteList = new ObservableCollection<CustomerNote>();
            try
            {
                string query = "select NDAN8 as 'CustomerId',NDID as 'NoteDetailId', ";
                query = query + " NDDTTM as 'NotesDate', NDDTLS as 'NoteDetail',";
                query = query + " NDTYP  as 'NoteType', NDDFLT as 'NoteDefault', NDCRBY as 'CreatedBy',";
                query = query + " NDCRDT as 'CreatedDate',NDUPBY as 'UpdatedBy', NDUPDT as 'UpdatedDate'";
                query = query + " from BUSDTA.M0112  where NDAN8 ='" + customerID + "'";
                query = query + " order by NDDTTM desc;";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                CustomerNote note;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        note = new CustomerNote();
                        note.NoteID = row["NoteDetailId"].ToString().Trim();
                        note.NoteDetails = row["NoteDetail"].ToString().Trim();
                        note.IsDefault = string.IsNullOrEmpty(row["NoteDefault"].ToString()) ? false : Convert.ToBoolean(row["NoteDefault"].ToString());
                        note.CustomerID = row["CustomerId"].ToString().Trim();
                        DateTime dt = Convert.ToDateTime(row["NotesDate"]);
                        note.DateTime = String.Format("{0:MM/dd/yyyy hh:mmtt}", dt).Replace('-', '/').ToLower();
                        //note.IsDefault = !string.IsNullOrEmpty(row["NDDFLT"].ToString()) ? Convert.ToBoolean(row["NDDFLT"].ToString()) : false;
                        customerNoteList.Add(note);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerNotesManager][GetCustomerNotes][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerNotesManager][End:GetCustomerNotes][customerID=" + customerID + "]");
            return customerNoteList;
        }

        public int AddNewCustomerNote(CustomerNote note)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerNotesManager][Start:AddNewCustomerNote][Note=" + note + "]");
            try
            {
                string query = "update BUSDTA.M0112 set NDDFLT=0 WHERE NDAN8 = '" + note.CustomerID + "'";
                //result = Helpers.DbEngine.ExecuteNonQuery(query);
                query = "insert into busdta.M0112 (NDAN8, NDDTTM, NDDTLS, NDTYP, NDDFLT, NDCRBY, NDCRDT, NDUPBY, NDUPDT)";
                query = query + " values ('" + note.CustomerID + "', getdate(), ";
                query = query + "?, 'Customer', " + "'" + Convert.ToInt32(note.IsDefault) + "', " + "'"+CommonNavInfo.UserName+"', ";
                query = query + "getdate(), " + "'"+CommonNavInfo.UserName+"', " + "getdate());";

                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.NVarChar;
                parm.Value = note.NoteDetails;
                //command.Parameters[0].Value = note.NoteDetails;
                command.Parameters.Add(parm);
                result = new DB_CRUD().InsertUpdateData(command);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerNotesManager][AddNewCustomerNote][Note=" + note + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerNotesManager][End:AddNewCustomerNote][Note=" + note + "]");
            return result;
        }

        public int DeleteSelectedNote(CustomerNote note)
        {
            int result = -1;
            try
            {
                string query = "DELETE FROM BUSDTA.M0112 WHERE NDID = '" + note.NoteID + "' AND ";
                query = query + "NDAN8 = '" + note.CustomerID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        internal void SetDefaultNote(CustomerNote selectedNote)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerNotesManager][Start:SetDefaultNote][selectedNote=" + selectedNote.SerializeToJson() + "]");
            try
            {
                string query = string.Empty;
                query = "update BUSDTA.M0112 set NDDFLT=0 WHERE NDAN8 = '" + selectedNote.CustomerID + "'";
                int result = Helpers.DbEngine.ExecuteNonQuery(query);
                query = "update BUSDTA.M0112 set NDDFLT=1 WHERE NDID = '" + selectedNote.NoteID + "'";
                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerNotesManager][SetDefaultNote][selectedNote=" + selectedNote.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException == null ? ex.Message : ex.InnerException.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerNotesManager][End:SetDefaultNote][selectedNote=" + selectedNote.SerializeToJson() + "]");
        }
    }
}
