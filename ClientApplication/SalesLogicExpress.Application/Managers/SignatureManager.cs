﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;

namespace SalesLogicExpress.Application.Managers
{
    public class SignatureManager
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.SignatureManager");


        public Int32 UpdateSignature(Flow lifeCycleFlow, SignType signType, int idToUpdate, byte[] signbyte, bool saveCanvasAsImage, string imageName = "")
        {

            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][Start:UpdateSignature]");
            try
            {
                string query = "";

                if ((lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.OrderSign) || (lifeCycleFlow == Flow.OrderReturn && signType == SignType.ReturnOrderSign))
                {
                    query = "Update busdta.ORDER_HEADER set orderSign=? where orderid=" + idToUpdate.ToString();
                }
                else if (lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.ChargeOnAccountSign)
                {
                    query = "Update busdta.ORDER_HEADER set ChargeOnAccountSign=? where orderid=" + idToUpdate.ToString();
                }
                else if (lifeCycleFlow == Flow.Credits && signType == SignType.CreditProcessSign)
                {
                    query = "Update busdta.Receipt_Header set CustomerSignature=? where ReceiptID=" + idToUpdate.ToString();
                }
                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                iAnywhere.Data.SQLAnywhere.SAParameter parms = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parms.ParameterName = "SignatureParam";
                parms.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongBinary;
                parms.Value = signbyte;
                command.Parameters.Add(parms);
                new DB_CRUD().InsertUpdateData(command);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Manager][SignatureManager][UpdateSignature][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][End:UpdateSignature]");
            return 1;

        }

        public StrokeCollection GetSignature(Flow lifeCycleFlow, SignType signType, int idToGet)
        {
            
            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][Start:GetSignature]");
            StrokeCollection SignStrokeCollection = new StrokeCollection();
            try
            {
                byte[] signbytes = null;
                string query = "";

                if ((lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.OrderSign) || (lifeCycleFlow == Flow.OrderReturn && signType == SignType.ReturnOrderSign))
                {
                    query = "select ordersign from busdta.ORDER_HEADER where orderid=" + idToGet.ToString();
                }
                else if (lifeCycleFlow == Flow.OrderLifeCycle && signType == SignType.ChargeOnAccountSign)
                {
                    query = "select ChargeOnAccountSign from busdta.ORDER_HEADER where orderid=" + idToGet.ToString();
                }
                else if (lifeCycleFlow == Flow.Credits && signType == SignType.CreditProcessSign)
                {
                    query = "select CustomerSignature from busdta.Receipt_Header where ReceiptID=" + idToGet.ToString();
                }
                else if (lifeCycleFlow == Flow.OrderReturn && signType == SignType.ReturnOrderSign)
                {
                    query = "select OrderSign from busdta.ORDER_HEADER where orderid=" + idToGet.ToString();
                }
                else if (lifeCycleFlow == Flow.PreTripInspection && signType == SignType.PreTripInspection)
                {
                    query = "select VerSignature from busdta.PreTrip_Inspection_Header where PreTripInspectionHeaderId=" + idToGet.ToString();
                }

                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);

                using (iAnywhere.Data.SQLAnywhere.SADataReader sdr = new DB_CRUD().ExecuteReader(command))
                {
                    if (sdr.Read())
                    {
                        signbytes = string.IsNullOrEmpty(sdr.GetValue(0).ToString()) ? null : (byte[])sdr.GetValue(0);
                    }
                    sdr.Close();
                }

                if (signbytes != null)
                {
                    using (MemoryStream ms = new MemoryStream(signbytes))
                    {
                        SignStrokeCollection = new StrokeCollection(ms);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Manager][SignatureManager][GetSignature][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][SignatureManager][End:GetSignature]");

            return SignStrokeCollection;

        }


    }
}
