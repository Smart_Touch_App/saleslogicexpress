﻿using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    /// <summary>
    /// Static class for getting logged in user's Route and Branch
    /// </summary>
    public static class UserManager
    {
        private static string _userRoute, _RouteEntered, _userBranch, _userName, _RouteName;

        
        private static int _userId;
        public static string UserRoute
        {
            get
            {
                if (string.IsNullOrEmpty(_userRoute))
                {
                    _userRoute = DbEngine.ExecuteScalar("select FFROUT from busdta.F56M0001 where FFUSER =" + "'" + _RouteEntered + "'" + ";");
                }
                return _userRoute.Trim();
            }
            set
            {
                _userRoute = value;
            }
        }

        private static int replenishmentToBranch = 101;

        public static int ReplenishmentToBranch
        {
            get { return replenishmentToBranch; }
            set { replenishmentToBranch = value; }
        }
        public static string RouteEntered
        {
            get { return _RouteEntered; }
            set { _RouteEntered = value; }
        }
        public static string RouteName
        {
            get
            {
                if (string.IsNullOrEmpty(_RouteName))
                {
                    _RouteName = DbEngine.ExecuteScalar("select (select DRDL01 from BUSDTA.F0005 where  DRSY = '42' and DRRT = 'RT' and ltrim(DRKY) = '" + UserRoute + "') as 'RouteDescription' from dummy;");

                }
                return _RouteName.Trim();
            }
            set { _RouteName = value; }
        }
        public static string UserBranch
        {
            get
            {
                if (string.IsNullOrEmpty(_userBranch))
                {
                    _userBranch = DbEngine.ExecuteScalar("select FFMCU from busdta.F56M0001;");
                }
                return _userBranch.Trim();
            }
        }

        public static string UserName
        {
            get
            {
                return _userName.Trim();
            }
            set
            {
                _userName = value;
            }
        }

        public static int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        private static string   repnlBranchType = string.Empty;

        public static string RepnlBranchType
        {
            get { return repnlBranchType; }
            set { repnlBranchType = value; }
        }
        public static string GetLoginIDofDomainUser(string DomainUserName)
        {
            string query = string.Format("SELECT App_user FROM BUSDTA.user_master WHERE DomainUser='{0}'", DomainUserName);
            return Convert.ToString(DbEngine.ExecuteScalar(query));
        }
        public static int GetUserId(string UserName, string Password = "")
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar(" select App_user_id from busdta.user_master where App_User = '" + UserName + "' "));
        }

        public static int GetRouteId(string routeName)
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar(" select RouteMasterID from busdta.Route_Master where routeName ='" + routeName + "' "));
        }

        public static int GetRepnBranchID(int routeID)
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar(" select RepnlBranch from busdta.Route_Master where  RouteMasterID= " + routeID + " "));
        }

        public static string GetRepnBranchType(int routeID)
        {
            return Convert.ToString(DbEngine.ExecuteScalar(" select LTRIM(RTRIM(RepnlBranchType)) from busdta.Route_Master where  RouteMasterID= " + routeID + " "));
        }
    }
}
