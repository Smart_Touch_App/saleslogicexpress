﻿//************************************************************************************************
// Comment: Double dastatypes changed to decimal as part from nimesh consideration
// Created: feb 22, 2016
// Author: Vivensas (Rajesh,Yuvaraj)
// Revisions: 
//*************************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using System.Data;
using log4net;
using iAnywhere.Data.SQLAnywhere;
using SalesLogicExpress.Application.Helpers;
using System.Collections.ObjectModel;
namespace SalesLogicExpress.Application.Managers
{
    public class QuotePricingManager
    {
        private readonly ILog log = LogManager.GetLogger("PricingManager");
        private String connectionString = "Data Source=SLPOCClientDB;UID=dba;PWD=sql";
        private Boolean validinputBranch, validinputShipTo, validinputItem, validinputQuantity;
        private DB_CRUD dbEngine;
        //Variable for input to Pricing Algorithm
        string textBoxBranchPlant = null;
        string textBoxShipTo = null;
        string textBoxItemNumber = null;
        string textBoxQuantity = null;

        //Other derived fields
        string textBoxPricingUoM = null;
        string textBoxPrimaryUoM = null;
        string textBoxSchedule = null;
        string textBoxBillTo = null;
        string textBoxParent = null;
        string textBoxQuantityInPrimary = null;

        string textBoxUnitPrice = null;// Use this as unit price
        /* DataSets */
        private DataSet dsPricing;
        private SADataAdapter daPricing;
        private SACommand cmdPricing;

        private DateTime start = DateTime.Now;
        private DateTime End = DateTime.Now;

        /* Define what preference options are need for adjustment search */
        static private String[] preferenceUsingItemNumber = { "HYHY01", "HYHY04", "HYHY07", "HYHY10", "HYHY13", "HYHY16", "HYHY19" };
        static private String[] preferenceUsingItemGroup = { "HYHY02", "HYHY05", "HYHY08", "HYHY11", "HYHY14", "HYHY17", "HYHY20" };
        static private String[] preferenceUsingItemAll = { "HYHY03", "HYHY06", "HYHY09", "HYHY12", "HYHY15", "HYHY18", "HYHY21" };
        static private String[] preferenceUsingCustomerNumber = { "HYHY01", "HYHY02", "HYHY03", "HYHY07", "HYHY08", "HYHY09", "HYHY13", "HYHY14", "HYHY15" };
        static private String[] preferenceUsingCustomerGroup = { "HYHY04", "HYHY05", "HYHY06", "HYHY10", "HYHY11", "HYHY12", "HYHY16", "HYHY17", "HYHY18" };
        static private String[] preferenceUsingCustomerAll = { "HYHY19", "HYHY20", "HYHY21" };
        static private String[] preferenceUsingShipTo = { "HYHY01", "HYHY02", "HYHY03", "HYHY04", "HYHY05", "HYHY06" };
        static private String[] preferenceUsingBillTo = { "HYHY07", "HYHY08", "HYHY09", "HYHY10", "HYHY11", "HYHY12" };
        static private String[] preferenceUsingParent = { "HYHY13", "HYHY14", "HYHY15", "HYHY16", "HYHY17", "HYHY18" };

        private Boolean useHierarchyItemNumber, useHierarchyItemGroup, userHierarchyAllItems,
            useHierarchyCustomerNumber, useHierarchyCustomerGroup, userHierarchyAllCustomers,
            useHierarchyShipTo, useHierarchyBillTo, useHierarchyParent;

        private int keyF4072_ITM, keyF4072_AN8, keyF4072_IGID, keyF4072_CGID;
        private DataRow itemRow, customerShipToRow, customerBillToRow, customerParentRow;

        private int jdeDateNow = 0, jdeTimeNow = 0;
        private decimal accumlateprice = 0;
        private decimal baseprice = 0;
        private Boolean basepricefound = false;

        public QuotePricingManager()
        {
            dbEngine = new DB_CRUD();
            validinputBranch = validinputShipTo = validinputItem = validinputQuantity = false;
        }
        private void textBoxBranchPlant_Validating(string branchPlant)
        {
            textBoxBranchPlant = branchPlant;
            if (textBoxBranchPlant.Length > 0)
            {
                textBoxBranchPlant = formatBranch(textBoxBranchPlant.ToUpper());
                if (validateBranch(textBoxBranchPlant))
                {
                    validinputBranch = true;
                }

                else
                {
                    validinputBranch = false;
                }
            }
            else
                validinputBranch = false;
        }
        private void textBoxShipTo_Validating(string shipTo)
        {
            textBoxShipTo = shipTo;
            if (textBoxShipTo.Length > 0)
            {
                if (validateAddressBook(Convert.ToInt32(textBoxShipTo)))
                {
                    getShipToRelatedFields(Convert.ToInt32(textBoxShipTo));
                    validinputShipTo = true;
                }
                else
                {
                    validinputShipTo = false;
                }
            }
            else
                validinputShipTo = false;
        }
        private void textBoxItemNumber_Validating(string itemNumber)
        {
            textBoxItemNumber = itemNumber;
            if (textBoxItemNumber.Length > 0)
            {
                textBoxItemNumber = textBoxItemNumber.ToUpper();
                if (validateItemMaster(textBoxItemNumber))
                {
                    validinputItem = true;
                }

                else
                {
                    validinputItem = false;
                }
            }
            else
                validinputItem = false;
        }
        private void textBoxQuantity_Validating(string quantity)
        {
            textBoxQuantity = quantity;
            if (textBoxQuantity.Length > 0)
            {
                if (Convert.ToInt32(textBoxQuantity) > 0)
                {
                    validinputQuantity = true;
                }

                else
                {
                    validinputQuantity = false;
                }
            }
            else
                validinputQuantity = false;

        }
        private String formatBranch(String szBranch)
        {
            String returnStr = String.Empty;
            if (szBranch.Length != 0)
                returnStr = szBranch.Trim().PadLeft(12);
            return returnStr;
        }
        private Boolean validateBranch(String szBranch)
        {
            int returnCount = 0;

            SAConnection jdeConn = null;

            SACommand jderequest = null;
            try
            {
                jderequest = new SACommand("select count(*) from BUSDTA.F0006 where rtrim(ltrim(MCMCU)) = '" + szBranch.Trim() + "'", jdeConn);
                returnCount = (int)dbEngine.ExecuteScalar(jderequest);
            }
            catch (Exception e)
            {
            }
            finally
            {
            }

            if (returnCount > 0)
                return true;
            return false;
        }
        private Boolean validateAddressBook(int mnABNumber)
        {
            int returnCount = 0;

            SAConnection jdeConn = null;

            SACommand jderequest = null;
            try
            {
                jderequest = new SACommand("select count(*) from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where ABAN8 = " + mnABNumber + "", jdeConn);
                returnCount = (int)dbEngine.ExecuteScalar(jderequest);
            }
            catch (Exception e)
            {
            }
            finally
            {
            }

            if (returnCount > 0)
                return true;
            return false;
        }
        private Boolean validateItemMaster(String szItemNumber)
        {

            Boolean rtn = false;

            SAConnection jdeConn = null;

            SACommand jderequest = null;

            SADataReader jdedatardr = null;
            try
            {
                jderequest = new SACommand("select IMUOM4 from BUSDTA.F4101 where IMLITM like '" + szItemNumber + "%'", jdeConn);
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                if (jdedatardr.Read())
                {
                    textBoxPricingUoM = jdedatardr["IMUOM4"].ToString();
                    rtn = true;
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
            return rtn;
        }
        private void getShipToRelatedFields(int mnABNumber)
        {

            SAConnection jdeConn = null;

            SACommand jderequest = null;

            SADataReader jdedatardr = null;
            try
            {
                jderequest = new SACommand("SELECT AIAN8,AIASN from BUSDTA.F03012 where AIAN8 in (Select ABAN81 from BUSDTA.F0101 where ABAN8=" + mnABNumber + ")", jdeConn);
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                if (jdedatardr.Read())
                {
                    textBoxBillTo = jdedatardr["AIAN8"].ToString();
                    textBoxSchedule = jdedatardr["AIASN"].ToString();
                }
                if (jdedatardr != null)
                    jdedatardr.Close();
                jderequest.CommandText = "SELECT AIAN8 from BUSDTA.F03012 where AIAN8 in (select MAPA8 from BUSDTA.F0150 where MAAN8=" + mnABNumber + " and MAOSTP='   ')";
                jdedatardr = jderequest.ExecuteReader();
                if (jdedatardr.Read())
                    textBoxParent = jdedatardr["AIAN8"].ToString();
                else
                    textBoxParent = textBoxShipTo;
            }
            catch (Exception e)
            {
                //addTraceLine("Exception: " + e.Message.ToString());
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
        }
        private void getJDEDateTimeNow(ref int jdeDate, ref int jdeTime)
        {
            DateTime cdt = DateTime.Now;
            jdeDate = 1000 * (cdt.Year - 1900) + cdt.DayOfYear;
            jdeTime = (cdt.Hour * 10000) + (cdt.Minute * 100) + (cdt.Second);
        }
        private Dictionary<int, String> getPreferenceOrdering(String preference)
        {
            Dictionary<int, String> preferenceDictionary = new Dictionary<int, string>();
            DataRow preferenceRow = dsPricing.Tables["Preferences"].Rows.Find(preference);
            for (int i = 1; i <= 21; i++)
            {
                int level = Convert.ToInt32(preferenceRow[i].ToString());
                if (level > 0)
                    preferenceDictionary.Add(level, preferenceRow.Table.Columns[i].ColumnName);
            }
            return preferenceDictionary;
        }
        private int getCustomerGroupKey(String customerGroup, DataRow customerRow)
        {
            int groupKeyID = 0;
            DataRow customerGroupRow = dsPricing.Tables["CustomerGroupDefs"].Rows.Find(customerGroup);

            String CGP1, CGP2, CGP3, CGP4, CGP5, CGP6, CGP7, CGP8, CGP9, CGP10;
            CGP1 = CGP2 = CGP3 = CGP4 = CGP5 = CGP6 = CGP7 = CGP8 = CGP9 = CGP10 = "   ";
            String csColumnValue;
            for (int x = 1; x <= 10; x++)
            {
                if (customerGroupRow[x].ToString().CompareTo("          ") == 1)
                {
                    csColumnValue = customerRow["AI" + customerGroupRow[x].ToString().Trim()].ToString();
                    switch (x)
                    {
                        case 1:
                            CGP1 = csColumnValue;
                            break;
                        case 2:
                            CGP2 = csColumnValue;
                            break;
                        case 3:
                            CGP3 = csColumnValue;
                            break;
                        case 4:
                            CGP4 = csColumnValue;
                            break;
                        case 5:
                            CGP5 = csColumnValue;
                            break;
                        case 6:
                            CGP6 = csColumnValue;
                            break;
                        case 7:
                            CGP7 = csColumnValue;
                            break;
                        case 8:
                            CGP8 = csColumnValue;
                            break;
                        case 9:
                            CGP9 = csColumnValue;
                            break;
                        case 10:
                            CGP10 = csColumnValue;
                            break;
                    }
                }
                else
                    break;
            }

            SAConnection jdeConn = null;
            SACommand jderequest = null;
            SADataReader jdedatardr = null;

            String sqlgroupKeySearch = "select CKCGID from BUSDTA.F40942 where CKCPGP = '" + customerGroup + "'";
            //sqlgroupKeySearch += CGP1.Trim().Equals("") ? "" : " and CKCGP1 ='" + CGP1 + "' ";
            //sqlgroupKeySearch += CGP2.Trim().Equals("") ? "" : " and CKCGP2 ='" + CGP2 + "' ";
            //sqlgroupKeySearch += CGP3.Trim().Equals("") ? "" : " and CKCGP3 ='" + CGP3 + "' ";
            //sqlgroupKeySearch += CGP4.Trim().Equals("") ? "" : " and CKCGP4 ='" + CGP4 + "' ";
            //sqlgroupKeySearch += CGP5.Trim().Equals("") ? "" : " and CKCGP5 ='" + CGP5 + "' ";
            //sqlgroupKeySearch += CGP6.Trim().Equals("") ? "" : " and CKCGP6 ='" + CGP6 + "' ";
            //sqlgroupKeySearch += CGP7.Trim().Equals("") ? "" : " and CKCGP7 ='" + CGP7 + "' ";
            //sqlgroupKeySearch += CGP8.Trim().Equals("") ? "" : " and CKCGP8 ='" + CGP8 + "' ";
            //sqlgroupKeySearch += CGP9.Trim().Equals("") ? "" : " and CKCGP9 ='" + CGP9 + "' ";
            //sqlgroupKeySearch += CGP10.Trim().Equals("") ? "" : " and CKCGP10 ='" + CGP10 + "' ";
            sqlgroupKeySearch += " and CKCGP1 ='" + CGP1 + "' ";
            sqlgroupKeySearch += " and CKCGP2 ='" + CGP2 + "' ";
            sqlgroupKeySearch += " and CKCGP3 ='" + CGP3 + "' ";
            sqlgroupKeySearch += " and CKCGP4 ='" + CGP4 + "' ";
            sqlgroupKeySearch += " and CKCGP5 ='" + CGP5 + "' ";
            sqlgroupKeySearch += " and CKCGP6 ='" + CGP6 + "' ";
            sqlgroupKeySearch += " and CKCGP7 ='" + CGP7 + "' ";
            sqlgroupKeySearch += " and CKCGP8 ='" + CGP8 + "' ";
            sqlgroupKeySearch += " and CKCGP9 ='" + CGP9 + "' ";
            sqlgroupKeySearch += " and CKCGP10 ='" + CGP10 + "' ";

            try
            {
                jderequest = new SACommand(sqlgroupKeySearch, jdeConn);
                start = DateTime.Now;
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                End = DateTime.Now;
                TimeSpan timeItTook = End - start;
                //log.Info("Time taken for <getCustomerGroupKey()> : " + timeItTook);
                if (jdedatardr.Read())
                {
                    groupKeyID = Convert.ToInt32(jdedatardr[0].ToString());
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
            return groupKeyID;
        }
        private int getItemGroupKey(String itemGroup)
        {
            int groupKeyID = 0;
            DataRow itemGroupRow = dsPricing.Tables["ItemGroupDefs"].Rows.Find(itemGroup);

            String IGP1, IGP2, IGP3, IGP4, IGP5, IGP6, IGP7, IGP8, IGP9, IGP10;
            IGP1 = IGP2 = IGP3 = IGP4 = IGP5 = IGP6 = IGP7 = IGP8 = IGP9 = IGP10 = "   ";


            String ibColumnValue;
            for (int x = 1; x <= 10; x++)
            {
                if (itemGroupRow[x].ToString().CompareTo("          ") == 1)
                {
                    ibColumnValue = itemRow["IB" + itemGroupRow[x].ToString().Trim()].ToString();
                    switch (x)
                    {
                        case 1:
                            IGP1 = ibColumnValue;
                            break;
                        case 2:
                            IGP2 = ibColumnValue;
                            break;
                        case 3:
                            IGP3 = ibColumnValue;
                            break;
                        case 4:
                            IGP4 = ibColumnValue;
                            break;
                        case 5:
                            IGP5 = ibColumnValue;
                            break;
                        case 6:
                            IGP6 = ibColumnValue;
                            break;
                        case 7:
                            IGP7 = ibColumnValue;
                            break;
                        case 8:
                            IGP8 = ibColumnValue;
                            break;
                        case 9:
                            IGP9 = ibColumnValue;
                            break;
                        case 10:
                            IGP10 = ibColumnValue;
                            break;
                    }
                }
                else
                    break;
            }


            SAConnection jdeConn = null;
            SACommand jderequest = null;
            SADataReader jdedatardr = null;

            String sqlgroupKeySearch = "select IKIGID from BUSDTA.F40941 where IKPRGR = '" + itemGroup + "'";
            //sqlgroupKeySearch += IGP1.Trim().Equals("") ? "" : " and IKIGP1 ='" + IGP1 + "' ";
            //sqlgroupKeySearch += IGP2.Trim().Equals("") ? "" : " and IKIGP2 ='" + IGP2 + "' ";
            //sqlgroupKeySearch += IGP3.Trim().Equals("") ? "" : " and IKIGP3 ='" + IGP3 + "' ";
            //sqlgroupKeySearch += IGP4.Trim().Equals("") ? "" : " and IKIGP4 ='" + IGP4 + "' ";
            //sqlgroupKeySearch += IGP5.Trim().Equals("") ? "" : " and IKIGP5 ='" + IGP5 + "' ";
            //sqlgroupKeySearch += IGP6.Trim().Equals("") ? "" : " and IKIGP6 ='" + IGP6 + "' ";
            //sqlgroupKeySearch += IGP7.Trim().Equals("") ? "" : " and IKIGP7 ='" + IGP7 + "' ";
            //sqlgroupKeySearch += IGP8.Trim().Equals("") ? "" : " and IKIGP8 ='" + IGP8 + "' ";
            //sqlgroupKeySearch += IGP9.Trim().Equals("") ? "" : " and IKIGP9 ='" + IGP9 + "' ";
            //sqlgroupKeySearch += IGP10.Trim().Equals("") ? "" : " and IKIGP10 ='" + IGP10 + "' ";
            sqlgroupKeySearch += " and IKIGP1 ='" + IGP1 + "' ";
            sqlgroupKeySearch += " and IKIGP2 ='" + IGP2 + "' ";
            sqlgroupKeySearch += " and IKIGP3 ='" + IGP3 + "' ";
            sqlgroupKeySearch += " and IKIGP4 ='" + IGP4 + "' ";
            sqlgroupKeySearch += " and IKIGP5 ='" + IGP5 + "' ";
            sqlgroupKeySearch += " and IKIGP6 ='" + IGP6 + "' ";
            sqlgroupKeySearch += " and IKIGP7 ='" + IGP7 + "' ";
            sqlgroupKeySearch += " and IKIGP8 ='" + IGP8 + "' ";
            sqlgroupKeySearch += " and IKIGP9 ='" + IGP9 + "' ";
            sqlgroupKeySearch += " and IKIGP10 ='" + IGP10 + "' ";
            try
            {
                jderequest = new SACommand(sqlgroupKeySearch, jdeConn);
                start = DateTime.Now;
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                End = DateTime.Now;
                TimeSpan timeItTook = End - start;
                //log.Info("Time taken for <getItemGroupKey()> : " + timeItTook);
                if (jdedatardr.Read())
                {
                    groupKeyID = Convert.ToInt32(jdedatardr[0].ToString());
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
            return groupKeyID;
        }
        private void setHierarchyOption(String hierarchy)
        {
            useHierarchyItemNumber = useHierarchyItemGroup = userHierarchyAllItems = false;
            useHierarchyCustomerNumber = useHierarchyCustomerGroup = userHierarchyAllCustomers = false;
            useHierarchyShipTo = useHierarchyBillTo = useHierarchyParent = false;

            /* Revolve Item Hierarchy */
            foreach (String hierarchyOption in preferenceUsingItemNumber)
            {
                if (hierarchyOption == hierarchy)
                {
                    useHierarchyItemNumber = true;
                    break;
                }
            }
            if (!useHierarchyItemNumber)
            {
                foreach (String hierarchyOption in preferenceUsingItemGroup)
                {
                    if (hierarchyOption == hierarchy)
                    {
                        useHierarchyItemGroup = true;
                        break;
                    }
                }
                if (!useHierarchyItemGroup)
                    userHierarchyAllItems = true;
            }

            /* Revolve Invoice Hierarchy */
            foreach (String hierarchyOption in preferenceUsingCustomerNumber)
            {
                if (hierarchyOption == hierarchy)
                {
                    useHierarchyCustomerNumber = true;
                    break;
                }
            }
            if (!useHierarchyCustomerNumber)
            {
                foreach (String hierarchyOption in preferenceUsingCustomerGroup)
                {
                    if (hierarchyOption == hierarchy)
                    {
                        useHierarchyCustomerGroup = true;
                        break;
                    }
                }
                if (!useHierarchyCustomerGroup)
                    userHierarchyAllCustomers = true;
            }
            /* Revolve Invoice Hierarchy Type */
            if (!userHierarchyAllCustomers)
            {
                /* Ship To */
                foreach (String hierarchyOption in preferenceUsingShipTo)
                {
                    if (hierarchyOption == hierarchy)
                    {
                        useHierarchyShipTo = true;
                        break;
                    }
                }
                if (!useHierarchyShipTo)
                {
                    /* Bill To */
                    foreach (String hierarchyOption in preferenceUsingBillTo)
                    {
                        if (hierarchyOption == hierarchy)
                        {
                            useHierarchyBillTo = true;
                            break;
                        }
                    }
                    if (!useHierarchyBillTo)
                    {
                        /* Parent */
                        foreach (String hierarchyOption in preferenceUsingParent)
                        {
                            if (hierarchyOption == hierarchy)
                            {
                                useHierarchyParent = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        private String dumpSQLCommand(SACommand cmd)
        {
            String expandedSQL = String.Empty;
            expandedSQL = cmd.CommandText;
            foreach (SAParameter parm in cmd.Parameters)
            {
                expandedSQL = expandedSQL.Replace(parm.ParameterName, parm.Value.ToString());
            }
            return expandedSQL;
        }
        private Boolean setF4072SearchKeys(DataRow adjustmentRow)
        {
            try
            {
                DataRow pRow = null;
                keyF4072_ITM = keyF4072_AN8 = keyF4072_IGID = keyF4072_CGID = 0;
                /* Abort setting search keys if preference uses groups and group is not define on adjustment*/
                if (useHierarchyItemGroup && (String.IsNullOrWhiteSpace(adjustmentRow["ATPRGR"].ToString())))
                    return false;
                if (useHierarchyCustomerGroup && (String.IsNullOrWhiteSpace(adjustmentRow["ATCPGP"].ToString())))
                    return false;
                if (useHierarchyItemGroup)
                {
                    keyF4072_IGID = getItemGroupKey(adjustmentRow["ATPRGR"].ToString());
                    if (keyF4072_IGID == 0)
                        return false;
                }
                else
                {
                    if (!userHierarchyAllItems)
                        keyF4072_ITM = Convert.ToInt32(itemRow["IMITM"].ToString());
                }

                if (useHierarchyShipTo)
                    pRow = customerShipToRow;
                else if (useHierarchyBillTo)
                    pRow = customerBillToRow;
                else if (useHierarchyParent)
                    pRow = customerParentRow;

                if (useHierarchyCustomerGroup)
                {
                    keyF4072_CGID = getCustomerGroupKey(adjustmentRow["ATCPGP"].ToString(), pRow);
                    if (keyF4072_CGID == 0)
                        return false;
                }
                else
                {
                    if (!userHierarchyAllCustomers)
                        keyF4072_AN8 = Convert.ToInt32(pRow["AIAN8"].ToString());
                }
            }
            catch (Exception ex)
            {
                
            }
            
            return true;
        }
        public Decimal jdeUOMConversion(String fromUoM, String toUom, Int32 jdeShortItem)
        {
            Decimal conversionfactor = -1;
            Decimal fromToPrimary = -1;
            Decimal ToToPrimary = -1;

            SAConnection jdeConn = null;
            SACommand jderequest = null;
            SADataReader jdedatardr = null;
            if (fromUoM == toUom)
                return 1;
            try
                {
                /* Direct From/To */
                jderequest = new SACommand("SELECT UMCONV from BUSDTA.F41002 where UMMCU='            ' and UMITM=" + jdeShortItem + " and rtrim(ltrim(UMUM))='" + fromUoM + "' and rtrim(ltrim(UMRUM))='" + toUom + "'", jdeConn);
                jderequest.Parameters.Add("@ITM", SqlDbType.Int).Value = jdeShortItem;
                jderequest.Parameters.Add("@UM", SqlDbType.VarChar).Value = fromUoM;
                jderequest.Parameters.Add("@RUM", SqlDbType.VarChar).Value = toUom;
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                if (jdedatardr.Read())
                {
                    conversionfactor = Convert.ToDecimal(jdedatardr["UMCONV"].ToString()) / 10000000.0000m;
                }
                else
                {
                    /* Direct To/From */
                    jdedatardr.Close();
                    jderequest.CommandText = "SELECT UMCONV from BUSDTA.F41002 where UMMCU='            ' and UMITM=" + jdeShortItem + " and rtrim(ltrim(UMUM))='" + toUom + "' and rtrim(ltrim(UMRUM))='" + fromUoM + "'";
                    jderequest.Parameters["@UM"].Value = toUom;
                    jderequest.Parameters["@RUM"].Value = fromUoM;
                    jdedatardr = dbEngine.ExecuteReader(jderequest);
                    if (jdedatardr.Read())
                    {
                        conversionfactor = (1 / Convert.ToDecimal(jdedatardr["UMCONV"].ToString())) * 10000000.0000m;
                    }
                    else
                    {
                        /* Triangulate */
                        /* Get From to Primary */
                        jdedatardr.Close();
                        jderequest.CommandText = "SELECT UMCNV1 from BUSDTA.F41002 where UMMCU='            ' and UMITM=" + jdeShortItem + " and rtrim(ltrim(UMUM))='" + fromUoM + "'";
                        jderequest.Parameters["@UM"].Value = fromUoM;
                        jdedatardr = dbEngine.ExecuteReader(jderequest);
                        if (jdedatardr.Read())
                        {
                            fromToPrimary = Convert.ToDecimal(jdedatardr["UMCNV1"].ToString()) / 10000000.0000m;
                        }
                        else
                        {
                            jdedatardr.Close();
                            jderequest.CommandText = "SELECT UMCONV,UMCNV1 from BUSDTA.F41002 where UMMCU='            ' and UMITM=" + jdeShortItem + " and rtrim(ltrim(UMRUM))='" + fromUoM + "'";
                            jderequest.Parameters["@RUM"].Value = fromUoM;
                            jdedatardr = dbEngine.ExecuteReader(jderequest);
                            if (jdedatardr.Read())
                            {
                                fromToPrimary = (Convert.ToDecimal(jdedatardr["UMCNV1"].ToString())) / (Convert.ToDecimal(jdedatardr["UMCONV"].ToString()));
                            }
                        }
                        /* Get To to Primary */
                        if (fromToPrimary > 0)
                        {
                            jdedatardr.Close();
                            jderequest.CommandText = "SELECT UMCONV,UMCNV1 from BUSDTA.F41002 where UMMCU='            ' and UMITM=" + jdeShortItem + " and rtrim(ltrim(UMUM))='" + toUom + "'";
                            jderequest.Parameters["@UM"].Value = toUom;
                            jdedatardr = dbEngine.ExecuteReader(jderequest);
                            if (jdedatardr.Read())
                            {
                                ToToPrimary = Convert.ToDecimal(jdedatardr["UMCONV"].ToString()) / 10000000;
                            }
                            else
                            {
                                jdedatardr.Close();
                                jderequest.CommandText = "SELECT UMCONV,UMCNV1 from BUSDTA.F41002 where UMMCU='            ' and UMITM=" + jdeShortItem + " and rtrim(ltrim(UMRUM))='" + toUom + "'";
                                jderequest.Parameters["@RUM"].Value = toUom;
                                jdedatardr = dbEngine.ExecuteReader(jderequest);
                                if (jdedatardr.Read())
                                {
                                    ToToPrimary = (Convert.ToDecimal(jdedatardr["UMCNV1"].ToString())) / (Convert.ToDecimal(jdedatardr["UMCONV"].ToString()));
                                }
                            }
                            if (ToToPrimary > 0)
                            {
                                conversionfactor = fromToPrimary / ToToPrimary;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Info(string.Format("Pricing Manager jdeUOMConversion {0}", e.InnerException.StackTrace));
                log.Info(string.Format("Pricing Manager jdeUOMConversion fromUOM {0}", fromUoM));
                log.Info(string.Format("Pricing Manager jdeUOMConversion fromUOM {0}", toUom));
                log.Info(string.Format("Pricing Manager jdeUOMConversion fromUOM {0}", jdeShortItem));
            }

            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
            if (conversionfactor != -1)
                conversionfactor = Math.Round(conversionfactor, 7, MidpointRounding.AwayFromZero);
            return conversionfactor;
        }
        private Decimal getVariableTableAmount(String variableTable)
        {
            Decimal returnAmt = 0;
            SAConnection jdeConn = null;
            SACommand jderequest = null;
            SADataReader jdedatardr = null;
            try
            {
                jderequest = new SACommand("select * from BUSDTA.F4075 where rtrim(ltrim(VBVBT))='" + variableTable.Trim() + "' and VBEFTJ<=" + jdeDateNow + " and (VBEXDJ>=" + jdeDateNow + " or VBEXDJ=0)" +
                                            " Order By (Case WHEN rtrim(ltrim(VBUOM))='" + textBoxPricingUoM.Trim() + "' THEN 1 WHEN rtrim(ltrim(VBUOM))='" + textBoxPrimaryUoM.Trim() + "' THEN 2 ELSE 3 END)", jdeConn);
                start = DateTime.Now;
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                End = DateTime.Now;
                TimeSpan timeItTook = End - start;
                //log.Info("Time taken for <getVariableTableAmount()> : " + timeItTook);
                if (jdedatardr.Read())
                {
                    if (jdedatardr["VBUOM"].ToString().Trim() == textBoxPricingUoM.Trim())
                    { /* In Pricing */
                        returnAmt = Convert.ToDecimal(jdedatardr["VBUPRC"].ToString()) / 10000.0000m;
                    }
                    else
                    {
                        // TO DO - Implement non pricing variable table entry
                        returnAmt = Convert.ToDecimal(jdedatardr["VBUPRC"].ToString()) / 10000.0000m;
                        returnAmt = returnAmt / jdeUOMConversion(jdedatardr["VBUOM"].ToString(), textBoxPricingUoM.Trim(), Convert.ToInt32(itemRow["IMITM"]));
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
            return returnAmt;
        }

        private Boolean findandApplyAdjustmentDetails(DataRow adjustmentRow)
        {
            Boolean adjustmentApplied = false;
            SAConnection jdeConn = null;
            SACommand jderequest = null;
            SADataReader jdedatardr = null;
            try
            {
                // TOCHECK
                jderequest = new SACommand("SELECT * from BUSDTA.F4072 where ADAST='" + adjustmentRow["ATAST"].ToString() + "' and ADITM=" + keyF4072_ITM + " and ADAN8=" + keyF4072_AN8 + " and ADIGID=" + keyF4072_IGID + " and ADCGID=" + keyF4072_CGID + " and ADOGID=0 "
                                            + "and (ADUOM='" + textBoxPricingUoM + "' or ADUOM='" + textBoxPrimaryUoM + "' or ADUOM=''  or ADUOM='  ') and ADMNQ<=" + Convert.ToInt64(textBoxQuantity) * 100000000 + " and ADEFTJ<=" + jdeDateNow + " and (ADEXDJ>=" + jdeDateNow + " or ADEXDJ=0) "
                                            + "Order By (Case WHEN ADUOM='" + textBoxPricingUoM + "' THEN 1 WHEN ADUOM='" + textBoxPrimaryUoM + "' THEN 2 WHEN (ADUOM='' or ADUOM='  ') THEN 3 END),ADMNQ DESC", jdeConn);
                jderequest.Parameters.Add("@AST", SqlDbType.VarChar).Value = adjustmentRow["ATAST"].ToString();
                jderequest.Parameters.Add("@ITM", SqlDbType.Int).Value = keyF4072_ITM;
                jderequest.Parameters.Add("@AN8", SqlDbType.Int).Value = keyF4072_AN8;
                jderequest.Parameters.Add("@IGID", SqlDbType.Int).Value = keyF4072_IGID;
                jderequest.Parameters.Add("@CGID", SqlDbType.Int).Value = keyF4072_CGID;
                jderequest.Parameters.Add("@UOM4", SqlDbType.VarChar).Value = textBoxPricingUoM;
                jderequest.Parameters.Add("@UOM1", SqlDbType.VarChar).Value = textBoxPrimaryUoM;
                jderequest.Parameters.Add("@MNQ", SqlDbType.Int).Value = Convert.ToInt32(textBoxQuantity) * 100000000;
                jderequest.Parameters.Add("@JDEDATENOW", SqlDbType.Int).Value = jdeDateNow;
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                if (jdedatardr.Read())
                {

                    adjustmentApplied = true;
                    String basisCodes = jdedatardr["ADBSCD"].ToString().Trim();
                    String basisCodesDescription = "";
                    String adjustmentNameDescription = "";
                    String priceOverideAdjustment = "";
                    Decimal factorValue = 0;

                    Decimal unitPrice = 0;
                    if (adjustmentRow["ATACNT"].ToString().Trim() == "1" || adjustmentRow["ATACNT"].ToString().Trim() == "2")
                    {
                        switch (basisCodes)
                        {
                            case "1":
                                break;
                            case "2":
                                factorValue = Convert.ToDecimal(jdedatardr["ADFVTR"]) / 10000.0000m;
                                unitPrice = accumlateprice * (factorValue / 100);
                                break;
                            case "3":
                                break;
                            case "4":
                                break;
                            case "5":
                                factorValue = Convert.ToDecimal(jdedatardr["ADFVTR"]) / 10000.0000m;
                                unitPrice = factorValue;
                                break;
                            case "6":
                                unitPrice = getVariableTableAmount(jdedatardr["ADFRMN"].ToString());
                                break;
                            case "7":
                                break;
                            case "8":
                                break;
                            case "9":
                                break;
                        }
                        unitPrice = Math.Round(unitPrice, 4, MidpointRounding.AwayFromZero);
                        if (adjustmentRow["ATABAS"].ToString().Trim() == "Y")
                        {
                            accumlateprice = unitPrice;
                            priceOverideAdjustment = "Y";
                        }
                        else
                        {
                            accumlateprice = accumlateprice + unitPrice;
                            priceOverideAdjustment = "N";
                        }
                        adjustmentNameDescription = getUDCDescription("40", "TY", adjustmentRow["ATAST"].ToString());
                        basisCodesDescription = getUDCDescription("40", "BC", basisCodes);
                    }
                    else /* Round Rule */
                    {
                        if ((basepricefound) && baseprice > 0)
                        {
                            /* JDE Bug - only apply rounding rules when base price is found and not zero */
                            if (jdedatardr["ADRULENAME"].ToString().Trim() == "PENNY-WHL")
                            {
                                //unitPrice = Math.Round(accumlateprice, 2, MidpointRounding.AwayFromZero);
                                //unitPrice = unitPrice - accumlateprice;
                                //unitPrice = Math.Round(unitPrice, 4, MidpointRounding.AwayFromZero);
                                //factorValue = unitPrice;
                                accumlateprice = accumlateprice + unitPrice;

                                //************************************************************************************************
                                // Comment: New Rounding Calculation given by nimesh
                                // Created: feb 16, 2016
                                // Author: Vivensas (Rajesh,Yuvaraj)
                                // Revisions: 
                                //*************************************************************************************************
                                unitPrice = 0;
                                factorValue = 0;
                                Decimal numtemp;
                                numtemp = Math.Truncate(accumlateprice * 1000.0000m) / 1000.0000m;
                                numtemp = Math.Round(numtemp, 3, MidpointRounding.AwayFromZero);
                                numtemp = Math.Round(numtemp, 2, MidpointRounding.AwayFromZero);
                                unitPrice = numtemp - accumlateprice;
                                unitPrice = Math.Round(unitPrice, 4, MidpointRounding.AwayFromZero);
                                accumlateprice = Math.Round(accumlateprice + unitPrice, 3, MidpointRounding.AwayFromZero);

                                //*************************************************************************************************
                                // Vivensas changes ends over here
                                //**************************************************************************************************
                            }
                            if (jdedatardr["ADRULENAME"].ToString().Trim() == "Nickel")
                            {
                                /* TO DO - Implement 5 cent round rule */
                            }
                            adjustmentNameDescription = getUDCDescription("40", "TY", adjustmentRow["ATAST"].ToString());
                            basisCodesDescription = getUDCDescription("40", "BC", basisCodes);
                        }

                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
            return adjustmentApplied;
        }
        public string loadPriceDetailsGrid(string textBoxBranchPlant, string textBoxShipTo, string textBoxItemNumber, string textBoxQuantity, string customerQuery="")
        {

            textBoxBranchPlant_Validating(textBoxBranchPlant);
            textBoxItemNumber_Validating(textBoxItemNumber);
            textBoxShipTo_Validating(textBoxShipTo);
            textBoxQuantity_Validating(textBoxQuantity);

            String sqlSchedule, sqlPreferences, sqlCustomerEntities, sqlItemData, sqlCustomerGroupDefs, sqlItemGroupDefs;
            // Set the Bill_To and Parent of Prospect to Ship_To/ProspectId itself. The default schedule for prospect is set as 'FBCOMBO'
            if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
            {
                textBoxSchedule = "FBCOMBO";
                textBoxBillTo = textBoxShipTo;
                textBoxParent = textBoxShipTo;
            }

            getJDEDateTimeNow(ref jdeDateNow, ref jdeTimeNow);

            sqlSchedule = "select SNASN,SNOSEQ,ATAST,ATPRGR,ATCPGP,ATSDGR,ATPRFR,ATLBT,ATGLC,ATACNT,ATLNTY,ATMDED,ATABAS,ATOLVL,ATTXB,ATPA01,ATPA04,ATAPRP1,ATPA07";
            sqlSchedule += " from BUSDTA.F4070 join BUSDTA.F4071 on SNAST=ATAST where SNEFTJ<=" + jdeDateNow + " and SNEXDJ>=" + jdeDateNow + " and rtrim(ltrim(SNASN)) ='" + textBoxSchedule.Trim() + "' and ATACNT in (1,2,8) Order By SNOSEQ";

            sqlPreferences = "select HYPRFR,HYHY01,HYHY02,HYHY03,HYHY04,HYHY05,HYHY06,HYHY07,HYHY08,HYHY09,HYHY10,HYHY11,HYHY12,HYHY13,HYHY14,HYHY15,HYHY16,HYHY17,HYHY18,HYHY19,HYHY20,HYHY21 ";
            sqlPreferences += "from BUSDTA.F40073 where rtrim(ltrim(HYPRFR)) in (select rtrim(ltrim(ATPRFR)) from BUSDTA.F4070 join BUSDTA.F4071 on SNAST=ATAST where SNEFTJ<=" + jdeDateNow + " and " + jdeDateNow + ">=114266 and rtrim(ltrim(SNASN)) = '" + textBoxSchedule.Trim() + "')";

            // Check if the pricing is to be calculated for Prospect/Customer
            if (!ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
            {
                if (string.IsNullOrEmpty(customerQuery))
                {
                    sqlCustomerEntities = "select * from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where ABAN8 = " + Convert.ToInt32(textBoxShipTo.Trim()) + " or rtrim(ltrim(ABAN8)) = '" + Convert.ToInt32(textBoxBillTo.Trim()) + "' or rtrim(ltrim(ABAN8)) = '" + Convert.ToInt32(textBoxParent.Trim()) + "'";
                }
                else
                {
                    // Add UNION query to the main query to select the BillTo/Parent. If All are same, no UNION query is attached.
                    string filter=string.Empty; 

                    if(!(textBoxBillTo.Trim().Equals((textBoxShipTo.Trim()))))
                    {
                        filter = "rtrim(ltrim(ABAN8)) = '" + textBoxBillTo.Trim() + "'";
                    }
                        
                    if(!(textBoxParent.Trim().Equals((textBoxShipTo.Trim()))))
                    {
                        filter = filter + (string.IsNullOrEmpty(filter) ? "" : " OR ") + "rtrim(ltrim(ABAN8)) = '" + textBoxParent.Trim() + "'"; 
                    }

                    if(!string.IsNullOrEmpty(filter))
                    {
                        customerQuery = customerQuery + " \nUNION\n select * from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 WHERE " + filter;
                    }
                    customerQuery = string.Format(customerQuery, textBoxShipTo.Trim());
                        
                }
            }
            else
            {
                sqlCustomerEntities = customerQuery;
                sqlCustomerEntities = string.Format(customerQuery, Convert.ToInt32(textBoxShipTo.Trim()), Convert.ToInt32(textBoxBillTo.Trim()), Convert.ToInt32(textBoxParent.Trim()));
            }
            

            sqlCustomerGroupDefs = "select GPGPC,GPGPK1,GPGPK2,GPGPK3,GPGPK4,GPGPK5,GPGPK6,GPGPK7,GPGPK8,GPGPK9,GPGPK10 from BUSDTA.F4092 where rtrim(ltrim(GPGPTY))='C' and rtrim(ltrim(GPGPC)) in ";
            sqlCustomerGroupDefs += "(select rtrim(ltrim(ATCPGP)) from BUSDTA.F4070 join BUSDTA.F4071 on SNAST=ATAST where SNEFTJ<=" + jdeDateNow + " and SNEXDJ>=" + jdeDateNow + " and rtrim(ltrim(SNASN)) ='" + textBoxSchedule.Trim() + "' and ATACNT in (1,2,8))";

            sqlItemGroupDefs = "select GPGPC,GPGPK1,GPGPK2,GPGPK3,GPGPK4,GPGPK5,GPGPK6,GPGPK7,GPGPK8,GPGPK9,GPGPK10 from BUSDTA.F4092 where rtrim(ltrim(GPGPTY))='I' and rtrim(ltrim(GPGPC)) in  ";
            sqlItemGroupDefs += "(select rtrim(ltrim(ATPRGR)) from BUSDTA.F4070 join BUSDTA.F4071 on SNAST=ATAST where SNEFTJ<=" + jdeDateNow + " and SNEXDJ>=" + jdeDateNow + " and rtrim(ltrim(SNASN))='" + textBoxSchedule.Trim() + "' and ATACNT in (1,2,8))";

            sqlItemData = "select * from BUSDTA.F4101 join BUSDTA.F4102 on IMITM=IBITM where rtrim(ltrim(IBMCU))='" + textBoxBranchPlant.Trim() + "' and rtrim(ltrim(IMLITM)) = '" + textBoxItemNumber.Trim() + "'";


            SAConnection jdeConn = null;
            cmdPricing = new SACommand();
            daPricing = new SADataAdapter();
            dsPricing = new DataSet();
            // TODO
            try
            {
                cmdPricing.Connection = dbEngine.GetActiveConnection();
                daPricing.SelectCommand = cmdPricing;
                /* Fill Dataset */
                cmdPricing.CommandText = sqlSchedule;
                //QRY#1
                start = DateTime.Now;
                End = DateTime.Now;
                TimeSpan timeItTook = End - start;
                daPricing.Fill(dsPricing, "Schedule");
                //log.Info("Time taken for <Schedule> : " + timeItTook);

                cmdPricing.CommandText = sqlPreferences;
                start = DateTime.Now;
                daPricing.Fill(dsPricing, "Preferences");
                dsPricing.Tables["Preferences"].PrimaryKey = new DataColumn[] { dsPricing.Tables["Preferences"].Columns["HYPRFR"] };
                End = DateTime.Now;
                timeItTook = End - start;
                //log.Info("Time taken for <Preferences> : " + timeItTook);

                cmdPricing.CommandText = customerQuery;
                start = DateTime.Now;
                daPricing.Fill(dsPricing, "CustomerEntities");
                // The primary_key for prospect is 'AIAN8' for Customer is 'ABAN8'
                if (!ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                {
                    dsPricing.Tables["CustomerEntities"].PrimaryKey = new DataColumn[] { dsPricing.Tables["CustomerEntities"].Columns["ABAN8"] };
                }
                else
                {
                    dsPricing.Tables["CustomerEntities"].PrimaryKey = new DataColumn[] { dsPricing.Tables["CustomerEntities"].Columns["AIAN8"] };
                }
                End = DateTime.Now;
                timeItTook = End - start;
                //log.Info("Time taken for <CustomerEntities> : " + timeItTook);

                cmdPricing.CommandText = sqlCustomerGroupDefs;
                start = DateTime.Now;
                daPricing.Fill(dsPricing, "CustomerGroupDefs");
                dsPricing.Tables["CustomerGroupDefs"].PrimaryKey = new DataColumn[] { dsPricing.Tables["CustomerGroupDefs"].Columns["GPGPC"] };
                End = DateTime.Now;
                timeItTook = End - start;
                //log.Info("Time taken for <CustomerGroupDefs> : " + timeItTook);

                cmdPricing.CommandText = sqlItemGroupDefs;
                start = DateTime.Now;
                daPricing.Fill(dsPricing, "ItemGroupDefs");
                dsPricing.Tables["ItemGroupDefs"].PrimaryKey = new DataColumn[] { dsPricing.Tables["ItemGroupDefs"].Columns["GPGPC"] };
                End = DateTime.Now;
                timeItTook = End - start;
                //log.Info("Time taken for <ItemGroupDefs> : " + timeItTook);

                cmdPricing.CommandText = sqlItemData;
                start = DateTime.Now;
                daPricing.Fill(dsPricing, "ItemData");
                dsPricing.Tables["ItemData"].PrimaryKey = new DataColumn[] { dsPricing.Tables["ItemData"].Columns["IMLITM"] };
                End = DateTime.Now;
                timeItTook = End - start;
                //log.Info("Time taken for <ItemData> : " + timeItTook);


            }
            catch (Exception e)
            {
            }
            finally
            {
                if (daPricing != null)
                    daPricing.Dispose();
                if (cmdPricing != null)
                    cmdPricing.Dispose();
            }

            /* Set Item/Invoice Rows */
            itemRow = dsPricing.Tables["ItemData"].Rows.Find(textBoxItemNumber);
            customerShipToRow = dsPricing.Tables["CustomerEntities"].Rows.Find(textBoxShipTo);
            customerBillToRow = dsPricing.Tables["CustomerEntities"].Rows.Find(textBoxBillTo);
            customerParentRow = dsPricing.Tables["CustomerEntities"].Rows.Find(textBoxParent);

            /* Set Primary Uom and Quantity in Primary */
            textBoxPrimaryUoM = itemRow["IMUOM1"].ToString();
            if (textBoxPricingUoM.CompareTo(textBoxPrimaryUoM) == 1)
            {
                textBoxQuantityInPrimary = Convert.ToString(
                    jdeUOMConversion(textBoxPricingUoM, textBoxPrimaryUoM, Convert.ToInt32(itemRow["IMITM"]))
                    * Convert.ToInt32(textBoxQuantity));
            }
            else
                textBoxQuantityInPrimary = textBoxQuantity;

            /* Load Base Price */
            start = DateTime.Now;
            getBasePrice();
            End = DateTime.Now;
            TimeSpan timeItTook1 = End - start;
            //log.Info("Time taken for <getBasePrice()> : " + timeItTook1);
            if (basepricefound) { }

            /* Process Adjustments */
            accumlateprice = 0;
            Dictionary<int, String> preferencelist;
            DataRow adjustmentRow;
            int i, x;

            /* For Each Adjustment Name */
            for (i = 0; i <= dsPricing.Tables["Schedule"].Rows.Count - 1; i++)
            {
                start = DateTime.Now;
                adjustmentRow = dsPricing.Tables["Schedule"].Rows[i];

                start = DateTime.Now;
                preferencelist = getPreferenceOrdering(adjustmentRow["ATPRFR"].ToString());
                End = DateTime.Now;
                timeItTook1 = End - start;
                //log.Info("Time taken for <getPreferenceOrdering()> : " + timeItTook1);
                /* For Each Preference Level */
                Boolean adjustmentApplied = false;
                for (x = 1; x <= preferencelist.Count && !adjustmentApplied; x++)
                {
                    setHierarchyOption(preferencelist[x]);
                    if (setF4072SearchKeys(adjustmentRow))
                    {
                        start = DateTime.Now;
                        adjustmentApplied = findandApplyAdjustmentDetails(adjustmentRow);
                        End = DateTime.Now;
                        timeItTook1 = End - start;
                        //log.Info("Time taken for <findandApplyAdjustmentDetails()> : " + timeItTook1);

                    }
                }
                preferencelist.Clear();
            }
            textBoxUnitPrice = Convert.ToString(accumlateprice);//required
            return textBoxUnitPrice;
        }
        private void getBasePrice()
        {



            SAConnection jdeConn = null;
            SACommand jderequest = null;
            SADataReader jdedatardr = null;
            baseprice = 0;
            basepricefound = false;
            try
            {
                /* Get with Pricing UOM */
                jderequest = new SACommand("SELECT BPUPRC,BPUOM from BUSDTA.F4106 where rtrim(ltrim(BPLITM)) = '" + itemRow["IMLITM"].ToString().Trim() + "' and BPEFTJ<=" + jdeDateNow + " and BPEXDJ>=" + jdeDateNow + " and TRIM(BPUOM)='" + itemRow["IMUOM4"].ToString().Trim() + "' ", jdeConn);
                jderequest.Parameters.Add("@LITM", SADbType.VarChar).Value = itemRow["IMLITM"].ToString();
                jderequest.Parameters.Add("@UOM", SADbType.VarChar).Value = itemRow["IMUOM4"].ToString();
                jderequest.Parameters.Add("@JDEDATENOW", SADbType.Integer).Value = jdeDateNow;
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                if (jdedatardr.Read())
                {
                    baseprice = Convert.ToDecimal(jdedatardr["BPUPRC"]);
                    if (baseprice > 0)
                        baseprice = baseprice / 10000;
                    basepricefound = true;
                }
                else
                {
                    /* Get with any UOM and convert */
                    if (jdedatardr != null)
                        jdedatardr.Close();
                    jderequest.CommandText = "SELECT BPUPRC,BPUOM from BUSDTA.F4106 where rtrim(ltrim(BPLITM)) = '" + itemRow["IMLITM"].ToString().Trim() + "' and BPEFTJ<=" + jdeDateNow + " and BPEXDJ>=" + jdeDateNow + " ";
                    jdedatardr = dbEngine.ExecuteReader(jderequest);
                    if (jdedatardr.Read())
                    {
                        baseprice = Convert.ToDecimal(jdedatardr["BPUPRC"]);
                        if (baseprice > 0)
                        {
                            baseprice = baseprice / 10000;
                            baseprice = baseprice * jdeUOMConversion(jdedatardr["BPUOM"].ToString(), itemRow["IMUOM4"].ToString(), Convert.ToInt32(itemRow["IMITM"]));
                        }
                        basepricefound = true;
                    }
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
        }
        private String getUDCDescription(String udcSY, String udcRT, String udcKY)
        {
            String returnDescription = String.Empty;



            SAConnection jdeConn = null;
            SACommand jderequest = null;
            SADataReader jdedatardr = null;
            try
            {
                jderequest = new SACommand("select DRDL01 from BUSDTA.F0005 where DRSY = '" + udcSY.PadRight(4) + "' and DRRT = '" + udcRT.PadRight(2) + "' and DRKY = '" + udcKY.PadRight(10) + "'", jdeConn);
                jderequest.Parameters.Add("@SY", SADbType.VarChar).Value = udcSY;
                jderequest.Parameters.Add("@RT", SADbType.VarChar).Value = udcRT;
                jderequest.Parameters.Add("@KY", SADbType.VarChar).Value = udcKY;
                start = DateTime.Now;
                jdedatardr = dbEngine.ExecuteReader(jderequest);
                End = DateTime.Now;
                TimeSpan timeItTook = End - start;
                //log.Info("Time taken for <getUDCDescription()> : " + timeItTook);
                if (jdedatardr.Read())
                {
                    returnDescription = jdedatardr["DRDL01"].ToString();
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (jdedatardr != null)
                {
                    jdedatardr.Close();
                }
            }
            return returnDescription;
        }

        public ObservableCollection<PricingModel> GetCustomerHomePricing(string customerNo)
        {
            log.Info("[SalesLogicExpress.Application.Managers][PricingManager][Start:GetCustomerHomePricing]");
            ObservableCollection<PricingModel> pricingList = new ObservableCollection<PricingModel>();
            try
            {
                //string query = "select "+
                //                " AIAC27 as 'AlliedDiscountOverallCd',  "+
                //                " BUSDTA.GetUDCDescription('01','27',AIAC27) as 'AlliedDiscountOverallDesc', "+
                //                " AIAC15 'AlliedDiscountCategory1Cd',  "+
                //                " BUSDTA.GetUDCDescription('01','15',AIAC01) as 'AlliedDiscountCategory1Desc', "+
                //                " AIAC16 'AlliedDiscountCategory2Cd',  "+
                //                " BUSDTA.GetUDCDescription('01','16',AIAC01) as 'AlliedDiscountCategory2Desc', "+
                //                " AIAC17 'AlliedDiscountCategory3Cd', "+
                //                " BUSDTA.GetUDCDescription('01','17',AIAC01) as 'AlliedDiscountCategory3Desc', "+
                //                " AIAC18 'AlliedDiscountCategory4Cd',  "+
                //                " BUSDTA.GetUDCDescription('01','18',AIAC01) as 'AlliedDiscountCategory4Desc', "+
                //                " AIAC23 'LiquidBracketCd', BUSDTA.GetUDCDescription('01','23',AIAC23) as  'LiquidBracketDesc', "+
                //                " AIAC24 'PriceProtectionCd', "+
                //                " BUSDTA.GetUDCDescription('01','24',AIAC24)  as 'PriceProtectionDesc', "+
                //                " AIAC28 'CoffVolumeCd', "+
                //                " BUSDTA.GetUDCDescription('01','28',AIAC28) as 'CoffVolumeDesc', "+
                //                " AIAC29 'EquipProgPtsCd', "+
                //                " BUSDTA.GetUDCDescription('01','29',AIAC29) as 'EquipProgPtsDesc', "+
                //                " AIAC22 'POSChargeCd', "+
                //                " BUSDTA.GetUDCDescription('01','22',AIAC22) as 'POSChargeDesc', "+
                //                " AIAC30 'CCPBracketCd', "+
                //                " BUSDTA.GetUDCDescription('01','30',AIAC30) as 'CCPBracketDesc', "+
                //                " AIAC11 'TAXGroupCd', "+
                //                " BUSDTA.GetUDCDescription('01','11',AIAC11) as 'TAXGroupDesc' "+
                //                " from busdta.F03012 "+
                //                " where AIAN8 = "+customerNo+" ";
                string query = "select " +
                " AIAC27 as 'AlliedDiscountOverallCd',  " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '27' and ltrim(DRKY) = AIAC27) as 'AlliedDiscountOverallDesc', " +
                " AIAC15 'AlliedDiscountCategory1Cd',  " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '15' and ltrim(DRKY) = AIAC27) as 'AlliedDiscountCategory1Desc', " +
                " AIAC16 'AlliedDiscountCategory2Cd',  " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '16' and ltrim(DRKY) = AIAC01) as 'AlliedDiscountCategory2Desc', " +
                " AIAC17 'AlliedDiscountCategory3Cd', " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '17' and ltrim(DRKY) = AIAC01) as 'AlliedDiscountCategory3Desc', " +
                " AIAC18 'AlliedDiscountCategory4Cd',  " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '18' and ltrim(DRKY) = AIAC01) as 'AlliedDiscountCategory4Desc', " +
                " AIAC23 'LiquidBracketCd', "+
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '23' and ltrim(DRKY) = AIAC23) as  'LiquidBracketDesc', " +
                " AIAC24 'PriceProtectionCd', " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '24' and ltrim(DRKY) = AIAC24)  as 'PriceProtectionDesc', " +
                " AIAC28 'CoffVolumeCd', " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '28' and ltrim(DRKY) = AIAC28) as 'CoffVolumeDesc', " +
                " AIAC29 'EquipProgPtsCd', " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '29' and ltrim(DRKY) = AIAC29) as 'EquipProgPtsDesc', " +
                " AIAC22 'POSChargeCd', " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '22' and ltrim(DRKY) = AIAC22) as 'POSChargeDesc', " +
                " AIAC30 'CCPBracketCd', " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '30' and ltrim(DRKY) = AIAC30) as 'CCPBracketDesc', " +
                " AIAC11 'TAXGroupCd', " +
                " (select DRDL01 from BUSDTA.F0005 where  DRSY = '01' and DRRT = '11' and ltrim(DRKY) = AIAC11) as 'TAXGroupDesc' " +
                " from busdta.F03012 " +
                " where AIAN8 = " + customerNo + " ";
                DataSet dsResult = DbEngine.ExecuteDataSet(query);

                string[] titleList = new string[] { "Allied Discount/Upcharge -Overall:"
                                                    ,"Allied Discount/Upcharge by Category 1:" 
                                                    ,"Allied Discount/Upcharge by Category 2:"
                                                    ,"Allied Discount/Upcharge by Category 3:"
                                                    ,"Allied Discount/Upcharge by Category 4:"
                                                    ,"Liquid Bracket:"
                                                    ,"Price Protection:"
                                                    ,"Coffee Volume Bracket:"
                                                    ,"Equipement Program/CCP Points:"
                                                    ,"P.O.S.Upcharge:"
                                                    ,"CCP Bracket:"
                                                    ,"Customer Tax Group:"
                                                    };

                int columnCount = dsResult.Tables[0].Columns.Count / 2;
                for (int i = 0; i < columnCount; i++)
                {
                    PricingModel pricing = new PricingModel();
                    if (dsResult.HasData())
                    {
                        pricing.Code = dsResult.Tables[0].Rows[0][i * 2].ToString().Trim();
                        pricing.Detail = dsResult.Tables[0].Rows[0][(i * 2) + 1].ToString().Trim();
                    }
                    else
                    {
                        pricing.Code = "";
                        pricing.Detail = "";
                    }
                    pricing.Title = titleList[i].ToString();
                    pricingList.Add(pricing);
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][PricingManager][GetCustomerHomePricing][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            log.Info("[SalesLogicExpress.Application.Managers][PricingManager][End:GetCustomerHomePricing]");
            return pricingList;
        }
    }
}
