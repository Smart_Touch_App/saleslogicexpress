﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Domain.Prospect_Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.ViewModels;
using System.Globalization;

namespace SalesLogicExpress.Application.Managers
{
    public class ProspectManager
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ProspectManager");
        string routeId = string.Empty;
        string _baseDate = ConfigurationManager.AppSettings["BaseDate"].ToString();
        public TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;


        public ObservableCollection<Prospect> GetProspectsForRoute(string routeID)
        {
            DateTime date = DateTime.Today;

            ObservableCollection<Prospect> objListProspect = new ObservableCollection<Prospect>();

            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetProspectsForRoute][routeID=" + routeID + "]");
            try
            {
                //                DataSet queryResult = DbEngine.ExecuteDataSet(@"                                                               
                //                                                                SELECT PM.PMAN8 AS ProspectId, pm.PMalph  AS Name,
                //                                                                CASE ISNULL( pc.PCAR1,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(pc.PCAR1),') ') END + ' '+ CONVERT(NVARCHAR(50), pc.Phone1) +' '+  CONVERT(NVARCHAR(50),pc.PCEXTN1) AS PhoneNumber,
                //                                                                pa.PAADD1 AS AddressLine1, pa.PAADD2 AS AddressLine2, pa.PAADD3 AS AddressLine3, pa.PAADD4 AS AddressLine4,
                //                                                                pa.PACTY1 AS City, pa.PAaDDS AS State, pa.PAADDZ AS Zip, pm.pmrout as Route
                //                                                                FROM BUSDTA.M04012 as pm left join BUSDTA.M40116 as pa on pm.PMAN8=pa.PAAN8 left join BUSDTA.M40111 pc on pm.PMAN8=pc.PCAN8 and pc.PCIDLN=0
                //                                                                WHERE pm.pmrout='" + routeID + "' order by pm.pman8 desc");
                var get_str = @"select pm.prospectId as ProspectId, pm.ProspectName as Name, 
                                      CASE 
                                WHEN ISNULL(pc.IsDefaultPhone1,0) = 1 THEN 
                                (CASE ISNULL( pc.AreaCode1,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(pc.AreaCode1),') ')  + ' '+ STRING(RTRIM(pc.phone1)) END + ' '+  CONVERT(NVARCHAR(50),pc.EXTN1) ) 
                                WHEN ISNULL(pc.IsDefaultPhone2,0) = 1 THEN 
                                (CASE ISNULL( pc.AreaCode2,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(pc.AreaCode2),') ')  + ' '+ STRING(RTRIM(pc.phone2)) END + ' '+  CONVERT(NVARCHAR(50),pc.EXTN2) ) 
                                WHEN ISNULL(pc.IsDefaultPhone3,0) = 1 THEN 
                                (CASE ISNULL( pc.AreaCode3,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(pc.AreaCode3),') ')  + ' '+ STRING(RTRIM(pc.phone3) ) END + ' '+  CONVERT(NVARCHAR(50),pc.EXTN3) ) 
                                WHEN ISNULL(pc.IsDefaultPhone4,0) = 1 THEN 
                                (CASE ISNULL( pc.AreaCode4,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(pc.AreaCode4),') ')  + ' '+ STRING(RTRIM(pc.phone4)) END + ' '+  CONVERT(NVARCHAR(50),pc.EXTN4) ) 
                                ELSE 
                                CASE ISNULL( pc.AreaCode1,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(pc.AreaCode1),') ')  + ' '+ STRING(RTRIM(pc.phone1)) END + ' '+  CONVERT(NVARCHAR(50),pc.EXTN1)  
                                END AS PhoneNumber,                                
                                pa.AddressLine1 AS AddressLine1, pa.AddressLine2 AS AddressLine2, pa.AddressLine3 AS AddressLine3, pa.AddressLine4 AS AddressLine4,
                                pa.CityName AS City, pa.StateName AS State, pa.ZipCode AS Zip, pm.RouteId as Route,
                                (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = pm.prospectid AND RPSTDT >= cast(getdate() as date) order by 1) as NextStop,
                                (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = pm.prospectid AND RPSTDT < cast(getdate() as date) order by 1 desc) as LastStop
                                from busdta.Prospect_Master pm left join busdta.Prospect_Address pa on pm.prospectid = pa.ProspectId 
                                left join busdta.Prospect_Contact pc on pm.ProspectId = pc.ProspectId and pc.ProspectContactId = '1'
                                where pm.Routeid = '" + CommonNavInfo.RouteID + "' order by pm.prospectid desc";

                DataSet queryResult = DbEngine.ExecuteDataSet(get_str);

                if (queryResult.HasData())
                {
                    Prospect objprospect;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                        objprospect = new Prospect();
                        {
                            objprospect.Route = customerDataRow["Route"].ToString();
                            objprospect.ProspectID = customerDataRow["ProspectId"].ToString();
                            objprospect.Phone = customerDataRow["PhoneNumber"].ToString();
                            objprospect.Address = customerDataRow["AddressLine1"].ToString().Trim() + " " + customerDataRow["AddressLine2"].ToString().Trim() + " " + customerDataRow["AddressLine3"].ToString().Trim() + " " + customerDataRow["AddressLine4"].ToString().Trim();
                            objprospect.City = customerDataRow["City"].ToString();
                            objprospect.Name = customerDataRow["Name"].ToString();
                            objprospect.Zip = customerDataRow["Zip"].ToString();
                            objprospect.State = customerDataRow["State"].ToString();
                            if (!(customerDataRow["NextStop"].ToString() == null || customerDataRow["NextStop"].ToString() == ""))
                            {
                                DateTime next = Convert.ToDateTime(customerDataRow["NextStop"]);
                                objprospect.NextStop = next.ToString("MM'/'dd'/'yyyy ");
                            }
                            else
                            {
                                objprospect.NextStop = "None";
                            }
                            if (!(customerDataRow["LastStop"].ToString() == null || customerDataRow["LastStop"].ToString() == ""))
                            {
                                DateTime next = Convert.ToDateTime(customerDataRow["LastStop"]);
                                objprospect.PreviousStop = next.ToString("MM'/'dd'/'yyyy ");
                            }
                            else
                            {
                                objprospect.PreviousStop = "None";
                            }
                            objprospect.VisiteeType = "prospect";
                            objprospect.ItemVisible = true;
                        }

                        objListProspect.Add(objprospect);
                    }
                }

                Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetProspectsForRoute][routeID=" + routeID + "]");
                return objListProspect;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetProspectsForRoute][routeID=" + routeID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return objListProspect;
            }
        }

        public int GetNewProspectSeries(string routeID)
        {

            string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Prospect);
            return Convert.ToInt32(val);
        }

        public int AddNewProspect(Prospect prospect, string routeID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddNewProspect]");
            int result = 0;
            try
            {
                if (!IsProspectAlreadyExists(prospect, routeID))
                {
                    prospect.ProspectID = GetNewProspectSeries(routeID).ToString(); //routeID.Length > 3 ? Managers.UserManager.GetRouteId(CommonNavInfo.RouteUser.Trim()).ToString() + GetNewProspectSeries(routeID) : Convert.ToInt32(routeID) + GetNewProspectSeries(routeID).ToString();

                    var query = @"INSERT INTO BUSDTA.Prospect_Master (ProspectId,RouteId,
                                        ProspectName,AcctSegmentType,
                                        BuyingGroup,LocationCount,
                                        CategoryCode01,OperatingUnit,
                                        Branch,District,
                                        Region,Reporting,
                                        Chain,BrewmaticAgentCode,
                                        NTR,ProspectTaxGrp,
                                        CategoryCode12,APCheckCode,
                                        CategoryCode14,CategoryCode15,
                                        CategoryCode16,CategoryCode17,
                                        CategoryCode18,CategoryCode19,
                                        CategoryCode20,CategoryCode21,
                                        CategoryCode22,SpecialEquipment,
                                        CAProtection,ProspectGroup,
                                        SPCommisFBType,AlliedDiscount,
                                        CoffeeVolume,EquipmentProgPts,
                                        SpecialCCP,CreatedBy,
                                        CreatedDatetime,UpdatedBy,
                                        UpdatedDatetime) 
                                VALUES('{0}','{1}','{2}',null,null,null,null,null,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                                        NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())";

                    var p_query = string.Format(query, prospect.ProspectID, CommonNavInfo.RouteID, prospect.Name.SqlStringEscaping().Trim());

                    var add_str = @"INSERT INTO BUSDTA.Prospect_Address (ProspectId,RouteId,
                                                                                AddressLine1,AddressLine2,
                                                                                AddressLine3,AddressLine4,
                                                                                CityName,StateName,ZipCode,
                                                                                CreatedBy,CreatedDatetime,
                                                                                UpdatedBy,UpdatedDatetime) 
                    VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',getdate(),'{9}',getdate())";
                    var addr_query = string.Format(add_str, prospect.ProspectID, CommonNavInfo.RouteID, prospect.AddressLine1.SqlStringEscaping().Trim()
                                                   , (string.IsNullOrEmpty(prospect.AddressLine2) ? "" : prospect.AddressLine2.ToString().SqlStringEscaping().Trim())
                                                   , (string.IsNullOrEmpty(prospect.AddressLine3) ? "" : prospect.AddressLine3.ToString().SqlStringEscaping().Trim())
                                                   , (string.IsNullOrEmpty(prospect.AddressLine4) ? "" : prospect.AddressLine4.ToString().SqlStringEscaping().Trim()), prospect.City.SqlStringEscaping().Trim(), prospect.State.Trim(), prospect.Zip.Trim(), UserManager.UserId);

                    var phone_str = @"INSERT INTO BUSDTA.Prospect_Contact (ProspectId,ProspectContactId,RouteId,FirstName,MiddleName,LastName,Title,IsActive,AreaCode1,Phone1,Extn1,
                                        PhoneType1,IsDefaultPhone1,AreaCode2,Phone2,Extn2,PhoneType2,IsDefaultPhone2,AreaCode3,Phone3,Extn3,PhoneType3,IsDefaultPhone3,
                                        AreaCode4,Phone4,Extn4,PhoneType4,IsDefaultPhone4,EmailID1,EmailType1,IsDefaultEmail1,EmailID2,EmailType2,IsDefaultEmail2,EmailID3,
                                        EmailType3,IsDefaultEmail3,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime,ShowOnDashboard) 
                                        VALUES('{0}','1','{1}','{2}',NULL,NULL,NULL,'{6}','{3}','{4}','{5}','{6}','{6}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                                        NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'{7}',getdate(),'{7}',getdate(),'{6}')";

                    var phone_query = string.Format(phone_str, prospect.ProspectID, CommonNavInfo.RouteID, prospect.Name.SqlStringEscaping(), prospect.Phone1, prospect.Phone2 + "-" + prospect.Phone3, string.IsNullOrEmpty(prospect.PhoneExtention) ? "" : prospect.PhoneExtention.SqlStringEscaping(), "1", UserManager.UserId);


                    DbEngine.ExecuteNonQuery(p_query);
                    DbEngine.ExecuteNonQuery(addr_query);
                    DbEngine.ExecuteNonQuery(phone_query);

                    /*  DbEngine.ExecuteNonQuery(@"
                                                INSERT INTO BUSDTA.M04012 (PMAN8,PMSRS,PMalph,pmrout) values ("
                                                  + prospect.ProspectId + ","
                                                + GetNewProspectSeries(routeID).ToString() + ",'"
                                                + prospect.Name.SqlStringEscaping().Trim() + "','" + routeID.Trim() + "')");

                    DbEngine.ExecuteNonQuery(@"
                                                INSERT INTO BUSDTA.M40116 (PAAN8,PAADD1,PAADD2,PAADD3,PAADD4,PACTY1,PAaDDS,PAADDZ)  values ("
                                                  + prospect.ProspectId + ",'"
                                                + prospect.AddressLine1.SqlStringEscaping().Trim() + "','"
                                                + (string.IsNullOrEmpty(prospect.AddressLine2) ? "" : prospect.AddressLine2.ToString().SqlStringEscaping().Trim())
                                                + "','" + (string.IsNullOrEmpty(prospect.AddressLine3) ? "" : prospect.AddressLine3.ToString().SqlStringEscaping().Trim()) + "','"
                                                + (string.IsNullOrEmpty(prospect.AddressLine4) ? "" : prospect.AddressLine4.ToString().SqlStringEscaping().Trim()) + "','"
                                                + prospect.City.SqlStringEscaping().Trim() + "','" + prospect.State.Trim()
                                                + "','" + prospect.Zip.Trim() + "')");

                    DbEngine.ExecuteNonQuery(@"
                                                INSERT INTO BUSDTA.M40111 (PCAN8,PCAR1,PCPH1,PCEXTN1,PCIDLN)  values ("
                                                  + prospect.ProspectId + " ,'" + prospect.Phone1 + "','" + prospect.Phone2 + "-" + prospect.Phone3 + "','"
                                                + (string.IsNullOrEmpty(prospect.PhoneExtention) ? "" : prospect.PhoneExtention.SqlStringEscaping().ToString().Trim()) + "',0)");
                      */
                    result = 1;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public bool IsProspectAlreadyExists(Prospect prospect, string routeID)
        {
            bool result = false;
            try
            {
                result = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(@"select count(*) from BUSDTA.Prospect_Master WHERE routeid='" + routeID + "' and lower(ProspectName)='" + prospect.Name.SqlStringEscaping().Trim().ToLower() + "'")));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][IsProspectAlreadyExists][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        #region Competitors

        #region Coffee
        public List<string> GetCompetitiors()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CompetitorID, Competitor FROM [BUSDTA].tblCompetitor");
            List<string> listCompetitors = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Competitor = Convert.ToString(CompetitiorDataRow["Competitor"]);

                    listCompetitors.Add(Competitor);
                }

            }

            return listCompetitors;

        }
        public List<string> GetCoffeeBlends()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CoffeeBlendID, CoffeeBlend FROM [BUSDTA].tblCoffeeBlends");
            List<string> listCoffeeBlend = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Competitor = Convert.ToString(CompetitiorDataRow["CoffeeBlend"]);

                    listCoffeeBlend.Add(Competitor);
                }

            }

            return listCoffeeBlend;

        }
        public List<string> GetUOMs()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT UOMID, UOMLable FROM [BUSDTA].tblUOM");
            List<string> listUOM = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string UOM = Convert.ToString(CompetitiorDataRow["UOMLable"]);

                    listUOM.Add(UOM);
                }

            }

            return listUOM;

        }
        public List<string> GetCoffeeVolumes()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CoffeeVolumeID, Volume FROM [BUSDTA].tblCoffeeVolume");
            List<string> listCoffeeVolume = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Volume = Convert.ToString(CompetitiorDataRow["Volume"]);

                    listCoffeeVolume.Add(Volume);
                }

            }

            return listCoffeeVolume;
        }
        public List<string> GetPackSize()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT PacketSizeID, Size FROM [BUSDTA].tblPacketSize");
            List<string> listPackSize = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Size = Convert.ToString(CompetitiorDataRow["Size"]);

                    listPackSize.Add(Size);
                }

            }

            return listPackSize;
        }
        public List<string> GetCsPkLb()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CSPKLBID, CSPKLBType FROM [BUSDTA].tblCSPKLB");
            List<string> listcspklb = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string CSPKLBType = Convert.ToString(CompetitiorDataRow["CSPKLBType"]);

                    listcspklb.Add(CSPKLBType);
                }

            }

            return listcspklb;

        }
        public List<string> GetLiqCoffees()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT LiqCoffeeTypeID, LiqCoffeeType FROM [BUSDTA].tblLiqCoffeeType");
            List<string> listLiqCoffee = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string LiqCoffee = Convert.ToString(CompetitiorDataRow["LiqCoffeeType"]);

                    listLiqCoffee.Add(LiqCoffee);
                }

            }

            return listLiqCoffee;

        }

        public int AddNewCoffeeProspect(CoffeeProspect coffeeProspect, string routeID, string prospectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddNewProspect]");
            int result = 0; string qry = string.Empty;
            try
            {
                coffeeProspect.CoffeeBlendID = GetCoffeeBlendID(coffeeProspect.CoffeeBlendSelected);
                coffeeProspect.CompetitorID = GetCompetitiorID(coffeeProspect.CompetitorSelected);
                coffeeProspect.UOMID = GetUOMID(coffeeProspect.UOMSelected);
                coffeeProspect.PackSize = GetPackSize(coffeeProspect.PackSizeSelected);
                coffeeProspect.CS_PK_LB = GetCSPKLB(coffeeProspect.CSPKLBSelected);
                coffeeProspect.UsageMeasurementID = GetCoffeeVolume(coffeeProspect.CoffeeVolumeSelected);
                coffeeProspect.LiqCoffeeTypeID = GetCoffeeType(coffeeProspect.LIQCoffeeSelected);
                coffeeProspect.UpdatedBy = Convert.ToInt32(PayloadManager.ApplicationPayload.LoggedInUserID);

                if (coffeeProspect.CoffeeProspectID == 0)
                {
                    if (!string.IsNullOrEmpty(coffeeProspect.CompetitorSelected) && !string.IsNullOrEmpty(coffeeProspect.CoffeeBlendSelected))// && !IsCoffeeProspectAlreadyExists(prospectID, coffeeProspect.CompetitorSelected)
                    {
                        coffeeProspect.CoffeeProspectID = Convert.ToInt32(GetNewCoffeeProspectSeries(prospectID));
                        //coffeeProspect.CoffeeProspectID = coffeeProspect.CoffeeProspectID.ToString().Length == 1 ? Convert.ToInt32(routeID.Trim().Substring(3, routeID.Length - 3) + GetNewCoffeeProspectSeries(prospectID)) : Convert.ToInt32(GetNewCoffeeProspectSeries(prospectID));

                        qry = @"INSERT INTO [BUSDTA].tblCoffeeProspect (ProspectID, CoffeeProspectID, CoffeeBlendID, CompetitorID, UOMID, PackSize, CS_PK_LB, Price, UsageMeasurementID, LiqCoffeeTypeID, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) VALUES ("
                                                    + prospectID.ToString() + ","
                                                    + coffeeProspect.CoffeeProspectID.ToString() + ","
                                                    + coffeeProspect.CoffeeBlendID.ToString() + ","
                                                    + coffeeProspect.CompetitorID.ToString() + ","
                                                    + coffeeProspect.UOMID.ToString() + ","
                                                    + coffeeProspect.PackSize.ToString() + ","
                                                    + coffeeProspect.CS_PK_LB.ToString() + ","
                                                    + coffeeProspect.Price.ToString() + ","
                                                    + coffeeProspect.UsageMeasurementID.ToString() + ","
                                                    + coffeeProspect.LiqCoffeeTypeID.ToString() + ",'"
                                                    + coffeeProspect.UpdatedBy.ToString() + "', GETDATE(),'"
                                                    + coffeeProspect.UpdatedBy.ToString() + "', GETDATE())";
                    }
                }
                else if (coffeeProspect.CoffeeProspectID > 0)
                {
                    qry = @"UPDATE [BUSDTA].tblCoffeeProspect
                            SET CoffeeBlendID =" + coffeeProspect.CoffeeBlendID.ToString() + ", CompetitorID =" + coffeeProspect.CompetitorID.ToString() + ", PackSize =" + coffeeProspect.PackSize.ToString() + ", CS_PK_LB =" + coffeeProspect.CS_PK_LB.ToString() + ", Price =" + coffeeProspect.Price.ToString() + ", UsageMeasurementID =" + coffeeProspect.UsageMeasurementID.ToString() + ", LiqCoffeeTypeID =" + coffeeProspect.LiqCoffeeTypeID.ToString() + ", UpdatedBy =" + coffeeProspect.UpdatedBy.ToString() + ", UpdatedDatetime =GETDATE(), ProspectID =" + prospectID.ToString() + " WHERE CoffeeProspectID = " + coffeeProspect.CoffeeProspectID.ToString();
                }
                if (!string.IsNullOrEmpty(qry))
                {
                    DbEngine.ExecuteNonQuery(qry);
                    result = 1;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public bool IsCoffeeProspectAlreadyExists(string prospectID, string competitor)
        {
            bool result = false;
            try
            {
                string qryExist = @"SELECT COUNT(1) FROM [BUSDTA].tblCoffeeProspect WHERE ProspectID ='" + prospectID + "' AND CompetitorID IN (SELECT CompetitorID FROM [BUSDTA].tblCompetitor WHERE Competitor='" + competitor + "') ";
                result = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(qryExist)));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][IsProspectAlreadyExists][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public int GetNewCoffeeProspectSeries(string prospectID)
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar(@"SELECT ISNULL(MAX(CoffeeProspectID),0)+1 NewCoffeeProspectID FROM [BUSDTA].tblCoffeeProspect"));
        }

        public ObservableCollection<CoffeeProspectVM> GetCoffeeProspectForRoute(string prospectID)
        {
            DateTime date = DateTime.Today;
            ObservableCollection<CoffeeProspectVM> coffeeProspectCollection = new ObservableCollection<CoffeeProspectVM>();

            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetProspectsForRoute][prospectID=" + prospectID + "]");
            try
            {
                DataSet queryResult = DbEngine.ExecuteDataSet(@"
                        SELECT cp.ProspectID, CoffeeProspectID, cb.CoffeeBlend, com.Competitor, uom.UOMLable AS UOM, pack.Size PackSize, cp.CS_PK_LB CSPKLBID, cspklb.CSPKLBType CS_PK_LB, Price, cp.UsageMeasurementID, vol.Volume UsageMeasurement, 
                            liq.LiqCoffeeType, upd.App_User UpdatedUser, cp.UpdatedDatetime
                        FROM [BUSDTA].tblCoffeeProspect AS cp
                        LEFT JOIN [BUSDTA].tblCoffeeBlends AS cb ON cp.CoffeeBlendID=cb.CoffeeBlendID
                        LEFT JOIN [BUSDTA].tblCompetitor AS com ON cp.CompetitorID=com.CompetitorID
                        LEFT JOIN [BUSDTA].tblUOM AS uom ON cp.UOMID=uom.UOMID
                        LEFT JOIN [BUSDTA].tblPacketSize AS pack ON cp.PackSize=pack.PacketSizeID
                        LEFT JOIN [BUSDTA].tblCSPKLB AS cspklb ON cp.CS_PK_LB=cspklb.CSPKLBID
                        LEFT JOIN [BUSDTA].tblCoffeeVolume AS vol ON cp.UsageMeasurementID=vol.CoffeeVolumeID
                        LEFT JOIN [BUSDTA].tblLiqCoffeeType AS liq ON cp.LiqCoffeeTypeID=liq.LiqCoffeeTypeID
                        LEFT JOIN BUSDTA.user_master AS cr ON cp.CreatedBy=cr.app_user_id
                        LEFT JOIN BUSDTA.user_master AS upd ON cp.CreatedBy=upd.app_user_id
                        WHERE cp.ProspectID='" + prospectID + "'");

                if (queryResult.HasData())
                {
                    CoffeeProspectVM coffeeProspect;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                        coffeeProspect = new CoffeeProspectVM
                        {
                            CoffeeProspectID = Convert.ToInt32(customerDataRow["CoffeeProspectID"]),
                            CoffeeBlend = customerDataRow["CoffeeBlend"].ToString(),
                            Competitor = customerDataRow["Competitor"].ToString(),
                            UOM = customerDataRow["UOM"].ToString(),
                            PackSizeText = customerDataRow["PackSize"].ToString(),
                            CS_PK_LB = Convert.ToInt32(customerDataRow["CSPKLBID"]),
                            CS_PK_LBText = customerDataRow["CS_PK_LB"].ToString(),
                            Price = Convert.ToInt32(customerDataRow["Price"]),
                            UsageMeasurementID = Convert.ToInt32(customerDataRow["UsageMeasurementID"]),
                            CoffeeVolume = Convert.ToString(customerDataRow["UsageMeasurement"]),
                            LiqCoffeeType = customerDataRow["LiqCoffeeType"].ToString(),
                            UpdatedUser = customerDataRow["UpdatedUser"].ToString(),
                            UpdatedDatetimeText = Convert.ToDateTime(customerDataRow["UpdatedDatetime"]).ToString("MM/dd/yyyy hh.mm tt")
                        };

                        coffeeProspectCollection.Add(coffeeProspect);
                    }


                }

                Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetProspectsForRoute][prospectID=" + prospectID + "]");
                return coffeeProspectCollection;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetProspectsForRoute][prospectID=" + prospectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return coffeeProspectCollection;
            }
        }

        public int GetCompetitiorID(string competitor)
        {
            competitor = competitor.ToString().Replace("'", "''");
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CompetitorID FROM [BUSDTA].tblCompetitor WHERE Competitor='" + competitor + "'");
            int CompetitorID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    CompetitorID = Convert.ToInt32(CompetitiorDataRow["CompetitorID"]);
                }
            }
            return CompetitorID;
        }
        public int GetCoffeeBlendID(string coffeeblend)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CoffeeBlendID FROM [BUSDTA].tblCoffeeBlends WHERE CoffeeBlend='" + coffeeblend + "'");
            int CoffeeBlendID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    CoffeeBlendID = Convert.ToInt32(CompetitiorDataRow["CoffeeBlendID"]);
                }
            }
            return CoffeeBlendID;
        }
        public int GetUOMID(string UOM)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT UOMID FROM [BUSDTA].tblUOM WHERE UOMLable='" + UOM + "'");
            int UOMID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    UOMID = Convert.ToInt32(CompetitiorDataRow["UOMID"]);
                }

            }

            return UOMID;

        }
        public int GetLiqCoffees(string liqCoffee)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT LiqCoffeeTypeID FROM [BUSDTA].tblLiqCoffeeType WHERE LiqCoffeeType='" + liqCoffee + "'");
            int LiqCoffeeTypeID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    LiqCoffeeTypeID = Convert.ToInt32(CompetitiorDataRow["LiqCoffeeTypeID"]);
                }

            }

            return LiqCoffeeTypeID;

        }
        public int GetPackSize(string packsize)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT PacketSizeID FROM [BUSDTA].tblPacketSize WHERE Size='" + packsize + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["PacketSizeID"]);
                }

            }

            return ID;
        }
        public int GetCSPKLB(string cspklb)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CSPKLBID FROM [BUSDTA].tblCSPKLB WHERE CSPKLBType='" + cspklb + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["CSPKLBID"]);
                }

            }

            return ID;
        }
        public int GetCoffeeVolume(string coffeevolume)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CoffeeVolumeID FROM [BUSDTA].tblCoffeeVolume WHERE Volume='" + coffeevolume + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["CoffeeVolumeID"]);
                }

            }

            return ID;
        }
        public int GetCoffeeType(string coffeetype)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT LiqCoffeeTypeID FROM [BUSDTA].tblLiqCoffeeType WHERE LiqCoffeeType='" + coffeetype + "'");
            int LiqCoffeeTypeID = 0;
            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    LiqCoffeeTypeID = Convert.ToInt32(CompetitiorDataRow["LiqCoffeeTypeID"]);
                }

            }

            return LiqCoffeeTypeID;
        }

        public int RemoveNewCoffeeProspect(CoffeeProspect coffeeProspect)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:RemoveNewCoffeeProspect]");
            int result = 0;
            try
            {
                string qry = @"DELETE FROM [BUSDTA].tblCoffeeProspect WHERE CoffeeProspectID='" + coffeeProspect.CoffeeProspectID + "'";
                DbEngine.ExecuteNonQuery(qry);

                result = 1;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][RemoveNewCoffeeProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        #endregion


        #region Allied
        public List<string> GetAlliedCompetitiors()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CompetitorID, Competitor FROM [BUSDTA].tblCompetitor");
            List<string> listCompetitors = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Competitor = Convert.ToString(CompetitiorDataRow["Competitor"]);

                    listCompetitors.Add(Competitor);
                }

            }

            return listCompetitors;

        }
        public List<string> GetAlliedCategory()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CategoryID, Category FROM [BUSDTA].tblAlliedProspCategory");
            List<string> listAlliedBlend = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Competitor = Convert.ToString(CompetitiorDataRow["Category"]);

                    listAlliedBlend.Add(Competitor);
                }

            }

            return listAlliedBlend;

        }
        public List<string> GetAlliedSubCategory(string CategorySelected)
        {
            DataSet queryResult = new DataSet();
            if (string.IsNullOrWhiteSpace(CategorySelected))
            {
                queryResult = DbEngine.ExecuteDataSet(@"SELECT SubCategoryID, SubCategory, CategoryID FROM [BUSDTA].tblAlliedProspSubcategory ");
            }
            else
            {
                queryResult = DbEngine.ExecuteDataSet(@"SELECT SubCategoryID, SubCategory, CategoryID FROM [BUSDTA].tblAlliedProspSubcategory WHERE CategoryID IN(SELECT CategoryID FROM [BUSDTA].tblAlliedProspCategory WHERE Category='" + CategorySelected + "')");
            }
            List<string> listAlliedBlend = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Competitor = Convert.ToString(CompetitiorDataRow["SubCategory"]);

                    listAlliedBlend.Add(Competitor);
                }

            }

            return listAlliedBlend;

        }
        public List<string> GetAlliedBrand()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT BrandID, BrandLable BrandName FROM [BUSDTA].tblBrandLable");
            List<string> listUOM = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string UOM = Convert.ToString(CompetitiorDataRow["BrandName"]);

                    listUOM.Add(UOM);
                }

            }

            return listUOM;

        }
        public List<string> GetAlliedPackSize()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT PacketSizeID, Size FROM [BUSDTA].tblPacketSize");
            List<string> listPackSize = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Size = Convert.ToString(CompetitiorDataRow["Size"]);

                    listPackSize.Add(Size);
                }

            }

            return listPackSize;
        }
        public List<string> GetAlliedCsPkLb()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CSPKLBID, CSPKLBType FROM [BUSDTA].tblCSPKLB");
            List<string> listcspklb = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string CSPKLBType = Convert.ToString(CompetitiorDataRow["CSPKLBType"]);

                    listcspklb.Add(CSPKLBType);
                }

            }

            return listcspklb;

        }
        public List<string> GetAlliedVolumes()
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CoffeeVolumeID, Volume FROM [BUSDTA].tblCoffeeVolume");
            List<string> listAlliedVolume = new List<string>();

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    string Volume = Convert.ToString(CompetitiorDataRow["Volume"]);

                    listAlliedVolume.Add(Volume);
                }

            }

            return listAlliedVolume;
        }

        public int AddNewAlliedProspect(AlliedProspect AlliedProspect, string routeID, string prospectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddNewProspect]");
            int result = 0; string qry = string.Empty;
            try
            {

                AlliedProspect.CompetitorID = GetAlliedCompetitiorID(AlliedProspect.AlliedCompetitorSelected);
                AlliedProspect.CategoryID = GetAlliedCategoryID(AlliedProspect.AlliedCategorySelected);
                AlliedProspect.SubCategoryID = GetAlliedSubCategoryID(AlliedProspect.AlliedSubCategorySelected);
                AlliedProspect.BrandID = GetAlliedBrandID(AlliedProspect.AlliedBrandSelected);
                AlliedProspect.PackSize = GetAlliedPackSize(AlliedProspect.AlliedPackSizeSelected);
                AlliedProspect.CS_PK_LB = GetAlliedCSPKLB(AlliedProspect.AlliedCSPKLBSelected);
                AlliedProspect.AverageVolumeID = GetAlliedVolume(AlliedProspect.AlliedVolumeSelected);
                AlliedProspect.UpdatedBy = Convert.ToInt32(PayloadManager.ApplicationPayload.LoggedInUserID);
                if (AlliedProspect.AlliedProspectID == 0)
                {
                    if (!string.IsNullOrEmpty(AlliedProspect.AlliedCompetitorSelected) && !string.IsNullOrEmpty(AlliedProspect.AlliedCategorySelected) && !string.IsNullOrEmpty(AlliedProspect.AlliedSubCategorySelected))// && !IsAlliedProspectAlreadyExists(prospectID, AlliedProspect.AlliedCompetitorSelected)
                    {
                        AlliedProspect.AlliedProspectID = Convert.ToInt32(GetNewAlliedProspectSeries(prospectID));
                        //AlliedProspect.AlliedProspectID = AlliedProspect.AlliedProspectID.ToString().Length == 1 ? Convert.ToInt32(routeID.Trim().Substring(3, routeID.Length - 3) + GetNewAlliedProspectSeries(prospectID)) : Convert.ToInt32(GetNewAlliedProspectSeries(prospectID));

                        qry = @"INSERT INTO [BUSDTA].tblAlliedProspect (AlliedProspectID, ProspectID, CompetitorID, CategoryID, SubCategoryID, BrandID,  PackSize, CS_PK_LB, Price, UsageMeasurementID, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) VALUES ("
                                                    + AlliedProspect.AlliedProspectID.ToString() + ","
                                                    + prospectID.ToString() + ","
                                                    + AlliedProspect.CompetitorID.ToString() + ","
                                                    + AlliedProspect.CategoryID.ToString() + ","
                                                    + AlliedProspect.SubCategoryID.ToString() + ","
                                                    + AlliedProspect.BrandID.ToString() + ","
                                                    + AlliedProspect.PackSize.ToString() + ","
                                                    + AlliedProspect.CS_PK_LB.ToString() + ","
                                                    + AlliedProspect.Price.ToString() + ","
                                                    + AlliedProspect.AverageVolumeID.ToString() + ","
                                                    + AlliedProspect.UpdatedBy.ToString() + ", GETDATE(),"
                                                    + AlliedProspect.UpdatedBy.ToString() + ", GETDATE())";
                    }
                }
                else if (AlliedProspect.AlliedProspectID > 0)
                {

                    qry = @"UPDATE [BUSDTA].tblAlliedProspect
                    SET ProspectID =" + prospectID.ToString() + ", CompetitorID =" + AlliedProspect.CompetitorID.ToString() + ", CategoryID =" + AlliedProspect.CategoryID.ToString() + ", SubCategoryID =" + AlliedProspect.SubCategoryID.ToString() + ", BrandID =" + AlliedProspect.BrandID.ToString() + ", PackSize =" + AlliedProspect.PackSize.ToString() + ", CS_PK_LB =" + AlliedProspect.CS_PK_LB.ToString() + ", Price =" + AlliedProspect.Price.ToString() + ", UsageMeasurementID =" + AlliedProspect.AverageVolumeID.ToString() + ", UpdatedBy =" + AlliedProspect.UpdatedBy.ToString() + ", UpdatedDatetime = GETDATE() WHERE AlliedProspectID =" + AlliedProspect.AlliedProspectID;
                }
                if (!string.IsNullOrEmpty(qry))
                {
                    DbEngine.ExecuteNonQuery(qry);
                    result = 1;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public bool IsAlliedProspectAlreadyExists(string prospectID, string competitor)
        {
            bool result = false;
            try
            {
                string qryExist = @"SELECT COUNT(1) FROM [BUSDTA].tblAlliedProspect WHERE ProspectID ='" + prospectID + "' AND CompetitorID IN (SELECT CompetitorID FROM [BUSDTA].tblCompetitor WHERE Competitor='" + competitor + "') ";
                result = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(qryExist)));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][IsProspectAlreadyExists][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public int GetNewAlliedProspectSeries(string prospectID)
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar(@"SELECT ISNULL(MAX(AlliedProspectID),0)+1 NewAlliedProspectID FROM [BUSDTA].tblAlliedProspect"));
        }

        public ObservableCollection<AlliedProspectVM> GetAlliedProspectForRoute(string prospectID)
        {
            DateTime date = DateTime.Today;
            ObservableCollection<AlliedProspectVM> AlliedProspectCollection = new ObservableCollection<AlliedProspectVM>();

            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetProspectsForRoute][prospectID=" + prospectID + "]");
            try
            {
                DataSet queryResult = DbEngine.ExecuteDataSet(@"
                        SELECT cp.ProspectID, cp.AlliedProspectID, 
                        com.CompetitorID, com.Competitor, 
                        cate.CategoryID, cate.Category, 
                        scat.SubCategoryID, scat.SubCategory,
                        br.BrandID, br.BrandLable BrandName,
                        pack.PacketSizeID, pack.Size PackSize, 
                        cp.CS_PK_LB CSPKLBID, cspklb.CSPKLBType CS_PK_LB, 
                        Price, cp.UsageMeasurementID, vol.Volume UsageMeasurement, 
                        cr.App_User CreatedBy, cp.CreatedDatetime, 
                        upd.App_User UpdatedBy , cp.UpdatedDatetime
                        FROM [BUSDTA].tblAlliedProspect AS cp
                        LEFT JOIN [BUSDTA].tblCompetitor AS com ON cp.CompetitorID=com.CompetitorID
                        LEFT JOIN [BUSDTA].tblAlliedProspCategory AS cate ON cp.CategoryID=cate.CategoryID
                        LEFT JOIN [BUSDTA].tblAlliedProspSubcategory AS scat ON cp.SubCategoryID=scat.SubCategoryID
                        LEFT JOIN [BUSDTA].tblBrandLable AS br ON cp.BrandID=br.BrandID
                        LEFT JOIN [BUSDTA].tblPacketSize AS pack ON cp.PackSize=pack.PacketSizeID
                        LEFT JOIN [BUSDTA].tblCSPKLB AS cspklb ON cp.CS_PK_LB=cspklb.CSPKLBID
                        LEFT JOIN [BUSDTA].tblCoffeeVolume AS vol ON cp.UsageMeasurementID=vol.CoffeeVolumeID
                        LEFT JOIN BUSDTA.user_master AS cr ON cp.CreatedBy=cr.app_user_id
                        LEFT JOIN BUSDTA.user_master AS upd ON cp.CreatedBy=upd.app_user_id
                        WHERE cp.ProspectID='" + prospectID + "'");

                if (queryResult.HasData())
                {
                    AlliedProspectVM AlliedProspect;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                        AlliedProspect = new AlliedProspectVM
                        {
                            AlliedProspectID = Convert.ToInt32(customerDataRow["AlliedProspectID"]),
                            CompetitorID = Convert.ToInt32(customerDataRow["CompetitorID"]),
                            Competitor = customerDataRow["Competitor"].ToString(),
                            CategoryID = Convert.ToInt32(customerDataRow["CategoryID"]),
                            Category = customerDataRow["Category"].ToString(),
                            SubCategoryID = Convert.ToInt32(customerDataRow["SubCategoryID"]),
                            SubCategory = customerDataRow["SubCategory"].ToString(),
                            BrandID = Convert.ToInt32(customerDataRow["BrandID"]),
                            Brand = customerDataRow["BrandName"].ToString(),
                            PackSize = Convert.ToInt32(customerDataRow["PacketSizeID"]),
                            PackSizeText = customerDataRow["PackSize"].ToString(),
                            CS_PK_LB = Convert.ToInt32(customerDataRow["CSPKLBID"]),
                            CSPKLBText = customerDataRow["CS_PK_LB"].ToString(),
                            Price = Convert.ToInt32(customerDataRow["Price"]),
                            AverageVolumeID = Convert.ToInt32(customerDataRow["UsageMeasurementID"]),
                            AverageVolume = Convert.ToString(customerDataRow["UsageMeasurement"]),
                            CreatedUser = customerDataRow["CreatedBy"].ToString(),
                            CreatedDatetimeText = Convert.ToDateTime(customerDataRow["CreatedDatetime"]).ToString("MM/dd/yyyy hh.mm tt"),
                            UpdatedUser = customerDataRow["UpdatedBy"].ToString(),
                            UpdatedDatetimeText = Convert.ToDateTime(customerDataRow["UpdatedDatetime"]).ToString("MM/dd/yyyy hh.mm tt")
                        };

                        AlliedProspectCollection.Add(AlliedProspect);
                    }


                }

                Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetProspectsForRoute][prospectID=" + prospectID + "]");
                return AlliedProspectCollection;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetProspectsForRoute][prospectID=" + prospectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return AlliedProspectCollection;
            }
        }

        public int GetAlliedCompetitiorID(string competitor)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CompetitorID FROM [BUSDTA].tblCompetitor WHERE Competitor='" + competitor + "'");
            int CompetitorID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    CompetitorID = Convert.ToInt32(CompetitiorDataRow["CompetitorID"]);
                }
            }
            return CompetitorID;
        }
        public int GetAlliedCategoryID(string category)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CategoryID FROM [BUSDTA].tblAlliedProspCategory WHERE Category='" + category + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["CategoryID"]);
                }
            }
            return ID;
        }
        public int GetAlliedSubCategoryID(string subcategory)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT SubCategoryID FROM [BUSDTA].tblAlliedProspSubcategory WHERE SubCategory='" + subcategory + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["SubCategoryID"]);
                }
            }
            return ID;
        }
        public int GetAlliedBrandID(string brand)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT BrandID FROM [BUSDTA].tblBrandLable WHERE BrandLable='" + brand + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["BrandID"]);
                }
            }
            return ID;
        }
        public int GetAlliedPackSize(string packsize)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT PacketSizeID FROM [BUSDTA].tblPacketSize WHERE Size='" + packsize + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["PacketSizeID"]);
                }

            }

            return ID;
        }
        public int GetAlliedCSPKLB(string cspklb)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CSPKLBID FROM [BUSDTA].tblCSPKLB WHERE CSPKLBType='" + cspklb + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["CSPKLBID"]);
                }

            }

            return ID;
        }
        public int GetAlliedVolume(string avgvolume)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT CoffeeVolumeID FROM [BUSDTA].tblCoffeeVolume WHERE Volume='" + avgvolume + "'");
            int ID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow CompetitiorDataRow = queryResult.Tables[0].Rows[index - 1];
                    ID = Convert.ToInt32(CompetitiorDataRow["CoffeeVolumeID"]);
                }

            }

            return ID;
        }

        public int RemoveAlliedProspect(AlliedProspect AlliedProspect)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:RemoveNewAlliedProspect]");
            int result = 0;
            try
            {
                string qry = @"DELETE FROM [BUSDTA].tblAlliedProspect WHERE AlliedProspectID='" + AlliedProspect.AlliedProspectID + "'";
                DbEngine.ExecuteNonQuery(qry);

                result = 1;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][RemoveNewAlliedProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        #endregion
        #endregion

        #region Serialized

        public int AddNewSerialized(Prospect prospect, string routeID, string ProspectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddNewProspect]");
            int result = 0;
            int EqOwned;
            try
            {
                string EquipmentType = "Serialized Equipment";
                prospect.EquipmentID = Convert.ToInt32(GetNewSerializedProspectSeries(ProspectID));
                //prospect.EquipmentID = prospect.EquipmentID.ToString().Length == 1 ? Convert.ToInt32(routeID.Trim().Substring(3, routeID.Length - 3) + GetNewSerializedProspectSeries(ProspectID)) : Convert.ToInt32(GetNewSerializedProspectSeries(ProspectID));

                prospect.EquipmentCategoryID = GetSerializedID(prospect.EquipmentSelected);
                string CreatedBy = (PayloadManager.ApplicationPayload.LoggedInUserID);
                if (prospect.OwnedSelected.Trim() == "Yes")
                {
                    EqOwned = 1;
                }
                else
                {
                    EqOwned = 0;
                }
                if (prospect.EquipmentSelectedID > 0)
                {
                    string qry = @" UPDATE [BUSDTA].TBLEQUIPMENT SET EQUIPMENTQUANTITY ='" + prospect.QuantitySelected + "',EquipmentOwned='" + EqOwned + "',UpdatedBy='" + CreatedBy + "',UpdatedDatetime=getdate() WHERE EQUIPMENTID='" + prospect.EquipmentSelectedID + "'AND EQUIPMENTCATEGORYID='" + prospect.EquipmentCategoryID + "' AND EQUIPMENTTYPE='" + EquipmentType + "'";
                    DbEngine.ExecuteNonQuery(qry);
                    result = 1;
                }
                else
                {
                    if (!IsEquipmentAlreadyExists(ProspectID, EquipmentType, prospect.EquipmentSelected, prospect.SubCategorySelected))
                    {
                        string query = (@"INSERT INTO [BUSDTA].TBLEQUIPMENT (EquipmentID, ProspectID,EquipmentType,EquipmentCategoryID,EquipmentSubCategory,EquipmentQuantity,EquipmentOwned,CreatedBy,CreatedDateTime,UpdatedBy,UpdatedDatetime)
                                VALUES  (" + prospect.EquipmentID + "," + ProspectID + "," + "'" + EquipmentType + "'" + "," + prospect.EquipmentCategoryID + "," + "'" + prospect.SubCategorySelected + "'" + ","
                                                + prospect.QuantitySelected + "," + EqOwned + "," + "'" + CreatedBy + "'" + "," + "Getdate()" + "," + "'" + CreatedBy + "'" + "," + "Getdate())");
                        DbEngine.ExecuteNonQuery(query);
                        result = 1;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public int GetNewSerializedProspectSeries(string prospectID)
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar(@"SELECT ISNULL(MAX(EquipmentID),0)+1 EquipmentID FROM [BUSDTA].tblEquipment WHERE ProspectID='" + prospectID + "'"));
        }

        public ProspectContact GetDefaultContact(int prospectid)
        {
            var ContactCollection = new ObservableCollection<ProspectContact>();
            var defaultContact = new ProspectContact();
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:GetDefaultContact]");
            try
            {
                var s_query = @"select * from busdta.Prospect_Contact where RouteId = " + CommonNavInfo.RouteID + " and prospectid=" + prospectid + "";
                var dbset = DbEngine.ExecuteDataSet(s_query);
                if (dbset.HasData())
                {
                    foreach (DataRow row in dbset.Tables[0].Rows)
                    {
                        var prospectContact = new ProspectContact();
                        string firstName = row["FirstName"].ToString().Trim();
                        prospectContact.FirstName = textInfo.ToTitleCase(firstName.ToLower());
                        string middleName = row["MiddleName"].ToString().Trim();
                        prospectContact.MiddleName = textInfo.ToTitleCase(middleName.ToLower());
                        string lastName = row["LastName"].ToString().Trim();
                        prospectContact.LastName = textInfo.ToTitleCase(lastName.ToLower());
                        prospectContact.ContactName = prospectContact.FirstName + " " + prospectContact.MiddleName + " " + prospectContact.LastName;

                        prospectContact.ContactID = row["ProspectContactId"].ToString();
                        prospectContact.ProspectID = row["ProspectId"].ToString();


                        prospectContact.EmailList = new ObservableCollection<Email>();
                        if (!string.IsNullOrEmpty(row["EmailID1"].ToString()))
                        {
                            prospectContact.EmailList.Add(new Email { Index = 1, ContactID = row["ProspectContactId"].ToString(), Type = row["EmailType1"].ToString().Trim(), Value = row["EmailID1"].ToString(), IsDefault = !string.IsNullOrEmpty(row["IsDefaultEmail1"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsDefaultEmail1"].ToString())) : false });
                        }
                        if (!string.IsNullOrEmpty(row["EmailID2"].ToString()))
                        {
                            prospectContact.EmailList.Add(new Email { Index = 2, ContactID = row["ProspectContactId"].ToString(), Type = row["EmailType2"].ToString().Trim(), Value = row["EmailID2"].ToString(), IsDefault = !string.IsNullOrEmpty(row["IsDefaultEmail2"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsDefaultEmail2"].ToString())) : false });
                        }
                        if (!string.IsNullOrEmpty(row["EmailID3"].ToString()))
                        {
                            prospectContact.EmailList.Add(new Email { Index = 3, ContactID = row["ProspectContactId"].ToString(), Type = row["EmailType3"].ToString().Trim(), Value = row["EmailID3"].ToString(), IsDefault = !string.IsNullOrEmpty(row["IsDefaultEmail3"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsDefaultEmail3"].ToString())) : false });
                        }
                        //prospectContact.EmailList.Add(new Email { Type = row["CDETP3"].ToString(), Value = row["CDEMAL4"].ToString(), IsDefault = !string.IsNullOrEmpty(row["CDDFLTEM4"].ToString()) ? Convert.ToBoolean(row["CDDFLTEM4"].ToString()) : false });
                        if (prospectContact.EmailList.Count > 0)
                        {
                            prospectContact.DefaultEmail = prospectContact.EmailList.FirstOrDefault(em => em.IsDefault == true);
                        }
                        prospectContact.PhoneList = new ObservableCollection<Phone>();


                        if (!string.IsNullOrEmpty(row["Phone1"].ToString()))
                        {
                            prospectContact.PhoneList.Add(new Phone { Index = 1, ContactID = row["ProspectContactId"].ToString(), Type = row["PhoneType1"].ToString().Trim(), Value = row["Phone1"].ToString().Trim(), Extension = row["Extn1"].ToString().Trim(), AreaCode = row["AreaCode1"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["IsDefaultPhone1"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsDefaultPhone1"].ToString())) : false });
                        }
                        if (!string.IsNullOrEmpty(row["Phone2"].ToString()))
                        {
                            prospectContact.PhoneList.Add(new Phone { Index = 2, ContactID = row["ProspectContactId"].ToString(), Type = row["PhoneType2"].ToString().Trim(), Value = row["Phone2"].ToString().Trim(), Extension = row["Extn2"].ToString().Trim(), AreaCode = row["AreaCode2"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["IsDefaultPhone2"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsDefaultPhone2"].ToString())) : false });
                        }
                        if (!string.IsNullOrEmpty(row["Phone3"].ToString()))
                        {
                            prospectContact.PhoneList.Add(new Phone { Index = 3, ContactID = row["ProspectContactId"].ToString(), Type = row["PhoneType3"].ToString().Trim(), Value = row["Phone3"].ToString().Trim(), Extension = row["Extn3"].ToString().Trim(), AreaCode = row["AreaCode3"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["IsDefaultPhone3"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsDefaultPhone3"].ToString())) : false });
                        }
                        if (!string.IsNullOrEmpty(row["Phone4"].ToString()))
                        {
                            prospectContact.PhoneList.Add(new Phone { Index = 4, ContactID = row["ProspectContactId"].ToString(), Type = row["PhoneType4"].ToString().Trim(), Value = row["Phone4"].ToString().Trim(), Extension = row["Extn4"].ToString().Trim(), AreaCode = row["AreaCode4"].ToString().Trim(), IsDefault = !string.IsNullOrEmpty(row["IsDefaultPhone4"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsDefaultPhone4"].ToString())) : false });
                        }
                        if (prospectContact.PhoneList.Count > 0)
                        {
                            prospectContact.DefaultPhone = prospectContact.PhoneList.FirstOrDefault(em => em.IsDefault == true);
                        }
                        prospectContact.ContactTitle = row["Title"] == null ? "" : row["Title"].ToString().Trim();


                        prospectContact.IsActive = !string.IsNullOrEmpty(row["IsActive"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["IsActive"].ToString())) : false;
                        prospectContact.ShowOnDashboard = !string.IsNullOrEmpty(row["ShowOnDashboard"].ToString()) ? Convert.ToBoolean(Convert.ToInt32(row["ShowOnDashboard"].ToString())) : false;
                        prospectContact.IsExpanded = prospectContact.ShowOnDashboard;
                        //prospectContact.IsDefault = row["CDIDLN"].ToString().Trim().Equals("0") ? true : false;

                        if (!string.IsNullOrEmpty(prospectContact.FirstName.Trim()))
                        {
                            ContactCollection.Add(prospectContact);
                        }
                    }
                }

                defaultContact = ContactCollection.FirstOrDefault(x => x.ShowOnDashboard);
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][GetDefaultContact][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][End:GetDefaultContact]");
            return defaultContact;
        }

        public int RemoveSerializedProspect(Prospect serializedProspect)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:RemoveSerializedProspect]");
            int result = 0;
            try
            {
                string qry = @"DELETE FROM [BUSDTA].TBLEQUIPMENT WHERE EquipmentID='" + serializedProspect.EquipmentID + "'";
                DbEngine.ExecuteNonQuery(qry);

                result = 1;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][RemoveNewCoffeeProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        public int RemoveExpensedProspect(Prospect expensedProspect)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:RemoveExpensedProspect]");
            int result = 0;
            try
            {
                string qry = @"DELETE FROM [BUSDTA].TBLEQUIPMENT WHERE EquipmentID='" + expensedProspect.EquipmentID + "'";
                DbEngine.ExecuteNonQuery(qry);

                result = 1;

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][RemoveNewCoffeeProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public int AddNewExpensed(Prospect prospect, string routeID, string ProspectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddNewExpensed]");
            int result = 0;
            int EqOwned;
            try
            {
                prospect.EquipmentCategoryID = GetSerializedID(prospect.EquipmentSelected);
                string EquipmentType = "Expensed Equipment";
                prospect.EquipmentID = Convert.ToInt32(GetNewSerializedProspectSeries(ProspectID));
                //prospect.EquipmentID = prospect.EquipmentID.ToString().Length == 1 ? Convert.ToInt32(routeID.Trim().Substring(3, routeID.Length - 3) + GetNewSerializedProspectSeries(ProspectID)) : Convert.ToInt32(GetNewSerializedProspectSeries(ProspectID));

                string CreatedBy = (PayloadManager.ApplicationPayload.LoggedInUserID);
                if (prospect.OwnedSelected.Trim() == "Yes")
                {
                    EqOwned = 1;
                }
                else
                {
                    EqOwned = 0;
                }
                if (prospect.EquipmentSelectedID > 0)
                {
                    string qry = @" UPDATE [BUSDTA].TBLEQUIPMENT SET EQUIPMENTQUANTITY ='" + prospect.QuantitySelected + "',EquipmentOwned='" + EqOwned + "',UpdatedBy='" + CreatedBy + "',UpdatedDateTime=getdate() WHERE EQUIPMENTID='" + prospect.EquipmentSelectedID + "'AND EQUIPMENTCATEGORYID='" + prospect.EquipmentCategoryID + "' AND EQUIPMENTTYPE='" + EquipmentType + "'";
                    DbEngine.ExecuteNonQuery(qry);
                    result = 1;
                }
                else
                {
                    if (!IsExpensedAlreadyExists(ProspectID, EquipmentType, prospect.EquipmentSelected, prospect.SubCategorySelected))
                    {
                        string query = (@"INSERT INTO [BUSDTA].TBLEQUIPMENT (EquipmentID, ProspectID,EquipmentType,EquipmentCategoryID,EquipmentSubCategory,EquipmentQuantity,EquipmentOwned,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)
                                VALUES  (" + prospect.EquipmentID + "," + ProspectID + "," + "'" + EquipmentType + "'" + "," + prospect.EquipmentCategoryID + "," + "'" + prospect.SubCategorySelected + "'" + ","
                                              + prospect.QuantitySelected + "," + EqOwned + "," + "'" + CreatedBy + "'" + "," + "Getdate()" + "," + "'" + CreatedBy + "'" + "," + "Getdate())");
                        DbEngine.ExecuteNonQuery(query);
                        result = 1;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }


        public int GetSerializedID(string EquipmentCategory)
        {
            DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT EquipmentCategoryID FROM [BUSDTA].tblEquipmentCategory WHERE LTRIM(RTRIM(EquipmentCategory)) ='" + EquipmentCategory + "'");
            int EquipmentCategoryID = 0;

            if (queryResult.HasData())
            {
                for (var index = 1; index
                                    <= queryResult.Tables[0].Rows.Count; index++)
                {
                    DataRow EquipmentDataRow = queryResult.Tables[0].Rows[index - 1];
                    EquipmentCategoryID = Convert.ToInt32(EquipmentDataRow["EquipmentCategoryID"]);
                }
            }
            return EquipmentCategoryID;
        }

        public bool IsEquipmentAlreadyExists(string ProspectID, string EquipmentType, string EquipmentSelected, string SubCategorySelected)
        {
            bool result = false;
            try
            {
                string qryExist = @"SELECT COUNT(1) FROM [BUSDTA].tblEquipment E 
                                    INNER JOIN [BUSDTA].TBLEQUIPMENTCATEGORY EC ON EC.EQUIPMENTCATEGORYID = E.EQUIPMENTCATEGORYID 
                                    WHERE ProspectID ='" + ProspectID + "' AND  LTRIM(RTRIM(EC.EquipmentCategory)) ='" + EquipmentSelected + "' AND  LTRIM(RTRIM(E.EquipmentSubCategory)) ='" + SubCategorySelected + "' AND E.EQUIPMENTTYPE='" + EquipmentType + "'";
                result = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(qryExist)));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][IsProspectAlreadyExists][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public bool IsExpensedAlreadyExists(string ProspectID, string EquipmentType, string EquipmentCategory, string SubCategorySelected)
        {
            bool result = false;
            try
            {
                string qryExist = @"SELECT COUNT(1) FROM [BUSDTA].tblEquipment E 
                                        INNER JOIN [BUSDTA].TBLEQUIPMENTCATEGORY EC ON EC.EQUIPMENTCATEGORYID = E.EQUIPMENTCATEGORYID 
                                        WHERE ProspectID ='" + ProspectID + "' AND  LTRIM(RTRIM(EC.EquipmentCategory)) ='" + EquipmentCategory + "' AND  LTRIM(RTRIM(E.EquipmentSubCategory)) ='" + SubCategorySelected + "' AND E.EQUIPMENTTYPE='" + EquipmentType + "'";
                result = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(qryExist)));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][IsProspectAlreadyExists][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public ObservableCollection<Prospect> GetEquipmentDetails(string ProspectID)
        {
            DateTime date = DateTime.Today;

            ObservableCollection<Prospect> EquipmentCollection = new ObservableCollection<Prospect>();

            try
            {
                DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT EQ.EquipmentID,EQ.EquipmentCategoryID, EQC.EQUIPMENTCATEGORY,EQ.EQUIPMENTSUBCATEGORY,EQ.EQUIPMENTQUANTITY,EQ.EQUIPMENTOWNED,EQ.UpdatedDatetime,UM.APP_USER UpdatedBy 
                                                                FROM [BUSDTA].tblEquipment EQ
                                                                INNER JOIN [BUSDTA].tblEquipmentCategory EQC ON EQC.EquipmentCategoryID = EQ.EquipmentCategoryID
                                                                LEFT JOIN BUSDTA.USER_MASTER UM ON UM.APP_USER_ID = EQ.UpdatedBy
                                                                WHERE EQ.PROSPECTID ='" + ProspectID + "' and  EQ.EquipmentType = 'Serialized Equipment'");
                // To Add EquipmentTypeID in Where Class EQC.EquipmentTypeID=1 
                if (queryResult.HasData())
                {
                    Prospect prospect;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow EquipmentDataRow = queryResult.Tables[0].Rows[index - 1];
                        prospect = new Prospect
                        {
                            EquipmentID = Convert.ToInt32(EquipmentDataRow["EquipmentID"]),
                            EquipmentCategoryID = Convert.ToInt32(EquipmentDataRow["EquipmentCategoryID"]),
                            EquipmentCategory = EquipmentDataRow["EquipmentCategory"].ToString(),
                            EquipmentSubCategory = EquipmentDataRow["EquipmentSubCategory"].ToString(),
                            EquipmentQuantity = Convert.ToInt32(EquipmentDataRow["EquipmentQuantity"]),
                            EquipmentOwned = Convert.ToBoolean(EquipmentDataRow["EquipmentOwned"]) == true ? "Yes" : "No",
                            ModifiedBy = EquipmentDataRow["UpdatedBy"].ToString(),
                            CreatedOn = Convert.ToDateTime(EquipmentDataRow["UpdatedDatetime"]),
                        };
                        EquipmentCollection.Add(prospect);
                    }
                }

                Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetProspectsForRoute][routeID=" + ProspectID + "]");
                return EquipmentCollection;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetProspectsForRoute][routeID=" + ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return EquipmentCollection;
            }
        }

        public ObservableCollection<Prospect> GetExpensedDetails(string ProspectID)
        {
            DateTime date = DateTime.Today;

            ObservableCollection<Prospect> EquipmentCollection = new ObservableCollection<Prospect>();

            try
            {
                DataSet queryResult = DbEngine.ExecuteDataSet(@"SELECT EQ.EquipmentID,EQ.EquipmentCategoryID, EQC.EQUIPMENTCATEGORY,EQ.EQUIPMENTSUBCATEGORY,EQ.EQUIPMENTQUANTITY,EQ.EQUIPMENTOWNED,EQ.UpdatedDatetime,UM.APP_USER UpdatedBy 
                                                                FROM [BUSDTA].tblEquipment EQ
                                                                INNER JOIN [BUSDTA].tblEquipmentCategory EQC ON EQC.EquipmentCategoryID = EQ.EquipmentCategoryID
                                                                LEFT JOIN BUSDTA.USER_MASTER UM ON UM.APP_USER_ID = EQ.UpdatedBy
                                                                WHERE EQ.PROSPECTID ='" + ProspectID + "' and  EQ.EquipmentType = 'Expensed Equipment'");
                // To Add EquipmentTypeID in Where Class EQC.EquipmentTypeID=1 
                if (queryResult.HasData())
                {
                    Prospect prospect;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow EquipmentDataRow = queryResult.Tables[0].Rows[index - 1];
                        prospect = new Prospect
                        {
                            EquipmentID = Convert.ToInt32(EquipmentDataRow["EquipmentID"]),
                            EquipmentCategoryID = Convert.ToInt32(EquipmentDataRow["EquipmentCategoryID"]),
                            EquipmentCategory = EquipmentDataRow["EquipmentCategory"].ToString(),
                            EquipmentSubCategory = EquipmentDataRow["EquipmentSubCategory"].ToString(),
                            EquipmentQuantity = Convert.ToInt32(EquipmentDataRow["EquipmentQuantity"]),
                            EquipmentOwned = Convert.ToBoolean(EquipmentDataRow["EquipmentOwned"]) == true ? "Yes" : "No",
                            ModifiedBy = EquipmentDataRow["UpdatedBy"].ToString(),
                            CreatedOn = Convert.ToDateTime(EquipmentDataRow["UpdatedDatetime"]),
                        };
                        EquipmentCollection.Add(prospect);
                    }
                }

                Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetProspectsForRoute][routeID=" + ProspectID + "]");
                return EquipmentCollection;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetProspectsForRoute][routeID=" + ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return EquipmentCollection;
            }
        }
        #endregion

        #region Prospect Contacts
        public List<string> GetPhoneTypeList()
        {
            DateTime timer = DateTime.Now;
            List<string> CategoryList = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT * FROM BUSDTA.M080111 where CTTYP = 'PHONE'");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        CategoryList.Add(dataRow["CTDSC1"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetPhoneTypeList][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return CategoryList;
        }

        public List<string> GetEmailTypeList()
        {
            DateTime timer = DateTime.Now;
            List<string> CategoryList = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT * FROM BUSDTA.M080111 where CTTYP = 'EMAIL'");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        CategoryList.Add(dataRow["CTDSC1"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetEmailTypeList][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return CategoryList;
        }

        public List<string> GetEditedPhoneType(int ProspectID, string SelectedContactID, int Index)
        {
            DateTime timer = DateTime.Now;
            List<string> PhoneType = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT B.CTDSC1 FROM BUSDTA.Prospect_Contact A INNER JOIN BUSDTA.M080111 B ON A.PhoneType" + Index + "= B.CTID WHERE A.ProspectContactId= '" + SelectedContactID + "' AND ProspectId = '" + ProspectID + "'");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        PhoneType.Add(dataRow["CTDSC1"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetPhoneTypeList][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return PhoneType;
        }

        public string GetEditedEmailType(int ProspectID, string SelectedContactID, int Index)
        {
            DateTime timer = DateTime.Now;
            string EmailType = string.Empty;
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT B.CTDSC1 FROM BUSDTA.Prospect_Contact A INNER JOIN BUSDTA.M080111 B ON A.EmailType" + Index + "= B.CTID WHERE A.ProspectContactId= '" + SelectedContactID + "' AND ProspectId = '" + ProspectID + "'");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        EmailType = dataRow["CTDSC1"].ToString().Trim();
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetPhoneTypeList][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return EmailType;
        }

        #region GetProspectContactList
        public ObservableCollection<ContactProspect> GetProspectContactList(string ProspectID)
        {
            DateTime date = DateTime.Today;
            ObservableCollection<ContactProspect> ContactCollection = new ObservableCollection<ContactProspect>();
            try
            {
                string qry = "SELECT ProspectId, ProspectContactId, "
                            + "AreaCode1, Phone1, Extn1, PhoneType1, IsDefaultPhone1, "
                            + "AreaCode2, Phone2, Extn2, PhoneType2, IsDefaultPhone2, "
                            + "AreaCode3, Phone3, Extn3, PhoneType3, IsDefaultPhone3, "
                            + "AreaCode4, Phone4, Extn4, PhoneType4, IsDefaultPhone4, "
                            + "EmailID1, EmailType1, IsDefaultEmail1, "
                            + "EmailID2, EmailType2, IsDefaultEmail2, "
                            + "EmailID3, EmailType3, IsDefaultEmail3, "
                            + "FirstName, Title, IsActive, ShowOnDashboard, "
                            + "LTRIM(RTRIM(PC1.CTDSC1)) AS PCPT1, LTRIM(RTRIM(PC2.CTDSC1)) AS PCPT2, LTRIM(RTRIM(PC3.CTDSC1)) AS PCPT3, LTRIM(RTRIM(PC4.CTDSC1)) AS PCPT4, "
                            + "LTRIM(RTRIM(PC5.CTDSC1)) AS PCET1, LTRIM(RTRIM(PC6.CTDSC1)) AS PCET2, LTRIM(RTRIM(PC7.CTDSC1)) AS PCET3 "
                            + "FROM BUSDTA.Prospect_Contact "
                            + "LEFT JOIN BUSDTA.M080111 PC1 ON BUSDTA.Prospect_Contact.[PhoneType1] = PC1.[CTID] "
                            + "LEFT JOIN BUSDTA.M080111 PC2 ON BUSDTA.Prospect_Contact.[PhoneType2] = PC2.[CTID] "
                            + "LEFT JOIN BUSDTA.M080111 PC3 ON BUSDTA.Prospect_Contact.[PhoneType3] = PC3.[CTID] "
                            + "LEFT JOIN BUSDTA.M080111 PC4 ON BUSDTA.Prospect_Contact.[PhoneType4] = PC4.[CTID] "
                            + "LEFT JOIN BUSDTA.M080111 PC5 ON BUSDTA.Prospect_Contact.[EmailType1] = PC5.[CTID] "
                            + "LEFT JOIN BUSDTA.M080111 PC6 ON BUSDTA.Prospect_Contact.[EmailType2] = PC6.[CTID] "
                            + "LEFT JOIN BUSDTA.M080111 PC7 ON BUSDTA.Prospect_Contact.[EmailType3] = PC7.[CTID] "
                            + "WHERE ProspectId ='" + ProspectID + "' ORDER BY ProspectContactId";
                DataSet queryResult = DbEngine.ExecuteDataSet(qry);
                if (queryResult.HasData())
                {
                    ContactProspect prospectcontact;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow EquipmentDataRow = queryResult.Tables[0].Rows[index - 1];
                        prospectcontact = new ContactProspect();
                        prospectcontact.ProspectID = Convert.ToInt32(EquipmentDataRow["ProspectId"]);
                        prospectcontact.ContactID = EquipmentDataRow["ProspectContactId"].ToString();
                        //prospectcontact.RouteID = EquipmentDataRow["RouteId"].ToString();
                        prospectcontact.Name = EquipmentDataRow["FirstName"].ToString();
                        prospectcontact.Title = EquipmentDataRow["Title"].ToString();
                        prospectcontact.EmailID = EquipmentDataRow["EmailID1"].ToString();
                        prospectcontact.Phone1 = EquipmentDataRow["AreaCode1"].ToString();
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["Phone1"])))
                        {
                            prospectcontact.Phone2 = Convert.ToString(EquipmentDataRow["Phone1"]).Substring(0, 3);
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["Phone1"])))
                        {
                            prospectcontact.Phone3 = Convert.ToString(EquipmentDataRow["Phone1"]).Substring(4, 4);
                        }
                        prospectcontact.PhoneExtention = EquipmentDataRow["Extn1"].ToString();
                        prospectcontact.LineID = Convert.ToString(EquipmentDataRow["ProspectContactId"]);
                        prospectcontact.LineNo = "0"; // EquipmentDataRow["PCRCK7"].ToString();
                        prospectcontact.RelatedPersonID = "0"; //EquipmentDataRow["PCCNLN"].ToString();

                        prospectcontact.PhoneList = new ObservableCollection<ProspectPhone>();
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["AreaCode1"])))
                        {
                            prospectcontact.PhoneList.Add(new ProspectPhone(1, Convert.ToInt32(EquipmentDataRow["ProspectId"]), Convert.ToString(EquipmentDataRow["ProspectContactId"]), Convert.ToString(EquipmentDataRow["AreaCode1"]), Convert.ToString(EquipmentDataRow["Phone1"]), Convert.ToString(EquipmentDataRow["Extn1"]), Convert.ToString(EquipmentDataRow["PhoneType1"]), Convert.ToInt32(EquipmentDataRow["IsDefaultPhone1"]), Convert.ToString(EquipmentDataRow["PCPT1"])));
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["AreaCode2"])))
                        {
                            prospectcontact.PhoneList.Add(new ProspectPhone(2, Convert.ToInt32(EquipmentDataRow["ProspectId"]), Convert.ToString(EquipmentDataRow["ProspectContactId"]), Convert.ToString(EquipmentDataRow["AreaCode2"]), Convert.ToString(EquipmentDataRow["Phone2"]), Convert.ToString(EquipmentDataRow["Extn2"]), Convert.ToString(EquipmentDataRow["PhoneType2"]), Convert.ToInt32(EquipmentDataRow["IsDefaultPhone2"]), Convert.ToString(EquipmentDataRow["PCPT2"])));
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["AreaCode3"])))
                        {
                            prospectcontact.PhoneList.Add(new ProspectPhone(3, Convert.ToInt32(EquipmentDataRow["ProspectId"]), Convert.ToString(EquipmentDataRow["ProspectContactId"]), Convert.ToString(EquipmentDataRow["AreaCode3"]), Convert.ToString(EquipmentDataRow["Phone3"]), Convert.ToString(EquipmentDataRow["Extn3"]), Convert.ToString(EquipmentDataRow["PhoneType3"]), Convert.ToInt32(EquipmentDataRow["IsDefaultPhone3"]), Convert.ToString(EquipmentDataRow["PCPT3"])));
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["AreaCode4"])))
                        {
                            prospectcontact.PhoneList.Add(new ProspectPhone(4, Convert.ToInt32(EquipmentDataRow["ProspectId"]), Convert.ToString(EquipmentDataRow["ProspectContactId"]), Convert.ToString(EquipmentDataRow["AreaCode4"]), Convert.ToString(EquipmentDataRow["Phone4"]), Convert.ToString(EquipmentDataRow["Extn4"]), Convert.ToString(EquipmentDataRow["PhoneType4"]), Convert.ToInt32(EquipmentDataRow["IsDefaultPhone4"]), Convert.ToString(EquipmentDataRow["PCPT4"])));
                        }

                        prospectcontact.EmailList = new ObservableCollection<ProspectEmail>();
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["EmailID1"])))
                        {
                            prospectcontact.EmailList.Add(new ProspectEmail(1, Convert.ToInt32(EquipmentDataRow["ProspectId"]), Convert.ToString(EquipmentDataRow["ProspectContactId"]), Convert.ToString(EquipmentDataRow["EmailID1"]), Convert.ToString(EquipmentDataRow["EmailType1"]), Convert.ToInt32(EquipmentDataRow["IsDefaultEmail1"]), Convert.ToString(EquipmentDataRow["PCET1"])));
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["EmailID2"])))
                        {
                            prospectcontact.EmailList.Add(new ProspectEmail(2, Convert.ToInt32(EquipmentDataRow["ProspectId"]), Convert.ToString(EquipmentDataRow["ProspectContactId"]), Convert.ToString(EquipmentDataRow["EmailID2"]), Convert.ToString(EquipmentDataRow["EmailType2"]), Convert.ToInt32(EquipmentDataRow["IsDefaultEmail2"]), Convert.ToString(EquipmentDataRow["PCET2"])));
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(EquipmentDataRow["EmailID3"])))
                        {
                            prospectcontact.EmailList.Add(new ProspectEmail(3, Convert.ToInt32(EquipmentDataRow["ProspectId"]), Convert.ToString(EquipmentDataRow["ProspectContactId"]), Convert.ToString(EquipmentDataRow["EmailID3"]), Convert.ToString(EquipmentDataRow["EmailType3"]), Convert.ToInt32(EquipmentDataRow["IsDefaultEmail3"]), Convert.ToString(EquipmentDataRow["PCET3"])));
                        }

                        prospectcontact.IsActive = EquipmentDataRow["IsActive"].ToString() == "1" ? true : false;
                        prospectcontact.ShowOnDashboard = EquipmentDataRow["ShowOnDashboard"].ToString() == "1" ? true : false;
                        //prospectcontact.ShowOnDashboard = !string.IsNullOrEmpty(EquipmentDataRow["ShowOnDashboard"].ToString()) ? Convert.ToBoolean(EquipmentDataRow["ShowOnDashboard"].ToString()) : false;                        
                        prospectcontact.IsExpanded = prospectcontact.ShowOnDashboard;
                        prospectcontact.IsDefault = prospectcontact.ContactID == "1" ? true : false;

                        if (prospectcontact.PhoneList.Count == 4)
                        {
                            prospectcontact.TogglePhoneImg = false;
                        }
                        if (prospectcontact.EmailList.Count == 3)
                        {
                            prospectcontact.ToggleEmailImg = false;
                        }
                        ContactCollection.Add(prospectcontact);
                    }
                }
                ProspectEmail selectedEmail = null;
                ProspectPhone selectedPhone = null;
                foreach (ContactProspect contactItem in ContactCollection)
                {
                    if (contactItem.ShowOnDashboard)
                    {
                        selectedEmail = contactItem.EmailList.FirstOrDefault(email => email.IsDefault == true);
                        if (selectedEmail != null)
                        {
                            selectedEmail.IsSelectedEmail = true;
                        }
                        selectedPhone = contactItem.PhoneList.FirstOrDefault(phone => phone.IsDefault == true);
                        if (selectedPhone != null)
                        {
                            selectedPhone.IsSelectedPhone = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetProspectsForRoute][routeID=" + ProspectID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return ContactCollection;
            }
            return ContactCollection;
        }
        #endregion

        #region ProspectContact

        public int AddProspectContact(ContactProspect ContactProspect, string routeID, string ProspectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddProspectContact]");
            int result = 0;
            try
            {
                string query;
                if (ContactProspect.ContactID == null)
                {
                    string queryUniqueID = "SELECT COALESCE((MAX(ProspectContactId)+1),1) FROM BUSDTA.Prospect_Contact where ProspectId = '" + ProspectID + "';";
                    int uniqueID = Convert.ToInt32(DbEngine.ExecuteScalar(queryUniqueID));
                    int isDefault = 0;
                    if (uniqueID == 1)
                    {
                        isDefault = 1;
                    }
                    string PhoneType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedPhoneType + "';");
                    string EmailType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedEmailType + "';");

                    query = (@"INSERT INTO BUSDTA.Prospect_Contact (ProspectId,ProspectContactId,RouteId,FirstName,Title,PhoneType1,AreaCode1,Phone1,Extn1,EmailType1,EmailID1,IsActive,ShowOnDashboard,IsDefaultPhone1,IsDefaultEmail1)
                                VALUES ('" + ProspectID + "','" + uniqueID + "','" + CommonNavInfo.RouteID + "','" + ContactProspect.Name + "','" + ContactProspect.Title + "','" + PhoneType + "','" + ContactProspect.Phone1 + "','"
                                                + ContactProspect.Phone2 + "-" + ContactProspect.Phone3 + "','" + ContactProspect.PhoneExtention + "','" + EmailType + "','"
                                             + ContactProspect.EmailID + "','1','" + isDefault + "','1','1'" + ")");
                }
                else
                {
                    string PhoneType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedPhoneType + "';");
                    string EmailType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedEmailType + "';");

                    query = @"UPDATE BUSDTA.Prospect_Contact  SET FirstName ='" + ContactProspect.Name + "', Title ='" + ContactProspect.Title + "'"
                                                                              + ",PhoneType1 ='" + PhoneType + "'"
                                                                              + ",AreaCode1 ='" + ContactProspect.Phone1 + "'"
                                                                              + ",Phone1 ='" + ContactProspect.Phone2 + "-" + ContactProspect.Phone3 + "'"
                                                                              + ",Extn1 ='" + ContactProspect.PhoneExtention + "'"
                                                                              + ",EmailType1 ='" + EmailType + "'"
                                                                              + ",EmailID1 ='" + ContactProspect.EmailID + "'"
                                                                              + ",IsDefaultPhone1 ='" + 1 + "'"
                                                                              + ",IsDefaultEmail1 ='" + 1 + "'"
                                                                              + " WHERE ProspectContactId = '" + ContactProspect.ContactID + "'"
                                                                              + " AND ProspectId = '" + ProspectID + "'";
                }
                DbEngine.ExecuteNonQuery(query);
                result = 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        #endregion

        #region AddProspectPhone

        public int AddProspectPhone(ContactProspect ContactProspect, string routeID, string ProspectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddProspectPhone]");
            int result = 0;
            try
            {
                string PhoneType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedPhoneType + "';");
                string query;
                int PhNoCount = ContactProspect.PhoneList.Count;

                query = @"UPDATE BUSDTA.Prospect_Contact SET ";
                if (PhNoCount == 0)
                {
                    query += "PhoneType1 = '" + PhoneType + "'"
                    + ",AreaCode1 ='" + ContactProspect.Phone1 + "'"
                    + ",Phone1 ='" + ContactProspect.Phone2 + "-" + ContactProspect.Phone3 + "'"
                    + ",Extn1 ='" + ContactProspect.PhoneExtention + "'"
                    + ",IsDefaultPhone1 ='" + 1 + "'";
                }
                if (PhNoCount == 1)
                {
                    query += "PhoneType2 = '" + PhoneType + "'"
                    + ",AreaCode2 ='" + ContactProspect.Phone1 + "'"
                    + ",Phone2 ='" + ContactProspect.Phone2 + "-" + ContactProspect.Phone3 + "'"
                    + ",Extn2 ='" + ContactProspect.PhoneExtention + "'"
                     + ",IsDefaultPhone2 ='" + 0 + "'";
                }
                else if (PhNoCount == 2)
                {
                    query += "PhoneType3 = '" + PhoneType + "'"
                           + ",AreaCode3 ='" + ContactProspect.Phone1 + "'"
                          + ",Phone3 ='" + ContactProspect.Phone2 + "-" + ContactProspect.Phone3 + "'"
                           + ",Extn3 ='" + ContactProspect.PhoneExtention + "'"
                            + ",IsDefaultPhone3 ='" + 0 + "'";
                }
                else if (PhNoCount == 3)
                {
                    query += "PhoneType4 = '" + PhoneType + "'"
                               + ",AreaCode4 ='" + ContactProspect.Phone1 + "'"
                               + ",Phone4 ='" + ContactProspect.Phone2 + "-" + ContactProspect.Phone3 + "'"
                               + ",Extn4 ='" + ContactProspect.PhoneExtention + "'"
                               + ",IsDefaultPhone4 ='" + 0 + "'";
                }
                query += "WHERE ProspectContactId = '" + ContactProspect.ContactID + "' AND ProspectId ='" + ProspectID + "'";
                DbEngine.ExecuteNonQuery(query);
                result = 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddNewProspect][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        #endregion

        #region AddProspectEmail

        public int AddProspectEmail(ContactProspect ContactProspect, string routeID, string ProspectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:AddProspectEmail]");
            int result = 0;
            try
            {
                string EmailType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedEmailType + "';");
                string query;
                int EmailCount = ContactProspect.EmailList.Count;
                query = @"UPDATE BUSDTA.Prospect_Contact SET ";
                if (EmailCount == 0)
                {
                    query += "EmailType1 = '" + EmailType + "'"
                          + ",EmailID1 ='" + ContactProspect.EmailID + "'"
                          + ",IsDefaultEmail1 ='" + 1 + "'";
                }
                if (EmailCount == 1)
                {
                    query += "EmailType2 = '" + EmailType + "'"
                          + ",EmailID2 ='" + ContactProspect.EmailID + "'"
                          + ",IsDefaultEmail2 ='" + 0 + "'"; ;
                }
                else if (EmailCount == 2)
                {
                    query += "EmailType3 = '" + EmailType + "'"
                          + ",EmailID3 ='" + ContactProspect.EmailID + "'"
                          + ",IsDefaultEmail3 ='" + 0 + "'"; ;
                }
                query += "WHERE ProspectContactId = '" + ContactProspect.ContactID + "' AND ProspectId ='" + ProspectID + "'";
                DbEngine.ExecuteNonQuery(query);
                result = 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][AddProspectEmail][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }
        #endregion

        #region Edit Prospect Phone
        public ContactProspect GetContactNameAndTitle(string contactID, string ProspectID)
        {
            ContactProspect contact = new ContactProspect();
            try
            {
                string query = "SELECT ProspectId, ProspectContactId,FirstName, Title, IsActive, ShowOnDashboard FROM BUSDTA.Prospect_Contact WHERE ProspectContactId ='" + contactID + "' AND ";
                query = query + " ProspectId ='";
                query = query + ProspectID + "';";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contact = new ContactProspect();    // TODO: redundant memory allocation?
                            contact.ContactID = row["ProspectContactId"].ToString().Trim();
                            contact.Name = row["FirstName"].ToString().Trim();
                            contact.Title = row["Title"].ToString();
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][GetContactNameAndTitle][ProspectID=" + ProspectID + "][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][GetContactNameAndTitle][ProspectID=" + ProspectID + "][contactID=" + contactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return contact;
        }
        #endregion

        #region SaveEditedProspectEmail
        public int SaveEditedProspectEmail(ContactProspect ContactProspect, string routeID, string ProspectID, int Index)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:SaveEditedProspectEmail][contact=" + ContactProspect.EmailID + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();

            string EmailType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedEmailType + "';");
            int i = Index;
            try
            {
                string query = "UPDATE BUSDTA.Prospect_Contact SET EmailID" + i + "= '";
                query = query + ContactProspect.EmailID + "',";
                query = query + " EmailType" + i + "=" + "'" + EmailType + "'";
                query = query + " WHERE ProspectContactId ='" + ContactProspect.ContactID + "'";
                query = query + " AND ProspectId = '" + ProspectID + "';";
                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][SaveEditedProspectEmail][contact=" + ContactProspect.EmailID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][End:SaveEditedProspectEmail][contact=" + ContactProspect.EmailID + "]");
            return result;
        }
        #endregion

        #region SaveEditedProspectPhone
        public int SaveEditedProspectPhone(ContactProspect ContactProspect, string routeID, string ProspectID, int Index)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:SaveEditedEmail][contact=" + ContactProspect.SelectedPhoneType + "]");
            ObservableCollection<CustomerContact> contact = new ObservableCollection<CustomerContact>();

            string PhoneType = DbEngine.ExecuteScalar(@"SELECT CTID FROM BUSDTA.M080111 where CTDSC1 = '" + ContactProspect.SelectedPhoneType + "';");
            int i = Index;
            try
            {
                string query = "UPDATE BUSDTA.Prospect_Contact SET PhoneType" + i + "= '";
                query = query + PhoneType + "',";
                query = query + " AreaCode" + i + "=" + "'" + ContactProspect.Phone1 + "',";
                query = query + " Phone" + i + "='" + ContactProspect.Phone2 + "-" + ContactProspect.Phone3 + "',";
                query = query + " Extn" + i + "=" + "'" + ContactProspect.PhoneExtention + "'";
                query = query + " WHERE ProspectContactId ='" + ContactProspect.ContactID + "'";
                query = query + " AND ProspectId = '" + ProspectID + "';";
                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][SaveEditedPhone][contact=" + ContactProspect.SelectedPhoneType + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][End:SaveEditedPhone][contact=" + ContactProspect.SelectedPhoneType + "]");
            return result;
        }
        #endregion

        #region DeleteProspectContactEmail
        public int DeleteProspectContactEmail(ContactProspect ContactProspect, string ProspectID, int index)
        {
            int result = -1;
            int i = index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:DeleteContactEmail][contact=" + ContactProspect.SelectedEmailType + "]");
            try
            {
                string query = "UPDATE BUSDTA.Prospect_Contact SET EmailID" + i + "= NULL, EmailType" + i + "= NULL, IsDefaultEmail" + i + "= NULL";
                query = query + " WHERE ProspectContactId ='" + ContactProspect.ContactID + "'";
                query = query + " AND ProspectId = '" + ProspectID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][DeleteContactEmail][contact=" + ContactProspect.SelectedEmailType + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:DeleteContactEmail][contact=" + ContactProspect.SelectedEmailType + "]");
            return result;
        }
        #endregion

        #region SetContactAsDefault
        public void SetContactAsDefault(ContactProspect ContactProspect, string ProspectID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][Start:SetContactAsDefault][contact=" + ContactProspect.SerializeToJson() + "]");
            try
            {
                string query = string.Format("Update BUSDTA.Prospect_Contact set ShowOnDashboard=0 where ProspectId='{0}'", ProspectID);
                DbEngine.ExecuteNonQuery(query);
                int phoneIndex = ContactProspect.DefaultPhone != null ? ContactProspect.DefaultPhone.Index : 0;
                int emailIndex = ContactProspect.DefaultEmail != null ? ContactProspect.DefaultEmail.Index : 0;

                foreach (ProspectPhone phone in ContactProspect.PhoneList)
                {
                    if (phone.Index != phoneIndex && phoneIndex != null && phoneIndex > 0)
                    {
                        query = string.Format("Update BUSDTA.Prospect_Contact set IsDefaultPhone{0}=0 where ProspectContactId='{1}' AND  ProspectId='{2}'", phone.Index, ContactProspect.LineID, ProspectID);
                        DbEngine.ExecuteNonQuery(query);
                    }
                }
                foreach (ProspectEmail email in ContactProspect.EmailList)
                {
                    if (email.Index != emailIndex && emailIndex != null && emailIndex > 0)
                    {
                        query = string.Format("Update BUSDTA.Prospect_Contact set IsDefaultEmail{0}=0 where ProspectContactId='{1}' AND ProspectId='{2}'", email.Index, ContactProspect.LineID, ProspectID);
                        DbEngine.ExecuteNonQuery(query);
                    }
                }
                if (emailIndex > 0)
                {
                    query = string.Format("Update BUSDTA.Prospect_Contact set IsDefaultEmail{0}=1 where ProspectContactId='{1}' AND ProspectId='{2}'", emailIndex, ContactProspect.LineID, ProspectID);
                    DbEngine.ExecuteNonQuery(query);
                }
                if (phoneIndex > 0)
                {
                    query = string.Format("Update BUSDTA.Prospect_Contact set IsDefaultPhone{0}=1 where ProspectContactId='{1}' AND ProspectId='{2}'", phoneIndex, ContactProspect.LineID, ProspectID);
                    DbEngine.ExecuteNonQuery(query);
                }
                query = string.Format("Update BUSDTA.Prospect_Contact set ShowOnDashboard=1 where ProspectContactId='{0}'  AND ProspectId='{1}'", ContactProspect.LineID, ProspectID);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][SetContactAsDefault][contact=" + ex.ToString() + "][ExceptionStackTrace = " + ex.InnerException == null ? ex.Message : ex.InnerException.Message + "]");
            }
        }
        #endregion

        #region DeleteContactPhone
        public int DeleteContactPhone(ContactProspect ContactProspect, int index, string ProspectID)
        {
            int result = -1;
            int i = index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:DeleteContactPhone][contact=" + ContactProspect.ContactID + "]");
            try
            {
                string query = "UPDATE BUSDTA.Prospect_Contact SET AreaCode" + i + "= NULL, Phone" + i + "= NULL, Extn" + i + "= NULL, PhoneType" + i + "= NULL, IsDefaultPhone" + i + "= NULL";
                query = query + " WHERE ProspectContactId ='" + ContactProspect.ContactID + "'";
                query = query + " AND ProspectId = '" + ProspectID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][DeleteContactPhone][contact=" + ContactProspect.ContactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][End:DeleteContactPhone][contact=" + ContactProspect.ContactID + "]");
            return result;
        }
        #endregion

        #region UpdateEmailIndexAfterDelete
        public int UpdateEmailIndexAfterDelete(ContactProspect ContactProspect, int Index, string ProspectID)
        {
            int result = -1;
            int index = Index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:UpdateIndexAfterDelete][contact=" + ContactProspect.ContactID + "]");
            try
            {
                if (index < 3)
                {
                    string query = "select isnull(EmailID" + (index + 1) + ",NULL) from BUSDTA.Prospect_Contact where ProspectContactId ='" + ContactProspect.ContactID + "'";
                    query = query + " AND ProspectId = '" + ProspectID + "';";

                    string res = DbEngine.ExecuteScalar(query);
                    if (res != null)
                    {
                        for (int i = 0; i < ContactProspect.EmailList.Count; i++)
                        {
                            string updateQuery = "update BUSDTA.Prospect_Contact set EmailID" + index + " = EmailID" + (index + 1) + ", EmailID" + (index + 1) + " = null";
                            updateQuery = updateQuery + " ,EmailType" + index + " = EmailType" + (index + 1) + ", EmailType" + (index + 1) + "= null";
                            updateQuery = updateQuery + " where ProspectContactId ='" + ContactProspect.ContactID + "'";
                            updateQuery = updateQuery + " AND ProspectId = '" + ProspectID + "';";
                            result = DbEngine.ExecuteNonQuery(updateQuery);
                            index++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][UpdateIndexAfterDelete][contact=" + ContactProspect.ContactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][End:UpdateIndexAfterDelete][contact=" + ContactProspect.ContactID + "]");

            return result;
        }
        #endregion

        #region UpdatePhoneIndexAfterDelete
        public int UpdatePhoneIndexAfterDelete(ContactProspect ContactProspect, int Index, string ProspectID)
        {
            int result = -1;
            int index = Index;
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:UpdateIndexAfterDelete][contact=" + ContactProspect.ContactID + "]");
            try
            {
                if (index < 4)
                {
                    string query = "select isnull(AreaCode" + (index + 1) + ",NULL) from BUSDTA.Prospect_Contact where ProspectContactId ='" + ContactProspect.ContactID + "'";
                    query = query + " AND ProspectId = '" + ProspectID + "';";

                    string res = DbEngine.ExecuteScalar(query);
                    if (res != null)
                    {
                        for (int i = 0; i < ContactProspect.PhoneList.Count; i++)
                        {
                            string updateQuery = "update BUSDTA.Prospect_Contact set AreaCode" + index + " = AreaCode" + (index + 1) + ", AreaCode" + (index + 1) + " = null";
                            updateQuery = updateQuery + " ,Phone" + index + " = Phone" + (index + 1) + ", Phone" + (index + 1) + "= null";
                            updateQuery = updateQuery + " ,Extn" + index + " = Extn" + (index + 1) + ", Extn" + (index + 1) + "= null";
                            updateQuery = updateQuery + " ,PhoneType" + index + " = PhoneType" + (index + 1) + ", PhoneType" + (index + 1) + "= null";
                            updateQuery = updateQuery + " where ProspectContactId ='" + ContactProspect.ContactID + "'";
                            updateQuery = updateQuery + " AND ProspectId = '" + ProspectID + "';";
                            result = DbEngine.ExecuteNonQuery(updateQuery);
                            index++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][UpdateIndexAfterDelete][contact=" + ContactProspect.ContactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][End:UpdateIndexAfterDelete][contact=" + ContactProspect.ContactID + "]");

            return result;
        }
        #endregion

        #region DeleteProspectContact
        public int DeleteProspectContact(ContactProspect ContactProspect, string ProspectID)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][prospectManager][Start:DeleteProspectContact][contactID=" + ContactProspect.ContactID + "]");
            ObservableCollection<ContactProspect> contactList = new ObservableCollection<ContactProspect>();
            try
            {
                string query = "DELETE FROM BUSDTA.Prospect_Contact WHERE ProspectContactId ='";
                query = query + ContactProspect.ContactID + "' AND ProspectId='";
                query = query + ProspectID + "';";

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][prospectManager][DeleteProspectContact][contactID=" + ContactProspect.ContactID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][prospectManager][End:DeleteProspectContact][contactID=" + ContactProspect.ContactID + "]");

            return result;
        }
        #endregion
        #endregion

        public List<string> GetStateCodes()
        {
            DateTime timer = DateTime.Now;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetStateCodes]");
            Logger.Info("[CustomerManager][Start:GetStateCodes][FunctionStartTime:]\t" + DateTime.Now + "");
            Logger.Info("[CustomerManager][GetStateCodes][QueryStart:]\t" + DateTime.Now + "");
            List<string> stateCodeList = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("select distinct(RTRIM(ad.ALADDS)) cd from busdta.f0116 ad WHERE ad.ALADDS !=''");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        stateCodeList.Add(dataRow["cd"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStateCodes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[CustomerManager][GetStateCodes][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetStateCodes]");
            return stateCodeList;
        }

        #region Prospects Information
        public ProspectInformation GetProspectInformation(int prospectID)
        {
            DateTime timer = DateTime.Now;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][Start:GetCustomerInformation]");
            Logger.Info("[CustomerManager][Start:GetCustomerInformation][FunctionStartTime:]\t" + DateTime.Now + "");
            Logger.Info("[CustomerManager][GetCustomerInformation][QueryStart:]\t" + DateTime.Now + "");
            ProspectInformation prospectInformation = new ProspectInformation();
            try
            {
                #region  Implementation
                string customerByRouteQuery =
                            @" SELECT PROS.prospectid,PROS.routeid,ProspectName,AcctSegmentType,BuyingGroup,LocationCount,AddressLine1,AddressLine2
                                ,AddressLine3,AddressLine4,CityName,StateName,ZipCode
                                FROM busdta.Prospect_Master PROS
                                INNER JOIN busdta.Prospect_address ADDR ON PROS.ProspectId = ADDR.ProspectId AND PROS.routeid = ADDR.routeid
                                WHERE PROS.prospectid ='" + prospectID + "'";

                DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerByRouteQuery);
                prospectInformation.ProspectID = prospectID.ToString();

                if (queryResult.HasData())
                {
                    string[] deliveryCodes = new string[] { "11", "1A", "1E" };
                    Random randomCode = new Random();
                    prospectInformation = new ProspectInformation
                    {
                        ProspectID = Convert.ToString(queryResult.Tables[0].Rows[0]["prospectid"]),
                        BusinessName = queryResult.Tables[0].Rows[0]["ProspectName"].ToString().Trim().ToUpper(),
                        Name = queryResult.Tables[0].Rows[0]["ProspectName"].ToString().Trim().ToUpper(),
                        SegmentType = queryResult.Tables[0].Rows[0]["AcctSegmentType"].ToString().Trim().ToUpper(),
                        BuyingGroup = queryResult.Tables[0].Rows[0]["BuyingGroup"].ToString().Trim().ToUpper(),
                        LocationCount = queryResult.Tables[0].Rows[0]["LocationCount"].ToString().Trim(),
                        Address1 = queryResult.Tables[0].Rows[0]["AddressLine1"].ToString().Trim().ToUpper(),
                        Address2 = queryResult.Tables[0].Rows[0]["AddressLine2"].ToString().Trim().ToUpper(),
                        Address3 = queryResult.Tables[0].Rows[0]["AddressLine3"].ToString().Trim().ToUpper(),
                        Address4 = queryResult.Tables[0].Rows[0]["AddressLine4"].ToString().Trim().ToUpper(),
                        CustInfoCity = queryResult.Tables[0].Rows[0]["CityName"].ToString().Trim().ToUpper(),
                        CustInfoState = queryResult.Tables[0].Rows[0]["StateName"].ToString().Trim().ToUpper(),
                        CustInfoZip = queryResult.Tables[0].Rows[0]["ZipCode"].ToString().ToUpper().Trim(),
                        CurrentDate = (DateTime.Now.ToString("dddd', 'MM'/'dd'/'yyyy ")),
                        InitDisable = false,
                        //MailingName = queryResult.Tables[0].Rows[0]["MailingName"].ToString().ToUpper(),
                        //TaxExemptCertificate = queryResult.Tables[0].Rows[0]["TaxExemptCertificate"].ToString().ToUpper(),

                    };
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetCustomerInformation][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
            Logger.Info("[CustomerManager][GetCustomerInformation][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:GetCustomerInformation]");
            return prospectInformation;

        }

        public ObservableCollection<ContactType> GetContactTypes()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:GetContactTypes]");
            ObservableCollection<ContactType> contactTypeList = new ObservableCollection<ContactType>();
            try
            {
                string query = "select * from busdta.M080111";
                DataSet result = DbEngine.ExecuteDataSet(query);
                ContactType contactType;
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        try
                        {
                            row = result.Tables[0].Rows[i];
                            contactType = new ContactType();
                            contactType.TypeID = row["CTID"].ToString().Trim();
                            contactType.Type = row["CTTYP"].ToString().Trim();
                            contactType.Code = row["CTCD"].ToString().Trim();
                            contactType.Description = row["CTDSC1"].ToString().Trim();
                            contactTypeList.Add(contactType);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Application.Managers][ProspectManager][GetContactTypes][ExceptionStackTrace = " + ex.StackTrace + "]");
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ContactManager][GetContactTypes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ContactManager][End:GetContactTypes]");
            return contactTypeList;
        }

        public int SaveProspectInformation(ProspectInformation prospectInformation)
        {
            int result = -1;
            DateTime timer = DateTime.Now;
            Logger.Info("[SalesLogicExpress.Application.Managers][ProspectManager][Start:SaveProspectInformation]");
            Logger.Info("[ProspectManager][Start:SaveProspectInformation][FunctionStartTime:]\t" + DateTime.Now + "");
            Logger.Info("[CustomerManager][SaveCustomerInformation][QueryStart:]\t" + DateTime.Now + "");

            try
            {
                UpdateProspectInformation(prospectInformation);
                #region Implementation
                string updateQuery =
                                   @"update  busdta.Prospect_address set 
                                    AddressLine1='" + prospectInformation.Address1 + "'," +
                                   " AddressLine2='" + prospectInformation.Address2 + "'," +
                                   " AddressLine3='" + prospectInformation.Address3 + "'," +
                                   " AddressLine4='" + prospectInformation.Address4 + "'," +
                                   " CityName='" + prospectInformation.CustInfoCity + "'," +
                                   " StateName='" + prospectInformation.CustInfoState + "'," +
                                   " ZipCode='" + prospectInformation.CustInfoZip + "'" +
                                   " where prospectid='" + prospectInformation.ProspectID + "'";
                Sap.Data.SQLAnywhere.SACommand command = new Sap.Data.SQLAnywhere.SACommand(updateQuery);
                //Sap.Data.SQLAnywhere.SAParameter parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.ParameterName = "Address1";
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.NVarChar;
                //parm.Value = prospectInformation.Address1;
                //command.Parameters.Add(parm);

                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.ParameterName = "Address2";
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.NVarChar;
                //parm.Value = prospectInformation.Address2;
                //command.Parameters.Add(parm);

                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.ParameterName = "Address3";
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.NVarChar;
                //parm.Value = prospectInformation.Address3;
                //command.Parameters.Add(parm);

                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.ParameterName = "Address4";
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.NVarChar;
                //parm.Value = prospectInformation.Address4;
                //command.Parameters.Add(parm);

                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.ParameterName = "City";
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.NVarChar;
                //parm.Value = prospectInformation.CustInfoCity;
                //command.Parameters.Add(parm);

                //parm = new Sap.Data.SQLAnywhere.SAParameter();
                //parm.ParameterName = "State";
                //parm.SADbType = Sap.Data.SQLAnywhere.SADbType.NVarChar;
                //parm.Value = prospectInformation.CustInfoState;
                //command.Parameters.Add(parm);

                // UpdateTaxExemptCertificate(prospectInformation);

                result = new DB_CRUD().InsertUpdateData(command);
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][SaveCustomerInformation][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw ex;
            }
            Logger.Info("[CustomerManager][SaveCustomerInformation][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerManager][End:SaveCustomerInformation]");
            return result;

        }

        public int UpdateProspectInformation(ProspectInformation prospectInformation)
        {
            try
            {
                string updateQuery = string.Empty;
                if (!string.IsNullOrEmpty(prospectInformation.SegmentType))
                {

                    updateQuery = @"update  busdta.Prospect_Master set 
                            ProspectName='" + prospectInformation.BusinessName + "'," +
                          " AcctSegmentType='" + Convert.ToInt32(prospectInformation.SegmentType) + "'," +
                          " BuyingGroup='" + prospectInformation.BuyingGroup + "'," +
                          " LocationCount='" + prospectInformation.LocationCount + "'" +
                          " where prospectid='" + prospectInformation.ProspectID + "'";
                }
                else
                {
                    updateQuery = @"update  busdta.Prospect_Master set 
                            ProspectName='" + prospectInformation.BusinessName + "'," +
                              " BuyingGroup='" + prospectInformation.BuyingGroup + "'," +
                              " LocationCount='" + prospectInformation.LocationCount + "'" +
                              " where prospectid='" + prospectInformation.ProspectID + "'";
                }


                return DbEngine.ExecuteNonQuery(updateQuery);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UpdateTaxExemptCertificate ProspectID = " + prospectInformation.ProspectID + " Error = " + ex.Message);
                throw ex;
            }

        }
        #endregion Prospects Information

        public List<string> GetCategoryList(string EquipmentTypeID)
        {
            DateTime timer = DateTime.Now;
            List<string> CategoryList = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT DISTINCT(EQUIPMENTCATEGORY) TC,EQUIPMENTCATEGORYID FROM [BUSDTA].TBLEQUIPMENTCATEGORY where EquipmentTypeID =" + EquipmentTypeID);
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        CategoryList.Add(dataRow["TC"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStateCodes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return CategoryList;
        }
        public List<string> GetSubCategoryList(string EquipmentCategory, string EquipmentType)
        {
            DateTime timer = DateTime.Now;
            List<string> CategoryList = new List<string>();
            try
            {
                #region Implementation
                DataSet queryResult = new DataSet();
                if (string.IsNullOrWhiteSpace(EquipmentCategory))
                {
                    queryResult = DbEngine.ExecuteDataSet(@"select tsc.EquipmentSubCategory sc from [BUSDTA].tblequipmentCategory tc 
                                                    inner join [BUSDTA].TBLEQUIPMENTSUBCATEGORY tsc on tsc.EquipmentCategoryID = tc.EquipmentCategoryID 
                                                    where tc.EquipmentTypeID='" + EquipmentType + "' and rtrim(LTRIM( TC.EquipmentCategory))='" + EquipmentCategory + "' ");
                }
                else
                {
                    queryResult = DbEngine.ExecuteDataSet(@"select tsc.EquipmentSubCategory sc from [BUSDTA].tblequipmentCategory tc 
                                                    inner join [BUSDTA].TBLEQUIPMENTSUBCATEGORY tsc on tsc.EquipmentCategoryID = tc.EquipmentCategoryID 
                                                    where tc.EquipmentTypeID='" + EquipmentType + "' and rtrim(LTRIM(TC.EquipmentCategory))='" + EquipmentCategory + "' ");
                }

                if (queryResult.HasData())
                {
                    foreach (DataRow dataRow in queryResult.Tables[0].Rows)
                    {
                        CategoryList.Add(dataRow["sc"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStateCodes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return CategoryList;
        }

        public List<string> GetEQOwned()
        {
            List<string> EQOwnedList = new List<string>();
            DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT ResponseId, Response FROM BUSDTA.Response_Master");
            if (resultDataSet.HasData())
            {
                foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                {
                    EQOwnedList.Add(Convert.ToString(dataRow["Response"]).Trim());
                }
            }
            return EQOwnedList;
        }

        public string GetCategoryList(int EquipmentID, int EquipmentCategoryID)
        {
            DateTime timer = DateTime.Now;
            string CategoryList = string.Empty;
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT DISTINCT EQC.EQUIPMENTCATEGORY TC FROM [BUSDTA].TBLEQUIPMENT EQ INNER JOIN [BUSDTA].TBLEQUIPMENTCATEGORY EQC ON EQC.EQUIPMENTCATEGORYID = EQ.EQUIPMENTCATEGORYID WHERE EquipmentID =" + EquipmentID + "AND EQ.EquipmentCategoryID =" + EquipmentCategoryID);
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        CategoryList = dataRow["TC"].ToString().Trim();
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStateCodes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return CategoryList;
        }
        public List<string> ProspectSearch(string ProspectID, string SearchTerm, string EquipmentType)
        {
            List<string> serchList = new List<string>();
            try
            {
                #region Implementation
                DataSet resultDataSet = DbEngine.ExecuteDataSet("SELECT EC.EQUIPMENTCATEGORY TC FROM [BUSDTA].TBLEQUIPMENT E INNER JOIN [BUSDTA].TBLEQUIPMENTCATEGORY EC ON EC.EQUIPMENTCATEGORYID = E.EQUIPMENTCATEGORYID  where E.ProspectID=" + ProspectID + " AND LTRIM(RTRIM(E.EQUIPMENTTYPE))=" + EquipmentType + " AND EC.EQUIPMENTCATEGORY LIKE'%" + SearchTerm + "%'");
                if (resultDataSet.HasData())
                {
                    foreach (DataRow dataRow in resultDataSet.Tables[0].Rows)
                    {
                        serchList.Add(dataRow["TC"].ToString().Trim());
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerManager][GetStateCodes][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return serchList;
        }

    }

}
