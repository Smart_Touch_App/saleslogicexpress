﻿using System;
using System.Collections.Generic;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using log4net;
using System.Data;
using System.Linq;
using System.Diagnostics;
using System.Collections.ObjectModel;
namespace SalesLogicExpress.Application.Managers
{
    public class TransactionManager
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.TransactionManager");
        public Dictionary<string, ActivityTypeInfo> ActivityKeys { get; set; }
        public TransactionManager()
        {
            ActivityKeys = new Dictionary<string, ActivityTypeInfo>();
            LoadActivityKeys();
        }
        void LoadActivityKeys()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LoadActivityKeys]");
            try
            {
                string query = "select  ntx.TNTYP as 'TransactionType',ntx.TNKEY as ActivityKey ,'NonTransactionActivity' as [Type], ntx.TNILDGRD as IsLedgered, ntx.TNACTN as 'Description', 0 as 'IsStart', 0 as 'IsEnd'    from busdta.M5002 ntx ";
                query = query + " union select  tx.TTTYP as 'TransactionType', tx.TTKEY as ActivityKey ,'TransactionActivity' as [Type],1 as 'IsLedgered' , tx.TTACTN as 'Description', tx.TTISTRT as IsStart, tx.TTIEND as IsEnd    from busdta.M5001 tx";
                DataSet result = DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    foreach (DataRow item in result.Tables[0].Rows)
                    {
                        string ttkey = item["ActivityKey"].ToString().Trim();

                        if (!ActivityKeys.ContainsKey(ttkey))
                        {
                            ActivityKeys.Add(item["ActivityKey"].ToString().Trim(), new ActivityTypeInfo
                            {
                                ActivityKey = item["ActivityKey"].ToString().Trim().ParseEnum<ActivityKey>(ActivityKey.None),
                                IsLedgered = item["IsLedgered"].ToString().Trim().Length > 0 ? Convert.ToBoolean(Convert.ToInt32(item["IsLedgered"].ToString().Trim())) : false,
                                Description = item["Description"].ToString().Trim(),
                                TransactionType = item["TransactionType"].ToString().Trim(),
                                ActivityType = item["Type"].ToString().Trim().Equals("NonTransactionActivity") ? ActivityType.NonTransaction : ActivityType.Transaction,
                                IsStart = item["IsStart"].ToString().Trim().Length > 0 ? Convert.ToBoolean(Convert.ToInt32(item["IsStart"].ToString().Trim())) : false,
                                IsEnd = item["IsEnd"].ToString().Trim().Length > 0 ? Convert.ToBoolean(Convert.ToInt32(item["IsEnd"].ToString().Trim())) : false,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][LoadActivityKeys][ExceptionStackTrace = " + ex.InnerException.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:LoadActivityKeys]");
        }

        public void Initialize()
        {
        }

        public Activity LogActivity(Activity Activity)
        {
            //Stopwatch LogActivityTimer = Stopwatch.StartNew();

            Activity returnActivity = null;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LogActivity][Activity.ActivityType=" + Activity.ActivityType + "]");
                Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LogActivity][Activity.ActivityEnd=" + Activity.ActivityEnd + "]");
                //Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:LogActivity][Activity=" + Activity.SerializeToJson() + "]");
                string query = string.Empty;
                string activityID = string.Empty;
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                ActivityTypeInfo activityInfo = ActivityKeys[Activity.ActivityType.ToString().Trim()];
                //Activity.ActivityEnd = DateTime.Now;
                if (activityInfo.ActivityType == ActivityType.Transaction)
                {
                    bool addNewActivity = true;
                    if (activityInfo.IsStart && activityInfo.IsEnd && !string.IsNullOrEmpty(Activity.StopInstanceID))
                    {
                        query = string.Format("select count(*) from busdta.M50012 where TDTYP='{0}' and TDSTID='{1}' and TDAN8='{2}' and TDTYP='PreOrder'", Activity.ActivityType, Activity.StopInstanceID, Activity.CustomerID);
                        string count = DbEngine.ExecuteScalar(query).ToString();
                        if (count.ToString() != "0")
                        {
                            query = "Update busdta.M50012 set TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + " " +
                                     " ,TDSTAT = '" + activityInfo.ActivityKey.ToString() + "', TDDTLS=?,TDCLASS='" + Activity.ActivityDetailClass + "',TDREFHDRID='" + Activity.ActivityFlowID + "' " +
                                     " where TDSTID='" + Activity.StopInstanceID + "'  AND TDTYP!='NOACTIVITY'";
                            // used SACommand for inserting blob, inserting blobs needs to done using parameters
                            iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                            iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                            parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongNVarchar;
                            command.Parameters.Add(parm);
                            command.Parameters[0].Value = Activity.ActivityDetails;
                            new DB_CRUD().InsertUpdateData(command);
                            addNewActivity = false;
                        }
                    }
                    if (addNewActivity && activityInfo.IsStart || (activityInfo.IsEnd && Activity.ActivityHeaderID != null))
                    {
                        query = "select GET_IDENTITY('busdta.M50012')";
                        activityID = DbEngine.ExecuteScalar(query).ToString();
                        query = "INSERT INTO busdta.M50012(TDID,TDROUT,TDTYP,TDCLASS,TDDTLS,TDSTRTTM,TDENDTM,TDSTID,TDAN8,TDSTTLID,TDPNTID,TDSTAT,TDREFHDRID)VALUES (";
                        query = query + activityID + ",'";
                        query = query + Activity.RouteID + "','";
                        query = query + (!string.IsNullOrEmpty(Activity.ActivityHeaderID) ? Activity.ActivityType : activityInfo.TransactionType.ToString()) + "','";
                        //query = query + activityInfo.TransactionType.ToString() + "','"; //Stores the actvity entity type
                        query = query + Activity.ActivityDetailClass + "',";
                        query = query + "?,";
                        //query = query + Activity.ActivityDetails + "',";
                        query = query + Activity.ActivityStart.GetFormattedDateTimeForDb() + ",";
                        query = query + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",'";
                        query = query + Activity.StopInstanceID + "','";
                        query = query + Activity.CustomerID + "','";
                        query = query + Activity.SettlementID + "','";
                        query = query + Activity.ActivityHeaderID + "'";
                        query = query + ",''";
                        query = query + ",'" + Activity.ActivityFlowID + "'";
                        query = query + ");";

                        // used SACommand for inserting blob, inserting blobs needs to done using parameters
                        iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                        iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongNVarchar;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = Activity.ActivityDetails;
                        new DB_CRUD().InsertUpdateData(command);
                        //DbEngine.ExecuteNonQuery(query);
                        query = "select TDID as ActivityID, 1 as IsTxActivity,TDID as ActivityHeaderID, TDROUT AS RouteID,TDTYP as ActivityType,TDCLASS as ActivityDetailClass,TDDTLS as ActivityDetails,TDSTRTTM as ActivityStart,TDENDTM as ActivityEnd,TDSTID as StopInstanceID, TDAN8 as CustomerID,TDSTTLID as SettlementID,TDPNTID as ParentID,TDSTAT as ActivityStatus from busdta.M50012 where TDID='" + activityID + "'";
                        DataSet ds = DbEngine.ExecuteDataSet(query);
                        if (ds.HasData())
                        {
                            returnActivity = ds.Tables[0].Rows[0].GetEntity<Activity>();
                        }
                        Activity.ActivityID = activityID;
                        if (activityInfo.IsEnd)
                        {
                            query = "Update busdta.M50012 set TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",TDSTAT = '" + activityInfo.ActivityKey.ToString() + "'  where TDID='" + activityID + "'";
                            // used SACommand for inserting blob, inserting blobs needs to done using parameters
                            iAnywhere.Data.SQLAnywhere.SACommand cmd = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                            iAnywhere.Data.SQLAnywhere.SAParameter param = new iAnywhere.Data.SQLAnywhere.SAParameter();
                            param.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongNVarchar;
                            cmd.Parameters.Add(param);
                            cmd.Parameters[0].Value = Activity.ActivityDetails;
                            new DB_CRUD().InsertUpdateData(cmd);
                        }
                    }
                    if (activityInfo.IsEnd && !string.IsNullOrEmpty(Activity.ActivityHeaderID))
                    {
                        query = "Update busdta.M50012 set TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",TDSTAT = '" + activityInfo.ActivityKey.ToString() + "', TDDTLS=?,TDCLASS='" + Activity.ActivityDetailClass + "'  where TDID='" + Activity.ActivityHeaderID + "'";
                        // used SACommand for inserting blob, inserting blobs needs to done using parameters
                        iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                        iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongNVarchar;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = Activity.ActivityDetails;
                        new DB_CRUD().InsertUpdateData(command);
                    }
                    else if (!string.IsNullOrEmpty(Activity.ActivityHeaderID))
                    {
                        query = "Update busdta.M50012 set  TDENDTM=" + Activity.ActivityEnd.GetFormattedDateTimeForDb() + ",  TDSTAT = '" + activityInfo.ActivityKey.ToString() + "', TDDTLS=?,TDCLASS='" + Activity.ActivityDetailClass + "' where TDID='" + Activity.ActivityHeaderID + "'";
                        // used SACommand for inserting blob, inserting blobs needs to done using parameters
                        iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                        iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                        parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongNVarchar;
                        command.Parameters.Add(parm);
                        command.Parameters[0].Value = Activity.ActivityDetails;
                        new DB_CRUD().InsertUpdateData(command);
                    }


                }
                // Get the ActivityID that will be assigned to this entry, Will be used as HeaderID
                //query = "select GET_IDENTITY('busdta.ActivityLedger')";
                //activityID = DbEngine.ExecuteScalar(query).ToString();

                // Insert into TransactionActivityDetail
                query = "INSERT INTO busdta.M5003(ALHDID,ALTMSTMP,ALDSC,ALTYP,ALITX)VALUES ('";
                query = query + Activity.ActivityHeaderID + "',";
                query = query + Activity.ActivityStart.GetFormattedDateTimeForDb() + ",'";
                query = query + Activity.ActivityDescription + "','";
                query = query + Activity.ActivityType + "',";
                query = query + Convert.ToInt32(Activity.IsTxActivity);
                query = query + ");";
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][LogActivity][Activity=" + Activity.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                return returnActivity;
            }
            EventArgs e = new EventArgs();
            OnTransactionLogged(e);
            //Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:LogActivity][Activity=" + Activity.SerializeToJson() + "]");
            //LogActivityTimer.Stop();
            //if (Activity.ActivityType == ActivityKey.PickItem.ToString())
            //{
            //    Logger.Error("TransactionManager LogActivity Time taken :" + LogActivityTimer.Elapsed.TotalMilliseconds + "ms");
            //}
            return returnActivity;
        }
        public void AddNonTxActivity(Activity Activity)
        {
            try
            {
                string query = "INSERT INTO busdta.M5003(ALHDID,ALTMSTMP,ALDSC,ALTYP,ALITX)VALUES ('";
                query = query + Activity.ActivityHeaderID + "',";
                query = query + Activity.ActivityStart.GetFormattedDateTimeForDb() + ",'";
                query = query + Activity.ActivityDescription + "','";
                query = query + Activity.ActivityType + "',";
                query = query + Convert.ToInt32(Activity.IsTxActivity);
                query = query + ");";
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][LogActivity][Activity=" + Activity.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException.Message + "]");
            }
        }
        public void ResetActivity(string transactionID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][Start:ResetActivity][transactionID=" + transactionID + "]");
            try
            {
                string query = string.Format("update busdta.M50012 set TDENDTM=getdate() WHERE TDID='{0}'", transactionID);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][ResetActivity][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][TransactionManager][End:ResetActivity][transactionID=" + transactionID + "]");
        }

        public static void VoidPayment(string tdid, string reasonCodeId, string customerId, string routeId)
        {
            string query = "  update BUSDTA.M50012 set tdstat ='PaymentVoid' where tdtyp='payment' and tdid=  '" + tdid + "' ";
            DbEngine.ExecuteNonQuery(query);

            string receiptId = DbEngine.ExecuteScalar("SELECT REPLACE(REPLACE(LEFT(TDDTLS, CHARINDEX(',',TDDTLS)),'{\"TransactionID\":\"receipt#',''),'\",','') TDDTLS FROM BUSDTA.M50012 WHERE TDID='" + tdid + "'").ToString();

            if (!string.IsNullOrEmpty(receiptId))
            {
                query = string.Format("UPDATE BUSDTA.Receipt_Header SET StatusId=6, ReasonCodeId=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE ReasonCodeDescription='{0}' AND ReasonCodeType='Void Receipt') WHERE CustomerId={1} AND ReceiptId={2} AND RouteId={3}", reasonCodeId, customerId, receiptId, routeId);
                DbEngine.ExecuteNonQuery(query);

                query = string.Format("UPDATE BUSDTA.Customer_Ledger SET IsActive='0' WHERE CustomerId={0} AND ReceiptId={1} AND RouteId={2}", customerId, receiptId, routeId);
                DbEngine.ExecuteNonQuery(query);

            }

        }
        protected virtual void OnTransactionLogged(EventArgs e)
        {
            EventHandler<EventArgs> handler = TransactionLogged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public event EventHandler<EventArgs> TransactionLogged;

        public static string GetPaymentTransactionID(string orderTransID)
        {
            string transID = string.Empty;
            string query = "  select TDID from  BUSDTA.M50012  where TDTYP='payment'   and TDPNTID='" + orderTransID + "' ORDER BY TDID DESC  ";
            transID = DbEngine.ExecuteScalar(query);
            return transID;
        }

        /// <summary>
        /// Voids the invoices generated from the specified order 
        /// </summary>
        /// <param name="orderId">Order id</param>
        public void UpdateOrderInvoiceStatus(string orderId)
        {
            string query = "";

            try
            {
                if (string.IsNullOrEmpty(orderId))
                    return;

                query = "UPDATE BUSDTA.Invoice_Header SET DeviceStatusID=(SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID')";
                query = query + " FROM BUSDTA.Invoice_Header IH INNER JOIN BUSDTA.ORDER_HEADER OH";
                query = query + " ON IH.CustomerId=OH.CustomerId AND IH.OrderId=OH.OrderID";
                query = query + " AND IH.RouteId=OH.RouteId ";
                query = query + " WHERE OH.OrderID=" + orderId;

                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][UpdateOrderInvoiceStatus][OrderNo=" + orderId + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }

        /// <summary>
        /// Gets the complete status of EOD activity 
        /// </summary>
        /// <returns>True if EOD completed, blank as otherwise</returns>
        public string GetEODCompleteStatus()
        {
            string EODStatus = "";
            try
            {
                string query = "SELECT 'TRUE' AS ISEODDONETODAY FROM BUSDTA.Route_Settlement";
                query = query + "\nWHERE DATEFORMAT(SettlementDateTime, 'dd-mmm-yyyy')=DATEFORMAT(getdate(), 'dd-mmm-yyyy')";
                query = query + "\nAND Status IN (SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='VERF')";

                EODStatus = DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][TransactionManager][GetEODCompleteStatus][ExceptionStackTrace = " + ex.ToString() + "]");
            }

            return EODStatus; 
        }
    }

    public class ActivityTypeInfo
    {
        public ActivityType ActivityType { get; set; }
        public ActivityKey ActivityKey { get; set; }
        public bool IsLedgered { get; set; }
        public bool IsStart { get; set; }
        public bool IsEnd { get; set; }
        public string Description { get; set; }
        public string ParentTransaction { get; set; }
        public string TransactionType { get; set; }
    }
}
