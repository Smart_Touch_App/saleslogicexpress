﻿using System;
using System.Linq;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.Helpers;
using System.Collections.Generic;
using log4net;
using SalesLogicExpress.Application.ViewModels;
using System.Configuration;
using System.Globalization;

namespace SalesLogicExpress.Application.Managers
{
    class ServiceRouteManager : IDisposable
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ServiceRouteManager");
        string _basedate = ConfigurationManager.AppSettings["BaseDate"].ToString();
        string _FutureStopsToGenerate = ConfigurationManager.AppSettings["FutureStopsToGenerate"].ToString();
        int yearsToAdd = Convert.ToInt32(ConfigurationManager.AppSettings["YearsToAdd"].ToString());
        public static Customer GetCustomerForRoute(string routeID, string customerID)
        {
            string customerByRouteQuery = "";
            customerByRouteQuery = customerByRouteQuery + "SELECT AIZON,AISTOP as DeliveryCode,a.aban8 AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , ";
            customerByRouteQuery = customerByRouteQuery + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
            customerByRouteQuery = customerByRouteQuery + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
            customerByRouteQuery = customerByRouteQuery + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip ";
            customerByRouteQuery = customerByRouteQuery + "FROM busdta.F0101 AS a ";
            customerByRouteQuery = customerByRouteQuery + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
            customerByRouteQuery = customerByRouteQuery + "JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 ";
            customerByRouteQuery = customerByRouteQuery + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
            customerByRouteQuery = customerByRouteQuery + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
            customerByRouteQuery = customerByRouteQuery + "where AIZON>' ' and AISTOP>' ' and a.aban8='" + customerID + "'";

            DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerByRouteQuery);

            Customer customer = null;

            ObservableCollection<Customer> customersCollection = new ObservableCollection<Customer>();
            if (queryResult.HasData())
            {
                string[] deliveryCodes = new string[] { "11", "1A", "1E" };
                Random randomCode = new Random();
                customer = new Customer
                {
                    CustomerNo = queryResult.Tables[0].Rows[0]["CustomerNo"].ToString(),
                    Phone = queryResult.Tables[0].Rows[0]["Phone#"].ToString(),
                    Address = queryResult.Tables[0].Rows[0]["Address"].ToString(),
                    City = queryResult.Tables[0].Rows[0]["City"].ToString(),
                    Name = queryResult.Tables[0].Rows[0]["Name"].ToString(),
                    Zip = queryResult.Tables[0].Rows[0]["Zip"].ToString(),
                    State = queryResult.Tables[0].Rows[0]["State"].ToString(),
                    Shop = queryResult.Tables[0].Rows[0]["Bill_To_Name"].ToString(),
                    SequenceNo = 0,
                    DeliveryCode = queryResult.Tables[0].Rows[0]["DeliveryCode"].ToString(),
                    Route = queryResult.Tables[0].Rows[0]["Route"].ToString(),
                    RouteBranch = queryResult.Tables[0].Rows[0]["Route_Branch"].ToString(),
                    BillType = queryResult.Tables[0].Rows[0]["Billto_Type"].ToString(),
                };
            }
            return customer;
        }
        string _baseDate = ConfigurationManager.AppSettings["BaseDate"].ToString();
        string routeId = string.Empty;
        DateTime timer, timer2 = DateTime.Now;

        /// <summary>
        /// Get all customer which come under a route
        /// </summary>
        /// <param name="routeID">Route ID</param>
        /// <returns>Observable collection of customer class object</returns>
        /// 

        public ObservableCollection<Customer> GetCustomersForRoute(string routeID, DateTime stopDate)
        {
            ObservableCollection<Customer> customersCollection = new ObservableCollection<Customer>();
            ObservableCollection<Customer> sortedCollection = new ObservableCollection<Customer>();
            timer = DateTime.Now;
            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetCustomersForRoute][routeID=" + routeID + "][date=" + stopDate + "]");
            log.Info("[ServiceRouteManager][Start:GetCustomersForRoute][FunctionStartTime:]\t" + DateTime.Now + "");
            try
            {
                log.Info("[ServiceRouteManager][GetCustomersForRoute][QueryStart:]\t" + (DateTime.Now) + "");
                routeId = routeID;
                string intDate = stopDate.ToString("yyyyMMdd");
                string dailyStopsExists = "";
                string checkDailyStops = string.Format("SELECT COUNT(1) FROM BUSDTA.M56M0004 WHERE RPSTDT = CAST('{0}' AS DATE) and RPROUT='{1}'", intDate, routeID);
                dailyStopsExists = DbEngine.ExecuteScalar(checkDailyStops);
                //int resultRowsCount = 0;
                DateTime baseDate = Convert.ToDateTime(_baseDate);
                if (!(Convert.ToInt32(dailyStopsExists) > 0))
                {
                    // Generate Daily stop data in advance.
                    string stopQuery = string.Empty;
                    stopQuery = "CALL BUSDTA.generateDailyStopData(SelectedDate = '" + _basedate + "',BaseDate = '" + _basedate + "',FutureDays = " + _FutureStopsToGenerate + ")";
                    DataSet dataSet = DbEngine.ExecuteDataSet(stopQuery);
                }
                string customerByRouteQuery = "";

                customerByRouteQuery += @"select rss.RPSTID 'StopID', rss.RPROUT as 'Route',ab.ABMCU as 'Route_Branch', rss.RPAN8 as 'CustomerId', ab.ABALPH as 'CustomerName', rss.RPSTDT as 'StopDate',(select RPSTDT from busdta.M56M0004 where RPSTID = rss.RPRSTID and rss.rpsttp = 'Moved') as 'RecheduledDate',";
                customerByRouteQuery += " rss.RPOGDT as 'OriginalDate', ab.aban81  as 'BillToName', ab.abat1 as 'BillToType',cm.AISTOP as 'DeliveryCode',dmddcd as 'DeliveryCodeDesc',cm.AIEXHD as CreditHold, ";
                customerByRouteQuery += @" (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = (rss.RPAN8) AND RPSTTP <> 'Moved' AND RPSTDT > CAST('" + intDate + "' as date) order by 1) as NextStop, ";//" isnull(BUSDTA.getNextStopDate(rss.RPAN8,GETDATE(),'" + baseDate.ToString("yyyy-MM-dd") + "',1),NULL ) as NextStop, ";
                customerByRouteQuery += @" (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = (rss.RPAN8) AND RPSTTP <> 'Moved' AND RPSTDT < CAST('" + intDate + "' as date) order by 1 desc) as LastStop, ";//" BUSDTA.getLastStopDate(rss.RPAN8,GETDATE(),'" + baseDate.ToString("yyyy-MM-dd") + "',1) as LastStop, ";
                customerByRouteQuery += @" cm.AITRAR as 'TransMode', cm.AITRAR as 'PaymentMode','' as 'PaymentModeDescription', rtrim(d.aladd1) as 'AddressLine1',";
                customerByRouteQuery += " rtrim(d.aladd2) as 'AddressLine2', rtrim(d.aladd3) as 'AddressLine3', rtrim(d.aladd4) as 'AddressLine4', busdta.GetDefaultPhone(ab.ABAN8) as 'PhoneNumber',";
                customerByRouteQuery += " rtrim(d. alcty1) as 'City', rtrim(d.aladds) as 'State', rtrim(d. aladdz) as 'Zip', rss.RPSN as 'SequenceNumber', rss.RPVTTP as 'VisiteeType', 0 as 'Company',rss.RPSTTP as 'StopType',";
                customerByRouteQuery += " RPRSTID as 'ReferenceStopId', RPISRSN as 'IsResquence', RPRCID as 'NoSaleReasonId', RPACTID as 'SaleStatus',RPUPDT,RPCACT,RPPACT  ";
                customerByRouteQuery += " ,cm.aitrar as 'Payment Code',f.PNPTD  as 'Payment Desc'  ";
                customerByRouteQuery += @" from BUSDTA.M56M0004 rss ";
                customerByRouteQuery += " INNER JOIN BUSDTA.F03012 cm ON rss.RPAN8 = cm.AIAN8 ";
                customerByRouteQuery += " LEFT OUTER JOIN BUSDTA.F0101 ab ON rss.RPAN8 = ab.ABAN8 ";
                customerByRouteQuery += " LEFT OUTER JOIN busdta.F0116 d ON RSS.RPAN8=d.alan8 ";
                customerByRouteQuery += " LEFT OUTER JOIN busdta.M56M0001  ON cm.aistop=dmddc ";
                customerByRouteQuery += " LEFT OUTER JOIN busdta.F0014 f ON cm.aitrar =f.pnptc  ";
                customerByRouteQuery += " LEFT OUTER JOIN BUSDTA.Customer_Route_Map crm on aian8 = crm.CustomerShipToNumber ";
                customerByRouteQuery += @" where rss.RPSTDT ='" + stopDate.ToString("yyyy-MM-dd") + "' and cm.AISTOP <> '7'";
                customerByRouteQuery += " and crm.IsActive = 'Y' and crm.RelationshipType = 'AN85' ";
                customerByRouteQuery += @" union select rss.RPSTID 'StopID', rss.RPROUT as 'Route',pm.RouteId as 'Route_Branch', rss.RPAN8 as 'CustomerId', pm.ProspectName as 'CustomerName',  rss.RPSTDT as 'StopDate',";
                customerByRouteQuery += " (select RPSTDT from busdta.M56M0004 where RPSTID = rss.RPRSTID and rss.rpsttp = 'Moved') as 'RecheduledDate', rss.RPOGDT as 'OriginalDate', ''  as 'BillToName', '' as 'BillToType','' as 'DeliveryCode','' as 'DeliveryCodeDesc','' as CreditHold,";
                customerByRouteQuery += " null as NextStop, null as LastStop, '' as 'TransMode','' as 'PaymentMode','' as 'PaymentModeDescription',rtrim(pa.AddressLine1) as 'AddressLine1', rtrim(pa.AddressLine2) as 'AddressLine2', rtrim(pa.AddressLine3) as 'AddressLine3', rtrim(pa.AddressLine4) as 'AddressLine4', busdta.GetDefaultPhone(pm.ProspectId) as 'PhoneNumber', rtrim(pa.CityName) as 'City', rtrim(pa.StateName) as 'State',";
                customerByRouteQuery += @" rtrim(pa.ZipCode) as 'Zip', rss.RPSN as 'SequenceNumber', rss.RPVTTP as 'VisiteeType', 0 as 'Company',";
                customerByRouteQuery += " rss.RPSTTP as 'StopType', RPRSTID as 'ReferenceStopId', RPISRSN as 'IsResquence', RPRCID as 'NoSaleReasonId',";
                customerByRouteQuery += " RPACTID as 'SaleStatus',RPUPDT,RPCACT,RPPACT   , ";
                customerByRouteQuery += " '' as 'Payment Code',''  as 'Payment Desc'  ";
                customerByRouteQuery += " from BUSDTA.M56M0004 rss INNER JOIN BUSDTA.Prospect_Master pm ON rss.RPAN8 = pm.prospectid ";
                customerByRouteQuery += @"LEFT OUTER JOIN busdta.Prospect_Address pa ON RSS.RPAN8=pa.ProspectId  where rss.RPSTDT ='" + stopDate.ToString("yyyy-MM-dd") + "' order by SequenceNumber";
                DataSet queryResult = Helpers.DbEngine.ExecuteDataSet(customerByRouteQuery);
                DataSet todaysOrders = Helpers.DbEngine.ExecuteDataSet("select * from busdta.ORDER_HEADER oh where datepart(month,oh.CreatedDatetime) = datepart(month,cast('" + stopDate.ToString("yyyyMMdd") + "'as date)) and datepart(day,oh.OrderDate) = datepart(day,cast('" + stopDate.ToString("yyyyMMdd") + "'as date))");
                log.Info("[ServiceRouteManager][GetCustomersForRoute][QueryEnd:]\t" + (DateTime.Now - timer) + "");

                timer2 = DateTime.Now;
                log.Info("[ServiceRouteManager][GetCustomersForRoute][DataInitializationStart:]\t" + DateTime.Now + "");

                if (queryResult.HasData())
                {
                    string[] deliveryCodes = new string[] { "11", "1A", "1E" };
                    Random randomCode = new Random();
                    Customer customer;
                    for (var index = 1; index <= queryResult.Tables[0].Rows.Count; index++)
                    {
                        DataRow customerDataRow = queryResult.Tables[0].Rows[index - 1];
                        customer = new Customer();

                        customer.StopID = customerDataRow["StopID"].ToString().Trim();
                        customer.StopDate = string.IsNullOrEmpty(customerDataRow["StopDate"].ToString()) ? new DateTime() : Convert.ToDateTime(customerDataRow["StopDate"]);
                        customer.IsTodaysStop = (DateTime.Compare(customer.StopDate.Value, DateTime.Today.Date) == 0) ? true : false;
                        customer.StopType = customerDataRow["StopType"].ToString().Trim();
                        if (!(customerDataRow["OriginalDate"].ToString() == "" || customerDataRow["OriginalDate"].ToString() == null))
                        {
                            customer.OriginalDate = Convert.ToDateTime(customerDataRow["OriginalDate"]);
                        }
                        else { customer.OriginalDate = null; }
                        if (!(customerDataRow["RecheduledDate"].ToString() == "" || customerDataRow["RecheduledDate"].ToString() == null))
                        {
                            customer.RescheduledDate = Convert.ToDateTime(customerDataRow["RecheduledDate"]);
                        }
                        else { customer.RescheduledDate = null; }

                        customer.ReferenceStopId = customerDataRow["ReferenceStopId"].ToString().Trim();

                        customer.PreviousStop = string.IsNullOrEmpty(customerDataRow["LastStop"].ToString()) ? "" : customerDataRow["LastStop"].ToString().Trim();
                        if (!string.IsNullOrEmpty(customerDataRow["NextStop"].ToString()))
                        {
                            customer.NextStop = Convert.ToDateTime(customerDataRow["NextStop"]).ToString("MM'/'dd'/'yyyy");
                        }
                        else
                        {
                            customer.NextStop = "";
                        }


                        customer.CustomerNo = customerDataRow["CustomerId"].ToString().Trim();
                        customer.Phone = customerDataRow["PhoneNumber"].ToString().Trim();
                        customer.Address = customerDataRow["AddressLine1"].ToString().Trim() + " " + customerDataRow["AddressLine2"].ToString().Trim() + " " + customerDataRow["AddressLine3"].ToString().Trim() + " " + customerDataRow["AddressLine4"].ToString().Trim();
                        customer.City = customerDataRow["City"].ToString().Trim();
                        customer.Name = customerDataRow["CustomerName"].ToString().Trim();
                        customer.Zip = customerDataRow["Zip"].ToString().Trim();
                        customer.State = customerDataRow["State"].ToString().Trim();
                        customer.Shop = customerDataRow["BillToName"].ToString().Trim();
                        customer.SequenceNo = Convert.ToInt32(customerDataRow["SequenceNumber"].ToString().Trim());
                        customer.TempSequenceNo = Convert.ToInt32(customerDataRow["SequenceNumber"].ToString().Trim());
                        customer.DeliveryCode = customerDataRow["DeliveryCode"].ToString().Trim();
                        customer.DeliveryCodeDescription = customerDataRow["DeliveryCodeDesc"].ToString().Trim();
                        customer.Route = customerDataRow["Route"].ToString().Trim();
                        customer.RouteBranch = customerDataRow["Route_Branch"].ToString().Trim();
                        customer.BillType = customerDataRow["BillToType"].ToString().Trim();
                        customer.VisiteeType = customerDataRow["VisiteeType"].ToString().Trim();
                        customer.IsCustomerOnAccount = !string.IsNullOrEmpty(customerDataRow["TransMode"].ToString()) && customerDataRow["TransMode"].ToString().Trim() == "CSH" ? false : true;
                        customer.Company = customerDataRow["Company"].ToString().Trim();
                        customer.IsAllowCaptureDrag = (stopDate.Date >= DateTime.Now.Date) ? true : false;
                        customer.IsResquence = Convert.ToBoolean(customerDataRow["IsResquence"].ToString());
                        customer.PaymentMode = customerDataRow["PaymentMode"].ToString().Trim();
                        customer.PaymentModeDescription = customerDataRow["PaymentModeDescription"].ToString().Trim();
                        customer.PaymentModeForList = customerDataRow["Payment Code"].ToString().Trim();
                        customer.PaymentModeDescriptionForList = customerDataRow["Payment Desc"].ToString().Trim();
                        customer.PaymentModeDescriptionForList = "(" + customer.PaymentModeForList + ") " + customer.PaymentModeDescriptionForList + "";


                        if (customerDataRow["CreditHold"].ToString() == "y" || customerDataRow["CreditHold"].ToString() == "Y")
                        {
                            customer.CreditHold = true;
                        }
                        if (customerDataRow["CreditHold"].ToString() == "n" || customerDataRow["CreditHold"].ToString() == "N")
                        {
                            customer.CreditHold = false;
                        }
                        customer.SaleStatus = string.IsNullOrEmpty(customerDataRow["SaleStatus"].ToString()) ? string.Empty : customerDataRow["SaleStatus"].ToString().Trim();
                        customer.SaleStatusReason = string.IsNullOrEmpty(customerDataRow["NoSaleReasonId"].ToString()) ? string.Empty : customerDataRow["NoSaleReasonId"].ToString().Trim();
                        int pAct = Convert.ToInt32(customerDataRow["RPPACT"].ToString());
                        int cAct = Convert.ToInt32(customerDataRow["RPCACT"].ToString());
                        customer.Activity = ((cAct + pAct) > 0 && string.IsNullOrEmpty(customer.SaleStatus)) ? true : false;
                        customer.PendingActivity = pAct;
                        customer.CompletedActivity = cAct;
                        if (((cAct + pAct) > 0) || (customer.SaleStatus.Trim().ToLower() == "nosale"))
                        {
                            customer.IsAllowCaptureDrag = false;
                        }

                        if (stopDate.Date > DateTime.Today.Date)
                        {
                            customer.Activity = false;
                            customer.PendingActivity = 0;
                            customer.CompletedActivity = 0;
                        }
                        if (customer.VisiteeType.ToLower() == "prospect") customer.PaymentModeDescriptionForList = "";
                        customer.CustName = customer.Name;
                        customersCollection.Add(customer);
                    }
                }

                var orderList = customersCollection.OrderBy(X => X.SequenceNo);
                customersCollection = new ObservableCollection<Customer>(orderList);
                bool isResq = false;
                var Resq = customersCollection.FirstOrDefault(t => t.StopType.Trim() == "Planned");
                if (Resq != null)
                {
                    isResq = Resq.IsResquence;
                }
                //Here sequencing stops as per requorement
                sortedCollection = SequenceStops(customersCollection, isResq, stopDate.Date.ToString("yyyy-MM-dd"));
                //sortedCollection = customersCollection.OrderBy(X => X.SequenceNo);
                if (customersCollection.Any())
                {
                    customersCollection.Clear();
                }


                log.Info("[ServiceRouteManager][GetCustomersForRoute][DataInitializationEnd:]\t" + (DateTime.Now - timer2) + "");
                log.Info("[ServiceRouteManager][End:GetCustomersForRoute][FunctionEndTime:]\t" + (DateTime.Now - timer) + "");
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetCustomersForRoute][routeID=" + routeID + "][date=" + stopDate + "]");
                return sortedCollection;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetCustomersForRoute][routeID=" + routeID + "][date=" + stopDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return sortedCollection;
            }
        }
        public ObservableCollection<Customer> SequenceStops(ObservableCollection<Customer> collectionToSort, bool isResquence, string stopDate)
        {
            ObservableCollection<Customer> sortedCollection = new ObservableCollection<Customer>();
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:SequenceStops][collectionToSort=" + collectionToSort + "]");
                int stopNumber = 1;
                ObservableCollection<Customer> activityCustomers = new ObservableCollection<Customer>();
                var result = collectionToSort.Where(x => x.PendingActivity > 0 || x.CompletedActivity > 0 || x.SaleStatus.Trim().ToLower() == "nosale");
                activityCustomers = new ObservableCollection<Customer>(result);
                int indexOfActivity = 0;

                #region Drag Drop Resquencing
                //Checking isResquence for determing wether drag drop has happened or not
                //because sequencing is different 
                int temp = 1;
                //Initially allow drop for all customers
                foreach (Customer cust in collectionToSort)
                {
                    cust.IsAllowDrop = true;
                }
                #region Set Temp Seq No
                foreach (Customer cust in collectionToSort)
                {
                    if (cust.StopType.Trim().ToLower() == "planned")
                    {
                        cust.TempSequenceNo = temp;
                        temp++;
                    }
                    else
                    {
                        cust.TempSequenceNo = 0;
                    }

                    sortedCollection.Add(cust);
                    stopNumber++;
                }
                #endregion
                #region Set llow drop property
                if (activityCustomers.Count > 0)
                {
                    indexOfActivity = sortedCollection.IndexOf(sortedCollection.FirstOrDefault(x => x.CustomerNo.Trim() == activityCustomers[0].CustomerNo.Trim()));
                }
                int idx = 0;
                foreach (Customer item in sortedCollection)
                {
                    //Set allow drop for unplanned

                    if (idx < indexOfActivity)
                    {
                        item.IsAllowDrop = false;
                    }
                    else if (idx > indexOfActivity)
                    {
                        item.IsAllowDrop = true;
                    }
                    if (item.PendingActivity > 0 || item.CompletedActivity > 0 || item.SaleStatus.Trim().ToLower() == "nosale")
                    {
                        item.IsAllowDrop = false;
                    }
                    idx++;
                }
                #endregion
                #endregion
                #region Update Sequence No
                //string userName = CommonNavInfo.UserName.Trim();
                //for (int i = 0; i < sortedCollection.Count; i++)
                //{
                //    string stopID = sortedCollection[i].StopID.Trim();
                //    string refStopID = sortedCollection[i].ReferenceStopId.Trim();
                //    UpadateSequenceNo(stopID, (i + 1).ToString(), userName, stopDate, refStopID);
                //}
                #endregion
                return sortedCollection;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][SequenceStops][collectionToSort=" + collectionToSort + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return sortedCollection;
            }

            //ObservableCollection<Customer> sortedCollection = new ObservableCollection<Customer>();
            //try
            //{
            //    log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:SequenceStops][collectionToSort=" + collectionToSort + "]");
            //    int stopNumber = 1;
            //    ObservableCollection<Customer> reschCustomers = new ObservableCollection<Customer>();
            //    ObservableCollection<Customer> unplannedCustomers = new ObservableCollection<Customer>();
            //    ObservableCollection<Customer> activityCustomers = new ObservableCollection<Customer>();
            //    ObservableCollection<Customer> plannedCustomers = new ObservableCollection<Customer>();

            //    var result = collectionToSort.Where(x => x.PendingActivity > 0 || x.CompletedActivity > 0 || x.SaleStatus.Trim().ToLower() == "nosale").OrderBy(x => x.ActivityTimeIndex);
            //    activityCustomers = new ObservableCollection<Customer>(result);
            //    int indexOfActivity = 0;

            //    if (isResquence)
            //    {
            //        #region Drag Drop Resquencing
            //        //Checking isResquence for determing wether drag drop has happened or not
            //        //because sequencing is different 
            //        int temp = 1;

            //        //Initially allow drop for all customers
            //        foreach (Customer cust in collectionToSort)
            //        {
            //            cust.IsAllowDrop = true;
            //        }

            //        #region Handle Customers with Activity
            //        if (activityCustomers.Count > 0)
            //        {
            //            int idndexToDropActivityCust = collectionToSort.IndexOf(collectionToSort.FirstOrDefault(x => x.CustomerNo.Trim() == activityCustomers[0].CustomerNo.Trim()));

            //            //Check if there is only one activity. if it is on unplannend or Reschedule then all R AND U cust should be above activity,
            //            //Remove activity customers from base collection i.e.collectionToSort
            //            foreach (Customer cust in activityCustomers)
            //            {
            //                collectionToSort.Remove(cust);
            //            }
            //            if (activityCustomers.Count == 1)
            //            {
            //                int indexOfFirstPlannedCust = 0;
            //                result = collectionToSort.Where(x => x.StopType.Trim().ToLower() == "planned").OrderBy(x => x.SequenceNo);
            //                plannedCustomers = new ObservableCollection<Customer>(result);
            //                result = collectionToSort.Where(x => x.StopType.Trim().ToLower() == "rescheduled").OrderBy(x => x.SequenceNo);
            //                reschCustomers = new ObservableCollection<Customer>(result);
            //                result = collectionToSort.Where(x => x.StopType.Trim().ToLower() == "unplanned").OrderBy(x => x.SequenceNo);
            //                unplannedCustomers = new ObservableCollection<Customer>(result);
            //                if (plannedCustomers.Count > 0)
            //                {
            //                    idndexToDropActivityCust = collectionToSort.IndexOf(collectionToSort.FirstOrDefault(x => x.CustomerNo.Trim() == plannedCustomers[0].CustomerNo.Trim()));
            //                }
            //                else
            //                {
            //                    idndexToDropActivityCust = collectionToSort.Count;
            //                }
            //            }
            //            // if idndexToDropActivityCust is 0 then start dropping activity customer from 0 th index
            //            // else there are rescheduled or unplanned customer which are at first index in list, in that case activity cust
            //            // has to be appennd after that rescheduled or unplanned
            //            if (idndexToDropActivityCust == 0)
            //            {
            //                foreach (Customer cust in activityCustomers.OrderByDescending(x => x.ActivityTimeIndex))
            //                {
            //                    //Dont allow activity customers drop
            //                    cust.IsAllowDrop = false;
            //                    collectionToSort.Insert(0, cust);
            //                }
            //            }
            //            else
            //            {
            //                for (int i = 0; i < collectionToSort.Count; i++)
            //                {
            //                    if (collectionToSort[i].StopType.ToLower() != "rescheduled" && collectionToSort[i].StopType.ToLower() != "unplanned" && collectionToSort[i].PendingActivity == 0 && collectionToSort[i].CompletedActivity == 0 && collectionToSort[i].SaleStatus.Trim().ToLower() != "nosale")
            //                    {
            //                        if (idndexToDropActivityCust > i)
            //                        {
            //                            idndexToDropActivityCust = i;
            //                            break;
            //                        }
            //                        else if (idndexToDropActivityCust <= i)
            //                        {
            //                            break;
            //                        }
            //                    }
            //                }
            //                foreach (Customer cust in activityCustomers.OrderByDescending(x => x.ActivityTimeIndex))
            //                {
            //                    cust.IsAllowDrop = false;
            //                    if (collectionToSort.Count >= idndexToDropActivityCust)
            //                        collectionToSort.Insert(idndexToDropActivityCust, cust);
            //                }
            //                for (int i = 0; i < idndexToDropActivityCust; i++)
            //                {
            //                    if (collectionToSort.Count > i)
            //                        collectionToSort[i].IsAllowDrop = false;
            //                }
            //            }
            //        }
            //        #endregion

            //        #region Set Temp Seq No
            //        foreach (Customer cust in collectionToSort)
            //        {
            //            if (cust.StopType.Trim().ToLower() == "planned")
            //            {
            //                cust.TempSequenceNo = temp;
            //                temp++;
            //            }
            //            else
            //            {
            //                cust.TempSequenceNo = 0;
            //            }

            //            sortedCollection.Add(cust);
            //            stopNumber++;
            //        }
            //        #endregion
            //        #region Set llow drop property
            //        if (activityCustomers.Count > 0)
            //        {
            //            indexOfActivity = sortedCollection.IndexOf(sortedCollection.FirstOrDefault(x => x.CustomerNo.Trim() == activityCustomers[0].CustomerNo.Trim()));
            //        }
            //        int idx = 0;
            //        foreach (Customer item in sortedCollection)
            //        {
            //            //Set allow drop for unplanned

            //            if (idx < indexOfActivity)
            //            {
            //                item.IsAllowDrop = false;
            //            }
            //            else if (idx > indexOfActivity)
            //            {
            //                item.IsAllowDrop = true;
            //            }
            //            if (item.PendingActivity > 0 || item.CompletedActivity > 0 || item.SaleStatus.Trim().ToLower() == "nosale")
            //            {
            //                item.IsAllowDrop = false;
            //            }
            //            idx++;
            //        }
            //        #endregion
            //        #endregion
            //    }
            //    else
            //    {
            //        #region Handle customers when drag drop has not happened
            //        //remove  Activity Customers from base collection to sort
            //        if (activityCustomers.Count > 0)
            //        {
            //            foreach (Customer cust in activityCustomers)
            //            {
            //                collectionToSort.Remove(cust);
            //            }
            //        }

            //        int indexToDropActivity = 0;
            //        //Add Rescheduled Customers to Final collection
            //        if (collectionToSort.Where(x => x.StopType.Trim().ToLower() == "rescheduled").Count() > 0)
            //        {
            //            result = collectionToSort.Where(x => x.StopType.Trim().ToLower() == "rescheduled").OrderBy(x => x.SequenceNo);
            //            reschCustomers = new ObservableCollection<Customer>(result);
            //            foreach (Customer cust in reschCustomers)
            //            {
            //                cust.IsAllowDrop = false;
            //                cust.TempSequenceNo = 0;
            //                sortedCollection.Add(cust);
            //                indexToDropActivity++;
            //            }
            //        }
            //        //Remove Rescheduled cust from Base Collection
            //        foreach (Customer cust in reschCustomers)
            //        {
            //            collectionToSort.Remove(cust);
            //        }
            //        reschCustomers.Clear();
            //        //Add Unplanned Customers to Final collection
            //        if (collectionToSort.Where(x => x.StopType.Trim().ToLower() == "unplanned").Count() > 0)
            //        {
            //            result = collectionToSort.Where(x => x.StopType.Trim().ToLower() == "unplanned").OrderBy(x => x.SequenceNo);
            //            unplannedCustomers = new ObservableCollection<Customer>(result);
            //            foreach (Customer cust in unplannedCustomers)
            //            {
            //                cust.TempSequenceNo = 0;
            //                cust.IsAllowDrop = false;
            //                sortedCollection.Add(cust);
            //                indexToDropActivity++;
            //            }
            //        }
            //        //Remove Unplanned cust from Base Collection
            //        foreach (Customer cust in unplannedCustomers)
            //        {
            //            collectionToSort.Remove(cust);
            //        }
            //        unplannedCustomers.Clear();

            //        //Add activity customers to Final Collection
            //        foreach (Customer cust in activityCustomers.OrderByDescending(x => x.ActivityTimeIndex))
            //        {
            //            //Dont allow activity customers drop
            //            cust.IsAllowDrop = false;
            //            sortedCollection.Insert(indexToDropActivity, cust);
            //        }
            //        //Add planned customers to final collection
            //        foreach (Customer cust in collectionToSort.OrderBy(x => x.SequenceNo))
            //        {
            //            cust.TempSequenceNo = stopNumber;
            //            cust.IsAllowDrop = true;
            //            sortedCollection.Add(cust);
            //            stopNumber++;
            //        }

            //        //Set temp Sequence number to final collection
            //        int temp1 = 1;
            //        foreach (Customer cust in sortedCollection)
            //        {
            //            if (cust.StopType.Trim().ToLower() == "planned")
            //            {
            //                cust.TempSequenceNo = temp1;
            //                temp1++;
            //            }
            //            else
            //            {
            //                cust.TempSequenceNo = 0;
            //            }
            //        }
            //        #region Set Drag Drop Properties
            //        if (activityCustomers.Count > 0)
            //        {
            //            indexOfActivity = sortedCollection.IndexOf(sortedCollection.FirstOrDefault(x => x.CustomerNo.Trim() == activityCustomers[0].CustomerNo.Trim()));
            //        }
            //        int idxx = 0;

            //        foreach (Customer item in sortedCollection)
            //        {
            //            if (idxx < indexOfActivity)
            //            {
            //                item.IsAllowDrop = false;
            //            }
            //            else if (idxx > indexOfActivity)
            //            {
            //                item.IsAllowDrop = true;
            //            }
            //            if (item.PendingActivity > 0 || item.CompletedActivity > 0 || item.SaleStatus.Trim().ToLower() == "nosale")
            //            {
            //                item.IsAllowDrop = false;
            //            }
            //            idxx++;
            //        }
            //        #endregion
            //        collectionToSort.Clear();
            //        log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:SequenceStops][collectionToSort=" + collectionToSort + "]");
            //        #endregion
            //    }
            //    #region Update Sequence No
            //    string userName = CommonNavInfo.UserName.Trim();
            //    for (int i = 0; i < sortedCollection.Count; i++)
            //    {
            //        string stopID = sortedCollection[i].StopID.Trim();
            //        string refStopID = sortedCollection[i].ReferenceStopId.Trim();
            //        UpadateSequenceNo(stopID, (i + 1).ToString(), userName, stopDate, refStopID);
            //    }
            //    #endregion
            //    return sortedCollection;
            //}
            //catch (Exception ex)
            //{
            //    log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][SequenceStops][collectionToSort=" + collectionToSort + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            //    return sortedCollection;
            //}
        }
        public bool MoveCustomerStop(Visitee visiteeObj, DateTime moveToDate, DateTime currentStopDate)
        {
            try
            {

                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:MoveCustomerStop][customerObj=" + visiteeObj + "][moveToDate=" + moveToDate + "][currentStopDate=" + currentStopDate + "]");
                DateTime orgDate = DateTime.Now.Date;
                int resultRowsCount = 0;
                string routeDate = moveToDate.ToString("yyyyMMdd");
                if (visiteeObj.OriginalDate == null)
                {
                    orgDate = currentStopDate;
                }
                else if (visiteeObj.StopType == "Rescheduled")
                {
                    orgDate = visiteeObj.OriginalDate.Value;
                }
                else
                {
                    orgDate = currentStopDate;
                }


                string dailyStopsExists = "";
                string originalStopID = "";
                string checkDailyStops = " SELECT COUNT(1) FROM BUSDTA.M56M0004 WHERE RPSTDT = CAST('" + routeDate + "' AS DATE)";
                dailyStopsExists = DbEngine.ExecuteScalar(checkDailyStops);

                DateTime baseDate = Convert.ToDateTime(_baseDate);
                if (!(Convert.ToInt32(dailyStopsExists) > 0))
                {
                    string poulateCustForRoute = "";

                    poulateCustForRoute = poulateCustForRoute + "insert into busdta.M56M0004(RPROUT, RPAN8, RPSTDT, RPSN, RPVTTP, RPSTTP,RPCRBY,RPCRDT,RPUPBY,RPUPDT)(select r.RSROUT, r.RSAN8, cast('" + routeDate + "'";
                    poulateCustForRoute = poulateCustForRoute + " as date), r.RSSN, 'Cust', 'Planned','" + CommonNavInfo.UserName.Trim() + "',GETDATE(),'" + CommonNavInfo.UserName.Trim() + "',GETDATE() ";
                    poulateCustForRoute = poulateCustForRoute + " from BUSDTA.F03012 c join BUSDTA.M56M0003 r on c.aian8 = r.RSAN8 join BUSDTA.M56M0002 d on d.DCDDC = c.AISTOP ";
                    poulateCustForRoute = poulateCustForRoute + " where   R.RSWN = BUSDTA.GetWeekForDate(CAST('" + routeDate + "' as date), CAST('" + baseDate.ToString("yyyyMMdd") + "' as date))";
                    poulateCustForRoute = poulateCustForRoute + " and d.DCDN = BUSDTA.GetDayForDate(CAST('" + routeDate + "' as date), CAST('" + baseDate.ToString("yyyyMMdd") + "'as date)))";

                    resultRowsCount = DbEngine.ExecuteNonQuery(poulateCustForRoute);

                }

                originalStopID = visiteeObj.ReferenceStopId;

                ExecuteMoveStopQuery(visiteeObj, moveToDate, orgDate, routeDate, originalStopID, currentStopDate);

                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:MoveCustomerStop][customerObj=" + visiteeObj + "][moveToDate=" + moveToDate + "][currentStopDate=" + currentStopDate + "]");
                return true;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][MoveCustomerStop][customerObj=" + visiteeObj + "][moveToDate=" + moveToDate + "][currentStopDate=" + currentStopDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return false;
            }
        }
        private void ExecuteMoveStopQuery(Visitee visiteeObj, DateTime moveToDate, DateTime orgDate, string routeDate, string originalStopID, DateTime pCurrentDateTime)
        {

            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:ExecuteMoveStopQuery][customerObj=" + visiteeObj + "][moveToDate=" + moveToDate + "]");
            try
            {

                if (!((DateTime.Compare(moveToDate.Date, orgDate.Date) == 0) && visiteeObj.StopType == "Rescheduled"))
                {

                    if (visiteeObj.StopType.Trim() == "Rescheduled")
                    {
                        originalStopID = visiteeObj.ReferenceStopId;

                        string delQuery = "delete from busdta.M56M0004 where RPSTID=" + visiteeObj.StopID.Trim() + "";
                        DbEngine.ExecuteNonQuery(delQuery);

                    }
                    else if (visiteeObj.StopType.Trim().ToLower() == "unplanned")
                    {
                        string delQuery = "delete from busdta.M56M0004 where RPSTID=" + visiteeObj.StopID.Trim() + "";
                        DbEngine.ExecuteNonQuery(delQuery);
                    }
                    else
                    {
                        originalStopID = visiteeObj.StopID.Trim();
                    }

                    string selctQuery = "SELECT RPSTID FROM BUSDTA.M56M0004 WHERE RPSTDT='" + moveToDate.ToString("yyyy-MM-dd") + "' ORDER BY RPSN";
                    DataSet ds = DbEngine.ExecuteDataSet(selctQuery);
                    if (ds.HasData())
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            string stopID = ds.Tables[0].Rows[i]["RPSTID"].ToString();
                            string updateSeqNo = "update BUSDTA.M56M0004 set RPSN= " + (i + 2) + " where RPSTID='" + stopID + "'";
                            DbEngine.ExecuteNonQuery(updateSeqNo);
                        }
                    }
                    string insertMovedStop = "";
                    if (visiteeObj.StopType.ToLower() != "unplanned")
                    {

                        insertMovedStop = insertMovedStop + "INSERT INTO BUSDTA.M56M0004 (RPROUT,RPAN8,RPSTDT,RPOGDT,RPSN,RPVTTP,RPSTTP,RPRSTID)";
                        insertMovedStop = insertMovedStop + " VALUES('" + CommonNavInfo.RouteUser.Trim() + "'," + visiteeObj.VisiteeId + ",cast('" + routeDate + "' as date),cast('" + orgDate.ToString("yyyyMMdd") + "' as date),1,'" + visiteeObj.VisiteeType + "','Rescheduled'," + originalStopID + ")";


                        DbEngine.ExecuteNonQuery(insertMovedStop);

                        string getUpdatedStopID = "select RPSTID from busdta.M56M0004 where RPRSTID=" + originalStopID;

                        string RPRSTID = DbEngine.ExecuteScalar(getUpdatedStopID);
                        string updateMovedStop = "";
                        updateMovedStop = "UPDATE BUSDTA.M56M0004 SET RPRSTID=" + RPRSTID + ",RPSTTP='Moved'";
                        updateMovedStop = updateMovedStop + " WHERE RPSTID=" + originalStopID + " AND RPROUT='" + CommonNavInfo.RouteUser.Trim() + "' AND RPAN8=" + visiteeObj.VisiteeId + " AND RPSTDT=cast('" + orgDate.ToString("yyyyMMdd") + "' as date)";
                        DbEngine.ExecuteNonQuery(updateMovedStop);
                        UpdatePreorder(visiteeObj.VisiteeId, pCurrentDateTime, moveToDate);
                    }
                    else
                    {
                        insertMovedStop = insertMovedStop + "INSERT INTO BUSDTA.M56M0004 (RPROUT,RPAN8,RPSTDT,RPSN,RPVTTP,RPSTTP)";
                        insertMovedStop = insertMovedStop + " VALUES('" + CommonNavInfo.RouteUser.Trim() + "'," + visiteeObj.VisiteeId + ",cast('" + routeDate + "' as date),1,'" + visiteeObj.VisiteeType + "','Unplanned')";
                        DbEngine.ExecuteNonQuery(insertMovedStop);

                    }
                }
                else
                {
                    string delQuery = "delete from busdta.M56M0004 where RPSTID=" + visiteeObj.StopID.Trim() + "";
                    DbEngine.ExecuteNonQuery(delQuery);

                    string updateStop = "";
                    updateStop = "UPDATE BUSDTA.M56M0004 SET RPOGDT=null,RPSTTP='Planned',RPRSTID = null,RPUPBY='" + CommonNavInfo.UserName + "',RPUPDT='" + DateTime.Now.ToString("yyyy-MM-dd") + "' ";
                    updateStop += " WHERE RPSTID=(select RPSTID FROM BUSDTA.M56M0004 where RPRSTID =" + visiteeObj.StopID + ") AND RPROUT='" + CommonNavInfo.RouteUser + "' AND RPAN8=" + visiteeObj.VisiteeId + " AND RPSTDT='" + moveToDate.ToString("yyyy-MM-dd") + "'";
                    DbEngine.ExecuteNonQuery(updateStop);
                    UpdatePreorder(visiteeObj.VisiteeId, pCurrentDateTime, moveToDate);

                }
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:ExecuteMoveStopQuery][customerObj=" + visiteeObj + "][moveToDate=" + moveToDate + "]");

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][ExecuteMoveStopQuery][customerObj=" + visiteeObj + "][moveToDate=" + moveToDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }
        public void UpdatePreorder(string pCustomerNo, DateTime pOrgDate, DateTime pMoveToDate)
        {
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:updatePreorder][pCustomerNo=" + pCustomerNo + "][pOrgDate=" + pOrgDate + "]");

                string query = "select count(1) from busdta.m4016 where POAN8 = " + pCustomerNo + " and POSTDT='" + pOrgDate.ToString("yyyy-MM-dd") + "'";
                string count = DbEngine.ExecuteScalar(query);
                if (!string.IsNullOrWhiteSpace(count))
                {
                    if (Convert.ToInt32(count) > 0)
                    {
                        DataSet dbSet = new DataSet();
                        string updateQuery = "select * from busdta.m4016 where POAN8 =" + pCustomerNo + " and POSTDT='" + pOrgDate.ToString("yyyy-MM-dd") + "'";
                        dbSet = DbEngine.ExecuteDataSet(updateQuery);
                        if (dbSet.HasData())
                        {
                            string delquery = "delete from busdta.m4016 where POAN8 =" + pCustomerNo + " and POSTDT='" + pOrgDate.ToString("yyyy-MM-dd") + "'";
                            DbEngine.ExecuteNonQuery(delquery);
                            foreach (DataRow row in dbSet.Tables[0].Rows)
                            {
                                DateTime createdDate = Convert.ToDateTime(row[13].ToString().Trim());
                                string insertQuery = "insert into busdta.m4016(POORTP,POAN8,POITM,POSTDT,POOSEQ,POLITM,POQTYU,POUOM,POLNTY,POSRP1,POSRP5,POCRBY,POCRDT,POUPBY,POUPDT) ";
                                insertQuery += "values('" + row[0].ToString().Trim() + "'," + row[1].ToString().Trim() + ",'" + row[2].ToString().Trim() + "','" + pMoveToDate.ToString("yyyy-MM-dd") + "'," + row[4].ToString().Trim() + "";
                                insertQuery += ",'" + row[5].ToString().Trim() + "'," + row[6].ToString().Trim() + ",'" + row[7].ToString().Trim() + "','" + row[8].ToString().Trim() + "','" + row[9].ToString().Trim() + "',";
                                insertQuery += "'" + row[10].ToString().Trim() + "','" + row[11].ToString().Trim() + "','" + createdDate.ToString("yyyy-MM-dd HH:mm:ss") + "','" + CommonNavInfo.UserName + "','" + DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss") + "')";
                                DbEngine.ExecuteNonQuery(insertQuery);
                            }
                        }
                    }
                }
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:updatePreorder][pCustomerNo=" + pCustomerNo + "][pOrgDate=" + pOrgDate + "]");

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][updatePreorder][pCustomerNo=" + pCustomerNo + "][pOrgDate=" + pOrgDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                throw;
            }
        }

        public List<DateTime> GetPresentStops(string visiteeId, DateTime startDate, DateTime endDate, bool callFromMoveStop)
        {
            List<DateTime> presentStops = new List<DateTime>();
            DateTime lastDateConsidered = DateTime.Now.AddYears(yearsToAdd);
            DateTime tempBaseDate = Convert.ToDateTime(_basedate);
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetPresentStops][customerNo=" + visiteeId + "][startDate=" + startDate + "][endDate=" + endDate + "]");
                string stopQuery = "select rpstdt from busdta.M56M0004 where rpan8= " + visiteeId + " and rpstdt BETWEEN '" + startDate.ToString("yyyy-MM-dd") + "' and '" + endDate.ToString("yyyy-MM-dd") + "' and rpsttp <> 'Moved'";

                DataSet dbSet = DbEngine.ExecuteDataSet(stopQuery);
                if (dbSet.HasData())
                {
                    foreach (DataRow row in dbSet.Tables[0].Rows)
                    {
                        if (!(row["rpstdt"].ToString() == null || row["rpstdt"].ToString() == ""))
                        {
                            presentStops.Add(Convert.ToDateTime(row["rpstdt"]));
                        }
                    }
                }
                if (!callFromMoveStop)
                {
                    stopQuery = string.Empty;
                    stopQuery = "SELECT RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = " + visiteeId + " AND RPSTTP <> 'Moved' AND RPSTDT > '" + startDate.ToString("yyyy-MM-dd") + "'";
                    DataSet dataSet = DbEngine.ExecuteDataSet(stopQuery);
                    if (dataSet.HasData())
                    {
                        foreach (DataRow row in dataSet.Tables[0].Rows)
                        {
                            if (!(row["RPSTDT"].ToString() == null || row["RPSTDT"].ToString() == ""))
                            {
                                presentStops.Add(Convert.ToDateTime(row["RPSTDT"]));
                            }
                        }
                    }

                }
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetPresentStops][customerNo=" + visiteeId + "][startDate=" + startDate + "][endDate=" + endDate + "]");
                return presentStops;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetPresentStops][customerObj=" + visiteeId + "][startDate=" + startDate + "][endDate=" + endDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return presentStops;
            }
        }
        public Dictionary<string, float> GetDataStatistics(DateTime date)
        {
            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetDataStatistics][date=" + date + "]");
            try
            {
                Dictionary<string, float> statistics = new Dictionary<string, float>();
                int result = 0;
                float invoiceTotal = 0.0f;
                float TotalAlliedAmt = 0.0f;
                result = DateTime.Compare(date.Date, DateTime.Now.Date);
                if (result < 0 || result == 0)
                {
                    string query = "SELECT sum(isnull(TotalAlliedAmt,0)) as Total_Allied, sum(isnull(InvoiceTotalAmt,0)) as Total_Sale " +
                                    " FROM BUSDTA.ORDER_HEADER where OrderDate='" + date.ToString("yyyy-MM-dd") + "'  " +
                                    " and RouteId = " + CommonNavInfo.RouteID + "  " +
                                    " AND OrderStateId =(SELECT ISNULL(TTID,0) AS 'TTID' FROM BUSDTA.M5001 WHERE TTKEY='ORDERDELIVERED')";
                    DataSet dbset = DbEngine.ExecuteDataSet(query);
                    if (dbset.HasData())
                    {
                        foreach (DataRow totalRow in dbset.Tables[0].Rows)
                        {
                            float.TryParse(totalRow["Total_Sale"].ToString(), out invoiceTotal);
                            float.TryParse(totalRow["Total_Allied"].ToString(), out TotalAlliedAmt);
                            statistics.Add("TotalSale", invoiceTotal);
                            statistics.Add("TotalAlliedAmt", TotalAlliedAmt);
                        }
                    }
                }
                if (result > 0)
                {
                    statistics.Add("TotalSale", invoiceTotal);
                    statistics.Add("TotalAlliedAmt", TotalAlliedAmt);
                }
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetDataStatistics][date=" + date + "]");
                return statistics;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetDataStatistics][date=" + date + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                throw;
            }
        }
        public Dictionary<string, string> GetVisiteeStopInfo(Visitee visitee, DateTime selectedDate, bool includeUnplanned, bool includeToday)
        {

            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetCustomerStopInfo][selecteDate=" + selectedDate + "]");
            try
            {
                string unplannedFlag = includeUnplanned == true ? "1" : "0";
                string todayCondition = includeToday == true ? ">=" : ">";
                Dictionary<string, string> stopInfo = new Dictionary<string, string>();
                string lastStop = string.Empty;
                string nextStop = string.Empty;
                string query = null;

                if (includeUnplanned)
                {
                    query = "select (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = " + visitee.VisiteeId + " AND RPSTTP <> 'Moved' AND RPSTDT " + todayCondition + " '" + selectedDate.ToString("yyyy-MM-dd") + "' order by 1) as 'NextStop', ";
                    query += "(SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 =  " + visitee.VisiteeId + "  AND RPSTTP <> 'Moved' AND RPSTDT < '" + selectedDate.ToString("yyyy-MM-dd") + "' order by 1 desc)  as 'LastStop'";
                }
                else
                {
                    query = "select (SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 = " + visitee.VisiteeId + " AND RPSTTP <> 'Moved' AND RPSTTP <> 'Unplanned' AND RPSTDT " + todayCondition + " '" + selectedDate.ToString("yyyy-MM-dd") + "' order by 1) as 'NextStop', ";
                    query += "(SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPAN8 =  " + visitee.VisiteeId + "  AND RPSTTP <> 'Moved' AND RPSTTP <> 'Unplanned' AND RPSTDT < '" + selectedDate.ToString("yyyy-MM-dd") + "' order by 1 desc)  as 'LastStop'";
                }
                DataSet dbset = DbEngine.ExecuteDataSet(query);
                //Fall back mechanism for above logic. If future stops are notgenerated, get the stop info from below functions.
                if (!dbset.HasData())
                {
                    query = "SELECT BUSDTA.getNextStopDate(" + visitee.VisiteeId + ",'" + selectedDate.ToString("yyyy-MM-dd") + "','" + _baseDate + "'," + unplannedFlag + ") as 'NextStop', BUSDTA.getLastStopDate(" + visitee.VisiteeId + ",'" + selectedDate.ToString("yyyy-MM-dd") + "','" + _baseDate + "'," + unplannedFlag + ") as 'LastStop'";
                    dbset = DbEngine.ExecuteDataSet(query);
                }
                if (dbset.HasData())
                {
                    foreach (DataRow totalRow in dbset.Tables[0].Rows)
                    {
                        if (!(totalRow["NextStop"].ToString() == null || totalRow["NextStop"].ToString() == ""))
                        {
                            DateTime next = Convert.ToDateTime(totalRow["NextStop"]);
                            nextStop = next.ToString("MM'/'dd'/'yyyy ");
                            stopInfo.Add("NextStop", nextStop);
                        }
                        else
                        {
                            nextStop = "None";
                            stopInfo.Add("NextStop", nextStop);
                        }
                        if (!(totalRow["LastStop"].ToString() == null || totalRow["LastStop"].ToString() == ""))
                        {
                            DateTime next = Convert.ToDateTime(totalRow["LastStop"]);
                            lastStop = next.ToString("MM'/'dd'/'yyyy ");
                            stopInfo.Add("LastStop", lastStop);
                        }
                        else
                        {
                            lastStop = "None";
                            stopInfo.Add("LastStop", lastStop);
                        }
                    }
                }

                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetCustomerStopInfo][selecteDate=" + selectedDate + "]");
                return stopInfo;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetCustomerStopInfo][selecteDate=" + selectedDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                throw;
            }
        }
        public List<DateTime> GetFutureStops(Customer cust, DateTime selectedDate)
        {
            DateTime baseDate = Convert.ToDateTime(_baseDate);

            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetFutureStops][selecteDate=" + selectedDate + "]");
            try
            {
                List<DateTime> stopInfo = new List<DateTime>();
                int index = 0;
                string query = "CALL BUSDTA.getPreOrderDatesForCustomer(CustomerNum =" + cust.CustomerNo + ", SelectedDate ='" + selectedDate.ToString("yyyy-MM-dd") + "', BaseDate='" + baseDate.ToString("yyyy-MM-dd") + "')";
                DataSet futureStopDates = DbEngine.ExecuteDataSet(query);
                DateTime tempDate = new DateTime();
                if (futureStopDates.HasData())
                {
                    foreach (DataRow stopRow in futureStopDates.Tables[0].Rows)
                    {
                        tempDate = Convert.ToDateTime(stopRow["val"]);
                        stopInfo.Add(tempDate);
                        index++;
                        if (index > 4)
                        {
                            break;
                        }
                    }
                }

                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetFutureStops][selecteDate=" + selectedDate + "]");
                return stopInfo;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetFutureStops][selecteDate=" + selectedDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                throw;
            }
        }

        public List<DateTime> GetcustomerStops(string CustomerNo, DateTime selectedDate)
        {
            DateTime baseDate = Convert.ToDateTime(_baseDate);

            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetFutureStops][selecteDate=" + selectedDate + "]");
            try
            {
                List<DateTime> stopInfo = new List<DateTime>();
                int index = 0;
                string query = "CALL BUSDTA.getPreOrderDatesForCustomer(CustomerNum =" + CustomerNo + ", SelectedDate ='" + selectedDate.ToString("yyyy-MM-dd") + "', BaseDate='" + baseDate.ToString("yyyy-MM-dd") + "')";
                DataSet futureStopDates = DbEngine.ExecuteDataSet(query);
                DateTime tempDate = new DateTime();
                if (futureStopDates.HasData())
                {
                    foreach (DataRow stopRow in futureStopDates.Tables[0].Rows)
                    {
                        tempDate = Convert.ToDateTime(stopRow["val"]);
                        if (tempDate.Date == selectedDate.Date)
                        {
                            stopInfo.Add(tempDate);
                        }
                        index++;
                        if (index > 4)
                        {
                            break;
                        }
                    }
                }

                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetFutureStops][selecteDate=" + selectedDate + "]");
                return stopInfo;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetFutureStops][selecteDate=" + selectedDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");

                throw;
            }
        }

        
        public bool UpadateSequenceNo(string stopId, string sequenceNO, string userName, string stopDate, string refStopID)
        {
            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:UpadateSequenceNo]");
            bool flag = false;
            try
            {
                string query = @" UPDATE BUSDTA.M56M0004 R SET R.RPSN='" + sequenceNO + @"' ,R.RPUPBY='" + userName + @"' , 
                                 R.RPISRSN=1 WHERE R.RPSTID = " + stopId + " AND R.RPSTDT='" + stopDate + "' ";

                int i = DbEngine.ExecuteNonQuery(query);
                if (i > 0)
                {
                    flag = true;
                }
                else if (i == 0)
                {
                    query = @" UPDATE BUSDTA.M56M0004 R SET R.RPSN='" + sequenceNO + @"' ,R.RPUPBY='" + userName + @"' , 
                                 R.RPISRSN=1 WHERE R.RPRSTID = '" + refStopID + "' AND R.RPSTDT='" + stopDate + "' ";
                    i = DbEngine.ExecuteNonQuery(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][UpadateSequenceNo][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:UpadateSequenceNo]");
            return flag;

        }
        public bool CreateUnplannedStop(Visitee visitee, DateTime dateToCreateStop)
        {
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:CreateUnplannedStop][customerObj=" + visitee + "][dateToCreateStop=" + dateToCreateStop.ToString() + "]");

                string routeDate = dateToCreateStop.ToString("yyyyMMdd");
                string dailyStopsExists = "";
                string checkDailyStops = " SELECT COUNT(1) FROM BUSDTA.M56M0004 WHERE RPSTDT = CAST('" + routeDate + "' AS DATE)";
                dailyStopsExists = DbEngine.ExecuteScalar(checkDailyStops);
                int resultRowsCount = 0;
                DateTime baseDate = Convert.ToDateTime(_baseDate);

                if (!(Convert.ToInt32(dailyStopsExists) > 0))
                {
                    string poulateCustForRoute = "";
                    poulateCustForRoute = poulateCustForRoute + "insert into busdta.M56M0004(RPROUT, RPAN8, RPSTDT, RPSN, RPVTTP, RPSTTP,RPCRBY,RPCRDT,RPUPBY,RPUPDT)(select r.RSROUT, r.RSAN8, cast('" + routeDate + "'";
                    poulateCustForRoute = poulateCustForRoute + " as date), r.RSSN, 'Cust', 'Planned','" + CommonNavInfo.UserName.Trim() + "',GETDATE(),'" + CommonNavInfo.UserName.Trim() + "',GETDATE() ";
                    poulateCustForRoute = poulateCustForRoute + " from BUSDTA.F03012 c join BUSDTA.M56M0003 r on c.aian8 = r.RSAN8 join BUSDTA.M56M0002 d on d.DCDDC = c.AISTOP ";
                    poulateCustForRoute = poulateCustForRoute + " where   R.RSWN = BUSDTA.GetWeekForDate(CAST('" + routeDate + "' as date), CAST('" + baseDate.ToString("yyyyMMdd") + "' as date))";
                    poulateCustForRoute = poulateCustForRoute + " and d.DCDN = BUSDTA.GetDayForDate(CAST('" + routeDate + "' as date), CAST('" + baseDate.ToString("yyyyMMdd") + "'as date)))";

                    resultRowsCount = DbEngine.ExecuteNonQuery(poulateCustForRoute);

                    string selctQuery = "SELECT RPSTID FROM BUSDTA.M56M0004 WHERE RPSTDT='" + dateToCreateStop.ToString("yyyy-MM-dd") + "' ORDER BY RPSN";
                    DataSet ds = DbEngine.ExecuteDataSet(selctQuery);
                    if (ds.HasData())
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            string stopID = ds.Tables[0].Rows[i]["RPSTID"].ToString();
                            string updateSeqNo = "update BUSDTA.M56M0004 set RPSN= " + (i + 2) + " where RPSTID='" + stopID + "'";
                            DbEngine.ExecuteNonQuery(updateSeqNo);
                        }
                    }


                    string insertNewStop = "";

                    insertNewStop = insertNewStop + "INSERT INTO BUSDTA.M56M0004 (RPROUT,RPAN8,RPSTDT,RPSN,RPVTTP,RPSTTP,RPCRBY,RPCRDT,RPUPBY,RPUPDT)";
                    insertNewStop = insertNewStop + " VALUES('" + CommonNavInfo.RouteUser + "'," + visitee.VisiteeId + ",cast('" + routeDate + "' as date),1,'" + visitee.VisiteeType.Trim() + "','Unplanned','" + CommonNavInfo.UserName + "',getdate(),'" + CommonNavInfo.UserName + "',getdate())";

                    DbEngine.ExecuteNonQuery(insertNewStop);
                }
                else
                {

                    string selctQuery = "SELECT RPSTID FROM BUSDTA.M56M0004 WHERE RPSTDT='" + dateToCreateStop.ToString("yyyy-MM-dd") + "' ORDER BY RPSN";
                    DataSet ds = DbEngine.ExecuteDataSet(selctQuery);
                    if (ds.HasData())
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            string stopID = ds.Tables[0].Rows[i]["RPSTID"].ToString();
                            string updateSeqNo = "update BUSDTA.M56M0004 set RPSN= " + (i + 2) + " where RPSTID='" + stopID + "'";
                            DbEngine.ExecuteNonQuery(updateSeqNo);
                        }
                    }

                    string insertNewStop = "";

                    insertNewStop = insertNewStop + "INSERT INTO BUSDTA.M56M0004 (RPROUT,RPAN8,RPSTDT,RPSN,RPVTTP,RPSTTP,RPCRBY,RPCRDT,RPUPBY,RPUPDT)";
                    insertNewStop = insertNewStop + " VALUES('" + CommonNavInfo.RouteUser + "'," + visitee.VisiteeId + ",cast('" + routeDate + "' as date),1,'" + visitee.VisiteeType.Trim() + "','Unplanned','" + CommonNavInfo.UserName + "',getdate(),'" + CommonNavInfo.UserName + "',getdate())";

                    DbEngine.ExecuteNonQuery(insertNewStop);
                }
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:CreateUnplannedStop][customerObj=" + visitee + "][dateToCreateStop=" + dateToCreateStop.ToString() + "]");

                return true;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][CreateUnplannedStop][customerObj=" + visitee + "][dateToCreateStop=" + dateToCreateStop + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                return false;
            }

        }

        public bool IsLastStopServiced(string customerNo, DateTime? lastDate)
        {
            string StopId = string.Empty;

            try
            {
                int CompletedActivity = 0;
                int PendingActivity = 0;
                string noSale = string.Empty;
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:IsLastStopServiced]customerNo=" + customerNo + "][StopId" + StopId + "]");
                if (lastDate == null)
                {
                    return false;
                }
                string checkLastStop = "select rpstid from busdta.M56M0004 where rpstdt = '" + lastDate.Value.ToString("yyyy-MM-dd") + "' and rpan8 =" + customerNo + " and rpsttp <> 'Moved'";
                StopId = DbEngine.ExecuteScalar(checkLastStop);

                string query = "select RPCACT,RPPACT,RPACTID from busdta.M56M0004 where rpstid = " + StopId + " and rpan8 = " + customerNo + "";
                DataSet dbSet = DbEngine.ExecuteDataSet(query);
                if (dbSet.HasData())
                {
                    foreach (DataRow row in dbSet.Tables[0].Rows)
                    {
                        PendingActivity = Convert.ToInt32(row["RPPACT"].ToString());
                        CompletedActivity = Convert.ToInt32(row["RPCACT"].ToString());
                        noSale = string.IsNullOrEmpty(row["RPACTID"].ToString()) ? "" : row["RPACTID"].ToString();
                    }

                    if (PendingActivity > 0 || CompletedActivity > 0 || noSale.ToLower() == "nosale")
                    {
                        return true;
                    }
                }
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:IsLastStopServiced]customerNo=" + customerNo + "][StopId" + StopId + "]");
                return false;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][IsLastStopServiced][=" + customerNo + "][StopId" + StopId + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }

        public Dictionary<string, int> GetActivityCount(string customerNo, string StopId, DateTime lDate)
        {
            Dictionary<string, int> activityCount = new Dictionary<string, int>();
            try
            {
                bool Activity = false;
                int CompletedActivity = 0;
                int PendingActivity = 0;
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetActivityCount]customerNo=" + customerNo + "][lastDate" + lDate + "]");

                string query = "select RPCACT,RPPACT,RPACTID from busdta.M56M0004 where rpstid = " + StopId + " and rpan8 = " + customerNo + "";
                DataSet dbSet = DbEngine.ExecuteDataSet(query);
                if (dbSet.HasData())
                {
                    foreach (DataRow row in dbSet.Tables[0].Rows)
                    {
                        PendingActivity = Convert.ToInt32(row["RPPACT"].ToString());
                        CompletedActivity = Convert.ToInt32(row["RPCACT"].ToString());
                    }

                    if (PendingActivity > 0 || CompletedActivity > 0)
                    {
                        Activity = true;
                    }
                }
                activityCount.Add("PendingActivity", PendingActivity);
                activityCount.Add("CompletedActivity", CompletedActivity);
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetActivityCount]customerNo=" + customerNo + "][lastDate" + lDate + "]");
                return activityCount;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetActivityCount][=" + customerNo + "][lastDate" + lDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }

        public string GetCurrentWeekNo(DateTime calendarDate)
        {
            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:IsLastStopServiced]CalendarDate=" + calendarDate + "]");
            string baseDate = System.Configuration.ConfigurationManager.AppSettings["BaseDate"].ToString().Replace("-", "");
            string weekNo = "1";
            string query = "SELECT BUSDTA.GetWeekForDate('" + calendarDate.Date.ToString("yyyy-MM-dd") + "','" + baseDate + "')";
            try
            {
                weekNo = DbEngine.ExecuteScalar(query);
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetCurrentWeekNo]customerNo=" + calendarDate + "]");
                return weekNo;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetCurrentWeekNo][calendarDate=" + calendarDate + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }

        public string GetUpdatedStopId(string customerNo, string originalDate, string stopId)
        {
            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:GetUpdatedStopId]stopId=" + stopId + "]");
            string stopIdUpdated = string.Empty;
            try
            {
                stopIdUpdated = DbEngine.ExecuteScalar("SELECT rpstid FROM BUSDTA.M56M0004 WHERE rpan8=" + customerNo + " and rpogdt='" + originalDate + "' and rprstid='" + stopId + "'");
                log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:GetUpdatedStopId]stopId=" + stopId + "]");
                return stopIdUpdated;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][GetUpdatedStopId][stopId=" + stopId + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }

        public Customer GetCustomerForStop(string customerNo, bool isCustomer, DateTime stopDate)
        {
            Domain.Customer cust = new Domain.Customer();

            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][Start:IsStopForToday]");
            try
            {
                ObservableCollection<Domain.Customer> tempCollection = new ObservableCollection<Customer>();
                ServiceRouteManager serviceRouteManager = new ServiceRouteManager();
                tempCollection = serviceRouteManager.GetCustomersForRoute(CommonNavInfo.RouteUser, stopDate.Date);

                if (isCustomer)
                    cust = tempCollection.FirstOrDefault(c => (c.CustomerNo == customerNo) && c.StopType.ToLower() != "moved");
                else
                    cust = tempCollection.FirstOrDefault(c => (c.CustomerNo == customerNo));


            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][ServiceRouteManager][IsStopForToday][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            log.Info("[SalesLogicExpress.Application.Managers][ServiceRouteManager][End:IsStopForToday]");
            return cust;

        }

        public void Dispose()
        {
        }
    }
}
