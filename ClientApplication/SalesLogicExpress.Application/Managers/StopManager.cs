﻿using System;
using System.Linq;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using System.Data;
using SalesLogicExpress.Application.Helpers;
using System.Collections.Generic;
using log4net;
using SalesLogicExpress.Application.ViewModels;
using System.Configuration;
using System.Globalization;
using GalaSoft.MvvmLight.Messaging;
using System.Windows.Threading;
using SalesLogicExpress.Domain.Prospect_Models;

namespace SalesLogicExpress.Application.Managers
{
    public class StopManager
    {
        readonly ServiceRouteManager serviceRouteManager = new ServiceRouteManager();
        readonly string baseDate = ConfigurationManager.AppSettings["BaseDate"].ToString();
        readonly int yearsToAdd = Convert.ToInt32(ConfigurationManager.AppSettings["YearsToAdd"].ToString());
        string stopPresent = string.Empty;
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.StopManager");
        DateTime selectionEndDate;
        public void MoveStopDialogContext(ref CreateAndMoveStopViewModel moveStopViewModelProperty, Visitee pVisitee, DateTime pDailyStopDate, Guid messageToken)
        {
            Visitee visiteeToMove = new Visitee();
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:MoveStopDialogContext][MoveStopViewModelProperty=" + moveStopViewModelProperty + "][CustomerToMove=" + visiteeToMove + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");
                visiteeToMove = pVisitee.DeepCopy(); 
                DateTime baseDateApp = Convert.ToDateTime(baseDate);
                DateTime lastStop = new DateTime();
                Dictionary<string, string> dict = new Dictionary<string, string>();
                selectionEndDate = GetLastDate();
                // Dictionary contains next stop date and previous stop date  *2nd parameter to be changed based on decision
                DateTime dateToCalcMove = pDailyStopDate.Date < DateTime.Today.Date ? DateTime.Today.Date : pDailyStopDate;
                if (visiteeToMove.IsTodaysStop && pDailyStopDate.Date < DateTime.Today.Date)
                {
                    dateToCalcMove = pDailyStopDate;
                }
                dict = serviceRouteManager.GetVisiteeStopInfo(visiteeToMove, dateToCalcMove, false, false);

                visiteeToMove.NextStop = dict["NextStop"].ToString();
                visiteeToMove.PreviousStop = dict["LastStop"].ToString();
                if (!(visiteeToMove.PreviousStop.ToLower() == "none"))
                {
                    if (visiteeToMove.VisiteeType.ToLower() == "prospect")
                    {
                        lastStop = DateTime.Today;
                    }
                    else
                    {
                        lastStop = DateTime.Parse(visiteeToMove.PreviousStop, CultureInfo.CreateSpecificCulture("en-US"));
                    }
                }
                else
                {
                    lastStop = baseDateApp;
                }
                moveStopViewModelProperty.Visitee = visiteeToMove;
                if (!(visiteeToMove.PreviousStop.ToLower() == "none"))
                {
                    if (DateTime.Compare(pDailyStopDate.Date, DateTime.Today.Date) == 0)
                    {
                        moveStopViewModelProperty.SelectableDateStart = DateTime.Today.Date.AddDays(1);
                    }
                    else
                    {
                        if (!(DateTime.Parse(visiteeToMove.PreviousStop, CultureInfo.CreateSpecificCulture("en-US")) < DateTime.Today))
                        {
                            moveStopViewModelProperty.SelectableDateStart = DateTime.Parse(visiteeToMove.PreviousStop, CultureInfo.CreateSpecificCulture("en-US"));

                        }
                        else
                            moveStopViewModelProperty.SelectableDateStart = DateTime.Today;
                    }
                }
                if (visiteeToMove.StopType.ToLower() == "unplanned")
                {
                    moveStopViewModelProperty.SelectableDateStart = DateTime.Today;
                }
                if (visiteeToMove.OriginalDate == null)
                {
                    visiteeToMove.OriginalDate = pDailyStopDate;
                }
                else
                {
                    //try
                    //{
                    //    CustomerToMove.OriginalDate = DateTime.Parse(CustomerToMove.OriginalDate, CultureInfo.CreateSpecificCulture("en-US")).ToString("MM'/'dd'/'yyyy ");
                    //}
                    //catch
                    //{
                    //    CustomerToMove.OriginalDate = Convert.ToDateTime(CustomerToMove.OriginalDate).ToString("MM'/'dd'/'yyyy ");
                    //}
                }
                if (!(visiteeToMove.VisiteeType.ToLower() == "prospect"))
                {
                    if (!(visiteeToMove.NextStop == "None"))
                    {

                        moveStopViewModelProperty.SelectableDateEnd = DateTime.Parse(visiteeToMove.NextStop, CultureInfo.CreateSpecificCulture("en-US")).AddDays(-1);

                    }
                    else
                    {
                        moveStopViewModelProperty.SelectableDateEnd = DateTime.Today.AddDays(7);
                    }
                    if (visiteeToMove.StopType.ToLower() == "unplanned")
                    {
                        moveStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                    }
                }
                else
                {
                    moveStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                }

                moveStopViewModelProperty.DailyStopDate = pDailyStopDate;
                bool isNotUnplanned = visiteeToMove.StopType.ToLower() != "unplanned";
                moveStopViewModelProperty.BlackOutDates = serviceRouteManager.GetPresentStops(visiteeToMove.VisiteeId, moveStopViewModelProperty.SelectableDateStart, moveStopViewModelProperty.SelectableDateEnd, isNotUnplanned);

                if (visiteeToMove.IsTodaysStop && pDailyStopDate.Date < DateTime.Today.Date)
                {
                    moveStopViewModelProperty.BlackOutDates.Add(DateTime.Today.Date);

                }
                string stopStatusQuery = "select RPSTTP from busdta.m56m0004 where RPAN8=" + visiteeToMove.VisiteeId + " and RPSTDT='" + lastStop.ToString("yyyy-MM-dd") + "'";
                string stopStatus = DbEngine.ExecuteScalar(stopStatusQuery);
                if (!(stopStatus.ToLower() == "moved"))
                    moveStopViewModelProperty.BlackOutDates.Add(lastStop);


                CreateAndMoveStopViewModel objMoveStop = new CreateAndMoveStopViewModel();
                objMoveStop = moveStopViewModelProperty;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    var dialog = new Helpers.DialogWindow { TemplateKey = "MoveStopDialog", Title = "Move Stop", Payload = objMoveStop };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, messageToken);

                    objMoveStop.IsCreateButton = dialog.Closed ? true : false;
                }));
                moveStopViewModelProperty = objMoveStop;
                objMoveStop.Dispose();
                log.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:MoveStopDialogContext][MoveStopViewModelProperty=" + moveStopViewModelProperty + "][CustomerToMove=" + visiteeToMove + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][StopManager][MoveStopDialogContext][MoveStopViewModelProperty=" + moveStopViewModelProperty + "][CustomerToMove=" + visiteeToMove + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }



        public DateTime GetLastDate()
        {
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:GetLastDate]");
                var lastGeneratedDate = DateTime.Now.AddYears(1);
                string stopCount = DbEngine.ExecuteScalar("select count(1) from busdta.m56m0004");
                if (!string.IsNullOrEmpty(stopCount))
                {
                    if (Convert.ToInt32(stopCount) > 0)
                    {
                        string query = "SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPSTDT > '" + DateTime.Today.Date.ToString("yyyy-MM-dd") + "' order by RPSTDT desc";
                        string strSelectedDateEnd = DbEngine.ExecuteScalar(query);
                        lastGeneratedDate = Convert.ToDateTime(strSelectedDateEnd);
                    }
                }
                log.Info("[SalesLogicExpress.Application.Managers][StopManager][End:GetLastDate]");
                return lastGeneratedDate;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][StopManager][GetLastDate][ExceptionStackTrace = " + ex.StackTrace + "]");
                return DateTime.Now.AddYears(1);
            }
        }
        public void CreateStopDialogContext(ref CreateAndMoveStopViewModel createStopViewModelProperty, Visitee pVisitee, DateTime pDailyStopDate, Guid messageToken)
        {
            Visitee visiteeToCreate = pVisitee;
            try
            {
                log.Info("[SalesLogicExpress.Application.Managers][StopManager][Start:CreateStopDialogContext][CreateStopViewModelProperty=" + createStopViewModelProperty + "][CustomerToCreate=" + visiteeToCreate + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");

                visiteeToCreate = pVisitee.DeepCopy(); 
                Dictionary<string, string> dict = new Dictionary<string, string>();
                DateTime baseDateApp = Convert.ToDateTime(baseDate);
                string query = "SELECT top 1 RPSTDT FROM BUSDTA.M56M0004 WHERE RPSTDT > '" + DateTime.Today.Date.ToString("yyyy-MM-dd") + "' order by RPSTDT desc";
                string strSelectedDateEnd = DbEngine.ExecuteScalar(query);
                selectionEndDate = Convert.ToDateTime(strSelectedDateEnd);
                if (visiteeToCreate.IsTodaysStop && visiteeToCreate.StopType.ToLower() == "moved")
                    visiteeToCreate.IsTodaysStop = false;
                // Dictionary contains next stop date and previous stop date  *2nd parameter to be changed based on decision
                dict = serviceRouteManager.GetVisiteeStopInfo(visiteeToCreate, pDailyStopDate, false, false);
                string count = "select count(1) from busdta.M56M0004 where RPAN8=" + visiteeToCreate.VisiteeId + " and RPSTDT='" + pDailyStopDate.Date.ToString("yyyy-MM-dd") + "' and RPSTTP <> 'Moved'";
                stopPresent = DbEngine.ExecuteScalar(count);
                createStopViewModelProperty.Visitee = visiteeToCreate;
                // checks customer is from daily stop tab and is in todays stop list
                if (DateTime.Compare(pDailyStopDate.Date, DateTime.Today.Date) == 0)
                {

                    createStopViewModelProperty.SelectableDateStart = DateTime.Today.Date.AddDays(1);

                    if (!visiteeToCreate.IsTodaysStop || visiteeToCreate.IsFromListingTab)
                    {
                        createStopViewModelProperty.SelectableDateStart = DateTime.Today.Date;
                    }


                }
                // If create stop is from customers or prospects tab
                else
                {
                    string countAll = "select count(1) from busdta.M56M0004 where RPAN8=" + visiteeToCreate.VisiteeId + " and RPSTDT='" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "' and rpsttp <> 'Moved'";
                    string presentCount = DbEngine.ExecuteScalar(countAll);

                    createStopViewModelProperty.SelectableDateStart = Convert.ToInt32(presentCount) > 0 ? DateTime.Today.Date.AddDays(1) : DateTime.Today; ;

                }
                // if selected item is prospect date selection will be from today
                if (visiteeToCreate.VisiteeType.ToLower() == "prospect")
                {
                    createStopViewModelProperty.SelectableDateStart = DateTime.Today;
                }
                if (visiteeToCreate.VisiteeType.ToLower() == "cust")
                {
                    
                    visiteeToCreate.NextStop = dict["NextStop"].ToString();

                    visiteeToCreate.PreviousStop = dict["LastStop"].ToString();

                    if (visiteeToCreate.OriginalDate == null)
                    {

                        visiteeToCreate.OriginalDate = pDailyStopDate;
                    }

                    // Sets selectable date end as last stop generated date.
                    createStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                }
                else
                {
                    visiteeToCreate.OriginalDate = null;
                    visiteeToCreate.NextStop = "";
                    visiteeToCreate.PreviousStop = "";
                    createStopViewModelProperty.SelectableDateEnd = selectionEndDate;
                }
                createStopViewModelProperty.DailyStopDate = pDailyStopDate;
                createStopViewModelProperty.BlackOutDates = serviceRouteManager.GetPresentStops(visiteeToCreate.VisiteeId, createStopViewModelProperty.SelectableDateStart, createStopViewModelProperty.SelectableDateEnd, false);
                // Removes selected date from planned stops collection .
                if (visiteeToCreate.IsFromListingTab == false)
                {
                    // before removing checks stop count for that day
                    if (!(Convert.ToInt32(stopPresent) > 0))
                    {
                        if (createStopViewModelProperty.BlackOutDates.Any(item => item == pDailyStopDate.Date) && visiteeToCreate.StopType.ToLower() == "moved")
                        {
                            // check  for moved stop is in todays date or in future date list
                            //if (DateTime.Compare(pDailyStopDate.Date, DateTime.Today.Date) == 0) { }
                            createStopViewModelProperty.BlackOutDates.RemoveAll(item => item == pDailyStopDate.Date);
                        }
                        string movedStopSet = "select rpstdt as 'date' from busdta.M56M0004 where RPAN8=" + visiteeToCreate.VisiteeId + " and RPSTDT >='" + DateTime.Now.Date.ToString("yyyy-MM-dd") + "' and rpsttp = 'Moved'";
                        DataSet presentStops = DbEngine.ExecuteDataSet(movedStopSet);
                        if (presentStops.HasData())
                        {
                            foreach (DataRow rw in presentStops.Tables[0].Rows)
                            {
                                DateTime dt = Convert.ToDateTime(rw["date"]);
                                createStopViewModelProperty.BlackOutDates.RemoveAll(item => item.Date == dt.Date);
                            }
                        }
                    }
                }
                CreateAndMoveStopViewModel objCreateStop = new CreateAndMoveStopViewModel();
                objCreateStop = createStopViewModelProperty;
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    var dialog = new Helpers.DialogWindow { TemplateKey = "CreateStopDialog", Title = "Create Stop", Payload = objCreateStop };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, messageToken);

                    objCreateStop.IsCreateButton = dialog.Closed ? true : false;
                }));
                createStopViewModelProperty = objCreateStop;
                objCreateStop.Dispose();
                log.Info("[SalesLogicExpress.Application.Managers][StopManager][End:CreateStopDialogContext][CreateStopViewModelProperty=" + createStopViewModelProperty + "][CustomerToCreate=" + visiteeToCreate + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "]");

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][StopManager][CreateStopDialogContext][CreateStopViewModelProperty=" + createStopViewModelProperty + "][CustomerToCreate=" + visiteeToCreate + "][pDailyStopDate=" + pDailyStopDate + "][MessageToken=" + messageToken + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }


    }
}
