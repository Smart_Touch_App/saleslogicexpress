﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using log4net;

namespace SalesLogicExpress.Application.Managers
{
    public class NumberManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.NumberManager");
        public string GetNextNumberForEntity(string RouteID, Entity ForEntity)
        {
            string returnValue = string.Empty;
            try
            {
                string query = string.Format("SELECT BUSDTA.GetNextNumber({0},{1})", RouteID, ForEntity.GetEnumDescription());
                returnValue = DbEngine.ExecuteScalar(query);
                returnValue = string.IsNullOrEmpty(returnValue) ? null : returnValue;
            }
            catch (Exception ex)
            {
                returnValue = null;
                Logger.Error("[SalesLogicExpress.Application.Managers][NumberManager][GetNextNumberForEntity][RouteID=" + RouteID + "][ForEntity=" + ForEntity.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            return returnValue;
        }

        // The description value is derived from Entity_Bucket_Master table in the remote database
        public enum Entity
        {
            [Description("1")]
            Receipt,
            [Description("2")]
            SalesOrder,
            [Description("3")]
            Settlement,
            [Description("4")]
            ReturnOrder,
            [Description("5")]
            CreditMemo,
            [Description("6")]
            Quote,
            [Description("7")]
            Prospect
        }
    }
}
