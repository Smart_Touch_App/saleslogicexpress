﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class SettlementConfirmationManager
    {

        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.SettlementConfirmationManager");

        public static void AddConfirmationSettlement(string statusCD, double settlementAmount,
               ObservableCollection<CashMaster> cashDetailsCollection, ObservableCollection<CheckDetails> CheckDetailsCollection,
            ObservableCollection<MoneyOrderDetails> MoneyOrderDetailsCollection, double cashV, double checkV, double MoneyOrderV, double totalV,
            double expenses, double overShort, Byte[] sign)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:AddConfirmationSettlement]");

            try
            {
                #region Implementation
                string query = string.Empty;
                string settlementID = GetSettlementID().ToString();
                int routeId = CommonNavInfo.RouteID;
                int userID = Managers.UserManager.UserId;
                int originator = Managers.UserManager.UserId;
                int createdBy = Managers.UserManager.UserId;
                //Get settlement No from DB
                string settlementNo = "SO" + settlementID.ToString();
                string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();
                //insert into Route Settlement
                query = "insert into busdta.Route_Settlement " +
                                " (SettlementID,SettlementNo, RouteId,[Status],UserId,Originator,SettlementAmount,ExceptionAmount,[comment],CreatedBy ,OriginatingRoute,partitioningRoute,CreatedDatetime,SettlementDateTime) " +
                                " values(" + settlementID + ",'" + settlementNo + "'," + routeId + "," + statusID + "," + userID + "," + originator +
                                 " ," + settlementAmount + "," + overShort + ",''," + createdBy + ",'" + CommonNavInfo.OriginatingRouteID + "','" + CommonNavInfo.PartitioningRouteID + "',getdate(),getdate()) ";
                int i = DbEngine.ExecuteNonQuery(query);
                if (i > 0)
                {
                    string insertVVDetails = "insert into busdta.Route_Settlement_Detail(SettlementId,CashAmount,CheckAmount,Expenses,MoneyOrderAmount, " +
                                            " OverShortAmount,TotalVerified,Payments,VerificationNum,UserId,VerSignature,CreatedBy,CreatedDatetime,OriginatingRoute,partitioningRoute,RouteId) " +
                                            " values " +
                                            " (" + settlementID + "," + cashV + "," + checkV + "," + expenses + "," + MoneyOrderV + "," + overShort + "," + totalV + "," +
                                            " " + settlementAmount + ",'1','" + createdBy + "',?,'" + createdBy + "',GETDATE(),'" + CommonNavInfo.OriginatingRouteID + "','" + CommonNavInfo.PartitioningRouteID + "','" + routeId + "')";


                    // used SACommand for inserting blob, inserting blobs needs to done using parameters
                    iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(insertVVDetails);
                    iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                    parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongBinary;
                    command.Parameters.Add(parm);
                    command.Parameters[0].Value = sign;
                    i = new DB_CRUD().InsertUpdateData(command);
                    if (i > 0)
                    {
                        query = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId='" + settlementID + "'";
                        string settlementDetailID = DbEngine.ExecuteScalar(query).ToString();


                        if (cashDetailsCollection.Count > 0)
                        {
                            foreach (var item in cashDetailsCollection)
                            {
                                string insertCashDetails = "insert into busdta.Cash_Verification_Detail " +
                                    " ( SettlementDetailId ,CashTypeId,Quantity ,Amount,CreatedBy ,CreatedDatetime,RouteId )" +
                                    " values " +
                                    " ('" + settlementDetailID + "', '" + item.CashId + "','" + item.Quantity + "','" + item.Amount + "','" + createdBy + "',getdate(),'" + routeId + "')";
                                DbEngine.ExecuteNonQuery(insertCashDetails);
                            }

                        }

                        if (CheckDetailsCollection.Count > 0)
                        {
                            foreach (var objCheck in CheckDetailsCollection)
                            {
                                string insertCheckDetails = "insert into busdta.Check_Verification_Detail(SettlementDetailId,   CheckDetailsId,RouteId,CreatedBy,CreatedDatetime)  " +
                                                     " VALUES  (" + settlementDetailID + ",     '" + objCheck.CheckID + "', '" + routeId + "','" + createdBy + "',getdate())";

                                i = DbEngine.ExecuteNonQuery(insertCheckDetails);
                            }
                        }

                        if (MoneyOrderDetailsCollection.Count > 0)
                        {
                            foreach (var objMO in MoneyOrderDetailsCollection)
                            {
                                string insertMoDetails = "insert into busdta.MoneyOrder_Verification_Detail (SettlementDetailId,MoneyOrderId,RouteId,CreatedBy,CreatedDatetime) " +
                                                " VALUES (" + settlementDetailID + ",'" + objMO.MoneyOrderID + "', '" + routeId + "','" + createdBy + "',getdate())";
                                i = DbEngine.ExecuteNonQuery(insertMoDetails);
                            }
                        }
                        UpdateSettlementIdForTransaction(settlementID);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][AddConfirmationSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:AddConfirmationSettlement]");

        }

        public static void AddVerificationSettlement(string statusCD, double settlementAmount,
             ObservableCollection<CashMaster> cashDetailsCollection, ObservableCollection<CheckDetails> CheckDetailsCollection,
          ObservableCollection<MoneyOrderDetails> MoneyOrderDetailsCollection, double cashV, double checkV, double MoneyOrderV, double totalV,
            double expenses, double overShort, string settlementID, Byte[] sign)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:AddVerificationSettlement]");
            try
            {
                #region Implementation
                string query = "";
                int routeId = CommonNavInfo.RouteID;
                int userID = Managers.UserManager.UserId;
                int originator = Managers.UserManager.UserId;
                int verifier = 0;
                int createdBy = Managers.UserManager.UserId;
                string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();

                //Get settlement No from DB
                //insert into Route Settlement
                string insertVVDetails = "insert into busdta.Route_Settlement_Detail(SettlementId,CashAmount,CheckAmount,Expenses,MoneyOrderAmount, " +
                                             " OverShortAmount,TotalVerified,Payments,VerificationNum,UserId,VerSignature,CreatedBy,CreatedDatetime,OriginatingRoute,partitioningRoute,RouteId) " +
                                             " values " +
                                             " (" + settlementID + "," + cashV + "," + checkV + "," + expenses + "," + MoneyOrderV + "," + overShort + "," + totalV + "," +
                                             " " + settlementAmount + ",'2','" + createdBy + "',?,'" + createdBy + "',GETDATE(),'" + CommonNavInfo.OriginatingRouteID + "','" + CommonNavInfo.PartitioningRouteID + "','" + routeId + "')";


                // used SACommand for inserting blob, inserting blobs needs to done using parameters
                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(insertVVDetails);
                iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongBinary;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = sign;
                new DB_CRUD().InsertUpdateData(command);

                query = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId='" + settlementID + "' and VerificationNum ='2'";
                string settlementDetailID = DbEngine.ExecuteScalar(query).ToString();

                if (cashDetailsCollection.Count > 0)
                {
                    foreach (var item in cashDetailsCollection)
                    {
                        string insertCashDetails = "insert into busdta.Cash_Verification_Detail " +
                                " ( SettlementDetailId ,CashTypeId,Quantity ,Amount,CreatedBy ,CreatedDatetime,RouteId )" +
                                " values " +
                                " ('" + settlementDetailID + "', '" + item.CashId + "','" + item.Quantity + "','" + item.Amount + "','" + createdBy + "',getdate(),'" + routeId + "')";
                        DbEngine.ExecuteNonQuery(insertCashDetails);
                    }

                }

                if (CheckDetailsCollection.Count > 0)
                {
                    foreach (var objCheck in CheckDetailsCollection)
                    {
                        string insertCheckDetails = "insert into busdta.Check_Verification_Detail(SettlementDetailId,   CheckDetailsId,RouteId,CreatedBy,CreatedDatetime)  " +
                                             " VALUES  (" + settlementDetailID + ",     '" + objCheck.CheckID + "', '" + routeId + "','" + createdBy + "',getdate())";

                        DbEngine.ExecuteNonQuery(insertCheckDetails);
                    }
                }

                if (MoneyOrderDetailsCollection.Count > 0)
                {
                    foreach (var objMO in MoneyOrderDetailsCollection)
                    {
                        string insertMoDetails = "insert into busdta.MoneyOrder_Verification_Detail (SettlementDetailId,MoneyOrderId,RouteId,CreatedBy,CreatedDatetime) " +
                                        " VALUES (" + settlementDetailID + ",'" + objMO.MoneyOrderID + "', '" + routeId + "','" + createdBy + "',getdate())";
                        DbEngine.ExecuteNonQuery(insertMoDetails);
                    }
                }
                if (statusCD.ToUpper() == "REJCT")
                {
                    VoidSettlement(settlementID, statusCD, "");
                }

                //UpdateSettlementIdForTransaction(settlementID);
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][AddVerificationSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:AddVerificationSettlement]");
        }


        public static void GetConfirmedSettlements(string settlementID, out List<CashMaster> cashMstr, out  List<CheckDetails> verifiedCheck,
            out List<MoneyOrderDetails> verifiedMo, out List<VerifiedFunds> verifiedFunds)
        {
            DataSet ds = new DataSet();
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetConfirmedSettlements]");


            try
            {
                #region Implementation
                string querySettDetlId = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId ='" + settlementID + "'";
                string settlementDetialID = DbEngine.ExecuteScalar(querySettDetlId);

                #region Cash

                string query = "select CashTypeId AS 'CashId', Quantity,Amount,SettlementDetailId as 'SettlementDetailID' " +
                    " from busdta.Cash_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";

                ds = DbEngine.ExecuteDataSet(query);
                cashMstr = new List<CashMaster>();
                if (ds.HasData())
                {
                    cashMstr = ds.GetEntityList<CashMaster>();
                }

                #endregion

                #region Check
                ds = new DataSet();

                string queryVerifiedCheck = "select  ch.CheckDetailsId as 'CheckID', " +
                    " ch.SettlementDetailId  as 'SettlementID' from busdta.Check_Verification_Detail ch " +
                    " where SettlementDetailId='" + settlementDetialID + "'";

                ds = DbEngine.ExecuteDataSet(queryVerifiedCheck);
                verifiedCheck = new List<CheckDetails>();
                if (ds.HasData())
                {
                    verifiedCheck = ds.GetEntityList<CheckDetails>();
                }
                ds = new DataSet();
                #endregion

                #region MoneyOrder
                string queryVerifiedMO = "select a.MoneyOrderId as 'MoneyOrderID' ,a.SettlementDetailId as 'SettlementID' " +
                    " from busdta.MoneyOrder_Verification_Detail a " +
                    " where SettlementDetailId='" + settlementDetialID + "'";


                ds = DbEngine.ExecuteDataSet(queryVerifiedMO);
                verifiedMo = new List<MoneyOrderDetails>();
                if (ds.HasData())
                {
                    verifiedMo = ds.GetEntityList<MoneyOrderDetails>();
                }
                ds = new DataSet();
                #endregion

                #region Funds

                string queryVerifiedFunds = "select a.CashAmount as 'Cash',a.CheckAmount as 'Check',a.Expenses  as 'Expenses'" +
                                            " ,a.MoneyOrderAmount as 'MoneyOrder',a.OverShortAmount as 'OverShort', " +
                                            " a.SettlementId as 'SettlementID',a.TotalVerified as 'TotalVerified',a.Payments AS 'Payments'" +
                                            " from busdta.Route_Settlement_Detail a  " +
                                            " where SettlementId='" + settlementID + "'";


                ds = DbEngine.ExecuteDataSet(queryVerifiedFunds);
                verifiedFunds = new List<VerifiedFunds>();
                if (ds.HasData())
                {
                    verifiedFunds = ds.GetEntityList<VerifiedFunds>();
                }
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                cashMstr = new List<CashMaster>();
                verifiedCheck = new List<CheckDetails>();
                verifiedMo = new List<MoneyOrderDetails>();
                verifiedFunds = new List<VerifiedFunds>();
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetConfirmedSettlements][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetConfirmedSettlements]");

        }

        public static List<UsersForVerification> GetUserForVerification()
        {
            List<UsersForVerification> vUsers = new List<UsersForVerification>();
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetUserForVerification]");

            try
            {
                #region Implementation
                int userID = Managers.UserManager.UserId;
                string query = "SELECT App_user_id as 'UserID', App_User as 'UserName',AppPassword as 'Password' from busdta.user_master where App_user_id != '" + userID + "' ";
                DataSet ds = DbEngine.ExecuteDataSet(query);

                if (ds.HasData())
                {
                    vUsers = ds.GetEntityList<UsersForVerification>();
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetUserForVerification][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetUserForVerification]");

            return vUsers;


        }
        public static ObservableCollection<ViewModels.ReasonCode> GetReasonListForVoid(string ReasonCodeType)
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetReasonListForVoid]");

            try
            {
                #region Implementation
                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='" + ReasonCodeType + "'").Tables[0];

                ViewModels.ReasonCode ReasonCode = new ViewModels.ReasonCode();
                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ViewModels.ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetReasonListForVoid][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetReasonListForVoid]");

            return reasonCodeList;

        }

        public static ObservableCollection<RejectionReason> GetRejectionReasons()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetRejectionReasons]");

            ObservableCollection<RejectionReason> reasonCodeList = new ObservableCollection<RejectionReason>();
            #region Implementation
            try
            {
                string query = "SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Reject Settlement'";

                DataTable dt = DbEngine.ExecuteDataSet(query).Tables[0];
                RejectionReason ReasonCode = new RejectionReason();
                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new RejectionReason();
                    ReasonCode.ReasonID = (dr["Id"].ToString());
                    ReasonCode.ReasonName = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetRejectionReasons][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            #endregion
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetRejectionReasons]");

            return reasonCodeList;
        }

        public static List<CashMaster> GetCashMaster()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetCashMaster]");

            List<CashMaster> vUsers = new List<CashMaster>();

            try
            {
                string query = "select CashId,CashType,CashCode,CashDescription,'0' as 'Quantity' from busdta.Cash_Master";
                DataSet ds = DbEngine.ExecuteDataSet(query);

                if (ds.HasData())
                {
                    vUsers = ds.GetEntityList<CashMaster>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetCashMaster][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetCashMaster]");

            return vUsers;
        }

        public static void UpdateSettlementIdForTransaction(string settlementID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:UpdateSettlementIdForTransaction]");
            try
            {
                ObservableCollection<string> obj = ViewModelPayload.PayloadManager.RouteSettlementPayload.ActivityIDList;
                for (int i = 0; i < obj.Count; i++)
                {
                    string updateQuery = "update busdta.M50012  set TDSTTLID= '" + settlementID + "' where TDID='" + obj[i].ToString() + "' AND TRIM(ISNULL(TDSTTLID,''))=''";
                    DbEngine.ExecuteNonQuery(updateQuery);
                }
                ViewModelPayload.PayloadManager.RouteSettlementPayload.ActivityIDList.Clear();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][UpdateSettlementIdForTransaction][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:UpdateSettlementIdForTransaction]");


        }

        public static void VoidSettlement(string settlementID, string statusCD, string voidReason)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:VoidSettlement]");

            try
            {
                string updateQuery = "update busdta.M50012  set TDSTTLID= '' where TDSTTLID='" + settlementID + "'";
                DbEngine.ExecuteNonQuery(updateQuery);

                //string querySettDetlId = "select SettlementDetailId from busdta.Route_Settlement_Detail where SettlementId ='" + settlementID + "'";
                //string settlementDetialID = DbEngine.ExecuteScalar(querySettDetlId);

                //string queryDelete = "delete from busdta.Route_Settlement_Detail where SettlementId='" + settlementID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);
                string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();
                if (statusCD.ToLower() == "void")
                {
                    updateQuery = "Update busdta.Route_Settlement set [Status]='" + statusID + "' , [comment]='" + voidReason + "', SettlementDateTime=GETDATE()  where SettlementId='" + settlementID + "'";

                }
                else
                {
                    updateQuery = "Update busdta.Route_Settlement set [Status]='" + statusID + "' , SettlementDateTime=GETDATE()  where SettlementId='" + settlementID + "'";

                }
                DbEngine.ExecuteNonQuery(updateQuery);

                //queryDelete = "delete from busdta.MoneyOrder_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);

                //queryDelete = "delete from busdta.Cash_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);

                //queryDelete = "delete from busdta.Check_Verification_Detail where SettlementDetailId='" + settlementDetialID + "'";
                //DbEngine.ExecuteNonQuery(queryDelete);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][VoidSettlement][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:VoidSettlement]");


        }


        public static void UpdateSettlementVerification(string settlementID, string reason, string verifier, string statusCD)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:UpdateSettlementVerification]");

            try
            {
                string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='" + statusCD + "'").ToString();

                string query = "update busdta.Route_Settlement  set [Status]='" + statusID + "', [Comment]='" + reason + "', Verifier ='" + verifier + "' , SettlementDateTime=GETDATE()  where SettlementID='" + settlementID + "'";
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][UpdateSettlementVerification][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:UpdateSettlementVerification]");

        }

        public static bool IsPendingVerification()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:IsPendingVerification]");

            bool flag = false;
            try
            {
                string query = " select count ([status]) from busdta.Route_Settlement where [status]=(SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='NEW')";
                int i = Convert.ToInt16(DbEngine.ExecuteScalar(query));
                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {

                flag = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][IsPendingVerification][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:IsPendingVerification]");

            return flag;
        }

        /// <summary>
        /// Checks if the settlement is pending for verification 
        /// </summary>
        /// <returns></returns>
        public static bool IsSettlementPendingVerification(string settlementId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:IsSettlementPendingVerification]");

            bool flag = false;
            try
            {
                string query = " select count ([status]) from busdta.Route_Settlement where [status]=(SELECT StatusTypeId FROM BUSDTA.Status_Type WHERE StatusTypeCD='NEW') AND SettlementID="+settlementId;
                int i = Convert.ToInt16(DbEngine.ExecuteScalar(query));
                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {

                flag = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][IsSettlementPendingVerification][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:IsSettlementPendingVerification]");

            return flag;
        }
        public static bool IsNonServicedStopPresent()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:IsNonServicedStopPresent]");

            bool flag = false;
            try
            {
                string baseDate = System.Configuration.ConfigurationManager.AppSettings["BaseDate"].ToString().Replace("-", "");

                string query1 = "select count(*) from busdta.M56M0004";
                int custCount = Convert.ToInt32(DbEngine.ExecuteScalar(query1));
                if (custCount == 0)
                {
                    flag = true; return flag;
                }
                string date = DateTime.Now.ToString("yyyy-MM-dd");

                
                string query = @"select COUNT(*) from busdta.M56M0004  where (RPCACT=0 or RPPACT!=0) AND RPSTDT >=  
                                (select  CONVERT(VARCHAR(8), isNull(  max( settlementdatetime),'" + baseDate + @"'), 112) from  busdta.Route_Settlement)  
                                and RPSTDT < getdate() and RPVTTP !='prospect' and RPSTTP in ('Rescheduled','Planned')";
                int i = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][IsNonServicedStopPresent][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:IsNonServicedStopPresent]");

            return flag;
        }

        public static string InsertCheckDetails(CheckEntryDetails objChk)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:InsertCheckDetails]");

            string checkID = string.Empty;
            try
            {
                string query = "insert into BUSDTA.CheckDetails (RouteId,CheckNumber,checkdetailsnumber,CustomerId,CheckAmount, " +
                            " CheckDate,CreatedBy,CreatedDatetime)" +
                            " values " +
                            " ( '" + objChk.RoutId + "','" + objChk.CheckNo + "','" + objChk.CheckNo + "','" + objChk.CustomerId + "','" + objChk.CheckAmount + "','" + objChk.CheckDate + "','" + objChk.CreatedBy + "',getdate())";
                DbEngine.ExecuteNonQuery(query);

                string querycheckID = "select CheckDetailsId from  BUSDTA.CheckDetails where CheckNumber='" + objChk.CheckNo.Trim() + "'";
                checkID = DbEngine.ExecuteScalar(querycheckID);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][InsertCheckDetails][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:InsertCheckDetails]");

            return checkID;
        }

        static int GetSettlementID()
        {
            int returnValue = 0;
            try
            {
                string value = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Settlement);
                returnValue = Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                returnValue = -1;
            }
            return returnValue;
        }

        public static string GetNewSettlementID()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:GetNewSettlementID]");

            string settlementID = string.Empty;
            try
            {
                string statusID = DbEngine.ExecuteScalar("SELECT statustypeId FROM busdta.Status_Type where StatusTypeCD='new'").ToString();
                //select SettlementID from busdta.Route_Settlement  where [Status]=11
                settlementID = DbEngine.ExecuteScalar("select SettlementID from busdta.Route_Settlement  where [Status]='" + statusID + "'").ToString();

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][GetNewSettlementID][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:GetNewSettlementID]");

            return settlementID;
        }

        public static void UpdateReturnOrderRelatedRecordStatus(string settlementId)
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][Start:UpdateReturnOrderRelatedRecordStatus]");

                //Get all record bsed TDPNTID=0 AND settlementid and tdrout=fbm105 and tdtyp='ReturnOrder'
                string queryFormat = "SELECT ISNULL(TDREFHDRID,0) AS TDREFHDRID FROM BUSDTA.M50012 A INNER JOIN  BUSDTA.Route_Master B ON A.TDROUT=B.RouteName WHERE A.TDPNTID=0 AND A.TDSTTLID={0} AND B.RouteMasterID={1} AND A.TDTYP='ReturnOrder'";
                string queryForReturnOrder = string.Format(queryFormat, settlementId, CommonNavInfo.RouteID.ToString());
                DataSet result = DbEngine.ExecuteDataSet(queryForReturnOrder);
                if (result.HasData())
                {
                    List<string> OrderReturnIdList = result.Tables[0].AsEnumerable().Select(dataRow => Convert.ToString(dataRow["TDREFHDRID"])).ToList();

                    if (OrderReturnIdList.Count == 0)
                        return; 

                    string strOrderReturn = OrderReturnIdList.Aggregate((oldValue, newValue) => oldValue + "," + newValue);

                    if (!string.IsNullOrEmpty(strOrderReturn))
                    {
                        //Create In query and update the Invoice Status 
                        ARPaymentManager objARpayment = new ARPaymentManager("", "");
                        objARpayment.UpdateReceiptStatus(strOrderReturn, "SETTLED");
                    }
                }
                
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][UpdateReturnOrderRelatedRecordStatus][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][SettlementConfirmationManager][End:UpdateReturnOrderRelatedRecordStatus]");
            

        }
    }
}
