﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System.Data;
using System.Globalization;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    class CreditProcessManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CycleCountManager");

        public ObservableCollection<CreditProcess> GetCreditProcessList(string customerNo)
        {
            ObservableCollection<CreditProcess> creditProcessList = new ObservableCollection<CreditProcess>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetCreditProcessList]");

            try
            {
                string query = "select rh.CustomerId, rh.RouteId, rh.ReceiptID, rh.ReceiptNumber, rh.ReasonCodeId,rh.StatusId, rh.CreditReasonCodeId, rh.CreditMemoNote, rh.TransactionAmount, rh.SettelmentId, s.StatusTypeCD, rh.UpdatedDatetime, ";
                query = query + " rc.ReasonCodeDescription from BUSDTA.Receipt_Header rh join BUSDTA.ReasonCodeMaster rc on rh.CreditReasonCodeId = rc.ReasonCodeId";
                //query = query + " join BUSDTA.ReasonCodeMaster s on rh.ReasonCodeId = rc.ReasonCodeId";
                query = query + " join BUSDTA.Status_Type s on s.StatusTypeID = rh.StatusId";
                query = query + " where CustomerId = '" + customerNo + "' and RouteId = '" + CommonNavInfo.RouteID + "'";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        CreditProcess creditProcess = new CreditProcess();
                        creditProcess.CustomerNo = row["CustomerId"].ToString().Trim();
                        creditProcess.RouteID = row["RouteId"].ToString().Trim();
                        creditProcess.ReceiptID = Convert.ToInt32(row["ReceiptID"].ToString().Trim());
                        creditProcess.ReceiptNumber = Convert.ToInt32(row["ReceiptNumber"]);
                        creditProcess.CreditAmount = Convert.ToDecimal(row["TransactionAmount"]).ToString("F2");
                        creditProcess.CreditReasonID = row["CreditReasonCodeId"]!=null?row["CreditReasonCodeId"].ToString() : null;
                        creditProcess.CreditReason = row["ReasonCodeDescription"].ToString().Trim();
                        creditProcess.VoidReasonID = row["ReasonCodeId"].ToString().Trim();
                        creditProcess.CreditNote = row["CreditMemoNote"].ToString().Trim();
                        creditProcess.StatusCode = row["StatusTypeCD"].ToString().Trim();
                        creditProcess.RouteSettlementId = string.IsNullOrEmpty(row["SettelmentId"].ToString()) ? string.Empty : row["SettelmentId"].ToString().Trim();
                        if (creditProcess.StatusCode == "USTLD")
                        {
                            creditProcess.CreditSeqID = 1;
                            creditProcess.Status = "UNSETTLED";
                        }
                        else if (creditProcess.StatusCode == "VOID")
                        {
                            creditProcess.CreditSeqID = 2;
                            creditProcess.Status = "VOID";
                        }
                        else if (creditProcess.StatusCode == "STTLD")
                        {
                            creditProcess.CreditSeqID = 3;
                            creditProcess.Status = "SETTLED";
                        }
                        else if (creditProcess.StatusCode == "VDSTLD")
                        {
                            creditProcess.CreditSeqID = 4;
                            creditProcess.Status = "VOIDED / SETTLED";
                        }
                        creditProcess.StatusID = Convert.ToInt32(row["StatusId"]);
                        creditProcess.CreditMemoDate = Convert.ToDateTime((row["UpdatedDatetime"]).ToString().Trim());
                        creditProcess.CustomerName = PayloadManager.OrderPayload.Customer.Name.Trim();
                        creditProcessList.Add(creditProcess);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetCreditProcessList][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetCreditProcessList]");

            return creditProcessList;
        }

         public int GetStatusId(string code)
        {
            int statusID = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][GetStatusId][Start:AddCreditMemo]");

            try
            {
                statusID = Convert.ToInt32(DbEngine.ExecuteScalar(string.Format("select StatusTypeID FROM busdta.Status_Type where StatusTypeCD = 'USTLD'")));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][GetStatusId][AddCreditMemo][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][GetStatusId][End:AddCreditMemo]");

            return statusID;
        }
        public void AddCreditMemo(CreditProcess creditObj)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:AddCreditMemo]");

            try
            {
                string query = "insert into BUSDTA.Receipt_Header(CustomerId, RouteId, ReceiptID, ReceiptNumber, ReceiptDate, TransactionAmount, PaymentMode, ReasonCodeId, ";
                query = query + " CreditReasonCodeId, CreditMemoNote, TransactionMode, StatusId, JDEStatusId, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
                query = query + " values ('" + creditObj.CustomerNo + "', '" + CommonNavInfo.RouteID + "', '" + creditObj.ReceiptID + "', '" + creditObj.ReceiptNumber + "', getdate(), '" + creditObj.CreditAmount + "', ";
                query = query + " '1', '"+creditObj.VoidReasonID +"', '"+creditObj.CreditReasonID+"', ";
                query = query + " '" + creditObj.CreditNote + "', '1', '" + creditObj.StatusID + "', ";
                query = query + " (select StatusTypeId from busdta.Status_Type where StatusTypeCD = 'INMOBL'), ";
                query = query + " '" + UserManager.UserId + "', getdate(), '" + UserManager.UserId + "', getdate())";

                int result = Helpers.DbEngine.ExecuteNonQuery(query);

                CollectionManager collectionManager = new CollectionManager();
                collectionManager.InsertReceiptInCustomerLedger(CommonNavInfo.RouteID.ToString(), creditObj.ReceiptNumber, CommonNavInfo.Customer.CustomerNo, Convert.ToDecimal(creditObj.CreditAmount));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][AddCreditMemo][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:AddCreditMemo]");

        }

        // update the status
        public void UpdateCreditMemoStatus( int receiptNo, string customerNo, string routeID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:UpdateCreditMemoStatus]");

            try
            {
                string query = "update BUSDTA.Receipt_Header set StatusId = (select StatusTypeID FROM busdta.Status_Type where StatusTypeCD = '" + StatusTypesEnum.VOID + "'), ";
                query = query + " UpdatedBy = '" + UserManager.UserId + "', UpdatedDatetime= getdate()";
                query = query + " where CustomerId = '" + customerNo + "' AND RouteId = '" + routeID + "' and ReceiptID = '" + receiptNo + "'";

                int result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][UpdateCreditMemoStatus][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:UpdateCreditMemoStatus]");

        }

        public int GetNewCreditMemoNumber()
        {
            int OrderNumber = 0;
            try
            {
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.CreditMemo);
                OrderNumber = Convert.ToInt32(val);
            }
            catch (Exception ex)
            {
                OrderNumber = -1;
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetNewCreditMemoNumber][RouteID=" + CommonNavInfo.RouteID.ToString() + "][ForEntity=" + Managers.NumberManager.Entity.CreditMemo.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            return OrderNumber;
        }
        //get credit reasons
        public ObservableCollection<ReasonCode> GetCreditReasonCode()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetCreditReasonCode]");

            try
            {
                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Credit Memo'").Tables[0];

                ReasonCode ReasonCode = new ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetCreditReasonCode " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetCreditReasonCode]");

            return reasonCodeList;
        }


        //get void reasons
        public ObservableCollection<ReasonCode> GetVoidReasonCode()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetVoidReasonCode]");

            try
            {
                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Credit Memo'").Tables[0];

                ReasonCode ReasonCode = new ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetVoidReasonCode " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetVoidReasonCode]");

            return reasonCodeList;
        }

        public int GetNewReceiptNumber()
        {
            int ReceiptNum = 0;
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetNewReceiptNumber]");

            try
            {
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Receipt);
                ReceiptNum = Convert.ToInt32(val);

                //int updateStopActivityFlag = DbEngine.ExecuteNonQuery("update busdta.m56m0004 set rpactid=null where rpstid = " + CommonNavInfo.Customer.StopID + " and rpan8=" + CommonNavInfo.Customer.CustomerNo + "");
                //int updatePreOrderFlag = DbEngine.ExecuteNonQuery("update BUSDTA.M4016 SET POSTFG = '1' where POAN8 = '" + CommonNavInfo.Customer.CustomerNo + "' and POSTDT = '" + CommonNavInfo.Customer.StopDate.Value.ToString("yyyy-MM-dd") + "'");
            }
            catch (Exception ex)
            {
                ReceiptNum = -1;
                Logger.Error("[SalesLogicExpress.Application.Managers][CreditProcessManager][GetNewReceiptNumber][RouteID=" + CommonNavInfo.RouteID.ToString() + "][ForEntity=" + Managers.NumberManager.Entity.SalesOrder.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][End:GetNewReceiptNumber]");

            return ReceiptNum;
        }

       
    }
}
