﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System.Data;
using System.Globalization;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    public class ExpensesManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ExpensesManager");

        public ObservableCollection<Expenses> GetExpensesList()
        {
            ObservableCollection<Expenses> expensesList = new ObservableCollection<Expenses>();
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:GetExpensesList]");

            try
            {
                string query = "select ED.ExpenseId, ED.RouteId, ED.CategoryId, ED.ExpensesExplanation, ED.ExpenseAmount, ED.StatusId, ED.ExpensesDatetime, ED.VoidReasonId, ED.TransactionId, ED.RouteSettlementId, ";
                query = query + " ED.UpdatedDatetime, ST.StatusTypeCD as StatusCode, ST.StatusTypeDESC as Status, CT.CategoryTypeDESC as Category, CT.CategoryTypeCD as CategoryCode from BUSDTA.ExpenseDetails ED ";
                query = query + " join BUSDTA.Status_Type ST on ST.StatusTypeID = ED.StatusId ";
                query = query + " join BUSDTA.Category_Type CT on ED.CategoryId = CT.CategoryTypeID where ED.RouteId = '" + CommonNavInfo.RouteID + "'";
                query = query + " ORDER BY ED.UpdatedDatetime DESC";

                DataSet result = DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        Expenses expense = new Expenses();

                        expense.ExpenseID = row["ExpenseId"].ToString().Trim();
                        expense.RouteID = row["RouteId"].ToString().Trim();
                        expense.CategotyID = row["CategoryId"].ToString().Trim();
                        expense.CategoryCode = string.IsNullOrEmpty(row["CategoryCode"].ToString()) ? string.Empty : row["CategoryCode"].ToString().Trim();
                        if (expense.CategoryCode.ToLower().ToUpper() == "MOCHR")
                        {
                            expense.IsMoneyOrderCharge = true;
                        }
                        expense.ExpenseCategory = string.IsNullOrEmpty(row["Category"].ToString()) ? string.Empty : row["Category"].ToString().Trim();
                        expense.StatusID = row["StatusId"].ToString().Trim();
                        expense.StatusCode = row["StatusCode"].ToString().Trim();
                        expense.Status = row["Status"].ToString().Trim().ToLower().ToUpper();
                        expense.VoidReasonID = string.IsNullOrEmpty(row["VoidReasonId"].ToString()) ? string.Empty : row["VoidReasonId"].ToString().Trim();
                        expense.ExpenseAmount = (Convert.ToDecimal(row["ExpenseAmount"])).ToString("F2").Trim();
                        expense.ExpenseDate = Convert.ToDateTime(row["UpdatedDatetime"]);
                        expense.Explanation = row["ExpensesExplanation"].ToString().Trim();
                        expense.TransactionId = string.IsNullOrEmpty(row["TransactionId"].ToString()) ? string.Empty : row["TransactionId"].ToString();
                        expense.RouteSettlementId = string.IsNullOrEmpty(row["RouteSettlementId"].ToString()) ? string.Empty : row["RouteSettlementId"].ToString();

                        if (!string.IsNullOrEmpty(expense.TransactionId))
                        {
                            // to check whether transaction is MO or other expense.
                            query = "select count(isnull(MoneyOrderId,0)) from BUSDTA.MoneyOrderDetails where MoneyOrderId = '" + expense.TransactionId + "'";
                            expense.IsMoneyOrderCharge = Convert.ToInt32(DbEngine.ExecuteScalar(query)) > 0 ? true : false;
                        }
                        
                        expensesList.Add(expense);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][GetExpensesList][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetExpensesList]");

            return expensesList;
        }

        public Expenses AddExpenses(Expenses expense)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:AddExpenses]");
            //Expenses exp = null;
            try
            {
                string query = "insert into BUSDTA.ExpenseDetails(ExpenseId, RouteId, CategoryId, ExpensesExplanation, ExpenseAmount, StatusId, ExpensesDatetime, VoidReasonId, RouteSettlementId, TransactionId, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
                query = query + " VALUES((select isnull(max(ExpenseId),0)+1 from BUSDTA.ExpenseDetails), '" + CommonNavInfo.RouteID + "', '" + (string.IsNullOrEmpty(expense.CategotyID) ? "null" : expense.CategotyID) + "', '" + (string.IsNullOrEmpty(expense.Explanation) ? "null" : expense.Explanation) + "', ";
                query = query + " '" + expense.ExpenseAmount + "', (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'USTLD'), ";
                query = query + " getdate(), " + (string.IsNullOrEmpty(expense.VoidReasonID) ? "null" : expense.VoidReasonID) + ", null, " + (string.IsNullOrEmpty(expense.TransactionId) ? "null" : expense.TransactionId) + ", ";
                query = query + "'" + UserManager.UserId + "', getdate(), '" + UserManager.UserId + "', getdate())";

                int result = DbEngine.ExecuteNonQuery(query);

                if (result == 1)
                {
                    query = "select top 1 StatusId, ExpenseId from BUSDTA.ExpenseDetails order by UpdatedDatetime DESC";
                    DataSet obj = DbEngine.ExecuteDataSet(query);
                    if (obj.HasData())
                    {
                        DataRow dr = obj.Tables[0].Rows[0];
                        expense.StatusID = dr["StatusId"].ToString();
                        expense.ExpenseID = dr["ExpenseId"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][AddExpenses][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:AddExpenses]");
            return expense;
        }

        public int UpdateExpenses(Expenses expenses, bool flag)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:UpdateExpenses]");
            int statusID = -1;
            try
            {
                string query = string.Empty;
                if (flag)
                {
                     query = "update BUSDTA.ExpenseDetails set CategoryId = '" + expenses.CategotyID + "', ExpensesExplanation = '" + expenses.Explanation + "', ExpenseAmount='" + expenses.ExpenseAmount + "', ";
                     query = query + " UpdatedDatetime = getdate() where RouteID = '" + expenses.RouteID + "' and ExpenseId = '" + expenses.ExpenseID + "'";

                }
                else
                {
                     query = "update BUSDTA.ExpenseDetails set StatusId = (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'VOID'), VoidReasonId = '" + expenses.VoidReasonID + "', ";
                     query = query + " UpdatedDatetime = getdate() where RouteID = '" + expenses.RouteID + "' and ExpenseId = '" + expenses.ExpenseID + "'";

                }

                int result = DbEngine.ExecuteNonQuery(query);

                if (result ==1)
                {
                    query = "select StatusId from BUSDTA.ExpenseDetails where RouteID = '" + expenses.RouteID + "' and ExpenseId = '" + expenses.ExpenseID + "'";
                     statusID = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][UpdateExpenses][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:UpdateExpenses]");
            return statusID;
        }

        public ObservableCollection<ReasonCode> GetVoidReasonCode()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetVoidReasonCode]");

            try
            {
                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Expenses'").Tables[0];

                ReasonCode ReasonCode = new ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString().Trim());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString().Trim();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][GetVoidReasonCode " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetVoidReasonCode]");

            return reasonCodeList;
        }

        public ObservableCollection<ExpenseCategory> GetCategoryForExpenses()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetCategoryForExpenses]");
            ObservableCollection<ExpenseCategory> categoryList = new ObservableCollection<ExpenseCategory>();
            try
            {
                DataTable dt = DbEngine.ExecuteDataSet("SELECT CategoryTypeID Id, CategoryTypeCD Code,rtrim(ltrim(ISNULL(CategoryTypeDESC,''))) CategoryTypeDESC FROM BUSDTA.Category_Type where CategoryCodeType='Expenses'").Tables[0];

                foreach (DataRow dr in dt.Rows)
                {
                    ExpenseCategory category = new ExpenseCategory();
                    category.CategoryID = Convert.ToInt32(dr["Id"].ToString().Trim());
                    category.Code = dr["Code"].ToString().Trim();
                    category.Description = dr["CategoryTypeDESC"].ToString().Trim();
                    categoryList.Add(category);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][GetCategoryForExpenses " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetCategoryForExpenses]");
            return categoryList;
        }

    }
}
