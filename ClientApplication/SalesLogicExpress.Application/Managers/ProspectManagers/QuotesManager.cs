﻿using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers.ProspectManagers
{
    public class QuotesManager
    {
        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.QuotesManager");
        public static ObservableCollection<QuotePickModel> GetQuotePickCollection()
        {
            string QuoteID = PayloadManager.QuotePayload.QuoteId;
            Logger.Info("[SalesLogicExpress.Application.Managers][QuotesManager][Start:GetQuotePickCollection]");
            ObservableCollection<QuotePickModel> searchData = new ObservableCollection<QuotePickModel>();

            string query = @"select A.ProspectQuoteDetailId as 'TransactionDetailID'
                                , inventory.ItemId AS 'ItemId'
                                , LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNumber'
                                ,isnull( im.IMDSC1,'') as 'ItemDescription'
                                , ISNULL(A.SampleQty,0) AS 'ShippedQty'
                                ,isnull(inventory.OnHandQuantity,0) as 'ActualQtyOnHand'
                                ,(isnull(inventory.OnHandQuantity,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PickingUM'
                                ,A.SampleQty as 'TransactionQty'
                                ,A.SampleQty as 'TransactionQtyDisplay'
                                ,A.SampleUM as 'TransactionUOM'
                                ,pd.PickQtyPrimaryUOM as 'PickQtyPrimaryUOM'
                                ,pd.PickedQty as 'PickedQty'
                                ,(pd.PickedQty -A.SampleQty) as 'ExceptionCount'
                                ,pd.TransactionQtyPrimaryUOM  AS 'TransactionQtyPrimaryUOM',inventory.ParLevel
                                ,A.SampleUM  AS 'UM', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                                from busdta.Prospect_Quote_Detail A 
                                left join busdta.PICK_DETAIL pd on pd.TransactionDetailID=A.ProspectQuoteDetailId and A.ItemId=pd.ItemID  and pd.TransactionID=A.ProspectQuoteId and pd.TransactionUOM=A.SampleUM 
                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
                                where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ProspectQuoteId='" + QuoteID + @"'  
                                AND A.SampleQty!=0 
                                order by  A.ProspectQuoteDetailId asc ";
            List<QuotePickModel> activities = new List<QuotePickModel>();
            try
            {
                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    activities = result.GetEntityList<QuotePickModel>();
                    foreach (QuotePickModel item in activities)
                    {
                        if (item.TransactionUOM=="loose")
                        {
                            item.PickedQty =Convert.ToInt32( item.TransactionQty);
                            item.PickQtyPrimaryUOM = item.PickedQty;
                            item.PickingUM = item.TransactionUOM;
                        }
                    }
                }
                searchData = new ObservableCollection<QuotePickModel>(activities);
                searchData = new ObservableCollection<QuotePickModel>(searchData.OrderBy(x => x.TransactionUOM));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QuotesManager][QuotesManager][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QuotesManager][End:QuotesManager]");


            return searchData;
        }
    }
}
