﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class SuggestionReturnAlgo
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.SuggestionReturnAlgo");
        string Route; DateTime FromDate; DateTime ToDate; string RouteID;


        private List<StopListClass> stopList = new List<StopListClass>();
        public List<StopListClass> StopList
        {
            get { return stopList; }
            set { stopList = value; }
        }



        private List<ItemDemandListClass> itemDemandList = new List<ItemDemandListClass>();
        public List<ItemDemandListClass> ItemDemandList
        {
            get { return itemDemandList; }
            set { itemDemandList = value; }
        }



        private List<string> returnItemList = new List<string>();
        public List<string> ReturnItemList
        {
            get { return returnItemList; }
            set { returnItemList = value; }
        }




        private List<AddLoadDetails> inventoryClassList = new List<AddLoadDetails>();
        public List<AddLoadDetails> InventoryClassList
        {
            get { return inventoryClassList; }
            set { inventoryClassList = value; }
        }



        private List<RouteReplenishmentDetailsClass> routeReplenishmentDetailsClassList = new List<RouteReplenishmentDetailsClass>();
        public List<RouteReplenishmentDetailsClass> RouteReplenishmentDetailsClassList
        {
            get { return routeReplenishmentDetailsClassList; }
            set { routeReplenishmentDetailsClassList = value; }
        }



        private List<AddLoadDetails> replishmentSuggestionList = new List<AddLoadDetails>();
        public List<AddLoadDetails> ReplishmentSuggestionList
        {
            get { return replishmentSuggestionList; }
            set { replishmentSuggestionList = value; }
        }
        
        //route='FBM598'
        void GenerateCustomerStopList()
        {
            try
            {

                string query = @"select 
                                A.RPAN8 AS 'CustomerNumber'
                                ,A.RPSTDT AS 'StopDate'
                                ,isnull(A.RPACTID,'') as 'NoActivity'
                                from busdta.M56M0004 A 
                                where A.RPSTTP='PLANNED'  
                                AND A.RPSTDT BETWEEN '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + ToDate.ToString("yyyy-MM-dd") + @"' 
                                AND A.RPROUT='" + Route + "'  ";
               

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    StopList = result.GetEntityList<StopListClass>();

                    foreach (StopListClass item in StopList)
                    {

                        if (item.NoActivity == "NOSALE")
                        {
                            //Check for preorder
                            string queryCheckPreorder = "select count(*) from BUSDTA.M4016  WHERE  POAN8 ='" + item.CustomerNumber.Trim() + "' AND POSTDT='" + item.StopDate.ToString("yyyy-MM-dd") + "'";
                            int count = Convert.ToInt32(DbEngine.ExecuteScalar(queryCheckPreorder));
                            if (count > 0)
                            {
                                item.HasPreOrder = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReturnAlgo][GenerateCustomerStopList][ExceptionStackTrace = " + ex.StackTrace + "]");


            }
        }

        void GenerateItemDemandList()
        {
            try
            {
                foreach (StopListClass item in StopList)
                {
                    if (item.HasPreOrder)
                    {
                        // Get preorder item list
                        string queryGetPreorderItem = @"select  B.POAN8  as 'CustomerNumber' ,ISNULL(B.POUOM,'') AS 'PreOrderUOM'
                                                        ,ISNULL( B.POQTYU / 10000 ,0)AS 'PreOrderQty', B.POITM AS 'ItemID' 
                                                        from BUSDTA.M4016  B where  B.POAN8    ='" + item.CustomerNumber + "' AND B.POSTDT='" + item.StopDate.ToString("yyyy-MM-dd") + "'  ";

                        DataSet result1 = Helpers.DbEngine.ExecuteDataSet(queryGetPreorderItem);
                        List<PreOrderItems> PreOrderItems = new List<PreOrderItems>();

                        if (result1.HasData())
                        {

                            PreOrderItems = result1.GetEntityList<PreOrderItems>();

                            foreach (PreOrderItems item1 in PreOrderItems)
                            {
                                string queryPrimaryUM = " select PRIMARYUM from   busdta.ItemConfiguration  WHERE ITEMID='" + item1.ItemID + "'";
                                item1.PrimaryUM = DbEngine.ExecuteScalar(queryPrimaryUM);

                                string queryItemNo = "     SELECT LTRIM(RTRIM(C.IMLITM)) AS 'ItemNumber' FROM busdta.F4101 C WHERE IMITM='" + item1.ItemID + "'";
                                item1.ItemNumber = DbEngine.ExecuteScalar(queryItemNo);

                                item1.ConvesionFactor = SetConversionFactorForItem(item1.ItemID, item1.PreOrderUOM, item1.ItemNumber, item1.PrimaryUM);
                                item1.PreOrderQtyInPrimaryUM = Convert.ToInt32(item1.PreOrderQty * item1.ConvesionFactor);

                                ItemDemandListClass o = new ItemDemandListClass();
                                o = ItemDemandList.FirstOrDefault(x => x.ItemID == item1.ItemID);
                                if (ItemDemandList.Contains(o))
                                {
                                    o.DemandQuantity += item1.PreOrderQtyInPrimaryUM;
                                }
                                else
                                {
                                    o = new ItemDemandListClass();
                                    o.ItemID = item1.ItemID;
                                    o.DemandQuantity = item1.PreOrderQtyInPrimaryUM;
                                    ItemDemandList.Add(o);
                                }
                            }


                        }


                    }
                    else
                    {
                        string queryCustHistDemand = "select ItemID,NetQtySoldPerStop  from busdta.Metric_CustHistDemand A  where A.CustomerID= '" + item.CustomerNumber + "'";
                        List<MetricCustHistDemandClass> MetricCustHistDemandList = new List<MetricCustHistDemandClass>();

                        DataSet result = Helpers.DbEngine.ExecuteDataSet(queryCustHistDemand);
                        if (result.HasData())
                        {
                            MetricCustHistDemandList = result.GetEntityList<MetricCustHistDemandClass>();
                        }
                        foreach (MetricCustHistDemandClass item1 in MetricCustHistDemandList)
                        {

                            ItemDemandListClass o = new ItemDemandListClass();
                            o = ItemDemandList.FirstOrDefault(x => x.ItemID == item1.ItemID);
                            if (ItemDemandList.Contains(o))
                            {
                                o.DemandQuantity += item1.NetQtySoldPerStop;
                            }
                            else
                            {
                                o = new ItemDemandListClass();
                                o.ItemID = item1.ItemID;
                                o.DemandQuantity = item1.NetQtySoldPerStop;
                                ItemDemandList.Add(o);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReturnAlgo][GenerateItemDemandList][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
        }

        void GenerateReplenishmentItemList()
        {
            try
            {
                foreach (ItemDemandListClass item in ItemDemandList)
                {
                    ReturnItemList.Add(item.ItemID.Trim());
                }

                //Query Inventory

                string queryInventory = @"select A.ItemId from busdta.Inventory  A 
                                        LEFT JOIN busdta.ItemConfiguration D ON D.ItemId=A.ItemId  left join BUSDTA.ItemUoMs F ON F.ItemID=A.ItemId  where  A.RouteId='" + RouteID + @"'  
                                        and (A.OnHandQuantity -A.CommittedQuantity-A.HeldQuantity)> A.ParLevel and D.RouteEnabled = 1 and D.allowSearch=1   AND F.CanSell=1  ";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(queryInventory);
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        string itemId = result.Tables[0].Rows[i][0].ToString();
                        if (!ReturnItemList.Contains(itemId))
                        {
                            ReturnItemList.Add(itemId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReturnAlgo][GenerateReplenishmentItemList][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
        }
        public List<AddLoadDetails> GenerateSuggestionReturnList()
        {

            Route = ViewModels.CommonNavInfo.RouteUser; FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate; ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate; RouteID = ViewModels.CommonNavInfo.RouteID.ToString();
            GenerateCustomerStopList();
            GenerateItemDemandList();
            GenerateReplenishmentItemList();

            string separatedItems = ReturnItemList.Count > 1 ? ReturnItemList.Aggregate<string>((old, newstring) => old + ',' + newstring) : (ReturnItemList.Count == 1) ? ReturnItemList[0] : "''";

            try
            {

                //Query Inventory
                string queryInventory = @"select   inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty',
                                inventory.CommittedQuantity AS 'CommittedQty', 
                                inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
                                (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty',
                                isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4',
                                isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM'
                                from busdta.Inventory inventory 
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID   
                                left outer join BUSDTA.ItemUoMs iu on inventory.ItemId = iu.ItemID  
                                where iu.CanSell=1 and inventory.ItemId in (" + separatedItems + ") and iconfig.RouteEnabled = 1  and iconfig.allowSearch=1 order by ItemNumber ";
                DataSet resultInventory = Helpers.DbEngine.ExecuteDataSet(queryInventory);
                if (resultInventory.HasData())
                {
                    InventoryClassList = resultInventory.GetEntityList<AddLoadDetails>();
                }


                //Query RouteReplenishmentDetails


                string queryDetail = @"select X.AdjustedQty ,A.ReplenishmentQty,A.ReplenishmentQtyUM ,F.PrimaryUM,ltrim(rtrim( A.ItemID ))  as 'ItemID',
                                    LTRIM(RTRIM( H.IMLITM)) AS 'ItemNumber' ,X.PickedQty
                                    from busdta.Route_Replenishment_Detail     A 
                                    JOIN BUSDTA.Pick_Detail X ON A.ITEMID =X.ITEMID AND A.ReplenishmentID=X.TransactionID
                                    JOIN BUSDTA.ItemConfiguration F ON A.ItemID=F.ItemID
                                    JOIN BUSDTA.F4101 H ON H.IMITM=F.ItemID
                                    WHERE F.RouteEnabled = 1 and F.allowSearch=1 AND A.ItemId in (" + separatedItems + @")  AND  A.ReplenishmentID IN  
                                    (   SELECT B.ReplenishmentID FROM BUSDTA.Route_Replenishment_Header B 
                                        WHERE B.StatusId  IN (select b.StatusTypeID from busdta.Status_Type B 
                                        WHERE  B.StatusTypeCD in ('RELS','RTP','HOP')) and B.ToBranchId ='" + UserManager.UserRoute + @"')";
                DataSet resultRouteReplenishmentDetails = Helpers.DbEngine.ExecuteDataSet(queryDetail);
                if (resultRouteReplenishmentDetails.HasData())
                {
                    RouteReplenishmentDetailsClassList = resultRouteReplenishmentDetails.GetEntityList<RouteReplenishmentDetailsClass>();
                }

                foreach (var itemIdInList in ReturnItemList)
                {
                    AddLoadDetails objToAdd = new AddLoadDetails();

                    objToAdd = InventoryClassList.FirstOrDefault(x => x.ItemId == itemIdInList.ToString());
                    if (objToAdd == null)
                    {
                        continue;
                    }
                    objToAdd.AvailableQty = objToAdd.AvailableQty;
                    objToAdd.ParLevel = objToAdd.ParLevel;

                    #region OnOrderQty
                    RouteReplenishmentDetailsClass obj = RouteReplenishmentDetailsClassList.FirstOrDefault(x => x.ItemID == itemIdInList.ToString());
                    if (obj == null)
                    {
                        objToAdd.OnOrderQtyInPrimaryUM = 0;
                    }
                    else
                    {
                        double ConvesionFactor = SetConversionFactorForItem(obj.ItemID, obj.ReplenishmentQtyUM, obj.ItemNumber.ToString(), obj.PrimaryUM);

                        List<RouteReplenishmentDetailsClass> objList = RouteReplenishmentDetailsClassList.Where(x => x.ItemID == itemIdInList.ToString()).ToList<RouteReplenishmentDetailsClass>();

                        foreach (RouteReplenishmentDetailsClass item11 in objList)
                        {
                            double cFactor = SetConversionFactorForItem(item11.ItemID, item11.ReplenishmentQtyUM, item11.ItemNumber.ToString(), item11.PrimaryUM);
                            item11.OnOrderQty = (item11.ReplenishmentQty > item11.AdjustedQty ? item11.ReplenishmentQty : item11.AdjustedQty) - item11.PickedQty;
                            item11.OnOrderQtyInPrimaryUM = Convert.ToInt32(item11.OnOrderQty * cFactor);
                            objToAdd.OnOrderQtyInPrimaryUM = (objToAdd.OnOrderQtyInPrimaryUM + item11.OnOrderQtyInPrimaryUM);
                        }
                    }

                    objToAdd.OpenReplnQty = objToAdd.OnOrderQtyInPrimaryUM.ToString();
                    #endregion


                    ItemDemandListClass objDemand = ItemDemandList.FirstOrDefault(x => x.ItemID == itemIdInList.ToString());
                    objToAdd.DemandQty = objDemand == null ? 0 : objDemand.DemandQuantity;


                    //Set Load Quantity
                    if (((objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM) > objToAdd.ParLevel) || ((objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM) > objToAdd.DemandQty))
                    {
                        if (objToAdd.ParLevel < objToAdd.DemandQty)
                        {
                            objToAdd.LoadQuantity = (objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM)-  objToAdd.DemandQty ;
                        }
                        else if (objToAdd.ParLevel >= objToAdd.DemandQty)
                        {
                            objToAdd.LoadQuantity = (objToAdd.AvailableQty + objToAdd.OnOrderQtyInPrimaryUM) - objToAdd.ParLevel ;
                        }
                        objToAdd.SuggestedQty = objToAdd.LoadQuantity;

                        //************************************************************************************************
                        // Comment: Added to allow only qty greater than  zero suggested qty
                        // Created: Jan 24, 2016
                        // Author: Vivensas (Rajesh,Yuvaraj)
                        // Revisions: 
                        //*************************************************************************************************

                        if (objToAdd.SuggestedQty > 0)
                        {
                            ReplishmentSuggestionList.Add(objToAdd);
                        }
                        //*************************************************************************************************
                        // Vivensas changes ends over here
                        //*************************************************************************************************
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][SuggestionReturnAlgo][GenerateReplishmentSuggestionList][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            return ReplishmentSuggestionList;
        }
        static double SetConversionFactorForItem(string ItemID, string secondUM, string ItemNumber, string PrimaryUM)
        {
            double ConvesionFactor = 1.0;
            try
            {
                if (UoMManager.ItemUoMFactorList != null)
                {

                    var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == (ItemID.Trim()) && (x.FromUOM == (secondUM.ToString().Trim()) && (x.ToUOM == (PrimaryUM.Trim())))));
                    if (itemUomConversion == null)
                        ConvesionFactor = UoMManager.GetUoMFactor(secondUM, PrimaryUM, Convert.ToInt32(ItemID), ItemNumber.Trim());
                    else
                        ConvesionFactor = itemUomConversion.ConversionFactor;
                }
                else
                {
                    ConvesionFactor = UoMManager.GetUoMFactor(secondUM, PrimaryUM, Convert.ToInt32(ItemID), ItemNumber.Trim());
                }
            }
            catch (Exception ex)
            {

            }

            return ConvesionFactor;
        }

    }
}
