﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using log4net;
using System.Collections.ObjectModel;
using System.Data;
using System.ComponentModel;
namespace SalesLogicExpress.Application.Managers
{
    public class NotificationManager
    {
        public enum NotificationType
        {
            [Description("26")]
            ActionInformation,
            [Description("27")]
            ActionWarning,
            [Description("28")]
            ActionOptional,
            [Description("29")]
            ActionMandatory
        }
        public readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public TrulyObservableCollection<Notification> GetNotifications()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:GetNotifications]");
            TrulyObservableCollection<Notification> result = new TrulyObservableCollection<Notification>();
            try
            {
                string query = string.Empty;
                query = "select * from busdta.Notification where Read='{0}'";
                query = string.Format(query, 'N');
                DataSet dbResult = DbEngine.ExecuteDataSet(query);
                if (dbResult.HasData())
                {
                    result = new TrulyObservableCollection<Notification>(dbResult.GetEntityList<Notification>());
                    foreach (Notification item in result)
                    {
                        item.NotificationAge = item.CreatedDateTime.GetPrettyDate();
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionInformation.GetEnumDescription())
                        {
                            item.Indicator = "I";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionMandatory.GetEnumDescription())
                        {
                            item.Indicator = "M";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionOptional.GetEnumDescription())
                        {
                            item.Indicator = "O";
                        }
                        if (item.TypeID == Managers.NotificationManager.NotificationType.ActionWarning.GetEnumDescription())
                        {
                            item.Indicator = "W";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][GetNotifications][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:GetNotifications]");
            return result;
        }
        public bool MarkNotificationAsRead(Notification UpdatedNotification)
        {
            bool returnValue = false;
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][Start:MarkNotificationAsRead]");
            try
            {
                string query = string.Empty;
                query = "Update busdta.Notification set Read='{0}' where NotificationID='{1}'";
                query = string.Format(query, "Y", UpdatedNotification.NotificationID);
                int result = DbEngine.ExecuteNonQuery(query);
                returnValue = result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][NotificationManager][MarkNotificationAsRead][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][NotificationManager][End:MarkNotificationAsRead]");
            return returnValue;
        }
    }
}
