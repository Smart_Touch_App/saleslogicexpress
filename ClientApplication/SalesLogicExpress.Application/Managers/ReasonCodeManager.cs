﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class ReasonCodeManager
    {
        private readonly ILog Logger = LogManager.GetLogger("");
        public List<ReasonCodeVM.ReasonCode> GetReasonCodes()
        {

            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][Start:GetReasonCode]");
            List<ReasonCodeVM.ReasonCode> reasonCodes = new List<ReasonCodeVM.ReasonCode>();
            try
            {
                DataTable dtReasonCodes = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Order'").Tables[0];
                foreach (DataRow drReasonCode in dtReasonCodes.Rows)
                {
                    reasonCodes.Add(new ReasonCodeVM.ReasonCode() { ID = Convert.ToInt32(drReasonCode["Id"].ToString()), Code = drReasonCode["ReasonCodeDescription"].ToString() });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ReasonCodeManager][GetReasonCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ReasonCodeManager][End:GetReasonCode]");
            return reasonCodes;
        }
    }
}
