﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    public class CustomerReturnsManager
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerReturnsManager");

        public CustomerReturnsManager()
        {

        }

        /// <summary>
        /// Returns list of items with avilable retun qty 
        /// </summary>
        /// <param name="IncludeZeroAvailableItems">True if need to return item whose all quantities has been returned, false as otherwise</param>
        /// <returns>List of tem with their order information</returns>
        public ObservableCollection<ReturnItem> GetItemsForOrdersTab(bool IncludeZeroAvailableItems)
        {
            ObservableCollection<ReturnItem> returnItems = new ObservableCollection<ReturnItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetItemsForOrdersTab]");
            StringBuilder queryBuilder = new StringBuilder();
            try
            {
                queryBuilder.Append("SELECT distinct");
                queryBuilder.Append("\nS1.OrderID,S1.EnergySurchargeAmt");
                queryBuilder.Append("\n,TL.TDSTID as SettlementID");
                queryBuilder.Append("\n,S1.OrderDetailID , S1.OrderDetailID AS 'TransactionDetailID'");
                queryBuilder.Append("\n,S1.ItemId");
                queryBuilder.Append("\n,im.IMLITM as ItemNumber");
                queryBuilder.Append("\n,im.IMDSC1 as ItemDescription");
                queryBuilder.Append("\n,im.IMLNTY as StkType");
                queryBuilder.Append("\n,im.IMSRP1  as SalesCat1");
                queryBuilder.Append("\n,im.IMSRP5 as SalesCat5");
                queryBuilder.Append("\n,im.IMSRP4 as SalesCat4");
                queryBuilder.Append("\n,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty");
                queryBuilder.Append("\n,S1.OrderUM as OrderUM");
                queryBuilder.Append("\n,S1.OrderQty");
                queryBuilder.Append("\n,S1.RouteId");
                queryBuilder.Append("\n,S1.UnitPriceAmt");
                queryBuilder.Append("\n,S1.ExtnPriceAmt");
                queryBuilder.Append("\n,S1.ItemSalesTaxAmt");
                queryBuilder.Append("\n,S1.IsTaxable");
                queryBuilder.Append("\n,S1.ORDERQTY- ISNULL(S2.ORDERQTY,0) AS AvailableReturnQty");
                queryBuilder.Append("\n,inv.OnHandQuantity as OnHandQty");
                queryBuilder.Append("\n,inv.CommittedQuantity as CommittedQty");
                queryBuilder.Append("\n,inv.HeldQuantity as HeldQty");
                queryBuilder.Append("\n,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty ");
                queryBuilder.Append("\n,iconfig.PrimaryUM as PrimaryUM");
                queryBuilder.Append("\n,iconfig.PricingUM");
                queryBuilder.Append("\n,iconfig.TransactionUM as DefaultUM");
                queryBuilder.Append("\n,iconfig.OtherUM1 as OtherUM1");
                queryBuilder.Append("\n,iconfig.OtherUM2");
                queryBuilder.Append("\n,S1.OrderDate");
                queryBuilder.Append("\nFROM (");
                queryBuilder.Append("\nSELECT  OH.*, OD.OrderUM, OD.ORDERDETAILID,OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable");
                queryBuilder.Append("\nFROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID");
                queryBuilder.Append("\nWHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + " and OH.OrderStateId in (" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ",");
                queryBuilder.Append("\n" + ActivityKey.OrderReturned.GetStatusIdFromDB() + "," + ActivityKey.ItemReturn.GetStatusIdFromDB() + ")) S1");
                queryBuilder.Append("\nLEFT OUTER JOIN(");
                queryBuilder.Append("\nSELECT SUM(OD.ORDERQTY) AS ORDERQTY ,OD.ITEMID,SRM.SALESoRDERID AS RORDERDETAILID FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON");
                queryBuilder.Append("\nOH.ORDERID=OD.ORDERID");
                queryBuilder.Append("\nLEFT OUTER JOIN busdta.SalesOrder_ReturnOrder_Mapping SRM ON  OD.ORDERDETAILID=SRM.ORDERDETAILID");
                queryBuilder.Append("\nWHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND OH.OrderStateId not in(" + ActivityKey.VoidAtROEntry.GetStatusIdFromDB() + ",");
                queryBuilder.Append("\n" + ActivityKey.VoidAtROPick.GetStatusIdFromDB() + "," + ActivityKey.CreateReturnOrder.GetStatusIdFromDB() + ") AND ISNULL(RORDERDETAILID,0) <> 0");
                queryBuilder.Append("\n GROUP BY OD.ITEMID,SRM.SALESoRDERID");
                queryBuilder.Append("\n) S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID");
                queryBuilder.Append("\njoin busdta.F4101 im on S1.ItemId = im.IMITM ");
                queryBuilder.Append("\njoin busdta.F4102 ib on ib.IBLITM = im.IMLITM ");
                queryBuilder.Append("\njoin busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID");
                queryBuilder.Append("\njoin busdta.Inventory inv on inv.ItemId = S1.ItemId");
                queryBuilder.Append("\nLEFT OUTER JOIN BUSDTA.M50012 TL ON TL.TDREFHDRID=S1.OrderID");
                queryBuilder.Append("\nwhere S1.CustomerID='" + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "'" + " order by S1.OrderId desc"); // + (124 ? "" : "  AND (S1.ORDERQTY- ISNULL(S2.ORDERQTY,0))>0 ") + ** Condition removed as order is getting selected when we have one or more item returned

                var dbResult = DbEngine.ExecuteDataSet(queryBuilder.ToString());
                if (dbResult.HasData())
                {
                    foreach (DataRow datarow in dbResult.Tables[0].Rows)
                    {
                        ReturnItem returnItem = new ReturnItem
                        {
                            ItemId = string.IsNullOrEmpty(datarow["ItemId"].ToString()) ? string.Empty : datarow["ItemId"].ToString().Trim(),
                            ItemNumber = string.IsNullOrEmpty(datarow["ItemNumber"].ToString()) ? string.Empty : datarow["ItemNumber"].ToString().Trim(),
                            ItemDescription = string.IsNullOrEmpty(datarow["ItemDescription"].ToString()) ? string.Empty : datarow["ItemDescription"].ToString().Trim(),
                            StkType = string.IsNullOrEmpty(datarow["StkType"].ToString()) ? string.Empty : datarow["StkType"].ToString().Trim(),
                            SalesCat1 = string.IsNullOrEmpty(datarow["SalesCat1"].ToString()) ? string.Empty : datarow["SalesCat1"].ToString().Trim(),
                            SalesCat5 = string.IsNullOrEmpty(datarow["SalesCat5"].ToString()) ? string.Empty : datarow["SalesCat5"].ToString().Trim(),
                            SalesCat4 = string.IsNullOrEmpty(datarow["SalesCat4"].ToString()) ? string.Empty : datarow["SalesCat4"].ToString().Trim(),
                            PreviouslyReturnedQty = string.IsNullOrEmpty(datarow["PreviouslyReturnedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["PreviouslyReturnedQty"].ToString().Trim()),
                            UM = string.IsNullOrEmpty(datarow["OrderUM"].ToString()) ? string.Empty : datarow["OrderUM"].ToString().Trim(),
                            OrderQty = string.IsNullOrEmpty(datarow["OrderQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderQty"].ToString().Trim()),
                            RouteId = string.IsNullOrEmpty(datarow["RouteId"].ToString()) ? 0 : Convert.ToInt32(datarow["RouteId"].ToString().Trim()),
                            UnitPrice = string.IsNullOrEmpty(datarow["UnitPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["UnitPriceAmt"].ToString().Trim()),
                            ExtendedPrice = string.IsNullOrEmpty(datarow["ExtnPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ExtnPriceAmt"].ToString().Trim()),
                            TaxAmount = string.IsNullOrEmpty(datarow["ItemSalesTaxAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ItemSalesTaxAmt"].ToString().Trim()),
                            IsTaxable = datarow["IsTaxable"].ToString() == "0" ? false : true,
                            UMPrice = string.IsNullOrEmpty(datarow["PricingUM"].ToString()) ? string.Empty : datarow["PricingUM"].ToString().Trim(),
                            TransactionUOM = string.IsNullOrEmpty(datarow["DefaultUM"].ToString()) ? string.Empty : datarow["DefaultUM"].ToString().Trim(), /*Default UoM is Transaction UoM*/
                            OrderDateF = string.IsNullOrEmpty(datarow["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(datarow["OrderDate"].ToString().Trim()),
                            OrderID = string.IsNullOrEmpty(datarow["OrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderID"].ToString().Trim()),
                            QtyOnHand = string.IsNullOrEmpty(datarow["OnHandQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OnHandQty"].ToString().Trim()),
                            HeldQty = string.IsNullOrEmpty(datarow["HeldQty"].ToString()) ? 0 : Convert.ToInt32(datarow["HeldQty"].ToString().Trim()),
                            CommittedQty = string.IsNullOrEmpty(datarow["CommittedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["CommittedQty"].ToString().Trim()),
                            AvailableQty = string.IsNullOrEmpty(datarow["AvailableQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableQty"].ToString().Trim()),
                            QtyAvailableToReturn = string.IsNullOrEmpty(datarow["AvailableReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableReturnQty"].ToString().Trim()),
                            PrimaryUM = string.IsNullOrEmpty(datarow["PrimaryUM"].ToString()) ? string.Empty : datarow["PrimaryUM"].ToString().Trim(),
                            EnergySurchargeAmt = string.IsNullOrEmpty(datarow["EnergySurchargeAmt"].ToString()) ? 0 : Convert.ToDouble(datarow["EnergySurchargeAmt"].ToString().Trim()),
                            SettlementID = string.IsNullOrEmpty(datarow["SettlementID"].ToString()) ? 0 : Convert.ToInt32(datarow["SettlementID"].ToString().Trim()),
                            TransactionDetailID = string.IsNullOrEmpty(datarow["TransactionDetailID"].ToString()) ? 0 : Convert.ToInt32(datarow["TransactionDetailID"].ToString().Trim())
                        };
                        returnItem.OrderDate = returnItem.OrderDateF.ToString("MM'/'dd'/'yyyy");
                        returnItems.Add(returnItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetItemsForOrdersTab][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetItemsForOrdersTab]");
            return returnItems;
        }
        public TrulyObservableCollection<ReturnItem> GetItemsForReturnsTab()
        {
            TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetItemsForReturnsTab]");
            try
            {
                var query = @"
                        SELECT DISTINCT 
                        S1.OrderID,S1.EnergySurchargeAmt, S1.OrderDetailID AS 'TransactionDetailID'
                        ,TL.TDSTID as SettlementID
                        ,isnull(S2.OrderStateId,0) as OrderStateId
                        ,case isnull(S2.OrderStateId,0) when  0  then 'BL' when {1} then 'OrderDelivered' when {2} then 'OrderReturned' when {3} then 'HOLD' end as OrderState
                        ,S2.OrderID as ReturnOrderID
                        ,S2.ItemId
                        ,im.IMLITM as ItemNumber
                        ,im.IMDSC1 as ItemDescription
                        ,im.IMLNTY as StkType
                        ,im.IMSRP1  as SalesCat1
                        ,im.IMSRP5 as SalesCat5
                        ,im.IMSRP4 as SalesCat4
                        ,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty
                        ,S2.OrderUM as OrderUM
                        ,S1.OrderQty
                        ,S2.OrderQty as ReturnQty
                        ,S2.RouteId
                        ,S2.UnitPriceAmt
                        ,S2.ExtnPriceAmt
                        ,S2.ItemSalesTaxAmt
                        ,S2.IsTaxable
                        ,S1.ORDERQTY- ISNULL(S2.ORDERQTY,0) AS AvailableReturnQty
                        ,inv.OnHandQuantity as OnHandQty
                        ,inv.CommittedQuantity as CommittedQty
                        ,inv.HeldQuantity as HeldQty
                        ,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty 
                        ,iconfig.PrimaryUM as PrimaryUM
                        ,iconfig.PricingUM
                        ,iconfig.TransactionUM as DefaultUM
                        ,iconfig.OtherUM1 as OtherUM1
                        ,iconfig.OtherUM2
                        ,S2.OrderDate
                        FROM
                        (
                        SELECT  OH.*, OD.OrderUM, OD.ORDERDETAILID,OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                        WHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + " and OH.OrderStateId in (" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + ", " + ActivityKey.OrderReturned.GetStatusIdFromDB() + ")" +
                       @"  ) S1
                        right OUTER JOIN
                        (
                        SELECT OD.*,SRM.SALESoRDERID AS RORDERDETAILID,OH.OrderDate, OH.OrderStateId  FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                        LEFT OUTER JOIN busdta.SalesOrder_ReturnOrder_Mapping SRM ON  OD.ORDERDETAILID=SRM.ORDERDETAILID
                        WHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND ISNULL(RORDERDETAILID,0) <> 0" +
                        @") S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID
                         join busdta.F4101 im on S1.ItemId = im.IMITM 
                         join busdta.F4102 ib on ib.IBLITM = im.IMLITM 
                         join busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID
                         join busdta.Inventory inv on inv.ItemId = S1.ItemId
                        LEFT OUTER JOIN BUSDTA.M50012 TL ON TL.TDREFHDRID=S2.OrderID
                        where S1.CustomerID='{0}'  ORDER BY ReturnOrderID DESC
                        ";
                query = string.Format(query, PayloadManager.ReturnOrderPayload.Customer.CustomerNo, ActivityKey.OrderDelivered.GetStatusIdFromDB(), ActivityKey.OrderReturned.GetStatusIdFromDB(), ActivityKey.HoldAtROEntry.GetStatusIdFromDB());
                var dbResult = DbEngine.ExecuteDataSet(query);
                if (dbResult.HasData())
                {
                    foreach (DataRow datarow in dbResult.Tables[0].Rows)
                    {
                        ReturnItem returnItem = new ReturnItem
                        {
                            ItemId = string.IsNullOrEmpty(datarow["ItemId"].ToString()) ? string.Empty : datarow["ItemId"].ToString().Trim(),
                            ItemNumber = string.IsNullOrEmpty(datarow["ItemNumber"].ToString()) ? string.Empty : datarow["ItemNumber"].ToString().Trim(),
                            ItemDescription = string.IsNullOrEmpty(datarow["ItemDescription"].ToString()) ? string.Empty : datarow["ItemDescription"].ToString().Trim(),
                            StkType = string.IsNullOrEmpty(datarow["StkType"].ToString()) ? string.Empty : datarow["StkType"].ToString().Trim(),
                            SalesCat1 = string.IsNullOrEmpty(datarow["SalesCat1"].ToString()) ? string.Empty : datarow["SalesCat1"].ToString().Trim(),
                            SalesCat5 = string.IsNullOrEmpty(datarow["SalesCat5"].ToString()) ? string.Empty : datarow["SalesCat5"].ToString().Trim(),
                            SalesCat4 = string.IsNullOrEmpty(datarow["SalesCat4"].ToString()) ? string.Empty : datarow["SalesCat4"].ToString().Trim(),
                            PreviouslyReturnedQty = string.IsNullOrEmpty(datarow["PreviouslyReturnedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["PreviouslyReturnedQty"].ToString().Trim()),
                            UM = string.IsNullOrEmpty(datarow["OrderUM"].ToString()) ? string.Empty : datarow["OrderUM"].ToString().Trim(),
                            OrderQty = string.IsNullOrEmpty(datarow["OrderQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderQty"].ToString().Trim()),
                            ReturnQty = string.IsNullOrEmpty(datarow["ReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["ReturnQty"].ToString().Trim()),
                            RouteId = string.IsNullOrEmpty(datarow["RouteId"].ToString()) ? 0 : Convert.ToInt32(datarow["RouteId"].ToString().Trim()),
                            UnitPrice = string.IsNullOrEmpty(datarow["UnitPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["UnitPriceAmt"].ToString().Trim()),
                            ExtendedPrice = string.IsNullOrEmpty(datarow["ExtnPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ExtnPriceAmt"].ToString().Trim()),
                            TaxAmount = string.IsNullOrEmpty(datarow["ItemSalesTaxAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ItemSalesTaxAmt"].ToString().Trim()),
                            IsTaxable = datarow["IsTaxable"].ToString() == "0" ? false : true,
                            UMPrice = string.IsNullOrEmpty(datarow["PricingUM"].ToString()) ? string.Empty : datarow["PricingUM"].ToString().Trim(),
                            TransactionUOM = string.IsNullOrEmpty(datarow["DefaultUM"].ToString()) ? string.Empty : datarow["DefaultUM"].ToString().Trim(),
                            OrderDateF = string.IsNullOrEmpty(datarow["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(datarow["OrderDate"].ToString().Trim()),
                            OrderID = string.IsNullOrEmpty(datarow["OrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderID"].ToString().Trim()),
                            QtyOnHand = string.IsNullOrEmpty(datarow["OnHandQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OnHandQty"].ToString().Trim()),
                            HeldQty = string.IsNullOrEmpty(datarow["HeldQty"].ToString()) ? 0 : Convert.ToInt32(datarow["HeldQty"].ToString().Trim()),
                            CommittedQty = string.IsNullOrEmpty(datarow["CommittedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["CommittedQty"].ToString().Trim()),
                            AvailableQty = string.IsNullOrEmpty(datarow["AvailableQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableQty"].ToString().Trim()),
                            PrimaryUM = string.IsNullOrEmpty(datarow["PrimaryUM"].ToString()) ? string.Empty : datarow["PrimaryUM"].ToString().Trim(),
                            QtyAvailableToReturn = string.IsNullOrEmpty(datarow["AvailableReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableReturnQty"].ToString().Trim()),
                            ReturnOrderID = string.IsNullOrEmpty(datarow["ReturnOrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["ReturnOrderID"].ToString().Trim()),
                            OrderState = string.IsNullOrEmpty(datarow["OrderState"].ToString()) ? "" : datarow["OrderState"].ToString().Trim(),
                            OrderStateID = string.IsNullOrEmpty(datarow["OrderStateID"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderStateID"].ToString().Trim()),
                            SettlementID = string.IsNullOrEmpty(datarow["SettlementID"].ToString()) ? 0 : Convert.ToInt32(datarow["SettlementID"].ToString().Trim()),
                            EnergySurchargeAmt = string.IsNullOrEmpty(datarow["EnergySurchargeAmt"].ToString()) ? 0 : Convert.ToDouble(datarow["EnergySurchargeAmt"].ToString().Trim()),
                            TransactionDetailID = string.IsNullOrEmpty(datarow["TransactionDetailID"].ToString()) ? 0 : Convert.ToInt32(datarow["TransactionDetailID"].ToString().Trim())

                        };

                        returnItem.OrderDate = returnItem.OrderDateF.ToString("MM'/'dd'/'yyyy");
                        returnItems.Add(returnItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetItemsForReturnsTab][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetItemsForReturnsTab]");
            return returnItems;
        }


        public ReturnOrder GetOrderDetails(int OrderId)
        {
            var ro_detail = new ReturnOrder();
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:GetOrderDetails]");
            try
            {
                var oh_query = @"select oh.*, (SELECT if(isnull(LTRIM(RTRIM(M.TDSTTLID)),0) > 0) then 1 else 0 AS STTID FROM BUSDTA.M50012 M WHERE 
                                 TDSTAT='ORDERRETURNED' AND TDREFHDRID = " + OrderId + " ) as IsSettled " +
                                 " from busdta.Order_Header oh where orderid = " + OrderId + "";
                var oh_Dbset = DbEngine.ExecuteDataSet(oh_query);

                if (oh_Dbset.HasData())
                {
                    foreach (DataRow order in oh_Dbset.Tables[0].Rows)
                    {
                        ReturnOrder ro_item = new ReturnOrder();
                        ro_item.Surcharge = string.IsNullOrEmpty(order["EnergySurchargeAmt"].ToString()) ? 0 : Convert.ToDecimal(order["EnergySurchargeAmt"].ToString().Trim());
                        ro_item.OrderId = OrderId;
                        ro_item.IsSettled = !string.IsNullOrEmpty(order["IsSettled"].ToString()) && Convert.ToBoolean(order["IsSettled"]);
                        ro_item.OrderDate = string.IsNullOrEmpty(order["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(order["OrderDate"].ToString().Trim());

                        ro_detail = ro_item;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][GetOrderDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                ro_detail = null;
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:GetOrderDetails]");
            return ro_detail;
        }

        public TrulyObservableCollection<ReturnItem> GetItemsForPick()
        {
            TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetItemsForPick]");
            try
            {
                var query = @"SELECT OD.OrderID ,OD.OrderDetailId, OD.OrderQty, OD.OrderUM,OH.HoldCommitted ,
                                inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId, inventory.OnHandQuantity AS 'OnHandQty'
                                ,inventory.CommittedQuantity AS 'CommittedQty'
                                , inventory.HeldQuantity AS 'HeldQty',inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                                ,(isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                                ,OD.OrderDetailId AS 'TransactionDetailID'
                                , inventory.OnHandQuantity AS 'ActualQtyOnHand'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM'
                                , OD.OrderQty AS 'TransactionQty'
                                ,OD.OrderUM AS 'TransactionUOM'
                                FROM BUSDTA.Order_Detail OD 
                                JOIN BUSDTA.Order_Header OH  ON OD.OrderID=OH.OrderID AND OD.RouteID=OH.RouteID
                                JOIN BUSDTA.Inventory inventory ON inventory.ItemId=OD.ItemId AND inventory.RouteId=OD.RouteID AND inventory.RouteId=OH.RouteID
                                join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID 
                                join busdta.F4101 IM on LTRIM(RTRIM(inventory.ItemNumber)) = IM.IMLITM  
                                WHERE   OD.OrderID= " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "";

                DataSet dbResult = DbEngine.ExecuteDataSet(query);
                if (dbResult.HasData())
                {
                    returnItems = new TrulyObservableCollection<ReturnItem>(dbResult.GetEntityList<ReturnItem>());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetItemsForReturnsTab][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetItemsForReturnsTab]");
            return returnItems;
        }
        public bool CreateReturnOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID)
        {
            bool returnValue = true;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:SaveOrder]");
            try
            {

                string query = "select count(OrderID) from BUSDTA.ORDER_HEADER where OrderID='{0}'";
                query = string.Format(query, ReturnOrderID);
                bool orderExists = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(query)));
                if (orderExists)
                {
                    query = @"Update BUSDTA.ORDER_HEADER set 
                                    TotalCoffeeAmt='{0}'
                                    ,TotalAlliedAmt='{1}'
                                    ,OrderTotalAmt='{2}'
                                    ,SalesTaxAmt='{3}'
                                    ,InvoiceTotalAmt='{4}'
                                    ,EnergySurchargeAmt='{5}'
                                    ,HoldCommitted='0'
                                    ,UpdatedBy='{6}'
                                    ,UpdatedDatetime=getdate() 
                                    ,OrderStateId='{8}'
                                    where OrderID='{7}';";
                    query = string.Format(query, PayloadManager.ReturnOrderPayload.TotalCoffeeAmount, PayloadManager.ReturnOrderPayload.TotalAlliedAmount, PayloadManager.ReturnOrderPayload.OrderTotalAmount, PayloadManager.ReturnOrderPayload.ROSalesTax, PayloadManager.ReturnOrderPayload.InvoiceTotalAmt, PayloadManager.ReturnOrderPayload.EnergySurcharge, UserManager.UserId, ReturnOrderID, ActivityKey.CreateReturnOrder.GetStatusIdFromDB());
                    DbEngine.ExecuteNonQuery(query);

                    // Step 2 : Insert into Order Detail
                    foreach (var item in returnitems)
                    {
                        query = "select count(OrderID) from BUSDTA.Order_Detail where OrderID='{0}'and RouteID='{1}'and ItemId='{2}'";
                        query = string.Format(query, ReturnOrderID, CommonNavInfo.RouteID, item.ItemId);
                        bool orderItemExists = Convert.ToBoolean(Convert.ToInt32(DbEngine.ExecuteScalar(query)));

                        #region Check if Item exists , if yes then update else add a new mapping
                        if (orderItemExists)
                        {
                            var isTaxable = item.TaxAmount > 0 ? "1" : "0";
                            query = @"update BUSDTA.Order_Detail set
                                    OrderQty='{0}'
                                    ,OrderUM='{1}'
                                    ,UnitPriceAmt='{2}'
                                    ,ExtnPriceAmt='{3}'
                                    ,ItemSalesTaxAmt='{4}'
                                    ,IsTaxable='{5}'
                                    ,ReturnHeldQty='{6}'
                                    ,UpdatedBy='{7}'
                                    ,UpdatedDatetime=getdate() 
                                    where OrderID='{8}' and RouteID='{9}' and OrderDetailId='{10}' and ItemId='{11}'";
                            query = string.Format(query, item.ReturnQty, item.UM, item.UnitPrice, item.ExtendedPrice, item.TaxAmount, isTaxable, item.NonSellableQty, UserManager.UserId, ReturnOrderID, CommonNavInfo.RouteID, item.OrderDetailID, item.ItemId);
                            DbEngine.ExecuteNonQuery(query);
                        }
                        else
                        {
                            var isTaxable = item.TaxAmount > 0 ? "1" : "0";
                            query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,RouteID,ItemId,OrderQty,OrderUM,UnitPriceAmt,ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
                                         " VALUES( " + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + item.ItemId + "," + item.ReturnQty + ",'" + item.UM + "'," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
                                         " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
                            DbEngine.ExecuteNonQuery(query);
                            string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();
                            // Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
                            query = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},getdate())";
                            query = string.Format(query, item.OrderID, ReturnOrderID, item.ItemId, OrderDetailId, CommonNavInfo.RouteID, PayloadManager.ApplicationPayload.LoggedInUserID);
                            DbEngine.ExecuteNonQuery(query);
                        }
                        #endregion

                    }
                }
                else
                {
                    // Insert the new order in Order_Header, Order_Detailsand SalesOrder_ReturnOrder_Mapping
                    // Step 1 : Save Details in Order Header
                    var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,RouteID,OrderTypeId,CustomerId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
                                    " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSign,ChargeOnAccountSign,VoidReasonCodeId,ChargeOnAccount," +
                                    " HoldCommitted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)" +
                                    " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," +
                                              "getdate()," + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                                              "" + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," + PayloadManager.ReturnOrderPayload.ROSalesTax + "," + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "" +
                                              "," + PayloadManager.ReturnOrderPayload.EnergySurcharge + ",null,null,NULL,NULL,NULL,NULL,'0'," +
                                              "" + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate()) ";
                    DbEngine.ExecuteNonQuery(Oh_query);

                    // Step 2 : Insert into Order Detail
                    foreach (var item in returnitems)
                    {
                        var isTaxable = item.TaxAmount > 0 ? "1" : "0";
                        query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,RouteID,ItemId,OrderQty,OrderUM,UnitPriceAmt,ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
                                     " VALUES( " + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + item.ItemId + "," + item.ReturnQty + ",'" + item.UM + "'," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
                                     " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
                        DbEngine.ExecuteNonQuery(query);
                        string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();

                        item.OrderDetailID = string.IsNullOrEmpty(OrderDetailId) ? 0 : Convert.ToInt32(OrderDetailId);
                        // Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
                        string returnOrderMappingQuery = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},getdate())";
                        returnOrderMappingQuery = string.Format(returnOrderMappingQuery, item.OrderID, ReturnOrderID, item.ItemId, OrderDetailId, CommonNavInfo.RouteID, PayloadManager.ApplicationPayload.LoggedInUserID);
                        DbEngine.ExecuteNonQuery(returnOrderMappingQuery);
                    }
                }
                SalesLogicExpress.Domain.Activity a = LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.CreateReturnOrder);
                if (a != null && a.ActivityHeaderID != null && PayloadManager.ReturnOrderPayload.TransactionID == null)
                {
                    PayloadManager.ReturnOrderPayload.TransactionID = a.ActivityHeaderID;
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][SaveOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:SaveOrder]");
            return returnValue;
        }
        public bool VoidOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID, string VoidReasonCodeID, ActivityKey VoidAt)
        {
            bool returnValue = true;

            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][Start:VoidOrder]");
            try
            {
                if (VoidAt == ActivityKey.VoidAtROEntry || VoidAt == ActivityKey.VoidAtROPick)
                {
                    int affectedRecords = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + VoidAt.GetStatusIdFromDB() + "," +
                                                                            "TotalCoffeeAmt = " + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," +
                                                                            "TotalAlliedAmt = " + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                                                                            "OrderTotalAmt = " + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," +
                                                                            "SalesTaxAmt = " + PayloadManager.ReturnOrderPayload.ROSalesTax + "," +
                                                                            "InvoiceTotalAmt = " + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "," +
                                                                            "EnergySurchargeAmt = " + (PayloadManager.ReturnOrderPayload.IsReturnOrder == true ? PayloadManager.ReturnOrderPayload.EnergySurcharge : 0) + "," +
                                                                                                   "VoidReasonCodeId = '" + VoidReasonCodeID + "' " +
                                                                                                   " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "");
                    LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, VoidAt);


                    foreach (var item in returnitems)
                    {
                        var diff = item.ReturnQty - item.NonSellableQty;

                        var OD_query = @"update busdta.order_detail set " +
                            "OrderQty = " + item.ReturnQty + "," +
                            "OrderUM = '" + item.UM + "'," +
                            "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                            "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                            "ReturnHeldQty = " + item.NonSellableQty + ", " +
                            "ItemSalesTaxAmt = " + item.TaxAmount + " " +
                            "where OrderDetailId =" + item.OrderDetailID + " and OrderID=" + ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "";
                        DbEngine.ExecuteNonQuery(OD_query);
                    }
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][VoidOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][End:VoidOrder]");
            return returnValue;
        }
        public bool HoldOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID, ActivityKey HoldAt)
        {
            bool returnValue = true;
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][Start:HoldOrder]");
            try
            {
                if (HoldAt == ActivityKey.HoldAtROEntry || HoldAt == ActivityKey.HoldAtPick || HoldAt == ActivityKey.HoldAtROPrint || HoldAt == ActivityKey.HoldAtROPick)
                {
                    int affectedRecords = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set 
                                                                            OrderStateId = " + HoldAt.GetStatusIdFromDB() + ", " +
                                                                            "TotalCoffeeAmt = " + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," +
                                                                            "TotalAlliedAmt = " + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                                                                            "OrderTotalAmt = " + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," +
                                                                            "SalesTaxAmt = " + PayloadManager.ReturnOrderPayload.ROSalesTax + "," +
                                                                            "InvoiceTotalAmt = " + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + ", " +
                                                                            "EnergySurchargeAmt = " + (PayloadManager.ReturnOrderPayload.IsReturnOrder == true ? PayloadManager.ReturnOrderPayload.EnergySurcharge : 0) + " " +
                                                                                             " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "");
                    LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, HoldAt);
                }

                foreach (var item in returnitems)
                {
                    var diff = item.ReturnQty - item.NonSellableQty;

                    var OD_query = @"update busdta.order_detail set " +
                        "OrderQty = " + item.ReturnQty + "," +
                        "OrderUM = '" + item.UM + "'," +
                        "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                        "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                        "ReturnHeldQty = " + item.NonSellableQty + ", " +
                        "ItemSalesTaxAmt = " + item.TaxAmount + " " +
                        "where OrderDetailId =" + item.OrderDetailID + " and OrderID=" + ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "";
                    DbEngine.ExecuteNonQuery(OD_query);
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][HoldOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][End:HoldOrder]");
            return returnValue;
        }

        public bool SaveReturnOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID)
        {
            bool returnValue = true;

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:SaveReturnOrder]");
            try
            {
                var orderStateIdUpdate = PayloadManager.ReturnOrderPayload.IsReturnOrder ? "OrderStateId = " + ActivityKey.OrderReturned.GetStatusIdFromDB() : "OrderStateId = " + ActivityKey.ItemReturn.GetStatusIdFromDB();

                var OH_query = @"update busdta.ORDER_HEADER set " + orderStateIdUpdate + "," +
                                 "TotalCoffeeAmt = " + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," +
                                 "TotalAlliedAmt = " + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                                 "OrderTotalAmt = " + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," +
                                 "SalesTaxAmt = " + PayloadManager.ReturnOrderPayload.ROSalesTax + "," +
                                 "InvoiceTotalAmt = " + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "," +
                                 "EnergySurchargeAmt = " + PayloadManager.ReturnOrderPayload.EnergySurcharge + "" +
                                " where OrderID = " + ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "";
                int affectedRecords = DbEngine.ExecuteNonQuery(OH_query);
                LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.OrderReturned);

                foreach (var item in returnitems)
                {
                    var diff = item.ReturnQty - item.NonSellableQty;
                    //var invUpdateQuery = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity +" + item.ReturnQtyInPrimaryUoM + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;

                    //DbEngine.ExecuteNonQuery(invUpdateQuery);

                    //var invUpdateQueryHeld = @"update busdta.Inventory set HeldQuantity = HeldQuantity +" + item.NonSellableQtyInPrimaryUoM + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
                    //DbEngine.ExecuteNonQuery(invUpdateQueryHeld);

                    var OD_query = @"update busdta.order_detail set " +
                        "OrderQty = " + item.ReturnQty + "," +
                        "OrderUM = '" + item.UM + "'," +
                        "ExtnPriceAmt = " + item.ROExtendedPrice + "," +
                        "IsTaxable = " + (item.ROTaxAmount > 0 ? '1' : '0') + "," +
                        "ReturnHeldQty = " + item.NonSellableQty + ", " +
                        "ItemSalesTaxAmt = " + item.TaxAmount + " " +
                        "where OrderDetailId =" + item.OrderDetailID + " and OrderID=" + ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "";
                    DbEngine.ExecuteNonQuery(OD_query);
                }
                if (PayloadManager.ReturnOrderPayload.IsReturnOrder)
                {
                    var chngQuery = @"update busdta.ORDER_HEADER set OrderStateId = " + ActivityKey.OrderReturned.GetStatusIdFromDB() + "" +
                               " where OrderID = " + PayloadManager.ReturnOrderPayload.SalesOrderID + " and RouteID = " + CommonNavInfo.RouteID + "";
                    int result = DbEngine.ExecuteNonQuery(chngQuery);
                }
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][SaveReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:SaveReturnOrder]");
            return returnValue;
        }

        public bool SaveOrHoldReturnOrder(ObservableCollection<ReturnItem> returnitems, string ReturnOrderID, ActivityKey activity)
        {
            bool status = true;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:SaveReturnOrder]");
            try
            {
                int orderStateID = 0;
                switch (activity)
                {
                    case ActivityKey.OrderReturned:
                        orderStateID = ActivityKey.OrderReturned.GetStatusIdFromDB();
                        LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.OrderReturned);
                        break;
                    case ActivityKey.HoldAtROEntry:
                        orderStateID = ActivityKey.HoldAtROEntry.GetStatusIdFromDB();
                        LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.HoldAtROEntry);

                        break;
                    case ActivityKey.HoldAtROPrint:
                        orderStateID = ActivityKey.HoldAtROPrint.GetStatusIdFromDB();
                        LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.HoldAtROPrint);

                        break;
                    case ActivityKey.HoldAtPick:
                        orderStateID = ActivityKey.HoldAtPick.GetStatusIdFromDB();
                        LogActivity(returnitems.Clone(), PayloadManager.ReturnOrderPayload.Customer, PayloadManager.ReturnOrderPayload.Customer.StopDate.Value, PayloadManager.ReturnOrderPayload.Customer.StopID, ActivityKey.HoldAtPick);

                        break;
                    default:
                        orderStateID = -1;
                        break;
                }
                if (string.IsNullOrEmpty(ReturnOrderID))
                {
                    throw new Exception("ReturnOrderID cannot be null or empty");
                }
                // POSSIBLE CASES
                // 1. Return Complete 
                // 2. Hold at
                //      Order Entry
                //      Pick
                //      Print
                // 3. Void
                var orderState = DbEngine.ExecuteScalar(@"select OrderStateId from BUSDTA.ORDER_HEADER where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "").Trim();
                if (orderStateID == ActivityKey.HoldAtROEntry.GetStatusIdFromDB() ||
                    orderStateID == ActivityKey.HoldAtPick.GetStatusIdFromDB() ||
                    orderStateID == ActivityKey.HoldAtROPrint.GetStatusIdFromDB())
                {
                    var updateOh_state = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + activity.GetStatusIdFromDB() + " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "");
                }
                else
                {

                    // TODO : Order signature is remaining
                    // Step 1 : Save Details in Order Header
                    //var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,RouteID,OrderTypeId,CustomerId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
                    //                " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSign,ChargeOnAccountSign,VoidReasonCodeId,ChargeOnAccount," +
                    //                " HoldCommitted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)" +
                    //                " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," +
                    //                          "getdate()," + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                    //                          "" + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," + PayloadManager.ReturnOrderPayload.ROSalesTax + "," + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "" +
                    //                          "," + PayloadManager.ReturnOrderPayload.EnergySurcharge + ",null," + orderStateID + ",NULL,NULL,NULL,NULL,'0'," +
                    //                          "" + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate()) ";

                    //DbEngine.ExecuteNonQuery(Oh_query);

                    foreach (var item in returnitems)
                    {
                        //// Step 2 : Insert into Order Detail
                        //var isTaxable = item.TaxAmount > 0 ? "1" : "0";
                        //var query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,RouteID,ItemId,OrderQty,OrderUM,UnitPriceAmt,ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
                        //             " VALUES( " + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + item.ItemId + "," + item.ReturnQty + ",'" + item.UM + "'," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
                        //             " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
                        //DbEngine.ExecuteNonQuery(query);
                        //string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();
                        //// Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
                        //string returnOrderMappingQuery = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},getdate())";
                        //returnOrderMappingQuery = string.Format(returnOrderMappingQuery, item.OrderID, ReturnOrderID, item.ItemId, OrderDetailId, CommonNavInfo.RouteID, PayloadManager.ApplicationPayload.LoggedInUserID);
                        //DbEngine.ExecuteNonQuery(returnOrderMappingQuery);

                        // Step 4 : Update the Inventory
                        var invUpdateQuery = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity +" + item.ReturnQty + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
                        DbEngine.ExecuteNonQuery(invUpdateQuery);

                        var invUpdateQueryHeld = @"update busdta.Inventory set HeldQuantity = HeldQuantity +" + item.NonSellableQty + " where ItemId = " + item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
                        DbEngine.ExecuteNonQuery(invUpdateQueryHeld);

                    }
                }

            }
            catch (Exception ex)
            {
                status = false;
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][SaveReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:SaveReturnOrder]");
            return status;
        }

        public bool HoldReturnOrder()
        {
            bool status = true;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:HoldReturnOrder]");
            try
            {

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][HoldReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:HoldReturnOrder]");
            return status;
        }

        public bool VoidReturnOrder(ActivityKey activity, int ReturnOrderID, ObservableCollection<ReturnItem> returnitems)
        {
            bool status = true;
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][Start:VoidReturnOrder]");
            try
            {
                var oh_count = DbEngine.ExecuteScalar("select count(1) from busdta.order_header where OrderID =" + ReturnOrderID + "");

                if (!string.IsNullOrEmpty(oh_count) && Convert.ToInt32(oh_count) > 0)
                {
                    var updateOh_state = DbEngine.ExecuteNonQuery(@"update busdta.ORDER_HEADER set OrderStateId = " + activity.GetStatusIdFromDB() + " where OrderID = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " and RouteID = " + CommonNavInfo.RouteID + "");

                    foreach (var r_item in returnitems)
                    {
                        var r_query = @"update busdta.order_detail set OrderQty = 0 , ReturnHeldQty = 0 where OrderDetailID = '{0}' and OrderID = '{1}' 
                                        and RouteID = '{2}'";
                        var query = string.Format(r_query, r_item.OrderDetailID, ReturnOrderID, CommonNavInfo.RouteID);
                        DbEngine.ExecuteDataSet(query);
                    }
                }
                else
                {

                    var Oh_query = @" INSERT INTO BUSDTA.ORDER_HEADER (OrderID,RouteID,OrderTypeId,CustomerId,OrderDate,TotalCoffeeAmt,TotalAlliedAmt," +
                                       " OrderTotalAmt,SalesTaxAmt,InvoiceTotalAmt,EnergySurchargeAmt,SurchargeReasonCodeId,OrderStateId,OrderSign,ChargeOnAccountSign,VoidReasonCodeId,ChargeOnAccount," +
                                       " HoldCommitted,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)" +
                                       " VALUES(" + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + StatusTypesEnum.RETORD.GetStatusIdFromDB() + "," + PayloadManager.ReturnOrderPayload.Customer.CustomerNo + "," +
                                                 "getdate()," + PayloadManager.ReturnOrderPayload.TotalCoffeeAmount + "," + PayloadManager.ReturnOrderPayload.TotalAlliedAmount + "," +
                                                 "" + PayloadManager.ReturnOrderPayload.OrderTotalAmount + "," + PayloadManager.ReturnOrderPayload.ROSalesTax + "," + PayloadManager.ReturnOrderPayload.InvoiceTotalAmt + "" +
                                                 "," + PayloadManager.ReturnOrderPayload.EnergySurcharge + ",null," + activity.GetStatusIdFromDB() + ",NULL,NULL,NULL,NULL,'0'," +
                                                 "" + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate()) ";

                    DbEngine.ExecuteNonQuery(Oh_query);

                    foreach (var item in returnitems)
                    {
                        // Step 2 : Insert into Order Detail
                        var isTaxable = item.TaxAmount > 0 ? "1" : "0";
                        var query = @"INSERT INTO BUSDTA.Order_Detail (OrderID,RouteID,ItemId,OrderQty,OrderUM,UnitPriceAmt,ExtnPriceAmt,ItemSalesTaxAmt,PriceOverrideReasonCodeId,IsTaxable,ReturnHeldQty,CreatedDatetime,UpdatedBy,UpdatedDatetime) " +
                                     " VALUES( " + ReturnOrderID + "," + CommonNavInfo.RouteID + "," + item.ItemId + "," + item.ReturnQty + ",'" + item.UM + "'," + item.UnitPrice + "," + item.ExtendedPrice + "," + item.TaxAmount + "," +
                                     " NULL," + isTaxable + "," + item.NonSellableQty + ",getdate()," + UserManager.UserId + ",getdate())";
                        DbEngine.ExecuteNonQuery(query);
                        string OrderDetailId = DbEngine.ExecuteScalar("select @@identity").ToString();
                        // Step 3 : Insert into SalesOrder_ReturnOrder Mapping table
                        string returnOrderMappingQuery = "insert into busdta.SalesOrder_ReturnOrder_Mapping (SalesOrderId,ReturnOrderID,ItemID,OrderDetailID,RouteID,CreatedBy,CreatedDatetime) values({0},{1},{2},{3},{4},{5},getdate())";
                        returnOrderMappingQuery = string.Format(returnOrderMappingQuery, item.OrderID, ReturnOrderID, item.ItemId, OrderDetailId, CommonNavInfo.RouteID, PayloadManager.ApplicationPayload.LoggedInUserID);
                        DbEngine.ExecuteNonQuery(returnOrderMappingQuery);


                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][VoidReturnOrder][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Manager][CustomerReturnsManager][End:VoidReturnOrder]");
            return status;
        }

        public int GetRONumber()
        {
            int OrderNum = 0;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetRONumber]");
            try
            {
                //TODO : Implement number pool for return order
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.ReturnOrder);
                OrderNum = Convert.ToInt32(val);


                int updateStopActivityFlag = DbEngine.ExecuteNonQuery("update busdta.m56m0004 set rpactid=null where rpstid = " + CommonNavInfo.Customer.StopID + " and rpan8=" + CommonNavInfo.Customer.CustomerNo + "");
                return OrderNum;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetRONumber][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                OrderNum = -1;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetRONumber]");
            return OrderNum;
        }

        public SalesLogicExpress.Domain.Activity LogActivity(ObservableCollection<ReturnItem> returnItems, Customer customer, DateTime stopDate, string stopID, ActivityKey activity)
        {
            PayloadManager.ReturnOrderPayload.StopDate = customer.StopDate.Value;
            PayloadManager.ReturnOrderPayload.Customer = customer;
            TrulyObservableCollection<ReturnItem> oc = new TrulyObservableCollection<ReturnItem>(returnItems.ToList());
            PayloadManager.ReturnOrderPayload.ReturnItems = oc;
            Activity ac = new Activity
            {
                ActivityFlowID = PayloadManager.ReturnOrderPayload.ReturnOrderID,
                ActivityHeaderID = PayloadManager.ReturnOrderPayload.TransactionID,
                CustomerID = PayloadManager.ReturnOrderPayload.Customer.CustomerNo,
                RouteID = PayloadManager.ApplicationPayload.Route,
                ActivityType = activity.ToString(),
                ActivityStart = DateTime.Now,
                ActivityEnd = DateTime.Now,
                StopInstanceID = stopID,
                IsTxActivity = true,
                ActivityDetailClass = PayloadManager.ReturnOrderPayload.ToString(),
                ActivityDetails = PayloadManager.ReturnOrderPayload.SerializeToJson()
            };
            return ResourceManager.Transaction.LogActivity(ac);
        }

        public TrulyObservableCollection<ReturnItem> GetOrdersForOrderReturn(string OrderReturnId)
        {
            TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
            DataTable dbResult = new DataTable();
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetOrdersForOrderReturn]");
            try
            {
                var query = "";
                query = @"
                        SELECT DISTINCT 
                        S1.OrderID
                        ,S2.OrderID as ReturnOrderID
                        ,S2.ItemId
                        ,im.IMLITM as ItemNumber
                        ,im.IMDSC1 as ItemDescription
                        ,im.IMLNTY as StkType
                        ,im.IMSRP1  as SalesCat1
                        ,im.IMSRP5 as SalesCat5
                        ,im.IMSRP4 as SalesCat4
                        ,ISNULL(S2.ORDERQTY,0) AS PreviouslyReturnedQty
                        ,S2.OrderUM as OrderUM
                        ,S1.OrderQty
                        ,S2.RouteId
                        ,S2.UnitPriceAmt
                        ,S2.ExtnPriceAmt
                        ,S2.ItemSalesTaxAmt
                        ,S2.IsTaxable
                        ,S1.ORDERQTY- ISNULL(S2.ORDERQTY,0) AS AvailableReturnQty
                        ,inv.OnHandQuantity as OnHandQty
                        ,inv.CommittedQuantity as CommittedQty
                        ,inv.HeldQuantity as HeldQty
                        ,(inv.OnHandQuantity - (inv.CommittedQuantity + inv.HeldQuantity)) as AvailableQty 
                        ,iconfig.PrimaryUM as PrimaryUM
                        ,iconfig.PricingUM
                        ,iconfig.TransactionUM as DefaultUM
                        ,iconfig.OtherUM1 as OtherUM1
                        ,iconfig.OtherUM2
                        ,S2.OrderDate
                        FROM
                        (
                        SELECT  OH.*, OD.OrderUM, OD.ORDERDETAILID,OD.ITEMID,OD.ORDERQTY,OD.RETURNHELDQTY,OD.UnitPriceAmt,OD.ExtnPriceAmt,OD.ItemSalesTaxAmt, isnull(od.IsTaxable,0) as IsTaxable FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                        WHERE OH.OrderTypeId=" + StatusTypesEnum.SALORD.GetStatusIdFromDB() + "" +
                        @") S1
                        right OUTER JOIN
                        (
                        SELECT OD.*,SRM.SALESoRDERID AS RORDERDETAILID,OH.OrderDate FROM BUSDTA.ORDER_HEADER OH LEFT OUTER JOIN busdta.ORDER_Detail OD ON OH.ORDERID=OD.ORDERID
                        LEFT OUTER JOIN busdta.SalesOrder_ReturnOrder_Mapping SRM ON  OD.ORDERDETAILID=SRM.ORDERDETAILID
                        WHERE OH.OrderTypeId=" + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " AND ISNULL(RORDERDETAILID,0) <> 0" +
                       @" ) S2 ON S1.ORDERID= S2.RORDERDETAILID AND S1.ITEMID=S2.ITEMID
                         join busdta.F4101 im on S1.ItemId = im.IMITM 
                         join busdta.F4102 ib on ib.IBLITM = im.IMLITM 
                         join busdta.ItemConfiguration iconfig on S1.ItemId = iconfig.ItemID
                         join busdta.Inventory inv on inv.ItemId = S1.ItemId
                        where S1.CustomerID='{0}' and ReturnOrderID={1}
                        ";
                query = string.Format(query, PayloadManager.ReturnOrderPayload.Customer.CustomerNo, OrderReturnId);
                dbResult = DbEngine.ExecuteDataSet(query).Tables[0];
                if (dbResult.Rows.Count > 0)
                {
                    foreach (DataRow datarow in dbResult.Rows)
                    {
                        ReturnItem returnItem = new ReturnItem
                        {
                            ItemId = string.IsNullOrEmpty(datarow["ItemId"].ToString()) ? string.Empty : datarow["ItemId"].ToString().Trim(),
                            ItemNumber = string.IsNullOrEmpty(datarow["ItemNumber"].ToString()) ? string.Empty : datarow["ItemNumber"].ToString().Trim(),
                            ItemDescription = string.IsNullOrEmpty(datarow["ItemDescription"].ToString()) ? string.Empty : datarow["ItemDescription"].ToString().Trim(),
                            StkType = string.IsNullOrEmpty(datarow["StkType"].ToString()) ? string.Empty : datarow["StkType"].ToString().Trim(),
                            SalesCat1 = string.IsNullOrEmpty(datarow["SalesCat1"].ToString()) ? string.Empty : datarow["SalesCat1"].ToString().Trim(),
                            SalesCat5 = string.IsNullOrEmpty(datarow["SalesCat5"].ToString()) ? string.Empty : datarow["SalesCat5"].ToString().Trim(),
                            SalesCat4 = string.IsNullOrEmpty(datarow["SalesCat4"].ToString()) ? string.Empty : datarow["SalesCat4"].ToString().Trim(),
                            PreviouslyReturnedQty = string.IsNullOrEmpty(datarow["PreviouslyReturnedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["PreviouslyReturnedQty"].ToString().Trim()),
                            UM = string.IsNullOrEmpty(datarow["OrderUM"].ToString()) ? string.Empty : datarow["OrderUM"].ToString().Trim(),
                            OrderQty = string.IsNullOrEmpty(datarow["OrderQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderQty"].ToString().Trim()),
                            RouteId = string.IsNullOrEmpty(datarow["RouteId"].ToString()) ? 0 : Convert.ToInt32(datarow["RouteId"].ToString().Trim()),
                            UnitPrice = string.IsNullOrEmpty(datarow["UnitPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["UnitPriceAmt"].ToString().Trim()),
                            ExtendedPrice = string.IsNullOrEmpty(datarow["ExtnPriceAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ExtnPriceAmt"].ToString().Trim()),
                            TaxAmount = string.IsNullOrEmpty(datarow["ItemSalesTaxAmt"].ToString()) ? 0 : Convert.ToDecimal(datarow["ItemSalesTaxAmt"].ToString().Trim()),
                            IsTaxable = datarow["IsTaxable"].ToString() == "0" ? false : true,
                            UMPrice = string.IsNullOrEmpty(datarow["PricingUM"].ToString()) ? string.Empty : datarow["PricingUM"].ToString().Trim(),
                            TransactionUOM = string.IsNullOrEmpty(datarow["DefaultUM"].ToString()) ? string.Empty : datarow["DefaultUM"].ToString().Trim(),
                            OrderDateF = string.IsNullOrEmpty(datarow["OrderDate"].ToString()) ? new DateTime() : Convert.ToDateTime(datarow["OrderDate"].ToString().Trim()),
                            OrderID = string.IsNullOrEmpty(datarow["OrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["OrderID"].ToString().Trim()),
                            QtyOnHand = string.IsNullOrEmpty(datarow["OnHandQty"].ToString()) ? 0 : Convert.ToInt32(datarow["OnHandQty"].ToString().Trim()),
                            HeldQty = string.IsNullOrEmpty(datarow["HeldQty"].ToString()) ? 0 : Convert.ToInt32(datarow["HeldQty"].ToString().Trim()),
                            CommittedQty = string.IsNullOrEmpty(datarow["CommittedQty"].ToString()) ? 0 : Convert.ToInt32(datarow["CommittedQty"].ToString().Trim()),
                            AvailableQty = string.IsNullOrEmpty(datarow["AvailableQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableQty"].ToString().Trim()),
                            PrimaryUM = string.IsNullOrEmpty(datarow["PrimaryUM"].ToString()) ? string.Empty : datarow["PrimaryUM"].ToString().Trim(),
                            QtyAvailableToReturn = string.IsNullOrEmpty(datarow["AvailableReturnQty"].ToString()) ? 0 : Convert.ToInt32(datarow["AvailableReturnQty"].ToString().Trim()),
                            ReturnOrderID = string.IsNullOrEmpty(datarow["ReturnOrderID"].ToString()) ? 0 : Convert.ToInt32(datarow["ReturnOrderID"].ToString().Trim())
                        };

                        returnItem.OrderDate = returnItem.OrderDateF.ToString("MM'/'dd'/'yyyy");
                        returnItems.Add(returnItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetOrdersForOrderReturn][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetOrdersForOrderReturn]");
            return returnItems;
        }

        public static void UpdateOrderStatus(string orderNo, ActivityKey state, bool UpdateTable = true)
        {
            try
            {
                if (UpdateTable)
                {
                    string query = "update busdta.ORDER_HEADER set OrderStateId={0}, UpdatedDatetime=now() where OrderID='{1}' and isnull(OrderStateId,'')!='" + ActivityKey.OrderReturned.GetStatusIdFromDB() + "'";
                    query = string.Format(query, state.GetStatusIdFromDB(), orderNo);
                    DbEngine.ExecuteNonQuery(query);
                    //SalesLogicExpress.Application.ViewModels.Order.OrderStatus = state;
                }

            }
            catch (Exception ex)
            {
            }
        }

        public int CheckROExists()
        {
            int result = 0;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:CheckROExists]");
                var q_result = DbEngine.ExecuteScalar(@"select count(1) from busdta.Order_header where ordertypeid = " + StatusTypesEnum.RETORD.GetStatusIdFromDB() + " and orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");
                result = string.IsNullOrEmpty(q_result.ToString().Trim()) ? -1 : Convert.ToInt32(q_result.Trim());
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:CheckROExists]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][CheckROExists][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public int CheckROItems()
        {
            int result = 0;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:CheckROItems]");
                var q_result = DbEngine.ExecuteScalar(@"select count(1) from busdta.Order_detail where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");
                result = string.IsNullOrEmpty(q_result.ToString().Trim()) ? -1 : Convert.ToInt32(q_result.Trim());
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:CheckROItems]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][CheckROItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public bool CleanROItems()
        {
            bool result = false;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:CleanROItems]");
                var q_result = DbEngine.ExecuteNonQuery(@"delete from busdta.Order_detail where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");
                result = q_result > 0;

                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:CleanROItems]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][CleanROItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public List<ReturnItem> GetROItems()
        {
            List<ReturnItem> result = new List<ReturnItem>();
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:GetROItems]");
                var q_result = DbEngine.ExecuteDataSet(@"select * from busdta.Order_detail where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + "");


                if (q_result.HasData())
                {
                    foreach (DataRow od_row in q_result.Tables[0].Rows)
                    {
                        var r_item = new ReturnItem();
                        r_item.ItemId = string.IsNullOrEmpty(od_row["ItemId"].ToString()) ? string.Empty : od_row["ItemId"].ToString().Trim();
                        r_item.ReturnQty = string.IsNullOrEmpty(od_row["OrderQty"].ToString()) ? 0 : Convert.ToInt32(od_row["OrderQty"].ToString().Trim());
                        r_item.OrderID = PayloadManager.ReturnOrderPayload.ReturnOrderID;
                        result.Add(r_item);
                    }
                }

                Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][End:GetROItems]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][GetROItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }


        public void UpdateInventory(ObservableCollection<ReturnItem> returnItems, int returnOrderID, ReturnOrderActions action)
        {

            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:UpdateInventory]");
            try
            {
                bool isROModified = false;

                string query = @"select od.ItemID as ItemID,od.OrderQty as OrderQty,od.OrderUM as OrderUM,ic.PrimaryUM as PrimaryUM,im.imlitm as ItemNumber
                                 from busdta.Order_detail od join busdta.ItemConfiguration ic on od.ItemId = ic.ItemId 
                                 join busdta.F4101 im on od.itemid = im.imitm 
                                 where od.OrderID='{0}'";
                query = string.Format(query, returnOrderID);

                DataSet ds = DbEngine.ExecuteDataSet(query);
                List<ReturnItem> existingItems = new List<ReturnItem>();
                if (ds.HasData())
                {
                    foreach (DataRow itemRow in ds.Tables[0].Rows)
                    {
                        ReturnItem returnItem = new ReturnItem();
                        returnItem.ItemId = itemRow["ItemID"].ToString().Trim();
                        returnItem.UM = itemRow["OrderUM"].ToString().Trim();
                        returnItem.ReturnQty = Convert.ToInt32(itemRow["OrderQty"].ToString().Trim());
                        returnItem.PrimaryUM = itemRow["PrimaryUM"].ToString().Trim();
                        returnItem.ItemNumber = itemRow["ItemNumber"].ToString().Trim();

                        existingItems.Add(returnItem);

                    }
                }
                //** Cases handled
                //Existing item was removed
                //Existing item qty was altered
                //New Item was added 
                //Existing items were removed and new items were added with same qty
                var checkAlreadyCommitted = DbEngine.ExecuteScalar("select count(1) from busdta.Order_Detail where orderqty <> 0 and orderid = " + returnOrderID + "");
                var count = Convert.ToInt32(checkAlreadyCommitted);
                if (count > 0)
                {
                    foreach (var returnItem in existingItems)
                    {
                        var factor = 0.0m;
                        if (UoMManager.ItemUoMFactorList != null)
                        {
                            var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == returnItem.ItemId.Trim()) && (x.FromUOM == returnItem.UM.ToString()) && (x.ToUOM == returnItem.PrimaryUM));
                            factor = itemUomConversion == null ? Convert.ToDecimal(UoMManager.GetUoMFactor(returnItem.UM, returnItem.PrimaryUM, Convert.ToInt32(returnItem.ItemId.Trim()), returnItem.ItemNumber.Trim())) : Convert.ToDecimal(itemUomConversion.ConversionFactor);
                        }
                        else
                        {
                            factor = Convert.ToDecimal(UoMManager.GetUoMFactor(returnItem.UM, returnItem.PrimaryUM, Convert.ToInt32(returnItem.ItemId.Trim()), returnItem.ItemNumber.Trim()));
                        }
                        returnItem.ReturnQtyInPrimaryUoM = Convert.ToInt32(returnItem.ReturnQty * factor);

                        var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity + " + (returnItem.ReturnQtyInPrimaryUoM) + " WHERE ItemId=" + returnItem.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                        DbEngine.ExecuteNonQuery(invr_query);

                    }
                }
                switch (action)
                {
                    case ReturnOrderActions.Accept:
                        // Update committed Qty. for all items in RO
                        foreach (var r_item in returnItems) // Update inventory for first time.
                        {
                            var inv_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity - " + r_item.ReturnQtyInPrimaryUoM + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            DbEngine.ExecuteNonQuery(inv_query);
                        }

                        break;
                    //case ReturnOrderActions.Void:
                    //    if (count == 0)
                    //    {
                    //        foreach (var r_item in returnItems)
                    //        {
                    //            var invr_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity + " + (r_item.ReturnQtyInPrimaryUoM) + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    //            DbEngine.ExecuteNonQuery(invr_query);
                    //        }
                    //    }

                    //    break;
                    case ReturnOrderActions.OrderReturned:
                        foreach (var r_item in returnItems) // Commit final qty's to inventory.
                        {
                            //var inv_upd_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity + " + (r_item.ReturnQtyInPrimaryUoM) + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            //DbEngine.ExecuteNonQuery(inv_upd_query);
                            var inv_upd_onHand_query = @"update busdta.Inventory set OnHandQuantity = OnHandQuantity + " + r_item.ReturnQtyInPrimaryUoM + " WHERE ItemId=" + r_item.ItemId.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                            DbEngine.ExecuteNonQuery(inv_upd_onHand_query);

                            var invUpdateQueryHeld = @"update busdta.Inventory set HeldQuantity = HeldQuantity +" + r_item.NonSellableQtyInPrimaryUoM + " where ItemId = " + r_item.ItemId.Trim() + " and RouteId = " + CommonNavInfo.RouteID;
                            DbEngine.ExecuteNonQuery(invUpdateQueryHeld);
                        }
                        break;
                    case ReturnOrderActions.OrderModified:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][UpdateInventory][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManager][Start:UpdateInventory]");

        }


        public void UpdateCommitted(int returnQty = 0, string itemid = "", bool updateHold = false)
        {

            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:UpdateCommitted]");
            try
            {
                if (!updateHold)
                {
                    var inv_query = @"update busdta.Inventory set CommittedQuantity = CommittedQuantity - " + returnQty + " WHERE ItemId=" + itemid.Trim() + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    DbEngine.ExecuteNonQuery(inv_query);
                }
                else
                {
                    var OH_query = @"update busdta.Order_Header set HoldCommitted = 0 where orderid = " + PayloadManager.ReturnOrderPayload.ReturnOrderID + " AND RouteId='" + CommonNavInfo.RouteID + "'";
                    DbEngine.ExecuteNonQuery(OH_query);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][UpdateCommitted][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:UpdateCommitted]");
        }

        public bool IsROPicked(int returnOrderID)
        {
            bool result = false;
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:IsROPicked]");
            try
            {
                var pickedSum = DbEngine.ExecuteScalar(@"SELECT sum(pickedqty) FROM BUSDTA.Pick_Detail where TransactionID = " + returnOrderID + "");
                result = string.IsNullOrEmpty(pickedSum.Trim()) ? false : Convert.ToInt32(pickedSum.Trim()) > 0;
            }
            catch (Exception ex)
            {
                result = false;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][IsROPicked][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:IsROPicked]");
            return result;

        }

        /// <summary>
        /// GetROStatusTypeID
        /// </summary>
        /// <param name="salesOrderID"></param>
        /// <returns></returns>
        public int GetROStatusTypeId(int salesOrderID)
        {
            int statusid = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManagr][Start:GetROstatusTypeId]");
            try
            {
                string query = @"select OrderTypeId from busdta.order_header where orderid = (select top 1 returnorderid from busdta.SalesOrder_ReturnOrder_Mapping where salesorderid = " + salesOrderID + ")";
                var result = DbEngine.ExecuteScalar(query);
                statusid = Convert.ToInt32(string.IsNullOrEmpty(result) ? "-1" : result.Trim());
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerReturnsManagr][GetROstatusTypeId][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerReturnsManagr][End:GetROstatusTypeId]");
            return statusid;
        }

        public Dictionary<string, bool> GetOrderFlags(int OrderID)
        {
            bool IsSettled = false;
            bool IsOrderReturned = false;
            Dictionary<string, bool> result = new Dictionary<string, bool>();
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:GetOrderFlags]");
            try
            {
                var q_ordcmp = "select if orderstateid = " + ActivityKey.OrderReturned.GetStatusIdFromDB() + " then 1 else 0 ENDIF from busdta.Order_header where orderid = " + OrderID + "";
                var ordcmp = DbEngine.ExecuteScalar(q_ordcmp);

                IsOrderReturned = string.IsNullOrEmpty(ordcmp) ? false : Convert.ToInt32(ordcmp.Trim()) > 0;

                //var q_setld = "SELECT count(M.TDSTTLID) FROM BUSDTA.M50012 M WHERE TDTYP='ORDERRETURNED' and TDREFHDRID = " + OrderID + "";
                var q_setld = "SELECT LTRIM(rtrim((M.TDSTTLID))) AS STID FROM BUSDTA.M50012 M WHERE TDTYP='ORDERRETURNED' and TDREFHDRID = " + OrderID + "";
                var settld = DbEngine.ExecuteScalar(q_setld);

                IsSettled = string.IsNullOrEmpty(settld.Trim()) ? false : true;


                //var q_result = DbEngine.ExecuteDataSet(@"SELECT if isnull(LTRIM(RTRIM(M.TDSTTLID)),0) <> 0 then 1 else 0 endif AS STTID FROM BUSDTA.M50012 M WHERE TDSTAT='ORDERRETURNED' AND TDREFHDRID =" + OrderID + "");
                //if (q_result.HasData())
                //{
                //    IsSettled = !string.IsNullOrEmpty(q_result.Tables[0].Rows[0]["STTID"].ToString()) && Convert.ToBoolean(q_result.Tables[0].Rows[0]["STTID"]); ;
                //    IsOrderReturned = !string.IsNullOrEmpty(q_result.Tables[0].Rows[0]["IsCompleteReturn"].ToString()) && Convert.ToBoolean(q_result.Tables[0].Rows[0]["IsCompleteReturn"]); ;

                //}
                result.Add("IsSettled", IsSettled);
                result.Add("IsOrderReturned", IsOrderReturned);
            }
            catch (Exception ex)
            {
                result = null;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][GetOrderFlags][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:GetOrderFlags]");
            return result;

        }

        public bool DeleteHoldReturnItems(int returnOrderID)
        {
            bool result = false;
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:DeleteHoldReturnItems]");
            try
            {
                var pickedSum = DbEngine.ExecuteScalar(@"DELETE FROM BUSDTA.Order_Detail  WHERE  ORDERID='" + returnOrderID + "' ");
                result = string.IsNullOrEmpty(pickedSum.Trim()) ? false : Convert.ToInt32(pickedSum.Trim()) > 0;
            }
            catch (Exception ex)
            {
                result = false;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][DeleteHoldReturnItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:DeleteHoldReturnItems]");
            return result;

        }
        public bool IsOrderPartiallyReturn(int salesOrderID)
        {
            int result = 0;
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][Start:DeleteHoldReturnItems]");
            try
            {
                var stringa = "select ReturnOrderID FROM busdta.SalesOrder_ReturnOrder_Mapping where SalesOrderId = " + salesOrderID + "";
                var dbset = DbEngine.ExecuteDataSet(stringa);

                if (dbset.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow rw in dbset.Tables[0].Rows)
                    {
                        var checkResult = DbEngine.ExecuteScalar("select OrderStateId from busdta.order_header where OrderID = " + rw[0].ToString().Trim());
                        if (!string.IsNullOrEmpty(checkResult))
                            if (Convert.ToInt32(checkResult) == ActivityKey.VoidAtROPick.GetStatusIdFromDB() ||
                                Convert.ToInt32(checkResult) == ActivityKey.VoidAtROEntry.GetStatusIdFromDB() ||
                                Convert.ToInt32(checkResult) == ActivityKey.CreateReturnOrder.GetStatusIdFromDB())
                                continue;
                        var itemIdSet = DbEngine.ExecuteDataSet("select OrderDetailId from busdta.SalesOrder_ReturnOrder_Mapping where SalesOrderId = " + salesOrderID + " and ReturnOrderID =" + rw[0].ToString().Trim());

                        if (itemIdSet.HasData())
                        {
                            foreach (DataRow dataRow in itemIdSet.Tables[0].Rows)
                            {
                                string O_qty = DbEngine.ExecuteScalar("select orderqty from busdta.order_detail where orderdetailid =" + dataRow[0].ToString().Trim()).ToString();
                                if (!string.IsNullOrEmpty(O_qty))
                                {
                                    if (Convert.ToInt32(O_qty) > 0)
                                        result++;
                                }

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result = 0;
                Logger.Error("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][DeleteHoldReturnItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.CustomerReturnsManager][CustomerReturnsManager][End:DeleteHoldReturnItems]");
            return !(result > 0);

        }

    }
    public enum ReturnOrderActions
    {
        Accept,
        Hold,
        Void,
        OrderReturned,
        OrderModified
    }

    class ReturnOrderChanges
    {
        public int ItemID { get; set; }
        public int SalesOrderID { get; set; }
        public int OrderQty { get; set; }
    }
}
