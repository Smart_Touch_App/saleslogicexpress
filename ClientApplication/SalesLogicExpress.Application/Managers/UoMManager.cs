﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;


namespace SalesLogicExpress.Application.Managers
{
    public class UoMManager
    {
        //Todo Remove ItemUoM code from ResourceManager
        /// <summary>
        /// Manages UoM list for items.
        /// </summary>
        /// 
        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.UoMManager");

        private static Dictionary<string, List<string>> itemUMList = null;

        public static List<ItemUOMConversion> ItemUoMFactorList = null;

        public UoMManager()
        {
            ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;
        }

        void Synchronization_SyncProgressChanged(object sender, SyncManager.SyncUpdatedEventArgs e)
        {
            //Todo add singleton instance of uom manager in resource manager
            RefreshUoMList();
        }

        private static async void RefreshUoMList()
        {
            await Task.Run(() =>
            {
                itemUMList = null;
                itemUMList = GetItemUoMs();
            });
        }

        public static Dictionary<string, List<string>> GetItemUoMs()
        {

            var umList = new Dictionary<string, List<string>>();
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][UoMManager][Start:GetItemUoMs]");

                #region

                var templateUms = DbEngine.ExecuteDataSet(@"select distinct im.IMLITM as ItemNumber , iu.UOM as UOM, iu.DisplaySeq from busdta.ItemUoMs iu  inner join  busdta.F4101 im  on iu.ItemID = im.IMITM where iu.cansell=1 order by iu.DisplaySeq").Tables[0];

                foreach (DataRow dr in templateUms.Rows)
                {
                    if (umList.ContainsKey(dr["ItemNumber"].ToString().Trim()))
                    {
                        umList[dr["ItemNumber"].ToString().Trim()].Add(dr["UOM"].ToString().Trim());
                    }
                    else
                    {
                        umList.Add(dr["ItemNumber"].ToString().Trim(), new List<string>());
                        umList[dr["ItemNumber"].ToString().Trim()].Add(dr["UOM"].ToString().Trim());
                    }
                }
                #endregion
                Logger.Info("[SalesLogicExpress.Application.Managers][UoMManager][End:GetItemUoMs]");

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][UoMManager][GetItemUoMs][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            return umList;
        }

        public static string GetDefaultSaleableUOMForItem(string itemNumber)
        {
            string UOM;
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][UoMManager][Start:GetDefaultSaleableUOMForItem][itemNumber:" + itemNumber + "]");

                UOM = DbEngine.ExecuteScalar(@"select distinct iu.UOM as UOM
                                                from busdta.ItemUoMs iu  inner join  busdta.F4101 im  on iu.ItemID = im.IMITM where iu.cansell=1 
                                                and iu.DisplaySeq=(select min(DisplaySeq) from busdta.ItemUoMs where CanSell=1 and iu.ItemID=ItemID)
                                                and im.IMLITM='" + itemNumber + "'");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][UoMManager][GetItemUoMs][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            return UOM;
        }

        public static Dictionary<string, System.Collections.Generic.List<string>> GetItemUMList
        {
            get
            {
                return itemUMList ?? GetItemUoMs();
            }
        }
        public static double GetUoMFactor(string fromUoM, string toUom, int ItemId, string ItemNumber)
        {
            double uoMFactor = 0.0f;

            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][UoMManager][Start:GetUoMFactor]");
                // todo remove GetUOMConversionList() from inventory manager
                var pricingManager = new PricingManager();
                //************************************************************************************************
                // Comment: Converted to double as the jdeUOMConversion is passed as decimal as part of unit prce change request
                // Created: feb 16, 2016
                // Author: Vivensas (Rajesh,Yuvaraj)
                // Revisions: 
                //*************************************************************************************************
                uoMFactor = Convert.ToDouble(pricingManager.jdeUOMConversion(jdeShortItem: ItemId, fromUoM: fromUoM, toUom: toUom));

                //*************************************************************************************************
                // Vivensas changes ends over here
                //**************************************************************************************************

                if (ItemUoMFactorList == null)
                    ItemUoMFactorList = new List<ItemUOMConversion>();

                // If uomFactor is less than 0, set it to 1
                uoMFactor = uoMFactor < 0 ? 1 : uoMFactor;
                var itemFactorObj = new ItemUOMConversion
                {
                    FromUOM = fromUoM,
                    ToUOM = toUom,
                    ItemNumber = ItemNumber.ToString(),
                    ItemID = ItemId.ToString(),
                    ConversionFactor = uoMFactor

                };
                if (!ItemUoMFactorList.Contains(itemFactorObj))
                    ItemUoMFactorList.Add(itemFactorObj);
                Logger.Info("[SalesLogicExpress.Application.Managers][UoMManager][End:GetUoMFactor]");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][UoMManager][GetUoMFactor][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }

            return uoMFactor;
        }
    }
}
