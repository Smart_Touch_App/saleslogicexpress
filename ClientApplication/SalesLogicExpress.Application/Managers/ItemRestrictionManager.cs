﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class ItemRestrictionManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ItemRestrictionManager");

        public string GetFlag(string customerID)
        {
            string str = null;
            Logger.Info("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][Start:GetFlag][CustomerID = " + customerID + "]");

            try
            {
                string query = "select P.MAPA8 Parent,P.MAAN8 BillTo, C.AIAN8 ShipTo from";
                query = query + " BUSDTA.F0150 P join BUSDTA.F0101 B on P.MAAN8=B.ABAN81 and P.MAOSTP='   '";
                query = query + " join BUSDTA.F03012 C on B.ABAN8=C.AIAN8 ";
                query = query + " where C.AIAN8 = '" + customerID + "';";

                string parentCust = Helpers.DbEngine.ExecuteScalar(query);

                query = "select AIEDF2, AIAN8 from busdta.F03012 where AIAN8 = '" + parentCust + "'";
                string flag = Helpers.DbEngine.ExecuteScalar(query);
                str = parentCust + "-" + flag;
            }
            catch (Exception ex)
            {

            }       
            return str;
        }
        public ObservableCollection<ItemRestriction> GetItemRestrictionList(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][Start:GetItemRestrictionList][CustomerID = " + customerID + "]");
            ObservableCollection<ItemRestriction> restrictedItemList = new ObservableCollection<ItemRestriction>();

            try
            {
                string str = GetFlag(customerID);
                string flag = str.Split('-')[1].Trim();
                string parentCust = str.Split('-')[0];

                if (!string.IsNullOrEmpty(flag))
                {
                   string query = "select IMLITM,IMDSC1 ";
                   query = query + " from BUSDTA.F4013 JOIN BUSDTA.F4101 on CAST(SXXRVT as INT)=IMITM JOIN BUSDTA.F4102 on IMITM = IBITM JOIN  busdta.ItemConfiguration ic on imitm = ic.itemId ";
                   query = query + " where SXXRTC='IR' and rtrim(SXXRVF) like right('00000000'+ '" + parentCust + "', 8)  and RouteEnabled = 1 and allowSearch=1 ";

                    DataSet itemList = Helpers.DbEngine.ExecuteDataSet(query);
                    ItemRestriction restrictedItem;
                    if (itemList.HasData())
                    {
                        DataRow row;
                        for (int i = 0; i < itemList.Tables[0].Rows.Count; i++)
                        {
                            row = itemList.Tables[0].Rows[i];
                            restrictedItem = new ItemRestriction();
                            restrictedItem.ItemNo = row["IMLITM"].ToString().Trim();
                            restrictedItem.Description = row["IMDSC1"].ToString();
                            restrictedItem.RestrictionFlag = flag;
                            restrictedItemList.Add(restrictedItem);
                        }
                    }
                }
                else
                {
                    restrictedItemList = null;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ItemRestrictionManager][GetItemRestrictionList][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return restrictedItemList;
        }
    }
}
