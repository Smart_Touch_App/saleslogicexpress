﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using System.Collections.ObjectModel;
using System.Timers;
using log4net;
using SalesLogicExpress.Domain;
using System.Configuration;
using System.Threading;
using Timer = System.Timers.Timer;

namespace SalesLogicExpress.Application.Managers
{
    public class QueueManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.QueueManager");
        public event EventHandler<SyncQueueChangedEventArgs> QueueChanged;

        public QueueManager()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:QueueManager]");
            try
            {
                QueueTimer = new Timer();
                QueueTimer.Elapsed += QueueTimerElapsed;
                QueueTimer.Enabled = true;
                QueueTimer.AutoReset = true;
                QueueTimer.Interval = string.IsNullOrEmpty(ConfigurationManager.AppSettings["syncPollingTime"].ToString()) ? 60000 : Convert.ToInt32(ConfigurationManager.AppSettings["syncPollingTime"].ToString());
                QueueTimer.Start();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][QueueManager][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:QueueManager]");
        }


        public void ListenToSync()
        {
            ResourceManager.Synchronization.SyncProgressChanged += OnSyncStateChange;
            ResourceManager.Transaction.TransactionLogged += Transaction_TransactionLogged;
        }

        private void Transaction_TransactionLogged(object sender, EventArgs e)
        {
            var activities = GetActivities();
            if (activities != null)
            {
                AddSyncDetail(activities).ResetAllQueues().PopulateAllQueues().NotifyQueueChange(SyncQueueType.Priority);
            }
        }
        public void Transaction_TransactionLogged()
        {

        }
        void OnSyncStateChange(object sender, SyncManager.SyncUpdatedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:SyncProgressChanged][SyncManager.SyncUpdatedEventArgs=" + e.SerializeToJson() + "]");
            try
            {
                if (e.State == SyncManager.SyncUpdateType.SyncFailed || e.State == SyncManager.SyncUpdateType.ServerNotReachable)
                {
                    //  Popout error that sync didnot happened
                    ViewModels.CommonNavInfo.SyncMessage = Helpers.Constants.Common.SyncFailedMessage;
                    return;
                }
                if (e.State == SyncManager.SyncUpdateType.DownloadComplete)
                {
                    ViewModels.CommonNavInfo.SyncMessage = string.Empty;
                    if (e.Queue != null)
                    {
                        foreach (Domain.TransactionSyncDetail syncActivity in e.Queue)
                        {
                            UpdateQueue(syncActivity, e.StateChangedTime).RemoveTransactionFromQueue(syncActivity);
                        }
                    }
                    if (e.Queue == null && e.State == SyncManager.SyncUpdateType.DownloadComplete)
                    {
                        // Case of manual sync, where we are  processing all queue's hence queue is passed as null
                        UpdateQueue(null, e.StateChangedTime).ResetAllQueues();
                    }
                    ViewModels.CommonNavInfo.SetLastSynched(DateTime.Now.ToString("M'/'dd'/'yyyy' 'hh:mm tt"));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][SyncProgressChanged][SyncManager.SyncUpdatedEventArgs=" + e.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:SyncProgressChanged][SyncManager.SyncUpdatedEventArgs=" + e.SerializeToJson() + "]");
        }

        private void QueueTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:QueueTimerElapsed]");
            try
            {
                PopulateAllQueues().NotifyQueueChange(SyncQueueType.Priority);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][QueueTimerElapsed][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:QueueTimerElapsed]");
        }

        internal string GetTransactionFromQueue(Queue<string> SyncQueue)
        {
            string transaction = null;
            if (SyncQueue.Count != 0)
            {
                transaction = SyncQueue.Peek().ToString();
            }
            return transaction;
        }

        private List<Activity> GetActivities()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:GetActivities]");

                List<Activity> activityList = new List<Activity>();

                string getTransactionActivityList = "select TDID as ActivityID,TDROUT as RouteID,TDTYP as ActivityType,TDCLASS as ActivityDetailClass,TDDTLS as ActivityDetails,TDSTRTTM as ActivityStart,TDENDTM as ActivityEnd,TDSTID as StopInstanceID,TDAN8 as CustomerId,TDSTTLID as SettlementID,TDPNTID as ActivityHeaderID,TDSTAT as ActivityStatus ";
                getTransactionActivityList = getTransactionActivityList + " from busdta.M50012 WHERE TDPNTID=0 and TDID not in(select DISTINCT SDTXID from busdta.M50052 tsd where SDISYNCD=1) and (TDENDTM <> '0001-01-01 00:00:00.000')";
                //string getTransactionActivityList = "select td.TransactionID as ActivityID, td.TransactionType as ActivityType, td.* from busdta.TransactionActivityDetail td WHERE td.ParentTransactionID=0 and td.TransactionID not in( select ActivityID from busdta.TransactionSyncDetail tsd )";


                List<Activity> activities = DbEngine.ExecuteDataSet(getTransactionActivityList).GetEntityList<Activity>();
                Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:GetActivities]");

                return activities;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][GetActivities][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                return null;
            }
        }

        public QueueManager AddSyncDetail(List<Activity> activities)
        {
            if (ResourceManager.Synchronization.IsSynching) return this;
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:AddSyncDetail]");
            try
            {
                string query = string.Empty;
                string deleteQuery = string.Empty;
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                // Add the activites in SyncDetail table
                foreach (Activity activity in activities)
                {
                    query = "INSERT INTO busdta.M50052(SDTXID,SDKEY,SDISYNCD)VALUES ({0},'{1}',0)";
                    deleteQuery = "delete from busdta.M50052 where SDTXID={0} and SDKEY='{1}' and SDISYNCD=0";
                    deleteQuery = string.Format(deleteQuery, activity.ActivityID, activity.ActivityType);
                    DbEngine.ExecuteNonQuery(deleteQuery, parameters);
                    query = string.Format(query, activity.ActivityID, activity.ActivityType);
                    DbEngine.ExecuteNonQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][AddSyncDetail][activities=" + activities.SerializeToJson() + "][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:AddSyncDetail]");
            return this;
        }

        public QueueManager ResetAllQueues()
        {
            PriorityQueue.Clear();
            ImmediateQueue.Clear();
            NormalQueue.Clear();
            return this;
        }
        public QueueManager RemoveTransactionFromQueue(TransactionSyncDetail syncItem)
        {
            // Remove activity from in-memory queue

            switch (syncItem.Priority.Trim().ParseEnum<SyncPriority>(SyncPriority.Normal))
            {
                case SyncPriority.High:
                    if (PriorityQueue.Count > 0)
                    {
                        PriorityQueue.Dequeue();
                    }
                    break;
                case SyncPriority.Immediate:
                    if (ImmediateQueue.Count > 0)
                    {
                        ImmediateQueue.Dequeue();
                    }
                    break;
                case SyncPriority.Normal:
                    if (NormalQueue.Count > 0)
                    {
                        NormalQueue.Dequeue();
                    }
                    break;
            }
            return this;
        }
        public QueueManager UpdateQueue(TransactionSyncDetail syncItem, DateTime updateTimeStamp)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:UpdateQueue]");
            try
            {
                string query = string.Empty;
                if (syncItem == null)
                {
                    query = string.Format("update busdta.M50052 set SDISYNCD={0}, SDTMSTMP={1}", 1, updateTimeStamp.GetFormattedDateTimeForDb());
                }
                else
                {
                    query = string.Format("update busdta.M50052 set SDISYNCD={0}, SDTMSTMP={1} where SDKEY='{2}'", 1, updateTimeStamp.GetFormattedDateTimeForDb(), syncItem.TransactionKey);
                }
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][UpdateQueue][syncItem=" + syncItem + "][updateTimeStamp=" + updateTimeStamp.ToString() + "][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:UpdateQueue]");
            return this;
        }
        public void NotifyForManualSync()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:NotifyForImmediateSync]");
            try
            {
                PopulateAllQueues().NotifyQueueChange(SyncQueueType.All);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][NotifyForImmediateSync][ExceptionStackTrace = " + ex.InnerException.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:NotifyForImmediateSync]");
        }
        public QueueManager PopulateAllQueues()
        {
            if (ResourceManager.Synchronization.IsSynching) return this;
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:PopulateQueue]");
            try
            {
                string query = string.Empty;
                query = "select sd.SDID as SyncID, sd.SDTXID as TransactionID,sd.SDKEY as TransactionKey, sd.SDISYNCD as Synced,sd.SDTMSTMP as SyncTimeStamp,sm.SMPRTY as Priority from busdta.M50052 sd left outer join busdta.M5005 sm on sd.SDKEY = sm.SMKEY Where sd.SDISYNCD=0";
                DataSet result = DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    List<TransactionSyncDetail> SyncDetails = result.GetEntityList<TransactionSyncDetail>();
                    SyncPriority priority;
                    if (SyncDetails == null) return this;
                    List<string> transactions = new List<string>();
                    foreach (TransactionSyncDetail syncItem in SyncDetails)
                    {
                        if (transactions.Contains(syncItem.TransactionKey.Trim())) continue;
                        transactions.Add(syncItem.TransactionKey.Trim());
                        if (syncItem.Priority == null)
                        {
                            continue;
                        }
                        priority = syncItem.Priority.Trim().ParseEnum<SyncPriority>(SyncPriority.Normal);
                        if (priority == SyncPriority.Normal)
                        {
                            NormalQueue.Enqueue(syncItem);
                        }
                        if (priority == SyncPriority.High)
                        {
                            PriorityQueue.Enqueue(syncItem);
                        }
                        if (priority == SyncPriority.Immediate)
                        {
                            ImmediateQueue.Enqueue(syncItem);
                        }
                    }
                    // Set IsDataInQueue to true or false
                    IsDataInQueue = SyncDetails.Count > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][PopulateQueue][ExceptionStackTrace = " + ex.InnerException != null ? ex.InnerException.Message : "" + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:PopulateQueue]");
            return this;
        }
        public QueueManager NotifyQueueChange(SyncQueueType QueueType)
        {
            if (ResourceManager.Synchronization.IsSynching) return this;
            // Raising event for change
            SyncQueueChangedEventArgs args = new SyncQueueChangedEventArgs();
            args.IsDataInQueue = IsDataInQueue;
            args.NormalQueue = NormalQueue.Clone();
            args.PriorityQueue = PriorityQueue.Clone();
            args.ImmediateQueue = ImmediateQueue.Clone();
            args.StateChangedTime = DateTime.Now;
            args.QueueType = QueueType;
            OnQueueStateChange(args);
            return this;
        }
        public QueueManager NotifyQueueChange()
        {
            if (ResourceManager.Synchronization.IsSynching) return this;
            // Raising event for change
            SyncQueueChangedEventArgs args = new SyncQueueChangedEventArgs();
            args.IsDataInQueue = IsDataInQueue;
            args.NormalQueue = NormalQueue.Clone();
            args.PriorityQueue = PriorityQueue.Clone();
            args.ImmediateQueue = ImmediateQueue.Clone();
            args.StateChangedTime = DateTime.Now;
            OnQueueStateChange(args);
            return this;
        }
        public void OnTimeout()
        {
            throw new System.NotImplementedException();
        }
        protected virtual void OnQueueStateChange(SyncQueueChangedEventArgs e)
        {
            EventHandler<SyncQueueChangedEventArgs> handler = QueueChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        internal bool IsTransactionInSyncQueue(Queue<string> SyncQueue)
        {
            if (SyncQueue.Count != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        Queue<Domain.TransactionSyncDetail> _PriorityQueue = new Queue<Domain.TransactionSyncDetail>();
        Queue<Domain.TransactionSyncDetail> _NormalQueue = new Queue<Domain.TransactionSyncDetail>();
        Queue<Domain.TransactionSyncDetail> _ImmediateQueue = new Queue<Domain.TransactionSyncDetail>();
        public Queue<Domain.TransactionSyncDetail> ImmediateQueue
        {
            get
            {
                return _ImmediateQueue;
            }
            set
            {
                _ImmediateQueue = value;
            }
        }
        public Queue<Domain.TransactionSyncDetail> PriorityQueue
        {
            get
            {
                return _PriorityQueue;
            }
            set
            {
                _PriorityQueue = value;
            }
        }
        public Queue<Domain.TransactionSyncDetail> NormalQueue
        {
            get
            {
                return _NormalQueue;
            }
            set
            {
                _NormalQueue = value;
            }
        }

        public class SyncQueueItem
        {
            public string Transaction { get; set; }
            public int Count { get; set; }
        }
        public int TotalPendingSync { get; set; }

        public Timer QueueTimer
        {
            get;
            set;
        }

        public bool IsDataInQueue
        {
            get;
            set;
        }
        public class SyncQueueChangedEventArgs : EventArgs
        {
            public bool IsDataInQueue { get; set; }
            public bool ManualSync { get; set; }
            public Queue<Domain.TransactionSyncDetail> NormalQueue { get; set; }
            public Queue<Domain.TransactionSyncDetail> PriorityQueue { get; set; }
            public Queue<Domain.TransactionSyncDetail> ImmediateQueue { get; set; }
            public DateTime StateChangedTime { get; set; }
            public SyncQueueType QueueType { get; set; }
        }
        public enum SyncQueueType
        {
            All,
            Immediate,
            Priority,
            Normal
        }
        internal ObservableCollection<SyncQueueItem> GetPendingSyncItems()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][Start:GetPendingSyncItems]");
            ObservableCollection<SyncQueueItem> queueitems = new ObservableCollection<SyncQueueItem>();
            try
            {
                string query = "select count(SDKEY) as Count, SDKEY as Transaction from busdta.M50052 where SDISYNCD=0 group by SDKEY";
                System.Collections.Generic.List<SyncQueueItem> queueItems = DbEngine.ExecuteDataSet(query).GetEntityList<SyncQueueItem>();
                queueitems = new ObservableCollection<SyncQueueItem>(queueItems);
                if (queueitems.Count != (PriorityQueue.Count + ImmediateQueue.Count + NormalQueue.Count))
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][GetPendingSyncItems][Exception = InMemory and Db Queue count dont match..]");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][QueueManager][GetPendingSyncItems][ExceptionStackTrace = " + ex.Message + "]");
                return queueitems;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][QueueManager][End:GetPendingSyncItems]");
            return queueitems;
        }
    }

}
