﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Managers
{
    public class ARPaymentManager
    {

        #region Variables and object declaration

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.ARPaymentManager");
        private string customerId = "";
        private string companyId = "";

        #endregion

        #region Construtor


        public ARPaymentManager(string customerId, string companyId)
        {
            this.customerId = customerId;
            this.companyId = companyId;
        }

        #endregion

        #region Methods

        public List<Dictionary<string, decimal>> GetAgeingSummary(ObservableCollection<PaymentARModel.InvoiceDetails> OpenInvoices)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetAgeingSummary]");
            List<Dictionary<string, decimal>> AgeingSummary = new List<Dictionary<string, decimal>>();
            try
            {
                if (OpenInvoices == null)
                    OpenInvoices = new Managers.InvoiceManager().GetInvoices(this.customerId).Where(s => s.OpenAmountInvDtl > 0).ToObservableCollection();

                if (OpenInvoices != null && OpenInvoices.Count > 0)
                {
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR30Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 30) && (x.OpenAmountInvDtl>0)).Sum(x => x.OpenAmountInvDtl) } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR60Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 60 && x.AgingInvDtl > 30) && (x.OpenAmountInvDtl>0)).Sum(x => x.OpenAmountInvDtl) } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90Days", OpenInvoices.Where(x => (x.AgingInvDtl <= 90 && x.AgingInvDtl > 60) && (x.OpenAmountInvDtl>0)).Sum(x => x.OpenAmountInvDtl) } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90MoreDays", OpenInvoices.Where(x => (x.AgingInvDtl > 90) && (x.OpenAmountInvDtl>0)).Sum(x => x.OpenAmountInvDtl) } });
                }
                else
                {
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR30Days", 0 } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR60Days", 0 } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90Days", 0 } });
                    AgeingSummary.Add(new Dictionary<string, decimal>() { { "AR90MoreDays", 0 } });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetAgeingSummary][ExceptionStackTrace = " + ex.Message + "]");
                throw;
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetAgeingSummary]");
            return AgeingSummary;
        }

        /// <summary>
        /// Returns updated list of Receipt History 
        /// </summary>
        /// <returns>List Of Receipt History</returns>
        public List<Domain.PaymentARModel.ReceiptHistory> GetReceiptHistory()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetReceiptHistory]");
            DataSet receiptHistory = null;
            List<Domain.PaymentARModel.ReceiptHistory> receiptHistoryList = null;
            StringBuilder objQueryBuilder = new StringBuilder();
            
            try
            {
                string strUnknownStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='UNKNOWN'");

                //Used on xaml trigger for displaying the return on screen
                string strReturnOrderStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD'");

                //Write query to get list of receipt history 
                objQueryBuilder.Append("SELECT RH.CustomerId, RH.SettelmentId AS SettlementId, isnull(RH.CreditReasonCodeId, 0) AS CreditReasonCodeId, ISNULL(RM.ReasonCodeDescription,'') AS CreditReason, TRIM(ReceiptID) AS ReceiptId, ReceiptNumber, ROUND(TransactionAmount,2) AS PaymentAmount, case when PaymentMode= '0' then 0 else 1 end AS PaymentType, ");
                objQueryBuilder.Append("(CASE PaymentMode WHEN '0' THEN 'Cash' WHEN '1' THEN 'Check' END) AS ReceiptType,trim( ISNULL(ChequeNum,'')) AS ChequeNum,  Convert(VARCHAR(20), ChequeDate, 101) AS ChequeDate, ");
                objQueryBuilder.Append(" Convert(VARCHAR(20), ReceiptDate, 101) AS ReceiptDate, StatusId, isnull(rh.ReasonCodeId,0) AS ReasonCode, ");
                objQueryBuilder.Append(" (isnull((SELECT upper(trim(StatusTypeDESC)) FROM BUSDTA.Status_Type WHERE StatusTypeId=RH.StatusId),'UNKNOWN')) AS Status ");
                objQueryBuilder.Append(" FROM BUSDTA.Receipt_Header RH ");
                objQueryBuilder.Append(" LEFT OUTER JOIN BUSDTA.ReasonCodeMaster RM on RM.ReasonCodeId = RH.CreditReasonCodeId ");
                objQueryBuilder.Append(" WHERE RH.CustomerId=" + customerId + " ORDER BY RH.ReceiptDate DESC, RH.ReceiptId DESC");
                receiptHistory = DbEngine.ExecuteDataSet(objQueryBuilder.ToString(), "ReceiptHistory");

                //If Invoice Status is null, then it will treated as open. 
                objQueryBuilder.Clear();
                objQueryBuilder.Append("SELECT CL.CustomerID AS CustomerId, TRIM(CL.ReceiptId) AS ReceiptId,  CL.InvoiceId AS InvoiceId,");
                objQueryBuilder.Append(" TRIM(ISNULL(IH.DeviceInvoiceNumber,'')) +'/SO' AS InvoiceNumber, ROUND(CL.ReceiptAppliedAmt,2) AS InvoiceAmt, ");
                objQueryBuilder.Append(" (SELECT trim(ucase(StatusTypeDesc)) FROM BUSDTA.Status_Type WHERE StatusTypeId=isnull(isnull(NULLIF(IH.ARStatusID, " + strUnknownStatusId + "), IH.DeviceStatusID),1)) AS Status,");
                objQueryBuilder.Append(" convert(VARCHAR(20), IH.DeviceInvoiceDate, 101) AS InvoiceDate FROM BUSDTA.Customer_Ledger CL");
                objQueryBuilder.Append(" INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.CustomerID=IH.CustomerID");
                objQueryBuilder.Append(" WHERE ISNULL(CL.InvoiceId,0)!=0 AND ISNULL(CL.ReceiptId,0)!=0 AND CL.CustomerID=" + customerId + " AND CL.IsActive=1");
                objQueryBuilder.Append(" UNION ALL");
                objQueryBuilder.Append(" SELECT CL.CustomerID AS CustomerId, TRIM(CL.ReceiptId) AS ReceiptId, '' AS InvoiceId, 'Unapplied Cash'");
                objQueryBuilder.Append(" AS InvoiceNumber,  ROUND(RH.TransactionAmount-SUM(CASE CL.IsActive WHEN 1 THEN ISNULL(CL.ReceiptAppliedAmt,0) ELSE 0 END),2) AS InvoiceAmt ,");
                objQueryBuilder.Append(" '' AS Status,  '1/1/0001' AS InvoiceDate FROM BUSDTA.Customer_Ledger CL ");
                objQueryBuilder.Append(" INNER JOIN BUSDTA.Receipt_Header RH ON CL.ReceiptId=RH.ReceiptId WHERE ISNULL(CL.InvoiceId,0)!=0 ");
                objQueryBuilder.Append(" AND ISNULL(CL.ReceiptId,0)!=0 AND CL.CustomerID=" + customerId + " AND (RH.StatusId NOT IN (SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD IN ('VOID', 'VDSTLD')))");
                objQueryBuilder.Append(" GROUP BY CustomerId, ReceiptId, InvoiceNumber, InvoiceDate, StatusId, Status, RH.TransactionAmount HAVING InvoiceAmt<>0 ");
                objQueryBuilder.Append(" UNION ALL");
                objQueryBuilder.Append(" SELECT CL.CustomerID AS CustomerId, TRIM(CL.ReceiptId) AS ReceiptId, '' AS InvoiceId,");
                objQueryBuilder.Append(" 'Unapplied Cash'  AS InvoiceNumber, ROUND(CL.ReceiptUnAppliedAmt,2) AS InvoiceAmt ,'' AS Status, ");
                objQueryBuilder.Append(" '1/1/0001' AS InvoiceDate  FROM BUSDTA.Customer_Ledger CL WHERE CL.ReceiptId NOT IN");
                objQueryBuilder.Append(" (SELECT isnull(ReceiptId,0) FROM BUSDTA.Customer_Ledger WHERE ISNULL(InvoiceId,0)!=0)");
                objQueryBuilder.Append(" AND ISNULL(InvoiceId,0)=0  AND ROUND(CL.ReceiptUnAppliedAmt,2)<>0 AND CL.CustomerID=" + customerId);
                receiptHistory = DbEngine.ExecuteDataSet(objQueryBuilder.ToString(), null, "PaidInvoices", receiptHistory);

                receiptHistoryList = receiptHistory.Tables["ReceiptHistory"].AsEnumerable()
                        .Select(drReceiptHistory =>
                         new SalesLogicExpress.Domain.PaymentARModel.ReceiptHistory
                         {
                             ReceiptId = Convert.ToString(drReceiptHistory["ReceiptId"]),
                             IsCreditMemo = Convert.ToInt32(drReceiptHistory["CreditReasonCodeId"]) != 0 ? true : false,
                             CreditReason = drReceiptHistory["CreditReason"].ToString(),
                             ReceiptNo = Convert.ToString(drReceiptHistory["ReceiptNumber"]),
                             ReceiptType = Convert.ToString(drReceiptHistory["ReceiptType"]),
                             ReceiptAmount = Convert.ToDecimal(drReceiptHistory["PaymentAmount"]),
                             Status = Convert.ToString(drReceiptHistory["Status"]),
                             SettlementId = Convert.ToString(drReceiptHistory["SettlementId"]),
                             ReasonCode = (drReceiptHistory["ReasonCode"].ToString().Trim().Equals(strReturnOrderStatusId)?"return":""),
                             ReceiptDate = Convert.ToString(drReceiptHistory["ReceiptDate"]),
                             CheckNo = Convert.ToString(drReceiptHistory["ChequeNum"]),
                             CheckDate = Convert.ToString(drReceiptHistory["ChequeDate"]),
                             IsCash = !Convert.ToBoolean(drReceiptHistory["PaymentType"]),
                             IsCheck = Convert.ToBoolean(drReceiptHistory["PaymentType"]),
                             PaidInvoices = (receiptHistory.Tables["PaidInvoices"].AsEnumerable()
                                                .Where(dr => dr["ReceiptId"].Equals(drReceiptHistory["ReceiptId"]))
                                                .Select(dr =>
                                                    new SalesLogicExpress.Domain.PaymentARModel.InvoiceDetails
                                                    {
                                                        InvoiceNoInvDtl = (Convert.ToInt32(drReceiptHistory["CreditReasonCodeId"]) != 0 ? true : false) ? "" : Convert.ToString(dr["InvoiceNumber"]),
                                                        DateInvDtl = Convert.ToString(dr["InvoiceDate"]).Replace("1/1/0001", ""),
                                                        OpenAmountInvDtl = Convert.ToDecimal(dr["InvoiceAmt"]),
                                                        StatusInvDtl = Convert.ToString(dr["Status"]).Trim()
                                                    }).ToList<SalesLogicExpress.Domain.PaymentARModel.InvoiceDetails>())
                         }
                        ).ToList<SalesLogicExpress.Domain.PaymentARModel.ReceiptHistory>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetReceiptHistory][ExceptionStackTrace = " + ex.Message + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetReceiptHistory]");

            return receiptHistoryList;
        }

        /// <summary>
        /// Updates the payment type 
        /// </summary>
        /// <param name="isCash">True if payment type is cash, false if check</param>
        /// <param name="receiptNo">Receipt number</param>
        /// <param name="checkNo">Check no</param>
        /// <param name="receiptId">Autogenerated receipt Id</param>
        public void UpdateReceiptType(bool isCash, string receiptNo, string checkNo, string receiptId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateReceiptType]");

            try
            {
                string strSetQuery = "";

                if (isCash)
                    strSetQuery = "PaymentMode='0', ChequeNum=''";
                else
                    strSetQuery = "PaymentMode='1', ChequeNum=" + checkNo;

                //ToDo - Write Code to update the Receipt Type
                string updateQuery = string.Format("UPDATE BUSDTA.Receipt_Header SET {0} WHERE CustomerId={1} AND ReceiptNumber='{2}' AND RouteId={3}", strSetQuery, customerId, receiptNo, CommonNavInfo.RouteID.ToString());

                DbEngine.ExecuteNonQuery(updateQuery);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateReceiptType][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateReceiptType]");
        }

        /// <summary>
        /// Returns available un-applied amount of the receipt 
        /// </summary>
        /// <param name="receiptId">Receipt id</param>
        /// <returns>un-applied receipt amount</returns>
        public string GetReceiptUnAppliedAmt(string receiptId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetReceiptUnAppliedAmt]");
            string query = "";
            string unappliedAmt = "";
            try
            {
                query = "SELECT A.UnappliedAmount FROM (SELECT TRIM(CL.ReceiptId) AS ReceiptId, ROUND((RH.TransactionAmount-SUM(isnull(CL.ReceiptAppliedAmt,0))-SUM(isnull(CL.ConcessionAmt,0))),2) AS UnappliedAmount ";
                query = query + " FROM BUSDTA.Customer_Ledger CL INNER JOIN BUSDTA.Receipt_Header RH ON CL.ReceiptId=RH.ReceiptId";
                query = query + " WHERE ISNULL(CL.InvoiceId,'')!='' AND ISNULL(CL.ReceiptId,'')!='' AND CL.ReceiptId=" + receiptId;
                query = query + " GROUP BY ReceiptId, RH.TransactionAmount HAVING UnappliedAmount<>0) A";

                unappliedAmt = DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetReceiptUnAppliedAmt][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetReceiptUnAppliedAmt]");
            return unappliedAmt;
        }

        /// <summary>
        /// Updates receipt status into database 
        /// </summary>
        /// <param name="receiptId">receipt id to be updated</param>
        /// <param name="receiptStatus">receipt status</param>
        public void UpdateReceiptStatus(string receiptId, string receiptStatus)
        {
            string query = "";
            string strVoidStatusId = "";
            string strVoidSettledStatusId = "";
            string strSettledStatusId = ""; 

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateReceiptStatus]");

            try
            {
                //Get status id
                strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                strVoidSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VDSTLD'");
                strSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD'");

                if (!string.IsNullOrEmpty(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID))
                    query = ", SettelmentId=" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                //query = "UPDATE BUSDTA.Receipt_Header SET StatusId=(CASE StatusId WHEN 6 THEN 26 ELSE 3 END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();
                query = "UPDATE BUSDTA.Receipt_Header SET StatusId=(CASE StatusId WHEN " + strVoidStatusId + " THEN " + strVoidSettledStatusId + " ELSE " + strSettledStatusId + " END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();

                Logger.Info(" SettlementUpdateQuery = " + query );
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateReceiptStatus][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateReceiptStatus]");
        }

        public void UpdateExpenseStatus(string expenseId, string expenseStatus)
        {
            string query = "";
            string strVoidStatusId = "";
            string strVoidSettledStatusId = "";
            string strSettledStatusId = "";

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateExpenseStatus]");

            try
            {
                //Get status id
                strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                strVoidSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VDSTLD'");
                strSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD'");

                //if (!string.IsNullOrEmpty(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID))
                //    query = ", SettelmentId=" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                //query = "UPDATE BUSDTA.Receipt_Header SET StatusId=(CASE StatusId WHEN 6 THEN 26 ELSE 3 END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();
                query = "UPDATE BUSDTA.ExpenseDetails SET RouteSettlementId = '" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID + "', StatusId=(CASE StatusId WHEN " + strVoidStatusId + " THEN " + strVoidSettledStatusId + " ELSE " + strSettledStatusId + " END) WHERE ExpenseId IN (" + expenseId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();

                Logger.Info(" SettlementUpdateQuery = " + query);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateExpenseStatus][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateExpenseStatus]");
        }

        public void UpdateMoneyOrderStatus(string mOID, string mOStatus)
        {
            string query = "";
            string strVoidStatusId = "";
            string strVoidSettledStatusId = "";
            string strSettledStatusId = "";

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateMoneyOrderStatus]");

            try
            {
                //Get status id
                strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");
                strVoidSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VDSTLD'");
                strSettledStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='STTLD'");

                //if (!string.IsNullOrEmpty(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID))
                //    query = ", SettelmentId=" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID;

                //query = "UPDATE BUSDTA.Receipt_Header SET StatusId=(CASE StatusId WHEN 6 THEN 26 ELSE 3 END) " + query + " WHERE receiptId IN (" + receiptId + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();
                query = "UPDATE BUSDTA.MoneyOrderDetails SET RouteSettlementId = '" + ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID + "', StatusId=(CASE StatusId WHEN " + strVoidStatusId + " THEN " + strVoidSettledStatusId + " ELSE " + strSettledStatusId + " END) WHERE MoneyOrderId IN (" + mOID + ") AND RouteId=" + CommonNavInfo.RouteID.ToString();

                Logger.Info(" SettlementUpdateQuery = " + query);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateMoneyOrderStatus][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateMoneyOrderStatus]");
        }
        public decimal GetUnAppliedAmtForCustomer()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetUnAppliedAmtForCustomer]");
            decimal result = 0;
            try
            {
                string strAmount = DbEngine.ExecuteScalar(@"
                                    select round(sum(isnull(cl.receiptunappliedamt,0)) - sum(isnull(cl.receiptappliedamt,0)),2)  UnappliedAmount
                                    from BUSDTA.Customer_Ledger cl join busdta.Receipt_Header RH on cl.Receiptid=RH.Receiptid
                                    where RH.CustomerId=" + customerId + " and cl.isactive='1' ");
                if (!string.IsNullOrEmpty(strAmount))
                {
                    result = Convert.ToDecimal(strAmount);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetUnAppliedAmtForCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetUnAppliedAmtForCustomer]");
            return result;
        }
    
        /// <summary>
        /// Retrieves list of available reason code
        /// </summary>
        /// <returns>List of reason code</returns>
        public List<string> GetVoidReasonList()
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:GetVoidReasonList]");
            try
            {
                string strQuery = "SELECT ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster WHERE ReasonCodeType='Void Receipt'";
                DataTable dtVoidReasonList = DbEngine.ExecuteDataSet(strQuery, "VoidReasonList").Tables[0];
                return (from o in dtVoidReasonList.AsEnumerable()
                        select o.Field<string>("ReasonCodeDescription")).ToList<string>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][GetVoidReasonList][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            finally
            {
                Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:GetVoidReasonList]");
            }
        }

        /// <summary>
        /// Voids a specied receipt 
        /// </summary>
        /// <param name="receiptId">Receipt Id to be voided</param>
        /// <param name="reasonCodeId">Reason code provided for voiding receipt</param>
        public void VoidReceipt(string receiptId, string reasonCodeId, string routeId, bool reasonCodeTypeFlag )
        {
            string strQuery = "";
            string strVoidStatusId = "";
            string reasonCodeType = "";
            if (reasonCodeTypeFlag)
            {
                reasonCodeType = "Void Receipt";
            }
            else reasonCodeType = "Void Credit Memo";

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:VoidReceipt]");
            try
            {
                strVoidStatusId = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='VOID'");

                strQuery = string.Format("UPDATE BUSDTA.Receipt_Header SET StatusId=" + strVoidStatusId  + ", ReasonCodeId=(SELECT ReasonCodeId FROM BUSDTA.ReasonCodeMaster WHERE TRIM(ReasonCodeDescription)=TRIM('{0}') AND ReasonCodeType='"+reasonCodeType+"') WHERE CustomerId={1} AND ReceiptId={2} AND RouteId={3}", reasonCodeId, customerId, receiptId, routeId);
                DbEngine.ExecuteNonQuery(strQuery);

                strQuery = string.Format("UPDATE BUSDTA.Customer_Ledger SET IsActive='0' WHERE CustomerId={0} AND ReceiptId={1} AND RouteId={2}", customerId, receiptId, routeId);
                DbEngine.ExecuteNonQuery(strQuery);

                //ToDo - Update Invoice Status as Open 
                strQuery = string.Format("UPDATE BUSDTA.Invoice_Header SET DeviceStatusId=1,InvoicePaymentType='Credit' WHERE InvoiceId IN (SELECT InvoiceId FROM BUSDTA.Customer_Ledger WHERE CustomerId={0} AND ReceiptId={1} AND ISNULL(InvoiceId,0)<>0) AND RouteId={2}", customerId, receiptId, routeId);
                DbEngine.ExecuteNonQuery(strQuery);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateReceiptToVoid][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:VoidReceipt]");
        }

        /// <summary>
        /// Updates activities of the customer 
        /// </summary>
        /// <param name="activity">Activity object</param>
        public void UpdateReceiptActivity(Activity activity)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][Start:UpdateReceiptActivity]");
            string query = "";

            try
            {
                query = string.Format("UPDATE BUSDTA.M50012 SET TDDTLS='{0}', TDSTAT='{1}' WHERE TDID={2}", activity.ActivityDetails.Replace("'", "''"), activity.ActivityStatus, activity.ActivityID);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ARPaymentManager][UpdateReceiptActivity][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }

            Logger.Info("[SalesLogicExpress.Application.Managers][ARPaymentManager][End:UpdateReceiptActivity]");
        }

        #endregion

    }
}
