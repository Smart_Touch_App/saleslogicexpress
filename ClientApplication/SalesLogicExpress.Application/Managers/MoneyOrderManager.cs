﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System.Data;
using System.Globalization;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.ViewModelPayload;
using System.Data.SqlClient;
using iAnywhere;

namespace SalesLogicExpress.Application.Managers
{
    public class MoneyOrderManager
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.MoneyOrderManager");

        public ObservableCollection<MoneyOrder> GetMoneyOrderList()
        {
            ObservableCollection<MoneyOrder> moneyOrderList = new ObservableCollection<MoneyOrder>();
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:GetExpensesList]");

            try
            {
                string query = "select MO.MoneyOrderId, MO.RouteId, MO.MoneyOrderNumber, MO.MoneyOrderAmount, MO.MoneyOrderFeeAmount, MO.StatusId, MO.MoneyOrderDatetime, MO.VoidReasonId, MO.RouteSettlementId, ";
                query = query + " MO.UpdatedDatetime, ST.StatusTypeCD as StatusCode, ST.StatusTypeDESC as Status from BUSDTA.MoneyOrderDetails MO ";
                query = query + " join BUSDTA.Status_Type ST on ST.StatusTypeID = MO.StatusId ";
                query = query + " where MO.RouteId = '" + CommonNavInfo.RouteID + "'";
                query = query + " ORDER BY MO.UpdatedDatetime DESC";

                DataSet result = DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        DataRow row = result.Tables[0].Rows[i];
                        MoneyOrder order = new MoneyOrder();

                        order.MoneyOrderID = row["MoneyOrderId"].ToString().Trim();
                        order.RouteID = row["RouteId"].ToString().Trim();
                        order.MoneyOrderNumber = row["MoneyOrderNumber"].ToString().Trim();
                        order.MoneyOrderAmount = Convert.ToDecimal(row["MoneyOrderAmount"]).ToString("F2").Trim();
                        order.FeeAmount = row["MoneyOrderFeeAmount"].ToString() != null ? Convert.ToDecimal(row["MoneyOrderFeeAmount"]).ToString("F2").Trim() : "0.00";
                        order.TotalAmount = (Convert.ToDecimal(order.MoneyOrderAmount) + Convert.ToDecimal(order.FeeAmount)).ToString("F2");
                        order.StatusID = row["StatusId"].ToString().Trim();
                        order.StatusCode = row["StatusCode"].ToString().Trim();
                        order.Status = row["Status"].ToString().ToLower().ToUpper().Trim();
                        order.MoneyOrderDate = Convert.ToDateTime(row["UpdatedDatetime"]);
                        order.VoidReasonID = row["VoidReasonId"].ToString() != null ? row["VoidReasonId"].ToString().Trim() : string.Empty;
                        order.RouteSettlementId = row["RouteSettlementId"].ToString() != null ? row["RouteSettlementId"].ToString().Trim() : string.Empty;

                        moneyOrderList.Add(order);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][GetExpensesList][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetExpensesList]");

            return moneyOrderList;
        }
        public ObservableCollection<ReasonCode> GetVoidReasonCode()
        {
            ObservableCollection<ReasonCode> reasonCodeList = new ObservableCollection<ReasonCode>();
            Logger.Info("[SalesLogicExpress.Application.Managers][CreditProcessManager][Start:GetVoidReasonCode]");

            try
            {
                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void MoneyOrder'").Tables[0];

                ReasonCode ReasonCode = new ReasonCode();

                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString().Trim());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString().Trim();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][GetVoidReasonCode " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:GetVoidReasonCode]");

            return reasonCodeList;
        }
        public MoneyOrder AddMoneyOrder(MoneyOrder order)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][MoneyOrderManager][Start:AddMoneyOrder]");
            //Expenses exp = null;
            try
            {
                string query = "insert into BUSDTA.MoneyOrderDetails(MoneyOrderId, RouteId, MoneyOrderNumber, MoneyOrderAmount, MoneyOrderFeeAmount, StatusId, MoneyOrderDatetime, VoidReasonId, RouteSettlementId, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)";
                query = query + " VALUES((select isnull(max(MoneyOrderId),0)+1 from BUSDTA.MoneyOrderDetails), '" + CommonNavInfo.RouteID + "', '" + (string.IsNullOrEmpty(order.MoneyOrderNumber) ? "null" : order.MoneyOrderNumber) + "', '" + (string.IsNullOrEmpty(order.MoneyOrderAmount) ? "null" : order.MoneyOrderAmount) + "', ";
                query = query + " '" + (string.IsNullOrEmpty(order.FeeAmount) ? "null" : order.FeeAmount) + "', (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'USTLD'), ";
                query = query + " getdate(), " + (string.IsNullOrEmpty(order.VoidReasonID) ? "null" : order.VoidReasonID) + ", null, ";
                query = query + "'" + UserManager.UserId + "', getdate(), '" + UserManager.UserId + "', getdate())";

                try
                {
                    int result = DbEngine.ExecuteNonQuery(query);

                    if (result == 1)
                    {
                        query = "select top 1 MoneyOrderId, StatusId from BUSDTA.MoneyOrderDetails order by UpdatedDatetime DESC";
                        DataSet obj = DbEngine.ExecuteDataSet(query);
                        if (obj.HasData())
                        {
                            DataRow dr = obj.Tables[0].Rows[0];
                            //order = new MoneyOrder();
                            order.StatusID = dr["StatusId"].ToString();
                            order.MoneyOrderID = dr["MoneyOrderId"].ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][MoneyOrderManager][AddMoneyOrder][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                    order = null;
                    return order;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][MoneyOrderManager][AddMoneyOrder][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][MoneyOrderManager][End:AddMoneyOrder]");

            return order;
        }
        public int UpdateMoneyOrder(MoneyOrder order, bool flag)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][Start:UpdateExpenses]");
            int statusID = -1;
            try
            {
                string query = string.Empty;
                if (flag)
                {
                    query = "update BUSDTA.MoneyOrderDetails set MoneyOrderNumber = '" + order.MoneyOrderNumber + "', MoneyOrderAmount = '" + order.MoneyOrderAmount + "', MoneyOrderFeeAmount='" + order.FeeAmount + "', ";
                    query = query + " UpdatedDatetime = getdate() where RouteID = '" + order.RouteID + "' and MoneyOrderId = '" + order.MoneyOrderID + "'";

                }
                else
                {
                    query = "update BUSDTA.MoneyOrderDetails set StatusId = (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'VOID'), VoidReasonId = '" + order.VoidReasonID + "', ";
                    query = query + " UpdatedDatetime = getdate() where RouteID = '" + order.RouteID + "' and MoneyOrderId = '" + order.MoneyOrderID + "'";

                }

                int result = DbEngine.ExecuteNonQuery(query);

                if (result == 1)
                {
                    query = "select StatusId from BUSDTA.MoneyOrderDetails where RouteID = '" + order.RouteID + "' and MoneyOrderId = '" + order.MoneyOrderID + "'";
                    statusID = Convert.ToInt32(DbEngine.ExecuteScalar(query));

                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][ExpensesManager][UpdateExpenses][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][ExpensesManager][End:UpdateExpenses]");
            return statusID;
        }

        public Expenses UpdateExpense(MoneyOrder order, bool flag)
        {
            Expenses expense = new Expenses();
            Logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:UpdateExpense]");
            try
            {
                int result = -1;
                if (flag)
                {
                    result = DbEngine.ExecuteNonQuery("Update BUSDTA.ExpenseDetails set ExpenseAmount = '" + order.FeeAmount + "', ExpensesExplanation = 'Money Order Charges for " + order.MoneyOrderNumber + "',  UpdatedDatetime = getdate() where RouteID = '" + order.RouteID + "' and TransactionId = '" + order.MoneyOrderID + "'");
                }
                else
                    result = DbEngine.ExecuteNonQuery("Update BUSDTA.ExpenseDetails set StatusId = (select StatusTypeID from busdta.Status_Type where StatusTypeCD = 'VOID'), VoidReasonId = '" + order.VoidReasonID + "', UpdatedDatetime = getdate() where RouteID = '" + order.RouteID + "' and TransactionId = '" + order.MoneyOrderID + "'");

                if (result == 1)
                {
                    DataSet exp = DbEngine.ExecuteDataSet(string.Format("select ED.*, CT.CategoryTypeDESC as Category, ST.StatusTypeCD as StatusCode, ST.StatusTypeDESC as Status "
                     + " from BUSDTA.ExpenseDetails ED join BUSDTA.Category_Type CT on ED.CategoryId = CT.CategoryTypeID " +
                     " join BUSDTA.Status_Type ST on ST.StatusTypeID = ED.StatusId where ED.TransactionId = '" + order.MoneyOrderID + "'"));
                    if (exp.HasData())
                    {
                        DataRow dr = exp.Tables[0].Rows[0];
                        expense.ExpenseID = dr["ExpenseId"].ToString().Trim();
                        expense.RouteID = dr["RouteId"].ToString().Trim();
                        expense.CategotyID = dr["CategoryId"].ToString().Trim();
                        // expense.CategoryCode = string.IsNullOrEmpty(dr["CategoryCode"].ToString()) ? string.Empty : dr["CategoryCode"].ToString().Trim();
                        /// if (expense.CategoryCode.ToLower().ToUpper() == "MOCHR")
                        /// {
                        //     expense.IsMoneyOrderCharge = true;
                        // }
                        expense.ExpenseCategory = string.IsNullOrEmpty(dr["Category"].ToString()) ? string.Empty : dr["Category"].ToString().Trim();
                        expense.StatusID = dr["StatusId"].ToString().Trim();
                        expense.StatusCode = dr["StatusCode"].ToString().Trim();
                        expense.Status = dr["Status"].ToString().Trim().ToLower().ToUpper();
                        expense.VoidReasonID = string.IsNullOrEmpty(dr["VoidReasonId"].ToString()) ? string.Empty : dr["VoidReasonId"].ToString().Trim();
                        expense.ExpenseAmount = (Convert.ToDecimal(dr["ExpenseAmount"])).ToString("F2").Trim();
                        expense.ExpenseDate = Convert.ToDateTime(dr["UpdatedDatetime"]);
                        expense.Explanation = dr["ExpensesExplanation"].ToString().Trim();
                        expense.TransactionId = string.IsNullOrEmpty(dr["TransactionId"].ToString()) ? string.Empty : dr["TransactionId"].ToString();
                        expense.RouteSettlementId = string.IsNullOrEmpty(dr["RouteSettlementId"].ToString()) ? string.Empty : dr["RouteSettlementId"].ToString();

                    }
                }

            }
            catch (System.Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][UpdateExpense][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:UpdateExpense]");
            return expense;
        }

        public bool CheckMONumber(string MoneyOrderNumber, bool isEdit)
        {
            bool flag = true;
            try
            {
                string query = "SELECT mo.MoneyOrderNumber, st.StatusTypeCD FROM BUSDTA.MoneyOrderDetails mo join BUSDTA.Status_Type st on mo.StatusId = st.StatusTypeID where MoneyOrderNumber = '" + MoneyOrderNumber + "'";
                DataSet ds = DbEngine.ExecuteDataSet(query);
                if (ds.HasData())
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = ds.Tables[0].Rows[i];
                        if (!isEdit)
                        {
                            if ((dr["StatusTypeCD"].ToString() != "VOID") && (dr["StatusTypeCD"].ToString() != "VDSTLD") && (dr["StatusTypeCD"].ToString() != "USTLD"))
                            {
                                flag = false;
                                break;
                            }
                        }
                        else
                            if ((dr["StatusTypeCD"].ToString() != "VOID") && (dr["StatusTypeCD"].ToString() != "VDSTLD"))
                            {
                                flag = false;
                                break;
                            }
                    }

                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return flag;
        }
    }
}
