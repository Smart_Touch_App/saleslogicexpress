﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using System.Data;
namespace SalesLogicExpress.Application.Managers
{
    /// <summary>
    /// Scenario's to Handle
    /// 1. Normal Flow, Enter in pick screen and pick items, if overpicked put them in exception list.
    /// 2. Hold Pick
    /// 3. Void Pick
    /// 4. Navigate Back and Modify Pick List
    /// </summary>

    public class PickItemManager
    {
        #region Event Support at Pick item and Entire Pick Level
        public event EventHandler<PickItemEventArgs> PickItemUpdate;
        public event EventHandler<PickEventArgs> PickUpdate;
        protected virtual void OnItemPick(PickItemEventArgs e)
        {
            EventHandler<PickItemEventArgs> handler = PickItemUpdate;
            if (handler != null)
            {
                Delegate[] eventHandlers = handler.GetInvocationList();
                foreach (Delegate currentHandler in eventHandlers)
                {
                    EventHandler<PickItemEventArgs> currentSubscriber = (EventHandler<PickItemEventArgs>)currentHandler;
                    try
                    {
                        currentSubscriber(this, e);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
        protected virtual void OnPickStateChange(PickEventArgs e)
        {
            EventHandler<PickEventArgs> handler = PickUpdate;
            if (handler != null)
            {
                Delegate[] eventHandlers = handler.GetInvocationList();
                foreach (Delegate currentHandler in eventHandlers)
                {
                    EventHandler<PickEventArgs> currentSubscriber = (EventHandler<PickEventArgs>)currentHandler;
                    try
                    {
                        currentSubscriber(this, e);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
        public class PickItemEventArgs : EventArgs
        {
            public bool HasException { get; set; }
            public PickType Type { get; set; }
            public dynamic Item { get; set; }
            public PickItemState PickItemState { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        public class PickEventArgs : EventArgs
        {
            public bool HasExceptions { get; set; }
            public bool PickComplete { get; set; }
            public PickType Type { get; set; }
        }
        #endregion

        #region Enums for Type and States
        public enum PickItemState
        {
            [Description("1")]
            UnderPicked,
            [Description("2")]
            OverPicked,
            [Description("3")]
            IncorrectItem,
            [Description("4")]
            InvalidItemCode,
            [Description("5")]
            Short,
            [Description("6")]
            Adjusted,
            [Description("7")]
            Complete,
            [Description("8")]
            Valid,


        }
        public enum PickType
        {
            Normal,
            Exception
        }
        public enum PickMode
        {
            [Description("0")]
            Scanner,
            [Description("1")]
            Manual
        }
        public enum PickAdjustmentType
        {
            [Description("1")]
            Short,
            [Description("2")]
            Exception
        }
        public enum PickForTransaction
        {
            [Description("3")]
            Order,
            [Description("27")]
            CycleCount,
            [Description("23")]
            AddLoad,
            [Description("25")]
            Quote,
            [Description("24")]
            Suggestion,
            [Description("36")]
            ItemReturn,
            [Description("30")]
            SuggestionReturn,
            [Description("29")]
            HeldReturn,
            [Description("31")]
            Unload,            
            [Description("51")]
            QuotePick                     
            //[Description("29")]
            //SellableItemReturn
        }
        #endregion

        #region Properties and Fields
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.PickItemManager");
        string RouteID, TransactionID, TransactionTypeID;
        bool PickingFromTruck = true;
        bool UpdateCommittedQtyInInventoryOnFirstLoad = false;
        bool IsForVoid = false;
        //
        Dictionary<PickItemManager.PickType, ObservableCollection<dynamic>> ItemsForPick = new Dictionary<PickType, ObservableCollection<dynamic>>();
        List<dynamic> _PickItemList = new List<dynamic>();
        public ObservableCollection<dynamic> PickItemList = new ObservableCollection<dynamic>();
        ObservableCollection<dynamic> ItemExceptionList = new ObservableCollection<dynamic>();
        Func<ObservableCollection<dynamic>, List<dynamic>> SortPickItemsFunction;
        Func<ObservableCollection<dynamic>, List<dynamic>> SortPickItemsBySeqNoFunction;
        #endregion

        #region Private Methods
        void GetItemDetails(dynamic Item)
        {
            try
            {
                string query = "select * from busdta.F4101 where imitm='{0}'";
                query = string.Format(query, Item.ItemId);
                DataSet dsDetails = DbEngine.ExecuteDataSet(query);
                if (dsDetails.HasData())
                {
                    DataRow dr = dsDetails.Tables[0].Rows[0];
                    Item.ItemId = Item.ItemId.ToString().Trim();
                    Item.ItemNumber = dr["IMLITM"].ToString().Trim();
                    Item.ItemDescription = dr["IMDSC1"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][GetItemDetails][ExceptionStackTrace = " + ex.StackTrace + "][ItemId=" + Item.ItemId + "][Exception Message=" + ex.Message + "]");
            }
        }
        void SeperateList(List<dynamic> PickItems, List<dynamic> existingPickList)
        {

            foreach (dynamic item in PickItems)
            {
                dynamic currentItem = existingPickList.FirstOrDefault(refItem => refItem.ItemId == item.ItemId && refItem.TransactionUOM == item.TransactionUOM && refItem.TransactionDetailID == item.TransactionDetailID);
                if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                {
                    currentItem = existingPickList.FirstOrDefault(refItem => refItem.ItemId == item.ItemId && refItem.TransactionDetailID == item.TransactionDetailID);
                }
                if (currentItem != null)
                {
                    item.PickedQty = currentItem.PickedQty;
                    item.AdjustmentQty = currentItem.AdjustmentQty;
                    item.PickAdjusted = currentItem.PickAdjusted;
                    item.TransactionDetailID = currentItem.TransactionDetailID;
                    item.ItemScanSeq = currentItem.ItemScanSeq;
                    UpdateExceptionCount(item);
                    item.PickQtyPrimaryUOM = currentItem.PickedQty * item.ConversionFactor;
                    item.TransactionQtyPrimaryUOM = item.TransactionQty * item.ConversionFactor;
                    PickItemList.Add(item);
                }
                else
                {
                    PickItemList.Add(item);
                    AddToPickList(item);
                    UpdateExceptionCount(item);
                }
            }
            foreach (dynamic item in PickItems)
            {
                dynamic currentItem = existingPickList.FirstOrDefault(refItem => refItem.ItemId == item.ItemId && refItem.TransactionDetailID == item.TransactionDetailID && refItem.TransactionUOM == item.TransactionUOM);

                if (currentItem != null)
                {
                    // Check whether there was a change in UOM and check whether we have allready picked the item.
                    if (currentItem.TransactionQty == item.TransactionQty && currentItem.TransactionUOM != item.TransactionUOM)
                    {
                        SetConversionFactorForItem(currentItem);                        
                    }
                    // Check whether there was a change in quantity
                    if (currentItem.TransactionQty != item.TransactionQty && currentItem.TransactionUOM == item.TransactionUOM)
                    {
                        // if the quantity is decreased
                        if (currentItem.TransactionQty > item.TransactionQty)
                        {
                            // and a pick has already happened in the last iteration
                            if (currentItem.PickedQty > item.TransactionQty)
                            {
                                item.IsInException = true;
                                item.PickedQty = currentItem.PickedQty;
                                UpdateExceptionCount(item);
                                item.PickQtyPrimaryUOM = currentItem.PickedQty * item.ConversionFactor;
                                //item.ExceptionCount = Convert.ToInt32((currentItem.PickedQty - item.TransactionQty));
                                item.ExceptionCount = Convert.ToInt32((currentItem.PickedQty - (item.TransactionQty > (item.AdjustmentQty / item.ConversionFactor) ? item.TransactionQty : (item.AdjustmentQty / item.ConversionFactor))));
                                item.ExceptionReasonCodeID = PickItemState.OverPicked.GetEnumDescription();
                                ItemExceptionList.Add(item);
                            }
                        }
                        UpdatePickCount(item);
                    }
                    else if (currentItem.PickedQty > (item.TransactionQty > (item.AdjustmentQty / item.ConversionFactor) ? item.TransactionQty : (item.AdjustmentQty / item.ConversionFactor)) && currentItem.PickAdjusted == 0)
                    {
                        // TODO : Update the count properties for pick in exception for this item.
                        //item.ExceptionCount = Convert.ToInt32(currentItem.PickedQty - item.TransactionQty);
                        item.ExceptionCount = Convert.ToInt32(currentItem.PickedQty - (item.TransactionQty > (item.AdjustmentQty / item.ConversionFactor) ? item.TransactionQty : (item.AdjustmentQty / item.ConversionFactor)));

                        item.PickedQty = currentItem.PickedQty;
                        UpdateExceptionCount(item);
                        item.PickQtyPrimaryUOM = currentItem.PickedQty * item.ConversionFactor;
                        item.IsInException = true;
                        item.ExceptionReasonCodeID = PickItemState.OverPicked.GetEnumDescription();
                        ItemExceptionList.Add(item);
                    }
                }
            }
            //Incorrect item logic
            foreach (dynamic item in existingPickList)
            {
                Type unknown = item.GetType();
                // dynamic currentItem = Activator.CreateInstance(unknown);
                dynamic currentItem = PickItems.FirstOrDefault(refItem => refItem.ItemId == item.ItemId && refItem.TransactionDetailID == item.TransactionDetailID && refItem.TransactionUOM == item.TransactionUOM);

                if (currentItem == null)
                {
                    dynamic removeItem = PickItemList.FirstOrDefault(i => i.ItemId == item.ItemId && i.TransactionUOM == item.TransactionUOM);
                    if (removeItem != null)
                    {
                        PickItemList.Remove(removeItem);
                    }
                    // TODO : Update the count properties for pick in exception for this item.
                    item.ExceptionCount = Convert.ToInt32(item.PickedQty);
                    item.PrimaryUM = item.PrimaryUM;
                    item.UM = item.TransactionUOM;
                    item.IsInException = true;
                    item.ExceptionReasonCodeID = PickItemState.IncorrectItem.GetEnumDescription();
                    string itemNo = item.ExceptionReasonCodeID;
                    item.ExceptionReasonCode = itemNo.ParseEnumFromDescription<PickItemManager.PickItemState>().ToString();
                    GetItemDetails(item);
                    SetConversionFactorForItem(item);
                    if (item.PickedQty != 0)
                    {
                        ItemExceptionList.Add(item);
                    }
                }
            }

            int pickedItems = PickItemList.Count(items => items.TransactionQty <= items.PickedQty);
            bool pickComplete = pickedItems == PickItemList.Count ? true : false;
            int exceptionItems = PickItemList.Count(items => items.ExceptionCount > 0);
            if (pickComplete)
            {
                PickEventArgs pickArgs = new PickEventArgs();
                pickArgs.Type = PickType.Normal;
                pickArgs.HasExceptions = exceptionItems > 0 ? true : false;
                pickArgs.PickComplete = true;
                OnPickStateChange(pickArgs);
            }

            PickItemList.Sort(SortPickItemsFunction);//  SortPickItemsBySeqNoFunction


        }
        void SetConversionFactorForItem(dynamic item)
        {
            try
            {
                if (UoMManager.ItemUoMFactorList != null)
                {

                    var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == ((Item)item).ItemId.Trim()) && (x.FromUOM == ((Item)item).UM.ToString()) && (x.ToUOM == ((Item)item).PrimaryUM));
                    if (itemUomConversion == null)
                        item.ConversionFactor = UoMManager.GetUoMFactor(((Item)item).UM.ToString(), ((Item)item).PrimaryUM, Convert.ToInt32(((Item)item).ItemId.Trim()), ((Item)item).ItemNumber.Trim());
                    else
                        item.ConversionFactor = itemUomConversion.ConversionFactor;

                    item.TransactionQtyPrimaryUOM = item.TransactionQty * item.ConversionFactor;
                }
                else
                {
                    item.ConversionFactor = UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(((Item)item).ItemId.Trim()), ((Item)item).ItemNumber.Trim());
                    item.TransactionQtyPrimaryUOM = item.TransactionQty * item.ConversionFactor;
                }
            }
            catch (Exception ex)
            {

            }
        }
        void UpdatePickCount(dynamic Item)
        {
            try
            {
                string query = string.Empty;
                query = @"Update [BUSDTA].[PICK_DETAIL] set 
                                [IsInException]='{0}',
                                [ExceptionReasonCodeId]='{1}',
                                [PickedQty]='{2}',
                                [PickQtyPrimaryUOM]='{3}' ,
                                [TransactionQty]='{8}',
                                [TransactionQtyPrimaryUOM]='{9}',
                                [ManuallyPickCount]='{10}',
                                [ReasonCodeId] = '{11}',
                                [LastScanMode]='{12}',
                                [PickAdjusted]='{13}'
                                where RouteID='{4}' and TransactionID='{5}' and TransactionTypeID='{6}' and ItemID='{7}'  
                                and TransactionDetailID='{14}' AND TransactionUOM='{15}'";
                query = string.Format(query,
                    Convert.ToInt32(Item.IsInException),
                    Item.ExceptionReasonCodeID,
                    Item.PickedQty,
                    Item.PickQtyPrimaryUOM,
                    RouteID,
                    TransactionID,
                    TransactionTypeID,
                    Item.ItemId,
                    Item.TransactionQty,
                    Item.TransactionQtyPrimaryUOM,
                    Item.ManuallyPickCount,
                    Item.ReasonCodeId,
                    Item.LastScanMode,
                    Item.PickAdjusted,
                    Item.TransactionDetailID, Item.TransactionUOM);


                int cnt = DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][UpdatePickCount][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        void AddToPickList(List<dynamic> Items)
        {
            foreach (dynamic item in Items)
            {
                AddToPickList(item);
            }
        }
        void AddToPickList(dynamic Item)
        {
            string query = string.Empty;

            try
            {

                query = @"INSERT INTO [BUSDTA].[PICK_DETAIL]
                               ([RouteId]
                               ,[TransactionID]
                               ,[TransactionDetailID]
                               ,[TransactionTypeId]
                               ,[ItemID]
                               ,[TransactionQty]
                               ,[TransactionUOM]
                               ,[TransactionQtyPrimaryUOM]
                               ,[PrimaryUOM]
                               ,[PickQtyPrimaryUOM]
                               ,[LastScanMode]
                               ,[ItemScanSeq])
                                VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')";
                query = string.Format(query, RouteID, TransactionID, Item.TransactionDetailID, TransactionTypeID, Item.ItemId, Item.TransactionQty, Item.TransactionUOM, Item.TransactionQtyPrimaryUOM, Item.PrimaryUM, 0, PickMode.Scanner.GetEnumDescription(), Item.ItemScanSeq);

                if (Item.TransactionUOM!="loose")
                {
                    int cnt = DbEngine.ExecuteNonQuery(query);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][AddToPickList][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        void DeleteFromPickList(dynamic Item)
        {
            try
            {
                string query = @"DELETE FROM [BUSDTA].[PICK_DETAIL] WHERE 
                               [RouteId] = '{0}' AND 
                               [TransactionID]='{1}' AND 
                               [TransactionTypeId]='{2}' AND 
                               [ItemID]='{3}' and [TransactionDetailID]='{4}' AND TransactionUOM='{5}' ";
                query = string.Format(query, RouteID, TransactionID, TransactionTypeID, Item.ItemId, Item.TransactionDetailID, Item.TransactionUOM);
                int cnt=DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][DeleteFromPickList][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        List<dynamic> GetPickListForTransaction()
        {
            List<dynamic> PickItems = new List<dynamic>();
            try
            {
                string query = string.Empty;


                query = @"select DISTINCT DT.*,IM.IMITM as ItemId, IM.IMLITM as ItemNumber,IM.IMDSC1 as ItemDescription "+
                        " ,isnull(inventory.OnHandQuantity,0) as 'ActualQtyOnHand' " +
                        " ,isnull(inventory.CommittedQuantity,0) as 'CommittedQty' " +
                        " ,isnull(inventory.HeldQuantity,0) as 'HeldQty' " +
                        " ,(isnull(inventory.OnHandQuantity,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty' "+
                        " ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUM' "+
                        " from BUSDTA.PICK_DETAIL DT "+
                        " LEFT OUTER join BUSDTA.F4101 IM on DT.itemID=IM.IMITM " +
                        " left OUTER join busdta.Inventory inventory on DT.ItemId=inventory.ItemId " +
                        " left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID "+
                        " where DT.RouteId='{0}'  and TransactionID='{1}'  and TransactionTypeId='{2}' ORDER BY DT.ItemScanSeq ;";
                query = string.Format(query, RouteID, TransactionID, TransactionTypeID);

                DataSet dsPickitems = DbEngine.ExecuteDataSet(query);
                if (dsPickitems.HasData())
                {
                    foreach (DataRow row in dsPickitems.Tables[0].Rows)
                    {
                        dynamic obj = Activator.CreateInstance(itemType);
                        itemType.GetProperty("PickAdjusted").SetValue(obj, Convert.ToInt32(row["PickAdjusted"].ToString()), null);
                        itemType.GetProperty("AdjustmentQty").SetValue(obj, Convert.ToInt32(row["AdjustedQty"].ToString()), null);
                        itemType.GetProperty("ItemId").SetValue(obj, row["ItemId"].ToString(), null);
                        itemType.GetProperty("ActualQtyOnHand").SetValue(obj,Convert.ToInt32( row["ActualQtyOnHand"].ToString()), null);
                        itemType.GetProperty("CommittedQty").SetValue(obj, Convert.ToInt32(row["CommittedQty"].ToString()), null);
                        itemType.GetProperty("HeldQty").SetValue(obj, Convert.ToInt32(row["HeldQty"].ToString()), null);
                        itemType.GetProperty("AvailableQty").SetValue(obj, Convert.ToInt32(row["AvailableQty"].ToString()), null);
                        itemType.GetProperty("UM").SetValue(obj, row["TransactionUOM"].ToString(), null);
                        itemType.GetProperty("ItemDescription").SetValue(obj, row["ItemDescription"].ToString(), null);
                        itemType.GetProperty("ItemId").SetValue(obj, row["ItemId"].ToString(), null);
                        itemType.GetProperty("ItemNumber").SetValue(obj, row["ItemNumber"].ToString().Trim(), null);
                        itemType.GetProperty("ItemScanSeq").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["ItemScanSeq"].ToString()) ? "0" : row["ItemScanSeq"].ToString()), null);
                        itemType.GetProperty("LastScanMode").SetValue(obj, row["LastScanMode"].ToString(), null);
                        itemType.GetProperty("ManuallyPickCount").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["ManuallyPickCount"].ToString()) ? "0" : row["ManuallyPickCount"].ToString()), null);
                        itemType.GetProperty("PickQtyPrimaryUOM").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["PickQtyPrimaryUOM"].ToString()) ? "0" : row["PickQtyPrimaryUOM"].ToString()), null);
                        itemType.GetProperty("PickedQty").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["PickedQty"].ToString()) ? "0" : row["PickedQty"].ToString()), null);
                        itemType.GetProperty("ReasonCodeId").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["ReasonCodeId"].ToString()) ? "0" : row["ReasonCodeId"].ToString()), null);
                        itemType.GetProperty("PrimaryUM").SetValue(obj, row["PrimaryUOM"].ToString(), null);
                        itemType.GetProperty("RouteId").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["RouteId"].ToString()) ? "0" : row["RouteId"].ToString()), null);
                        itemType.GetProperty("TransactionID").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["TransactionID"].ToString()) ? "0" : row["TransactionID"].ToString()), null);
                        itemType.GetProperty("TransactionDetailID").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["TransactionDetailID"].ToString()) ? "0" : row["TransactionDetailID"].ToString()), null);
                        itemType.GetProperty("TransactionTypeId").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["TransactionTypeId"].ToString()) ? "0" : row["TransactionTypeId"].ToString()), null);
                        itemType.GetProperty("TransactionUOM").SetValue(obj, row["TransactionUOM"].ToString(), null);
                        itemType.GetProperty("TransactionStatusId").SetValue(obj, row["TransactionStatusId"].ToString(), null);
                        itemType.GetProperty("TransactionQtyPrimaryUOM").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["TransactionQtyPrimaryUOM"].ToString()) ? "0" : row["TransactionQtyPrimaryUOM"].ToString()), null);
                        itemType.GetProperty("TransactionQty").SetValue(obj, Convert.ToInt32(string.IsNullOrEmpty(row["TransactionQty"].ToString()) ? "0" : row["TransactionQty"].ToString()), null);
                        PickItems.Add(obj);
                    }
                }
                //PickItems = DbEngine.ExecuteDataSet(query).GetEntityList<PickItem>();
            }
            catch (Exception ex)
            {
                PickItems = null;
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][GetPickListForTransaction][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            return PickItems;

        }
        void UpdatePickListReference(dynamic Item)
        {
            dynamic itm = PickItemList.FirstOrDefault(i => i.ItemId == Item.ItemId && i.TransactionDetailID == Item.TransactionDetailID);

            if (itm == null)
            {
                PickItemList.Add(Item);
            }
            else
            {
                itm.ItemId = itm.ItemId; //TODO : Fix this 
                SetConversionFactorForItem(itm);
                itm.PickedQty = Item.PickedQty;
                itm.PickQtyPrimaryUOM = Item.PickedQty * itm.ConversionFactor;
                itm.ExceptionCount = Item.ExceptionCount;
                itm.IsInException = Item.IsInException;
                itm.ExceptionReasonCodeID = Item.ExceptionReasonCodeID;
            }
        }
        void UpdatePickExceptionListReference(dynamic Item)
        {
            dynamic itm = ItemExceptionList.FirstOrDefault(i => i.ItemId == Item.ItemId && i.TransactionDetailID == Item.TransactionDetailID);

            if (itm == null)
            {
                ItemExceptionList.Add(Item);
            }
            else
            {
                itm.ExceptionCount = Item.ExceptionCount;
            }
        }
        void UpdateExceptionCount(dynamic Item)
        {
            if (Item.PickedQty > (Item.TransactionQty > (Item.AdjustmentQty / Item.ConversionFactor) ? (Item.TransactionQty) : (Item.AdjustmentQty / Item.ConversionFactor)) && Item.PickAdjusted == 0)
            {
                Item.ExceptionCount = Convert.ToInt32(Item.PickedQty - (Item.TransactionQty > (Item.AdjustmentQty / Item.ConversionFactor) ? Item.TransactionQty : (Item.AdjustmentQty / Item.ConversionFactor)));
                Item.ExceptionReasonCodeID = PickItemManager.PickItemState.OverPicked.GetEnumDescription();
            }
            if (Item.PickedQty < (Item.TransactionQty > (Item.AdjustmentQty / Item.ConversionFactor) ? (Item.TransactionQty) : (Item.AdjustmentQty / Item.ConversionFactor)) && Item.PickAdjusted == 0)
            {
                Item.ExceptionCount = 0;
                Item.ExceptionReasonCodeID = PickItemManager.PickItemState.UnderPicked.GetEnumDescription();
            }
            if (Item.PickedQty == Item.TransactionQty && Item.PickAdjusted == 0)
            {
                Item.ExceptionCount = 0;
                Item.ExceptionReasonCodeID = PickItemManager.PickItemState.Complete.GetEnumDescription();
            }
            //Add Exception Adjusted
            if ((Item.PickedQty == (Item.AdjustmentQty / Item.ConversionFactor)) && Item.PickedQty > Item.TransactionQty)
            {
                Item.PickAdjusted = 2;
                Item.ExceptionReasonCodeID = PickItemManager.PickItemState.Adjusted.GetEnumDescription();
            }
            //Short Adjusted
            if ((Item.PickedQty == (Item.AdjustmentQty / Item.ConversionFactor)) && (Item.PickedQty < Item.TransactionQty) && Item.PickAdjusted == 1)
            {
                Item.ExceptionCount = 0;
                Item.PickAdjusted = 1;
                Item.AdjustmentQty = Item.PickQtyPrimaryUOM;
                Item.ExceptionReasonCodeID = PickItemManager.PickItemState.Short.GetEnumDescription();
            }

            //Incorrect Item Adjusted
            if ((Item.TransactionQty == 0) && (Item.PickedQty > 0))
            {
                Item.ExceptionCount = 0;
                Item.AdjustmentQty = Item.PickQtyPrimaryUOM;
                Item.ExceptionReasonCodeID = PickItemManager.PickItemState.IncorrectItem.GetEnumDescription();
            }
            if (!string.IsNullOrEmpty(Item.ExceptionReasonCodeID))
            {
                string IExceptionReasonCodeID = Item.ExceptionReasonCodeID;
                Item.ExceptionReasonCode = IExceptionReasonCodeID.ParseEnumFromDescription<PickItemManager.PickItemState>().ToString();
            }
            else
            {
                Item.ExceptionReasonCode = string.Empty;
            }
        }
        void UpdateItemSequence(dynamic Item)
        {
            try
            {
                bool pickCompleteForItem = Item.TransactionQty == Item.PickedQty ? true : false;
                string query = @"select  max([ItemScanSeq]) from [BUSDTA].[PICK_DETAIL] 
                                where RouteID='{1}' and TransactionID='{2}' and TransactionTypeID='{3}'";
                query = string.Format(query, RouteID, TransactionID, TransactionTypeID);
                int ScanSequence = Convert.ToInt32(DbEngine.ExecuteScalar(query));
                Item.ItemScanSeq = ScanSequence + 1;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][UpdateItemSequence][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        void UpdateException(PickItemEventArgs e)
        {
            try
            {
                if (e.Type == PickItemManager.PickType.Normal && e.PickItemState == PickItemManager.PickItemState.OverPicked)
                {
                    PickItem pickedItem = ItemExceptionList.FirstOrDefault(item => item.ItemId == e.Item.ItemId && item.TransactionDetailID == e.Item.TransactionDetailID);

                    if (pickedItem != null)
                    {
                        pickedItem.ExceptionCount = e.Item.ExceptionCount;
                        pickedItem.PickedQty = e.Item.PickedQty;
                    }
                    else
                    {
                        ItemExceptionList.Add(e.Item);
                    }
                }
                if (e.Type == PickItemManager.PickType.Exception && e.PickItemState == PickItemManager.PickItemState.Complete)
                {
                    int index = ItemExceptionList.IndexOf(e.Item);
                    if (index >= 0)
                    {
                        ItemExceptionList.RemoveAt(index);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][UpdateException][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        int PickedQuantity(dynamic PickedItem)
        {
            // TODO : 
            int quantityPicked = 1;

            return quantityPicked;
        }
        public void UpdateInventory(string itemID, double OnHandQuantity)
        {
            try
            {
                //Update on hand quantity
                string query = @"update busdta.Inventory set OnHandQuantity= '{0}'  where ItemId='{1}'";
                query = string.Format(query, OnHandQuantity, itemID);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][UpdateInventory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }

        void UpdateSequenceNo()
        {
            int id = 1;
            try
            {
               // PickItemList.Sort(SortPickItemsFunction);
                foreach (dynamic item in PickItemList)
                {
                    string updateQuery = "update BUSDTA.PICK_DETAIL a set a.ItemScanSeq ='{0}' where RouteID='{1}' and TransactionID='{2}' and TransactionTypeID='{3}' AND ItemID='{4}' and TransactionDetailID='{5}'";
                    updateQuery = string.Format(updateQuery, id, RouteID, TransactionID, TransactionTypeID, item.ItemId, item.TransactionDetailID);

                    int CNT = DbEngine.ExecuteNonQuery(updateQuery);
                    id++;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][UpdateSequenceNo][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }
        void UpdatePickAdjusted(dynamic PickItem)
        {
            try
            {
                string query = @"Update [BUSDTA].[PICK_DETAIL] set 
                                [PickAdjusted]='{0}',[AdjustedQty]='{5}'
                                where RouteID='{1}' and TransactionID='{2}' and TransactionTypeID='{3}' and ItemId='{4}' AND TransactionDetailID ='{6}'";
                query = string.Format(query, PickItem.PickAdjusted, RouteID, TransactionID, TransactionTypeID, PickItem.ItemId, PickItem.AdjustmentQty, PickItem.TransactionDetailID);
                DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][UpdatePickAdjusted][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        #endregion

        #region Public Methods
        Type itemType = null;
        public PickItemManager(string RouteID, string TransactionID, PickForTransaction TransactionTypeID,
            bool PickingFromTruck, bool UpdateCommittedQtyInInventoryOnFirstLoad = false, 
            bool IsForVoid = false)
        {
            try
            {
                this.RouteID = RouteID;
                this.TransactionID = TransactionID;
                this.TransactionTypeID = TransactionTypeID.GetEnumDescription();
                this.PickingFromTruck = PickingFromTruck;
                this.UpdateCommittedQtyInInventoryOnFirstLoad = UpdateCommittedQtyInInventoryOnFirstLoad;
                this.IsForVoid = IsForVoid;
                SortPickItemsBySeqNoFunction = (observable) =>
                {
                    //return observable.OrderBy(x => x.ItemScanSeq).ThenBy(x => x.ItemNumber).ToList();
                    return observable.OrderBy(x => x.ItemScanSeq).ToList();
                };
                SortPickItemsFunction = (observable) =>
                {
                    return observable.OrderBy(x => x.ExceptionReasonCodeID).ToList();
                    //return observable.OrderBy(x => x.ExceptionReasonCodeID).ThenBy(x => x.ItemNumber).ToList();
                };
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][Constructor][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        public Dictionary<PickType, ObservableCollection<dynamic>> CreatePickList(List<object> PickItems, Type itemTypeOfPickItems = null)
        {
            try
            {
                //if (PickItems == null || PickItems.Count == 0)
                //{
                //    ItemsForPick.Add(PickItemManager.PickType.Normal, PickItemList);
                //    ItemsForPick.Add(PickItemManager.PickType.Exception, ItemExceptionList);
                //    return ItemsForPick;
                //}
                /**********************************************************************************************************************************************/
                if (PickItems.Count > 0)
                {
                    itemType = PickItems[0].GetType();
                }
                else
                {
                    itemType = itemTypeOfPickItems;
                }
                foreach (dynamic item in PickItems)
                {
                    SetConversionFactorForItem(item);
                }
                /**********************************************************************************************************************************************/
                // Check whether there exists a list for the given  RouteID, TransactionID and TransactionTypeID.

                List<dynamic> existingPickList = GetPickListForTransaction();
                if (existingPickList.Count > 0)
                {
                    #region Possible Cases
                    // Possible seven cases
                    // 1. New Items are added in list
                    // 2. Existing Items are Removed from list
                    // 3. Few Items were removed and Same number of items were added.
                    // 4. Few or all existing Items are modified for OrderQty and OrderUOM and Price
                    // 5. Combination of 1 & 4
                    // 6. Combination of 2 & 4
                    // 7. Nothing Changed 
                    #endregion
                    SeperateList(PickItems, existingPickList);
                }
                else
                {
                    // If NO
                    //      1. Insert the items in the "PickItems" parameter into [BUSDTA].[PICK_DETAIL] table with default values
                    System.Console.WriteLine("Pick List does not exists");
                    System.Console.WriteLine("Insert the items in the PickItems parameter into [BUSDTA].[PICK_DETAIL] table with default values");
                    foreach (dynamic item in PickItems)
                    {
                        UpdateExceptionCount(item);
                        AddToPickList(item);

                        //Update Committed Qty. in inventory 
                        if (UpdateCommittedQtyInInventoryOnFirstLoad)
                        {
                            int transactionQtyInPrimaryUM = Convert.ToInt32(item.TransactionQtyPrimaryUOM);
                            int committedQty =  Convert.ToInt32(item.CommittedQty);
                            int heldQty = Convert.ToInt32( item.HeldQty);
                            int onHandQty =  Convert.ToInt32(item.ActualQtyOnHand);
                            committedQty = committedQty + transactionQtyInPrimaryUM;
                            item.CommittedQty = committedQty;
                            item.AvailableQty = onHandQty - (heldQty + committedQty);
                            new InventoryManager().UpdateCommittedQtyInTruckToBranchFlow(item.ItemId, transactionQtyInPrimaryUM, true);
                        }
                        PickItemList.Add(item);

                    }
                }
                //}
                // Return a Dictionary which contains both the items for PickitemList and Exceptions if any.
                /**********************************************************************************************************************************************/
                ItemsForPick.Add(PickItemManager.PickType.Normal, PickItemList);
                ItemsForPick.Add(PickItemManager.PickType.Exception, ItemExceptionList);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][CreatePickList][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            finally
            {
            }
            return ItemsForPick;
        }
        public async void PickItem(dynamic PickedItem)
        {
            await Task.Run(() =>
            {
                try
                {
                    
                    PickedItem.ExceptionMessage = "";
                    //You can not pick item which is short adjusted
                    if (PickedItem.ExceptionReasonCode == "Short")
                    {
                        return;
                    }

                    //  Update inventiry as per current pick
                    //  Cases for update inventory
                    //  1. manualqty> o && pickqty> manualqty && exceptionqty=0  =>update pickqty=manualqty and unpick item from which alreardy picked 
                    //  2. manualqty> o && pickqty< manualqty && exceptionqty=0
                    //  3. manualqty> o && pickqty> manualqty && exceptionqty>0 => means exception is present for item and you r entering manualqty < already ppick qty, then remove exception from exception list and set pickqty =manual pick

                    if (this.PickingFromTruck)
                    {
                        if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString()
                            || ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadPickView.ToString()
                            || ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnPickView.ToString())
                        {
                            int qty = Convert.ToInt32(PickedItem.HeldQty);
                            if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                            {
                                qty = Convert.ToInt32(PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? (PickedItem.HeldQty + (PickedItem.PickedQty * PickedItem.ConversionFactor)) - Convert.ToInt32((PickedItem.ManuallyPickCount) * PickedItem.ConversionFactor) : PickedItem.HeldQty - Convert.ToInt32(PickedItem.ConversionFactor));

                                if (qty < 0)
                                {
                                    PickedItem.ExceptionMessage = "Held Quantity is " + qty + ".";
                                    return;
                                }
                                PickedItem.HeldQty = qty;
                                int committedQty = PickedItem.ActualQtyOnHand - PickedItem.AvailableQty - qty;
                                PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = committedQty; y.HeldQty = qty; });
                            }
                            else
                            {
                                //CHECK FOR AVAILABLE QTY
                                int pickQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ManuallyPickCount : PickedItem.PickedQty + 1;
                                int pickQtyPrimary =Convert.ToInt32 (pickQty * PickedItem.ConversionFactor);
                                int transactionQtyPrimaryUOM = Convert.ToInt32(PickedItem.TransactionQtyPrimaryUOM);

                                if (PickedItem.ConversionFactor > PickedItem.AvailableQty && pickQtyPrimary > transactionQtyPrimaryUOM)
                                {
                                    return;
                                }
                                // Update AdjustmentQty=0 if picking already adjusted item
                                if ((PickedItem.ManuallyPickCount * PickedItem.ConversionFactor) < PickedItem.AdjustmentQty && (PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking))
                                {
                                    PickedItem.AdjustmentQty = 0;
                                }
                                UpdatePickAdjusted(PickedItem);

                                int previousPickQtyPrimaryUOM = Convert.ToInt32(PickedItem.PickQtyPrimaryUOM);
                                int AdjustmentQty = Convert.ToInt32(PickedItem.AdjustmentQty);

                                PickedItem.PickedQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ManuallyPickCount : PickedItem.PickedQty + 1;
                                PickedItem.PickQtyPrimaryUOM = PickedItem.PickedQty * PickedItem.ConversionFactor;
                                int newPickQtyPrimaryUOM = Convert.ToInt32(PickedItem.PickQtyPrimaryUOM);
                                //Over Picking by Scanner E.G. TRANSACTIONQTY=5, PICKEDQTY=8, NOW PICKING 1 BY SCANNER
                                if (newPickQtyPrimaryUOM > (transactionQtyPrimaryUOM > AdjustmentQty ? transactionQtyPrimaryUOM : AdjustmentQty) )
                                {
                                    //Increase Committed Qty by 1
                                    new InventoryManager().UpdateCommittedQtyInTruckToBranchFlow(PickedItem.ItemId.ToString(), 1, true);
                                    int newCommittedQty = PickedItem.CommittedQty + 1;
                                    int newAvailableQty = PickedItem.ActualQtyOnHand - (PickedItem.HeldQty + newCommittedQty);
                                    PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });   

                                }
                                //Over Picking ADUJSTMENT BY  Manual E.G. TRANSACTIONQTY=5, PICKEDQTY=8, NOW PICKING 5 OR <5 BY MANUAL PICK
                                else if (newPickQtyPrimaryUOM <= (transactionQtyPrimaryUOM > AdjustmentQty ? transactionQtyPrimaryUOM : AdjustmentQty) && previousPickQtyPrimaryUOM > (transactionQtyPrimaryUOM > AdjustmentQty ? transactionQtyPrimaryUOM : AdjustmentQty))
                                {
                                    //Decrease Committed Qty 
                                    int qtyToDecrease = previousPickQtyPrimaryUOM - (transactionQtyPrimaryUOM > AdjustmentQty ? transactionQtyPrimaryUOM : AdjustmentQty);
                                    new InventoryManager().UpdateCommittedQtyInTruckToBranchFlow(PickedItem.ItemId.ToString(), qtyToDecrease, false );
                                    int newCommittedQty = PickedItem.CommittedQty -qtyToDecrease;
                                    int newAvailableQty = PickedItem.ActualQtyOnHand - (PickedItem.HeldQty + newCommittedQty);
                                    PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });   

                                }
                                
                                //int availableQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.AvailableQty - Convert.ToInt32((PickedItem.ManuallyPickCount - PickedItem.PickedQty) * PickedItem.ConversionFactor) : PickedItem.AvailableQty - Convert.ToInt32(PickedItem.ConversionFactor);
                                //if (availableQty < 0)
                                //{
                                //    PickedItem.ExceptionMessage = "Available  Quantity is " + availableQty + ".";
                                //    return;
                                //}
                                //int committedQty = PickedItem.ActualQtyOnHand - availableQty ;                                
                                //PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = committedQty; y.AvailableQty = availableQty; });                                
                            }
                        }
                        else
                        {
                            int availqty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.AvailableQty - Convert.ToInt32((PickedItem.ManuallyPickCount - PickedItem.PickedQty) * PickedItem.ConversionFactor) : PickedItem.AvailableQty - Convert.ToInt32(PickedItem.ConversionFactor);
                            // Here return if available qty is 0 for handling negative values
                            if (availqty < 0)
                            {
                                PickedItem.ExceptionMessage = "Available  Quantity is " + availqty + ".";
                                return;
                            }
                            PickedItem.AvailableQty = availqty;
                            int onhandQty= PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ActualQtyOnHand - Convert.ToInt32((PickedItem.ManuallyPickCount - PickedItem.PickedQty) * PickedItem.ConversionFactor) : PickedItem.ActualQtyOnHand - Convert.ToInt32(PickedItem.ConversionFactor);
                            PickedItem.ActualQtyOnHand = onhandQty;
                            PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });
                            ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });

                        }
                    }
                    else
                    {
                        int onhandQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ActualQtyOnHand + Convert.ToInt32((PickedItem.ManuallyPickCount - PickedItem.PickedQty) * PickedItem.ConversionFactor) : PickedItem.ActualQtyOnHand + Convert.ToInt32(PickedItem.ConversionFactor);
                        PickedItem.ActualQtyOnHand = onhandQty;

                        int availqty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.AvailableQty + Convert.ToInt32((PickedItem.ManuallyPickCount - PickedItem.PickedQty) * PickedItem.ConversionFactor) : PickedItem.AvailableQty + Convert.ToInt32(PickedItem.ConversionFactor);
                        PickedItem.AvailableQty = availqty;

                        PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });
                        ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });
                    }

                   

                    if (!UpdateCommittedQtyInInventoryOnFirstLoad)
                    {
                        // Update AdjustmentQty=0 if picking already adjusted item
                        if ((PickedItem.ManuallyPickCount * PickedItem.ConversionFactor) < PickedItem.AdjustmentQty && (PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking))
                        {
                            PickedItem.AdjustmentQty = 0;
                        }
                        UpdatePickAdjusted(PickedItem);

                        PickedItem.PickedQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ManuallyPickCount : PickedItem.PickedQty + 1;
                        PickedItem.PickQtyPrimaryUOM = PickedItem.PickedQty * PickedItem.ConversionFactor;
                    }

                    PickedItem.LastScanMode = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickMode.Manual.GetEnumDescription() : PickMode.Scanner.GetEnumDescription();

                    UpdateExceptionCount(PickedItem);
                    //End

                    

                    // Check before insert,
                    // if the item count equals TransactionQty then insert record with the isInException property to true and ExceptionReasonCodeId to "OverPicked"
                    // else Update/increment the Pick count here for the given dynamic
                    bool overPicked = ((PickedItem.TransactionQty > (PickedItem.AdjustmentQty / PickedItem.ConversionFactor) ? PickedItem.TransactionQty : (PickedItem.AdjustmentQty / PickedItem.ConversionFactor)) < PickedItem.PickedQty) ? true : false;
                    bool pickCompleteForItem = PickedItem.TransactionQty == PickedItem.PickedQty ? true : false;
                    PickedItem.IsInException = overPicked ? true : false;
                    //PickedItem.ExceptionCount = overPicked ? Convert.ToInt32(PickedItem.PickedQty - PickedItem.TransactionQty) : PickedItem.ExceptionCount;
                    //PickedItem.ExceptionReasonCodeID = pickCompleteForItem ? PickItemState.Complete.GetEnumDescription() : (overPicked ? PickItemState.OverPicked.GetEnumDescription() : PickItemState.Valid.GetEnumDescription());
                    bool pickExceptionCompleted = PickedItem.ExceptionCount == 0 ? true : false;
                    if (pickExceptionCompleted)
                    {
                        ItemExceptionList.Remove(PickedItem);
                    }
                    UpdatePickCount(PickedItem);
                    UpdatePickListReference(PickedItem);

                    // Raise events
                    #region dynamic Events
                    PickItemEventArgs args = new PickItemEventArgs();
                    args.Type = PickType.Normal;
                    args.PickItemState = PickItemState.Valid;
                    if (overPicked)
                    {
                        // This will cause the  PickedItem.ExceptionCount to increment
                        //PickedItem.ExceptionCount++;
                        args.PickItemState = PickItemState.OverPicked;
                    }
                    args.HasException = PickedItem.ExceptionCount > 0 ? true : false;
                    args.Item = PickedItem;
                    OnItemPick(args);
                    UpdateException(args);
                    #endregion

                    #region Pick Level Events
                    int pickedItems = PickItemList.Count(items => items.TransactionQty <= items.PickedQty);
                    bool pickComplete = pickedItems == PickItemList.Count ? true : false;
                    int exceptionItems = PickItemList.Count(items => items.ExceptionCount > 0);
                    if (pickComplete)
                    {
                        PickEventArgs pickArgs = new PickEventArgs();
                        pickArgs.Type = PickType.Normal;
                        pickArgs.HasExceptions = exceptionItems > 0 ? true : false;
                        pickArgs.PickComplete = true;
                        OnPickStateChange(pickArgs);
                    }
                    #endregion


                   
                    PickItemList.Sort(SortPickItemsFunction);
                    UpdateSequenceNo();
                    PickedItem.ManuallyPickCount = 0;

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][CreatePickList][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
            });

        }
        public async void UnPickItem(dynamic PickedItem)
        {
            await Task.Run(() =>
            {
                try
                {
                    
                    PickedItem.ExceptionMessage = "";

                    if (this.PickingFromTruck)
                    {

                        if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString()
                            || ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadPickView.ToString()
                            || ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnPickView.ToString())
                        {
                            int qty = Convert.ToInt32(PickedItem.HeldQty);
                            if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnPickView.ToString())
                            {
                                qty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.HeldQty + Convert.ToInt32((PickedItem.ManuallyPickCount) * PickedItem.ConversionFactor) : PickedItem.HeldQty + Convert.ToInt32(PickedItem.ConversionFactor);
                                // Here return if available qty is 0 for handling negative values
                                if (qty < 0)
                                {
                                    PickedItem.ExceptionMessage = "Held Quantity is " + qty + ".";
                                    return;
                                }
                                PickedItem.HeldQty = qty;
                                int committedQty = PickedItem.ActualQtyOnHand - PickedItem.AvailableQty - qty;

                                PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = committedQty; y.HeldQty = qty; });

                                ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = committedQty; y.HeldQty = qty; });
                            }
                            else
                            {
                                int AdjustmentQty = Convert.ToInt32(PickedItem.AdjustmentQty);
                                int previousPickQtyPrimaryUOM = Convert.ToInt32(PickedItem.PickQtyPrimaryUOM);
                                int transactionQtyPrimaryUOM = Convert.ToInt32(PickedItem.TransactionQtyPrimaryUOM);
                                PickedItem.PickedQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.PickedQty - PickedItem.ManuallyPickCount : PickedItem.PickedQty - 1;
                                PickedItem.PickQtyPrimaryUOM = PickedItem.PickedQty * PickedItem.ConversionFactor;
                                int newPickQtyPrimaryUOM = Convert.ToInt32(PickedItem.PickQtyPrimaryUOM);
                                
                                //Decrease Committed Qty 
                                int qtyToDecrease = previousPickQtyPrimaryUOM - (newPickQtyPrimaryUOM);
                                new InventoryManager().UpdateCommittedQtyInTruckToBranchFlow(PickedItem.ItemId.ToString(), qtyToDecrease, false);
                                int newCommittedQty = PickedItem.CommittedQty - qtyToDecrease;
                                int newAvailableQty = PickedItem.ActualQtyOnHand - (PickedItem.HeldQty + newCommittedQty);
                                PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });
                                ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });

                                ////PICKED from exception in normal flow
                                //if (newPickQtyPrimaryUOM > transactionQtyPrimaryUOM && previousPickQtyPrimaryUOM > transactionQtyPrimaryUOM && !IsForVoid)
                                //{
                                //    //Decrease Committed Qty 
                                //    int qtyToDecrease = previousPickQtyPrimaryUOM - transactionQtyPrimaryUOM;
                                //    new InventoryManager().UpdateCommittedQtyInTruckToBranchFlow(PickedItem.ItemId.ToString(), qtyToDecrease, false);
                                //    int newCommittedQty = PickedItem.CommittedQty - qtyToDecrease;
                                //    int newAvailableQty = PickedItem.ActualQtyOnHand - (PickedItem.HeldQty + PickedItem.CommittedQty);
                                //    PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });
                                //    ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });

                                //}
                                ////Picking from Excepion for Void
                                //else if ( IsForVoid)
                                //{
                                //    //Decrease Committed Qty 
                                //    int qtyToDecrease = previousPickQtyPrimaryUOM - transactionQtyPrimaryUOM;
                                //    new InventoryManager().UpdateCommittedQtyInTruckToBranchFlow(PickedItem.ItemId.ToString(), qtyToDecrease, false);
                                //    int newCommittedQty = PickedItem.CommittedQty - qtyToDecrease;
                                //    int newAvailableQty = PickedItem.ActualQtyOnHand - (PickedItem.HeldQty + PickedItem.CommittedQty);
                                //    PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });
                                //    ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });

                                //}
                                

                                //int availableQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.AvailableQty + Convert.ToInt32((PickedItem.ManuallyPickCount) * PickedItem.ConversionFactor) : PickedItem.AvailableQty + Convert.ToInt32(PickedItem.ConversionFactor);
                                //if (availableQty < 0)
                                //{
                                //    PickedItem.ExceptionMessage = "Available Quantity is " + availableQty + ".";
                                //    return;
                                //}
                                //int committedQty = PickedItem.ActualQtyOnHand - availableQty ;
                                //PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = committedQty; y.AvailableQty = availableQty; });

                                //ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = committedQty; y.AvailableQty = availableQty; });

                            }

                        }
                        else
                        {
                            int availqty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.AvailableQty + Convert.ToInt32((PickedItem.ManuallyPickCount) * PickedItem.ConversionFactor) : PickedItem.AvailableQty + Convert.ToInt32(PickedItem.ConversionFactor);
                            // Here return if available qty is 0 for handling negative values
                            if (availqty < 0)
                            {
                                PickedItem.ExceptionMessage = "Available Quantity is " + availqty + ".";
                                return;
                            }
                            PickedItem.AvailableQty = availqty;
                            int onhandQty=PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ActualQtyOnHand + Convert.ToInt32((PickedItem.ManuallyPickCount) * PickedItem.ConversionFactor) : PickedItem.ActualQtyOnHand + Convert.ToInt32(PickedItem.ConversionFactor);
                            PickedItem.ActualQtyOnHand = onhandQty;

                            PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });
                            ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });
                   
                        }

                    }
                    else
                    {
                        int availqty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.AvailableQty - Convert.ToInt32((PickedItem.ManuallyPickCount) * PickedItem.ConversionFactor) : PickedItem.AvailableQty - Convert.ToInt32(PickedItem.ConversionFactor);
                        if (availqty < 0)
                        {
                            PickedItem.ExceptionMessage = "Available Quantity is " + availqty + ".";
                            return;
                        }
                        PickedItem.AvailableQty = availqty;
                        int onhandQty= PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ActualQtyOnHand - Convert.ToInt32((PickedItem.ManuallyPickCount) * PickedItem.ConversionFactor) : PickedItem.ActualQtyOnHand - Convert.ToInt32(PickedItem.ConversionFactor);
                        PickedItem.ActualQtyOnHand = onhandQty;
                        PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });
                        ItemExceptionList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.ActualQtyOnHand = onhandQty; y.AvailableQty = availqty; });
                    }
                    if (!UpdateCommittedQtyInInventoryOnFirstLoad)
                    {
                        PickedItem.PickedQty = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.PickedQty - PickedItem.ManuallyPickCount : PickedItem.PickedQty - 1;
                        PickedItem.PickQtyPrimaryUOM = PickedItem.PickedQty * PickedItem.ConversionFactor;
                    }
                    

                  
                    string exceptionID = PickedItem.ExceptionReasonCodeID;
                    if (exceptionID != PickItemState.IncorrectItem.GetEnumDescription())
                    {
                        UpdateExceptionCount(PickedItem);
                        bool pickExceptionCompleted = PickedItem.ExceptionCount == 0 ? true : false;
                        PickedItem.IsInException = pickExceptionCompleted ? false : true;
                        if (pickExceptionCompleted && exceptionID == PickItemState.OverPicked.GetEnumDescription())
                        {
                            PickedItem.ExceptionReasonCodeID = pickExceptionCompleted ? PickItemState.Complete.GetEnumDescription() : PickItemState.OverPicked.GetEnumDescription();
                            UpdatePickListReference(PickedItem);
                        }
                        UpdatePickExceptionListReference(PickedItem);
                        UpdatePickCount(PickedItem);
                    }
                    else
                    {
                        //UpdateExceptionCount(PickedItem);

                        PickedItem.ExceptionCount = PickedItem.ManuallyPickCount >= 0 && PickedItem.IsManuallyPicking ? PickedItem.ExceptionCount - PickedItem.ManuallyPickCount : PickedItem.ExceptionCount - 1;

                        bool pickExceptionCompleted = PickedItem.ExceptionCount == 0 ? true : false;
                        PickedItem.IsInException = pickExceptionCompleted ? false : true;
                        PickedItem.ExceptionReasonCodeID = pickExceptionCompleted ? PickItemState.Complete.GetEnumDescription() : PickItemState.IncorrectItem.GetEnumDescription();
                        string excptCode = PickedItem.ExceptionReasonCodeID;
                        PickedItem.ExceptionReasonCode = excptCode.ParseEnumFromDescription<PickItemManager.PickItemState>().ToString();
                        //PickedItem.PickedQty = pickExceptionCompleted ? 0 : PickedItem.PickedQty;
                        UpdatePickCount(PickedItem);
                        dynamic itm = ItemExceptionList.FirstOrDefault(i => i.ItemId == PickedItem.ItemId && i.TransactionDetailID == PickedItem.TransactionDetailID);

                        if (itm != null)
                        {
                            itm.ExceptionCount = PickedItem.ExceptionCount;
                        }
                        if (pickExceptionCompleted)
                        {
                            DeleteFromPickList(PickedItem);
                            ItemExceptionList.Remove(itm);
                        }
                    }
                    exceptionID = PickedItem.ExceptionReasonCodeID;

                    #region Pick Item Level Events
                    PickItemEventArgs args = new PickItemEventArgs();
                    args.Type = PickType.Exception;
                    args.Item = PickedItem;
                    args.PickItemState = exceptionID.ParseEnumFromDescription<PickItemState>();
                    OnItemPick(args);
                    UpdateException(args);

                    #endregion

                    #region Pick Level Events
                    int pickedItems = ItemExceptionList.Count(items => items.ExceptionCount == 0);
                    bool pickComplete = pickedItems == ItemExceptionList.Count ? true : false;
                    if (pickComplete)
                    {
                        PickEventArgs pickArgs = new PickEventArgs();
                        pickArgs.Type = PickType.Exception;
                        pickArgs.PickComplete = true;
                        OnPickStateChange(pickArgs);
                    }
                    #endregion

                    

                    PickItemList.Sort(SortPickItemsFunction);
                    UpdateSequenceNo();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][UnPickItem][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
            });

        }
        public void HoldPick(bool Hold)
        {
            try
            {


                foreach (dynamic item in PickItemList)
                {
                    string query = @"Update [BUSDTA].[PICK_DETAIL] set 
                                [IsOnHold]='{0}'
                               ,[PickedQty] = '{1}'
                                where RouteID='{2}' and TransactionID='{3}' and TransactionTypeID='{4}'  and ItemID = '{5}' and TransactionDetailID = '{6}'";
                    query = string.Format(query, Convert.ToInt32(Hold), item.PickedQty, RouteID, TransactionID, TransactionTypeID, item.ItemId, item.TransactionDetailID);
                   int cnt= DbEngine.ExecuteNonQuery(query);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][HoldPick][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        public void AdjustPick(dynamic PickedItem, PickAdjustmentType AdjustmentType)
        {
            try
            {
                // if PickAdjusted=1 then Short Adjustment. if PickAdjusted=2 then Add exception Adjustment 
                PickedItem.PickAdjusted = Convert.ToInt32(AdjustmentType.GetEnumDescription());

                //If short then adjust committed qty
                if (UpdateCommittedQtyInInventoryOnFirstLoad && PickedItem.PickAdjusted==1)
                {
                    int transactionQtyPrimaryUOM = Convert.ToInt32(PickedItem.TransactionQtyPrimaryUOM);
                    int newPickQtyPrimaryUOM = Convert.ToInt32(PickedItem.PickQtyPrimaryUOM);

                    //Decrease Committed Qty 
                    int qtyToDecrease = transactionQtyPrimaryUOM - (newPickQtyPrimaryUOM);
                    new InventoryManager().UpdateCommittedQtyInTruckToBranchFlow(PickedItem.ItemId.ToString(), qtyToDecrease, false);
                    int newCommittedQty = PickedItem.CommittedQty - qtyToDecrease;
                    int newAvailableQty = PickedItem.ActualQtyOnHand - (PickedItem.HeldQty + newCommittedQty);
                    PickItemList.Where(x => x.ItemId == PickedItem.ItemId).ToList().ForEach(y => { y.CommittedQty = newCommittedQty; y.AvailableQty = newAvailableQty; });

                }
                
                PickedItem.IsEnableScanner = (AdjustmentType == PickAdjustmentType.Short) ? false : true;
                //Remove item from exception list
                ItemExceptionList.Remove(PickedItem);

                PickedItem.ExceptionCount = 0;
                PickedItem.AdjustmentQty = PickedItem.PickQtyPrimaryUOM;
                //Sort
                UpdateExceptionCount(PickedItem);

                //Update in DB
                UpdatePickAdjusted(PickedItem);


                PickItemList.Sort(SortPickItemsFunction);
                UpdateSequenceNo();

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][AdjustPick][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][ItemId=" + PickedItem.ItemId + "][Exception Message=" + ex.Message + "]");
            }
        }
        public void VoidPick(string ReasonCodeID)
        {
            try
            {
                string ExceptionReasonCodeID = PickItemState.IncorrectItem.GetEnumDescription();
                foreach (dynamic item in PickItemList)
                {
                    if (item.PickedQty > 0)
                    {
                        PickItem currentItem = ItemExceptionList.FirstOrDefault(i => i.ItemId == item.ItemId && i.TransactionDetailID == item.TransactionDetailID);

                        if (currentItem != null)
                        {
                            currentItem.ExceptionCount = currentItem.PickedQty;
                            currentItem.ExceptionReasonCodeID = ExceptionReasonCodeID;
                            currentItem.IsInException = true;
                            currentItem.ExceptionReasonCode = ExceptionReasonCodeID.ParseEnumFromDescription<PickItemManager.PickItemState>().ToString();
                            currentItem.IsUnPickForVoid = true;
                        }
                        else
                        {
                            item.ExceptionCount = item.PickedQty;
                            item.ExceptionReasonCodeID = ExceptionReasonCodeID;
                            item.IsInException = true;
                            item.ExceptionReasonCode = ExceptionReasonCodeID.ParseEnumFromDescription<PickItemManager.PickItemState>().ToString();
                            item.IsUnPickForVoid = true;
                            ItemExceptionList.Add(item);
                        }
                    }
                }
                PickItemList.Clear();
                //PickItemList
                //ItemExceptionList
                string query = @"Update [BUSDTA].[PICK_DETAIL] set 
                                [ReasonCodeId]='{0}'
                                where RouteID='{1}' and TransactionID='{2}' and TransactionTypeID='{3}'";

                query = string.Format(query, ReasonCodeID, RouteID, TransactionID, TransactionTypeID);
                int cnt = DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][PickItemManager][VoidPick][RouteID=" + RouteID + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }
        public dynamic CreateAndAdd(int transactionId)
        {
            try
            {
                var query = @" select top 1 im.imitm as ItemID, im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMLNTY as StkType, 
                                im.IMSRP1  as SalesCat1, im.IMSRP5 as SalesCat5, im.IMSRP4 as SalesCat4, 
                                iconfig.PrimaryUM as PrimaryUM,iconfig.TransactionUM as TransactionUM 
                                FROM busdta.F4101 im join busdta.ItemConfiguration iconfig on im.IMITM=iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 order by newid()";
                DataRow dataRow = DbEngine.ExecuteDataSet(query).Tables[0].Rows[0];
                var pickObj = new CycleCountItems();
                pickObj.ItemId = string.IsNullOrEmpty(dataRow["ItemID"].ToString().Trim()) ? string.Empty : dataRow["ItemID"].ToString().Trim();
                pickObj.ItemNumber = string.IsNullOrEmpty(dataRow["ItemNumber"].ToString().Trim()) ? string.Empty : dataRow["ItemNumber"].ToString().Trim();
                pickObj.ItemDescription = string.IsNullOrEmpty(dataRow["ItemDescription"].ToString().Trim()) ? string.Empty : dataRow["ItemDescription"].ToString().Trim();
                pickObj.StkType = string.IsNullOrEmpty(dataRow["StkType"].ToString().Trim()) ? string.Empty : dataRow["StkType"].ToString().Trim();
                pickObj.SalesCat1 = string.IsNullOrEmpty(dataRow["SalesCat1"].ToString().Trim()) ? string.Empty : dataRow["SalesCat1"].ToString().Trim();
                pickObj.SalesCat5 = string.IsNullOrEmpty(dataRow["SalesCat5"].ToString().Trim()) ? string.Empty : dataRow["SalesCat5"].ToString().Trim();
                pickObj.PrimaryUM = string.IsNullOrEmpty(dataRow["PrimaryUM"].ToString().Trim()) ? string.Empty : dataRow["PrimaryUM"].ToString().Trim();
                pickObj.TransactionUOM = string.IsNullOrEmpty(dataRow["TransactionUM"].ToString().Trim()) ? string.Empty : dataRow["TransactionUM"].ToString().Trim();
                pickObj.TransactionID = transactionId;
                pickObj.TransactionTypeId = Convert.ToInt16(TransactionTypeID);
                pickObj.TransactionQty = 9999;
                pickObj.TransactionQtyPrimaryUOM = 9999;
                int cnt = DbEngine.ExecuteNonQuery(@"INSERT INTO BUSDTA.PICK_DETAIL (RouteId,TransactionID,TransactionDetailID,TransactionTypeId,TransactionStatusId,ItemID,TransactionQty,
                                           TransactionUOM,TransactionQtyPrimaryUOM,PrimaryUOM,PickedQty,PickQtyPrimaryUOM,LastScanMode,ItemScanSeq,IsOnHold,PickAdjusted,
                                            ReasonCodeId,ManuallyPickCount,IsInException,ExceptionReasonCodeId,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)
                                            VALUES(" + RouteID + "," + pickObj.TransactionID + "," + "0 ," +
                                           " " + pickObj.TransactionTypeId + ",NULL,'" + pickObj.ItemId + "',9999,'" + pickObj.TransactionUOM + "'," +
                                           "9999,'" + pickObj.PrimaryUM + "',0,0,0,0,NULL,0,0,0,'0',0," + UserManager.UserId + ",getdate()," + UserManager.UserId + ",getdate())");
                return pickObj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
