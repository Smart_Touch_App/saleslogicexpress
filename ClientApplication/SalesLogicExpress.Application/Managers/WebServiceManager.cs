﻿using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using SalesLogicExpress.Application.SLEStatService;
using SalesLogicExpress.Application.ViewModels;
namespace SalesLogicExpress.Application.Managers
{
    public class WebServiceManager
    {
        public readonly ILog logger = LogManager.GetLogger("WebServiceLogger");
        //public readonly ILog logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.WebServiceManager");
        public bool UseAppService { get; set; }
        public event EventHandler<ServiceStateChangeEventArgs> ServiceStateChanged;

        public Rq RqObj;
        public WebServiceManager()
        {
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:WebServiceManager-Constructor]");
            UseAppService = Convert.ToBoolean(ConfigurationManager.AppSettings["userAppService"].ToString()) == true ? true : false;
            #region Mocked Request Object
            //RqObj = new Rq
            //{
            //    IsReadyToSync = true,
            //    ShowSchemaChanges = true,
            //    ShowDeltaChanges = true,
            //    ShowSqlServerStatus = true,
            //    ShowConcurrentDevices = true,
            //    ShowSqlServerQueue = true,
            //    DeviceDetail =
            //    {
            //        DeviceID = "24FD52DAE72E",
            //        DeviceName = "24FD52DAE72E",
            //        ScriptVersion = "SLE_RemoteDB",
            //        Latitude = 0,
            //        Longitude = 0,
            //        LastSchemaUpdate = DateTime.Today.Date
            //    }
            //};
            #endregion
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:WebServiceManager-Constructor]");
        }
        protected virtual void OnServiceStateChange(ServiceStateChangeEventArgs e)
        {
            EventHandler<ServiceStateChangeEventArgs> handler = ServiceStateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public bool IsServerReachable()
        {
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:IsServerReachable]");
            try
            {
                // var Result = ResourceManager.ServiceClient.IsServerAvailable();
                DateTime timer = DateTime.Now;
                logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][IsServerReachable][CallStarted:SleStatServiceClient:IsServerReachable:" + timer + "]");
                var Result = ResourceManager.SleStatServiceClient.isServerAvailable();
                logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][IsServerReachable][CallEnded:SleStatServiceClient:IsServerReachable:" + DateTime.Now + "]");
                logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][IsServerReachable][CallDuration:SleStatServiceClient:IsServerReachable:" + (DateTime.Now - timer) + "]");
                logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:IsServerReachable]");
                return Result;
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Managers][WebServiceManager][IsServerReachable][ExceptionStackTrace = " + ex.StackTrace + "]");
                return false;
            }

        }
        public void InitiateQueryForDataFromServer()
        {
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:InitiateQueryForDataFromServer]");
            if (UseAppService)
            {
                QueryServerForNewData();
            }
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:InitiateQueryForDataFromServer]");
        }
        async void QueryServerForNewData()
        {
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:QueryServerForNewData]");
            try
            {
                // ResourceManager.ActiveRemoteID is set on user login
                // var Result = await ResourceManager.ServiceClient.NewDataAvailableAsync(ResourceManager.ActiveRemoteID);
                //var result = new WebServiceRequestAndResponse().MDoHealthCheck(RqObj);
                //var temp = ResourceManager.CommonNavInfo;
                // if (!ResourceManager.Synchronization.IsSynching) return;

                // if (InvokeWebRequestOnSync()) return;
                //QueryServerForNewData();
                logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:QueryServerForNewData]");
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Managers][WebServiceManager][QueryServerForNewData][ExceptionStackTrace = " + ex.StackTrace + "]");
                //QueryServerForNewData();
                var message = ex.Message;
            }

        }

        public HealthResponse InvokeWebRequestOnSync()
        {
            HealthResponse response = null;
            try
            {
                // Check whether Internet is available
                if (ResourceManager.NetworkManager.IsInterNetConnected())
                {
                    //Check for server availability
                    if (IsServerReachable())
                    {
                        //ResourceManager.QueueManager.PopulateQueue();
                        var sleStatClient = ResourceManager.SleStatServiceClient;
                        //Initilize and create web request object for DoHealthCheck.
                        var healthCheckReq = new SLEStatService.HealthCheckRequest();
                        healthCheckReq.IsReadyToSync =
                            Convert.ToBoolean(ConfigurationManager.AppSettings["IsReadyToSync"].ToString());
                        healthCheckReq.ShowConcurrentDevices =
                            Convert.ToBoolean(ConfigurationManager.AppSettings["ShowConcurrentDevices"].ToString());
                        healthCheckReq.ShowDeltaChanges =
                            Convert.ToBoolean(ConfigurationManager.AppSettings["ShowDeltaChanges"].ToString());
                        healthCheckReq.ShowSchemaChanges =
                            Convert.ToBoolean(ConfigurationManager.AppSettings["ShowSchemaChanges"].ToString());
                        healthCheckReq.ShowSqlServerQueue =
                            Convert.ToBoolean(ConfigurationManager.AppSettings["ShowSqlServerQueue"].ToString());
                        healthCheckReq.ShowSqlServerStatus =
                            Convert.ToBoolean(ConfigurationManager.AppSettings["ShowSqlServerStatus"].ToString());
                        healthCheckReq.ShowTruncatedTables =
                            Convert.ToBoolean(ConfigurationManager.AppSettings["ShowTruncatedTables"].ToString());
                        healthCheckReq.DeviceDeatil = new DeviceInfo();
                        healthCheckReq.DeviceDeatil.DeviceID = ResourceManager.DeviceID;
                        healthCheckReq.DeviceDeatil.DeviceName = ResourceManager.DeviceID;
                        healthCheckReq.DeviceDeatil.LastSchemaUpdate = DateTime.Now.AddDays(-20);
                        healthCheckReq.DeviceDeatil.Latitude = 0;
                        healthCheckReq.DeviceDeatil.Longitude = 0;
                        healthCheckReq.DeviceDeatil.ScriptVersion =
                            ConfigurationManager.AppSettings["ScriptVersion"].ToString();
                        //initialization of publication which needs to be synced.
                        // **Need to eliminate those pubs which need not be synced
                        // **Needs to be dynamically initialized.
                        var syncDetails = ResourceManager.Synchronization.GetSyncLastTimes();

                        var lastDownloadTime = syncDetails["LastDownloadTime"] ?? DateTime.Now.AddDays(-2);
                        var lastUploadTime = syncDetails["LastDownloadTime"] ?? DateTime.Now.AddDays(-2);

                        healthCheckReq.DeviceDeatil.RemoteSubscription = new SLEStatService.SyncSubscription[3];

                        healthCheckReq.DeviceDeatil.RemoteSubscription[0] = new SLEStatService.SyncSubscription();
                        healthCheckReq.DeviceDeatil.RemoteSubscription[0].LastDownload = lastDownloadTime;
                        healthCheckReq.DeviceDeatil.RemoteSubscription[0].LastUpload = lastUploadTime;
                        healthCheckReq.DeviceDeatil.RemoteSubscription[0].PublicationName =
                            Constants.WebServiceManager.PublicationGroup.pub_validate_user.ToString();

                        healthCheckReq.DeviceDeatil.RemoteSubscription[1] = new SLEStatService.SyncSubscription();
                        healthCheckReq.DeviceDeatil.RemoteSubscription[1].LastDownload = lastDownloadTime;
                        healthCheckReq.DeviceDeatil.RemoteSubscription[1].LastUpload = lastUploadTime;
                        healthCheckReq.DeviceDeatil.RemoteSubscription[1].PublicationName =
                            Constants.WebServiceManager.PublicationGroup.pub_everything.ToString();

                        healthCheckReq.DeviceDeatil.RemoteSubscription[2] = new SLEStatService.SyncSubscription();
                        healthCheckReq.DeviceDeatil.RemoteSubscription[2].LastDownload = lastDownloadTime;
                        healthCheckReq.DeviceDeatil.RemoteSubscription[2].LastUpload = lastDownloadTime;
                        healthCheckReq.DeviceDeatil.RemoteSubscription[2].PublicationName =
                            Constants.WebServiceManager.PublicationGroup.pub_immediate.ToString();
                        DateTime timer = DateTime.Now;

                        LogRequestActivity(healthCheckReq);
                        logger.Info(
                            "[SalesLogicExpress.Application.Managers][WebServiceManager][InvokeWebRequestOnSync][CallStarted:sleStatClient.DoHealthCheck:" +
                            timer + "]");
                        // Call server to do DoHealthCheck. 
                        //In this call, 'healthResponse' datacontract object is populated with the properties of server as requested by 'healthCheckReq' xml object.
                        var healthResponse = sleStatClient.DoHealthCheck(healthCheckReq);
                        logger.Info(
                            "[SalesLogicExpress.Application.Managers][WebServiceManager][InvokeWebRequestOnSync][CallEnded:sleStatClient.DoHealthCheck:" +
                            DateTime.Now + "]");
                        logger.Info(
                            "[SalesLogicExpress.Application.Managers][WebServiceManager][InvokeWebRequestOnSync][CallDuration:sleStatClient.DoHealthCheck:" +
                            (DateTime.Now - timer) + "]");
                        logger.Info(
                            "[SalesLogicExpress.Application.Managers][WebServiceManager][End:InvokeWebRequestOnSync]");

                        LogResponseActivity(healthResponse, DateTime.Now - timer);
                        if (!healthResponse.Equals(null))
                        {
                            NotifyUpdate(true, healthResponse);
                        }
                        logger.Info(
                            "[SalesLogicExpress.Application.Managers][WebServiceManager][End:InvokeWebRequestOnSync][HealthResponse = " +
                            healthResponse + "]");
                        response = healthResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Managers][WebServiceManager][InvokeWebRequestOnSync][ExceptionStackTrace = " + ex.StackTrace + "]");
                return null;
            }
            return response;
        }

        public void LogResponseActivity(HealthResponse pHealthResponse, TimeSpan timer)
        {
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:LogResponseActivity][HealthResponse = " + pHealthResponse.SerializeToJson() + "][timer = " + timer + "]");
            try
            {
                if (pHealthResponse != null)
                {
                    string query = String.Empty;
                    string jsonHealthResponse = pHealthResponse.SerializeToJson();
                    string typeOfResponse = pHealthResponse.GetType().ToString();
                    string isMandatorySync = String.Empty;
                    string isUpdateAvailable = String.Empty;
                    isMandatorySync = pHealthResponse.IsMandatorySync ? "1" : "0";

                    isUpdateAvailable = pHealthResponse.DeltaChangeDetails.Any() ? "1" : "0";

                    query = "INSERT INTO BUSDTA.WebServicesResponseDetail (ResponseBLOB,TimeForResponse,ResponseType,IsMandatorySync,IsSynced,IsUpdateAvailable,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)";
                    query += "VALUES('" + jsonHealthResponse + "','" + timer.ToString() + "','" + typeOfResponse + "'," + isMandatorySync + ",0," + isUpdateAvailable + ",111,getdate(),111,getdate())";

                    DbEngine.ExecuteNonQuery(query);
                    logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:LogResponseActivity][HealthResponse = " + pHealthResponse.SerializeToJson() + "][timer = " + timer + "]");

                }
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Managers][WebServiceManager][LogResponseActivity][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }

        public void LogRequestActivity(HealthCheckRequest pHealthRequest)
        {
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:LogRequestActivity][pHealthRequest = " + pHealthRequest.SerializeToJson() + "]");
            try
            {
                if (pHealthRequest != null)
                {
                    string query = string.Empty;
                    string jsonHealthRequest = pHealthRequest.SerializeToJson();
                    string typeOfResponse = pHealthRequest.GetType().ToString();

                    //query = "INSERT INTO BUSDTA.WebServicesResponseDetail (ResponseBLOB,TimeForResponse,ResponseType,IsMandatorySync,IsSynced,IsUpdateAvailable,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)";
                    //query += "VALUES('" + jsonHealthRequest + "','" + timer.ToString("hh:mm:ss.fff") + "','" + typeOfResponse + "'," + isMandatorySync + ",0,1,111,getdate(),111,getdate())";

                    query = "INSERT INTO BUSDTA.WebServicesRequestDetail(ResquestBLOB,ResquestType,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime)";
                    query += " VALUES('" + jsonHealthRequest + "','" + typeOfResponse + "',111,getdate(),111,getdate())";

                    DbEngine.ExecuteNonQuery(query);
                    logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:LogRequestActivity][HealthResponse = " + pHealthRequest.SerializeToJson() + "]");

                }
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Managers][WebServiceManager][LogRequestActivity][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }
        public Dictionary<string, bool> GetFlagsForSync()
        {
            try
            {
                logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:GetFlagsForSync]");
                Dictionary<string, bool> flagDict = new Dictionary<string, bool>();
                bool isUpdateAvailable = false, isMandatorySync = false, isSynced = false;
                string flagQuery = "select top 1 IsMandatorySync,IsSynced,IsUpdateAvailable from busdta.WebServicesResponseDetail order by ResponseID desc ";
                DataSet dbSet = new DataSet();
                dbSet = DbEngine.ExecuteDataSet(flagQuery);
                if (dbSet.HasData())
                {
                    foreach (DataRow row in dbSet.Tables[0].Rows)
                    {
                        isMandatorySync = Convert.ToBoolean(row[0].ToString().Trim());
                        isSynced = Convert.ToBoolean(row[1].ToString().Trim());
                        isUpdateAvailable = Convert.ToBoolean(row[2].ToString().Trim());
                    }
                    flagDict.Add("IsMandatorySync", isMandatorySync);
                    flagDict.Add("IsSynced", isSynced);
                    flagDict.Add("IsUpdateAvailable", isUpdateAvailable);

                }
                else
                {
                    flagDict = null;
                }
                logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:GetFlagsForSync]");

                return flagDict;
            }
            catch (Exception ex)
            {
                logger.Error("[SalesLogicExpress.Application.Managers][WebServiceManager][LogResponseActivity][ExceptionStackTrace = " + ex.StackTrace + "]");

                return null;
            }
        }
        void NotifyUpdate(bool IsServerReachable, HealthResponse pDoHealthCheckResult)
        {
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][Start:NotifyUpdate][IsServerReachable = " + IsServerReachable.ToString() + "][HealthResponse = " + pDoHealthCheckResult.SerializeToJson() + "]");
            ServiceStateChangeEventArgs args = new ServiceStateChangeEventArgs();
            args.IsServerReachable = IsServerReachable;
            args.IsReadyForSync = pDoHealthCheckResult.ReadyToSync;
            args.HealthResponse = pDoHealthCheckResult;
            args.StateChangedTime = DateTime.Now;
            OnServiceStateChange(args);
            logger.Info("[SalesLogicExpress.Application.Managers][WebServiceManager][End:NotifyUpdate][IsServerReachable = " + IsServerReachable.ToString() + "][HealthResponse = " + pDoHealthCheckResult.SerializeToJson() + "]");
        }
    }
    public class ServiceStateChangeEventArgs : EventArgs
    {
        public bool IsServerReachable { get; set; }
        public bool DataAvailable { get; set; }
        public bool IsReadyForSync { get; set; }
        public string[] Payload { get; set; }
        public HealthResponse HealthResponse { get; set; }
        public DateTime StateChangedTime { get; set; }
    }
}
