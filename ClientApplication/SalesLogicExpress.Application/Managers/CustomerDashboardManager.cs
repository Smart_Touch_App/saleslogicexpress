﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.Managers
{
    public class CustomerDashboardManager : IDisposable
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CustomerDashboardManager");

        public string GetPaymentDetails(string customerID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:GetPaymentDetails][customerID = " + customerID + "]");
            string paymentDetails = string.Empty;
            try
            {
                string query = "select aitrar, PNPTD from busdta.F03012, busdta.f0014";
                query = query + " where AIAN8 = '" + customerID + "' and pnptc = aitrar";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        paymentDetails = row["aitrar"].ToString().Trim() + "-" + row["PNPTD"].ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPaymentDetails][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            return paymentDetails;
        }

        public Dictionary<int, string> GetNoSaleReason()
        {
            Dictionary<int, string> noSaleReasonList = new Dictionary<int, string>();
            try
            {
                string query = "select ReasonCodeId, ReasonCode, rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription from busdta.ReasonCodeMaster";
                query = query + " where ReasonCodeType = 'No Sale Reason'";

                DataSet result = Helpers.DbEngine.ExecuteDataSet(query);
                if (result.HasData())
                {
                    DataRow row;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        row = result.Tables[0].Rows[i];
                        int reasonCodeID = Convert.ToInt32(row["ReasonCodeId"]);
                        string reasonCode = row["ReasonCode"].ToString();
                        string reasonDescription = row["ReasonCodeDescription"].ToString();

                        noSaleReasonList.Add(reasonCodeID, reasonDescription);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPaymentDetails " + ex.StackTrace + "]");
            }
            return noSaleReasonList;
        }

        public int UpdateNoSaleActivity(int noSaleReasonID, string stopID, string customerID, string routeId, string noSalereason, string stopDate)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:UpdateNoSaleActivity][noSaleReason = " + noSaleReasonID + "]");
            int result = -1;
            try
            {
                //Check wether cust is allready no sale or not
                //ikf it is alraedy no sale, then dont update RPUPDT, else update RPUPDT. this logic is for Daily stop sequencing
                string queryToCheckNoSale = "SELECT COUNT(*) FROM busdta.M56M0004 WHERE RPSTID='{0}' AND RPACTID = 'NOSALE'  ";
                queryToCheckNoSale = string.Format(queryToCheckNoSale, stopID);
                int count = Convert.ToInt32(DbEngine.ExecuteScalar(queryToCheckNoSale));

                Activity ac = new Activity
                {
                    CustomerID = customerID,
                    RouteID = routeId,
                    ActivityType = ActivityKey.NoActivity.ToString(),
                    ActivityStart = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = "NoActivity",
                    ActivityDetails = noSalereason,// PayloadManager.OrderPayload.SerializeToJson(),
                    ActivityEnd = DateTime.Now,
                    ActivityStatus="NoActivity",
                    StopInstanceID=stopID
                };


                string query = "update busdta.M56M0004  set RPRCID = {0} , RPACTID = 'NOSALE',RPUPDT=GETDATE() where RPSTID = '{1}' AND RPAN8 ='{2}'";
                if (count > 0)
                {
                    query = "update busdta.M56M0004  set RPRCID = {0} , RPACTID = 'NOSALE' where RPSTID = '{1}' AND RPAN8 ='{2}'";
                    UpdateActivityNoSale(ac);
                }
                else
                {
                    LogActivityNoSale(ac);
                    new CustomerManager().UpdateSequenceNo(stopID, stopDate);
                    new CustomerManager().UpdateActivityCount(stopID, false, true);
                }

                query = string.Format(query, noSaleReasonID, stopID, customerID);
                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][UpdateNoSaleActivity][noSaleReason=" + noSaleReasonID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:UpdateNoSaleActivity][noSaleReason = " + noSaleReasonID + "]");

            return result;
        }

        public DateTime? GetRescheduledDateForFuture(string customerID, DateTime selectedDate)
        {
            //throw new NotImplementedException();
            DateTime rescheduleDate = new DateTime();
            try
            {
                string query = "SELECT RPSTDT as 'ResceduledDate' FROM BUSDTA.M56M0004 WHERE RPSTID = (SELECT RPRSTID FROM BUSDTA.M56M0004 WHERE RPAN8= '" + customerID + "'";
                query = query + " AND RPSTDT='" + selectedDate.ToString("yyyy-MM-dd") + "' AND RPSTTP = 'Moved')";

                try
                {
                    rescheduleDate = Convert.ToDateTime(Helpers.DbEngine.ExecuteScalar(query));
                }
                catch
                {
                    rescheduleDate = DateTime.Parse(Helpers.DbEngine.ExecuteScalar(query), CultureInfo.CreateSpecificCulture("en-US"));
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetRescheduledDateForFuture][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return rescheduleDate;
        }

        public int GetNoSaleReasonForNoActivity(string stopID, string customerID)
        {
            int result = -1;
            try
            {
                string query = "select RPRCID from busdta.M56M0004 where RPSTID = '" + stopID + "' AND RPAN8 ='" + customerID + "'";

                result = Convert.ToInt32(string.IsNullOrEmpty( Helpers.DbEngine.ExecuteScalar(query).ToString())  ? "0" : Helpers.DbEngine.ExecuteScalar(query).ToString());
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetNoSaleReasonForNoActivity][stopID=" + stopID + "][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            return result;
        }

        public string GetStatusUpdateForNoActivity(string stopID, string customerID)
        {
            string result = string.Empty;
            try
            {
                string query = "select RPACTID from busdta.M56M0004 where RPSTID = '" + stopID + "' and RPAN8 ='" + customerID + "'";

                result = Helpers.DbEngine.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetStatusUpdateForNoActivity][stopID=" + stopID + "][customerID=" + customerID + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

            return result;
        }
        public void LogActivityNoSale(Activity ac)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:LogActivityNoSale]");
            try
            {


                //Activity a = ResourceManager.Transaction.LogActivity(ac);
                string query = "select GET_IDENTITY('busdta.M50012')";
                string activityID = DbEngine.ExecuteScalar(query).ToString();
                query = "INSERT INTO busdta.M50012(TDID,TDROUT,TDTYP,TDCLASS,TDDTLS,TDSTRTTM,TDENDTM,TDSTID,TDAN8,TDSTTLID,TDPNTID,TDSTAT)VALUES (";
                query = query + activityID + ",'";
                query = query + ac.RouteID + "','";
                query = query + ac.ActivityType + "','";
                query = query + ac.ActivityDetailClass + "',";
                query = query + "?,";
                //query = query + Activity.ActivityDetails + "',";
                query = query + ac.ActivityStart.GetFormattedDateTimeForDb() + ",";
                query = query + ac.ActivityEnd.GetFormattedDateTimeForDb() + ",'";
                query = query + ac.StopInstanceID + "','";
                query = query + ac.CustomerID + "','";
                query = query + ac.SettlementID + "','";
                query = query + ac.ActivityHeaderID + "'";
                query = query + ",'"+ac.ActivityStatus+"'";
                query = query + ");";

                // used SACommand for inserting blob, inserting blobs needs to done using parameters
                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongNVarchar;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = ac.ActivityDetails;
                new DB_CRUD().InsertUpdateData(command);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][LogActivityNoSale][ExceptionStackTrace = " + ex.StackTrace + "]");

            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:LogActivityNoSale]");

        }
        void UpdateActivityNoSale(Activity ac)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:UpdateActivityNoSale]");
            //Activity a = ResourceManager.Transaction.LogActivity(ac);
            try
            {
                string queryToUpdate = "UPDATE busdta.M50012 set  "+
                                        " TDENDTM=" + ac.ActivityEnd.GetFormattedDateTimeForDb() + ", "+
                                        " TDDTLS="+"?" +
                                        " where TDAN8 ='" + ac.CustomerID.Trim() + "' AND TDSTAT='NOACTIVITY' AND TDSTID='"+ac.StopInstanceID+"'";
                // used SACommand for inserting blob, inserting blobs needs to done using parameters
                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(queryToUpdate);
                iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongNVarchar;
                command.Parameters.Add(parm);
                command.Parameters[0].Value = ac.ActivityDetails;
                new DB_CRUD().InsertUpdateData(command);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][UpdateActivityNoSale][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:UpdateActivityNoSale]");
        }

        public void Dispose()
        {
           // throw new NotImplementedException();
        }

        public int GetStopCountOfCustomer(string deliveryCode)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:GetStopCountOfCustomer]");
            try
            {
                string query = "select count(1) from BUSDTA.M56M0002 where DCDDC = '" + deliveryCode + "'";
                result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetStopCountOfCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:GetStopCountOfCustomer]");
            return result;
        }

        public string GetStopIDForCust( string custNo, string stopDate)
        {
            string stopID = string.Empty;
            string query = "SELECT TOP 1 RPSTID FROM BUSDTA.M56M0004 WHERE RPAN8 ='"+custNo+"' and RPSTDT ='"+stopDate+"' and RPSTTP <> 'Moved'";
            stopID =DbEngine.ExecuteScalar(query).ToString();
            return stopID;
        }

        public int GetPreOrderInformationForCustomer(string customerNo, DateTime stopDate)
        {
            int result = -1;
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][Start:GetPreOrderInformationForCustomer]");
            try
            {
                string query = "select count(1) from BUSDTA.M4016 where POAN8 = '" + customerNo + "' and POSTDT ='" + stopDate.ToString("yyyy-MM-dd") + "' and ISNULL(POSTFG,0) = '0'";
                result = Convert.ToInt32(Helpers.DbEngine.ExecuteScalar(query));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][GetPreOrderInformationForCustomer][ExceptionStackTrace = " + ex.StackTrace + "]");
                result = 0;
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][CustomerDashboardManager][End:GetPreOrderInformationForCustomer]");
            return result;
        }
    }
}
