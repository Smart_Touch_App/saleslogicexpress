﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Data;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Application.Managers
{
    public class CollectionManager
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.CollectionManager");

        public CashDelivery GetCashDeliveryInfo(string CustmerId, string orderId, bool ShowLastPaymentAmountEntered = false)
        {
            try
            {
                CashDelivery cashDelivery = new CashDelivery();

                DataSet cashDeliveryResult = DbEngine.ExecuteDataSet(@" 
                                                BEGIN
                                                DECLARE PrvBal numeric(8,4);
                                                select 
                                                (sum(isnull(cl.InvoiceOpenAmt,0)) - sum(isnull(cl.ConcessionAmt,0)) - sum(isnull(cl.receiptappliedamt,0)) - sum(isnull(cl.receiptunappliedamt,0) ))  into PrvBal
                                                from BUSDTA.Customer_Ledger cl join busdta.Invoice_Header ih on cl.InvoiceId=ih.InvoiceID
                                                where IH.CustomerId=" + CustmerId + @" and cl.isactive='1' and ih.DeviceStatusID!=6 and ih.ARStatusID!=6;
                                                select isnull(PrvBal,0)PrvBal,isnull(oh.InvoiceTotalAmt,0) Invoice_Total,isnull(PrvBal,0) + isnull(oh.InvoiceTotalAmt,0) as Total ,
                                                0 as PDPAMT,
                                                0 as PDPMODE,
                                                '' as PDCHQNO from busdta.ORDER_HEADER oh where oh.OrderID=" + orderId + @"
                                                END");

                if (cashDeliveryResult.HasData())
                {
                    cashDelivery.PreviousBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["PrvBal"].ToString());
                    cashDelivery.CurrentInvoiceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Invoice_Total"].ToString());
                    cashDelivery.TotalBalanceAmount = Convert.ToDecimal(cashDeliveryResult.Tables[0].Rows[0]["Total"].ToString());
                    if (ShowLastPaymentAmountEntered)
                    {
                        cashDelivery.PaymentAmount = cashDeliveryResult.Tables[0].Rows[0]["PDPAMT"].ToString();
                        cashDelivery.PaymentMode = Convert.ToBoolean(cashDeliveryResult.Tables[0].Rows[0]["PDPMODE"]);
                        cashDelivery.ChequeNo = cashDeliveryResult.Tables[0].Rows[0]["PDCHQNO"].ToString();
                    }
                    cashDelivery.ChequeDate = DateTime.Today;
                    cashDelivery.CreditLimit = 100; // TODO: Hard Coded Value - CHANGE!

                }

                return cashDelivery;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][GetCashDeliveryInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }

        public ChargeOnAccount GetChargeOnAccountInfo(string CustmerId, string orderId, decimal TotalBalanceAmount, decimal PreviousBalanceAmount, decimal CurrentInvoiceAmount)
        {
            try
            {
                ChargeOnAccount chargeOnAccount = new ChargeOnAccount();
                chargeOnAccount.PreviousBalanceAmount = PreviousBalanceAmount;
                chargeOnAccount.TotalBalanceAmount = TotalBalanceAmount;
                chargeOnAccount.CurrentInvoiceAmount = CurrentInvoiceAmount;
                chargeOnAccount.TemporaryCharge = 2;
                chargeOnAccount.RequestCode = "78949875";   // TODO: Hard Coded Value - CHANGE!
                //chargeOnAccount.ApprovalCode = "1234-1234";
                chargeOnAccount.CreditLimit = 100;   // TODO: Hard Coded Value - CHANGE!

                return chargeOnAccount;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][GetChargeOnAccountInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
        }


        public int SaveCashDeliveryInfo(CashDelivery cashDelivery, string customerId, string routeId, string InvoiceId, ref string newReceiptId)
        {
            try
            {
                if (string.IsNullOrEmpty(cashDelivery.PaymentAmount))
                {
                    cashDelivery.PaymentAmount = 0.ToString();
                }


                string chequeNo = string.IsNullOrEmpty(cashDelivery.ChequeNo) ? "" : cashDelivery.ChequeNo;
                string chequeDate = cashDelivery.ChequeDate.ToString("yyyy-MM-dd");

                int NewReceiptId = InsertReceiptHeader(cashDelivery, customerId, routeId, chequeNo, chequeDate);
                newReceiptId = NewReceiptId.ToString();

                //Receipt entry in Customer ledger
                InsertReceiptInCustomerLedger(routeId, NewReceiptId, customerId, Convert.ToDecimal(cashDelivery.PaymentAmount));

                InsertReceiptInvoiceMappingInCustomerLedger(
                         CommonNavInfo.RouteID.ToString(),
                         CommonNavInfo.Customer.CustomerNo,
                         InvoiceId.ToString(),
                         cashDelivery.CurrentInvoiceAmount,
                         NewReceiptId,
                         cashDelivery.CurrentInvoiceAmount
                         );

            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Managers][CollectionManager][SaveCashDeliveryInfo][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return 1;
        }

        public int InsertReceiptHeader(CashDelivery cashDelivery, string customerId, string routeId, string chequeNo, string chequeDate, int receiptId=0, string reasonCodeId="")
        {
            int NewReceiptId = 0;

            //When receipt id is zero, number pool will be used to fetch the next receipt number, 
            //else passed receipt id will be used
            if (receiptId==0)
            {
                NewReceiptId = Convert.ToInt32(new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.Receipt));
            }
            else
            {
                NewReceiptId = receiptId;
            }
            
            //Save in Payment Details

            DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Receipt_Header  (StatusId,CustomerId ,RouteId ,ReceiptID,TransactionAmount ,PaymentMode ,ChequeNum ,ChequeDate ,CreatedBy,CreatedDatetime,TransactionMode,ReceiptNumber,ReceiptDate, ReasonCodeId)
                                        values(4," + customerId + "," + routeId + "," + NewReceiptId.ToString() + "," + cashDelivery.PaymentAmount + ",'" + Convert.ToInt32(cashDelivery.PaymentMode) + "','" + chequeNo + "','" + chequeDate + "',NULL,now(),'0'," + NewReceiptId.ToString() + ",'" + chequeDate + "'," + (string.IsNullOrEmpty(reasonCodeId)?"NULL":reasonCodeId) +")");
            return NewReceiptId;
        }

        public void InsertReceiptInCustomerLedger(string routeId, int NewReceiptId, string customerId, decimal receiptUnAppliedAmt)
        {
            DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Customer_Ledger (ReceiptID,CustomerId,RouteId,ReceiptUnAppliedAmt,IsActive) 
                                       values(" + NewReceiptId.ToString() + "," + customerId + "," + routeId + "," + receiptUnAppliedAmt.ToString() + ",'1')");
        }


        public void InsertReceiptInvoiceMappingInCustomerLedger(string routeId, string customerId, string InvoiceId, decimal InvoiceGrossAmt, int ReceiptId, decimal InvOpenAmt)
        {
            DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Customer_Ledger  (RouteId,CustomerId,ReceiptID ,InvoiceId,InvoiceGrossAmt,InvoiceOpenAmt,ReceiptUnAppliedAmt,ReceiptAppliedAmt,  IsActive) 
                                       values(" + routeId + "," + customerId + "," + ReceiptId.ToString() + "," + InvoiceId + "," + InvoiceGrossAmt.ToString() + ",0,0," + InvOpenAmt.ToString() + ",'1')");

            //Update Invoice Status if Invoice Open Amt =< Payment Amt
            DbEngine.ExecuteNonQuery(@"Update BUSDTA.Invoice_Header set DeviceStatusID=2 where InvoiceID=" + InvoiceId + " and RouteId=" + routeId);
        }



    }
}
