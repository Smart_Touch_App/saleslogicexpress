﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using SalesLogicExpress.Application.ViewModels;
namespace SalesLogicExpress.Application.Managers
{
    public class OrderManager
    {

        private static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.Managers.OrderManager");
        public static PricingManager pricingManager = new PricingManager();
        public OrderManager()
        {

        }

        //public ActivityKey GetOrderSubStatus(string orderId)
        //{
        //    string substate = DbEngine.ExecuteScalar("select TTKEY from busdta.ORDER_HEADER oh join BUSDTA.M5001  tt on tt.ttid=oh.OrderStateId WHERE orderid=" + orderId);

        //    if (substate.Contains("Void"))

        //        if (string.IsNullOrEmpty(substate))
        //        {
        //            return OrderSubState.None;
        //        }
        //        else
        //        {
        //            return OrderSubState.Hold.ToString() == substate ? OrderSubState.Hold : OrderSubState.Void;
        //        }

        //}

        public bool IsItemPickedForOrder(string orderId)
        {
            int itemCount = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.pickOrder where order_id=" + orderId + " and Picked_Qty_Primary_UOM !=0"));
            int itemExCount = Convert.ToInt32(DbEngine.ExecuteScalar("select count(*) from busdta.pickOrder_exception where order_id=" + orderId + " and exception_Qty !=0"));
            if (itemCount == 0 && itemExCount == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public int GetNewOrderNum()
        {
            int OrderNum = 0;
            try
            {
                string val = new Managers.NumberManager().GetNextNumberForEntity(CommonNavInfo.RouteID.ToString(), Managers.NumberManager.Entity.SalesOrder);
                OrderNum = Convert.ToInt32(val);


                //************************************************************************************************
                // Comment: Added to send pull the stopdate and stopId
                // Created: feb 10, 2016
                // Author: Vivensas (Rajesh,Yuvaraj)
                // Revisions: 
                //*************************************************************************************************

                Managers.CustomerDashboardManager dashboardManager = new CustomerDashboardManager();
                if (CommonNavInfo.Customer.StopID == null)
                {
                    CommonNavInfo.Customer.StopDate = DateTime.Now.Date;
                    CommonNavInfo.Customer.StopID = dashboardManager.GetStopIDForCust(CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.StopDate.Value.ToString("yyyy-MM-dd"));
                }

                //*************************************************************************************************
                // Vivensas changes ends over here
                //**************************************************************************************************

                int updateStopActivityFlag = DbEngine.ExecuteNonQuery("update busdta.m56m0004 set rpactid=null where rpstid = " + CommonNavInfo.Customer.StopID + " and rpan8=" + CommonNavInfo.Customer.CustomerNo + "");
                int updatePreOrderFlag = DbEngine.ExecuteNonQuery("update BUSDTA.M4016 SET POSTFG = '1' where POAN8 = '" + CommonNavInfo.Customer.CustomerNo + "' and POSTDT = '" + CommonNavInfo.Customer.StopDate.Value.ToString("yyyy-MM-dd") + "'");
            }
            catch (Exception ex)
            {
                OrderNum = -1;
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetNewOrderNum][RouteID=" + CommonNavInfo.RouteID.ToString() + "][ForEntity=" + Managers.NumberManager.Entity.SalesOrder.ToString() + "][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
            return OrderNum;
        }

        public int GetUncomletedLastOrderIdIfAny(string CustomerId)
        {
            return Convert.ToInt32(DbEngine.ExecuteScalar("select isnull(max(OrderID),0) from BUSDTA.ORDER_HEADER where customerid=" + CustomerId + " and isnull(OrderStateId,'') not in ( " +
                GetTerminatedOrderStatusIds() + " ) AND  OrderTypeId = " + StatusTypesEnum.SALORD.GetStatusIdFromDB() + ""));
        }

        public string GetTerminatedOrderStatusIds()
        {
            String strids = "";
            DataTable dt = DbEngine.ExecuteDataSet("SELECT TTID FROM BUSDTA.M5001 where TTKEY in ("
                  + "'" + ActivityKey.OrderDelivered.ToString() + "',"
                  + "'" + ActivityKey.VoidAtOrderEntry.ToString() + "',"
                  + "'" + ActivityKey.VoidAtDeliverCustomer.ToString() + "',"
                  + "'" + ActivityKey.VoidAtCashCollection.ToString() + "',"
                  + "'" + ActivityKey.HoldAtPick.ToString() + "',"
                  + "'" + ActivityKey.HoldAtOrderEntry.ToString() + "',"
                  + "'" + ActivityKey.HoldAtCashCollection.ToString() + "',"
                  + "'" + ActivityKey.OrderReturned.ToString() + "',"
                  + "'" + ActivityKey.HoldAtDeliverToCustomer.ToString() + "'"
                + ") ").Tables[0];

            foreach (DataRow item in dt.Rows)
            {
                strids = strids + item["TTID"].ToString() + ",";
            }

            strids = strids.Substring(0, strids.Length - 1);
            return strids;
        }

        public int SaveOrder(int OrderId, int CustomerId, ObservableCollection<OrderItem> OrderItems, string TotalCoffeeAmt = "", string TotalAlliedAmt = "")
        {
            int result = 0;

            try
            {
                //check for Order id
                string strOrderId = DbEngine.ExecuteScalar("select OrderID from BUSDTA.ORDER_HEADER where OrderID='" + OrderId.ToString() + "'");

                //Add mode
                if (string.IsNullOrEmpty(strOrderId))
                {
                    //Insert in ORDER_HEADER
                    result = SaveOrderHeader(OrderId, CustomerId, TotalCoffeeAmt, TotalAlliedAmt);
                }
                else
                {
                    //Update ORDER_HEADER
                    UpdateOrderHeader(OrderId);

                }

                //Delete and Insert in Order_details
                DeleteAndInsertOrderItems(OrderItems, OrderId);

            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public void DeleteOrderItems(int OrderId)
        {
            try
            {
                DbEngine.ExecuteNonQuery("delete from BUSDTA.Order_Detail where OrderID = " + OrderId.ToString());
            }
            catch (Exception)
            {

                throw;
            }
        }


        public int SaveOrderHeader(int OrderId, int CustomerId, string TotalCoffeeAmt = "", string TotalAlliedAmt = "")
        {
            int result = 0;
            try
            {
                result = DbEngine.ExecuteNonQuery(@"insert into BUSDTA.ORDER_HEADER (
                    OrderID,
                    OrderTypeId,
                    RouteId,
                    CustomerId,
                    OrderDate,
                    CreatedBy,
                    CreatedDatetime,
                    TotalCoffeeAmt,
                    TotalAlliedAmt,
                    OrderTotalAmt
                    )
                    values(
                    '" + OrderId + @"'," + StatusTypesEnum.SALORD.GetStatusIdFromDB()
                       + "," + ViewModels.CommonNavInfo.RouteID + "," +
                    "'" + CustomerId + @"',
                    date(now()),
                    " + Managers.UserManager.UserId + @",
                    now(),
                    " + TotalCoffeeAmt + "," +
                     TotalAlliedAmt + "," + ViewModelPayload.PayloadManager.OrderPayload.Amount + ");");
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }

        public int UpdateOrderHeader(int OrderId)
        {
            int result = 0;
            try
            {
                result = DbEngine.ExecuteNonQuery(@"update BUSDTA.ORDER_HEADER set                     
                    OrderDate = date(now()),
                    CreatedDatetime = date(now())
                    where  OrderID=" + OrderId.ToString());
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }


        public ObservableCollection<ViewModels.ReasonCode> GetReasonListForVoidOrder()
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();

            try
            {

                DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='Void Order'").Tables[0];

                ViewModels.ReasonCode ReasonCode = new ViewModels.ReasonCode();


                foreach (DataRow dr in dt.Rows)
                {
                    ReasonCode = new ViewModels.ReasonCode();
                    ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                    ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                    reasonCodeList.Add(ReasonCode);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Erro in GetReasonListForVoidOrder" + ex.Message.ToString());
            }


            return reasonCodeList;

        }

        public ObservableCollection<ViewModels.ReasonCode> GetReasonListFor(string Type)
        {
            ObservableCollection<ViewModels.ReasonCode> reasonCodeList = new ObservableCollection<ViewModels.ReasonCode>();
            DataTable dt = DbEngine.ExecuteDataSet("SELECT ReasonCodeId Id,ReasonCode Code,rtrim(ltrim(ISNULL(ReasonCodeDescription,''))) ReasonCodeDescription FROM BUSDTA.ReasonCodeMaster where ReasonCodeType='" + Type + "'").Tables[0];

            ViewModels.ReasonCode ReasonCode = new ViewModels.ReasonCode();


            foreach (DataRow dr in dt.Rows)
            {
                ReasonCode = new ViewModels.ReasonCode();
                ReasonCode.Id = Convert.ToInt32(dr["Id"].ToString());
                ReasonCode.Code = dr["ReasonCodeDescription"].ToString();
                reasonCodeList.Add(ReasonCode);
            }


            return reasonCodeList;

        }

        public void UpdateInventoryForDeletedOrderItem(List<Item> OrderItems, int OrderId)
        {

            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:UpdateInventoryForDeletedOrderItem]");
            try
            {
                string query = "select ItemID,OrderQty from busdta.Order_detail where OrderID='{0}'";
                query = string.Format(query, OrderId);
                DataSet ds = DbEngine.ExecuteDataSet(query);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    // This is a new order, saved from order template screen
                }
                else
                {
                    Dictionary<string, int> ExistingItemWithQty = new Dictionary<string, int>();
                    // Existing order being saved again.
                    /* Cases : 
                    * Existing item was removed
                    * Existing item qty was altered
                    * New Item was added 
                    * Existing items were removed and new items were added with same qty
                    */
                    List<Item> itemsRemoved = new List<Item>();
                    foreach (DataRow itemRow in ds.Tables[0].Rows)
                    {
                        string ItemID = itemRow["ItemID"].ToString();
                        int OrderQty = Convert.ToInt32(itemRow["OrderQty"].ToString());
                        ExistingItemWithQty.Add(ItemID, OrderQty);
                        //Existing item was removed
                        if (OrderItems.FirstOrDefault(item => item.ItemId == ItemID) == null)
                        {
                            // Increase inventory qty for this item
                            Item itemM = new Item();
                            itemM.ItemId = ItemID;
                            itemM.OrderQty = OrderQty;
                            itemsRemoved.Add(itemM);
                            continue;
                        }
                    }
                    // new InventoryManager().UpdateCommittedQuantity(itemsRemoved, true);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][UpdateInventoryForDeletedOrderItem][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:UpdateInventoryForDeletedOrderItem]");
        }
        public void DeleteAndInsertOrderItems(ObservableCollection<OrderItem> OrderItems, int OrderId)
        {
            try
            {
                UpdateInventoryForDeletedOrderItem(OrderItems.ToList<Item>(), OrderId);
                DeleteOrderItems(OrderId);

                foreach (OrderItem orderItem in OrderItems)
                {
                    var isTaxable = orderItem.IsTaxable ? 1 : 0;
                    DbEngine.ExecuteNonQuery(@"insert into BUSDTA.Order_Detail(
                        RouteId,
                        OrderID,
                        ItemId,
                        OrderQty,
                        OrderUM,
                        UnitPriceAmt,
                        ExtnPriceAmt,
                        ItemSalesTaxAmt,
                        IsTaxable,
                        PriceOverrideReasonCodeId
                        )values
                        (
                        " + CommonNavInfo.RouteID + "," +
                        "'" + OrderId + @"',
                        " + orderItem.ItemId + @",
                        " + orderItem.OrderQty + @",
                        '" + orderItem.UM + @"',
                        " + orderItem.UnitPrice + @",
                        " + orderItem.ExtendedPrice + @",
                        " + orderItem.TaxAmount + @",
                        " + isTaxable + @",
                        " + orderItem.ReasonCode + @"
                        );
                        ");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ObservableCollection<OrderItem> GetOrderForCustomer()
        {
            return null;
        }
        public void CreateOrderForCustomer()
        {

        }
        /// <summary>
        /// Gets the Order history for customer
        /// </summary>
        /// <param name="customerID">Cutomer No</param>
        /// <returns>DataSet with two tables, first table has OrderHistory Items and the second table includes Order Date headers</returns>
        public DataSet GetOrderHistoryForCustomer(string customerID)
        {
            DataSet dsOrderHistory = null;
            try
            {
                Dictionary<string, object> procParamaters = new Dictionary<string, object>();
                procParamaters.Add("@shipTo", customerID);
                int result = Helpers.DbEngine.ExecuteNonQuery("BUSDTA.prepareHistoryRecords", procParamaters, true);
                string queryOrderHistory = "select *,0 as AverageQty from busdta.localOrderHistoryPanel";
                //string queryOrderHistoryHeader = "select chronology,convert(varchar(15),SHDOCO) as OrderNumber,Dateformat(BUSDTA.DateG2J(SHTRDJ),'mm/dd/yyyy') as OrderDate from busdta.localLastOrders order by convert(varchar(15),SHDOCO) desc";
                string queryOrderHistoryHeader = "select chronology,OrderID as OrderNumber, Dateformat(OrderDate,'mm/dd/yyyy') as OrderDate from busdta.localLastOrders order by OrderID desc";

                Dictionary<string, string> orderHistoryAndDateQueryList = new Dictionary<string, string>();
                orderHistoryAndDateQueryList.Add("OrderHistory", queryOrderHistory);
                orderHistoryAndDateQueryList.Add("OrderHistoryHeaders", queryOrderHistoryHeader);
                dsOrderHistory = Helpers.DbEngine.ExecuteDataSet(orderHistoryAndDateQueryList);


                if (dsOrderHistory.HasData())
                {
                    if (dsOrderHistory.Tables[0].Columns["imlitm"] == null)
                        return dsOrderHistory;

                    dsOrderHistory.Tables[0].Columns["imlitm"].ColumnName = "ItemCode";
                    dsOrderHistory.Tables[0].Columns["imdsc1"].ColumnName = "ItemDesc";
                    int historyAvailableFor = dsOrderHistory.Tables[0].Columns.Count - 3;
                    foreach (DataRow orderHistoryItem in dsOrderHistory.Tables[0].Rows)
                    {
                        double average = 0;
                        historyAvailableFor = dsOrderHistory.Tables[0].Columns.Count - 3;

                        if (historyAvailableFor > 10)
                        {
                            historyAvailableFor = 10;
                        }

                        for (int i = 0; i < historyAvailableFor; i++)
                        {
                            average = average + (string.IsNullOrEmpty(orderHistoryItem["H" + (i + 1) + "_Qty"].ToString()) ? 0 : Convert.ToInt32(orderHistoryItem["H" + (i + 1) + "_Qty"]));
                            if (string.IsNullOrEmpty(orderHistoryItem["H" + (i + 1) + "_Qty"].ToString()))
                            {
                                orderHistoryItem["H" + (i + 1) + "_Qty"] = 0;
                            }
                        }
                        average = (average == 0) ? 0 : average / historyAvailableFor;
                        orderHistoryItem["AverageQty"] = average;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }

            return dsOrderHistory;
        }

        public DataTable GetNewOrderItems(string CustomerId)
        {
            DataTable dt = new DataTable();
            DataTable ExpectedDT = new DataTable();

            dt = DbEngine.ExecuteDataSet(@"select od.ItemId, im.imdsc1,od.OrderQty,oh.orderdate,oh.orderid
 from BUSDTA.Order_Detail od join BUSDTA.ORDER_HEADER oh on od.orderid=oh.orderid join busdta.F4101 im on od.ItemId= im.IMITM where oh.customerid='" + CustomerId + "'" +
" order by oh.orderid").Tables[0];

            string previousOrderId = "";

            ExpectedDT.Columns.Add("Item Number");
            ExpectedDT.Columns.Add("Item Desc");

            foreach (DataRow item in dt.Rows)
            {
                if (item["order_id"].ToString() != previousOrderId)
                {
                    previousOrderId = item["order_id"].ToString();
                    ExpectedDT.Columns.Add("H_Qty");
                }
                if (ExpectedDT.Select("Item_Number ='" + item["Item_Number"].ToString() + "'").CopyToDataTable().Rows.Count > 0) ;
                {

                }
            }

            return ExpectedDT;
        }
        public void SaveInvoiceItemsForReport(ObservableCollection<OrderItem> orderItems, int invoiceNo)
        {
            try
            {
                string truncateInvoiceDetailsQuery = "truncate table BUSDTA.InvoiceDetails;";
                Helpers.DbEngine.ExecuteNonQuery(truncateInvoiceDetailsQuery);

                string insertInvoiceDetailsQuery = "BUSDTA.SaveMasterInvoice";
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                foreach (OrderItem item in orderItems)
                {
                    parameters.Clear();
                    parameters.Add("@InvoiceNo", invoiceNo);
                    parameters.Add("@OrderQty", item.OrderQty);
                    parameters.Add("@UM", item.UM);
                    parameters.Add("@ItemCode", item.ItemNumber);
                    parameters.Add("@ProductDesc", item.ItemDescription);
                    parameters.Add("@UnitPrice", item.UnitPrice);
                    parameters.Add("@ExtendedPrice", item.ExtendedPrice);
                    parameters.Add("@OrderDate", item.OrderDate);
                    parameters.Add("@SalesCat1", item.SalesCat1);
                    Helpers.DbEngine.ExecuteNonQuery(insertInvoiceDetailsQuery, parameters, true);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public OrderItem ApplyPricingToItem(Item item, string routeBranch, string shipToCustomerID)
        {
            decimal unitPrice = 0;
            Random randomProvider = new Random();

            //*******************************************************
            //Code Line for changed to store orginal price of variables 
            //OriginalPrice is a read-only variable and is assined a value in constructor of orderitem 
            //************************ START *******************************
            //OrderItem orderItem = new OrderItem(item);
            //PricingManager pricingManager = new PricingManager();
            //unitPrice = 0;
            //orderItem.QtyOnHand = randomProvider.Next(0, 50);
            //orderItem.OrderQty = 1;
            //unitPrice = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, orderItem.ItemNumber, orderItem.OrderQty.ToString()));
            //orderItem.UnitPrice = unitPrice == 0 ? unitPrice : unitPrice;
            //orderItem.ExtendedPrice = (orderItem.UM == orderItem.UMPrice) ? decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice : (decimal.Parse(orderItem.OrderQty.ToString()) * orderItem.UnitPrice * decimal.Parse(pricingManager.jdeUOMConversion(orderItem.UMPrice, orderItem.UM, int.Parse(orderItem.ItemId)).ToString()));
            //orderItem.AppliedUMS = new ItemManager().GetAppliedUMs(orderItem.ItemNumber).Count != 0 ? new ItemManager().GetAppliedUMs(orderItem.ItemNumber) : new List<string>() { orderItem.UM };


            item.QtyOnHand = randomProvider.Next(0, 50);
            item.OrderQty = 1;
            //Get Price By Pricing UOM
            item.UnitPriceByPricingUOM = Convert.ToDecimal(pricingManager.loadPriceDetailsGrid(routeBranch, shipToCustomerID, item.ItemNumber, item.OrderQty.ToString()));
            //Get Price By UOM Factor
            unitPrice = GetPriceByUomFactor(item.ItemId, item.UMPrice, item.UM, item.UnitPriceByPricingUOM);
            item.UnitPrice = unitPrice;
            //item.ExtendedPrice = (item.UM == item.UMPrice) ? decimal.Parse(item.OrderQty.ToString()) * item.UnitPrice : (decimal.Parse(item.OrderQty.ToString()) * item.UnitPrice * decimal.Parse(pricingManager.jdeUOMConversion(item.UMPrice, item.UM, int.Parse(item.ItemId)).ToString()));
            item.ExtendedPrice = decimal.Parse(item.OrderQty.ToString()) * item.UnitPrice;
            item.AppliedUMS = new ItemManager().GetAppliedUMs(item.ItemNumber).Count != 0 ? new ItemManager().GetAppliedUMs(item.ItemNumber) : new List<string>() { item.UM };

            //************************ END *******************************
            return new OrderItem(item);
        }


        public static decimal GetPriceByUomFactor(string ItemId, string PriceUOM, string AppliedUOM, decimal UnitPriceForPricingUOM)
        {
            try
            {
                string ResultUnitPrice = "";

                if (PriceUOM != AppliedUOM)
                {
                    //************************************************************************************************
                    // Comment: Handling negative uomFactor
                    // Created: Feb 08, 2016
                    // Author: Vivensas (Rajesh,Yuvaraj)
                    // Revisions: 
                    //*************************************************************************************************
                    var uomFactor = decimal.Parse(pricingManager.jdeUOMConversion(AppliedUOM, PriceUOM, int.Parse(ItemId)).ToString());
                    // If uomFactor is less than 0, set it to 1
                    uomFactor = uomFactor < 0 ? 1 : uomFactor;

                    ResultUnitPrice = (UnitPriceForPricingUOM * uomFactor).ToString();
                    //*************************************************************************************************
                    // Vivensas changes ends over here
                    //**************************************************************************************************
                }

                if (string.IsNullOrEmpty(ResultUnitPrice))
                {
                    Logger.Error("UOM Convertion Factor is not Available for ItemId:" + ItemId + ",PriceUOM:" + PriceUOM + ",AppliedUOM:" + AppliedUOM + ",UnitPriceForPricingUOM:" + UnitPriceForPricingUOM.ToString());
                    return UnitPriceForPricingUOM;
                }
                else
                {
                    return Convert.ToDecimal(ResultUnitPrice);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetPriceByUomFactor Message:" + ex.Message);
                throw ex;
            }
        }

        public DataTable GetAllOpenOrdersForItem(string ItemNumber)
        {
            DataTable dt = new DataTable();

            dt = DbEngine.ExecuteDataSet(@"SELECT oh.Orderid,customerid,ct.ABALPH CustomerName,pk.Order_Qty_Primary_UOM,pk.Primary_UOM,pk.Picked_Qty_Primary_UOM FROM BUSDTA.Order_Detail od join BUSDTA.ORDER_HEADER oh on od.OrderID = oh.OrderID join BUSDTA.F0101 ct on ct.ABAN8=oh.CustomerId 
join busdta.PickOrder pk on oh.OrderID=pk.Order_ID and od.ItemId=pk.Item_Number where od.ItemId=" + ItemNumber + " and isnull(oh.OrderStateId,'')!=" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + "").Tables[0];
            return dt;
        }

        public class PriceOvrCodes
        {
            public ObservableCollection<PriceOvrCodes> GetPriceOvrCodes()
            {
                ObservableCollection<PriceOvrCodes> codes = new ObservableCollection<PriceOvrCodes>();
                codes.Add(new PriceOvrCodes { Id = 1, Name = "" });
                codes.Add(new PriceOvrCodes { Id = 2, Name = "CPR" });
                codes.Add(new PriceOvrCodes { Id = 3, Name = "PKN" });
                codes.Add(new PriceOvrCodes { Id = 4, Name = "SMP" });
                codes.Add(new PriceOvrCodes { Id = 5, Name = "TPR" });
                return codes;
            }
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public void SaveVoidOrderReasonCode(string orderNo, int reasonId)
        {
            string query = "update busdta.ORDER_HEADER set VoidReasonCodeId='{0}', UpdatedDatetime=now() where OrderID='{1}'";
            query = string.Format(query, reasonId.ToString(), orderNo.ToString());
            DbEngine.ExecuteNonQuery(query);
        }
        public static void UpdateOrderStatus(string orderNo, ActivityKey state, bool UpdateTable = true)
        {
            try
            {
                if (UpdateTable)
                {
                    string query = "update busdta.ORDER_HEADER set OrderStateId={0}, UpdatedDatetime=now() where OrderID='{1}' and isnull(OrderStateId,'')!='" + ActivityKey.OrderDelivered.GetStatusIdFromDB() + "'";
                    query = string.Format(query, state.GetStatusIdFromDB(), orderNo);
                    DbEngine.ExecuteNonQuery(query);
                    SalesLogicExpress.Application.ViewModels.Order.OrderStatus = state;
                }

            }
            catch (Exception ex)
            {
                //log.Error("Erro in ShowOrderTemplate" + ex.Message.ToString());
            }
        }

        public List<string> ValidateUM(string ItemCode)
        {
            List<string> validUM = new List<string>();
            try
            {

                DataTable dt = new DataTable();

                dt = DbEngine.ExecuteDataSet("select   distinct(UMUM) UM from busdta.F41002,busdta.F4101 union " +
                                                "select   distinct(UMRUM) UM from busdta.F41002,busdta.F4101 " +
                                                "where umitm = imitm and rtrim(imlitm) = '" + ItemCode + "'").Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    validUM.Add(dr["UM"].ToString());

                }
            }
            catch (Exception e)
            {
                Logger.Error("Page:OrderManager.cs,Method:validateUM Parameter:" + ItemCode + " Message: " + e.StackTrace);
            }

            return validUM;
        }
        public string GetParent(int mnABNumber)
        {
            string txtShipTo = null;
            string txtParent = null;
            string txtParentDesc = null;

            txtParent = DbEngine.ExecuteScalar("SELECT AIAN8 from BUSDTA.F03012 where AIAN8 in (select MAPA8 from BUSDTA.F0150 where MAAN8=" + mnABNumber + " and MAOSTP='   ')");
            if (string.IsNullOrEmpty(txtParent))
            {
                txtParent = txtShipTo;
            }

            txtParentDesc = DbEngine.ExecuteScalar("select ABAN8 ||', '|| ABALPH AS ParentDesc from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where rtrim(ltrim(ABAN8)) = '" + txtParent + "'");
            if (string.IsNullOrEmpty(txtParentDesc))
            {
                txtParentDesc = txtShipTo;
            }

            return txtParentDesc;
        }

        public string GetSoldTo(int mnABNumber)
        {
            string txtBillTo = null;
            string txtSoldTo = null;

            txtBillTo = DbEngine.ExecuteScalar("SELECT AIAN8 from BUSDTA.F03012 where AIAN8 in (Select ABAN81 from BUSDTA.F0101 where ABAN8=" + mnABNumber + ")");

            //cmd.Parameters.Add("@AN8", SADbType.Integer).Value = mnABNumber;

            txtSoldTo = DbEngine.ExecuteScalar("select ABAN8 ||', '|| ABALPH AS SoldTo from BUSDTA.F0101 join BUSDTA.F03012 on ABAN8=AIAN8 where rtrim(ltrim(ABAN8)) = '" + txtBillTo + "'");

            if (string.IsNullOrEmpty(txtSoldTo))
            {
                txtSoldTo = txtBillTo;
            }

            return txtSoldTo;
        }
        public string GetHoldCode(string strBranchId)
        {
            return DbEngine.ExecuteScalar("select AIHDAR from busdta.F03012 where aian8 = '" + strBranchId + "'");
        }

        public string GetEnergySurcharge(string customerID, string invoiceTotal)
        {
            string energySurcharge = "";
            energySurcharge = DbEngine.ExecuteScalar("CALL BUSDTA.calculateEnergySurcharge(CustomerNum = '" + customerID + "',OrderTotal =" + invoiceTotal + ")");
            if (energySurcharge == null || energySurcharge == "")
            {
                energySurcharge = "0.00";
            }
            return energySurcharge;
        }

        public int UpdateNoSaleReason(string stopID, string customerID)
        {
            int result = -1;

            try
            {
                string query = "update busdta.M56M0004  set RPRCID = NULL, RPACTID = '' where RPSTID = '{0}' ";
                query = string.Format(query, stopID);

                result = Helpers.DbEngine.ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                Logger.Error("Page:OrderManager.cs,Method:UpdateNoSaleReason Parameter:" + stopID + " Message: " + ex.StackTrace);
            }
            return result;
        }

        public DataTable GetOrderInfo(string orderId)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderInfo][Parameter:orderId:" + orderId + "]");
            DataTable items = new DataTable();
            try
            {
                items = DbEngine.ExecuteDataSet(
                               @"select oh.ordersign, oh.OrderDate OrderDate,oh.EnergySurchargeAmt,oh.SalesTaxAmt,
                                im.IMSRP1  as SalesCat1, im.IMSRP5 as SalesCat5, im.IMSRP4 as SalesCat4,
                                od.OrderQty OrderQty,od.OrderUM UM,
                                im.IMLITM ItemNumber,im.IMDSC1 ItemDescription,od.UnitPriceAmt UnitPrice,od.ExtnPriceAmt ExtendedPrice, case od.PriceOverrideReasonCodeId when 0 then '' else 'D' end as PriceOverrideFlag
                                from busdta.Order_Header oh 
                                join BUSDTA.Order_Detail od on oh.orderid=od.orderid 
                                join busdta.F4101 im on od.ItemId= im.IMITM
                                where oh.orderid=" + orderId).Tables[0];

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderInfo][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderInfo]");
            return items;
        }
        internal void UpdateOrderItemQty(List<OrderItem> orderItems, string OrderID)
        {
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:UpdateOrderItemQty]");
            try
            {
                string query = string.Empty;
                foreach (OrderItem item in orderItems)
                {
                    try
                    {
                        query = "update busdta.order_Detail  set OrderQty ={0}, OrderUM='{1}' where OrderID='{2}' and ItemID='{3}';";
                        query = string.Format(query, item.OrderQty, item.UM, OrderID, item.ItemId);
                        Helpers.DbEngine.ExecuteNonQuery(query);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][UpdateOrderItemQty][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][UpdateOrderItemQty][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:UpdateOrderItemQty]");
        }
        public List<OrderItem> GetOrderItems(string OrderID)
        {
            List<OrderItem> items = new List<OrderItem>();
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][Start:GetOrderItems]");
            try
            {
                //                string query = @"SELECT rtrim(IM.IMLITM) AS ItemNumber,IC.PrimaryUM,OD.ORDERUM AS UM, OD.* FROM BUSDTA.ORDER_detail OD 
                //                                LEFT OUTER JOIN BUSDTA.F4101 IM ON OD.ItemId= IM.IMITM 
                //                                left outer join busdta.ItemConfiguration IC on OD.ItemID = IC.ItemId 
                //                                where OrderID='{0}'";
                string query = "SELECT rtrim(IM.IMLITM) AS ItemNumber,PO.Primary_UOM AS PrimaryUM,OD.ORDERUM AS UM--, OD.*";
                query = "\n, (CASE WHEN isnull(PO.Order_Qty,0)>isnull(PO.Picked_Qty_Primary_UOM,0) THEN isnull(PO.Order_Qty,0) ELSE isnull(PO.Picked_Qty_Primary_UOM,0) END ) AS OrderQty";
                query = query + "\nFROM BUSDTA.ORDER_detail OD ";
                query = query + "\nLEFT OUTER JOIN BUSDTA.F4101 IM ON OD.ItemId= IM.IMITM ";
                query = query + "\nLEFT OUTER JOIN busdta.ItemConfiguration IC on OD.ItemID = IC.ItemId ";
                query = query + "\nLEFT OUTER JOIN BUSDTA.PickOrder PO ON PO.Order_Id=OD.OrderID AND IM.IMLITM=PO.Item_Number";
                query = query + "\nwhere OrderID='" + OrderID + "'";

                //query = string.Format(query, OrderID);
                DataSet dsOrderDetails = DbEngine.ExecuteDataSet(query);
                items = dsOrderDetails.GetEntityList<OrderItem>();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.Managers][OrderManager][GetOrderItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.Managers][OrderManager][End:GetOrderItems]");
            return items;
        }
        internal static void UpdateOrderItemsAndInventory(string p, List<OrderItem> list)
        {

        }
    }
}
