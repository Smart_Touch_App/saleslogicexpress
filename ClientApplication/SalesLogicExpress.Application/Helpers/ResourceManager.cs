﻿using iAnywhere.Data.SQLAnywhere;
using System;
using System.Configuration;
using log4net;

namespace SalesLogicExpress.Application.Helpers
{
    public static class ResourceManager
    {
        // static AppService.AppServiceClient serviceClient = null;
        private static SLEStatService.StatServiceClient sleServiceClient = null;
        static SAConnection connection = null;
        static string remoteID = null;
        static SalesLogicExpress.Application.Managers.TransactionManager transactionManager = null;
        static SalesLogicExpress.Application.Managers.SyncManager syncManager = null;
        static SalesLogicExpress.Application.Managers.NetworkManager networkManager = null;
        static SalesLogicExpress.Application.Managers.QueueManager queueManager = null;
        static SalesLogicExpress.Application.Managers.WebServiceManager webServiceManager = null;
        static string databaseDirectory, databaseResourcesDirectory, deviceID, activeDatabaseConnectionString;
        static System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<string>> itemUMList = null;
        static SalesLogicExpress.Application.Managers.NotificationManager notificationManager = null;
        private static readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Helpers.ResourceManager");

        public static string DatabaseDirectory
        {
            get
            {
                return databaseDirectory;
            }
            set
            {
                if (string.IsNullOrEmpty(databaseDirectory))
                {
                    databaseDirectory = value;
                }
            }
        }
        public static Managers.NetworkManager NetworkManager
        {
            get
            {
                if (networkManager == null)
                {
                    networkManager = new Managers.NetworkManager();
                }
                return networkManager;
            }
        }
        static ViewModels.CommonNavInfo commonNavigtionInfo = null;

        static ResourceManager()
        {
        }

        public static ViewModels.CommonNavInfo CommonNavInfo
        {
            get
            {
                if (commonNavigtionInfo == null)
                {
                    commonNavigtionInfo = new ViewModels.CommonNavInfo();
                }
                return commonNavigtionInfo;
            }
        }
        //public static AppService.AppServiceClient ServiceClient
        //{
        //    get
        //    {
        //        if (serviceClient == null)
        //        {
        //            serviceClient = new AppService.AppServiceClient();
        //        }
        //        return serviceClient;
        //    }
        //}

        public static SLEStatService.StatServiceClient SleStatServiceClient
        {
            get { return sleServiceClient ?? (sleServiceClient = new SLEStatService.StatServiceClient()); }
        }
        public static Managers.TransactionManager Transaction
        {
            get
            {
                if (transactionManager == null)
                {
                    transactionManager = new Managers.TransactionManager();
                }
                return transactionManager;
            }
        }
        public static Managers.SyncManager Synchronization
        {
            get
            {
                if (syncManager == null)
                {
                    syncManager = new Managers.SyncManager();
                }
                return syncManager;
            }
        }
        public static Managers.QueueManager QueueManager
        {
            get
            {
                if (queueManager == null)
                {
                    queueManager = new Managers.QueueManager();
                }
                return queueManager;
            }
        }
        public static Managers.WebServiceManager WebServiceManager
        {
            get
            {
                if (webServiceManager == null)
                {
                    webServiceManager = new Managers.WebServiceManager();
                }
                return webServiceManager;
            }
        }
        public static string DeviceID
        {
            get
            {
                return deviceID;
            }
            set
            {
                if (string.IsNullOrEmpty(deviceID))
                {
                    deviceID = value;
                }
            }
        }
        public static string ActiveRemoteID
        {
            get
            {
                return remoteID;
            }
            set
            {
                if (string.IsNullOrEmpty(remoteID))
                {
                    remoteID = value;
                }
            }
        }
        public static string ActiveDatabaseConnectionString
        {
            get
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["useDefaultDb"].ToString()) == true)
                {
                    return ConfigurationManager.AppSettings["connectionString"].ToString();
                }
                return activeDatabaseConnectionString;
            }
            set
            {
                activeDatabaseConnectionString = value;
            }
        }
        public static string DomainPath
        {
            get
            {
                return ConfigurationManager.AppSettings["DomainPath"].ToString();
            }
            set
            {
                DomainPath = value;
            }
        }
        public static string DatabaseResourcesDirectory
        {
            get
            {
                return databaseResourcesDirectory;
            }
            set
            {
                if (string.IsNullOrEmpty(databaseResourcesDirectory))
                {
                    databaseResourcesDirectory = value;
                }
            }
        }

        #region Methods for Database Connection instance
        public static SAConnection GetOpenConnectionInstance()
        {
            try
            {
                //log.Info("[SalesLogicExpress.Application.Helpers][ResourceManager][Start:GetOpenConnectionInstance]");

                if (string.IsNullOrEmpty(ActiveDatabaseConnectionString))
                {
                    throw new Exception();
                }
                if (connection == null && !string.IsNullOrEmpty(ActiveDatabaseConnectionString))
                {
                    connection = new SAConnection(ActiveDatabaseConnectionString);
                    connection.Open();
                }
                if (connection.State == System.Data.ConnectionState.Closed || connection.State == System.Data.ConnectionState.Broken)
                {
                    connection.Open();
                }
                //log.Info("[SalesLogicExpress.Application.Helpers][ResourceManager][GetOpenConnectionInstance][ActiveDatabaseConnectionString = " + ActiveDatabaseConnectionString + "]");
                return connection;
            }
            catch (System.Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][ResourceManager][GetOpenConnectionInstance][ExceptionStackTrace = " + ex.StackTrace + "]");
                return null;
            }

        }
        public static void CloseConnectionInstance()
        {
            log.Info("[SalesLogicExpress.Application.Helpers][ResourceManager][Start:CloseConnectionInstance]");
            if (connection == null) return;
            try
            {
                log.Info("[SalesLogicExpress.Application.Helpers][ResourceManager][CloseConnectionInstance][connection.State = " + connection.State + "]");
                if (connection.State == System.Data.ConnectionState.Open || connection.State == System.Data.ConnectionState.Broken)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.Helpers][ResourceManager][CloseConnectionInstance][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }
        #endregion

        public static System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<string>> GetItemUMList
        {
            get
            {
                if (itemUMList == null)
                {
                    itemUMList = new Managers.TemplateManager().GetTemplateUms();
                }
                return itemUMList;
            }
        }

        public static System.Collections.Generic.List<Managers.StatusType> StatusTypes { get; set; }
        public static Managers.NotificationManager NotificationManager
        {
            get
            {
                if (notificationManager == null)
                {
                    notificationManager = new Managers.NotificationManager();
                }
                return notificationManager;
            }
        }
    }
}
