﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
    [DataObject]
    public class ReportDataSource
    {
        public ReportDataSource()
        { }
        public ReportDataSource(object dataObject)
        {
            this.DataObject = dataObject;
        }
        public object DataObject { get; set; }
        public Func<object> Action { get; set; }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual object GetData()
        {
            return DataObject;
        }
    }
}
