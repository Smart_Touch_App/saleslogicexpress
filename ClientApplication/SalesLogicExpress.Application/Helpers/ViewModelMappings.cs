﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections;
using System.Reflection;
using System.ComponentModel;
namespace SalesLogicExpress.Application.Helpers
{
    public sealed class ViewModelMappings
    {
        public class TabView
        {
            // Reference to the classes inside the TAbView class is in the presentation layer, WindowMappings class, where we map 
            //these constants/static fields to the UI tab view names with the help of a dictionary
            public class CustomerDashboard
            {
                public static string Dashboard = "CustomerDashboard_Dashboard";
                public static string Activity = "CustomerDashboard_Activity";
                public static string Notes = "CustomerDashboard_Notes";
                public static string Information = "CustomerDashboard_Information";
                public static string Contacts = "CustomerDashboard_Contacts";
                public static string Pricing = "CustomerDashboard_Pricing";
                public static string Quotes = "QuotesCustomerTab";
                public static string ItemRestrictions = "CustomerDashboard_ItemRestrictions";

            }
            public class ReturnsAndCredits
            {
                public static string CreditProcess = "ReturnsAndCredits_CreditProcess";
                public static string OrdersProcess = "ReturnsAndCredits_OrdersProcess";
                public static string ReturnsProcess = "ReturnsAndCredits_ReturnsProcess";

            }
            public class RouteSettlement
            {
                public static string Transactions = "RouteSettlement_Transactions";
                public static string Settlement = "RouteSettlement_Settlement";
            }
            public class RouteHome
            {
                public static string Replenishment = "RouteHome_Replenishment";
                public static string Dashboard = "RouteHome_Dashboard";
                public static string CycleCount = "RouteHome_CycleCount";
                public static string ARAging = "RouteHome_ARAging";
            }
            public class ServiceRoute
            {
                public static string DailyStop = "TabItemDailyStops";

                public static string Customers = "TabItemCustomers";

                public static string ProspectTab = "TabItemProspects";
            }

            public class Prospects
            {
                public static string Coffee = "CoffeeProspects";
                public static string Serialized = "SerializedProspects";
                public static string ProspectTab = "TabItemProspects";
            }

            public class ProspectHome
            {
                public static string Dashboard = "ProspectHome_DashboardProspectTab";
                public static string Notes = "ProspectHome_NotesProspectTab";
                public static string Information = "InformationProspectTab";
                public static string Contacts = "ContactProspectTab";
                public static string Pricing = "PricingProspectTab";
                public static string Quotes = "QuotesProspectTab";
                public static string ItemRestrictions = "CompetitorInformationProspectTab";
                public static string Activity = "ProspectActivityTab";

            }

        }

        ViewModelMappings()
        {
        }
        public enum View
        {
            None,
            CustomerListing,
            OrderTemplate,
            Order,
            ConfirmAndSign,
            PreviewOrder,
            PickOrder,
            UserLogin,
            ServiceRoute,
            [Description("Customer Home")]
            CustomerHome,
            DeliveryScreen,
            RouteHome,
            PreOrder,
            [Description("Customer Stop Sequencing")]
            CustomerStopSequencingView,
            [Description("Payments & AR")]
            ARPayment,
            [Description("Route Settlement")]
            RouteSettlement,
            PrintPickOrder,
            PreTripVehicleInspection,
            PrintInvoice,
            [Description("Settlement Confirmation")]
            SettlementConfirmation,
            [Description("Settlement Verification")]
            SettlementVerification,
            [Description("Add Load Pick")]
            ReplenishAddLoadPickView,
            [Description("Add Load")]
            ReplenishAddLoadView,
            [Description("Suggestion")]
            ReplenishSggestionView,
            [Description("Suggestion Pick")]
            ReplenishSuggestionPickView,
            CycleCount,
            [Description("Returns And Credits")]
            ReturnsAndCredits,
            [Description("Item Return")]
            SuggestionReturnView,
            [Description("Non Sellable Item Return")]
            HeldReturnView,
            [Description("Item Return Pick")]
            SuggestionReturnPickView,
            [Description("Non Sellable Item Return Pick")]
            HeldReturnPickView,
            ARAging,
            [Description("Item Returns Pick")]
            ReturnOrderPickScreen,
            AcceptAndPrintReturns,
            [Description("Replenishment Unload")]
            ReplenishUnloadView,
            [Description("Replenishment Unload Pick")]
            ReplenishUnloadPickView,
            [Description("Item Returns")]
            ItemReturnsScreen,
            [Description("Order Returns")]
            OrderReturnsScreen,
            ExpensesWindow,
            [Description("Customer Product Quote")]
            CustomerQuote,
            [Description("Prospect Product Quote")]
            ProspectQuote,
            [Description("Prospect Home")]
            ProspectHome,
            MoneyOrderWindow,
            [Description("Quote Pick")]
            ProspectQuotePick,
            [Description("EOD Settlement")]
            RouteSettlementReport

        }
    }
}