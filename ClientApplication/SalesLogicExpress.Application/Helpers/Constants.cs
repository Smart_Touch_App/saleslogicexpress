﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.Helpers
{
    public class Constants
    {

        public class Common
        {
            public const string HoldOrderConfirmation = "Do you want to hold this order?";
            public const string SelectVoidOrderReasonAlert = "Please select a reason to void this order.";
            public const string DeleteItemConfirmation = "Are you sure you want to delete selected items?";
            public const string SelectItemForDeleteAlert = "Please select items to delete.";
            public const string SelectItemToAddAlert = "Please select at least one item to add.";
            public const string AppExitConfirmation = "Are you sure you want to exit the application?";
            public const string AllOrOneItemsExists = "Added item(s) already exist in the list.";
            public const string PartialItemsExists = "Some of the added item(s) already exist in the list.";
            public const string ActivityPending = "To exit the application, \n Please complete the pending activity ";


            //validation messages

            public const string NoteValidationMsg = "Note cannot be empty";
            public const string NameMandatoryMsg = "* Name cannot be blank";
            public const string NameValidationMsg = "Please enter valid name";
            public const string TitleValidationMsg = "Please select the title";
            public const string EmailValidationMsg = "Please enter valid email";
            public const string AreacodeValidationMsg = "Please enter valid code";
            public const string PhoneValidationMsg = "Please enter valid phone number";
            public const string ExtensionValidationMsg = "Please enter valid phone";
            public const string PhoneTypeSelectionMsg = "Please select the phone type";
            public const string EmailTypeSelectionMsg = "Please select the email type";

            //error message
            public const string ItemRestrictionErrorMessage = "The set up for restriction is yet to be done.";
            public const string NoRestrictionMessage = "There are no restricted items for this customer.";

            public const string CreditAmountErrorMessage = "Enter amount less than 100";
            public const string ItemNotAvailable = "Few items are not available in the inventory.";
            public const string ExpenseAmountErrorMsg = "Amount should be less than Cash on hand.";
            public const string FeeAmountErrorMsg = "Fee Amount should be less than Money Order Amount.";
            public const string MoneyOrderNoErrorMsg = "Money Order Number already exists.";
            public const string MoneyOrderNoEmptyMsg = "Money Order Number can not be empty.";
            public const string TotalAmountErrorMsg = "Total should be less than Cash on Hand.";
            public const string SyncFailedMessage = "(Recent Sync Failed)";
            public const string PreTripErrorMsg = "You will get navigated to RouteHome as \nPreTripInspection is not done for this date.";

        }

        public class OrderTemplate
        {

        }

        public class Order
        {

        }
        public class OrderPreview
        {
            public const string SelectReasonForSurchargeAlert = "Please select a reason code.";

        }
        public class PickOrder
        {
            public const string SelectItemToPickAlert = "Please select an item from pick list to manual pick.";
            public const string SelectItemToUnPickAlert = "Please select an item from exception list to remove.";
            public const string SelectReasonForManualPickAlert = "Please select a reason before manual pick.";
            public const string RemoveExceptionConfirmation = "Are you sure you want to remove items?";

        }
        public class CashCollection
        {
            public const string AmountMissMatchAlert = "Total balance amount should match with payment amount.";
            public const string MandatorySignAlert = "Please sign.";
            public const string MandatoryApprovalCodeAlert = "Please enter valid approval code.";
        }
        public class CustomerInformation
        {
            public const string UnableToSaveAlert = "Unable to save.";
            public const string InfoSaveAlert = "Info save successfully.";
            public const string InfoConfirmAlert = "Do you want to save information?";
        }

        public class ProspectInformation
        {
            public const string UnableToSaveAlert = "Unable to save.";
            public const string InfoSaveAlert = "Info save successfully.";
        }
        public class DeliveryScreen
        {

        }
        public class Contacts
        {
            public const string DeleteContactConfirmation = "Are you sure you want to delete selected contact?";
            public const string DeleteContactConfirmationForMany = "Are you sure you want to delete selected contacts?";
            public const string DeleteContactAlertForDefaultContact = "You cannot delete the default contact,\n please change the default and retry?";
            public const string DeleteEmailConfirmation = "Are you sure you want to delete this email?";
            public const string DeletePhoneConfirmation = "Are you sure you want to delete this phone number?";
        }

        public class Notes
        {
            public const string DeleteNoteConfirmation = "Are you sure you want to delete selected note?";
            public const string DeleteNoteConfirmationForMany = "Are you sure you want to delete selected notes?";
        }
        public class PreOrder
        {
            // public const DateTime baseDate = new DateTime(2015, 12, 29);
        }

        public class CustStopSequencing
        {
            public const string ResetConfirmation = "Your changes will be lost. Do you want to Save?";
            public const string Reset = "Your changes will be lost. Do you want to Continue?";
            public const string DayChangeConfirmation = "Your changes will be lost. Do you want to Save?";
            public const string ResetAlter = "No changes!";
            public const string SavedChanges = "Changes Saved Successfully.";
            public const string DeleteEmailConfirmation = "Are you sure you want to delete this email?";
            public const string DeletePhoneConfirmation = "Are you sure you want to delete this phone number?";
        }

        public class RouteSettlement
        {
            public const string PasswordMismatch = "Wrong Password";
            public const string NoSignature = "Signature Please...";
            public const string WrongApproval = "Wrong Approval Code";
            public const string PendimgVerification = "Pending Verification Available";
            public const string ServiceStop = "All Stops are not serviced";
            public const string BackNavigation = " This will clear your settlement details?";
            public const string BackNavigationVerification = " Are you sure you want to cancel the Verification?";
            public const string EODConfirmedValidation = " Verification process for EOD Settlement is pending \n Void it first!";
            public const string EODSettledValidation = " EOD settlement has been completed,\n Void it first!";


        }

        public class PreTripInspection
        {
            public static readonly string ConfirmationStatement_1 = "Corrective action. Prior to requiring or permitting a driver to operate a vehicle, every motor carrier or its agent shall repair any defect or deficiency listed on the driver vehicle inspection report which would be likely to affect the safety of operation of the vehicle.";
            public static readonly string ConfirmationStatement_2 = "1. Every motor carrier or its agent shall certify on the original driver vehicle insection report which lists any defect or deficiency that the defect or deficiency has been repaired or that repair is unnecessary before the vehicle is operated again.";
            public static readonly string ConfirmationStatement_3 = "2. Every motor carrier shall maintain the original driver vehicle inspection report, the certification of the repair, and the certificaiton of the driver's review for three months from the date the written report was prepared.";
            public static readonly string AlertMessageForFutureStop = "You already have a stop for this customer today.";

            public static readonly string SaveTemplateOnBackNavigation = "Do yot want to go back?\n All data will be discarded.";
            public static readonly string NotPassedReasonErrorMessage = "Please enter the reason.";

        }

        public class CustomerDashboard
        {
            public static readonly string AlertStatement = "You're going to service past activity !";


        }
        public class WebServiceManager
        {
            public static readonly DateTime LastUploadTime = new DateTime(2015, 06, 25);
            public static readonly DateTime LastDownloadTime = new DateTime(2015, 06, 25);
            public static readonly DateTime LastSchemaUpdate = new DateTime(2015, 06, 29);
            public static readonly Dictionary<ApplicationGroup, PublicationGroup> PubApplicationMapping = new Dictionary<ApplicationGroup, PublicationGroup>();


            public enum PublicationGroup
            {
                pub_validate_user,
                pub_everything,
                pub_immediate

            }

            public enum ApplicationGroup
            {
                UserDefineCodes,
                Customer,
                Pricing,
                Product
            }
            public WebServiceManager()
            {
                PubApplicationMapping.Add(ApplicationGroup.Customer, PublicationGroup.pub_immediate);
                PubApplicationMapping.Add(ApplicationGroup.Pricing, PublicationGroup.pub_immediate);
                PubApplicationMapping.Add(ApplicationGroup.Product, PublicationGroup.pub_everything);
                PubApplicationMapping.Add(ApplicationGroup.UserDefineCodes, PublicationGroup.pub_everything);
            }
        }

        public class CycleCount
        {
            public static readonly string ConfirmSubmitCycleCount = "Do you want to submit your cycle count?";
            public static readonly string ConfirmResetCycleCount = "Do you want to reset this count?";
            public const string SelectItemToCountAlert = "Please select an item from list to start manual count.";
            public static readonly string WarningForWrongItemPicked = "Your last picked item is not in the cycle count list.\n Please put it back.";
            public static readonly string HoldStatus = " Do you want to hold this count?";
        }
        public class UserLogin
        {
            public const string InValidUserRoute = "User/Route not valid. \n Kindly Contact IT Administration.";
            public const string InternetConnectionRequired = "Internet connection required for initial setup.  \n Please connect to internet and try again..";
            public const string DeviceNotRegistered = "Device is not registered or active. \n Kindly Contact IT Administration.\n Device ID : {0}";
            public const string RouteInactive = "Route is active on other device. \n Kindly Contact IT Administration.";
        }


        /// <summary>
        /// Contains the constant alert messages 
        /// </summary>
        public class RouteHome
        {
            public const string WithoutPretripAlertNavigationWarningMessage = "Pretrip inspection is pending. Do you want \nto open screen in read-only mode.";
            public const string NoStopForCustomerNavigationWarningMessage = "Customer do not have stop today.";
        }

        public class OrderReturns
        {
            public const string I_RMessage = "Do you want to return any more items? \n or continue return for selected items.";

            public const string ConfirmRO = "Do you want to hold or void this transaction?";
        }
        public class PreTrip
        {
            public const string SaveErrorDetailInPretripInspection = "Pre-Trip Inspection details were not saved.\n Please retry !";
            public const string SaveErrorHeaderInPretripInspection = "Warning !";
        }
        public class Competitor
        {
            public const string DeleteProspectConfirmation = "Are you sure you want to delete the prospect?";
            public const string DeleteSerProspectConfirmation = "Are you sure you want to delete the serialized item?";
            public const string DeleteExpProspectConfirmation = "Are you sure you want to delete the expensed item?";
            public const string DeleteProspectConfirmationForMany = "Are you sure you want to delete selected prospects?";
            public const string EditSerProspectConfirmationForMany = "You can edit only one serialized Item";
            public const string EditExpProspectConfirmationForMany = "You can edit only one Expensed Item";
            public const string DeleteSerProspectConfirmationForMany = "Are you sure you want to delete selected serialized Items?";
            public const string DeleteExpProspectConfirmationForMany = "Are you sure you want to delete selected expensed Items?";
            public const string DeleteProspectContactForMany = "Are you sure you want to delete selected Contact(s)?";
        }
        public class Quote
        {
            public const string CustomerQuoteBackNavigationMessage = "Moving back will discard your current quote. \n Do you want to continue?";
            public const string CustomerQuoteBackNavigationMessageInEditMode = "Do you want to save changes?";
            public const string CustomerQuoteDirtyBackNavigationMessage = "Quote does not have any item(s). \nDo you want to discard the changes and continue?";
            public const string CustomerQuoteDirtySaveAlertMessage = "Please add at least one item to save the quote!";
            public const string CustomerQuoteBackNavigationMessagePartialPick = "Pick/Unpick all sample items.";
            public const string CustomerQuoteBackNavigatioWithoutComplete = "Do you want to complete quote?"; 
        }
    }
}
