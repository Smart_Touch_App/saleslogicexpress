﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public abstract class PayloadBase
    {
        string _TransactionID;
        public string TransactionID
        {
            get
            {
                return _TransactionID;
            }
            set
            {
                _TransactionID = value;
            }
        }
        public override string ToString()
        {
            return base.ToString();
        }
        bool isCustomerLoadedForPastActivity = false;

        public bool IsCustomerLoadedForPastActivity
        {
            get { return isCustomerLoadedForPastActivity; }
            set { isCustomerLoadedForPastActivity = value; }
        }

        public DateTime? StopDateBeforeInvokingActivity { get; set; }

    }
}
