﻿using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class OrderPayload : PayloadBase
    {
        List<OrderItem> _Items;
        List<PickOrderItem> _PickedItems;
        Customer _Customer;
        PreOrderStops _PreOrderStops;
        DateTime _StopDate;
        string _RouteID;
        string _InvoiceNo, _OrderID, _Amount, _InvoicePaymentType;
        string _TransactionLastState;
        private string _StopID;
        private bool navigatedForVoid = false;

        public bool NavigatedForVoid
        {
            get { return navigatedForVoid; }
            set { navigatedForVoid = value; }
        }
        public CashDelivery CashDelivery { get; set; }

        public string TransactionLastState
        {
            get
            {
                return _TransactionLastState;
            }
            set
            {
                _TransactionLastState = value;
            }
        }

        public List<OrderItem> Items
        {
            get
            {
                return _Items;
            }
            set
            {
                _Items = value;
            }
        }
        public string Surcharge { get; set; }
        public string TaxAmount { get; set; }
        public List<PickOrderItem> PickOrderItems
        {
            get
            {
                return _PickedItems;
            }
            set
            {
                //Add items to print list only if it has any value
                if (value != null && value.Count > 0)
                {
                    //Clear the list to avoid duplication 
                    this.PickOrderItemsForPrint.Clear();

                    foreach (PickOrderItem item in value)
                    {
                        try
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(PickOrderItem));
                            using (MemoryStream ms = new MemoryStream())
                            {
                                serializer.WriteObject(ms, item);
                                ms.Seek(0, SeekOrigin.Begin);
                                this.PickOrderItemsForPrint.Add((PickOrderItem)serializer.ReadObject(ms));
                            }
                        }
                        catch (Exception)
                        {
                            this.PickOrderItemsForPrint.Add(item);
                        }
                        
                    };
                }

                _PickedItems = value;
            }
        }

        public Customer Customer
        {
            get
            {
                return _Customer;
            }
            set
            {
                _Customer = value;
            }
        }
        public DateTime StopDate
        {
            get
            {
                return _StopDate;
            }
            set
            {
                _StopDate = value;
            }
        }
        public PreOrderStops PreOrderStop
        {
            get
            {
                return _PreOrderStops;
            }
            set
            {
                _PreOrderStops = value;
            }
        }
        public string RouteID
        {
            get
            {
                return _RouteID;
            }
            set
            {
                _RouteID = value;
            }
        }
        public string StopID
        {
            get
            {
                return _StopID;
            }
            set
            {
                _StopID = value;
            }
        }
        public string InvoiceNo
        {
            get
            {
                return _InvoiceNo;
            }
            set
            {
                _InvoiceNo = value;
            }
        }
        public string InvoicePaymentType
        {
            get
            {
                return _InvoicePaymentType;
            }
            set
            {
                _InvoicePaymentType = value;
            }
        }
        public string Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                _Amount = value;
            }
        }
        public string OrderID
        {
            get
            {
                return _OrderID;
            }
            set
            {
                //This property holds item information which is used while printing the pick slip 
                //if _orderid and value does not match, then clear the value 
                if (_OrderID == null || (!_OrderID.Equals(value)))
                {
                    this.PickOrderItemsForPrint.Clear();
                }

                _OrderID = value;
            }
        }
        public string PickOrderTransactionID
        {
            get;
            set;
        }

        private List<PickOrderItem> _PickOrderItemsForPrint = null;


        /// <summary>
        /// Get or set pick order items for displaying data on pick slip
        /// It gets clear as soon as the payload order id changes 
        /// It is set in the pick order items
        /// </summary>
        public List<PickOrderItem> PickOrderItemsForPrint
        {
            get
            {
                if (_PickOrderItemsForPrint == null)
                {
                    _PickOrderItemsForPrint = new List<PickOrderItem>();
                }

                return _PickOrderItemsForPrint;
            }
            set
            {
                _PickOrderItemsForPrint = value;
            }
        }
    }
}