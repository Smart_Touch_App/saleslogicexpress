﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class QuotePayload: PayloadBase
    {
        public string QuoteId { get; set; }

        public List<string> ProspectQuoteIdList { get; set; }

        public List<string> QuoteIdList { get; set; }

        public object Parent { get; set; }
        public bool IsProspect { get; set; }

        public DateTime QuoteDate { get; set; }
    }
}
