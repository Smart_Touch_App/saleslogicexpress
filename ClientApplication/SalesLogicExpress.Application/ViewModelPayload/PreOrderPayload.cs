﻿﻿using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class PreOrderPayload : PayloadBase
    {
        PreOrderStops _PreOrderStopInstance;
        Customer _Customer;
        public PreOrderStops PreOrderStopInstance
        {
            get
            {
                return _PreOrderStopInstance;
            }
            set
            {
                _PreOrderStopInstance = value;
            }
        }

        List<TemplateItem> preorderitems;
        public List<TemplateItem> PreOrderItems
        {
            get
            {
                return preorderitems;
            }
            set
            {
                preorderitems = value;
            }
        }
        public Customer Customer
        {
            get
            {
                return _Customer;
            }
            set
            {
                _Customer = value;
            }
        }
        public DateTime StopDate
        {
            get;
            set;
        }
        public string StopID
        {
            get;
            set;
        }
        public PreOrderStops PreOrderStops { get; set; }
        public DateTime PreOrderCreatedDate { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsFromActivityTab { get; set; }
        public bool IsPreOrderExistForSameDate { get; set; }
        public bool IsPreOrderCreatedFromUnplanned
        {
            get;
            set;
        }
        public bool IsPreOrderForAnotherDate { get; set; }

        public void Reset()
        {
            IsReadOnly = false;
            IsFromActivityTab = false;
            IsPreOrderCreatedFromUnplanned = false;
            IsPreOrderExistForSameDate = false;
            IsPreOrderForAnotherDate = false;
        }
    }
}
