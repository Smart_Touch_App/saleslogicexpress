﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public static class PayloadManager
    {
        static ApplicationPayload _ApplicationPayload;
        static OrderPayload _OrderPayload;
        static PaymentPayload _PaymentPayload;
        static PreOrderPayload _PreOrderPayload;
        static RouteSettlementPayload _RouteSettlementPayload;
        static ChangePayload _ChangePayload;
        static RouteReplenishmentPayload _RouteReplenishmentPayload;
        static ReturnOrderPayload _ReturnOrderPayload;
        static ProspectPayload _ProspectPayload;
        static QuotePayload _QuotePayload = null; 

        public static RouteReplenishmentPayload RouteReplenishmentPayload
        {
            get 
            {
                if (PayloadManager._RouteReplenishmentPayload == null)
                {
                    PayloadManager._RouteReplenishmentPayload = new RouteReplenishmentPayload();
                }
                return PayloadManager._RouteReplenishmentPayload; 
            }
            set { PayloadManager._RouteReplenishmentPayload = value; }
        }
        public static ProspectPayload ProspectPayload
        {
            get
            {
                if (PayloadManager._ProspectPayload == null)
                {
                    PayloadManager._ProspectPayload = new ProspectPayload();
                }
                return PayloadManager._ProspectPayload;
            }
            set { PayloadManager._ProspectPayload = value; }
        }

        public static RouteSettlementPayload RouteSettlementPayload
        {
            get 
            {
                if (PayloadManager._RouteSettlementPayload==null)
                {
                    PayloadManager._RouteSettlementPayload = new RouteSettlementPayload();
                }
                return PayloadManager._RouteSettlementPayload; 
            }
            set { PayloadManager._RouteSettlementPayload = value; }
        }

        public static PreOrderPayload PreOrderPayload
        {
            get
            {
                if (_PreOrderPayload == null) {
                    _PreOrderPayload =  new PreOrderPayload();
                }
                return _PreOrderPayload;
            }
            set
            {
                _PreOrderPayload = value;
            }
        }
        public static ApplicationPayload ApplicationPayload
        {
            get
            {
                if (_ApplicationPayload == null)
                {
                    _ApplicationPayload= new ApplicationPayload();
                }
                return _ApplicationPayload;
            }
            set
            {
                _ApplicationPayload = value;
            }
        }
        public static OrderPayload OrderPayload
        {
            get
            {
                if (_OrderPayload == null)
                {
                    _OrderPayload =  new OrderPayload();
                }
                return _OrderPayload;
            }
            set
            {
                _OrderPayload = value;
            }
        }
        public static PaymentPayload PaymentPayload
        {
            get
            {
                if (_PaymentPayload == null)
                {
                    _PaymentPayload =  new PaymentPayload();
                }
                return _PaymentPayload;
            }
            set
            {
                _PaymentPayload = value;
            }
        }

        /// <summary>
        /// Get or set change payload
        /// </summary>
        public static ChangePayload ChangePayload
        {
            get
            {
                if (_ChangePayload == null)
                {
                    _ChangePayload = new ChangePayload();
                }

                return _ChangePayload;
            }

            set
            {
                _ChangePayload = value;
            }
        }

        public static ReturnOrderPayload ReturnOrderPayload
        {
            get
            {
                if (_ReturnOrderPayload == null)
                {
                    _ReturnOrderPayload = new ReturnOrderPayload();
                } 
                return _ReturnOrderPayload;
            }
            set {_ReturnOrderPayload = value; }
        }

        public static QuotePayload QuotePayload
        {
            get
            {
                if (_QuotePayload == null)
                {
                    _QuotePayload = new QuotePayload();
                }

                return _QuotePayload;
            }

            set
            {
                _QuotePayload = value;
            } 
        }
    }
}
