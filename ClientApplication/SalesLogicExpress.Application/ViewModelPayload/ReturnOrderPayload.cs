﻿using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class ReturnOrderPayload : PayloadBase
    {
        TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get
            {
                return returnItems;//?? new ObservableCollection<ReturnItem>();
            }
            set
            {
                returnItems = value;
            }
        }

        TrulyObservableCollection<ReturnItem> refReturnItems = new TrulyObservableCollection<ReturnItem>();
        public TrulyObservableCollection<ReturnItem> RefReturnItems
        {
            get
            {
                return refReturnItems;//?? new ObservableCollection<ReturnItem>();
            }
            set
            {
                refReturnItems = value;
            }
        }


        TrulyObservableCollection<ReturnItem> returnItemsAll = new TrulyObservableCollection<ReturnItem>();

        public TrulyObservableCollection<ReturnItem> ReturnItemsAll
        {
            get { return returnItemsAll; }
            set { returnItemsAll = value; }
        }

        public string CurrentViewName { get; set; }

        int returnOrderID = -1;
        public int ReturnOrderID
        {
            get
            {
                return returnOrderID;
            }
            set
            {
                returnOrderID = value;
            }
        }


        int salesOrderID;
        public int SalesOrderID
        {
            get
            {
                return salesOrderID;
            }
            set
            {


                salesOrderID = value;
            }
        }

        public string StopID { get; set; }

        bool isROIsInProcess = false;

        private InkCanvas ro_CanvasPayload;

        public InkCanvas RO_CanvasPayload
        {
            get { return ro_CanvasPayload; }
            set { ro_CanvasPayload = value; }
        }

        Customer customer = new Customer();

        public Customer Customer
        {
            get { return customer; }
            set { customer = value; }
        }

        bool _IsReturnOrder = false;
        public bool IsReturnOrder
        {
            get
            {
                return _IsReturnOrder;
            }
            set
            {
                _IsReturnOrder = value;
            }
        }

        decimal totalCoffeeAmount;
        public decimal TotalCoffeeAmount
        {
            get
            {
                return totalCoffeeAmount;
            }
            set
            {
                totalCoffeeAmount = value;
            }
        }


        decimal totalAlliedAmount;
        public decimal TotalAlliedAmount
        {
            get
            {
                return totalAlliedAmount;
            }
            set
            {


                totalAlliedAmount = value;
            }
        }


        decimal orderTotalAmount;
        public decimal OrderTotalAmount
        {
            get
            {
                return orderTotalAmount;
            }
            set
            {


                orderTotalAmount = value;
            }
        }


        float ro_SalesTax;
        public float ROSalesTax
        {
            get
            {
                return ro_SalesTax;
            }
            set
            {
                ro_SalesTax = value;
            }
        }


        decimal invoiceTotalAmt;
        public decimal InvoiceTotalAmt
        {
            get
            {
                return invoiceTotalAmt;
            }
            set
            {


                invoiceTotalAmt = value;
            }
        }


        decimal energySurcharge;
        public decimal EnergySurcharge
        {
            get
            {
                return energySurcharge;
            }
            set
            {


                energySurcharge = value;
            }
        }

        public DateTime StopDate { get; set; }
        public bool ROIsInProgress { get; set; }

        public bool IsFromActivityTab { get; set; }

        public bool NavigateToAcceptAndPrint { get; set; }
        bool _IsOnHold = false;
        public bool IsOnHold
        {
            get
            {
                return _IsOnHold;
            }
            set
            {
                _IsOnHold = value;
            }
        }


        bool pickCompleted;
        public bool PickCompleted
        {
            get
            {
                return pickCompleted;
            }
            set
            {
                pickCompleted = value;
            }
        }


        bool pickIsInProgress =false;
        public bool PickIsInProgress
        {
            get
            {
                return pickIsInProgress;
            }
            set
            {
                pickIsInProgress = value;
            }
        }

        bool isNavigatedForVoid = false;

        public bool IsNavigatedForVoid
        {
            get { return isNavigatedForVoid; }
            set { isNavigatedForVoid = value; }
        }


        bool isROModified;
        public bool IsROModified
        {
            get
            {
                return isROModified;
            }
            set
            {
                isROModified = value;
            }
        }

        int reasonCodeID;
        public int ReasonCodeID
        {
            get
            {
                return reasonCodeID;
            }
            set
            {


                reasonCodeID = value;
            }
        }

        public void Reset()
        {
            ReturnItems.Clear();
            ReturnOrderID = -1;
            IsReturnOrder = false;
            IsROModified = false;
            IsFromActivityTab = false;
            NavigateToAcceptAndPrint = false;
            ROIsInProgress = false;
            PickIsInProgress = false;
            IsNavigatedForVoid = false;
            PickCompleted = false;
            StopDateBeforeInvokingActivity = null;
            IsCustomerLoadedForPastActivity = false;
            ReasonCodeID = 0;
            TotalCoffeeAmount = 0;
            TotalAlliedAmount = 0;
            SalesOrderID = 0;
            OrderTotalAmount = 0;
            ROSalesTax = 0;
            SalesOrderID = 0;
            InvoiceTotalAmt = 0;
            EnergySurcharge = 0;
            TransactionID = null;
            IsOnHold = false;
            StopDate = DateTime.SpecifyKind(StopDate, DateTimeKind.Utc);
        }
    }
}
