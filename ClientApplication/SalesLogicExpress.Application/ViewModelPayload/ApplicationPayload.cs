﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class ApplicationPayload : PayloadBase
    {
        string _Route;
        public string Route
        {
            get
            {
                return _Route;
            }
            set
            {
                _Route = value;
            }
        }
        public string LoggedInUserID { get; set; }

        public DateTime BaseDate { get; set; }
        private bool viewModelLoadComplete = false;

        public bool ViewModelLoadComplete
        {
            get { return viewModelLoadComplete; }
            set { viewModelLoadComplete = value; }
        }


        Dictionary<string, int> ActivityList = new Dictionary<string, int>();
        bool _ActivityStarted, _ActivityCompleted;
        public bool ActivityStarted { get { return _ActivityStarted; } }
        public bool ActivityCompleted { get { return ActivityList.Count == 0; } }
        CurrentActivity StartedActivity;
        public void StartActivity(CurrentActivity Activity)
        {
            StartedActivity = Activity;
            if (!ActivityList.ContainsKey(Activity.ToString()))
            {
                ActivityList.Add(Activity.ToString(), 1);
            }
            else
            {
                ActivityList[Activity.ToString()]= ActivityList[Activity.ToString()]++;
            }
            _ActivityStarted = true;
        }
        public void CompleteActivity(CurrentActivity Activity)
        {
            if (ActivityList.ContainsKey(Activity.ToString()))
            {
                ActivityList[Activity.ToString()] = ActivityList[Activity.ToString()] - 1;
                if (ActivityList[Activity.ToString()] == 0)
                {
                    ActivityList.Remove(Activity.ToString());
                }
            }
        }
        public enum CurrentActivity
        {
            Order,
            Settlement,
            Payment,
            ReturnOrder,
            ReturnItems,
            PreOrder
        }
    }
}
