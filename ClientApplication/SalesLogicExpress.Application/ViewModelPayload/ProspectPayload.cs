﻿using SalesLogicExpress.Domain;
using SalesLogicExpress.Domain.Prospect_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;


namespace SalesLogicExpress.Application.ViewModelPayload
{
    public class ProspectPayload : PayloadBase
    {
        Prospect prospect;

        public Prospect Prospect
        {
            get { return prospect; }
            set { prospect = value; }
        }

        #region Prospect Quote
        private string quoteNo = string.Empty;

        public string QuoteNo
        {
            get { return quoteNo; }
            set { quoteNo = value; }
        }

        private bool navigatedForVoid = false;
        public bool NavigatedForVoid
        {
            get { return navigatedForVoid; }
            set { navigatedForVoid = value; }
        }

        private bool isProspect = false;

        public bool IsProspect
        {
            get { return isProspect; }
            set { isProspect = value; }
        }

        private bool isForPricingTab = false;

        public bool IsForPricingTab
        {
            get { return isForPricingTab; }
            set { isForPricingTab = value; }
        }

        private bool isPartialPicking = false;

        public bool IsPartialPicking
        {
            get { return isPartialPicking; }
            set { isPartialPicking = value; }
        }



        private string navigatedFromTab = string.Empty;
        public string NavigatedFromTab
        {
            get { return navigatedFromTab; }
            set { navigatedFromTab = value; }
        }


        private bool showEditModeButton = true;

        public bool ShowEditModeButton
        {
            get { return showEditModeButton; }
            set { showEditModeButton = value; }
        }
        private bool isPickedItemRemoved = false;

        public bool IsPickedItemRemoved
        {
            get { return isPickedItemRemoved; }
            set { isPickedItemRemoved = value; }
        }


        #endregion


        public bool IsNavigatedFromActivityTab { get; set; }
    }
}
