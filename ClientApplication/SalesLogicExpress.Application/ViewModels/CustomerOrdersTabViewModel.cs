﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ReportViewModels;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerOrdersTabViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerReturnsTabViewModel");

        #region Delegates
        public DelegateCommand SetReturnOrder { get; set; }
        public DelegateCommand SetReturnItems { get; set; }
        public DelegateCommand PrintOrders { get; set; }

        public DelegateCommand ToggleSelectedOrderItem { get; set; }
        public DelegateCommand ToggleSelectedOrder { get; set; }
        #endregion
        #region Prop's

        private bool _IncludeAvailableZeroItems = false;

        /// <summary>
        /// Get or set flag for including zero available qty items
        /// If true, resfresh the data context of grid
        /// </summary>
        public bool IncludeAvailableZeroItems
        {
            get
            {
                return _IncludeAvailableZeroItems;
            }
            set
            {
                _IncludeAvailableZeroItems = value;
                OnPropertyChanged("IncludeAvailableZeroItems");

                IsReturnItemsEnable = false;
                IsPrintEnable = false;
                IsReturnOrderEnable = false;
                this.ReturnOrders.Clear();
                this.LoadOrders();
            }
        }

        bool _IsPrintEnable = false;
        public bool IsPrintEnable
        {
            get
            {
                return _IsPrintEnable;
            }
            set
            {
                _IsPrintEnable = value;
                OnPropertyChanged("IsPrintEnable");
            }
        }
        bool _IsReturnItemsEnable = false;
        public bool IsReturnItemsEnable
        {
            get
            {
                return _IsReturnItemsEnable;
            }
            set
            {
                _IsReturnItemsEnable = value;
                OnPropertyChanged("IsReturnItemsEnable");
            }
        }
        bool _IsReturnOrderEnable = false;
        public bool IsReturnOrderEnable
        {
            get
            {

                return _IsReturnOrderEnable;

            }
            set
            {
                _IsReturnOrderEnable = value;
                OnPropertyChanged("IsReturnOrderEnable");
            }
        }
        public int SelectedOrderId { get; set; }
        TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();
        TrulyObservableCollection<ReturnOrder> returnOrderList = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get
            {
                return returnItems;
            }
            set
            {
                returnItems = value;
                OnPropertyChanged("ReturnItems");
            }
        }


        TrulyObservableCollection<ReturnOrder> returnOrders = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnOrder> ReturnOrders
        {
            get
            {
                if (returnOrders == null)
                {
                    returnOrders = new TrulyObservableCollection<ReturnOrder>();
                }

                return returnOrders;
            }
            set
            {

                returnOrders = value;
                OnPropertyChanged("ReturnOrders");
            }
        }
        bool selectCompleteOrder;

        public bool SelectCompleteOrder
        {
            get { return selectCompleteOrder; }
            set { selectCompleteOrder = value; OnPropertyChanged("SelectCompleteOrder"); }
        }

        public bool IsUnplannedCreatedForFuture { get; set; }
        public Guid MessageToken { get; set; }

        #endregion
        public CustomerOrdersTabViewModel(BaseViewModel ParentView, bool flag)
        {
            IsUnplannedCreatedForFuture = flag;
            this.ParentView = ParentView;
            this.ParentView.IsBusy = true;
            if (PayloadManager.ReturnOrderPayload.IsFromActivityTab)
                this.ParentView.IsDirty = true;

            if (PayloadManager.ReturnOrderPayload.PickIsInProgress)
                this.ParentView.IsDirty = true;
            LoadOrders();
            InitialzeCommands();
        }
        Dictionary<string, bool> CompletelySelectedOrders = new Dictionary<string, bool>();
        private void InitialzeCommands()
        {
            PrintOrders = new DelegateCommand((param) =>
            {
                PrintAsync();
            });

            SetReturnOrder = new DelegateCommand(param =>
            {

            });

            SetReturnItems = new DelegateCommand(param =>
            {
                var r_itemsCollection = new List<ReturnItem>();
                //foreach (var item in (((System.Collections.ObjectModel.ObservableCollection<object>)(param))))
                //{
                //    r_itemsCollection.Add(item as ReturnItem);
                //}
                foreach (var roitem in ReturnOrders)
                {
                    foreach (var ritem in roitem.ReturnItems)
                    {
                        if (ritem.IsSelectedItem)
                        {
                            r_itemsCollection.Add(ritem);
                        }
                    }
                }
                PayloadManager.ReturnOrderPayload.ReturnItems = new TrulyObservableCollection<ReturnItem>();
                foreach (var item in r_itemsCollection)
                {
                    PayloadManager.ReturnOrderPayload.ReturnItems.Add(item);
                }
                PayloadManager.ReturnOrderPayload.IsReturnOrder = IsReturnOrderEnable;
                PayloadManager.ReturnOrderPayload.SalesOrderID = IsReturnOrderEnable ? SelectedOrderId : 0;

                PayloadManager.ReturnOrderPayload.CurrentViewName = PayloadManager.ReturnOrderPayload.IsReturnOrder ? ViewModelMappings.View.OrderReturnsScreen.ToString() : ViewModelMappings.View.ItemReturnsScreen.ToString();

                //Delete previously hold items if ReturnOrder is in progress
                if (PayloadManager.ReturnOrderPayload.ROIsInProgress)
                {
                    new CustomerReturnsManager().DeleteHoldReturnItems(PayloadManager.ReturnOrderPayload.ReturnOrderID);
                }
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ItemReturnsScreen, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
            });
            ToggleSelectedOrderItem = new DelegateCommand(param =>
            {

                if (param == null) return;
                List<object> parameters = param as List<object>;
                var r_item = parameters[0] as ReturnItem;

                var ro_item = ReturnOrders.FirstOrDefault(x => x.OrderId == r_item.OrderID);
                IsReturnItemsEnable = true;
                IsReturnOrderEnable = false;
                IsPrintEnable = false;
                int selectedCount = 0;
                ReturnOrders.ToList().ForEach(x => { x.IsSelected = false; });

                foreach (var roitem in ReturnOrders)
                {
                    foreach (var ritem in roitem.ReturnItems)
                    {
                        selectedCount += ritem.IsSelectedItem ? 1 : 0;
                    }
                }

                IsReturnItemsEnable = selectedCount == 0 ? false : true;
                //if (ro_item != null)
                //{

                //    //if (ro_item.ReturnItems.All(x => x.IsSelectedItem && x.PreviouslyReturnedQty == 0))
                //    //{
                //    //    //ro_item.IsSelected = true;
                //    //    ReturnOrders.Where(x => x.OrderId != r_item.OrderID).ToList().ForEach(x => { x.IsSelected = false; });

                //    //}
                //}
            });
            ToggleSelectedOrder = new DelegateCommand(param =>
            {
                if (param == null) return;
                List<object> parameters = param as List<object>;
                var orderItems = ReturnOrders.Where(ri => ri.OrderId == Convert.ToInt32(parameters[0])).FirstOrDefault().ReturnItems;
                foreach (var item in orderItems)
                {
                    if (item.QtyAvailableToReturn != 0)
                        item.IsSelectedItem = Convert.ToBoolean(parameters[1]);
                }
                ReturnOrders.Where(x => x.OrderId != Convert.ToInt32(parameters[0])).ToList().ForEach(x => { x.IsSelected = false; });

                if (ReturnOrders.Where(ri => ri.OrderId == Convert.ToInt32(parameters[0])).FirstOrDefault().ReturnItems.All(x => x.IsSelectedItem && x.PreviouslyReturnedQty == 0))
                {
                    ReturnOrders.Where(x => x.OrderId != Convert.ToInt32(parameters[0])).ToList().ForEach(x => { x.ReturnItems.ToList().ForEach(i => { i.IsSelectedItem = false; }); });

                    IsReturnOrderEnable = true;
                    IsPrintEnable = true;
                    IsReturnItemsEnable = false;
                    SelectedOrderId = Convert.ToInt32(parameters[0]);
                }
                if (!Convert.ToBoolean(parameters[1]))
                    IsPrintEnable = IsReturnOrderEnable = false;
            });
        }
        async void PrintAsync()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                var payload = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().OrderId.ToString();
                new InvoiceReport(payload);
            });
        }
        async void LoadOrders()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:LoadOrders]");
                try
                {
                    ReturnItems = new TrulyObservableCollection<ReturnItem>(new CustomerReturnsManager().GetItemsForOrdersTab(this.IncludeAvailableZeroItems));

                    if (ReturnOrders != null)
                        ReturnOrders.Clear();

                    var orderIDCollection = ReturnItems.GroupBy(x => x.OrderID).Distinct();
                    orderIDCollection = orderIDCollection.OrderByDescending(x => x.Key);
                    foreach (var item in orderIDCollection)
                    {
                        ReturnOrder ro_item = new ReturnOrder();
                        var ro_items = ReturnItems.Where(x => x.OrderID == Convert.ToInt32(item.Key)).Clone();
                        //var orderDetailCollection = new CustomerReturnsManager().GetOrderDetails(item.Key);

                        foreach (var ro in ro_items)
                        {
                            ro.OrderDateF = ReturnItems.Where(x => x.OrderID == ro.OrderID && x.ItemId == ro.ItemId).FirstOrDefault().OrderDateF;
                            ro_item.ReturnItems.Add(ro as ReturnItem);
                        }
                        ro_item.OrderId = item.Key;
                        var dict = new CustomerReturnsManager().GetOrderFlags(item.Key);
                        if (dict != null)
                        {
                            ro_item.IsSettled = dict["IsSettled"];
                            ro_item.IsOrderReturned = dict["IsOrderReturned"];
                        }

                        ro_item.IsEnabled = (!ro_item.ReturnItems.Any(x => x.OrderQty == x.ReturnQty));
                        ro_item.IsEnabled = !ro_item.ReturnItems.Any(x => x.PreviouslyReturnedQty > 0);
                        ro_item.IsEnabled = new CustomerReturnsManager().IsOrderPartiallyReturn(ro_item.OrderId);

                        ro_item.Surcharge = Convert.ToDecimal(ro_item.ReturnItems[0].EnergySurchargeAmt); // Energy surcharge is taken at item level as it is common for all items which belongs to same order
                        ro_item.OrderDate = ro_item.ReturnItems[0].OrderDateF;
                        ro_item.OrderTotal = ro_item.ReturnItems.Sum(x => x.ExtendedPrice) + (decimal)ro_item.ReturnItems[0].EnergySurchargeAmt + ro_item.ReturnItems.Sum(x => x.TaxAmount);

                        ro_item.ReturnItems.CollectionChanged += ReturnItems_CollectionChanged;
                        if (ro_item.IsOrderReturned)
                        {
                            ro_item.IsEnabled = false;
                            ro_item.ReturnItems.ToList().ForEach(x => { x.IsItemEnabled = false; });

                        }

                        ro_item.TotalItems = ro_item.ReturnItems.Count();
                        ReturnOrders.Add(ro_item);
                    }
                    // List is used as reference to return orders
                    returnOrderList = new TrulyObservableCollection<ReturnOrder>(ReturnOrders.ToList<ReturnOrder>()).Clone();
                    foreach (var returnOrder in returnOrderList)
                    {
                        foreach (var rItem in returnOrder.ReturnItems)
                        {
                            rItem.OrderDateF = ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId && x.OrderID == rItem.OrderID).OrderDateF;
                            rItem.IsSelectedItem = ReturnOrders.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId).IsSelectedItem;
                            rItem.IsItemEnabled = ReturnOrders.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId).IsItemEnabled;

                        }
                    }

                    // Modify elements in existing return order
                    if (PayloadManager.ReturnOrderPayload.ROIsInProgress && PayloadManager.ReturnOrderPayload.ReturnItems.Count != 0)
                    {
                        foreach (var item in PayloadManager.ReturnOrderPayload.ReturnItems)
                        {
                            var ro_item = ReturnOrders.FirstOrDefault(x => x.OrderId == item.OrderID);

                            var R_item_derived = ro_item.ReturnItems.Where(x => x.OrderID == ro_item.OrderId && x.ItemId == item.ItemId).FirstOrDefault();
                            R_item_derived.ReturnQty = item.ReturnQty;
                            R_item_derived.NonSellableQty = item.NonSellableQty;
                            R_item_derived.IsSelectedItem = true;
                            if (R_item_derived.ReturnQty == R_item_derived.OrderQty)
                                R_item_derived.IsItemEnabled = true;
                            //ReturnItems.ElementAt(ReturnItems.IndexOf(ro_item)).IsSelectedItem = true;
                        }
                        IsReturnItemsEnable = true;
                        // Parent View model check to make sure RO is in terminating state
                        ParentView.IsDirty = true;
                    }
                    //ReturnOrders[0].ReturnItems.CollectionChanged 




                    ReturnItems.ItemPropertyChanged += ReturnItems_ItemPropertyChanged;
                    ReturnOrders.ItemPropertyChanged += ReturnOrders_ItemPropertyChanged;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][LoadOrders][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                finally
                {
                    this.ParentView.IsBusy = false;
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:LoadOrders]");
            });

        }

        private void ReturnOrders_ItemPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void ReturnItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }


        void ReturnItems_ItemPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][Start:ReturnItems_ItemPropertyChanged]");
            try
            {
                // Enable/Disable UI Buttons for Print/ReturnOrder/ReturnItems
                if (e.PropertyName.Equals("IsSelectedItem"))
                {
                    var items = ReturnItems.GroupBy(l => l.OrderID)
                              .Select(lg =>
                                    new
                                    {
                                        ItemCount = lg.Count(),
                                        TotalSelected = lg.Count(w => w.IsSelectedItem == true),
                                        ReturnedQty = lg.Count(w => w.PreviouslyReturnedQty > 0)
                                    });
                    var totalItemIn = items.Count(i => i.TotalSelected > 0);
                    var totallySelected = items.Count(i => i.ItemCount == i.TotalSelected && i.ReturnedQty == 0);
                    var newOrderSelected = items.Count(i => i.ItemCount == i.TotalSelected && i.ReturnedQty == 0);
                    IsReturnOrderEnable = totalItemIn == totallySelected && totalItemIn == 1 && newOrderSelected == 1 ? true : false;
                    IsReturnItemsEnable = totalItemIn > 0 && !IsReturnOrderEnable ? true : false;
                    IsPrintEnable = totalItemIn == 1;

                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][ReturnItems_ItemPropertyChanged][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerOrdersTabViewModel][End:ReturnItems_ItemPropertyChanged]");
        }

        internal void Search(string searchTerm)
        {
            if (searchTerm.Trim().Length > 2)
            {
                SearchItems(searchTerm);
            }
            if (searchTerm.Trim().Length == 0)
            {
                ResetSearch();
            }
        }
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        public async void SearchItems(string searchTerm)
        {
            await Task.Run(() =>
           {
               System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
               {
                   if (searchTerm.Trim().Length > 2 || searchTerm.Trim().Length == 0)
                   {
                       //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                       foreach (var returnOrder in ReturnOrders)
                       {
                           var refROItem = returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId);
                           returnOrder.ReturnItems = new ObservableCollection<ReturnItem>(refROItem.ReturnItems.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                       }
                   }
               }));
           }, tokenForCancelTask.Token);
        }
        internal void ResetSearch()
        {
            //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.ToList<ReturnItem>());
            foreach (var returnOrder in ReturnOrders)
            {
                returnOrder.ReturnItems.Clear();
                returnOrder.ReturnItems = returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.Clone();
                for (int index = 0; index < returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems.Count(); index++)
                {
                    returnOrder.ReturnItems[index].OrderDateF = returnOrderList.FirstOrDefault(x => x.OrderId == returnOrder.OrderId).ReturnItems[index].OrderDateF;
                }

            }
        }
        private void FilterCurrentOrders()
        {
            if (ReturnOrders.Count() > 0)
            {
                foreach (var ro_item in ReturnOrders)
                {
                    foreach (var r_Item in ro_item.ReturnItems)
                    {
                        if (r_Item.QtyAvailableToReturn == 0)
                            ro_item.ReturnItems.Remove(r_Item);
                    }
                }
            }
        }
    }
}
