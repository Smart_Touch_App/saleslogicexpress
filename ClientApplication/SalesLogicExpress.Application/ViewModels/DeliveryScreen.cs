﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Linq;
using System.Data;
using System.Text;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using System.Threading;
using System.Windows.Ink;
using System.Collections.Specialized;
using System.IO;
using System.Windows.Markup;
using System.Windows.Controls;
using log4net;
using SalesLogicExpress.Domain;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Application.Managers;



namespace SalesLogicExpress.Application.ViewModels
{
    public class DeliveryScreen : BaseViewModel
    {
        private readonly ILog logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.DeliveryScreen");
        public DeliveryScreen()
        {
            IsBusy = true;
            InitializeCommands();
            if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter != null)
            {
                IsPaymentTermChanged = Convert.ToBoolean(SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter);
            }
            LoadData();
            if (PayloadManager.OrderPayload.TransactionLastState == ActivityKey.OrderDelivered.ToString())
            {
                IsInDeliverOrderMode = true;
            }
            if (PayloadManager.OrderPayload.TransactionLastState !=null && PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtDeliverToCustomer.ToString())
            {
                PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
            }
            RenderSign();

            if ((!string.IsNullOrEmpty(PayloadManager.OrderPayload.TransactionLastState)) && PayloadManager.OrderPayload.TransactionLastState.Equals(ActivityKey.AcceptOrderOnDelivery.ToString()))
            {
                SignatureCanvasVM.CanClearCanvas = false;
                PayloadManager.OrderPayload.TransactionLastState = string.Empty;//Reset flag
            }

            //Place this below line, after click of accept
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = false;
        }
        private void RenderSign()
        {
            SignatureCanvasVM = new CanvasViewModel(Flow.OrderLifeCycle, SignType.OrderSign, Order.OrderId, IsInDeliverOrderMode)
            {
                Height = 245,
                Width = 300,
                FileName = "SmallPic.jpg",
                FooterText = "Customer Signature",
            };
        }

        void LogActivity(ActivityKey key)
        {
            PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;
            PayloadManager.OrderPayload.OrderID = Order.OrderId.ToString();
            PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();

            Activity ac = new Activity
            {
                ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                RouteID = PayloadManager.OrderPayload.RouteID,
                ActivityType = key.ToString(),
                ActivityStart = DateTime.Now,
                IsTxActivity = true,
                ActivityDetailClass = PayloadManager.OrderPayload.ToString(),
                ActivityDetails = PayloadManager.OrderPayload.SerializeToJson()
            };
            if (key == ActivityKey.HoldAtDeliverToCustomer || key == ActivityKey.OrderDelivered)
            {
                ac.ActivityEnd = DateTime.Now;
                PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
            }
            Activity a = ResourceManager.Transaction.LogActivity(ac);
            if (a != null && a.ActivityHeaderID != null)
            {
                PayloadManager.OrderPayload.TransactionID = a.ActivityHeaderID;
            }
        }
        async void LoadData()
        {
            await Task.Run(() =>
            {
                OrderId = Order.OrderId;
                ObservableCollection<Models.OrderItem> DeliveryScreenCollection = new ObservableCollection<OrderItem>(PayloadManager.OrderPayload.Items);
                DeliveryScreenItems = DeliveryScreenCollection;
                //_SignStroke = new StrokeCollection();
                GetAllTotals();
                IsBusy = false;
            });
        }
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Managers.OrderManager");

        #region Command Declarations
        public DelegateCommand PickOrder { get; set; }
        public DelegateCommand ValidateAndSaveSign { get; set; }
        public DelegateCommand ClearCanvas { get; set; }
        public DelegateCommand GetReasonCode { get; set; }
        public DelegateCommand OrderDelivered { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand BackToCustomerHome { get; set; }
        public DelegateCommand BackToDailyStops { get; set; }
        public DelegateCommand HoldOrder { get; set; }

        #endregion
        string strDateTime = DateTime.Now.ToString("M'/'dd'/'yyyy' - 'hh:mm ") + DateTime.Now.ToString("tt").ToLower();
        private int _OrderId;
        private ObservableCollection<Models.OrderItem> _DeliveryScreen = new ObservableCollection<Models.OrderItem>();
        private bool _IsCanvasEnable = true;
        public bool IsInDeliverOrderMode = false;
        # region Properties
        public ReasonCode ReasonCodeList = new ReasonCode(string.Empty);
        public int OrderId
        {
            get { return Order.OrderId; }
            set { _OrderId = value; }
        }
        public CanvasViewModel SignatureCanvasVM { get; set; }

        public bool IsCanvasEnable
        {
            get { return _IsCanvasEnable; }
            set
            {
                _IsCanvasEnable = value;
                OnPropertyChanged("IsCanvasEnable");

            }
        }
        public Boolean IsPaymentTermChanged { get; set; }
        public bool IsSignPresent { get; set; }

        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");

            }
        }

        public Guid MessageToken { get; set; }
        public string CurrentDate
        {
            get { return strDateTime; }
        }

        private bool _ToggleSignPanelVisibility = false;
        public bool ToggleSignPanelVisibility
        {
            get
            {
                return _ToggleSignPanelVisibility;

            }
            set
            {
                _ToggleSignPanelVisibility = value;
                OnPropertyChanged("ToggleSignPanelVisibility");
            }
        }

        // OrderItem Collection for DeliveryScreen Grid
        // public ObservableCollection<Models.TemplateItem> PreviewItems
        public ObservableCollection<Models.OrderItem> DeliveryScreenItems
        {
            get
            {

                ///  this.previewitems = new Managers.TemplateManager().GetTemplateItemsForCustomer("1108911");
                return _DeliveryScreen;
            }
            set
            {
                _DeliveryScreen = value;
                OnPropertyChanged("DeliveryScreenItems");
            }
        }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        #endregion

        # region Properties For Textbox Values

        float varTotalCoffeeAmtValue, varTotalAlliedAmtValue, varOrderTotalValue, varEnergySurchargeValue, varSalesTaxValue, varInvoiceTotalValue;
        private string _TotalCoffeeAmtValue = "0.00", _TotalAlliedAmtValue = "0.00",
                       _OrderTotalValue = "0.00", _EnergySurchargeValue = "0.00",
                       _SalesTaxValue = "0.00", _InvoiceTotalValue = "0.00";
        public string TotalCoffeeAmtValue
        {
            get
            {
                return "$" + _TotalCoffeeAmtValue;
            }
            set
            {
                _TotalCoffeeAmtValue = value;
                OnPropertyChanged("TotalCoffeeAmtValue");
            }
        }

        public string TotalAlliedAmtValue
        {
            get { return "$" + _TotalAlliedAmtValue; }
            set { _TotalAlliedAmtValue = value; OnPropertyChanged("TotalAlliedAmtValue"); }
        }

        public string OrderTotalValue
        {
            get { return "$" + _OrderTotalValue; }
            set
            {
                _OrderTotalValue = value;
                OnPropertyChanged("OrderTotalValue");
            }
        }

        public string EnergySurchargeValue
        {
            get { return "$" + _EnergySurchargeValue; }
            set
            {
                _EnergySurchargeValue = value;
                OnPropertyChanged("EnergySurchargeValue");
            }
        }
        public string SalesTaxValue
        {
            get { return "$" + _SalesTaxValue; }
            set { _SalesTaxValue = value; OnPropertyChanged("SalesTaxValue"); }
        }
        public string InvoiceTotalValue
        {
            get { return "$" + _InvoiceTotalValue; }
            set { _InvoiceTotalValue = value; OnPropertyChanged("InvoiceTotalValue"); }
        }

        #endregion


        #region methods
        // Synchronize to calculate values on right panel
        async void GetAllTotals()
        {
            await Task.Run(() =>
            {
                try
                {
                    DataTable dset = new DataTable();

                    dset = DbEngine.ExecuteDataSet("select * from BUSDTA.ORDER_HEADER where  OrderID =" + "'" + OrderId + "'").Tables[0];
                    foreach (DataRow dr in dset.Rows)
                    {
                        varEnergySurchargeValue = (float)Convert.ToDecimal(dr["EnergySurchargeAmt"]);
                        EnergySurchargeValue = varEnergySurchargeValue.ToString("0.00");
                        varInvoiceTotalValue = (float)Convert.ToDecimal(dr["InvoiceTotalAmt"]);
                        InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");
                        varOrderTotalValue = (float)Convert.ToDecimal(dr["OrderTotalAmt"]);
                        OrderTotalValue = varOrderTotalValue.ToString("0.00");
                        varSalesTaxValue = (float)Convert.ToDecimal(dr["SalesTaxAmt"]);
                        SalesTaxValue = varSalesTaxValue.ToString("0.00");
                        varTotalAlliedAmtValue = (float)Convert.ToDecimal(dr["TotalAlliedAmt"]);
                        TotalAlliedAmtValue = varTotalAlliedAmtValue.ToString("0.00");
                        varTotalCoffeeAmtValue = (float)Convert.ToDecimal(dr["TotalCoffeeAmt"]);
                        TotalCoffeeAmtValue = varTotalCoffeeAmtValue.ToString("0.00");

                    }
                }
                catch (Exception)
                {
                    log.Error("Error occured in GetAllTotals(): Method");
                }
            });
        }
        #endregion

        void UpdateActivityStatus(string stopID, bool isPendingAct, bool isIncrement, string stopDate)
        {
            string query1 = "[DeliveryScreen:-> UpdateActivityStatus --> Start : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "";
            query1 += "\n stopID :" + stopID + "\t isPendingAct :" + isPendingAct + "isIncrement :" + isIncrement + " stopDate :" + stopDate + "";
            logger.Info(query1);

            new CustomerManager().UpdateSequenceNo(stopID, stopDate);
            logger.Info("Before call to UpdateActivityCount");
            new CustomerManager().UpdateActivityCount(stopID, isPendingAct, isIncrement);
            logger.Info("After call to UpdateActivityCount");

            PayloadManager.OrderPayload.Customer.HasActivity = true;
            if (isPendingAct)
            {
                PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                //PayloadManager.OrderPayload.Customer.PendingActivity += 1;
            }
            else
            {
                PayloadManager.OrderPayload.Customer.CompletedActivity += 1;
                PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
            }
            PayloadManager.OrderPayload.Customer.SaleStatus = "";
            new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
            query1 = "[DeliveryScreen:-> UpdateActivityStatus --> End : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "";
            query1 += "\n stopID :" + stopID + "\t isPendingAct :" + isPendingAct + "isIncrement :" + isIncrement + " stopDate :" + stopDate + "";
            logger.Info(query1);
        }
        private void InitializeCommands()
        {
            VoidOrder = new DelegateCommand((param) =>
          {

              if (ReasonCodeList != null)
                  Order.VoidOrderReasonId = ReasonCodeList.Id;
              // Set reason for void Order if not available 
              if (Order.VoidOrderReasonId == 0)
              {
                  ReasonCodeList.ParentViewModel = this;
                  ReasonCodeList.MessageToken = MessageToken;
                  ReasonCodeList.ActivityKey = ActivityKey.VoidAtDeliverCustomer;
                  var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = ReasonCodeList };
                  Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                  if (OpenDialog.Cancelled)
                  {
                      OrderStateChangeArgs arg = new OrderStateChangeArgs();
                      arg.State = OrderState.None;
                      ReasonCodeList.OnStateChanged(arg);
                  }
                  return;
              }
              else
              {
                  // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0

                  Order.VoidOrderReasonId = 0;
              }
          });

            BackToCustomerHome = new DelegateCommand((param) =>
            {
                var moveToView = new Helpers.NavigateToView
                {
                    NextViewName = ViewModelMappings.View.CustomerHome,
                    CurrentViewName = ViewModelMappings.View.DeliveryScreen,
                    CloseCurrentView = true,
                    ShowAsModal = false,
                    Refresh = true
                };
                if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                }
                else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_DashBoard.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
                }               
                moveToView.SelectTab = SelectedTab;
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            BackToDailyStops = new DelegateCommand((param) =>
            {
                var moveToView = new Helpers.NavigateToView
                {
                    NextViewName = ViewModelMappings.View.ServiceRoute,
                    CurrentViewName = ViewModelMappings.View.DeliveryScreen,
                    CloseCurrentView = true,
                    ShowAsModal = false,
                    Refresh = true
                };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            HoldOrder = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                SignatureCanvasVM.UpdateSignatureToDB(false, false, true, "test.jpg");
                LogActivity(ActivityKey.HoldAtDeliverToCustomer);
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.HoldAtDeliverToCustomer);

                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.OrderPayload.Customer.StopID;
                }
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                logger.Info("[DeliveryScreen:-> HoldOrder --> Before UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");
                UpdateActivityStatus(stopID, true, true, tempStopDate);
                logger.Info("[DeliveryScreen:-> HoldOrder --> After UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");

                #region Update CommittedQty
                // Log commited qty to inventory_ledger and update inventory
                var objInv = new Managers.InventoryManager();
                if (PayloadManager.OrderPayload.Items != null)
                    objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), StatusTypesEnum.HOLD);
                #endregion

                #region Update tem squncing no.
                //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                DateTime todayDate = Convert.ToDateTime(tempStopDate);
                ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                PayloadManager.OrderPayload.Customer.CustName = PayloadManager.OrderPayload.Customer.Name;
                #endregion
                //ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.HoldOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
            ShowInvoiceReport = new DelegateCommand((param) =>
           {
               var payload = Order.OrderId;
               var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PrintInvoice, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = false, Payload = payload, ShowAsModal = false };
               Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
           });
            ShowARPayment = new DelegateCommand((param) =>
         {
             PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();
             SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = "AutoCheckInvoice";

             SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
             SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
             SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Payments & AR";
             SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
             SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
             SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ARPayment;
             var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ARPayment, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
             Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
         });
            OrderDelivered = new DelegateCommand((param) =>
            {

                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.OrderPayload.StopDate.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.OrderPayload.StopID;
                }
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                logger.Info("[DeliveryScreen:-> OrderDelivered --> Before UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");
                UpdateActivityStatus(stopID, false, true, tempStopDate);
                logger.Info("[DeliveryScreen:-> OrderDelivered --> After UpdateActivityStatus : \n Completed Activity :" + PayloadManager.OrderPayload.Customer.CompletedActivity + "\t PendingActivity : " + PayloadManager.OrderPayload.Customer.CompletedActivity + "");

                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.OrderDelivered);
                if (PayloadManager.OrderPayload.TransactionLastState != "OrderDelivered")
                {
                    LogActivity(ActivityKey.OrderDelivered);
                }
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = false;
                #region Update CommittedQty
                // Log commited qty to inventory_ledger and update inventory
                var objInv = new Managers.InventoryManager();
                if (PayloadManager.OrderPayload.Items != null && PayloadManager.OrderPayload.TransactionLastState != "OrderDelivered")
                    objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), StatusTypesEnum.DLVRD);

                #region Update temp sequencing no.
                //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                DateTime todayDate = Convert.ToDateTime(tempStopDate);
                ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                #endregion

                #endregion
            });

            PickOrder = new DelegateCommand((param) =>
             {
                 var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.DeliveryScreen, CloseCurrentView = false, ShowAsModal = false };
                 Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
             });

            ValidateAndSaveSign = new DelegateCommand((param) =>
            {
                SignatureCanvasVM.UpdateSignatureToDB(true, true, true, "test.jpg");
                SignatureCanvasVM.CanClearCanvas = false;
                SignatureCanvasVM.IsCanvasEnable = false;

                //Store the current state 
                //It help in disbling the canvas clear button which get enabled when clicked on print button
                PayloadManager.OrderPayload.TransactionLastState = ActivityKey.AcceptOrderOnDelivery.ToString();
            });
            ClearCanvas = new DelegateCommand((param) =>
            {
                //SignStroke.Clear();
                IsCanvasEnable = true;
            });

        }

        public string _SignPanelHeaderText;
        private StrokeCollection _SignStroke;

        public string SignPanelHeaderText { get { return _SignPanelHeaderText; } set { _SignPanelHeaderText = value; OnPropertyChanged("SignPanelHeaderText"); } }

        public double canvasWidth { get; set; }

        public double canvasHeight { get; set; }

        public InkCanvas InkCanvas { get; set; }

        public bool LoadSignState { get; set; }

        public DelegateCommand ShowInvoiceReport { get; set; }
        public DelegateCommand ShowARPayment { get; set; }
    }
}
