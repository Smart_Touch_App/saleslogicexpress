﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{   
    public class PickOrderWithPickManager : ViewModelBase, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PickOrder");

        #region DelegateCommand
        public DelegateCommand ScanByDevice { get; set; }
        public DelegateCommand HoldPickList { get; set; }
        public DelegateCommand VerifyPickOrder { get; set; }
        public DelegateCommand ManualPick { get; set; }
        public DelegateCommand GetManualPickReasonList { get; set; }
        public DelegateCommand RemoveExceptions { get; set; }
        public DelegateCommand DeliverToCustomer { get; set; }
        public DelegateCommand SwitchScannerMode { get; set; }
        public DelegateCommand PickByKeyboard { get; set; }
        public DelegateCommand SetManualPickReason { get; set; }
        public DelegateCommand VoidOrder { get; set; }
        public DelegateCommand SetPickItemLabels { get; set; }
        public DelegateCommand ChargeOnAccount { get; set; }
        public DelegateCommand AcceptChargeOnAccount { get; set; }
        public DelegateCommand ShowPickSlipReport { get; set; }
        public DelegateCommand ShowInvoiceReport { get; set; }
        public DelegateCommand DeclineChargeOnAccount { get; set; }
        public DelegateCommand ClearSignInChargeOnAccount { get; set; }
        public ReasonCode selectedReason;


        #endregion
        void LogActivity(ActivityKey key)
        {
            LogActivity(key, false);
        }
        async void LogActivity(ActivityKey key, bool logEndTime)
        {
            await Task.Factory.StartNew(() =>
            {
                PayloadManager.OrderPayload.OrderID = Order.OrderId.ToString();
                PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();
                if (key == ActivityKey.HoldAtCashCollection)
                {
                    PayloadManager.OrderPayload.CashDelivery = CashDeliveryContext;
                }
                SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
                {
                    ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                    CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                    RouteID = PayloadManager.OrderPayload.RouteID,
                    ActivityType = key.ToString(),
                    ActivityStart = DateTime.Now,
                    //ActivityEnd = logEndTime ? DateTime.Now : DateTime.MinValue,
                    IsTxActivity = true,
                    ActivityDetailClass = PayloadManager.OrderPayload.ToString(),
                    ActivityDetails = PayloadManager.OrderPayload.SerializeToJson()
                };
                if (key == ActivityKey.PickComplete)
                {
                    ac.ActivityHeaderID = PayloadManager.OrderPayload.PickOrderTransactionID;
                }
                if (key == ActivityKey.PickOrder)
                {
                    ac.ActivityHeaderID = PayloadManager.OrderPayload.TransactionID;
                }
                if (key == ActivityKey.PickItem)
                {
                    ac.ActivityHeaderID = PayloadManager.OrderPayload.PickOrderTransactionID;
                }
                if (key == ActivityKey.HoldAtPick || key == ActivityKey.HoldAtCashCollection || key == ActivityKey.VoidAtCashCollection)
                {
                    ac.ActivityEnd = DateTime.Now;
                }
                switch (key)
                {
                    case ActivityKey.HoldAtPick:
                        PayloadManager.OrderPayload.TransactionLastState = ActivityKey.HoldAtPick.ToString();
                        break;
                    case ActivityKey.HoldAtCashCollection:
                        PayloadManager.OrderPayload.TransactionLastState = ActivityKey.HoldAtCashCollection.ToString();
                        break;
                    case ActivityKey.VoidAtCashCollection:
                        PayloadManager.OrderPayload.TransactionLastState = ActivityKey.VoidAtCashCollection.ToString();
                        break;
                }
                ac.ActivityDetails = PayloadManager.OrderPayload.SerializeToJson();
                SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac);
                if (key == ActivityKey.HoldAtPick || key == ActivityKey.HoldAtCashCollection || key == ActivityKey.VoidAtCashCollection)
                {
                    PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                }
                if (a != null && a.ActivityHeaderID != null)
                {
                    if (key == ActivityKey.PickItem)
                    {
                    }
                    if (key == ActivityKey.PickOrder || key == ActivityKey.PickComplete)
                    {
                        PayloadManager.OrderPayload.PickOrderTransactionID = a.ActivityID;
                    }
                    //PayloadManager.OrderPayload.TransactionID = a.ActivityHeaderID;
                }
            });
        }
        void LogPaymentActivity(ActivityKey key, string receiptId)
        {
            PayloadManager.PaymentPayload.InvoiceNo.Add(Order.OrderId.ToString());
            PayloadManager.PaymentPayload.CashDelivery = CashDeliveryContext;
            PayloadManager.PaymentPayload.Customer = CommonNavInfo.Customer;
            PayloadManager.PaymentPayload.RouteID = PayloadManager.OrderPayload.RouteID;

            //PayloadManager.PaymentPayload.TransactionID = PayloadManager.OrderPayload.TransactionID; 

            if (!string.IsNullOrEmpty(receiptId))
                PayloadManager.PaymentPayload.TransactionID = "receipt#" + receiptId;

            SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
            {
                ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                CustomerID = PayloadManager.PaymentPayload.Customer.CustomerNo,
                RouteID = PayloadManager.PaymentPayload.RouteID,
                StopInstanceID = PayloadManager.PaymentPayload.Customer.StopID,
                ActivityType = key.ToString(),
                ActivityStart = DateTime.Now,
                ActivityEnd = DateTime.Now,
                IsTxActivity = true,
                ActivityDetailClass = PayloadManager.PaymentPayload.ToString(),
                ActivityDetails = PayloadManager.PaymentPayload.SerializeToJson()
            };
            SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac);
            PayloadManager.PaymentPayload.TransactionID = a.ActivityHeaderID;
        }
        #region Contructors

        public PickOrderWithPickManager()
        {
            try
            {
                IsBusy = true;
                InitializeCommands();
                PickOrderItems.CollectionChanged += PickOrderItems_CollectionChanged;
                PickOrderItems.ItemPropertyChanged += PickOrderItems_ItemPropertyChanged;
                selectedReason = new ReasonCode("");
                pickManager = new Managers.PickManager();

                if (PayloadManager.OrderPayload.PickOrderTransactionID != null)
                {
                    //UnHOLD Order if HOLD 
                    pickManager.HoldUnHoldPickList(Order.OrderId.ToString(), false);
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.OrderPayload.Customer.StopID;
                    }
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                }

                LoadData();


                if (PayloadManager.OrderPayload.PickOrderTransactionID == null)
                {
                    LogActivity(ActivityKey.PickOrder);
                }
                if (PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtCashCollection.ToString())
                {
                    CanOpenCashCollection = true;
                    CashDeliveryContext = PayloadManager.OrderPayload.CashDelivery;
                }
                if (PayloadManager.OrderPayload.TransactionLastState != null && (PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtPick.ToString() || PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtCashCollection.ToString()))
                {
                    PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                    PayloadManager.OrderPayload.TransactionLastState = null;
                }
            }
            catch (Exception e)
            {
                Logger.Error("SalesLogicExpress.ViewModels.PickOrder, PickOrder()  Message: " + e.StackTrace);
            }
        }

        async void LoadData()
        {
            await Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.DataBind,
                  new Action(() =>
                  {
                      try
                      {
                          pickManager = new Managers.PickManager();
                          ObservableCollection<Models.OrderItem> orderItemsCollectionFromOrder = new ObservableCollection<Models.OrderItem>(PayloadManager.OrderPayload.Items);

                          ObservableCollection<Models.PickOrderItem> PickorderItemsCollectionFromDB = pickManager.GetPickItemsFromDB(Order.OrderId.ToString());
                          ObservableCollection<Models.PickOrderItem> PickorderItemsCollectionFromOrder = new ObservableCollection<PickOrderItem>();

                          foreach (Models.OrderItem orderItem in orderItemsCollectionFromOrder)
                          {
                              // Code added to replace pricing manager for uom Manager
                              // orderItem.UMConversionFactor = new Managers.PricingManager().jdeUOMConversion(orderItem.UM, orderItem.PrimaryUM, Convert.ToInt32(orderItem.ItemId));

                              if (UoMManager.ItemUoMFactorList != null)
                              {
                                  var item = orderItem;
                                  var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == item.ItemId.Trim()) && (x.FromUOM == item.UM.ToString()) && (x.ToUOM == item.PrimaryUM));
                                  if (itemUomConversion == null)
                                      orderItem.UMConversionFactor = UoMManager.GetUoMFactor(item.UM.ToString(), item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim());
                                  else
                                      orderItem.UMConversionFactor = itemUomConversion.ConversionFactor;
                              }
                              else
                              {
                                  orderItem.UMConversionFactor = UoMManager.GetUoMFactor(orderItem.UM, orderItem.PrimaryUM, Convert.ToInt32(orderItem.ItemId.Trim()), orderItem.ItemNumber.Trim());
                              }
                              PickOrderItem pickOrderItem = new Models.PickOrderItem(orderItem);
                              PickorderItemsCollectionFromOrder.Add(pickOrderItem);
                          }


                          bool IsPickListExists = pickManager.IsPickListCreated(Order.OrderId.ToString());
                          string PickStatus = pickManager.GetPickListStatus(Order.OrderId.ToString());
                          List<string> itemsToDelete = new List<string>();
                          ExceptionItems = pickManager.GetExceptionList(Order.OrderId.ToString());
                          if (IsPickListExists && PickStatus != "Hold")
                          {

                              foreach (Models.PickOrderItem item in PickorderItemsCollectionFromOrder)
                              {
                                  // New item added from Order
                                  if (!PickorderItemsCollectionFromDB.Any(i => i.ItemNumber == item.ItemNumber))
                                  {
                                      if (ExceptionItems.Count > 0)
                                      {
                                          PickOrderItem exItem = ExceptionItems.FirstOrDefault(i => i.ItemNumber.Trim() == item.ItemNumber.Trim());
                                          if (exItem != null)
                                          {
                                              item.ExceptionReason = item.OrderQty < exItem.ExceptionQtyInOrderUOM ? "Over Picked" : "Valid";
                                              if (item.OrderQty < exItem.ExceptionQtyInOrderUOM)
                                              {
                                                  item.PickedQuantityInPrimaryUOM = exItem.PickedQuantityInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                  item.ExceptionQtyInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                  pickManager.SaveException(Order.OrderId.ToString(), item, CommonNavInfo.RouteID.ToString());
                                              }
                                              if (item.OrderQty >= exItem.ExceptionQtyInOrderUOM)
                                              {
                                                  item.PickedQuantityInPrimaryUOM = exItem.PickedQuantityInPrimaryUOM = exItem.ExceptionQtyInPrimaryUOM;
                                                  pickManager.DeleteException(Order.OrderId.ToString(), item);
                                              }
                                          }
                                      }
                                      pickManager.InsertPickItem(Order.OrderId.ToString(), item, CommonNavInfo.RouteID.ToString());
                                  }
                              }
                              //PickorderItemsCollectionFromDB = pickManager.GetPickItemsFromDB(Order.OrderId.ToString());
                              bool isInException = false;
                              Models.PickOrderItem exceptionItem = null;
                              foreach (Models.PickOrderItem item in PickorderItemsCollectionFromDB)
                              {
                                  //exceptionItem = ExceptionItems.FirstOrDefault(eitem => eitem.ItemNumber.Trim() == item.ItemNumber.Trim());
                                  //isInException = exceptionItem != null ? true : false;

                                  // Existing item deleted from Order
                                  if (!PickorderItemsCollectionFromOrder.Any(i => i.ItemNumber == item.ItemNumber)) //&& item.PickedQuantityInPrimaryUOM != 0
                                  {


                                      itemsToDelete.Add(item.ItemNumber);
                                      if (item.PickedQuantityInPrimaryUOM != 0)
                                      {
                                          // Add to exception list in scanner ON mode
                                          item.ExceptionReason = "Incorrect Item";
                                          //item.PickedQuantityInPrimaryUOM = 0;
                                          item.ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                          pickManager.SaveException(Order.OrderId.ToString(), item, CommonNavInfo.RouteID.ToString());
                                      }
                                  }

                                  // Change in Order Qty or Order UM
                                  if (PickorderItemsCollectionFromOrder.Any(i => i.ItemNumber == item.ItemNumber))
                                  {
                                      Models.PickOrderItem currentItem = null;
                                      currentItem = PickorderItemsCollectionFromOrder.FirstOrDefault(i => i.ItemNumber == item.ItemNumber);
                                      //Item UM has changed
                                      if (item.UM != currentItem.UM && item.PickedQuantityInPrimaryUOM > 0)
                                      {
                                          item.ExceptionReason = "Incorrect Item";
                                          pickManager.UpdateItemForUMChange(Order.OrderId.ToString(), item, currentItem, CommonNavInfo.RouteID.ToString());
                                      }
                                      else
                                      {
                                          pickManager.UpdatePickedItem(Order.OrderId.ToString(), currentItem);
                                          currentItem.PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                                          UpdateExceptions(currentItem, IsValidItem(currentItem));
                                      }
                                  }
                              }

                          }

                          if (IsPickListExists && PickStatus == "Hold")
                          {
                              //Just get the data from db

                          }

                          if (!IsPickListExists)
                          {
                              pickManager.InsertPickList(Order.OrderId.ToString(), PickorderItemsCollectionFromOrder, CommonNavInfo.RouteID.ToString());
                          }

                          ExceptionItems = pickManager.GetExceptionList(Order.OrderId.ToString());

                          if (itemsToDelete.Count > 0)
                          {
                              foreach (string itemNumber in itemsToDelete)
                              {
                                  pickManager.DeletePickedItem(Order.OrderId.ToString(), itemNumber);
                              }
                          }

                          // get the data from db
                          PickorderItemsCollectionFromDB = pickManager.GetPickItemsFromDB(Order.OrderId.ToString());

                          // Pass existing Order Sub state Is it VOID or HOLD 
                          Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.None, false);

                          ManualPickReasonList = pickManager.GetReasonListForManualPick();

                          foreach (PickOrderItem pickOrderItem in PickorderItemsCollectionFromDB)
                          {
                              PickOrderItem item = ExceptionItems.FirstOrDefault(i => i.ItemNumber == pickOrderItem.ItemNumber);
                              if (item != null && item.ExceptionReason == "Incorrect Item")
                              {
                                  pickOrderItem.PickedQuantityInPrimaryUOM = item.ExceptionQtyInPrimaryUOM;
                                  //if (pickOrderItem.OrderQtyInPrimaryUOM <= item.ExceptionQtyInPrimaryUOM) {
                                  //    pickOrderItem.PickedQuantityInPrimaryUOM = item.ExceptionQtyInPrimaryUOM;
                                  //}
                              }
                              PickOrderItems.Add(pickOrderItem);
                          }
                          foreach (PickOrderItem pickOrderItem in PickorderItemsCollectionFromDB)
                          {
                              UpdateExceptions(pickOrderItem, IsValidItem(pickOrderItem));
                          }
                          OnPropertyChanged("IsPickCompleted");
                          OnPropertyChanged("ExceptionItems");
                          OnPropertyChanged("CanRemoveExceptions");

                          SortPickedItems();
                          PayloadManager.OrderPayload.PickOrderItems = PickOrderItems.ToList<PickOrderItem>();

                          IsBusy = false;
                      }
                      catch (Exception e)
                      {
                          IsBusy = false;
                          Logger.Error("SalesLogicExpress.ViewModels.PickOrder, LoadData()  Message: " + e.Message);
                          throw;
                      }
                  }));
            });
        }

        #endregion

        #region Methods
        private void RenderSign()
        {
            SignatureCanvasVM = new CanvasViewModel(Flow.OrderLifeCycle, SignType.ChargeOnAccountSign, Order.OrderId)
            {
                Height = 280,
                Width = 350,
                FileName = "SmallPic.jpg",
                FooterText = "Sign here",
            };
            SignatureCanvasVM.CanvasChanged += SignatureCanvasVM_CanvasChanged;
        }

        void SignatureCanvasVM_CanvasChanged(object sender, CanvasViewModel.CanvasUpdatedEventArgs e)
        {
            CheckForAcceptForChargeOnAccount();
        }
        public List<object> GetPickOrderItems()
        {
            return (List<object>)PickOrderItems.ToList<object>();
        }

        public List<Models.PickOrderItem> GetPickOrderItems(string OrderNo)
        {
            ObservableCollection<Models.PickOrderItem> PickorderItemsCollectionFromDB = new Managers.PickManager().GetPickItemsFromDB(OrderNo);
            return PickorderItemsCollectionFromDB.ToList<Models.PickOrderItem>();
        }
        private void InitializeCommands()
        {
            AcceptChargeOnAccount = new DelegateCommand((param) =>
            {
                //!RequestAuthCodeVM.IsAuthCodeValid
                if (!RequestAuthCodeVM.IsAuthCodeValid && ChargeOnAccountContext.IsApprovalCodeNeeded)
                {
                    var Alert = new AlertWindow { Message = Constants.CashCollection.MandatoryApprovalCodeAlert };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }

                SignatureCanvasVM.UpdateSignatureToDB(false, false, false);

                PayloadManager.OrderPayload.InvoiceNo = invoiceManager.CreateInvoice
                  (
                  "Credit",
                 CommonNavInfo.RouteID.ToString(),
                  Order.OrderId.ToString(),
                  PayloadManager.OrderPayload.Customer.CustomerNo,
                  DateTime.Now,
                  Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                  Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                  Order.OrderId.ToString()
                  ).ToString();
                PayloadManager.OrderPayload.InvoicePaymentType = "Credit";
                DeliverToCustomerUpdateAndNavigate(true);
                if (RequestAuthCodeVM.IsAuthCodeValid && ChargeOnAccountContext.IsApprovalCodeNeeded)
                {
                    RequestAuthCodeVM.LogAuthCode(PayloadManager.ApplicationPayload.LoggedInUserID, RequestAuthCodeVM.RequestCode, RequestAuthCodeVM.Authcode, CommonNavInfo.RouteID.ToString(), PayloadManager.OrderPayload.SerializeToJson(), PayloadManager.OrderPayload.Customer.CustomerNo, PayloadManager.OrderPayload.Amount.ToString(), RequestAuthCodeVM.Params.Feature.ToString());
                }
            });
            DeclineChargeOnAccount = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
            });
            ClearSignInChargeOnAccount = new DelegateCommand((param) =>
            {
                SignStroke.Clear();
            });

            ShowPickSlipReport = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    IsBusy = true;
                    var payload = param;
                    TotalPickedQty = PickOrderItems.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PrintPickOrder, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, Payload = payload, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
            });
            ShowInvoiceReport = new DelegateCommand((param) =>
            {
                IsBusy = true;
                var payload = Order.OrderId;
                TotalPickedQty = PickOrderItems.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PrintInvoice, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, Payload = payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            VoidOrder = new DelegateCommand((param) =>
            {
                if (ReasonCodeList != null)
                    Order.VoidOrderReasonId = ReasonCodeList.Id;
                // Set reason for void Order if not available 
                if (Order.VoidOrderReasonId == 0)
                {
                    ReasonCodeList.ParentViewModel = this;
                    ReasonCodeList.MessageToken = MessageToken;
                    ReasonCodeList.ActivityKey = ActivityKey.VoidAtCashCollection;
                    var OpenDialog = new Helpers.DialogWindow { TemplateKey = "VoidOrderReason", Title = "Void Order Reason", Payload = ReasonCodeList };
                    Messenger.Default.Send<DialogWindow>(OpenDialog, MessageToken);
                    if (OpenDialog.Cancelled)
                    {
                        OrderStateChangeArgs arg = new OrderStateChangeArgs();
                        arg.State = OrderState.None;
                        ReasonCodeList.OnStateChanged(arg);
                    }
                    return;
                }
                else
                {
                    // Update void reason in DB and Clear InMemory VoidOrderReasonId = 0

                    Order.VoidOrderReasonId = 0;
                }
            });

            ChargeOnAccount = new DelegateCommand((param) =>
            {
                Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                RequestAuthCodeViewModel.RequestAuthCodeParams codeParams = new RequestAuthCodeViewModel.RequestAuthCodeParams
                {
                    Amount = CashDeliveryContext.TotalBalanceAmount,
                    UserID = UserManager.UserId.ToString(),
                    Feature = AuthCodeManager.Feature.ChargeOnAccount
                };
                RequestAuthCodeVM = new RequestAuthCodeViewModel(codeParams);

                RequestAuthCodeVM.AuthCodeChanged += RequestAuthCodeVM_AuthCodeChanged;
                ChargeOnAccountContext = collectionManager.GetChargeOnAccountInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString(), CashDeliveryContext.TotalBalanceAmount, CashDeliveryContext.PreviousBalanceAmount, CashDeliveryContext.CurrentInvoiceAmount);
                if (!ChargeOnAccountContext.IsApprovalCodeNeeded)
                {
                    RequestAuthCodeVM.IsAuthCodeValid = true;
                    RequestAuthCodeVM.RequestCode = string.Empty;
                }
                COAReasonList = new Managers.OrderManager().GetReasonListFor("Charge on Account");
                _SignStroke = new StrokeCollection();

                //Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                RenderSign();
                DialogWindow ChargeOnAccountDialog = new DialogWindow();
                ChargeOnAccountDialog.Title = "Charge on Account";
                ChargeOnAccountDialog.TemplateKey = "ChargeOnAccount";
                Messenger.Default.Send<DialogWindow>(ChargeOnAccountDialog, MessageToken);
            });

            SetPickItemLabels = new DelegateCommand((param) =>
            {
                Models.PickOrderItem orderItem = param as Models.PickOrderItem;
                PickedItemNo = orderItem.ItemNumber.Trim();
                PickedItemUM = orderItem.UM;
                ManualEnteredQty = orderItem.OrderQty;
            });
            ScanByDevice = new DelegateCommand((param) =>
            {
                ScanOperation(param);
                LastScanItem = param as PickOrderItem;
            });
            ManualPick = new DelegateCommand((param) =>
            {
                if (param == null)
                {
                    string strMessage;
                    if (IsAddingToPalette)
                    {
                        strMessage = Constants.PickOrder.SelectItemToPickAlert;
                    }
                    else
                    {
                        strMessage = Constants.PickOrder.SelectItemToUnPickAlert;
                    }
                    var Alert = new AlertWindow { Message = strMessage };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }
                Models.PickOrderItem item = param as Models.PickOrderItem;
                if (item.LastManualPickReasonCode != 0)
                {
                    ScanOperation(param);
                }
                else
                {
                    if (IsScannerOn && LastScanItem != item)
                    {
                        var Alert = new AlertWindow { Message = Constants.PickOrder.SelectReasonForManualPickAlert };
                        Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                        return;
                    }
                    else
                    {
                        ScanOperation(param);
                    }
                }

            });
            GetManualPickReasonList = new DelegateCommand((param) =>
            {
                string strMessage;
                if (IsAddingToPalette)
                {
                    param = SelectedPickItem;
                    strMessage = Constants.PickOrder.SelectItemToPickAlert;
                }
                else
                {
                    param = SelectedExceptionItem;
                    strMessage = Constants.PickOrder.SelectItemToUnPickAlert;
                }


                if (param == null)
                {
                    var Alert = new AlertWindow { Message = strMessage };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }

                var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });

            SetManualPickReason = new DelegateCommand((param) =>
            {
                selectedReason = param as ReasonCode;

                if (selectedReason == null)
                {
                    ErrorText = "Please Choose Reason Code";
                }

                if (IsAddingToPalette)
                {
                    SelectedPickItem.LastManualPickReasonCode = selectedReason.Id;
                    //Update in DB
                    pickManager.UpdateManualPickReasonAndCountForPick(SelectedPickItem);
                }
                else
                {
                    SelectedExceptionItem.LastManualPickReasonCode = selectedReason.Id;
                    //Update in DB
                    pickManager.UpdateManualPickReasonAndCountForException(SelectedExceptionItem);
                }

            });

            HoldPickList = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                if (confirmMessage.Confirmed)
                {
                    OrderStateChangeArgs arg = new OrderStateChangeArgs();
                    arg.State = OrderState.Hold;
                    selectedReason.OnStateChanged(arg);

                    if (ActivityKey.HoldAtPick.ToString() == param.ToString())
                    {
                        LogActivity(ActivityKey.HoldAtPick, true);
                    }
                    else
                    {
                        LogActivity(ActivityKey.HoldAtCashCollection, true);
                    }

                    System.Threading.Thread.Sleep(50);
                    Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                }
                Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.HoldAtPick);
                pickManager.HoldUnHoldPickList(Order.OrderId.ToString(), true);

                #region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.OrderPayload.Customer.StopID;
                }
                new Managers.CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new Managers.CustomerManager().UpdateActivityCount(stopID, true, true);
                PayloadManager.OrderPayload.Customer.SaleStatus = "";
                PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                #endregion

                #region UpdateCommitedQuantity
                // Log commited qty to inventory_ledger  and update inventory
                var objInv = new Managers.InventoryManager();
                if (PayloadManager.OrderPayload.Items != null)
                    objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items.ToList(), StatusTypesEnum.HOLD);
                #endregion

                #region Update temp sequence no.
                //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. --- Priya Natu
                DateTime todayDate = Convert.ToDateTime(tempStopDate);
                ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);

                #endregion

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            RemoveExceptions = new DelegateCommand((s) =>
            {

                var confirmMessage = new Helpers.ConfirmWindow { Message = Constants.PickOrder.RemoveExceptionConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                IsAddingToPalette = false;
            });

            DeliverToCustomer = new DelegateCommand(param =>
            {
                Managers.CollectionManager collectionManager = new Managers.CollectionManager();
                string newReceiptId = "";
                if (param != null)
                {
                    bool? IsCashDelivery = param as bool?;
                    if (IsCashDelivery.Value)
                    {
                        if (!CashDeliveryContext.IsCashDeliveryAllowed)
                        {
                            Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = Constants.CashCollection.AmountMissMatchAlert }, MessageToken);
                            return;
                        }
                        else
                        {
                            //Insert Check Details
                            if (CashDeliveryContext.PaymentMode)
                            {
                                CheckEntryDetails o = new CheckEntryDetails();
                                o.CheckNo = CashDeliveryContext.ChequeNo;
                                o.CheckAmount = CashDeliveryContext.PaymentAmount;
                                o.CheckDate = CashDeliveryContext.ChequeDate.ToString("yyyy-MM-dd");
                                o.CreatedBy = Managers.UserManager.UserId.ToString();
                                o.RoutId = CommonNavInfo.RouteID.ToString();
                                o.CustomerId = CommonNavInfo.Customer.CustomerNo;
                                o.CustomerName = CommonNavInfo.Customer.Name;
                                string checkID = SettlementConfirmationManager.InsertCheckDetails(o);
                                CashDeliveryContext.ChequeNoId = checkID;
                            }
                            PayloadManager.OrderPayload.InvoiceNo = invoiceManager.CreateInvoice
                     (
                     "Cash",
                     CommonNavInfo.RouteID.ToString(),
                     Order.OrderId.ToString(),
                     PayloadManager.OrderPayload.Customer.CustomerNo,
                     DateTime.Now,
                     Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                     Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                     Order.OrderId.ToString()
                     ).ToString();
                            PayloadManager.OrderPayload.InvoicePaymentType = "Cash";
                            string receiptId = "";
                            collectionManager.SaveCashDeliveryInfo(CashDeliveryContext, CommonNavInfo.Customer.CustomerNo, CommonNavInfo.RouteID.ToString(), PayloadManager.OrderPayload.InvoiceNo, ref receiptId);
                            //collectionManager.SaveCashSummaryForCustomer(CashDeliveryContext, CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.Company, true);
                            OrderStateChangeArgs arg = new OrderStateChangeArgs();
                            arg.State = OrderState.Hold;
                            selectedReason.OnStateChanged(arg);
                            System.Threading.Thread.Sleep(50);
                            DeliverToCustomerUpdateAndNavigate();
                            LogPaymentActivity(ActivityKey.Payment, receiptId);
                            return;
                        }
                    }
                }
                if (CommonNavInfo.Customer.PaymentMode.ToLower() == "cod" || CommonNavInfo.Customer.PaymentMode.ToLower() == "csh")
                {
                    collectionManager = new Managers.CollectionManager();
                    CashDeliveryContext = collectionManager.GetCashDeliveryInfo(CommonNavInfo.Customer.CustomerNo, Order.OrderId.ToString(), CanOpenCashCollection);
                    OpenCashCollectionPopUp();
                }
                else
                {
                    PayloadManager.OrderPayload.InvoiceNo = invoiceManager.CreateInvoice
                       (
                       "Credit",
                       CommonNavInfo.RouteID.ToString(),
                       Order.OrderId.ToString(),
                       PayloadManager.OrderPayload.Customer.CustomerNo,
                       DateTime.Now,
                       Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                       Convert.ToDecimal(PayloadManager.OrderPayload.Amount),
                       Order.OrderId.ToString()
                       ).ToString();
                    PayloadManager.OrderPayload.InvoicePaymentType = "Credit";
                    DeliverToCustomerUpdateAndNavigate();
                }
            });

            SwitchScannerMode = new DelegateCommand((param) =>
            {
                bool InvertMode = !IsScannerOn;
                IsScannerOn = InvertMode;
            });

        }

        void RequestAuthCodeVM_AuthCodeChanged(object sender, RequestAuthCodeViewModel.AuthCodeChangedEventArgs e)
        {
            CheckForAcceptForChargeOnAccount();
        }

        public async void OpenCashCollectionPopUp()
        {
            await Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.DataBind,
                  new Action(() =>
                  {
                      DialogWindow CashCollectionDialog = new DialogWindow();
                      CashCollectionDialog.Title = "Cash Delivery";
                      CashCollectionDialog.TemplateKey = "CashDelivery";
                      Messenger.Default.Send<DialogWindow>(CashCollectionDialog, MessageToken);
                  }));
            });
        }

        private void DeliverToCustomerUpdateAndNavigate(Boolean IsPaymentTermChanged = false)
        {
            Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.DeliverToCustomer);

            //Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "Order has been picked successfully" }, MessageToken);
            //ResourceManager.Transaction.AddTransactionInQueueForSync(Managers.Transaction.DeliverOrder, SalesLogicExpress.Application.Managers.SyncQueueManager.Priority.immediate);
            Managers.CollectionManager collectionManager = new Managers.CollectionManager();

            if (IsPaymentTermChanged)
            {
                CashDeliveryContext.PaymentAmount = "0";

                //collectionManager.SaveCashSummaryForCustomer(CashDeliveryContext, CommonNavInfo.Customer.CustomerNo, CommonNavInfo.Customer.Company, false);
            }

            ObservableCollection<OrderItem> ItemsToDeliver = new ObservableCollection<OrderItem>();

            foreach (PickOrderItem pickItem in PickOrderItems)
            {
                ItemsToDeliver.Add(pickItem);
            }
            Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);

            pickManager.HoldUnHoldPickList(Order.OrderId.ToString(), false);

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.NavigationParameter = IsPaymentTermChanged;
            LogActivity(ActivityKey.PickComplete, true);
            PayloadManager.OrderPayload.Items = ItemsToDeliver.ToList();

            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.DeliveryScreen, Refresh = true, CloseCurrentView = true, ShowAsModal = false, CurrentViewName = ViewModelMappings.View.PickOrder };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        }


        private void PickOrderItems_ItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PickOrderItem pickedItem = sender as PickOrderItem;
            if (e.PropertyName == "PickedQuantityInPrimaryUOM")
            {
                //Updt qty in DB
                pickManager.UpdatePickQty(Order.OrderId.ToString(), pickedItem.ItemNumber, pickedItem.PickedQuantityInPrimaryUOM.ToString());

                OnPropertyChanged("IsPickCompleted");
                //ScanOperation(pickedItem);
            }
        }



        private void PickOrderItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }




        // check for item validity and return Item Status
        public string IsValidItem(Models.PickOrderItem item)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:IsValidItem]" +
                "[Parameters:item.ItemNumber:" + item.ItemNumber + ", item.OrderQtyInPrimaryUOM:" + item.OrderQtyInPrimaryUOM + ", item.PickedQuantityInPrimaryUOM:" + item.PickedQuantityInPrimaryUOM + "]");
            string result = "";
            try
            {
                if (item.OrderQtyInPrimaryUOM < item.PickedQuantityInPrimaryUOM && PickOrderItems.Any(i => i.ItemNumber == item.ItemNumber))
                {
                    result = "Over Picked";

                }
                else if (!PickOrderItems.Any(i => i.ItemNumber == item.ItemNumber))
                {
                    result = "Incorrect Item";
                }
                else if (PickOrderItems.Count != 0 && PickOrderItems.Any(i => i.ItemNumber == item.ItemNumber))
                {
                    result = "Valid";
                }

                //result = "Invalid Item Code";
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][IsValidItem][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:IsValidItem]");

            return result;
        }


        //update picked quantity in memory collection
        public void AddingToPalette(PickOrderItem item)
        {
            item.PickedQuantityInPrimaryUOM = Convert.ToInt32(item.PickedQuantityInPrimaryUOM + item.UMConversionFactor);
        }

        //update picked quantity in memory collection
        public void RemovingFromPalette(PickOrderItem item)
        {
            if (item.PickedQuantityInPrimaryUOM > 0)
            {
                //if (item.OrderQtyInPrimaryUOM == item.PickedQuantityInPrimaryUOM)
                //{
                //    SpeechSynthesizer speechSynth = new SpeechSynthesizer();
                //    speechSynth.Speak("Cannot unpick ordered item");
                //    return;
                //}
                item.PickedQuantityInPrimaryUOM = Convert.ToInt32(item.PickedQuantityInPrimaryUOM - item.UMConversionFactor);
            }
            else
            {

                //var Alert = new AlertWindow { Message = "Picked item doesn't exist in palette" };
                //Messenger.Default.Send<AlertWindow>(Alert, MessageToken);

            }
            if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber))
            {
                PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = Convert.ToInt32(PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM - item.UMConversionFactor);
            }
        }

        // picking/unpicking qty from keypad
        public void ManuallyPicking(PickOrderItem item)
        {

            int intPickedQuantityInPrimaryUOM = Convert.ToInt32(ManualEnteredQty * item.UMConversionFactor);

            if ((item.ExceptionReason == "Incorrect Item" || item.ExceptionReason == "Over Picked") &&
                ExceptionItems.Count > 0 && ExceptionItems.Any(s => s.ItemNumber == item.ItemNumber)
                )
            {
                if (intPickedQuantityInPrimaryUOM > item.ExceptionQtyInPrimaryUOM)
                {

                    var Alert = new AlertWindow { Message = "Item qty exceeds exception qty" };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }

                item.ExceptionQtyInPrimaryUOM = item.ExceptionQtyInPrimaryUOM - intPickedQuantityInPrimaryUOM;
                item.PickedQuantityInPrimaryUOM = item.ExceptionQtyInPrimaryUOM + item.OrderQtyInPrimaryUOM;
                if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber))
                {
                    PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                }
            }
            else
            {
                if (intPickedQuantityInPrimaryUOM > item.OrderQtyInPrimaryUOM)
                {

                    var Alert = new AlertWindow { Message = "Picked item qty exceeds order qty" };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;

                }

                item.PickedQuantityInPrimaryUOM = intPickedQuantityInPrimaryUOM;
                if (PickOrderItems.Count > 0 && PickOrderItems.Any(s => s.ItemNumber == item.ItemNumber))
                {
                    PickOrderItems.FirstOrDefault(s => s.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = intPickedQuantityInPrimaryUOM;
                }
            }



            ManualEnteredQty = 0;
            IsManualPick = false;
            item.LastManualPickReasonCode = 0;
        }

        // save exception in Insert/update
        public void SaveException(string OrderId, PickOrderItem item)
        {
            pickManager.SaveException(OrderId, item, CommonNavInfo.RouteID.ToString());
        }

        // update exception list and made changes in DB data for transaction
        public void UpdateExceptions(PickOrderItem item, string ItemValidity)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][Start:UpdateExceptions][Parameters:item.SerializeToJson():" + item.SerializeToJson() + "]");
            try
            {
                if (ItemValidity == "Over Picked")
                {
                    if (ExceptionItems.Any(i => i.ItemNumber.Trim() == item.ItemNumber.Trim()))
                    {
                        if (ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).ExceptionQtyInPrimaryUOM > 0)
                        {
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - item.OrderQtyInPrimaryUOM;
                        }
                    }
                    else
                    {
                        ExceptionItems.Add(new Models.PickOrderItem()
                        {
                            OrderQtyInPrimaryUOM = item.OrderQtyInPrimaryUOM,
                            PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM,
                            ExceptionQtyInPrimaryUOM = item.PickedQuantityInPrimaryUOM - item.OrderQtyInPrimaryUOM,
                            ExceptionQtyInOrderUOM = Convert.ToInt32(item.ExceptionQtyInPrimaryUOM / item.UMConversionFactor),
                            PrimaryUM = item.PrimaryUM,
                            ItemNumber = item.ItemNumber,
                            ItemDescription = item.ItemDescription,
                            ItemStatus = item.ItemStatus,
                            ExceptionReason = ItemValidity,
                            UMConversionFactor = item.UMConversionFactor,
                            UM = item.UM
                        });
                    }
                    if (ExceptionItems.Count > 0)
                    {
                        pickManager.SaveException(Order.OrderId.ToString(), ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber), CommonNavInfo.RouteID.ToString());
                    }
                }
                else if (ItemValidity == "Valid")
                {
                    RemoveItemFromException(item);
                }
                else if (ItemValidity == "Incorrect Item")
                {
                    if (ExceptionItems.Any(i => i.ItemNumber == item.ItemNumber))
                    {
                        if (ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).ExceptionQtyInPrimaryUOM > 0)
                        {
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).PickedQuantityInPrimaryUOM = item.PickedQuantityInPrimaryUOM;
                            ExceptionItems.FirstOrDefault(i => i.ItemNumber == item.ItemNumber).ExceptionQtyInPrimaryUOM = Convert.ToInt32(item.ExceptionQtyInPrimaryUOM - item.UMConversionFactor);
                            if (item.ExceptionQtyInPrimaryUOM == 0)
                            {
                                RemoveItemFromException(item);
                            }
                        }
                        else
                        {
                            RemoveItemFromException(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PickOrder][UpdateExceptions][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PickOrder][End:UpdateExceptions]");
        }

        private void RemoveItemFromException(PickOrderItem item)
        {
            if (ExceptionItems.Any(i => i.ItemNumber == item.ItemNumber))
            {
                foreach (var ExceptionItem in ExceptionItems)
                {
                    if (ExceptionItem.ItemNumber == item.ItemNumber)
                    {
                        ExceptionItems.Remove(ExceptionItem);
                        break;
                    }
                }
            }
            pickManager.DeleteException(Order.OrderId.ToString(), item);
        }

        // comman scan operation function for every pick/ unpick from scanner or manual
        async void ScanOperation(object param)
        {
            await Task.Factory.StartNew(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                  new Action(() =>
                  {
                      //Stopwatch ScanOperationTimer = Stopwatch.StartNew();
                      Models.PickOrderItem item = param as Models.PickOrderItem;
                      if (item == null) return;
                      //Adding mode from scanner (Pick list)
                      if (IsAddingToPalette && IsScannerOn && !IsManualPick)
                      {
                          AddingToPalette(item);
                      }

                      //Removing mode from scanner (Exception list)
                      if (!IsAddingToPalette && IsScannerOn && !IsManualPick)
                      {
                          RemovingFromPalette(item);
                      }

                      //Picking using keypad
                      if (IsManualPick)
                      {
                          ManuallyPicking(item);
                      }

                      //Stopwatch UpdateExceptionsTimer = Stopwatch.StartNew();

                      UpdateExceptions(item, IsValidItem(item));

                      //UpdateExceptionsTimer.Stop();
                      //Logger.Info("UpdateExceptions Time taken :" + UpdateExceptionsTimer.Elapsed.TotalMilliseconds + "ms");

                      //Stopwatch UpdatePickQtyTimer = Stopwatch.StartNew();

                      //Updt qty in DB
                      pickManager.UpdatePickQty(Order.OrderId.ToString(), item.ItemNumber, item.PickedQuantityInPrimaryUOM.ToString());

                      //UpdatePickQtyTimer.Stop();
                      //Logger.Info("pickManager.UpdatePickQty Time taken :" + UpdatePickQtyTimer.Elapsed.TotalMilliseconds + "ms");

                      if (IsScannerOn)
                      {
                          // Sort Pick List
                          SortPickedItems();
                      }

                      // Change Scanner mode to adding, If there are no any exception
                      if (ExceptionItems.Count == 0)
                      {
                          IsAddingToPalette = true;
                      }

                      // Update All relevant properties
                      OnPropertyChanged("PickOrderItems");
                      OnPropertyChanged("TotalOrderQtyCount");
                      OnPropertyChanged("DoneItemsQtyCount");
                      OnPropertyChanged("UnderPickItemsQtyCount");
                      OnPropertyChanged("OverPickItemsQtyCount");
                      OnPropertyChanged("IsPickCompleted");
                      OnPropertyChanged("IsAddingToPalette");
                      OnPropertyChanged("CanRemoveExceptions");


                      //Set selected Item
                      if (IsAddingToPalette)
                      {
                          SelectedPickItem = item;
                      }
                      else
                      {
                          SelectedExceptionItem = item;
                      }
                      OnPropertyChanged("SelectedPickItem");
                      OnPropertyChanged("SelectedExceptionItem");
                      PayloadManager.OrderPayload.PickOrderItems = PickOrderItems.ToList<PickOrderItem>();

                      if (IsInVoidState && PickOrderItems.Count == 0 && ExceptionItems.Count == 0)
                      {
                          System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                           new Action(() =>
                           {
                               Messenger.Default.Send<AlertWindow>(new AlertWindow { Message = "Items unpicked successfully" }, MessageToken);
                               SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                               PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                               var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.PickOrder, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                               Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                           }
                       ));
                      }
                      else if (IsInVoidState && PickOrderItems.Count == 0 && ExceptionItems.Count > 0)
                      {
                          //Let Activity be in VOID state
                      }
                      else
                      {
                          LogActivity(ActivityKey.PickItem);
                      }
                      //ScanOperationTimer.Stop();
                      //Logger.Error("Scan Operation Time taken :" + ScanOperationTimer.Elapsed.TotalMilliseconds + "ms");
                  }));
            });
        }

        private void SortPickedItems()
        {
            PickOrderItems = new TrulyObservableCollection<PickOrderItem>(PickOrderItems.OrderByDescending(x => x.ItemStatus).ThenBy(x => x.ItemNumber));
            ExceptionItems = new TrulyObservableCollection<PickOrderItem>(ExceptionItems.OrderByDescending(x => x.ItemStatus).ThenBy(x => x.ItemNumber));
        }
        #endregion

        #region Variables
        private TrulyObservableCollection<Models.PickOrderItem> _PickOrderItems = new TrulyObservableCollection<PickOrderItem>();
        private ObservableCollection<Models.PickOrderItem> _ExceptionItems = new ObservableCollection<PickOrderItem>();
        public bool _IsAddingToPalette = true, _IsScannerOn = true, _IsManualPick = false, _CanRemoveExceptions = false;
        public int _PackSlipNo = new Random().Next(1000, 10000), _ManualEnteredQty, _PickedItemQty;
        private string _OrderNumber, _PickedItemUM, _PickedItemNo;
        ObservableCollection<ReasonCode> _ManualPickReasonList;
        ObservableCollection<ReasonCode> _COAReasonList;
        public Models.PickOrderItem _SelectedPickItem;
        public Models.PickOrderItem _SelectedExceptionItem;
        public Models.PickOrderItem _LastScanItem;
        private Managers.PricingManager pricingManager;
        private Managers.PickManager pickManager;
        private Managers.InvoiceManager invoiceManager = new InvoiceManager();
        private string _ErrorText;
        public ReasonCode ReasonCodeList = new ReasonCode(string.Empty);
        CashDelivery _CashDeliveryContext;
        ChargeOnAccount _ChargeOnAccountContext;
        StrokeCollection _SignStroke;
        Boolean _IsInVoidState;
        private bool _IsBusy = true;
        private string _TotalPickedQty;
        #endregion

        #region Properties

        bool _CanAcceptChargeOnAccount = false;
        public bool CanAcceptChargeOnAccount
        {
            get
            {
                return _CanAcceptChargeOnAccount;
            }
            set
            {
                _CanAcceptChargeOnAccount = value;
                OnPropertyChanged("CanAcceptChargeOnAccount");
            }
        }
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public Boolean IsInVoidState
        {
            get
            {
                return (Order.OrderStatus == ActivityKey.VoidAtOrderEntry
                    || Order.OrderStatus == ActivityKey.VoidAtDeliverCustomer
                    || Order.OrderStatus == ActivityKey.VoidAtCashCollection)
                    ? true : false;
            }
            set
            {
                _IsInVoidState = value; OnPropertyChanged("IsInVoidState");
            }
        }
        public string TotalPickedQty
        {
            get { return _TotalPickedQty; }
            set { _TotalPickedQty = value; OnPropertyChanged("TotalPickedQty"); }
        }
        public CanvasViewModel SignatureCanvasVM { get; set; }

        public string ErrorText
        {
            get { return _ErrorText; }
            set { _ErrorText = value; OnPropertyChanged("ErrorText"); }
        }
        public CashDelivery CashDeliveryContext
        {
            get { return _CashDeliveryContext; }
            set
            {
                _CashDeliveryContext = value;
                OnPropertyChanged("CashDeliveryContext");
            }
        }
        public ChargeOnAccount ChargeOnAccountContext
        {
            get { return _ChargeOnAccountContext; }
            set
            {
                _ChargeOnAccountContext = value;
                OnPropertyChanged("ChargeOnAccountContext");
            }
        }

        RequestAuthCodeViewModel _RequestAuthCodeVM;
        public RequestAuthCodeViewModel RequestAuthCodeVM
        {
            get
            {
                return _RequestAuthCodeVM;
            }
            set
            {
                _RequestAuthCodeVM = value;
                OnPropertyChanged("RequestAuthCodeVM");
            }
        }
        public Models.PickOrderItem LastScanItem
        {
            get
            {
                return _LastScanItem;
            }
            set
            {
                _LastScanItem = value;
                OnPropertyChanged("LastScanItem");
            }
        }

        public Models.PickOrderItem SelectedPickItem
        {
            get
            {
                return _SelectedPickItem;
            }
            set
            {
                _SelectedPickItem = value;
                OnPropertyChanged("SelectedPickItem");
            }
        }
        public Models.PickOrderItem SelectedExceptionItem
        {
            get
            {
                return _SelectedExceptionItem;
            }
            set
            {
                _SelectedExceptionItem = value;
                OnPropertyChanged("SelectedExceptionItem");
            }
        }

        public bool CanOpenCashCollection { get; set; }

        public ObservableCollection<ReasonCode> ManualPickReasonList
        {
            get
            {
                return _ManualPickReasonList;
            }
            set
            {
                _ManualPickReasonList = value;
            }
        }
        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");
            }
        }
        public string PickedItemNo
        {
            get
            {
                return _PickedItemNo;
            }
            set
            {
                _PickedItemNo = value;
                OnPropertyChanged("PickedItemNo");
            }
        }

        public int PickedItemQty
        {
            get
            {
                return _PickedItemQty;
            }
            set
            {
                _PickedItemQty = value;
                OnPropertyChanged("PickedItemQty");
            }
        }

        public string PickedItemUM
        {
            get
            {
                return _PickedItemUM;
            }
            set
            {
                _PickedItemUM = value;
                OnPropertyChanged("PickedItemUM");
            }
        }

        public int ManualEnteredQty
        {
            get
            {
                return _ManualEnteredQty;
            }
            set
            {
                _ManualEnteredQty = value;
                OnPropertyChanged("ManualEnteredQty");
            }
        }

        public ObservableCollection<ReasonCode> COAReasonList
        {
            get
            {
                return _COAReasonList;
            }
            set
            {
                _COAReasonList = value;
                OnPropertyChanged("COAReasonList");
            }
        }

        ReasonCode _SelectedReasonCode;
        public ReasonCode SelectedReasonCode
        {
            get
            {
                return _SelectedReasonCode;
            }
            set
            {
                _SelectedReasonCode = value;
                CheckForAcceptForChargeOnAccount();
                OnPropertyChanged("SelectedReasonCode");
            }
        }

        private void CheckForAcceptForChargeOnAccount()
        {
            CanAcceptChargeOnAccount = SelectedReasonCode != null && RequestAuthCodeVM.IsAuthCodeValid && SignatureCanvasVM.SignatureAvailable;
        }

        public bool CanRemoveExceptions
        {
            get
            {
                return ExceptionItems.Count > 0 && !PickOrderItems.Any(i => i.OrderQtyInPrimaryUOM > i.PickedQuantityInPrimaryUOM) ? true : false;
            }
            set
            {
                _CanRemoveExceptions = value;
                OnPropertyChanged("CanRemoveExceptions");
            }
        }

        public bool IsManualPick
        {
            get
            {
                return _IsManualPick;
            }
            set
            {
                _IsManualPick = value;
                OnPropertyChanged("IsManualPick");
            }
        }

        public bool IsPickCompleted
        {
            get
            {
                //Pick completed will be false if any item in picking scrren has ActualAvailableQty<0
                if (this._PickOrderItems != null)
                    return !this._PickOrderItems.Any(w => w.PickedQuantityInPrimaryUOM != w.OrderQtyInPrimaryUOM || w.ActualAvailableQty < 0) && ExceptionItems.Count == 0 && PickOrderItems.Count != 0;
                return false;
            }
            set
            {
                OnPropertyChanged("IsPickCompleted");
            }
        }
        public Guid MessageToken { get; set; }

        public int UnderPickItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Where(w => w.PickedQuantityInPrimaryUOM < w.OrderQtyInPrimaryUOM).Sum(s => s.OrderQtyInPrimaryUOM - s.PickedQuantityInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("UnderPickItemsQtyCount");
            }
        }
        public int OverPickItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Where(w => w.PickedQuantityInPrimaryUOM > w.OrderQtyInPrimaryUOM).Sum(s => s.PickedQuantityInPrimaryUOM - s.OrderQtyInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("OverPickItemsQtyCount");
            }
        }
        public bool IsAddingToPalette
        {
            get
            {
                return _IsAddingToPalette;
            }
            set
            {
                _IsAddingToPalette = value;
                OnPropertyChanged("IsAddingToPalette");

            }
        }

        public bool IsScannerOn
        {
            get
            {
                return _IsScannerOn;
            }
            set
            {
                _IsScannerOn = value;
                OnPropertyChanged("IsScannerOn");

            }
        }
        public int PackSlipNo
        {
            get { return _PackSlipNo; }
            set
            {
                _PackSlipNo = value;
                OnPropertyChanged("PackSlipNo");
            }
        }
        public int DoneItemsQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Sum(s => s.PickedQuantityInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("DoneItemsQtyCount");
            }
        }

        public int TotalOrderQtyCount
        {
            get
            {
                if (this._PickOrderItems != null) return this._PickOrderItems.Sum(s => s.OrderQtyInPrimaryUOM);
                return 0;
            }
            set
            {
                OnPropertyChanged("TotalOrderQtyCount");
            }
        }
        private Models.Customer _Customer;
        public Models.Customer Customer
        {
            get
            {
                if (_Customer == null)
                {
                    _Customer = new Models.Customer();
                    _Customer.Name = "STOP N SHOP";
                    _Customer.CustomerNo = "121343";
                    _Customer.Phone = "9878767655";
                    _Customer.Route = "783";
                    _Customer.City = "Oklahoma City";
                    _Customer.State = "OK";
                    _Customer.Zip = "72132";
                }
                return _Customer;
            }
            set
            {
                _Customer = value;
            }
        }

        public string OrderNumber
        {
            get { return Order.OrderId.ToString(); }
            set
            {
                if (value != _OrderNumber)
                {
                    _OrderNumber = value;
                    OnPropertyChanged("OrderNumber");
                }
            }

        }
        public TrulyObservableCollection<Models.PickOrderItem> PickOrderItems
        {
            get { return _PickOrderItems; }
            set
            {
                _PickOrderItems = value;
                OnPropertyChanged("PickOrderItems");
            }
        }

        public ObservableCollection<Models.PickOrderItem> ExceptionItems
        {
            get { return _ExceptionItems; }
            set
            {
                _ExceptionItems = value;
                OnPropertyChanged("ExceptionItems");
            }
        }
        #endregion





        public string Error
        {
            get { return ErrorText; }

        }

        public string this[string columnName]
        {
            get
            {

                if (columnName == "SelectedPickItem")
                {

                }


                return ErrorText;
            }
        }

    }
}
