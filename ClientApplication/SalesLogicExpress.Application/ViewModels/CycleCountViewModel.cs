﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using Telerik.Windows.Controls;
using System.Windows.Threading;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CycleCountViewModel : BaseViewModel
    {
        CycleCountManager CCManager = new CycleCountManager();
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CycleCountViewModel");

        #region Properties
        private string _Route;
        public string Route
        {
            get
            {
                return _Route;
            }
            set
            {
                _Route = value;
                OnPropertyChanged("Route");
            }
        }

        private string _RouteName;
        public string RouteName
        {
            get
            {
                return _RouteName;
            }
            set
            {
                _RouteName = value;
                OnPropertyChanged("RouteName");
            }
        }

        private Guid _MessageToken;

        private ObservableCollection<CycleCount> _CycleCountList = new ObservableCollection<CycleCount>();

        private CycleCount cycleCountObject = new Domain.CycleCount();
        public CycleCount CycleCountObject
        {
            get { return cycleCountObject; }
            set { cycleCountObject = value; OnPropertyChanged("CycleCountObject"); }
        }


        #region Cycle Count Tab Properties

        public ObservableCollection<CycleCount> CycleCountList
        {
            get { return _CycleCountList; }
            set
            {
                _CycleCountList = value;
                OnPropertyChanged("CycleCountList");
            }
        }

        #endregion

        #region Cycle Count Window Properties

        private StatusTypesEnum requestStatus;
        private bool isResetButtonEnabled = false;
        private bool isSubmitButtonEnabled = false;
        private bool isManualCountButtonEnabled = false;
        private bool isRecountItemsVisible = false;
        private bool toggleReasonCodeButton = false;
        private bool isVoidEnabled = true;
        private bool isHoldEnabled = true;
        //Counter to handle pick using onscreen keypad
        private int pickCounter = 0;
        public bool IsHoldEnabled
        {
            get { return isHoldEnabled; }
            set { isHoldEnabled = value; OnPropertyChanged("IsHoldEnabled"); }
        }
        public bool IsVoidEnabled
        {
            get { return isVoidEnabled; }
            set { isVoidEnabled = value; OnPropertyChanged("IsVoidEnabled"); }
        }
        private ObservableCollection<ReasonCode> reasonCode = null;
        private int selectedReasonCode;
        private string reasonCodeDescription;
        private int reasonCodeID;
        private ObservableCollection<CycleCountItems> cycleCountDetailList = new ObservableCollection<Domain.CycleCountItems>();
        public int ReasonCodeID
        {
            get { return reasonCodeID; }
            set { reasonCodeID = value; OnPropertyChanged("ReasonCodeID"); }
        }
        public string ReasonCodeDescription
        {
            get { return reasonCodeDescription; }
            set { reasonCodeDescription = value; OnPropertyChanged("ReasonCodeDescription"); }
        }
        public int SelectedReasonCode
        {
            get { return selectedReasonCode; }
            set
            {
                selectedReasonCode = value;
                if (selectedReasonCode != -1)
                {
                    ToggleReasonCodeButton = true;
                }
                OnPropertyChanged("SelectedReasonCode");
            }
        }
        public ObservableCollection<ReasonCode> ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; OnPropertyChanged("ReasonCode"); }
        }
        public BaseViewModel ParentViewModel { get; set; }
        public Guid MessageToken
        {

            get { return _MessageToken; }
            set
            {
                _MessageToken = value;
                OnPropertyChanged("MessageToken");
            }

        }

        List<dynamic> items = null;
        public bool IsResetButtonEnabled
        {
            get { return isResetButtonEnabled; }
            set
            {
                isResetButtonEnabled = value;
                OnPropertyChanged("IsResetButtonEnabled");
            }
        }
        public bool IsSubmitButtonEnabled
        {
            get { return isSubmitButtonEnabled; }
            set
            {
                isSubmitButtonEnabled = value;
                OnPropertyChanged("IsSubmitButtonEnabled");
            }
        }
        public bool IsManualCountButtonEnabled
        {
            get { return isManualCountButtonEnabled; }
            set
            {
                isManualCountButtonEnabled = value;
                OnPropertyChanged("IsManualCountButtonEnabled");
            }
        }
        public bool IsRecountItemsVisible
        {
            get { return isRecountItemsVisible; }
            set
            {
                isRecountItemsVisible = value;
                OnPropertyChanged("IsRecountItemsVisible");
            }
        }
        public bool ToggleReasonCodeButton
        {
            get
            { return toggleReasonCodeButton; }
            set
            {
                toggleReasonCodeButton = value;
                OnPropertyChanged("ToggleReasonCodeButton");
            }
        }
        public ObservableCollection<CycleCountItems> CycleCountDetailList
        {
            get { return cycleCountDetailList; }
            set { cycleCountDetailList = value; OnPropertyChanged("CycleCountDetailList"); }
        }

        private CycleCountItems selectedCCDetailItem = null;
        public CycleCountItems SelectedCCDetailItem
        {
            get { return selectedCCDetailItem; }
            set { selectedCCDetailItem = value; OnPropertyChanged("SelectedCCDetailItem"); }
        }

        private bool isKeyPanelOpened;
        public bool IsKeyPanelOpened
        {
            get { return isKeyPanelOpened; }
            set { isKeyPanelOpened = value; OnPropertyChanged("IsKeyPanelOpened"); }
        }

        private string lastPickedItem;
        public string LastPickedItem
        {
            get { return lastPickedItem; }
            set { lastPickedItem = value; OnPropertyChanged("LastPickedItem"); }
        }

        private ObservableCollection<ReasonCode> manualCountReasonList = null;
        public ObservableCollection<ReasonCode> ManualCountReasonList
        {
            get
            {
                return manualCountReasonList;
            }
            set
            {
                manualCountReasonList = value;
                OnPropertyChanged("ManualCountReasonList");
            }
        }

        private bool isManualCountEnabled = false;
        public bool IsManualCountEnabled
        {
            get { return isManualCountEnabled; }
            set { isManualCountEnabled = value; OnPropertyChanged("IsManualCountEnabled"); }
        }

        private int manualOrderQty = 0;

        public int ManualOrderQty
        {
            get { return manualOrderQty; }
            set { manualOrderQty = value; OnPropertyChanged("ManualOrderQty"); }
        }
        private bool IsManualCount = false;

        private PickItemManager pickMgr = null;
        ObservableCollection<dynamic> _PickItems = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItems
        {
            get { return _PickItems; }
            set
            {
                _PickItems = value;
                OnPropertyChanged("PickItems");
            }
        }
        private bool isGridSelectionEnabled = true;

        public bool IsGridSelectionEnabled
        {
            get { return isGridSelectionEnabled; }
            set { isGridSelectionEnabled = value; OnPropertyChanged("IsGridSelectionEnabled"); }
        }

        private bool isScanButtonVisible = false;

        public bool IsScanButtonVisible
        {
            get { return isScanButtonVisible; }
            set { isScanButtonVisible = value; OnPropertyChanged("IsScanButtonVisible"); }
        }

        #endregion

        #endregion

        #region Delegates

        public DelegateCommand NavigateToCycleCount { get; set; }
        public DelegateCommand StartManualCounting { get; set; }
        public DelegateCommand CountItem { get; set; }
        public DelegateCommand HoldCount { get; set; }
        //public DelegateCommand MoveToCycleCount { get; set; }
        public DelegateCommand SubmitCycleCount { get; set; }
        public DelegateCommand ResetCycleCount { get; set; }
        public DelegateCommand VoidCycleCount { get; set; }
        public DelegateCommand PickItem { get; set; }
        public DelegateCommand SaveVoidCycleCount { get; set; }
        public DelegateCommand ToggleRecountItemsOnChecked { get; set; }
        public DelegateCommand ToggleRecountItemsOnUnChecked { get; set; }
        public DelegateCommand ToggleKeyboard { get; set; }
        public DelegateCommand SelectionChanged { get; set; }
        public DelegateCommand OpenDialogForManualCount { get; set; }
        public DelegateCommand UpdateOnManualCount { get; set; }
        public DelegateCommand ScanItemFromVehicle { get; set; }
        private bool isCycleCountSelected = false;
        public bool IsCycleCountSelected
        {
            get { return isCycleCountSelected; }
            set { isCycleCountSelected = value; OnPropertyChanged("IsCycleCountSelected"); }
        }
        #endregion

        #region Constructor

        public CycleCountViewModel(Guid messageToken, bool isCycleCountSelected, CycleCount cycleCount)
        {
            IsCycleCountSelected = isCycleCountSelected;
            Route = PayloadManager.ApplicationPayload.Route;
            RouteName = Managers.UserManager.RouteName;
            this.MessageToken = messageToken;
            if (isCycleCountSelected)
                GetCycleCountList();
            else
            {
                //GetCountItemList();
                SelectedTab = ViewModelMappings.TabView.RouteHome.CycleCount;

                LoadData(cycleCount);
            }
            InitializeCommands();
            ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;
        }

        void Synchronization_SyncProgressChanged(object sender, SyncManager.SyncUpdatedEventArgs e)
        {
            if (e.State == SyncManager.SyncUpdateType.DownloadComplete)
                GetCycleCountList();
        }


        #endregion

        #region Methods

        private void InitializeCommands()
        {
            StartManualCounting = new DelegateCommand((param) =>
            {
                IsKeyPanelOpened = true;
                SelectedCCDetailItem.ReasonCodeId = Convert.ToInt32(param);
            });
            UpdateOnManualCount = new DelegateCommand(param =>
            {
                //if (ManualOrderQty == 0) return;
                SelectedCCDetailItem.ManuallyPickCount = SelectedCCDetailItem.CountedQuantity = ManualOrderQty;
                SelectedCCDetailItem.IsManuallyPicking = true;
                if (ManualOrderQty != 0)
                    pickMgr.PickItem(SelectedCCDetailItem);
                SelectedCCDetailItem.ItemStatus = true;
                ManualOrderQty = 0;
                IsSubmitButtonEnabled = true;
                IsDirty = true;
            });
            CountItem = new DelegateCommand(param =>
            {

            });

            HoldCount = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Constants.CycleCount.HoldStatus, MessageIcon = "Alert" };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                RemoveDummyItem();

                Managers.CycleCountManager cycleCountM = new CycleCountManager();
                UpdatePhysicalHeader();
                cycleCountM.SaveOnHold(CycleCountDetailList, CycleCountObject.RequestTypeStatusCD);
                CycleCountObject.IsCountInitiated = true;
                //pickMgr.HoldPick(true);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CycleCount, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.CycleCount };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            OpenDialogForManualCount = new DelegateCommand(param =>
            {
                if (SelectedCCDetailItem == null)
                {
                    var strMessage = Constants.CycleCount.SelectItemToCountAlert;
                    var Alert = new AlertWindow { Message = strMessage };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    return;
                }
                ManualOrderQty = SelectedCCDetailItem.CountedQuantity;

                var pickManager = new PickManager();
                ManualCountReasonList = pickManager.GetReasonListForManualPick();

                var dialog = new Helpers.DialogWindow { TemplateKey = "ReasonCodeDialog", Title = "Reason Code" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            ToggleRecountItemsOnChecked = new DelegateCommand(param =>
            {
                if (CycleCountDetailList.Any())
                {
                    //var recountList = CCManager.GetRecountItemsList(CycleCountObject.CycleCountID);
                    var recountList = CCManager.GetRecountItemsList(CycleCountObject.CycleCountID);


                    foreach (var Cc_Item in CycleCountDetailList)
                    {

                        if (recountList != null && recountList.Count() != 0)
                        {
                            var item = recountList.FirstOrDefault(x => x.ItemId.ToLower().Trim() == Cc_Item.ItemId.ToLower().Trim());
                            if (item != null)
                                CycleCountDetailList.FirstOrDefault(x => x.ItemId.ToLower().Trim() == item.ItemId.ToLower().Trim()).IsVisible = true;
                            else
                                Cc_Item.IsVisible = false;
                        }
                    }
                }
            });

            ToggleRecountItemsOnUnChecked = new DelegateCommand(param =>
            {
                if (CycleCountDetailList.Count() != 0 && CycleCountDetailList != null)
                {
                    foreach (var CC_Item in CycleCountDetailList)
                    {
                        CC_Item.IsVisible = true;
                    }
                }
            });

            PickItem = new DelegateCommand(param =>
            {
                if (param == null) return;
         
                var pickedCcItem = param as PickItem;
                SelectedCCDetailItem = pickedCcItem as CycleCountItems;
                SelectedCCDetailItem.IsManuallyPicking = false;

                var result = CycleCountDetailList.Any(x => x.ItemNumber.ToString().Trim() == pickedCcItem.ItemNumber.ToString().Trim());
                if (pickedCcItem.ItemNumber == "" || !result)
                {
                   
                    var dialog = new Helpers.DialogWindow { TemplateKey = "ExtraItemPickedTemplate", Title = "Warning", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    return;
                }
                // TODO : Different implementation needed in case of different UoM selection, while counting an item.

                pickedCcItem.IsManuallyPicking = false;
                pickMgr.PickItem(pickedCcItem);
                SelectedCCDetailItem = pickedCcItem as CycleCountItems;
                SelectedCCDetailItem.IsManuallyPicking = false;
                SelectedCCDetailItem.CountedQuantity = SelectedCCDetailItem.PickedQty;
                //if (SelectedCCDetailItem.CountedQuantity > 0 )
                //{
                //    SelectedCCDetailItem.IsManuallyPicking = true;
                //}

                SelectedCCDetailItem.ItemStatus = true;

                IsSubmitButtonEnabled = !CycleCountDetailList.Any(x => x.CountedQuantity == 0);
                if (requestStatus == StatusTypesEnum.RELS || requestStatus == StatusTypesEnum.RECNT || CycleCountObject.RequestTypeStatusCD.ToLower() == StatusTypesEnum.PHYSCL.ToString().ToLower())
                    IsSubmitButtonEnabled = true;
                IsDirty = true;
                pickCounter++;
            });

            ToggleKeyboard = new DelegateCommand(param =>
            {
                if (SelectedCCDetailItem != null && SelectedCCDetailItem.CountedQuantity > 0)
                    IsKeyPanelOpened = IsKeyPanelOpened ? false : true;
                if ((SelectedCCDetailItem != null && !SelectedCCDetailItem.IsForCount) || (SelectedCCDetailItem != null && pickCounter == 0))
                    IsKeyPanelOpened = false;
            });
            SelectionChanged = new DelegateCommand(param =>
            {

                //var ccItem = param as CycleCountItems ?? new CycleCountItems();
                pickCounter = 0;
                if (SelectedCCDetailItem != null) // && SelectedCCDetailItem.CountedQuantity == 0
                    IsKeyPanelOpened = false;


                //if (requestStatus == StatusTypesEnum.PENDG || requestStatus == StatusTypesEnum.RECNT)
                IsResetButtonEnabled = CycleCountDetailList.Any(x => x.CountedQuantity > 0);

                if (SelectedCCDetailItem.IsForCount)
                    IsManualCountEnabled = true;
                else
                    IsManualCountEnabled = false;

                # region Code added to enable submit button, when all items have counted quantity greater than 0
                if (SelectedCCDetailItem != null)
                {
                    var result = CycleCountDetailList.Any(x => x.ItemNumber.ToString().Trim() == SelectedCCDetailItem.ItemNumber.ToString().Trim());
                    if (SelectedCCDetailItem.ItemNumber == "" || !result)
                    {
                        SelectedCCDetailItem.IsForCount = true;
                        IsManualCountEnabled = false;
                    }
                }

                #endregion
                if (CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.VOID.ToString().ToLower() ||
                   CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.SUBMIT.ToString().ToLower() ||
                   CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.ACCEPT.ToString().ToLower())
                    IsResetButtonEnabled = false;

            });
            NavigateToCycleCount = new DelegateCommand((param) =>
            {
                CycleCountObject = param as CycleCount;
                SetStatusTypeForCCRequest();
                if (DateTime.Compare(CycleCountObject.CycleCountDate.Date, DateTime.Now.Date) > 0 && requestStatus == StatusTypesEnum.RELS)
                    return;
                var moveToView = new Helpers.NavigateToView
                {
                    NextViewName = ViewModelMappings.View.CycleCount,
                    CurrentViewName = ViewModelMappings.View.RouteHome,
                    CloseCurrentView = true,
                    ShowAsModal = false,
                    Payload = param as CycleCount

                };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

            });
            SubmitCycleCount = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow
                {
                    Message = Helpers.Constants.CycleCount.ConfirmSubmitCycleCount,
                    MessageIcon = "Alert",
                    Confirmed = false
                };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                RemoveDummyItem();

                if (CCManager.UpdateCycleCountItems(CycleCountDetailList.Clone(), CycleCountObject.CycleCountStatusID, CycleCountObject.RequestTypeStatusCD))
                {
                    CCManager.UpdateCycleCountRequest(StatusTypesEnum.SUBMIT, CycleCountObject.CycleCountID);
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CycleCount, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.CycleCount };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }

            });

            ResetCycleCount = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow
                {
                    Message = Helpers.Constants.CycleCount.ConfirmResetCycleCount,
                    MessageIcon = "Alert",
                    Confirmed = false,
                    CancelButtonContent = "No",
                    OkButtonContent = "Yes"
                };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                // Managers.CycleCountManager cycleCountM = new CycleCountManager();
                //cycleCountM = 

                foreach (CycleCountItems item in CycleCountDetailList)
                {
                    if (item.IsForCount)
                    {
                        item.CountedQuantity = 0;
                        item.ManuallyPickCount = 0;
                    }
                }
                IsDirty = true;
                IsSubmitButtonEnabled = false;
                IsManualCountButtonEnabled = false;
                IsResetButtonEnabled = false;
                //RemoveDummyItem();

                CCManager.UpdatePickDetailsOnReset(CycleCountDetailList.Clone(), CycleCountObject.CycleCountID.ToString(), PickItemManager.PickForTransaction.CycleCount.GetEnumDescription().ToString());
            });

            VoidCycleCount = new DelegateCommand((param) =>
            {
                ToggleReasonCodeButton = false;
                GetReasonCodeForVoid();
                SelectedReasonCode = -1;
                var dialog = new Helpers.DialogWindow { TemplateKey = "VoidCycleCount", Title = "Void Cycle Count", Payload = this };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                //var dialog2 = new Helpers.ConfirmWindow { TemplateKey = "VoidCycleCount", Header = "Void Cycle Count", Context = this };
                //Messenger.Default.Send<Helpers.ConfirmWindow>(dialog2, MessageToken);


            });

            SaveVoidCycleCount = new DelegateCommand((param) =>
            {
                Managers.CycleCountManager cycleCountM = new CycleCountManager();
                ReasonCodeID = Convert.ToInt32(SelectedReasonCode);
                RemoveDummyItem();
                cycleCountM.UpdateCycleCountRequest(StatusTypesEnum.VOID, CycleCountObject.CycleCountID, ReasonCodeID);
                pickMgr.VoidPick(ReasonCodeID.ToString());
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.CycleCount, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.RouteHome.CycleCount };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
            ScanItemFromVehicle = new DelegateCommand(param =>
            {
                var item = pickMgr.CreateAndAdd(CycleCountObject.CycleCountID);

                if (item != null)
                {
                    var tempvar = item as CycleCountItems;
                    tempvar.CountedQtyUoM = tempvar.PrimaryUM;
                    tempvar.IsForCount = true;
                    CycleCountDetailList.Add(tempvar);
                }
            });
        }

        /// <summary>
        /// Method removes item which mimics behaviour of pickng extra item which is not in list
        /// </summary>
        private void RemoveDummyItem()
        {
            if (CycleCountObject.RequestTypeStatusCD.ToLower() != StatusTypesEnum.PHYSCL.ToString().ToLower())
            {
                var index = CycleCountDetailList.Count();
                CycleCountDetailList.RemoveAt(index - 1);
            }
        }


        private async void GetCountItemList()
        {
            // Todo : Populate items to be listed in gridview. 

            await Task.Run(() =>
            {

            });

        }

        private async void LoadData(CycleCount cycleCount)
        {
            await Task.Run(() =>
            {
                base.Message = "Do you want to hold the cycle count?";
                base.OkContent = "Hold";
                base.CancelContent = "Cancel";
                IsBusy = true;
                if (cycleCount != null)
                {
                    CycleCountObject = cycleCount;
                    if (CycleCountObject.RequestTypeStatusCD.ToLower() != StatusTypesEnum.PHYSCL.ToString().ToLower())
                    {

                        CycleCountDetailList = CCManager.GetItemDetailForRequest(Convert.ToInt32(CycleCountObject.CycleCountID), CycleCountObject.CycleCountStatusID);
                        // Update request status if count is started
                        if (!(CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.VOID.ToString().ToLower() ||
                       CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.SUBMIT.ToString().ToLower() ||
                       CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.ACCEPT.ToString().ToLower()))
                        {
                            CCManager.UpdateInitiationStatus(Convert.ToInt32(CycleCountObject.CycleCountID));
                        }
                        items = new System.Collections.Generic.List<dynamic>();
                        foreach (var CC_Item in CycleCountDetailList)
                        {
                            //if (CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.RECNT.ToString().ToLower() && CC_Item.IsForCount)
                            //    CC_Item.count
                            items.Add(CC_Item);
                        }

                        EnableDisableButtonsOnStatus(IsCycleCountSelected);

                        pickMgr = new PickItemManager(CommonNavInfo.RouteID.ToString(), CycleCountObject.CycleCountID.ToString(), PickItemManager.PickForTransaction.CycleCount, false);
                        //var dictPickList = new System.Collections.Generic.Dictionary<PickItemManager.PickType,ObservableCollection<Domain.PickItem>>(); 
                        //var d = dictPickList[PickItemManager.PickType.Normal]
                        Dictionary<PickItemManager.PickType, ObservableCollection<dynamic>> pickList = pickMgr.CreatePickList(items);
                        PickItems = pickList[PickItemManager.PickType.Normal];
                        pickMgr.PickItemUpdate += pickMgr_PickItemUpdate;
                        //item added to mimic behaviour of counting out of list item
                        var extraItem = new CycleCountItems
                        {
                            ItemNumber = "",
                            ItemId = "",
                            ItemStatus = true,
                            
                        };
                        CycleCountDetailList.Add(extraItem);
                    }
                    else
                    {

                        items = new System.Collections.Generic.List<dynamic>();


                        //Console.WriteLine("Before pick call");

                        //if (!CycleCountObject.IsCountInitiated)
                        //{
                        CycleCountDetailList = CCManager.GetItemDetailForRequest(Convert.ToInt32(CycleCountObject.CycleCountID), CycleCountObject.CycleCountStatusID);

                        
                        if (!(CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.VOID.ToString().ToLower() ||
                              CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.SUBMIT.ToString().ToLower() ||
                              CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.ACCEPT.ToString().ToLower()))
                        {
                            CCManager.UpdateInitiationStatus(Convert.ToInt32(CycleCountObject.CycleCountID));
                        }
                        foreach (var CC_Item in CycleCountDetailList)
                        {
                            items.Add(CC_Item);
                            //Console.WriteLine("CycleCountDetailList item :", CC_Item.ItemId);

                        }

                        //}
                        pickMgr = new PickItemManager(CommonNavInfo.RouteID.ToString(), CycleCountObject.CycleCountID.ToString(), PickItemManager.PickForTransaction.CycleCount, false);
                        //var dictPickList = new System.Collections.Generic.Dictionary<PickItemManager.PickType,ObservableCollection<Domain.PickItem>>(); 
                        //var d = dictPickList[PickItemManager.PickType.Normal]
                        Dictionary<PickItemManager.PickType, ObservableCollection<dynamic>> pickList = pickMgr.CreatePickList(items);
                        PickItems = pickList[PickItemManager.PickType.Normal];
                        IsScanButtonVisible = true;
                        if (CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.RECNT.ToString().ToLower())
                        {
                            IsScanButtonVisible = false;
                            IsRecountItemsVisible = true;
                        }

                    }
                    SetStatusTypeForCCRequest();
                    //Console.WriteLine("After pick call");
                    foreach (var CC_Item in CycleCountDetailList)
                    {
                        //Console.WriteLine("CycleCountDetailList item :", CC_Item.ItemId);
                    }
                    IsResetButtonEnabled = CycleCountDetailList.Any(x => x.CountedQuantity > 0);

                    if (CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.ACCEPT.ToString().ToLower() ||
                           CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.SUBMIT.ToString().ToLower() ||
                           CycleCountObject.CycleCountStatusCD.ToLower() == StatusTypesEnum.VOID.ToString().ToLower())
                    {
                        IsScanButtonVisible = false;
                        IsVoidEnabled = false;
                        IsHoldEnabled = false;
                        IsResetButtonEnabled = false;
                        IsSubmitButtonEnabled = false;
                        IsManualCountButtonEnabled = false;
                    }
                    else
                        if (CycleCountObject.IsCountInitiated && CycleCountDetailList.Count() > 0)
                            IsSubmitButtonEnabled = true;
                }
                IsBusy = false;

            });
        }

        void pickMgr_PickItemUpdate(object sender, PickItemManager.PickItemEventArgs e)
        {
            IsResetButtonEnabled = CycleCountDetailList.Any(x => x.CountedQuantity > 0);
        }

        private void SetStatusTypeForCCRequest()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels.CycleCountViewModel][CycleCountViewModel][Start:SetStatusTypeForCCRequest()]");

                if (ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any())
                    ResourceManager.StatusTypes = new InventoryManager().GetStatusTypes();
                var typeName = string.Empty;
                var statusType = ResourceManager.StatusTypes.FirstOrDefault(x => x.ID == CycleCountObject.CycleCountStatusID);
                if (statusType != null)
                {
                    typeName = statusType.StatusCode.Contains('-') ? statusType.StatusCode.Replace("-", "").ToLower().Trim() : statusType.StatusCode.ToLower().Trim();
                }
                //  Sets statusType for current CC request
                IEnumerable<StatusTypesEnum> typeEnum = Enum.GetValues(typeof(StatusTypesEnum)).Cast<StatusTypesEnum>();
                requestStatus = typeEnum.First(x => x.ToString().ToLower() == typeName.ToLower());
                Logger.Info("[SalesLogicExpress.Application.ViewModels.CycleCountViewModel][CycleCountViewModel][End:SetStatusTypeForCCRequest()]");

            }
            catch (System.Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CycleCountViewModel][CycleCountViewModel][SetStatusTypeForCCRequest()][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
            }
        }

        public async void GetCycleCountList()
        {
            await Task.Run(() =>
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Application.ViewModels.CycleCountViewModel][CycleCountViewModel][Start:GetCycleCountList()]");
                    if (this.ParentViewModel != null)
                    {
                        ParentViewModel.IsBusy = true;
                    }

                    Managers.CycleCountManager cycleCountM = new CycleCountManager();
                    CycleCountList = cycleCountM.GetCycleCountList(CommonNavInfo.RouteID);

                    EnableDisableButtonsOnStatus(IsCycleCountSelected);

                    if (this.ParentViewModel != null)
                    {
                        ParentViewModel.IsBusy = false;
                    }
                    Logger.Info("[SalesLogicExpress.Application.ViewModels.CycleCountViewModel][CycleCountViewModel][End:GetCycleCountList()]");

                }
                catch (System.Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CycleCountViewModel][CycleCountViewModel][GetCycleCountList()][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                finally
                {
                    if (this.ParentViewModel != null)
                    {
                        ParentViewModel.IsBusy = false;
                        IsBusy = false;
                    }
                }
            });
        }

        //public void GetReasonCode()
        //{
        //    ReasonCode = new Dictionary<int, string>();
        //    Managers.CycleCountManager cycleCountM = new Managers.CycleCountManager();
        //    try
        //    {
        //        ReasonCode = cycleCountM.GetReasonCode();
        //    }
        //    catch (Exception)
        //    {

        //        // throw;
        //    }
        //}

        public void GetReasonCodeForVoid()
        {
            ReasonCode = new ObservableCollection<ReasonCode>();
            Managers.CycleCountManager cycleCountM = new Managers.CycleCountManager();
            try
            {
                ReasonCode = cycleCountM.GetReasonCodeForVoid();
            }
            catch (Exception)
            {

                // throw;
            }
        }
        public void EnableDisableButtonsOnStatus(bool isCycleCountSelected)
        {


            if (isCycleCountSelected)
            {
                // Code for Cycle Count Tab 
                foreach (CycleCount item in CycleCountList)
                {
                    switch (item.CycleCountStatusCD.ToLower())
                    {
                        case "recnt":
                            item.IsItemsToCountVisible = true;
                            item.IsItemsCountedVisible = true;
                            item.IsBorderVisible = true;
                            break;
                        case "void":
                            item.IsItemsToCountVisible = false;
                            item.IsItemsVoidedVisible = true;
                            item.IsItemsCountedVisible = false;
                            break;
                        case "submit":
                        case "accept":
                            item.IsItemsCountedVisible = true; item.IsItemsToCountVisible = false;
                            item.IsBorderVisible = false;
                            if (item.ItemsCountedAfterRecount > 0)
                            {
                                item.IsItemsCountedAfterRecountVisible = true;
                                item.IsBorderVisible = true;
                            }
                            break;
                        case "rels":
                            item.IsItemsToCountVisible = true;
                            if (item.ItemsCounted > 0)
                                item.IsItemsCountedVisible = true;
                            break;
                    }

                    if (item.IsItemsToCountVisible && item.IsItemsCountedVisible)
                    {
                        item.IsBorderVisible = true;
                    }
                    //else if ((item.IsItemsCountedVisible && !item.IsItemsCountedVisible) || (!item.IsItemsCountedVisible && item.IsItemsCountedVisible))
                    //{
                    //    item.IsBorderVisible = false;
                    //}
                }
            }
            else
            {
                // Code for Cycle Count Screen
                IsManualCountButtonEnabled = false;
                switch (CycleCountObject.CycleCountStatusCD.ToLower())
                {
                    case "recnt":
                        //IsSubmitButtonEnabled = IsRecountItemsVisible = true;
                        IsRecountItemsVisible = true;
                        break;
                    case "void":
                        IsSubmitButtonEnabled = IsResetButtonEnabled = IsRecountItemsVisible = IsManualCountButtonEnabled = false;
                        IsVoidEnabled = IsHoldEnabled = false;
                        break;
                    case "submit":
                        IsHoldEnabled = IsSubmitButtonEnabled = IsResetButtonEnabled = IsRecountItemsVisible = IsManualCountButtonEnabled = IsVoidEnabled = false;
                        break;
                    case "rels":
                        IsSubmitButtonEnabled = true;
                        break;
                    case "accept":
                        IsGridSelectionEnabled = IsSubmitButtonEnabled = IsVoidEnabled = IsHoldEnabled = IsResetButtonEnabled = IsManualCountButtonEnabled = false;
                        break;
                }
            }
        }

        public override bool ConfirmSave()
        {
            IsDirty = false;
            CanNavigate = true;
            RemoveDummyItem();
            UpdatePhysicalHeader();
            CCManager.SaveOnHold(CycleCountDetailList, CycleCountObject.RequestTypeStatusCD);
            return base.ConfirmSave();
        }

        private void UpdatePhysicalHeader()
        {
            if (CycleCountObject.RequestTypeStatusCD.ToLower() == StatusTypesEnum.PHYSCL.ToString().ToLower())
            {
                CCManager.UpdateInitiationStatus(CycleCountObject.CycleCountID);
            }
        }

        public override bool ConfirmCancel()
        {
            CanNavigate = false;
            IsDirty = true;
            return base.ConfirmCancel();
        }

        public override bool ConfirmClosed()
        {
            IsDirty = true;
            CanNavigate = false;
            return base.ConfirmClosed();
        }
        #endregion
    }
}
