﻿using System;
using System.Collections.Generic;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain.Prospect_Models;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CreateAndMoveStopViewModel : ViewModelBase
    {
        ServiceRouteManager serviceRouteManager = new ServiceRouteManager();


        public event EventHandler<StopActionCompletedEventArgs> StopModified;

        public DelegateCommand  MoveStop { get; set; }

        public DelegateCommand CreateStop { get; set; }
        public DelegateCommand CalendarSelectionChanged { get; set; }
        public Visitee _customer = new Visitee();
        public Prospect _prospect = new Prospect();
        public Visitee Visitee
        {
            get
            {
                return _customer;
            }
            set
            {
                _customer = value;
                OnPropertyChanged("customer");
            }
        }

        public Prospect Prospect
        {
            get
            {
                return _prospect;
            }
            set
            {
                _prospect = value;
                OnPropertyChanged("Prospect");
            }
        }

        public List<DateTime> _BlackOutDates = new List<DateTime>();

        public List<DateTime> BlackOutDates
        {
            get
            {
                return _BlackOutDates;
            }
            set
            {
                _BlackOutDates = value;
                OnPropertyChanged("BlackOutDates");
            }
        }

        public bool IsCreateButton = true;

        public DateTime SelectableDateStart { get; set; }
        public DateTime SelectableDateEnd { get; set; }
        public DateTime DailyStopDate { set; get; }
        private bool _IsContinueEnabled = false;
        public bool IsContinueEnabled
        {
            get
            {
                return _IsContinueEnabled;
            }
            set
            {
                _IsContinueEnabled = value;
                OnPropertyChanged("IsContinueEnabled");
            }
        }
        public CreateAndMoveStopViewModel(Visitee VisiteeObj)
        {
            Visitee = VisiteeObj;
            IsContinueEnabled = false;
            InitializeCommands();
        }
        //public CreateAndMoveStopViewModel(Visitee visitee)
        //{
        //    visitee = CustomerObj;
        //    IsContinueEnabled = false;
        //    InitializeCommands();
        //}

        public CreateAndMoveStopViewModel()
        {
            // TODO: Complete member initialization
            InitializeCommands();
        }

        private void InitializeCommands()
        {
            MoveStop = new DelegateCommand((param) =>
            {
                IsContinueEnabled = false;
                DateTime moveToDate = Convert.ToDateTime(param);
                if (param != null)
                {
                    if (serviceRouteManager.MoveCustomerStop(Visitee, moveToDate, DailyStopDate))
                    {

                        string stopId = Visitee.StopType.ToLower() == "rescheduled" ? Visitee.ReferenceStopId : Visitee.StopID;
                        string orgDate = Visitee.OriginalDate.HasValue ? Visitee.OriginalDate.Value.ToString("yyyy-MM-dd") : Visitee.StopDate.Value.ToString("yyyy-MM-dd");
                        Visitee.StopID = serviceRouteManager.GetUpdatedStopId(Visitee.VisiteeId, orgDate, stopId);
                        StopActionCompletedEventArgs args = new StopActionCompletedEventArgs();
                        args.DateSelected = moveToDate;
                        args.visiteeChanged = Visitee;
                        OnStopMoved(args);
                    }
                }

            });
            CreateStop = new DelegateCommand((param) =>
            {
                DateTime dateToCreateStop = Convert.ToDateTime(param);
                IsContinueEnabled = false;
                if (param != null)
                {
                    if (serviceRouteManager.CreateUnplannedStop(Visitee, dateToCreateStop))
                    {
                        StopActionCompletedEventArgs args = new StopActionCompletedEventArgs();
                        args.DateSelected = dateToCreateStop;
                        args.visiteeChanged = Visitee;
                        OnStopMoved(args);
                    }
                }

            });
            CalendarSelectionChanged = new DelegateCommand((param) =>
            {
                IsContinueEnabled = true;
            });
        }

        private void OnStopMoved(StopActionCompletedEventArgs e)
        {
            EventHandler<StopActionCompletedEventArgs> handler = StopModified;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
    public class StopActionCompletedEventArgs : EventArgs
    {
        public Visitee visiteeChanged { get; set; }
        public DateTime? DateSelected = null;
        public StopActionCompletedEventArgs()
        {

        }

    }

}
