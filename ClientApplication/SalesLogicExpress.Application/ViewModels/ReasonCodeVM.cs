﻿using SalesLogicExpress.Application.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ReasonCodeVM : ViewModelBase
    {
        ReasonCode _SelectedReasonCode = null;
        public ReasonCode SelectedReasonCode
        {
            get
            {
                return _SelectedReasonCode;
            }
            set
            {
                _SelectedReasonCode = value;
                OnPropertyChanged("SelectedReasonCode");
            }
        }
        List<ReasonCode> _ReasonCodes = new List<ReasonCode>();
        public List<ReasonCode> ReasonCodes
        {
            get
            {
                return _ReasonCodes;
            }
            set
            {
                _ReasonCodes = value;
                OnPropertyChanged("ReasonCodes");
            }
        }
        public ReasonCodeVM()
        {
            LoadReasonCodes();
        }

        async void LoadReasonCodes()
        {
            await Task.Run(() =>
            {
                ReasonCodes = new ReasonCodeManager().GetReasonCodes();
            });
        }
        public class ReasonCode
        {
            public int ID { get; set; }
            public string Code { get; set; }
        }
        public enum ReasonCodeType
        {
            Order,
            ReturnOrder
        }
    }
}
