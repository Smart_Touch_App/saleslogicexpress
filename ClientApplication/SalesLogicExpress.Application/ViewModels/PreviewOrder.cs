﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Domain.Vertex;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Ink;
using SalesLogicExpress.Application.Managers;
using Telerik.Windows.Controls;
using Models = SalesLogicExpress.Domain;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{
    public class PreviewOrder : ViewModelBase, IDataErrorInfo
    {
        public static readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PreviewOrder");
        public PreviewOrder()
        {
            try
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][PreviewOrder][PreviewOrder][START]");

                // TODO: Complete member initialization
                OrderId = Order.OrderId;
                _SignStroke = new StrokeCollection();
                InitializeCommands();
                IsBusy = true;
                LoadData();
                //if (PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtCashCollection.ToString()
                //    || PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtOrderEntry.ToString()
                //    || PayloadManager.OrderPayload.TransactionLastState == ActivityKey.HoldAtPick.ToString())
                //{

                RenderSign();


                //}

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][PreviewOrder][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][PreviewOrder][PreviewOrder][END]");
        }

        private void RenderSign()
        {
            SignatureCanvasVM = new CanvasViewModel(Flow.OrderLifeCycle, SignType.OrderSign, Order.OrderId)
            {
                Height = 280,
                Width = 300,
                FileName = "SmallPic.jpg",
                FooterText = "Customer Signature",
            };
        }

        async void LoadData()
        {
            await Task.Run(() =>
            {
                try
                {
                    Initialize();
                    ObservableCollection<Models.OrderItem> PreviewOrderCollection = new ObservableCollection<Models.OrderItem>(PayloadManager.OrderPayload.Items);
                    PreviewItems = PreviewOrderCollection;
                    if (!string.IsNullOrEmpty(PayloadManager.OrderPayload.Surcharge))
                    {
                        EnergySurchargeValue = PayloadManager.OrderPayload.Surcharge.Replace("$", "");
                    }
                    CalculateTax();
                    CalulateTotal(PreviewOrderCollection);
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    IsBusy = false;
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][PreviewOrder][LoadData][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
        }

        private void Initialize()
        {
            Managers.OrderManager order = new Managers.OrderManager();

            if (string.IsNullOrEmpty(PayloadManager.OrderPayload.Surcharge))
            {
                varEnergySurchargeValue = float.Parse(order.GetEnergySurcharge(SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo, PayloadManager.OrderPayload.Amount));
            }
            else
            {
                varEnergySurchargeValue = float.Parse(PayloadManager.OrderPayload.Surcharge);
            }

            EnergySurchargeValue = varEnergySurchargeValue.ToString("0.00");
            varInvoiceTotalValue = (varOrderTotalValue + varEnergySurchargeValue + varSalesTaxValue);
            InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");
            //PreviewInputText = varEnergySurchargeValue.ToString("0.00");
        }


        // Public Var's

        #region Command Declarations
        public DelegateCommand UpdateAllTotals { get; set; }
        public DelegateCommand PickOrder { get; set; }
        public DelegateCommand SaveSign { get; set; }
        public DelegateCommand ClearCanvas { get; set; }
        public DelegateCommand CompleteAndSign { get; set; }
        public DelegateCommand ResetText { get; set; }
        public DelegateCommand GetReasonCode { get; set; }
        #endregion
        public bool LoadSignState { get; set; }

        CanvasViewModel _SignatureCanvasVM;
        public CanvasViewModel SignatureCanvasVM
        {
            get
            {
                return _SignatureCanvasVM;
            }
            set
            {
                _SignatureCanvasVM = value;
                OnPropertyChanged("SignatureCanvasVM");
            }
        }

        string strDateTime = DateTime.Now.ToString("M'/'dd'/'yyyy' - 'hh:mm ") + DateTime.Now.ToString("tt").ToLower();
        private bool _ComboBoxItemVisibility = false;
        private bool _ComboBoxIsEnabled = false;
        private int _ComboBoxSelectedIndex = 0;
        private bool _ToggleUpdateButton = false;
        private string _SurchargeReasonCode;
        private string _previewTextBoxText = "";
        private string _ErrorText = String.Empty;
        private StrokeCollection _SignStroke = null;
        private int _OrderId;
        private ObservableCollection<Models.OrderItem> previewOrder = new ObservableCollection<Models.OrderItem>();
        bool ResetTextEnable = true;
        private bool _IsCanvasEnable = true;
        bool IsEmptyAllowed = true;
        # region Properties
        public int OrderId
        {
            get { return Order.OrderId; }
            set { _OrderId = value; }
        }
        public bool IsCanvasEnable
        {
            get { return _IsCanvasEnable; }
            set
            {
                _IsCanvasEnable = value;
                OnPropertyChanged("IsCanvasEnable");
            }
        }
        public StrokeCollection SignStroke
        {
            get { return _SignStroke; }
            set
            {
                _SignStroke = value;
                OnPropertyChanged("SignStroke");
            }
        }
        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }
        public Guid MessageToken { get; set; }
        public bool ComboBoxItemVisibility
        {
            get { return _ComboBoxItemVisibility; }
            set { _ComboBoxItemVisibility = value; OnPropertyChanged("ComboBoxItemVisibility"); }
        }
        public string CurrentDate
        {
            get { return strDateTime; }
        }
        public bool ComboBoxIsEnabled
        {
            get { return _ComboBoxIsEnabled; }
            set
            {
                _ComboBoxIsEnabled = value;
                OnPropertyChanged("ComboBoxIsEnabled");
            }

        }


        public int ComboBoxSelectedIndex
        {
            get { return _ComboBoxSelectedIndex; }
            set { _ComboBoxSelectedIndex = value; OnPropertyChanged("ComboBoxSelectedIndex"); }
        }


        public bool ToggleUpdateButton
        {
            get { return _ToggleUpdateButton; }
            set { _ToggleUpdateButton = value; OnPropertyChanged("ToggleUpdateButton"); }
        }

        public string SurchargeReasonCode
        {
            get { return _SurchargeReasonCode; }
            set
            {
                _SurchargeReasonCode = value;
            }
        }
        public string ErrorText
        {
            get { return _ErrorText; }
            set { _ErrorText = value; OnPropertyChanged("ErrorText"); }
        }

        public string PreviewInputText
        {
            get { return _previewTextBoxText; }
            set
            {
                _previewTextBoxText = value;

                OnPropertyChanged("PreviewInputText");
            }
        }

        private bool _ToggleSignPanelVisibility = false;
        public bool ToggleSignPanelVisibility
        {
            get
            {
                return _ToggleSignPanelVisibility;

            }
            set
            {
                _ToggleSignPanelVisibility = value;
                OnPropertyChanged("ToggleSignPanelVisibility");
            }
        }

        private bool _isValidSurcharge;

        public bool IsValidSurcharge
        {
            get { return _isValidSurcharge; }
            set
            {
                _isValidSurcharge = value;
                OnPropertyChanged("IsValidSurcharge");
            }
        }

        // OrderItem Collection for PreviewOrder Grid
        // public ObservableCollection<Models.TemplateItem> PreviewItems
        public ObservableCollection<Models.OrderItem> PreviewItems
        {
            get
            {

                ///  this.previewitems = new Managers.TemplateManager().GetTemplateItemsForCustomer("1108911");
                return previewOrder;
            }
            set
            {
                previewOrder = value;
                OnPropertyChanged("PreviewItems");
            }
        }

        #endregion

        # region Properties For Textbox Values

        float varTotalCoffeeAmtValue, varTotalAlliedAmtValue, varOrderTotalValue, varEnergySurchargeValue, varSalesTaxValue, varInvoiceTotalValue;
        private string _TotalCoffeeAmtValue = "0.00", _TotalAlliedAmtValue = "0.00",
                       _OrderTotalValue = "0.00", _EnergySurchargeValue = "0.00",
                       _SalesTaxValue = "0.00", _InvoiceTotalValue = "0.00";
        public string TotalCoffeeAmtValue
        {
            get
            {
                return "$" + _TotalCoffeeAmtValue;
            }
            set
            {
                _TotalCoffeeAmtValue = value;
                OnPropertyChanged("TotalCoffeeAmtValue");
            }
        }

        public string TotalAlliedAmtValue
        {
            get { return "$" + _TotalAlliedAmtValue; }
            set { _TotalAlliedAmtValue = value; OnPropertyChanged("TotalAlliedAmtValue"); }
        }

        public string OrderTotalValue
        {
            get { return "$" + _OrderTotalValue; }
            set
            {
                _OrderTotalValue = value;
                OnPropertyChanged("OrderTotalValue");
            }
        }

        public string EnergySurchargeValue
        {
            get { return "$" + _EnergySurchargeValue; }
            set
            {
                _EnergySurchargeValue = value;
                OnPropertyChanged("EnergySurchargeValue");
            }
        }
        public string SalesTaxValue
        {
            get { return "$" + _SalesTaxValue; }
            set { _SalesTaxValue = value; OnPropertyChanged("SalesTaxValue"); }
        }
        public string InvoiceTotalValue
        {
            get { return "$" + _InvoiceTotalValue; }
            set { _InvoiceTotalValue = value; OnPropertyChanged("InvoiceTotalValue"); }
        }

        #endregion


        # region Validations

        public string Error
        {
            get { return ErrorText; }
        }

        public string this[string columnName]
        {
            get
            {
                ErrorText = "";
                double value;
                double.TryParse(PreviewInputText.ToString(), out value);
                //Check for multiple decimal points
                if (!double.TryParse(PreviewInputText.ToString(), out value) && !(PreviewInputText == ""))
                {
                    //ErrorText = "Please enter amount in format : 999.99";
                    ErrorText = "";
                    ComboBoxIsEnabled = false;
                    ToggleUpdateButton = false;
                }
                // Check for default value and value greater than 1000
                else if (columnName == "PreviewInputText" && PreviewInputText != "0.00" && value >= 1000)
                {
                    ErrorText = "Please enter value less than $1000";
                    //ErrorText = "";
                    //PreviewInputText = PreviewInputText.Substring(0, 3);
                    ComboBoxIsEnabled = false;
                    ToggleUpdateButton = false;
                }
                // Check for empty value
                else if (PreviewInputText == "")
                {
                    //if (!IsEmptyAllowed)
                    //{
                    //    ErrorText = "Value cannot be empty";
                    ComboBoxIsEnabled = false;
                    ToggleUpdateButton = false;
                    //}
                }
                else if (PreviewInputText != "")
                {
                    if (ComboBoxSelectedIndex == 0)
                    {
                        ToggleUpdateButton = false;
                    }
                    else
                    {
                        ToggleUpdateButton = true;
                    }
                    ComboBoxIsEnabled = true;
                }
                // if value is correct
                else
                {
                    ErrorText = "";
                    ComboBoxIsEnabled = false;
                    ToggleUpdateButton = false;
                    if (!(value == 0))
                    {
                        ComboBoxIsEnabled = true;
                        //Check for reason code 
                        if (columnName == "ComboBoxSelectedIndex" && ComboBoxSelectedIndex == 0)
                        {
                            ErrorText = Constants.OrderPreview.SelectReasonForSurchargeAlert;
                            ToggleUpdateButton = false;
                        }
                        // Check for if user select default reason code text
                        else if (columnName == "ComboBoxSelectedIndex" || columnName == "PreviewInputText" && ComboBoxSelectedIndex != 0)
                        {
                            ToggleUpdateButton = true;
                            ComboBoxItemVisibility = true;
                        }
                        else if (columnName == "PreviewInputText" && ComboBoxSelectedIndex == 0)
                        {
                            ToggleUpdateButton = false;
                        }

                    }
                    else if (columnName == "PreviewInputText" && value == 0)
                    {
                        ComboBoxIsEnabled = true;
                        ToggleUpdateButton = true;
                        ComboBoxIsEnabled = true;
                    }
                }
                if (string.IsNullOrEmpty(ErrorText))
                {
                    IsValidSurcharge = true;
                }
                else
                {
                    IsValidSurcharge = false;
                }
                return ErrorText;
            }
        }
        # endregion

        #region methods
        // Synchronize to calculate values on right panel
        void CalulateTotal(ObservableCollection<Models.OrderItem> orderitem)
        {
            foreach (Models.OrderItem order in orderitem)
            {
                varOrderTotalValue = varOrderTotalValue + float.Parse(order.ExtendedPrice.ToString());
                if (order.SalesCat1.Equals("COF"))
                {
                    varTotalCoffeeAmtValue = varTotalCoffeeAmtValue + float.Parse(order.ExtendedPrice.ToString());

                }
                if (order.SalesCat1.Equals("ALL"))
                {
                    varTotalAlliedAmtValue = varTotalAlliedAmtValue + float.Parse(order.ExtendedPrice.ToString());

                }
            }
            TotalCoffeeAmtValue = varTotalCoffeeAmtValue.ToString("0.00");
            TotalAlliedAmtValue = varTotalAlliedAmtValue.ToString("0.00");
            OrderTotalValue = varOrderTotalValue.ToString("0.00");
            varInvoiceTotalValue = (varOrderTotalValue + varEnergySurchargeValue + varSalesTaxValue);
            InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");
            PayloadManager.OrderPayload.Amount = InvoiceTotalValue.Replace("$", "").ToString();
        }

        // Synchronize to update totals after updating surcharge value
        private void UpdateTotals()
        {

            if (ComboBoxSelectedIndex != 0)
            {
                varEnergySurchargeValue = float.Parse(PreviewInputText.ToString());
                EnergySurchargeValue = varEnergySurchargeValue.ToString("0.00");
                varInvoiceTotalValue = (varOrderTotalValue + varEnergySurchargeValue + varSalesTaxValue);
                InvoiceTotalValue = varInvoiceTotalValue.ToString("0.00");
                PreviewInputText = varEnergySurchargeValue.ToString("0.00");
                PayloadManager.OrderPayload.Surcharge = varEnergySurchargeValue.ToString("0.00");
                PayloadManager.OrderPayload.TaxAmount = varSalesTaxValue.ToString();
                PayloadManager.OrderPayload.Amount = InvoiceTotalValue.Replace("$", "").ToString();
            }

        }

        private void CalculateTax()
        {
            InvoiceObject objInvoice = new InvoiceObject();

            //Calculate tax for current invoice.
            string custClasscode = DbEngine.ExecuteScalar("select AIAC11 from BUSDTA.F03012 where AIAN8 =" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
            string geocode = DbEngine.ExecuteScalar("select AITXA1 from BUSDTA.F03012 where AIAN8 =" + SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo);
            long _geocode = Convert.ToInt32(geocode.Substring(1));
            objInvoice.Customer = SalesLogicExpress.Application.ViewModels.CommonNavInfo.Customer.CustomerNo;
            objInvoice.CustomerClassCode = custClasscode;
            objInvoice.GeoCode = _geocode;
            objInvoice.InvoiceNumber = OrderId.ToString();
            objInvoice.ProductSetCode = "";
            foreach (Models.OrderItem item in PreviewItems)
            {
                string opsetcode = DbEngine.ExecuteScalar("select IBPRP0 from BUSDTA.F4102 where IBLITM =" + "'" + item.ItemNumber + "'");
                string setcode = opsetcode.Trim();
                objInvoice.InvoiceItemList.Add(new InvoiceItem(setcode, item));

            }
            bool bTaxComputed = false;
            try
            {
                Managers.TaxManager taxManager = new Managers.TaxManager();
                bTaxComputed = taxManager.CalculateTax(ref objInvoice);
                new OrderManager().DeleteAndInsertOrderItems(PreviewItems, Convert.ToInt32(PayloadManager.OrderPayload.OrderID));
            }
            catch (FileNotFoundException ex)
            {
                bTaxComputed = false;
                Logger.Debug("Tax manager FileNotFoundException: ", ex);
            }
            catch (Exception ex)
            {
                Logger.Debug("Tax manager Exception: ", ex);
            }
            if (bTaxComputed)
            {
                try
                {
                    Logger.Info("Return Value TaxCalulated() :" + bTaxComputed);
                    varSalesTaxValue = (float)objInvoice.InvoiceTax;
                    SalesTaxValue = varSalesTaxValue.ToString("0.00");
                    OnPropertyChanged("SalesTaxValue");

                    for (int index = 0; index < objInvoice.InvoiceItemList.Count; index++)
                    {
                        string itemId = (objInvoice.InvoiceItemList[index].ItemId.Trim());
                        if ((objInvoice.InvoiceItemList[index].ItemTax != 0))
                        {
                            Logger.Info("Before Updating IsTaxable value to 1");
                            PreviewItems[index].IsTaxable = true;
                            string updateTaxStatus = "update BUSDTA.Order_Detail set IsTaxable =1, ItemSalesTaxAmt =" + objInvoice.InvoiceItemList[index].ItemTax.ToString() + " where OrderID=" + "'" + OrderId + "'" + " and ItemId=" + "" +itemId + "";
                            DbEngine.ExecuteNonQuery(updateTaxStatus);
                            Logger.Info("After Updating IsTaxable value to 1");
                        }
                        else
                        {
                            Logger.Info("Before Updating IsTaxable value to 0");
                            string updateTaxStatus = "update BUSDTA.Order_Detail set IsTaxable =0 where OrderID=" + "'" + OrderId + "'" + " and ItemId=" + "" + itemId + "";
                            DbEngine.ExecuteNonQuery(updateTaxStatus);
                            PreviewItems[index].IsTaxable = false;
                            Logger.Info("After Updating IsTaxable value to 1");
                        }
                    }
                }
                catch (Exception ex)
                {
                    var Alert = new AlertWindow { Message = "Tax Routine Failed" };
                    Messenger.Default.Send<AlertWindow>(Alert, MessageToken);
                    Logger.Debug("File: ProviewOrder.cs Method:CalculateTax()  Failed" + ex.InnerException.Message);

                }
            }
            else
            {
                // TODO: ONLY SHOW DURING DEVELOPMENT STAGE
            }

        }
        #endregion
        private void InitializeCommands()
        {


            UpdateAllTotals = new DelegateCommand((param) =>
            {
                UpdateTotals();
            });


            PickOrder = new DelegateCommand((param) =>
            {
                PayloadManager.OrderPayload.Items = PreviewItems.ToList();

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.PreviewOrder, CloseCurrentView = true, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            SaveSign = new DelegateCommand((param) =>
            {
                Logger.Info("PreviewOrder SaveSign Enter");
                try
                {
                    SignatureCanvasVM.UpdateSignatureToDB(false, false, true, "test.jpg");
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
                    IsBusy = true;
                    PayloadManager.OrderPayload.Surcharge = varEnergySurchargeValue.ToString("0.00");
                    UpdateOrderAndNavigate();

                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.PreviewOrder, CloseCurrentView = true, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);


                }
                catch (Exception ex)
                {
                    Logger.Error("File: PreviewOrder.cs Method:SaveSign Command" + ex.InnerException.ToString());
                }
            });
            ClearCanvas = new DelegateCommand((param) =>
            {
                IsCanvasEnable = true;
                //SignStroke.Clear();
                //base.OnPropertyChanged("SignStroke");
            });
            CompleteAndSign = new DelegateCommand((param) =>
            {
                if (!ToggleSignPanelVisibility)
                    ToggleSignPanelVisibility = true;
                else
                    ToggleSignPanelVisibility = false;
            });
            ResetText = new DelegateCommand((param) =>
            {
                if (ResetTextEnable)
                {
                    PreviewInputText = "";
                    ResetTextEnable = false;
                    IsEmptyAllowed = false;
                }
            });
            GetReasonCode = new DelegateCommand((param) =>
            {
                SurchargeReasonCode = param.ToString();
            });
        }

        async void UpdateOrderAndNavigate()
        {
            await Task.Run(() =>
             {
                 try
                 {
                     double TotalCoffeeAmt = Math.Round(varTotalCoffeeAmtValue, 4);
                     double TotalAlliedAmt = Math.Round(varTotalAlliedAmtValue, 4);
                     double energysurcharge = Math.Round(varEnergySurchargeValue, 4);
                     double ordertotal = Math.Round(varOrderTotalValue, 4);
                     double salestax = Math.Round(varSalesTaxValue, 4);
                     double invoiceTotal = Math.Round(varInvoiceTotalValue, 4);

                     string strSurchargeReasonCode = string.IsNullOrEmpty(SurchargeReasonCode) ? "NULL" : SurchargeReasonCode;

                     string query = "update busdta.ORDER_HEADER set TotalCoffeeAmt =" + TotalCoffeeAmt + ", TotalAlliedAmt =" + TotalAlliedAmt +
                                     ", EnergySurchargeAmt =" + energysurcharge + ", OrderTotalAmt =" + ordertotal + ", SalesTaxAmt = " + salestax +
                                     ", InvoiceTotalAmt =" + invoiceTotal + ", SurchargeReasonCodeId ="
                                     + strSurchargeReasonCode
                                     + " where OrderID =" + Order.OrderId;
                     Logger.Info("Preview Order update busdta.ORDER_HEADER");
                     DbEngine.ExecuteNonQuery(query);
                     Logger.Info("Preview Order update busdta.ORDER_HEADER complete");
                     PayloadManager.OrderPayload.Amount = invoiceTotal.ToString();
                     Logger.Info("Preview Order UpdateOrderStatus");
                     Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.AcceptOrderOnPreview);
                     Logger.Info("Preview Order UpdateOrderStatus exit");

                     Logger.Info("Preview Order Navigation Initiate");
                     Logger.Info("UpdateOrderAndNavigate() moveToView MessageToken=" + MessageToken.ToString());
                     IsBusy = false;
                     foreach (OrderItem item in PreviewItems)
                     {
                         item.LastComittedQty =Convert.ToInt32(item.OrderQty * UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId), item.ItemNumber));
                     }
                     PayloadManager.OrderPayload.Items = PreviewItems.ToList();
                     #region UpdateCommitedQuantity
                     // Log commited qty to inventory_ledger  and update inventory
                     var objInv = new Managers.InventoryManager();
                     if (PayloadManager.OrderPayload.Items != null)
                         objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items, StatusTypesEnum.HOLD);
                     #endregion
                     //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PickOrder, CurrentViewName = ViewModelMappings.View.PreviewOrder, CloseCurrentView = false, ShowAsModal = false };
                     //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                 }
                 catch (Exception ex)
                 {
                     IsBusy = false;
                     Logger.Error("Erro in UpdateOrderAndNavigate" + ex.Message.ToString());
                 }

             });
        }


        public double canvasWidth { get; set; }

        public double canvasHeight { get; set; }

        public InkCanvas InkCanvas { get; set; }
        public static InkCanvas GlobalInkCanvas { get; set; }

    }
}
