﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RouteReplenishmentTabViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.RouteReplenishmentTabViewModel");

        #region Properties & Variables
        public Guid MessageToken { get; set; }
        private int addLoadTypeCode = 0;
        public int AddLoadTypeCode
        {
            get { return addLoadTypeCode; }
            set { addLoadTypeCode = value; }
        }
        private int suggestionReplnTypeCode = 0;

        public int SuggestionReplnTypeCode
        {
            get { return suggestionReplnTypeCode; }
            set { suggestionReplnTypeCode = value; }
        }

        private int suggestionReturnTypeCode = 0;

        public int SuggestionReturnTypeCode
        {
            get { return suggestionReturnTypeCode; }
            set { suggestionReturnTypeCode = value; }
        }


        private int heldReturnTypeCode = 0;

        public int HeldReturnTypeCode
        {
            get { return heldReturnTypeCode; }
            set { heldReturnTypeCode = value; }
        }

        private int unloadTypeCode = 0;

        public int UnloadTypeCode
        {
            get { return unloadTypeCode; }
            set { unloadTypeCode = value; }
        }


        public ObservableCollection<ViewModels.ReasonCode> ReasonCodeList { get; set; }
        private ViewModels.ReasonCode reasonCodeSelected = new ReasonCode();

        private string truckId = string.Empty;
        public string TruckId
        {
            get { return truckId; }
            set { truckId = value; }
        }




        private static bool IsSyncRefresh = true;

        #endregion

        #region OnPropertyChanged

        #region Bool
        private bool isVisibleRelease = false;
        public bool IsVisibleRelease
        {
            get { return isVisibleRelease; }
            set { isVisibleRelease = value; OnPropertyChanged("IsVisibleRelease"); }
        }

        private bool isVisibleVoid = false;
        public bool IsVisibleVoid
        {
            get { return isVisibleVoid; }
            set { isVisibleVoid = value; OnPropertyChanged("IsVisibleVoid"); }
        }

        private bool isEnableRelease = false;
        public bool IsEnableRelease
        {
            get { return isEnableRelease; }
            set { isEnableRelease = value; OnPropertyChanged("IsEnableRelease"); }
        }

        private bool isEnableVoid = false;
        public bool IsEnableVoid
        {
            get { return isEnableVoid; }
            set { isEnableVoid = value; OnPropertyChanged("IsEnableVoid"); }
        }

        private bool isEnableAddLoad = false;
        public bool IsEnableAddLoad
        {
            get { return isEnableAddLoad; }
            set { isEnableAddLoad = value; OnPropertyChanged("IsEnableAddLoad"); }
        }

        private bool isEnableSuggestion = false;
        public bool IsEnableSuggestion
        {
            get { return isEnableSuggestion; }
            set { isEnableSuggestion = value; OnPropertyChanged("IsEnableSuggestion"); }
        }

        private bool isEnablePick = false;
        public bool IsEnablePick
        {
            get { return isEnablePick; }
            set { isEnablePick = value; OnPropertyChanged("IsEnablePick"); }
        }

        private bool isEnablePrint = false;
        public bool IsEnablePrint
        {
            get { return isEnablePrint; }
            set { isEnablePrint = value; OnPropertyChanged("IsEnablePrint"); }
        }

        private bool isEnablePrintYes = true;

        public bool IsEnablePrintYes
        {
            get { return isEnablePrintYes; }
            set { isEnablePrintYes = value; OnPropertyChanged("IsEnablePrintYes"); }
        }

        private bool isEnableSugRepln = false;
        public bool IsEnableSugRepln
        {
            get { return isEnableSugRepln; }
            set { isEnableSugRepln = value; OnPropertyChanged("IsEnableSugRepln"); }
        }


        private bool isEnableSugReturn = false;

        public bool IsEnableSugReturn
        {
            get { return isEnableSugReturn; }
            set { isEnableSugReturn = value; OnPropertyChanged("IsEnableSugReturn"); }
        }



        private bool isEnableHeldReturn = false;

        public bool IsEnableHeldReturn
        {
            get { return isEnableHeldReturn; }
            set { isEnableHeldReturn = value; OnPropertyChanged("IsEnableHeldReturn"); }
        }


        #endregion

        #region string


        private string messageLine1 = string.Empty;
        public string MessageLine1
        {
            get { return messageLine1; }
            set { messageLine1 = value; OnPropertyChanged("MessageLine1"); }
        }

        private string messageLine2 = string.Empty;
        public string MessageLine2
        {
            get { return messageLine2; }
            set { messageLine2 = value; OnPropertyChanged("MessageLine2"); }
        }

        //private string suggestionFrom = string.Empty;
        //public string SuggestionFrom
        //{
        //    get { return suggestionFrom; }
        //    set { suggestionFrom = value; OnPropertyChanged("SuggestionFrom"); }
        //}

        //private string suggestionTo = string.Empty;
        //public string SuggestionTo
        //{
        //    get { return suggestionTo; }
        //    set { suggestionTo = value; OnPropertyChanged("SuggestionTo"); }
        //}

        private string fromBranch = string.Empty;
        public string FromBranch
        {
            get { return fromBranch; }
            set
            {
                fromBranch = value; OnPropertyChanged("FromBranch");
                PayloadManager.RouteReplenishmentPayload.FromBranch = FromBranch;
            }
        }

        private string toBranch = string.Empty;
        public string ToBranch
        {
            get { return toBranch; }
            set
            {
                toBranch = value; OnPropertyChanged("ToBranch");
                PayloadManager.RouteReplenishmentPayload.ToBranch = ToBranch;
            }
        }

        private string transDateToAddLoad = DateTime.Now.Date.ToShortDateString();
        public string TransDateToAddLoad
        {
            get { return transDateToAddLoad; }
            set { transDateToAddLoad = value; OnPropertyChanged("TransDateToAddLoad"); }
        }
        #endregion

        #region Date
        private DateTime selectedTransFromDate = DateTime.Now.Date;
        public DateTime SelectedTransFromDate
        {
            get { return selectedTransFromDate; }
            set
            {
                selectedTransFromDate = value; OnPropertyChanged("SelectedTransFromDate");
                if (SelectedTransToDate < SelectedTransFromDate)
                {
                    SelectedTransToDate = SelectedTransFromDate;
                }
                SelectionTransToDateStart = SelectedTransFromDate;
                PayloadManager.RouteReplenishmentPayload.FromDate = SelectedTransFromDate;
            }
        }

        private DateTime selectionTransFromDateEnd = DateTime.Now.Date.AddDays(21);
        public DateTime SelectionTransFromDateEnd
        {
            get { return selectionTransFromDateEnd; }
            set { selectionTransFromDateEnd = value; OnPropertyChanged("SelectionTransFromDateEnd"); }
        }

        private DateTime selectionTransFromDateStart = DateTime.Now.Date.AddDays(-21);
        public DateTime SelectionTransFromDateStart
        {
            get { return selectionTransFromDateStart; }
            set { selectionTransFromDateStart = value; OnPropertyChanged("SelectionTransFromDateStart"); }
        }

        private DateTime selectedTransToDate = DateTime.Now.Date;
        public DateTime SelectedTransToDate
        {
            get { return selectedTransToDate; }
            set
            {
                selectedTransToDate = value;
                OnPropertyChanged("SelectedTransToDate");
                if (SelectedTransToDate < SelectedTransFromDate)
                {
                    SelectedTransFromDate = SelectedTransToDate;
                }
                //SelectionTransFromDateEnd = SelectedTransToDate;
                PayloadManager.RouteReplenishmentPayload.ToDate = SelectedTransToDate;
            }
        }

        private DateTime selectionTransToDateEnd = DateTime.Now.Date.AddDays(21);
        public DateTime SelectionTransToDateEnd
        {
            get { return selectionTransToDateEnd; }
            set { selectionTransToDateEnd = value; OnPropertyChanged("SelectionTransToDateEnd"); }
        }

        private DateTime selectionTransToDateStart = DateTime.Now.Date.AddDays(-21);
        public DateTime SelectionTransToDateStart
        {
            get { return selectionTransToDateStart; }
            set { selectionTransToDateStart = value; OnPropertyChanged("SelectionTransToDateStart"); }
        }
        #endregion

        #region Class Variables
        private ReplenishmentDetails selectedReplenishment = new ReplenishmentDetails();
        public ReplenishmentDetails SelectedReplenishment
        {
            get { return selectedReplenishment; }
            set
            {
                selectedReplenishment = value;
                if (selectedReplenishment != null)
                {
                    IsEnableRelease = false; IsEnableVoid = false; IsEnablePick = false;
                    if (selectedReplenishment.StatusCode.ToUpper().Trim() == "PEND"
                        || selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP"
                        || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP"
                        || selectedReplenishment.StatusCode.ToUpper().Trim() == "PIC"
                        || selectedReplenishment.StatusCode.ToUpper().Trim() == "INPRS")
                    {
                        if (selectedReplenishment.TypeCode == AddLoadTypeCode || selectedReplenishment.TypeCode == SuggestionReplnTypeCode)
                        {
                            if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                            {
                                IsEnablePick = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableVoid = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableRelease = false;
                            }
                            else
                            {
                                IsEnablePick = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableVoid = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "INPRS") ? true : false;
                                IsEnableRelease = selectedReplenishment.StatusCode.ToUpper().Trim() == "INPRS" ? true : false;
                            }
                        }
                        else if (selectedReplenishment.TypeCode == SuggestionReturnTypeCode || selectedReplenishment.TypeCode == UnloadTypeCode)
                        {
                            if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                            {
                                IsEnablePick = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableVoid = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableRelease = false;
                            }
                            else
                            {
                                IsEnablePick = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableVoid = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableRelease = (selectedReplenishment.StatusCode.ToUpper().Trim() == "PIC") ? true : false;
                            }
                        }
                        else if (selectedReplenishment.TypeCode == HeldReturnTypeCode)
                        {
                            if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                            {
                                IsEnablePick = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableVoid = false;
                                IsEnableRelease = false;
                            }
                            else
                            {
                                IsEnablePick = (selectedReplenishment.StatusCode.ToUpper().Trim() == "RTP" || selectedReplenishment.StatusCode.ToUpper().Trim() == "HOP") ? true : false;
                                IsEnableVoid = false;
                                IsEnableRelease = (selectedReplenishment.StatusCode.ToUpper().Trim() == "PIC") ? true : false;
                            }
                        }
                    }
                    else
                    {
                        IsEnableRelease = false; IsEnableVoid = false; IsEnablePick = false;
                    }
                    IsEnablePrint = selectedReplenishment.StatusCode.ToUpper().Trim() == "VOID" ? false : true;
                    PayloadManager.RouteReplenishmentPayload.FromBranch = SelectedReplenishment.FromBranch;
                    PayloadManager.RouteReplenishmentPayload.ToBranch = SelectedReplenishment.ToBranch;
                    PayloadManager.RouteReplenishmentPayload.FromDate = SelectedReplenishment.TransDateFrom;
                    PayloadManager.RouteReplenishmentPayload.ToDate = SelectedReplenishment.TransDateTo;
                    FromBranch = SelectedReplenishment.FromBranch;
                    IsEnablePrint = true;
                }
                else
                {
                    IsEnableRelease = false; IsEnableVoid = false; IsEnablePick = false;IsEnablePrint = false;
                }
                OnPropertyChanged("SelectedReplenishment");
            }
        }


        private AddLoadBranchDetails addLoadSelectedBranch = new AddLoadBranchDetails();
        public AddLoadBranchDetails AddLoadSelectedBranch
        {
            get { return addLoadSelectedBranch; }
            set { addLoadSelectedBranch = value; OnPropertyChanged("AddLoadSelectedBranch"); }
        }
        #endregion

        #region Collection
        private ObservableCollection<ReplenishmentDetails> replenishmentDetailsCollection;
        public ObservableCollection<ReplenishmentDetails> ReplenishmentDetailsCollection
        {
            get { return replenishmentDetailsCollection; }
            set { replenishmentDetailsCollection = value; OnPropertyChanged("ReplenishmentDetailsCollection"); }
        }


        private ObservableCollection<AddLoadBranchDetails> addLoadBranchList = new ObservableCollection<AddLoadBranchDetails>();
        public ObservableCollection<AddLoadBranchDetails> AddLoadBranchList
        {
            get { return addLoadBranchList; }
            set { addLoadBranchList = value; OnPropertyChanged("AddLoadBranchList"); }
        }

        #endregion

        #endregion

        #region Commands Declaretion
        public DelegateCommand ReplenishmentRelease { get; set; }
        public DelegateCommand ReplenishmentVoid { get; set; }
        public DelegateCommand AddLoad { get; set; }
        public DelegateCommand SuggestionReturn { get; set; }
        public DelegateCommand HeldReturn { get; set; }
        public DelegateCommand Suggestion { get; set; }
        public DelegateCommand AddLoadContinue { get; set; }
        public DelegateCommand SuggestionContinue { get; set; }
        public DelegateCommand BranchSelectionChangedCommand { get; set; }
        public DelegateCommand VoidReplenishment { get; set; }
        public DelegateCommand ShowReplenishment { get; set; }
        public DelegateCommand Pick { get; set; }
        public DelegateCommand Print { get; set; }
        public DelegateCommand PrintYesCommand { get; set; }
        public DelegateCommand PrintNoCommand { get; set; }
        public DelegateCommand ReleaseYesCommand { get; set; }
        public DelegateCommand ReleaseNoCommand { get; set; }

        //   ReplenishmentRelease 

        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {
            AddLoad = new DelegateCommand((param) =>
            {
                AddLoadExecute();
            });
            Suggestion = new DelegateCommand((param) =>
            {
                SuggestionExecute();
            });
            SuggestionReturn = new DelegateCommand((param) =>
            {
                SuggestionReturnExecute();
            });
            HeldReturn = new DelegateCommand((param) =>
            {
                HeldReturnExecute();
            });
            ReplenishmentVoid = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReplenishmentDetails obj = (ReplenishmentDetails)param;
                    ReplenishmentVoidExecute(obj);
                }
            });
            VoidReplenishment = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReasonCode selectedBranch = (ReasonCode)param;
                    VoidReplenishmentExecute(selectedBranch);
                    if (SelectedReplenishment.StatusCode == AddLoadTypeCode.ToString())
                    {
                        IsEnableAddLoad = true;
                    }
                    else if (SelectedReplenishment.StatusCode == SuggestionReplnTypeCode.ToString())
                    {
                        IsEnableSuggestion = true;
                    }
                }
            });
            ReplenishmentRelease = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReplenishmentDetails obj = (ReplenishmentDetails)param;
                    ReplenishmentReleaseExecute(obj);
                }
            });

            SuggestionContinue = new DelegateCommand((param) =>
            {
                SuggestionContinueExecute();
            });
            AddLoadContinue = new DelegateCommand((param) =>
            {
                AddLoadContinueExecute();
            });
            BranchSelectionChangedCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    AddLoadBranchDetails selectedBranch = (AddLoadBranchDetails)param;
                    BranchSelectionChangedCommandExecute(selectedBranch);

                }
            });

            ShowReplenishment = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReplenishmentDetails selectedBranch = (ReplenishmentDetails)param;
                    ShowReplenishmentExecute(selectedBranch);

                }
            });
            Pick = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReplenishmentDetails selectedBranch = (ReplenishmentDetails)param;
                    PickExecute(selectedBranch);

                }
            });
            Print = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ReplenishmentDetails selectedBranch = (ReplenishmentDetails)param;
                    PrintExecute(selectedBranch);

                }
            });
            PrintYesCommand = new DelegateCommand((param) =>
            {
                PrintYesExecute();
            });
            PrintNoCommand = new DelegateCommand((param) =>
            {
                PrintNoExecute();
            });
            ReleaseYesCommand = new DelegateCommand((param) =>
            {
                ReleaseYesExecute();

            });
            ReleaseNoCommand = new DelegateCommand((param) =>
            {
                ReleaseNoExecute();
            });
            //
        }

        public void AddLoadExecute()
        {
            GetAddLoadBranchList();

            //Reset the replenishment status to blank
            PayloadManager.RouteReplenishmentPayload.Status = string.Empty;

            var dialog = new Helpers.DialogWindow { TemplateKey = "AddLoadDialog", Title = "Add Load" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
        }
        public void SuggestionExecute()
        {
            //Reset the replenishment status to blank
            PayloadManager.RouteReplenishmentPayload.Status = string.Empty;
            PayloadManager.RouteReplenishmentPayload.IsSuggestionReplenishment = true;
            PayloadManager.RouteReplenishmentPayload.CurrentViewName = ViewModelMappings.View.ReplenishSggestionView.ToString();

            PayloadManager.RouteReplenishmentPayload.FromBranch = UserManager.ReplenishmentToBranch.ToString();
            PayloadManager.RouteReplenishmentPayload.ToBranch = UserManager.UserRoute.ToString();
            PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = true;

            //Check For Pending Suggestion For the day
            bool flag = RouteReplenishmentTabManagercs.IsAllowSuggestion();
            if (!flag)
            {
                // Show alert for already pending  suggestion 
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "There is open Suggestion Repln. Request." }, MessageToken);
                return;
            }

            var dialog = new Helpers.DialogWindow { TemplateKey = "SuggestionDialog", Title = "Sug. Repln" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
        }
        public void HeldReturnExecute()
        {
            //Navigate to held return
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:HeldReturnExecute]\t" + DateTime.Now + "");

            try
            {
                //Check For Pending Held Return 
                bool flag = RouteReplenishmentTabManagercs.IsOpenHeldReturn();
                if (flag)
                {
                    // Show alert for already pending  suggestion 
                    Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "There is one open Held Return." }, MessageToken);
                    return;
                }


                PayloadManager.RouteReplenishmentPayload.CurrentViewName = ViewModelMappings.View.HeldReturnView.ToString();
                PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = false;
                PayloadManager.RouteReplenishmentPayload.SelectedReplnId = "New";
                PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                PayloadManager.RouteReplenishmentPayload.FromBranch = UserManager.UserRoute.ToString();
                PayloadManager.RouteReplenishmentPayload.ToBranch = UserManager.ReplenishmentToBranch.ToString();
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadView, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel HeldReturnExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:HeldReturnExecute]\t");

        }
        public void SuggestionReturnExecute()
        {
            //Reset the replenishment status to blank
            PayloadManager.RouteReplenishmentPayload.Status = string.Empty;
            PayloadManager.RouteReplenishmentPayload.IsSuggestionReplenishment = false;
            PayloadManager.RouteReplenishmentPayload.CurrentViewName = ViewModelMappings.View.SuggestionReturnView.ToString();
            PayloadManager.RouteReplenishmentPayload.FromBranch = UserManager.UserRoute.ToString();
            PayloadManager.RouteReplenishmentPayload.ToBranch = UserManager.ReplenishmentToBranch.ToString();
            PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = false;
            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;

            //Check For Pending Suggestion For the day
            bool flag = RouteReplenishmentTabManagercs.IsAllowSuggestionReturn();
            if (!flag)
            {
                // Show alert for already pending  suggestion 
                Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "There is open Suggestion Return Request." }, MessageToken);
                return;
            }

            var dialog = new Helpers.DialogWindow { TemplateKey = "SuggestionDialog", Title = "Sug. Return" };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
        }
        public void AddLoadContinueExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:AddLoadContinueExecute]\t" + DateTime.Now + "");

            try
            {
                PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = FromBranch == TruckId ? false : true;
                string loadOrUnload = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? "AddLoad" : "Unload";
                //Check For Pending Add load For the day
                bool flag = RouteReplenishmentTabManagercs.IsAllowAddLoad(loadOrUnload);
                if (!flag)
                {
                    // Show alert for already pending  suggestion 
                    Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "There is one open Add Load request." }, MessageToken);
                    return;
                }



                PayloadManager.RouteReplenishmentPayload.SelectedReplnId = "New";
                PayloadManager.RouteReplenishmentPayload.CurrentViewName = PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck ? ViewModelMappings.View.ReplenishAddLoadView.ToString() : ViewModelMappings.View.ReplenishUnloadView.ToString();
                if (PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck)
                {
                    PayloadManager.RouteReplenishmentPayload.FromBranch = UserManager.ReplenishmentToBranch.ToString();
                    PayloadManager.RouteReplenishmentPayload.ToBranch = UserManager.UserRoute.ToString();
                }
                else
                {
                    PayloadManager.RouteReplenishmentPayload.FromBranch = UserManager.UserRoute.ToString();
                    PayloadManager.RouteReplenishmentPayload.ToBranch = UserManager.ReplenishmentToBranch.ToString();
                }

                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadView, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel AddLoadContinueExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:AddLoadContinueExecute]\t");



        }
        public void SuggestionContinueExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:SuggestionContinueExecute]\t" + DateTime.Now + "");

            try
            {
                PayloadManager.RouteReplenishmentPayload.SelectedReplnId = "New";
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadView, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel SuggestionContinueExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:SuggestionContinueExecute]\t");


        }
        public void ReplenishmentVoidExecute(ReplenishmentDetails param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:ReplenishmentVoidExecute]\t" + DateTime.Now + "");

            try
            {
                PayloadManager.RouteReplenishmentPayload.SelectedReplnId = param.ReplnID;
                PayloadManager.RouteReplenishmentPayload.CurrentViewName = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ?
                    ViewModelMappings.View.ReplenishAddLoadPickView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ?
                    ViewModelMappings.View.ReplenishSuggestionPickView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ?
                    ViewModelMappings.View.SuggestionReturnPickView.ToString() : (SelectedReplenishment.TypeCode == UnloadTypeCode) ?
                    ViewModelMappings.View.ReplenishUnloadPickView.ToString() : ViewModelMappings.View.HeldReturnPickView.ToString();

                if (FromBranch == TruckId)
                {
                    PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = false;
                }
                else
                {
                    PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = true;

                }
                PayloadManager.RouteReplenishmentPayload.Status = param.StatusCode.ToUpper();
                PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;


                var dialog = new Helpers.DialogWindow { TemplateKey = "VoidReplenishment", Title = "Void Replenishment" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel ReplenishmentVoidExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:ReplenishmentVoidExecute]\t");




        }
        public void VoidReplenishmentExecute(ReasonCode param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:VoidReplenishmentExecute]\t" + DateTime.Now + "");

            try
            {
                PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ?
                                   true : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ?
                                   true : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ?
                                   false : (SelectedReplenishment.TypeCode == UnloadTypeCode) ?
                                   false : (SelectedReplenishment.TypeCode == HeldReturnTypeCode) ? false : true;

                if (SelectedReplenishment.StatusCode.ToUpper() == "HOP" || SelectedReplenishment.StatusCode.ToUpper() == "RELS" || SelectedReplenishment.StatusCode.ToUpper() == "PIC")
                {
                    //Check picked Qty. first. If pickedQty==0,  then no need to navigate pick scren
                    int cnt = RouteReplenishmentTabManagercs.GetPickedQty();

                    if (cnt == 0)
                    {
                        ReplenishAddLoadManager.UpdateAddLoadHeader("VOID", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                        ReplenishmentDetailsCollection.FirstOrDefault(x => x.ReplnID == PayloadManager.RouteReplenishmentPayload.SelectedReplnId).Status = "VOID";
                        ReplenishmentDetailsCollection.FirstOrDefault(x => x.ReplnID == PayloadManager.RouteReplenishmentPayload.SelectedReplnId).StatusCode = "VOID";
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                        IsEnableVoid = false; IsEnableRelease = false;
                    }
                    else
                    {
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                        PayloadManager.RouteReplenishmentPayload.NavigatedForVoid = true;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }

                }
                else if (SelectedReplenishment.StatusCode.ToUpper() == "RTP" || SelectedReplenishment.StatusCode.ToUpper() == "INPRS")
                {
                    //Update header
                    ReplenishAddLoadManager.UpdateAddLoadHeader("VOID", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                    ReplenishmentDetailsCollection.FirstOrDefault(x => x.ReplnID == PayloadManager.RouteReplenishmentPayload.SelectedReplnId).Status = "VOID";
                    ReplenishmentDetailsCollection.FirstOrDefault(x => x.ReplnID == PayloadManager.RouteReplenishmentPayload.SelectedReplnId).StatusCode = "VOID";
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    IsEnableVoid = false; IsEnableRelease = false;
                }
                CanAllowOtherReplenishment();

            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel VoidReplenishmentExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:VoidReplenishmentExecute]\t");






        }
        public void ReplenishmentReleaseExecute(ReplenishmentDetails param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:ReplenishmentReleaseExecute]\t" + DateTime.Now + "");

            try
            {
                PayloadManager.RouteReplenishmentPayload.SelectedReplnId = param.ReplnID;
                PayloadManager.RouteReplenishmentPayload.NavigatedForVoid = false;
                PayloadManager.RouteReplenishmentPayload.CurrentViewName = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ? 
                    ViewModelMappings.View.ReplenishAddLoadView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ? 
                    ViewModelMappings.View.ReplenishSggestionView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ? 
                    ViewModelMappings.View.SuggestionReturnView.ToString() : ViewModelMappings.View.HeldReturnView.ToString();

                //Show poopup for realease
                MessageLine1 = (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishAddLoadView.ToString()) ? 
                    "The Add Load ID. " + param.ReplnID + " is about to release." : 
                    (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishSggestionView.ToString()) ?
                    "The Suggestion Replenishment ID. " + param.ReplnID + " is about to release." : 
                    (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()) ? 
                    "The Suggestion Return ID. " + param.ReplnID + " is about to release." : 
                    (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()) ? 
                    "The Unload ID. " + param.ReplnID + " is about to release.":"The Held Return ID. " + param.ReplnID + " is about to release.";
                MessageLine2 = "Do you want to continue?";
                var dialog = new Helpers.DialogWindow { TemplateKey = "ReleaseReplenishment", Title = "Confirmation" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                CanAllowOtherReplenishment();

            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel ReplenishmentReleaseExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:ReplenishmentReleaseExecute]\t");



        }
        public void BranchSelectionChangedCommandExecute(AddLoadBranchDetails param)
        {
            ToBranch = FromBranch;
            FromBranch = param.BranchCode;
        }
        public void ShowReplenishmentExecute(ReplenishmentDetails param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:ShowReplenishmentExecute]\t" + DateTime.Now + "");

            try
            {
                
                PayloadManager.RouteReplenishmentPayload.SelectedReplnId = param.ReplnID;
                PayloadManager.RouteReplenishmentPayload.NavigatedForVoid = false;
                PayloadManager.RouteReplenishmentPayload.CurrentViewName = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ?
                    ViewModelMappings.View.ReplenishAddLoadView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ?
                    ViewModelMappings.View.ReplenishSggestionView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ?
                    ViewModelMappings.View.SuggestionReturnView.ToString() : (SelectedReplenishment.TypeCode == UnloadTypeCode) ?
                    ViewModelMappings.View.ReplenishUnloadView.ToString() : ViewModelMappings.View.HeldReturnView.ToString();


                PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ?
                                    true : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ?
                                    true : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ?
                                    false : (SelectedReplenishment.TypeCode == UnloadTypeCode) ?
                                    false : (SelectedReplenishment.TypeCode == HeldReturnTypeCode) ? false : true;


                PayloadManager.RouteReplenishmentPayload.Status = SelectedReplenishment.StatusCode.ToUpper();
                PayloadManager.RouteReplenishmentPayload.IsSuggestionReplenishment = (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ? true : false;
                if (SelectedReplenishment.TypeCode == AddLoadTypeCode || SelectedReplenishment.TypeCode == SuggestionReplnTypeCode)
                {
                    #region Add load and Sugge. Replenishment
                    if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                    {
                        if (SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }

                    }
                    else
                    {
                        if (SelectedReplenishment.StatusCode.ToUpper() == "INPRS")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP" || SelectedReplenishment.StatusCode.ToUpper() == "RELS" || SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                    }

                    #endregion
                }
                else if (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode || SelectedReplenishment.TypeCode == UnloadTypeCode)
                {
                    #region Sugg Return  && Unload
                    PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = false;
                    if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                    {
                        if (SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }
                    }
                    else
                    {
                        if (SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP"  || SelectedReplenishment.StatusCode.ToUpper() == "PIC")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT" || SelectedReplenishment.StatusCode.ToUpper() == "RELS")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }

                    }
                    #endregion
                }
                else if (SelectedReplenishment.TypeCode == HeldReturnTypeCode)
                {
                    #region Held Region
                    PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = false;
                    if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                    {
                        if (SelectedReplenishment.StatusCode.ToUpper() == "RTP" || SelectedReplenishment.StatusCode.ToUpper() == "HOP")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }
                    }
                    else
                    {
                        if (SelectedReplenishment.StatusCode.ToUpper() == "RTP" || SelectedReplenishment.StatusCode.ToUpper() == "HOP" || SelectedReplenishment.StatusCode.ToUpper() == "PIC")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                        }
                        else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT" || SelectedReplenishment.StatusCode.ToUpper() == "RELS")
                        {
                            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                        }
                    }
                    #endregion
                }
               
                if (SelectedReplenishment.StatusCode.ToUpper() == "VOID")
                {
                    PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                    PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                }
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadView, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel ShowReplenishmentExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:ShowReplenishmentExecute]\t");




        }
        public void PickExecute(ReplenishmentDetails param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:PickExecute]\t" + DateTime.Now + "");

            try
            {
                
                PayloadManager.RouteReplenishmentPayload.Status = param.StatusCode.ToUpper();
                PayloadManager.RouteReplenishmentPayload.SelectedReplnId = param.ReplnID;
                PayloadManager.RouteReplenishmentPayload.NavigatedForVoid = false;

                PayloadManager.RouteReplenishmentPayload.CurrentViewName = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ?
                    ViewModelMappings.View.ReplenishAddLoadPickView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ?
                    ViewModelMappings.View.ReplenishSuggestionPickView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ?
                    ViewModelMappings.View.SuggestionReturnPickView.ToString() : (SelectedReplenishment.TypeCode == UnloadTypeCode) ?
                    ViewModelMappings.View.ReplenishUnloadPickView.ToString() : ViewModelMappings.View.HeldReturnPickView.ToString();


                PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ?
                                    true : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ?
                                    true : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ?
                                    false : (SelectedReplenishment.TypeCode == UnloadTypeCode) ?
                                    false : (SelectedReplenishment.TypeCode == HeldReturnTypeCode) ? false : true;


                PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;
                ReplenishAddLoadManager.UpdateAddLoadHeader("HOP", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadPickView, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel PickExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:PickExecute]\t");



        }
        public void PrintExecute(ReplenishmentDetails param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:PrintExecute]\t" + DateTime.Now + "");

            try
            {
                IsEnablePrintYes = true;

                // Show popup for print
                if (SelectedReplenishment.TypeCode == AddLoadTypeCode)
                {
                    MessageLine1 = "Do you want to print this Add Load Transaction?";
                }
                else if (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode)
                {
                    MessageLine1 = "Do you want to print this Suggestion Transaction?";
                }
                else if (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode)
                {
                    MessageLine1 = "Do you want to print this Suggestion Return Transaction?";
                }
                else if (SelectedReplenishment.TypeCode == HeldReturnTypeCode)
                {
                    MessageLine1 = "Do you want to print this Held Return Transaction?";
                }
                else if (SelectedReplenishment.TypeCode == UnloadTypeCode)
                {
                    MessageLine1 = "Do you want to print this Unload Transaction?";
                }
                var dialog = new Helpers.DialogWindow { TemplateKey = "PrintReplenishment", Title = "Print" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel PrintExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:PrintExecute]\t");



        }
        public void PrintYesExecute()
        {
            IsEnablePrintYes = false;
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
            PrintAsync();

        }

        async void PrintAsync()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:PrintAsync]\t" + DateTime.Now + "");

                try
                {
                    PayloadManager.RouteReplenishmentPayload.SelectedReplnId = SelectedReplenishment.ReplnID;
                    PayloadManager.RouteReplenishmentPayload.NavigatedForVoid = false;
                    PayloadManager.RouteReplenishmentPayload.CurrentViewName = (SelectedReplenishment.TypeCode == AddLoadTypeCode) ? 
                        ViewModelMappings.View.ReplenishAddLoadView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode) ? 
                        ViewModelMappings.View.ReplenishSggestionView.ToString() : (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode) ? 
                        ViewModelMappings.View.SuggestionReturnView.ToString() : (SelectedReplenishment.TypeCode == UnloadTypeCode)?
                        ViewModelMappings.View.ReplenishUnloadView.ToString() : ViewModelMappings.View.HeldReturnView.ToString();
                    
                    if (SelectedReplenishment.TypeCode == AddLoadTypeCode || SelectedReplenishment.TypeCode == SuggestionReplnTypeCode)
                    {
                        #region Add load and Sugge. Replenishment
                        if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                        {
                            if (SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                        }
                        else
                        {
                            if (SelectedReplenishment.StatusCode.ToUpper() == "INPRS")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP" || SelectedReplenishment.StatusCode.ToUpper() == "RELS" || SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                        }

                        #endregion
                    }
                    else if (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode || SelectedReplenishment.TypeCode == UnloadTypeCode)
                    {
                        #region Sugg Return
                        if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                        {
                            if (SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                            }
                        }
                        else
                        {
                            if (SelectedReplenishment.StatusCode.ToUpper() == "RTP")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "HOP" || SelectedReplenishment.StatusCode.ToUpper() == "PIC")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT" || SelectedReplenishment.StatusCode.ToUpper() == "RELS" )
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                            }

                        }
                        #endregion
                    }
                    else if (SelectedReplenishment.TypeCode == HeldReturnTypeCode)
                    {
                        #region Held Region
                        if (PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                        {
                            if (SelectedReplenishment.StatusCode.ToUpper() == "RTP" || SelectedReplenishment.StatusCode.ToUpper() == "HOP")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                            }
                        }
                        else
                        {
                            if (SelectedReplenishment.StatusCode.ToUpper() == "RTP" || SelectedReplenishment.StatusCode.ToUpper() == "HOP" || SelectedReplenishment.StatusCode.ToUpper() == "PIC")
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;
                            }
                            else if (SelectedReplenishment.StatusCode.ToUpper() == "CMPLT" || SelectedReplenishment.StatusCode.ToUpper() == "RELS" )
                            {
                                PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = true;
                            }
                        }
                        #endregion
                    }

                    
                    if (SelectedReplenishment.TypeCode == AddLoadTypeCode)
                    {
                        new ReportViewModels.AddLoadRequestReport(SelectedReplenishment, ReplenishAddLoadManager.GetAddLoadDetails(SelectedReplenishment.ReplnID, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet));
                    }
                    else if (SelectedReplenishment.TypeCode == SuggestionReplnTypeCode)
                    {
                        new ReportViewModels.LoadSuggetionsReport(SelectedReplenishment, ReplenishAddLoadManager.GetAddLoadDetails(SelectedReplenishment.ReplnID, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet));
                    }
                    else if (SelectedReplenishment.TypeCode == HeldReturnTypeCode)
                    {
                        new ReportViewModels.HeldReturnLoadReport(SelectedReplenishment, ReplenishAddLoadManager.GetHeldReturnDetails(SelectedReplenishment.ReplnID, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet));
                    }
                    else if (SelectedReplenishment.TypeCode == UnloadTypeCode)
                    {
                        new ReportViewModels.UnloadReport(SelectedReplenishment, ReplenishAddLoadManager.GetAddLoadDetails(SelectedReplenishment.ReplnID, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet));
                    }
                    else if (SelectedReplenishment.TypeCode == SuggestionReturnTypeCode)
                    {
                        new ReportViewModels.SuggesionReturnLoadReport(SelectedReplenishment, ReplenishAddLoadManager.GetAddLoadDetails(SelectedReplenishment.ReplnID, PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet));
                    }
                    //TODO: CAPTURE PRINT ENDED EVENT AND PUT BELOW ALERT IN THAT
                    ////System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    ////{
                    ////    Messenger.Default.Send<AlertWindow>(new AlertWindow() { Message = "Print Completed." }, MessageToken);
                    ////}));
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteReplenishmentTabViewModel PrintAsync() error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:PrintAsync]\t");





            });

        }
        public void PrintNoExecute()
        {
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
        }

        public void ReleaseYesExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:ReleaseYesExecute]\t" + DateTime.Now + "");

            try
            {
                IsEnableRelease = false; IsEnableVoid = false;
                ReplenishmentDetailsCollection.FirstOrDefault(x => x.ReplnID == PayloadManager.RouteReplenishmentPayload.SelectedReplnId).Status = "RELEASE";
                ReplenishmentDetailsCollection.FirstOrDefault(x => x.ReplnID == PayloadManager.RouteReplenishmentPayload.SelectedReplnId).StatusCode = "RELS";
                ReplenishAddLoadManager.UpdateAddLoadHeader("RELS", PayloadManager.RouteReplenishmentPayload.SelectedReplnId);
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                if ((PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.SuggestionReturnView.ToString()
                        || PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.ReplenishUnloadView.ToString()
                        || PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnView.ToString())
                        && !PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                {
                    // Update Committed qty
                    ReplenishAddLoadManager.UpdateCommittedQtyForReplenishment(PayloadManager.RouteReplenishmentPayload.SelectedReplnId);

                    //Update Replenishment detail after completing  replenishment
                    List<AddLoadDetails> AddLoadDetailsCollection = ReplenishAddLoadManager.GetItemDetailsForReplenishment().ToList();
                    ReplenishAddLoadManager.UpdateAddLoad(AddLoadDetailsCollection, PayloadManager.RouteReplenishmentPayload.SelectedReplnId);

                    if (PayloadManager.RouteReplenishmentPayload.CurrentViewName == ViewModelMappings.View.HeldReturnView.ToString())
                    {
                        LogHeldReturnActivity();
                    }
                    //Refresh Inventory Tab
                    PayloadManager.RouteReplenishmentPayload.IsRefreshInventoryTab = true;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel LogHeldReturnActivity() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:LogHeldReturnActivity]\t");

        }

        public  void LogHeldReturnActivity()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:LogHeldReturnActivity]\t" + DateTime.Now + "");

            try
            {
                //Log activity
                HeldReturnLogActivity hr = new HeldReturnLogActivity();
                hr.ReplnID = PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                hr.ReplnType = "Held Return Item";
                hr.FromBranch = PayloadManager.RouteReplenishmentPayload.FromBranch;
                hr.ToBranch = PayloadManager.RouteReplenishmentPayload.ToBranch;
                hr.RequedtedBy = UserManager.UserName;

                Activity ac = new Activity
                {
                    RouteID = CommonNavInfo.RouteID.ToString(),
                    ActivityType = ActivityKey.HeldReturn.ToString(),
                    ActivityStart = DateTime.Now,
                    IsTxActivity = true,
                    ActivityDetailClass = "HeldReturn",
                    ActivityDetails = hr.SerializeToJson(),
                    ActivityEnd = DateTime.Now,
                    ActivityStatus = "NoActivity"
                };

                new CustomerDashboardManager().LogActivityNoSale(ac);

            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel ReleaseYesExecute() error: " + ex.Message);

            }
           
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:ReleaseYesExecute]\t");

        }

        public void ReleaseNoExecute()
        {
            Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
        }
        #endregion

        #region Constructor & Methods
        public RouteReplenishmentTabViewModel()
        {
            InitialiseCommands();
            InitialiseVariables();
        }

        public void InitialiseVariables()
        {
            GetAddLoadBranchList();
            GetReplenishments();
            CanAllowOtherReplenishment();
            SetTypeCodes();
            GetVoidReasons();
            TruckId = UserManager.UserRoute;
            PayloadManager.RouteReplenishmentPayload.IsNavigatedFromRouteHome = false;
            PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad = Managers.UserManager.UserRoute + ", " + Managers.UserManager.RouteName;
            PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = false;
            PayloadManager.RouteReplenishmentPayload.IsCompletedReplenishmnet = false;

        }
        public void GetReplenishments()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:GetReplenishments]\t" + DateTime.Now + "");

            try
            {
                ReplenishmentDetailsCollection = new ObservableCollection<ReplenishmentDetails>();
                ReplenishmentDetailsCollection = RouteReplenishmentTabManagercs.GetReplenishments();
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel GetReplenishments() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:GetReplenishments]\t");

        }

        public void GetAddLoadBranchList()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:GetAddLoadBranchList]\t" + DateTime.Now + "");

            try
            {
                //string branchType = ConfigurationManager.AppSettings["ReplenishBranchType"].ToString();
                string branchType = UserManager.RepnlBranchType;
                PayloadManager.RouteReplenishmentPayload.IsSalesBranch = branchType == "SalesBranch" ? true : false;

                AddLoadBranchList = new ObservableCollection<AddLoadBranchDetails>();
                AddLoadBranchDetails o = new AddLoadBranchDetails();
                //Dont allow load when there is held return item.
                if (!IsEnableHeldReturn)
                {
                    o.BranchCode = UserManager.ReplenishmentToBranch.ToString();
                    o.BranchName = "Load";
                    AddLoadBranchList.Add(o);
                }

                o = new AddLoadBranchDetails();
                o.BranchCode = UserManager.UserRoute;
                o.BranchName = "Unload";
                AddLoadBranchList.Add(o);
                if (AddLoadBranchList.Count==1)
                {
                    FromBranch = AddLoadBranchList[0].BranchCode.ToString().Trim();
                    ToBranch = UserManager.ReplenishmentToBranch.ToString();
                }
                else
                {
                    FromBranch = UserManager.ReplenishmentToBranch.ToString();
                    ToBranch = UserManager.UserRoute.ToString();
                }
                
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel GetAddLoadBranchList() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:GetAddLoadBranchList]\t");
        }

        public void SetTypeCodes()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:SetTypeCodes]\t" + DateTime.Now + "");

            try
            {
                AddLoadTypeCode = RouteReplenishmentTabManagercs.GetTypeCode("addload");
                SuggestionReplnTypeCode = RouteReplenishmentTabManagercs.GetTypeCode("suggestion");
                SuggestionReturnTypeCode = RouteReplenishmentTabManagercs.GetTypeCode("sugg. return");
                HeldReturnTypeCode = RouteReplenishmentTabManagercs.GetTypeCode("held return");
                UnloadTypeCode = RouteReplenishmentTabManagercs.GetTypeCode("unload");
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel SetTypeCodes() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:SetTypeCodes]\t");

        }

        public void GetVoidReasons()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][Start:GetVoidReasons]\t" + DateTime.Now + "");

            try
            {
                ReasonCodeList = new ObservableCollection<ReasonCode>();
                ReasonCodeList = SettlementConfirmationManager.GetReasonListForVoid("Void Replenishment");
            }
            catch (Exception ex)
            {
                Logger.Error("RouteReplenishmentTabViewModel GetVoidReasons() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteReplenishmentTabViewModel][End:GetVoidReasons]\t");



        }

        public void EnableDisableHeldReturn()
        {

        }

        public void CanAllowOtherReplenishment()
        {
            IsEnableSugRepln = false;
            IsEnableSugReturn = false;
            IsEnableHeldReturn = false;
            IsEnableAddLoad = false;
            //Check for held return item count
            int countHeldItems = RouteReplenishmentTabManagercs.GetHeldReturnItemCount();

            if (countHeldItems > 0)
            {
                IsEnableHeldReturn = true;
                IsEnableAddLoad = true;
            }
            else
            {
                //Check  for open held return replenishment
                bool flag = RouteReplenishmentTabManagercs.IsOpenHeldReturn();
                if (!flag)
                {
                    IsEnableSugReturn = true;
                    IsEnableSugRepln = true;
                    IsEnableAddLoad = true;
                }
            }
        }


        protected override void OnSyncProgressChanged(SyncManager.SyncUpdatedEventArgs e)
        {
            if (!PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
            {
                ReplenishmentDetails obj = ReplenishmentDetailsCollection.FirstOrDefault(x => x.StatusCode.ToUpper() == "RELS");
                if (obj != null)
                {
                    InitialiseCommands();
                    InitialiseVariables();
                    base.OnSyncProgressChanged(e);
                }
            }
        }

        #endregion
    }

    public class ReplenishmentDetails : ViewModelBase
    {
        private string status = string.Empty;
        private string statusCode = string.Empty;

        public string StatusCode
        {
            get { return statusCode; }
            set { statusCode = value; OnPropertyChanged("StatusCode"); }
        }
        private string type = string.Empty;


        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        private int typeCode = 0;

        public int TypeCode
        {
            get { return typeCode; }
            set { typeCode = value; }
        }
        private string replnID = string.Empty;


        public string ReplnID
        {
            get { return replnID; }
            set { replnID = value; }
        }
        private DateTime transDateFrom = DateTime.Now.Date;


        public DateTime TransDateFrom
        {
            get { return transDateFrom.Date; }
            set { transDateFrom = value; }
        }
        private DateTime transDateTo = DateTime.Now.Date;

        public DateTime TransDateTo
        {
            get { return transDateTo.Date; }
            set { transDateTo = value; }
        }
        private string requestedBy = string.Empty;

        public string RequestedBy
        {
            get { return requestedBy; }
            set { requestedBy = value; }
        }
        private string fromBranch = string.Empty;

        public string FromBranch
        {
            get { return fromBranch; }
            set { fromBranch = value; }
        }
        private string toBranch = string.Empty;

        public string ToBranch
        {
            get { return toBranch; }
            set { toBranch = value; }
        }

        public string Status
        {
            get { return status.ToUpper(); }
            set { status = value; OnPropertyChanged("Status"); }
        }
    }

    public class AddLoadBranchDetails
    {
        private string branchName = string.Empty;

        public string BranchName
        {
            get { return branchName; }
            set { branchName = value; }
        }

        private string branchCode = string.Empty;

        public string BranchCode
        {
            get { return branchCode; }
            set { branchCode = value; }
        }
    }
}
