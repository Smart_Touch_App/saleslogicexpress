﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.ReportViewModels;
using SalesLogicExpress.Application.ViewModelPayload;


namespace SalesLogicExpress.Application.ViewModels
{
    public class PreTripInspectionHistoryViewModel : BaseViewModel
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel");
        
        #region Properties
        Guid _MessageToken;
        public Guid MessageToken
        {

            get
            {
                return _MessageToken;
            }
            set
            {
                _MessageToken = value;
                OnPropertyChanged("MessageToken");
            }

        }
        ObservableCollection<PreTripVehicleInspection> _PreTripVehicleInspectionList = new ObservableCollection<PreTripVehicleInspection>();
        ObservableCollection<PreTripVehicleInspection> _SelectedInspections = new ObservableCollection<PreTripVehicleInspection>();
        public ObservableCollection<PreTripVehicleInspection> PreTripVehicleInspectionList
        {
            get
            {
                return _PreTripVehicleInspectionList;
            }
            set
            {
                _PreTripVehicleInspectionList = value;
                OnPropertyChanged("PreTripVehicleInspectionList");
            }
        }
        public ObservableCollection<PreTripVehicleInspection> SelectedItems
        {
            get
            {
                return _SelectedInspections;
            }
            set
            {
                _SelectedInspections = value;
                OnPropertyChanged("SelectedItems");
            }
        }

        PrintSettlementViewModel _PrintSettlementVM = null;
        public PrintSettlementViewModel PrintSettlementVM
        {
            get
            {
                return _PrintSettlementVM;
            }
            set
            {
                _PrintSettlementVM = value;
                OnPropertyChanged("PrintSettlementVM");
            }
        }
        public BaseViewModel ParentViewModel { get; set; }

        private int statusCheck;
        public int StatusCheck
        {
            get { return statusCheck; }
            set { statusCheck = value; OnPropertyChanged("StatusCheck"); }
        }

        private string vehicleModel;
        public string VehicleModel
        {
            get { return vehicleModel; }
            set { vehicleModel = value; OnPropertyChanged("VehicleModel"); }
        }

        private string vehicleNo;
        public string VehicleNo
        {
            get { return vehicleNo; }
            set { vehicleNo = value; OnPropertyChanged("VehicleNo"); }
        }

        private DateTime preTripDateTime;
        public DateTime PreTripDateTime
        {
            get { return preTripDateTime; }
            set { preTripDateTime = value; OnPropertyChanged("PreTripDateTime"); }
        }

        private string odometerReading;
        public string OdometerReading
        {
            get { return odometerReading; }
            set { odometerReading = value; }
        }

        private PreTripVehicleInspection pretripInspectionObject = new PreTripVehicleInspection();
        public PreTripVehicleInspection PretripInspectionObject
        {
            get
            { return pretripInspectionObject; }
            set
            {
                pretripInspectionObject = value;
                OnPropertyChanged("PretripInspectionObject");
            }
        }

        private string route;

        public string Route
        {
            get { return route; }
            set { route = value; OnPropertyChanged("Route"); }
        }
        #endregion

        #region Delegate Commands

        public DelegateCommand ShowListDetails { get; set; }
        public DelegateCommand PrintSettlement { get; set; } 
        #endregion

        #region Constructor and Methods

        Managers.PreTripInspectionManager preTripM = new Managers.PreTripInspectionManager();
        public PreTripInspectionHistoryViewModel(Guid MessageToken)
        {
            Route = PayloadManager.ApplicationPayload.Route;
            this.MessageToken = MessageToken;
            InitializeCommands();
            GetInspectionHistory();
        } 
        void InitializeCommands()
        {
            #region Print section
            PrintSettlement = new DelegateCommand((param) =>
            {
                if (SelectedItems != null && SelectedItems.Count == 0)
                {
                    PrintSettlementVM = new PrintSettlementViewModel
                    {
                        SelectedFromDate = DateTime.Now,
                        SelectedToDate = DateTime.Now,

                        SelectionToDateEnd = DateTime.Now,
                        SelectionFromDateEnd = DateTime.Now,
                        SelectionFromDateStart = SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.BaseDate,
                        SelectionToDateStart = SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.BaseDate
                    };
                    PreTripVehicleInspection preTripObj = preTripM.GetVehicleDetails(Route, string.Empty);
                    PrintSettlementVM.VehicleMake = new ObservableCollection<VehicleMake>();
                    PrintSettlementVM.VehicleNumber = new ObservableCollection<VehicleNumber>();
                    VehicleMake allMake = new VehicleMake() { MakeID="0", Make = "All" };
                    PrintSettlementVM.VehicleMake.Add(allMake);
                    VehicleNumber allNo = new VehicleNumber() { VehicleID = "0", VehicleNo = "All" };
                    PrintSettlementVM.VehicleNumber.Add(allNo);
                    var ENUM = preTripObj.VehicleMake.Select(x => x.Make).Distinct().ToObservableCollection();
                   
                    for (int i = 1; i <= ENUM.Count; i++)
                    {
                        VehicleMake obj = new VehicleMake();
                        obj.MakeID = i.ToString();
                        obj.Make = ENUM[i-1];
                        PrintSettlementVM.VehicleMake.Add(obj);
                    }
                    for (int i = 1; i <= preTripObj.VehicleNo.Count; i++)
                    {
                        VehicleNumber obj = new VehicleNumber();
                        obj.VehicleID = preTripObj.VehicleNo[i - 1].VehicleID;
                        obj.VehicleNo = preTripObj.VehicleNo[i - 1].VehicleNo;
                        PrintSettlementVM.VehicleNumber.Add(obj);
                    }
                    var OpenDialog = new Helpers.ConfirmWindow { TemplateKey = "PrintInspectionPrintSelection", Header = "Print Pre-Trip", OkButtonContent = "Continue", Context = PrintSettlementVM };
                    Messenger.Default.Send<ConfirmWindow>(OpenDialog, MessageToken);
                    if (OpenDialog.Confirmed)
                    {
                        ObservableCollection<PreTripVehicleInspection> newCollection = new ObservableCollection<PreTripVehicleInspection>();

                         newCollection = PrintSettlementVM.GetCollection(PreTripVehicleInspectionList);
                         PrintSelectedItems(newCollection);
                    }
                    else if (OpenDialog.Cancelled)
                    {

                    }
                }
                if (SelectedItems != null && SelectedItems.Count > 0)
                {
                    var OpenDialog = new Helpers.ConfirmWindow { Message = "Do You want to print this inspection form?", Header = "Print" };
                    Messenger.Default.Send<ConfirmWindow>(OpenDialog, MessageToken);
                    if (OpenDialog.Confirmed)
                    {
                        PrintSelectedItems(SelectedItems);
                    }
                    else if (OpenDialog.Cancelled)
                    {

                    }
                }

            });

            #endregion

            #region Show all details
            ShowListDetails = new DelegateCommand((param) =>
            {

                if (param != null)
                {
                    pretripInspectionObject.ShowDetailsOfPreTripInspection = true;

                    var moveToView = new Helpers.NavigateToView
                    {
                        NextViewName = ViewModelMappings.View.PreTripVehicleInspection,
                        CurrentViewName = ViewModelMappings.View.RouteHome,
                        CloseCurrentView = true,
                        ShowAsModal = false,
                        Payload = param as PreTripVehicleInspection

                    };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
            });
            #endregion
            
        }

        
        public async void GetInspectionHistory()
        {
            await Task.Run(() =>
            {
                try
                {
                    PreTripVehicleInspectionList = new ObservableCollection<PreTripVehicleInspection>();
                    Managers.PreTripInspectionManager inspectionManager = new Managers.PreTripInspectionManager();
                    PreTripVehicleInspectionList = inspectionManager.GetPreTripInsectionHistory();

                    for (int i = 0; i < PreTripVehicleInspectionList.Count; i++)
                    {
                        string status = Managers.PreTripInspectionManager.GetStatusTypeForInspection(PreTripVehicleInspectionList[i].StatusID);
                        if (status == "PASS")
                        {
                            StatusCheck = 1;
                        }
                        else
                            StatusCheck = 0;
                        PreTripVehicleInspectionList[i].StatusCheckList = StatusCheck;
                        PreTripVehicleInspectionList[i].StatusCode = status;
                        OdometerReading = PreTripVehicleInspectionList[i].OdometerReading;
                        VehicleModel = PreTripVehicleInspectionList[i].VehicleModel;
                        VehicleNo = PreTripVehicleInspectionList[i].VehicleNumber;
                        PreTripDateTime = PreTripVehicleInspectionList[i].PreTripDateTime;
                    }

                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel][GetInspectionHistory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                }
                finally
                {
                    if (this.ParentViewModel != null)
                    {
                        ParentViewModel.IsBusy = false;
                    }
                }


            });
        }
        public void PrintSelectedItems(ObservableCollection<PreTripVehicleInspection> SelectedItems)
        {
            if (SelectedItems.Count > 0)
            {
                for (int i = 0; i < SelectedItems.Count; i++)
                {
                    SelectedItems[i].QuestionList = preTripM.GetDetailsForPreTrip(SelectedItems[i]);
                    new ReportViewModels.PreTripInspectionReport(SelectedItems[i]);
                }
            }
        }
        #endregion
    }
    /// <summary>
    /// Viewmodel for binding to Print Dialog
    /// </summary>
    public class PrintSettlementViewModel : BaseViewModel
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.PrintSettlementViewModel");

        #region Properties
        private string route;
        public string Route
        {
            get { return route; }
            set { route = value; OnPropertyChanged("Route"); }
        }

        DateTime _SelectedToDate, _SelectedFromDate, _SelectionToDateEnd, _SelectionToDateStart, _SelectionFromDateEnd, _SelectionFromDateStart;
        string _SelectedVehicleMake, _SelectedVehicle;
        public DateTime SelectionFromDateEnd
        {
            get
            {
                return _SelectionFromDateEnd;
            }
            set
            {
                _SelectionFromDateEnd = value;
                OnPropertyChanged("SelectionFromDateEnd");
            }
        }
        public DateTime SelectionFromDateStart
        {
            get
            {
                return _SelectionFromDateStart;
            }
            set
            {
                _SelectionFromDateStart = value;
                OnPropertyChanged("SelectionFromDateStart");
            }
        }
        public DateTime SelectionToDateEnd
        {
            get
            {
                return DateTime.Today;
            }
            set
            {
                _SelectionToDateEnd = value;
                OnPropertyChanged("SelectionToDateEnd");
            }
        }
        public DateTime SelectionToDateStart
        {
            get
            {
                return SelectedFromDate;
            }
            set
            {
                _SelectionToDateStart = value;
                OnPropertyChanged("SelectionToDateStart");
            }
        }
        public DateTime SelectedToDate
        {
            get
            {
                return _SelectedToDate;
            }
            set
            {
                _SelectedToDate = value;
                if (SelectedFromDate > value)
                {
                    SelectedFromDate = value;
                }
                OnPropertyChanged("SelectedToDate");
            }
        }
        public DateTime SelectedFromDate
        {
            get
            {
                return _SelectedFromDate;
            }
            set
            {
                _SelectedFromDate = value;
                if (SelectedToDate < value)
                {
                    SelectedToDate = value;
                }
                if (value != null && value.DayOfYear != DateTime.Now.DayOfYear)
                    SelectionToDateStart = value;
                OnPropertyChanged("SelectedFromDate");
                OnPropertyChanged("SelectionToDateStart");
            }
        }
        public string SelectedVehicleMake
        {
            get
            {
                return _SelectedVehicleMake;
            }
            set
            {
                _SelectedVehicleMake = value;
                OnPropertyChanged("SelectedVehicleMake");
            }
        }
        public string SelectedVehicle
        {
            get
            {
                return _SelectedVehicle;
            }
            set
            {
                _SelectedVehicle = value;
                OnPropertyChanged("SelectedVehicle");
            }
        }

        private PreTripVehicleInspection preTripObj = new PreTripVehicleInspection();
        public PreTripVehicleInspection PreTripObj
        {
            get { return preTripObj; }
            set { preTripObj = value; OnPropertyChanged("PreTripObj"); }
        }
        private ObservableCollection<VehicleNumber> vehicleNumber = new ObservableCollection<VehicleNumber>();
        public ObservableCollection<VehicleNumber> VehicleNumber
        {
            get { return vehicleNumber; }
            set { vehicleNumber = value; OnPropertyChanged("VehicleNumber"); }
        }

        private ObservableCollection<VehicleMake> vehicleMake = new ObservableCollection<VehicleMake>();
        public ObservableCollection<VehicleMake> VehicleMake
        {
            get { return vehicleMake; }
            set { vehicleMake = value; OnPropertyChanged("VehicleMake"); }
        }

        #endregion

        #region DelegtCommand declaration
        public DelegateCommand SelectionOfVehicleNo { get; set; }
        #endregion

        #region Constructor
        public PrintSettlementViewModel()
        {
            Route = PayloadManager.ApplicationPayload.Route;
            InitializeCommands();
        }
        
        #endregion

        #region Methods
        Managers.PreTripInspectionManager preTripM = new Managers.PreTripInspectionManager();
        private void InitializeCommands()
        {
            try
            {
                PreTripVehicleInspection obj = new PreTripVehicleInspection();
                SelectionOfVehicleNo = new DelegateCommand((param) =>
                {
                    if (SelectedVehicleMake != "All")
                    {
                        VehicleNumber.Clear();

                        obj = preTripM.GetVehicleDetails(Route, SelectedVehicleMake);
                        VehicleNumber allNo = new VehicleNumber() { VehicleID = "0", VehicleNo = "All" };
                        VehicleNumber.Add(allNo);
                        for (int i = 1; i <= obj.VehicleNo.Count; i++)
                        {
                            VehicleNumber obj1 = new VehicleNumber();
                            obj1.VehicleID = obj.VehicleNo[i - 1].VehicleID;
                            obj1.VehicleNo = obj.VehicleNo[i - 1].VehicleNo;
                            VehicleNumber.Add(obj1);
                        }
                    }
                    else if (SelectedVehicleMake == "All")
                    {
                        
                    }

                });
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel][GetInspectionHistory][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
        }

        public ObservableCollection<PreTripVehicleInspection> GetCollection(ObservableCollection<PreTripVehicleInspection> preTripList) 
        {
            ObservableCollection<PreTripVehicleInspection> newCollectionForPrint = new ObservableCollection<PreTripVehicleInspection>();
            try
            {
                if (SelectedVehicle == null)
                {
                    SelectedVehicle = "All";
                }
                if (SelectedVehicleMake == null)
                {
                    SelectedVehicleMake = "All";
                }
                var newcollection = preTripList.Where(x => (x.PreTripDateTime.Date >= SelectedFromDate.Date && x.PreTripDateTime.Date <= SelectedToDate.Date)).ToObservableCollection();

                if (SelectedVehicleMake != "All")
                {
                    newcollection = newcollection.Where(x => (x.VehicleModel == SelectedVehicleMake)).ToObservableCollection();
                }
                if (SelectedVehicle != "All")
                {
                    newcollection = newcollection.Where(x => (x.VehicleNumber == SelectedVehicle)).ToObservableCollection();
                }

                newCollectionForPrint = newcollection;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.PreTripInspectionHistoryViewModel][GetCollection][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                throw;
            }
            return newCollectionForPrint;
        }
        #endregion
    }
}
