﻿using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ReturnsAndCredits : BaseViewModel
    {

        #region Properties
        CreditProcessViewModel _creditProcesViewModel;
        public CreditProcessViewModel CreditProcesViewModel
        {
            get
            {
                return _creditProcesViewModel;
            }
            set
            {


                _creditProcesViewModel = value;
                OnPropertyChanged("CreditProcesViewModel");
            }
        }


        CustomerOrdersTabViewModel customersOrdersTabViewModel;
        public CustomerOrdersTabViewModel CustomersOrdersTabViewModel
        {
            get
            {
                return customersOrdersTabViewModel;
            }
            set
            {
                customersOrdersTabViewModel = value;
                OnPropertyChanged("CustomersOrdersTabViewModel");
            }
        }

        CustomerReturnsTabViewModel customerReturnsTabViewModel;
        public CustomerReturnsTabViewModel CustomerReturnsTabViewModel
        {
            get
            {
                return customerReturnsTabViewModel;
            }
            set
            {
                customerReturnsTabViewModel = value;
                OnPropertyChanged("CustomerReturnsTabViewModel");
            }
        }

        ReasonCodeVM _ReasonCodes = new ReasonCodeVM();
        public ReasonCodeVM ReasonCodes
        {
            get
            {
                return _ReasonCodes;
            }
            set
            {
                _ReasonCodes = value;
                OnPropertyChanged("ReasonCodes");
            }
        }

        bool _IsCreditMemoActive = true;
        public bool IsCreditMemoActive
        {
            get
            {
                return _IsCreditMemoActive;
            }
            set
            {

                if (value && PayloadManager.ReturnOrderPayload.ROIsInProgress)
                {
                    ShowHoldVoidDialog();
                    IsOrdersTabActive = true;
                    _IsCreditMemoActive = value;
                    CreditProcesViewModel = new CreditProcessViewModel(false);
                }
                else
                    _IsCreditMemoActive = value;


                OnPropertyChanged("IsCreditMemoActive");
            }
        }
        bool isReturnsTabActive;

        public bool IsReturnsTabActive
        {
            get { return isReturnsTabActive; }
            set
            {
                if (value && PayloadManager.ReturnOrderPayload.ROIsInProgress)
                {
                    if (ShowHoldVoidDialog())
                    {
                        IsOrdersTabActive = true;
                        isReturnsTabActive = value;
                        CustomerReturnsTabViewModel = new CustomerReturnsTabViewModel(this);
                    }
                }
                else
                {
                    isReturnsTabActive = value;
                    if (isReturnsTabActive && customerReturnsTabViewModel == null)
                    {
                        CustomerReturnsTabViewModel = new CustomerReturnsTabViewModel(this);
                    }
                }
                OnPropertyChanged("IsReturnsTabActive");
            }
        }

        bool isOrdersTabActive;

        public bool IsOrdersTabActive
        {
            get { return isOrdersTabActive; }
            set
            {
                isOrdersTabActive = value;
                if (isOrdersTabActive && customersOrdersTabViewModel == null)
                {
                    CustomersOrdersTabViewModel = new CustomerOrdersTabViewModel(this, false);
                }
                OnPropertyChanged("IsOrdersTabActive");
            }
        }

        public Guid MessageToken { get; set; }

        private bool isSwitchDisable = false;
        public bool IsSwitchDisable
        {
            get { return isSwitchDisable; }
            set { isSwitchDisable = value; OnPropertyChanged("IsSwitchDisable"); }
        }

        public bool IsUnplannedCreatedForFuture { get; set; }
        bool changeTab;
        public bool ChangeTab
        {
            get
            {
                return changeTab;
            }
            set
            {


                changeTab = value;
                OnPropertyChanged("ChangeTab");
            }
        }
        #endregion
        public DelegateCommand SearchItemOnType { get; set; }
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand ViewChange { get; set; }
        public DelegateCommand HoldReturnOrderBtn { get; set; }
        public DelegateCommand VoidReturnOrderBtn { get; set; }

        public ReturnsAndCredits(bool flag)
        {
            IsUnplannedCreatedForFuture = flag;
            if (PayloadManager.ReturnOrderPayload.ROIsInProgress || PayloadManager.ReturnOrderPayload.PickIsInProgress)
                IsDirty = true;
            IsBusy = true;
            CreditProcesViewModel = new CreditProcessViewModel(IsUnplannedCreatedForFuture);
            if (PayloadManager.ReturnOrderPayload.PickIsInProgress || PayloadManager.ReturnOrderPayload.ROIsInProgress)
                CustomersOrdersTabViewModel = new CustomerOrdersTabViewModel(this, IsUnplannedCreatedForFuture);
            InitializeCommands();

            //Check if control is coming from customer activity, if yes then set the tab page details
            if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_Activity.ToString())
            {
                SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
            }
            else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.CustomerHome_DashBoard.ToString())
            {
                SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
            }
        }

        private void InitializeCommands()
        {
            ViewChange = new DelegateCommand((term) =>
            {
                SearchText = "";
                if (IsCreditMemoActive && this.CreditProcesViewModel != null)
                {
                    this.CreditProcesViewModel.ResetSearch();
                }
                if (IsReturnsTabActive && this.CustomerReturnsTabViewModel != null)
                {
                    if (!PayloadManager.ReturnOrderPayload.ROIsInProgress)
                        this.CustomerReturnsTabViewModel.ResetSearch();
                }
                if (IsOrdersTabActive && CustomersOrdersTabViewModel != null)
                {
                    if (!PayloadManager.ReturnOrderPayload.ROIsInProgress)
                        this.CustomersOrdersTabViewModel.ResetSearch();
                }
            });
            SearchItemOnType = new DelegateCommand((term) =>
            {
                if (term == null)
                {
                    return;
                }
                string searchText = term as string;
                if (IsCreditMemoActive && this.CreditProcesViewModel != null)
                {
                    this.CreditProcesViewModel.Search(searchText);
                }
                if (IsReturnsTabActive && this.CustomerReturnsTabViewModel != null)
                {
                    this.CustomerReturnsTabViewModel.Search(searchText);
                }
                if (IsOrdersTabActive && CustomersOrdersTabViewModel != null)
                {
                    this.CustomersOrdersTabViewModel.Search(searchText);
                }
            });

            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchItemOnType.Execute(SearchText);
            });
            HoldReturnOrderBtn = new DelegateCommand(param =>
            {
                ChangeTab = true;
                HoldReturnOrder();
            });
            VoidReturnOrderBtn = new DelegateCommand(param =>
            {
                ChangeTab = true;
                VoidRO();
            });
        }

        private bool ShowHoldVoidDialog()
        {
            var dialog = new Helpers.DialogWindow { TemplateKey = "HoldVoidRODialog", Title = "Warning", Payload = this };
            Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            return !(dialog.Cancelled || dialog.Closed);

        }

        public override bool ConfirmCancel()
        {
            IsDirty = false;
            VoidRO();
            return base.ConfirmCancel();
        }

        private void VoidRO()
        {

            #region update quantities
            string tempStopDate = string.Empty;
            string stopID = string.Empty;
            if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
            {
                tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
            }
            else
            {
                tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
            }

            new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
            new CustomerManager().UpdateActivityCount(stopID, false, true);
            PayloadManager.OrderPayload.Customer.CompletedActivity += 1;
            PayloadManager.OrderPayload.Customer.SaleStatus = "";
            new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
            #endregion
            CustomerReturnsManager ItemReturnsManager = new CustomerReturnsManager();
            var activity = new ActivityKey();
            if (PayloadManager.ReturnOrderPayload.ROIsInProgress)
                activity = ActivityKey.VoidAtROPick;
            else
                activity = ActivityKey.VoidAtROEntry;

            ReasonCodes.SelectedReasonCode = null;
            IsDirty = false;
            var ConfirmWindowDialog = new Helpers.ConfirmWindow { TemplateKey = "ReasonCodeDialog", Context = ReasonCodes, Header = "Void Return Order" };
            Messenger.Default.Send<ConfirmWindow>(ConfirmWindowDialog, MessageToken);
            // Check to cancle tab change if popup cancelled or closed
            if (ConfirmWindowDialog.Cancelled || ConfirmWindowDialog.Closed)
            {
                ChangeTab = false;
                IsReturnsTabActive = false;
                IsCreditMemoActive = false;
            }
            if (ConfirmWindowDialog.Confirmed && ReasonCodes.SelectedReasonCode != null)
            {
                bool returnValue = ItemReturnsManager.VoidOrder(PayloadManager.ReturnOrderPayload.ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ReasonCodes.SelectedReasonCode.ID.ToString(), activity);

                if (ItemReturnsManager.IsROPicked(PayloadManager.ReturnOrderPayload.ReturnOrderID))
                {
                    PayloadManager.ReturnOrderPayload.IsNavigatedForVoid = true;
                    // back navigation is disabled if pick screen is in void state
                    CommonNavInfo.IsBackwardNavigationEnabled = false;
                    PayloadManager.ReturnOrderPayload.ReasonCodeID = ReasonCodes.SelectedReasonCode.ID;
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnOrderPickScreen, CurrentViewName = ViewModelMappings.View.ItemReturnsScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    return;
                }
                if (returnValue)
                {
                    ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                    if (!ChangeTab)
                    {
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    ChangeTab = false;
                }
            }
        }
        public override bool ConfirmSave()
        {
            HoldReturnOrder();
            IsDirty = false;
            return base.ConfirmSave();
        }

        private void HoldReturnOrder()
        {
            CustomerReturnsManager ItemReturnsManager = new CustomerReturnsManager();

            var activity = new ActivityKey();
            if (PayloadManager.ReturnOrderPayload.PickIsInProgress)
                activity = ActivityKey.HoldAtROPick;
            else
                activity = ActivityKey.HoldAtROEntry;

            IsDirty = false;

            PayloadManager.ReturnOrderPayload.IsOnHold = true;
            var selected_items = CustomersOrdersTabViewModel.ReturnItems.Where(x => x.IsSelectedItem == true);
            foreach (var item in selected_items)
            {
                PayloadManager.ReturnOrderPayload.ReturnItems.Add(item);
            }
            bool returnValue = ItemReturnsManager.HoldOrder(PayloadManager.ReturnOrderPayload.ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), activity);
            if (returnValue)
            {

                #region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                }
                new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new CustomerManager().UpdateActivityCount(stopID, true, true);
                PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
                #endregion
                ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Dashboard;
                if (!ChangeTab)
                {
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.CustomerHome, CurrentViewName = ViewModelMappings.View.ReturnsAndCredits, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                }
                ChangeTab = false;
            }
        }
        public override bool ConfirmClosed()
        {

            return base.ConfirmClosed();
        }
    }

}
