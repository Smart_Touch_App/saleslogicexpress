﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Telerik.Windows.Controls;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RouteHome : BaseViewModel
    {
        #region Properties and Commands

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.RouteHome");

        public Guid MessageToken { get; set; }
        public DelegateCommand NavigateToServiceRoute { get; set; }
        public DelegateCommand NavigateToPreTripInspection { get; set; }
        public DelegateCommand NavigateToRouteSettlement { get; set; }
        public DelegateCommand SelectReplenishmentTab { get; set; }

        string _Route, _CurrentDate, _RouteName;

        private bool togglePreTripInspectionButton;
        public bool TogglePreTripInspectionButton
        {
            get { return togglePreTripInspectionButton; }
            set { togglePreTripInspectionButton = value; OnPropertyChanged("TogglePreTripInspectionButton"); }
        }

        private bool toggleCustomerServiceButton;
        public bool ToggleCustomerServiceButton
        {
            get { return toggleCustomerServiceButton; }
            set
            {
                toggleCustomerServiceButton = value;
                OnPropertyChanged("ToggleCustomerServiceButton");

                //Update the flag to true if pre-inspection is done. 
                //This flag can be used anywhere to check if pre-inspection has been done or not. 
                //It used to check the pre-trip condition before opening AR payment screen
                CommonNavInfo.IsPretripInspectionDone = value;
            }
        }
        public string Route
        {
            get
            {
                return _Route;
            }
            set
            {
                _Route = value;
                OnPropertyChanged("Route");
            }
        }

        private int heldItemReturnCount = 0;
        public int HeldItemReturnCount
        {
            get { return heldItemReturnCount; }
            set { heldItemReturnCount = value; OnPropertyChanged("HeldItemReturnCount"); }
        }
        #region Pr-Trip Inspection Tab related properties
        PreTripInspectionHistoryViewModel _PreTripInspectionHistoryVM = null;
        public PreTripInspectionHistoryViewModel PreTripInspectionHistoryVM
        {
            get
            {
                return _PreTripInspectionHistoryVM;
            }
            set
            {
                _PreTripInspectionHistoryVM = value;
                OnPropertyChanged("PreTripInspectionHistoryVM");
            }
        }
        bool _IsPreTripInspectionSelected = false;
        public bool IsPreTripInspectionSelected
        {
            get
            {
                return _IsPreTripInspectionSelected;
            }
            set
            {
                _IsPreTripInspectionSelected = value;
                if (value && this.PreTripInspectionHistoryVM == null)
                {
                    IsBusy = true;
                    LoadPreTripInspectionList();
                }
                OnPropertyChanged("IsPreTripInspectionSelected");
            }
        }

        ARAgingSummaryViewModel _ARAgingSummaryVM = null;

        public ARAgingSummaryViewModel ARAgingSummaryVM
        {
            get { return _ARAgingSummaryVM; }
            set { _ARAgingSummaryVM = value; OnPropertyChanged("ARAgingSummaryVM"); }
        }

        bool _IsARAgingSummarySelected = false;
        public bool IsARAgingSummarySelected
        {
            get { return _IsARAgingSummarySelected; }
            set
            {
                _IsARAgingSummarySelected = value;
                if (value && this.ARAgingSummaryVM == null)
                {
                    LoadARAgingSummary();
                }
                OnPropertyChanged("IsARAgingSummarySelected");
            }
        }


        CycleCountViewModel _CycleCountVM = null;
        public CycleCountViewModel CycleCountVM
        {
            get { return _CycleCountVM; }
            set { _CycleCountVM = value; OnPropertyChanged("CycleCountVM"); }
        }

        bool _IsCycleCountSelected = false;
        public bool IsCycleCountSelected
        {
            get { return _IsCycleCountSelected; }
            set
            {
                _IsCycleCountSelected = value;
                if (value && this.CycleCountVM == null)
                {
                    LoadCycleCountList();
                }
                OnPropertyChanged("IsCycleCountSelected");
            }
        }

        #endregion

        #region Inventory Tab related properties
        bool _IsInventorySelected = false;
        public bool IsInventorySelected
        {
            get
            {
                return _IsInventorySelected;
            }
            set
            {
                _IsInventorySelected = value;
                if (value && this.inventoryVM == null)
                {
                    if (!HasPendingCountRequests && !HasPendingCountRequestsForPast)
                    {
                        IsBusy = true;
                        LoadInventory();
                    }
                }
                else if (value && this.inventoryVM != null)
                {
                    if (PayloadManager.RouteReplenishmentPayload.IsRefreshInventoryTab)
                    {
                        PayloadManager.RouteReplenishmentPayload.IsRefreshInventoryTab = false;
                        IsBusy = true;
                        LoadInventory();
                    }
                    this.inventoryVM.MessageToken = MessageToken;
                }
                OnPropertyChanged("IsInventorySelected");
            }
        }
        InventoryTabViewModel inventoryVM = null;
        public InventoryTabViewModel InventoryVM
        {
            get
            {
                return this.inventoryVM;
            }
            set
            {
                this.inventoryVM = value;
                OnPropertyChanged("InventoryVM");
            }
        }
        private bool hasPendingCountRequests = false;

        public bool HasPendingCountRequests
        {
            get { return hasPendingCountRequests; }
            set { hasPendingCountRequests = value; OnPropertyChanged("HasPendingCountRequests"); }
        }

        #endregion

        public string CurrentDate
        {
            get
            {
                return _CurrentDate;
            }
            set
            {
                _CurrentDate = value;
                OnPropertyChanged("CurrentDate");
            }
        }
        public string RouteName
        {
            get
            {
                return _RouteName;
            }
            set
            {
                _RouteName = value;
                OnPropertyChanged("RouteName");
            }
        }

        #region Replenishment Tab

        private RouteReplenishmentTabViewModel routeReplenishmentTabVM;
        public RouteReplenishmentTabViewModel RouteReplenishmentTabVM
        {
            get { return routeReplenishmentTabVM; }
            set { routeReplenishmentTabVM = value; OnPropertyChanged("RouteReplenishmentTabVM"); }
        }


        private bool isReplenishmentTabSelected = false;
        public bool IsReplenishmentTabSelected
        {
            get { return isReplenishmentTabSelected; }
            set
            {
                isReplenishmentTabSelected = value;
                if (value)
                {
                    if (RouteReplenishmentTabVM == null)
                    {
                        RouteReplenishmentTabVM = new RouteReplenishmentTabViewModel();
                        RouteReplenishmentTabVM.MessageToken = this.MessageToken;
                    }
                    RouteReplenishmentTabVM.IsVisibleRelease = true; RouteReplenishmentTabVM.IsVisibleVoid = true;
                }
                else
                {
                    if (RouteReplenishmentTabVM != null)
                    {
                        RouteReplenishmentTabVM.IsVisibleRelease = false; RouteReplenishmentTabVM.IsVisibleVoid = false;
                    }
                }
                OnPropertyChanged("IsReplenishmentTabSelected");
            }
        }

        private bool isDashboardTabSelected = false;

        public bool IsDashboardTabSelected
        {
            get { return isDashboardTabSelected; }
            set { isDashboardTabSelected = value; OnPropertyChanged("IsDashboardTabSelected"); }
        }


        private bool hasPendingCountRequestsForPast;
        public bool HasPendingCountRequestsForPast
        {
            get { return hasPendingCountRequestsForPast; }
            set { hasPendingCountRequestsForPast = value; OnPropertyChanged("HasPendingCountRequestsForPast"); }
        }


        private Brush heldItemReturnBGColor = (Brush)new BrushConverter().ConvertFromString("Green");
        public Brush HeldItemReturnBGColor
        {
            get { return heldItemReturnBGColor; }
            set { heldItemReturnBGColor = value; OnPropertyChanged("HeldItemReturnBGColor"); }
        }
        #endregion

        #endregion

        #region AR Aging summary

        public decimal Age30Days { get; set; }

        public decimal Age60Days { get; set; }

        public decimal Age90Days { get; set; }

        public decimal Age90PlusDays { get; set; }

        public decimal UnappliedCash { get; set; }

        public decimal AgeSummaryTotal
        {
            get
            {
                return (Age30Days + Age60Days + Age90Days + Age90PlusDays + UnappliedCash);
            }
        }


        private void GetAgingDetails()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteHome][Start:GetAgingDetails]");

            try
            {
                //ToDo - Get the aging List 
                ARAgingManager objARAgingMgr = new ARAgingManager();
                ObservableCollection<SalesLogicExpress.Domain.ARAgingSummary> objOpenAging = objARAgingMgr.GetAgingDetails();

                if (objOpenAging != null && objOpenAging.Count > 0)
                {
                    this.Age30Days = objOpenAging.Where(x => x.Age.ToLower().Equals("30 days")).Sum(x => x.OpenInvoiceOpenAmt);
                    this.Age60Days = objOpenAging.Where(x => x.Age.ToLower().Equals("60 days")).Sum(x => x.OpenInvoiceOpenAmt);
                    this.Age90Days = objOpenAging.Where(x => x.Age.ToLower().Equals("90 days")).Sum(x => x.OpenInvoiceOpenAmt);
                    this.Age90PlusDays = objOpenAging.Where(x => x.Age.ToLower().Equals("90+ days")).Sum(x => x.OpenInvoiceOpenAmt);
                }
                else
                {
                    this.Age30Days = 0;
                    this.Age60Days = 0;
                    this.Age90Days = 0;
                    this.Age90PlusDays = 0;
                }

                this.UnappliedCash =(-1)* objARAgingMgr.GetUnappliedAmountForRoute();
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][RouteHome][ExceptionStackTrace = " + ex.Message == null ? ex.InnerException.Message : ex.Message + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteHome][Start:GetAgingDetails]");
        }
        #endregion

        public RouteHome()
        {
            //IsDashboardTabSelected = true;
            PayloadManager.ApplicationPayload.ViewModelLoadComplete = false;
            Route = PayloadManager.ApplicationPayload.Route;
            InitializeCommands();
            LoadData();
            ResourceManager.Synchronization.SyncProgressChanged += Synchronization_SyncProgressChanged;
        }

        void Synchronization_SyncProgressChanged(object sender, SyncManager.SyncUpdatedEventArgs e)
        {
            if (e.State == SyncManager.SyncUpdateType.DownloadComplete)
                ToggleTabsLock();
            if (!HasPendingCountRequests && !HasPendingCountRequestsForPast && IsInventorySelected)
            {
                IsBusy = true; 
                LoadInventory();
            }
           
        }
        async void LoadInventory()
        {
            await Task.Run(() =>
            {
                if(InventoryVM==null)
                {
                    InventoryVM = new InventoryTabViewModel();
                    InventoryVM.ParentViewModel = this;
                }
                else
                {
                    InventoryVM.ParentViewModel = this;
                    InventoryVM.LoadData();
                }

                InventoryVM.IsSearchVisible = true;
                InventoryVM.MessageToken = this.MessageToken;
            });
        }
        async void LoadPreTripInspectionList()
        {
            await Task.Run(() =>
            {
                PreTripInspectionHistoryVM = new PreTripInspectionHistoryViewModel(this.MessageToken);
                PreTripInspectionHistoryVM.ParentViewModel = this;
            });
        }

        async void LoadCycleCountList()
        {
            await Task.Run(() =>
            {
                CycleCountVM = new CycleCountViewModel(messageToken: this.MessageToken, isCycleCountSelected: true, cycleCount: null);
                CycleCountVM.ParentViewModel = this;
            });
        }

        async void LoadARAgingSummary()
        {
            await Task.Run(() =>
            {
                ARAgingSummaryVM = new ARAgingSummaryViewModel();
                ARAgingSummaryVM.ParentViewModel = this;
            });
        }

        public void NavigateRouteSettlement()
        {
            RouteSettlementViewModel.SelectedCalendarDate = DateTime.Now;
            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteSettlement, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false };
            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
        }
        async void LoadData()
        {
            await Task.Run(() =>
            {
                RouteName = Managers.UserManager.RouteName;
                CurrentDate = DateTime.Now.ToString("dddd, MM/dd/yyyy");
                ToggleCustomerServiceFunction();
                ToggleTabsLock();
                GetAgingDetails();
                GetHeldItemReturnStatus();
                ResourceManager.StatusTypes = ResourceManager.StatusTypes == null || !ResourceManager.StatusTypes.Any() ? new InventoryManager().GetStatusTypes() : ResourceManager.StatusTypes;
                PayloadManager.ApplicationPayload.ViewModelLoadComplete = true;
                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = "";
            });
        }

        private void ToggleTabsLock()
        {
            CycleCountManager CCManager = new CycleCountManager();
            var statusDict = CCManager.GetPendingCycleCountStatus();
            if (statusDict != null && statusDict.Count() > 0)
            {
                HasPendingCountRequests = statusDict["PartialLock"] || statusDict["FullLock"] ? true : false;
                HasPendingCountRequestsForPast = statusDict["FullLock"] ? true : false;

                

            }
        }

        void InitializeCommands()
        {
            NavigateToServiceRoute = new DelegateCommand((param) =>
            {
                ServiceRoute.SelectedCalendarDate = DateTime.Now;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ServiceRoute, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            NavigateToPreTripInspection = new DelegateCommand((param) =>
            {
                PreTripInspectionViewModel.SelectedCalendarDate = DateTime.Now;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PreTripVehicleInspection, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Payload = null };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            NavigateToRouteSettlement = new DelegateCommand((param) =>
            {
                NavigateRouteSettlement();
            });

            SelectReplenishmentTab = new DelegateCommand((param) =>
            {
                if (HeldItemReturnCount > 0)
                {
                    IsReplenishmentTabSelected = HeldItemReturnCount > 0 ? true : false;
                    PayloadManager.RouteReplenishmentPayload.CurrentViewName = ViewModelMappings.View.HeldReturnView.ToString();
                    PayloadManager.RouteReplenishmentPayload.IsFromBranchToTruck = false;
                    PayloadManager.RouteReplenishmentPayload.SelectedReplnId = ReplenishAddLoadManager.GetReplnIdForOpenHeldReturn();

                    //Get replenishmnet id of open held return
                    PayloadManager.RouteReplenishmentPayload.IsNonEditableReplenishment = true;
                    PayloadManager.RouteReplenishmentPayload.Status = "HOP";
                    PayloadManager.RouteReplenishmentPayload.FromBranch = UserManager.UserRoute;
                    PayloadManager.RouteReplenishmentPayload.ToBranch = UserManager.ReplenishmentToBranch.ToString();
                    PayloadManager.RouteReplenishmentPayload.IsNavigatedFromRouteHome = true;
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReplenishAddLoadView, CurrentViewName = ViewModelMappings.View.RouteHome, CloseCurrentView = false, ShowAsModal = false, Refresh = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
            });


        }

        public void ToggleCustomerServiceFunction()
        {
            Managers.PreTripInspectionManager inspectionManager = new PreTripInspectionManager();
            Dictionary<int, string> dict= inspectionManager.CheckVerificationSignature(CommonNavInfo.RouteID.ToString());
            //int verifySign = 0;
            //string status = string.Empty;
            int dayOfWeek = (int)DateTime.Now.DayOfWeek;
            if (dict.ContainsValue("pass"))
            {
                ToggleCustomerServiceButton = true;
                TogglePreTripInspectionButton = false;
                //verifySign = dict.Where(x => x.Value=="pass");
                //status = string.IsNullOrEmpty(dict.Values.FirstOrDefault()) ? string.Empty : dict.Values.FirstOrDefault().ToString().ToLower().Trim();
            }
            
            //************************************************************************************************
            // Comment: Commented to enable sunday and saturday.
            // created: Dec 24, 2015
            // Author: Vivensas (Rajesh,Yuvaraj)
            // Revisions: 
            //*************************************************************************************************
            //else if (dayOfWeek != 0 && dayOfWeek != 6)
            //{
            //    ToggleCustomerServiceButton = false;
            //    TogglePreTripInspectionButton = true;
            //}

            else
            {
                ToggleCustomerServiceButton = false;
                TogglePreTripInspectionButton = true;
            }

            //*************************************************************************************************
            // Vivensas changes ends over here
            //**************************************************************************************************
        }

        public void GetHeldItemReturnStatus()
        {
            HeldItemReturnCount = RouteReplenishmentTabManagercs.GetHeldReturnItemCount();
            HeldItemReturnBGColor = HeldItemReturnCount == 0 ? (Brush)new BrushConverter().ConvertFromString("Green") : (Brush)new BrushConverter().ConvertFromString("Red");
        }
    }
}
