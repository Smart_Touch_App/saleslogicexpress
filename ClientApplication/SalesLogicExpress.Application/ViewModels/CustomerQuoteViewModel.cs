﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.ViewModels.ProspectViewModels;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerQuoteViewModel : BaseViewModel
    {

        #region Object and variable declaration

        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerQuoteViewModel");
        private Managers.CustomerQuoteManager manager = new Managers.CustomerQuoteManager();
        private bool _IsProspectCustomer = false;
        private bool _ShowEditModeButton = false;
        private bool _IsQuoteEditable = false;
        private bool _EnableRemoveItemButton = false;
        private CustomerQuoteHeader _CustomerQuote = null;

        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        private int _SearchItemsCount = 0;
        private string _SearchResultText = string.Empty;
        private bool _SearchResultTextVisibility = false;
        private ObservableCollection<Item> _SearchItems = new ObservableCollection<Item>();
        private bool _ToggleExpander = false;
        private bool _IsSearching = false;
        private List<Item> _RemovedSearchItemList = new List<Item>();

        private PricingSetupDialogViewModel _PriceSetupDialogVM = null;

        #endregion

        #region Constructor & destructor

        public CustomerQuoteViewModel()
        {
            //ToDo - Write code to set the flag for the customer

            try
            {
                this.IsBusy = true;
                this.InitializeCommands();
                LoadData(null);
                RemoveItemsFromSearchList();

                //update reference
                PayloadManager.QuotePayload.Parent = this;
            }
            catch (Exception)
            {
                this.IsBusy = false;
                //Do not do anything;
            }
        }

        public CustomerQuoteViewModel(Guid token, bool isProspect, BaseViewModel Parent = null)
        {
            try
            {
                IsBusy = true;
                PayloadManager.ProspectPayload.IsPickedItemRemoved = false;
                //ToDo - Write code to set theflag for the customer
                this.InitializeCommands();
                switch (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab)
                {
                    case "EOD_Transaction":
                        SelectedTab = ViewModelMappings.TabView.RouteSettlement.Transactions;
                        break;
                    case "ProspectHome_Activity":
                        SelectedTab = ViewModelMappings.TabView.ProspectHome.Activity;
                        break;
                    case "ProspectHome_Quote":
                        SelectedTab = ViewModelMappings.TabView.ProspectHome.Quotes;
                        break;
                    case "CustomerHome_Activity":
                        SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                        break;
                    case "CustomerHome_Quote":
                        SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Quotes;
                        break;
                    default:
                        SelectedTab = ViewModelMappings.TabView.ProspectHome.Quotes;
                        break;
                }
                //SelectedTab = ViewModelPayload.PayloadManager.ProspectPayload.IsNavigatedFromActivityTab ? ViewModelMappings.TabView.ProspectHome.Activity : ViewModelMappings.TabView.ProspectHome.Quotes;
                IsProspectCustomer = true;

                LoadData(Parent);
                RemoveItemsFromSearchList();

            }
            catch (Exception)
            {
                IsBusy = false;

            }

        }


        public async void LoadData(BaseViewModel Parent)
        {
            await Task.Run(() =>
            {

                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    try
                    {
                        if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                        {
                            CustomerQuote = manager.GetCustomerQuote();
                            //
                            HasPickingPerformedForQuote = CustomerQuote.Items.Sum(x => x.PickedQty) > 0 ? true : false;
                            #region Set Header
                            CustomerQuote.QuoteIdString = "Prospect Id: " + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID;

                            CustomerQuote.QuotePersonName = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name;

                            CustomerQuote.QuotePersonAddress = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Address;

                            CustomerQuote.QuotePersonCity = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.City;

                            CustomerQuote.QuotePersonState = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.State;

                            CustomerQuote.QuotePersonZip = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Zip;

                            CustomerQuote.QuotePersonPhone = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Phone;

                            CustomerQuote.Parent = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID + ", " + ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name;

                            CustomerQuote.BillTo = CustomerQuote.Parent;
                            #endregion
                            if (CustomerQuote.IsNewQuote)
                            {
                                this.CustomerQuote.Items.CollectionChanged += Items_CollectionChanged;
                                this.IsDirty = false;
                                this.GetSearchItems();
                                this.IsQuoteEditable = true;
                            }
                            else
                            {
                                //button should be enabled 
                                //1. when quote is not printed 
                                //2. Is create on the same day
                                if (CustomerQuote.IsPrinted || ((DateTime.Now.Date - CustomerQuote.QuoteDate.Date).TotalDays != 0) || CustomerQuote.IsSettled)
                                {
                                    this.IsQuoteEditable = false;
                                }
                                else
                                {
                                    this.IsQuoteEditable = true;
                                    this.GetSearchItems();
                                }
                            }
                            ShowEditModeButton = ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton;

                            EnableDisableButtons();

                            this.ParentView = Parent != null ? Parent : this;
                            //Get all the pricing list in memory
                            this.PriceSetupDialogVM.LoadLists();
                            if (!IsEnablePickButton && IsEnableCompleteButton && !ViewModelPayload.PayloadManager.ProspectPayload.ShowEditModeButton)
                            {
                                this.IsDirty = true; //data is saved but picking has not completed yet and hence re-setting the value to true
                            }
                            IsBusy = false;

                        }
                        else
                        {
                            CustomerQuote = manager.GetCustomerQuote();

                            //this.ToggleUnitPriceVisibility(); 

                            switch (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab)
                            {
                                case "EOD_Transaction":
                                    SelectedTab = ViewModelMappings.TabView.RouteSettlement.Transactions;
                                    break;
                                case "ProspectHome_Activity":
                                    SelectedTab = ViewModelMappings.TabView.ProspectHome.Activity;
                                    break;
                                case "ProspectHome_Quote":
                                    SelectedTab = ViewModelMappings.TabView.ProspectHome.Quotes;
                                    break;
                                case "CustomerHome_Activity":
                                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Activity;
                                    break;
                                case "CustomerHome_Quote":
                                    SelectedTab = ViewModelMappings.TabView.CustomerDashboard.Quotes;
                                    break;
                                default:
                                    SelectedTab = ViewModelMappings.TabView.ProspectHome.Quotes;
                                    break;
                            }

                            //Get all the pricing list in memory
                            this.PriceSetupDialogVM.LoadLists();

                            if (CustomerQuote.IsNewQuote)
                            {
                                this.CustomerQuote.Items.CollectionChanged += Items_CollectionChanged;
                                this.ShowEditModeButton = false;
                                this.IsDirty = false;
                                this.IsQuoteEditable = true;
                            }
                            else
                            {

                                //button should be enabled 
                                //1. when quote is not printed 
                                //2. Is create on the same day
                                if (CustomerQuote.IsPrinted || ((DateTime.Now.Date - CustomerQuote.QuoteDate.Date).TotalDays != 0) || CustomerQuote.IsSettled)
                                {
                                    this.IsQuoteEditable = false;
                                }
                                else
                                {
                                    this.IsQuoteEditable = true;
                                    manager.ApplyPricingToTemplates(this.CustomerQuote.Items, CommonNavInfo.UserBranch, !IsProspectCustomer ? CommonNavInfo.Customer.CustomerNo : PayloadManager.ProspectPayload.Prospect.ProspectID, this.CustomerQuote.OverridePriceSetup);
                                }

                                this.ShowEditModeButton = true;
                            }

                            this.GetSearchItems();
                            //Set Header

                            CustomerQuote.QuoteIdString = "Customer Id:" + CommonNavInfo.Customer.CustomerNo;

                            CustomerQuote.QuotePersonName = CommonNavInfo.Customer.Name;

                            CustomerQuote.QuotePersonAddress = CommonNavInfo.Customer.Address;

                            CustomerQuote.QuotePersonCity = CommonNavInfo.Customer.City;

                            CustomerQuote.QuotePersonState = CommonNavInfo.Customer.State;

                            CustomerQuote.QuotePersonZip = CommonNavInfo.Customer.Zip;

                            CustomerQuote.QuotePersonPhone = CommonNavInfo.Customer.Phone;

                            IsEnableCompleteButton = false;
                            this.IsBusy = false;
                        }

                    }
                    catch (Exception ex)
                    {
                        IsBusy = false;

                    }
                }));
            });
        }
        public void RemoveItemsFromSearchList()
        {
            if (CustomerQuote != null)
                foreach (CustomerQuoteDetail item in CustomerQuote.Items)
                {
                    Item itemInSearch = SearchItemList.FirstOrDefault(x => x.ItemId == item.ItemId);
                    if (itemInSearch != null)
                    {
                        RemovedSearchItemList.Add(itemInSearch);

                        SearchItemList.Remove(itemInSearch);
                    }
                }
        }


        #endregion

        #region Properties

        /// <summary>
        /// Get or set price-set dialog 
        /// </summary>
        public PricingSetupDialogViewModel PriceSetupDialogVM
        {
            get
            {
                PayloadManager.ProspectPayload.IsForPricingTab = false; ;

                if (_PriceSetupDialogVM == null)
                {
                    _PriceSetupDialogVM = new PricingSetupDialogViewModel(this.CustomerQuote, MessageToken, this);
                }

                return _PriceSetupDialogVM;
            }
            set
            {
                _PriceSetupDialogVM = value;
                this.OnPropertyChanged("PriceSetupDialogVM");
            }
        }

        /// <summary>
        /// Get or set flag to determine whether customer is prospect
        /// </summary>
        public bool IsProspectCustomer
        {
            get { return _IsProspectCustomer; }
            set
            {
                _IsProspectCustomer = value;
                OnPropertyChanged("IsProspectCustomer");
            }
        }

        public Guid MessageToken { get; set; }

        /// <summary>
        /// Get or set customer quote 
        /// </summary>
        public CustomerQuoteHeader CustomerQuote
        {
            get
            {
                return _CustomerQuote;
            }
            set
            {
                _CustomerQuote = value;
                OnPropertyChanged("CustomerQuote");
            }
        }

        /// <summary>
        /// Get or set flag to hide/show buttons for edit mode
        /// </summary>
        public bool ShowEditModeButton
        {
            get { return _ShowEditModeButton; }
            set
            {
                _ShowEditModeButton = value;
                OnPropertyChanged("ShowEditModeButton");
            }
        }

        public bool IsQuoteEditable
        {
            get
            {
                return _IsQuoteEditable;
            }
            set
            {
                _IsQuoteEditable = value;
                OnPropertyChanged("IsQuoteEditable");
            }
        }


        /// <summary>
        /// Get or set flag to enable / disable remove item button 
        /// </summary>
        public bool EnableRemoveItemButton
        {
            get
            {
                return _EnableRemoveItemButton && IsQuoteEditable;
            }
            set
            {
                _EnableRemoveItemButton = value;
                OnPropertyChanged("EnableRemoveItemButton");
            }
        }


        private bool isEnablePickButton = false;

        public bool IsEnablePickButton
        {
            get { return isEnablePickButton; }
            set
            {
                isEnablePickButton = value; OnPropertyChanged("IsEnablePickButton");
                PayloadManager.ProspectPayload.IsPartialPicking = IsEnablePickButton;
            }
        }

        private bool isEnableCompleteButton = false;

        public bool IsEnableCompleteButton
        {

            get { return isEnableCompleteButton; }

            set { isEnableCompleteButton = value; OnPropertyChanged("IsEnableCompleteButton"); }

        }

        private bool hasPickingPerformedForQuote = false;

        public bool HasPickingPerformedForQuote
        {
            get { return hasPickingPerformedForQuote; }
            set { hasPickingPerformedForQuote = value; }
        }

        #region Search grid properties

        /// <summary>
        /// Get or set flag to togle expander
        /// </summary>
        public bool ToggleExpander
        {
            get { return _ToggleExpander; }
            set
            {
                _ToggleExpander = value;
                OnPropertyChanged("ToggleExpander");
            }
        }

        /// <summary>
        /// Get or set flag to show search in progress indicator 
        /// </summary>
        public bool IsSearching
        {
            get
            {
                return _IsSearching;
            }
            set
            {


                _IsSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }

        /// <summary>
        /// Get or set item search list 
        /// </summary>
        public ObservableCollection<Item> SearchItemList
        {
            get
            {
                return this._SearchItems;
            }
            set
            {
                try
                {
                    this._SearchItems = value;
                    OnPropertyChanged("SearchItemList");
                    OnPropertyChanged("SearchResultText");
                }
                catch (Exception) { }



            }
        }

        /// <summary>
        /// Add or remove items to the search item remove list 
        /// </summary>
        public List<Item> RemovedSearchItemList
        {
            get
            {
                return _RemovedSearchItemList;
            }
            set
            {
                _RemovedSearchItemList = value;
            }
        }
        /// <summary>
        /// Get or set search items count 
        /// </summary>
        public int SearchItemsCount
        {
            get
            {

                return _SearchItemsCount;
            }
            set
            {
                _SearchItemsCount = value;
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            }
        }

        /// <summary>
        /// Get or set search result text 
        /// </summary>
        public string SearchResultText
        {
            get
            {
                if (SearchItemsCount == 0)
                {
                    return "No items found";
                }
                else
                {
                    SearchResultTextVisibility = true;
                    OnPropertyChanged("SearchResultTextVisibility");
                    return "Items found " + SearchItemsCount;
                }
            }
            set
            {
                if (value != _SearchResultText)
                {
                    _SearchResultText = value;
                    OnPropertyChanged("SearchResultText");
                }
            }
        }

        /// <summary>
        /// Get or set search result text visibility flag
        /// </summary>
        public bool SearchResultTextVisibility
        {
            get
            {
                return _SearchResultTextVisibility;
            }
            set
            {
                _SearchResultTextVisibility = value;
            }
        }

        #endregion

        #endregion

        #region Delegate Properties

        public DelegateCommand QuoteQuantityChangeCmdDlgt { get; set; }

        public DelegateCommand QuantityLostFocusDlgtCmd { get; set; }
        public DelegateCommand QuoteItemSelectionChanged { get; set; }

        public DelegateCommand AddItemToQuote { get; set; }
        public DelegateCommand SearchResultTextChange { get; set; }

        public DelegateCommand ClearSearchText { get; set; }

        public DelegateCommand SearchItemOnType { get; set; }

        public DelegateCommand SearchItem { get; set; }

        public DelegateCommand OpenExpander { get; set; }

        public DelegateCommand RowEditEndedDlgtCmd { get; set; }
        public DelegateCommand OpenQuoteScreenDlgtCmd { get; set; }

        public DelegateCommand OpenQuoteSetupDialogDlgCmd { get; set; }

        public DelegateCommand RemoveItemDlgtCmd { get; set; }

        public DelegateCommand SaveQuoteDlgtCmd { get; set; }
        public DelegateCommand TransactionUMChange { get; set; }
        public DelegateCommand SampleUMChange { get; set; }
        public DelegateCommand UpdateAvailableQty { get; set; }
        public DelegateCommand PickDlgtCmd { get; set; }
        public DelegateCommand UpdateSampleQty { get; set; }
        public DelegateCommand CopyQuoteDlgCmd { get; set; }

        public DelegateCommand PrintQuoteDlgtCmd { get; set; }
        #endregion

        #region public Methods
        public async Task SearchItems(string searchTerm, bool skipMinLengthCheck)
        {
            await Task.Run(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Managers.ItemManager itemManager = new Managers.ItemManager();

                    if (!skipMinLengthCheck)
                    {
                        if (searchTerm != null && searchTerm.ToString().Length < 3)
                        {
                            return;
                        }
                        if (searchTerm != null && searchTerm.ToString().Length == 0)
                        {
                            SearchItemList.Clear();
                            RemovedSearchItemList.Clear();
                            this.OnPropertyChanged("SearchItemsCount");
                            this.OnPropertyChanged("SearchResultText");
                            return;
                        }
                    }
                    SearchItemList.Clear();
                    RemovedSearchItemList.Clear();
                    var visiteeId = PayloadManager.ProspectPayload.IsProspect ? PayloadManager.ProspectPayload.Prospect.ProspectID : CommonNavInfo.Customer.customerNo;
                    SearchItemList = itemManager.SearchItem(searchTerm.ToString(), visiteeId);

                    this.RemoveItemsFromSearchList();
                    SearchItemsCount = SearchItemList.Count;
                    this.OnPropertyChanged("SearchItemsCount");
                    this.OnPropertyChanged("SearchResultText");
                }));
            }, tokenForCancelTask.Token);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Iniatiliazes quote summary list 
        /// </summary>
        private void InitializeCommands()
        {

            #region Customer Quote Delegate Commands
            QuoteQuantityChangeCmdDlgt = new DelegateCommand(
              (param) =>
              {
                  this.IsDirty = true && this.IsQuoteEditable;
              }
              );

            QuantityLostFocusDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    manager.ApplyPricingToTemplates(this.CustomerQuote.Items, CommonNavInfo.UserBranch, !IsProspectCustomer ? CommonNavInfo.Customer.CustomerNo : PayloadManager.ProspectPayload.Prospect.ProspectID, this.CustomerQuote.OverridePriceSetup);
                }
                );

            RowEditEndedDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    this.IsDirty = true;
                    this.IsQuoteEditable = true;
                    //manager.ApplyPricingToTemplates(this.CustomerQuote.Items, CommonNavInfo.UserBranch, !IsProspectCustomer ? CommonNavInfo.Customer.CustomerNo : PayloadManager.ProspectPayload.Prospect.ProspectId, this.CustomerQuote.OverridePriceSetup);

                }
                );

            CopyQuoteDlgCmd = new DelegateCommand(
                (param) =>
                {
                    //
                    if (PayloadManager.ProspectPayload.IsProspect && PayloadManager.ProspectPayload.IsPartialPicking)
                    {
                        //Alert  for picking
                        string messagestr = Helpers.Constants.Quote.CustomerQuoteBackNavigationMessagePartialPick;
                        var alertMessage = new Helpers.AlertWindow { Header = "Alert", Message = messagestr, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    string confirmationMessage = "Only Item(s) will be copied to the new quote.\nDo you want to continue?";
                    ConfirmWindow confirmReadOnlyNavigation = new Helpers.ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No" };
                    confirmReadOnlyNavigation.Message = confirmationMessage;

                    Messenger.Default.Send<Helpers.ConfirmWindow>(confirmReadOnlyNavigation, MessageToken);

                    if (confirmReadOnlyNavigation.Confirmed)
                    {
                        this.IsBusy = true;
                        this.CustomerQuote.QuoteId = 0;
                        this.IsDirty = true;
                        this.IsQuoteEditable = true;
                        this.IsEnableCompleteButton = true;
                        this.CustomerQuote.IsPrinted = false;
                        this.CustomerQuote.IsNewQuote = true;
                        this.CustomerQuote.IsSampled = false;
                        this.ShowEditModeButton = false;
                        this.CustomerQuote.OverridePriceSetup = manager.GetMasterPriceDetails(true);

                        ViewModelPayload.PayloadManager.QuotePayload.QuoteId = "NEW";

                        this.CustomerQuote.QuoteDate = DateTime.Now;
                        this.CustomerQuote = this.CustomerQuote;//Refresh all the screen
                        this.CustomerQuote.IsSettled = false;
                        this.CustomerQuote.Items.ToList().ForEach(x => { x.SampleQty = 0; x.SampleUOM = ""; x.SelectedQtyUOMSample = new QtyUOMClass(); x.PickedQty = 0; x.PickedUOM = ""; });
                        //Set availabl eqty
                        SetAvailableQty();
                        this.PriceSetupDialogVM = new PricingSetupDialogViewModel(this.CustomerQuote, MessageToken, this);
                        this.PriceSetupDialogVM.LoadLists();
                        PayloadManager.ProspectPayload.ShowEditModeButton = false;
                        IsQuoteEditable = true;
                        this.IsBusy = false;
                    }
                });

            QuoteItemSelectionChanged = new DelegateCommand(
                (param) =>
                {
                    #region Start
                    if (param == null)
                    {
                        this.EnableRemoveItemButton = false;
                    }

                    ObservableCollection<object> items = param as ObservableCollection<object>;

                    if (items.Count > 0)
                    {
                        this.EnableRemoveItemButton = true;
                    }
                    else
                    {
                        this.EnableRemoveItemButton = false;
                    }
                    #endregion
                });

            SaveQuoteDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    #region Save
                    //param expected 
                    // NEW - New Order, Complete button clicked 
                    // OLD - Old Order, Save button clicked 
                    //ToDo - Write code to print the data 
                    if (ViewModelPayload.PayloadManager.ProspectPayload.IsProspect)
                    {
                        this.CustomerQuote.IsNewQuote = ViewModelPayload.PayloadManager.QuotePayload.QuoteId == "NEW" ? true : false;
                        if (this.CustomerQuote.Items.Count <= 0)
                        {
                            var alertMessage = new Helpers.AlertWindow { Header = "Save", Message = Constants.Quote.CustomerQuoteDirtySaveAlertMessage, MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);

                            return;
                        }
                        manager.SaveQuote(this.CustomerQuote);
                        this.IsDirty = false; //data is saved and hence re-setting the value to false

                        #region Update Activity Count
                        string tempStopDate = string.Empty;
                        string stopID = string.Empty;
                        if (PayloadManager.ProspectPayload.Prospect.StopDate.Value.Date > DateTime.Now.Date)
                        {
                            tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                            stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ProspectPayload.Prospect.ProspectID, tempStopDate);
                        }
                        else
                        {
                            tempStopDate = PayloadManager.ProspectPayload.Prospect.StopDate.Value.Date.ToString("yyyy-MM-dd");
                            stopID = PayloadManager.ProspectPayload.Prospect.StopID;
                        }
                        new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                        new CustomerManager().UpdateActivityCount(stopID, false, true);

                        #endregion
                        // Prospect date is taken as today, quote is created only for today date only
                        PayloadManager.ProspectPayload.Prospect = new DashboardViewModel().GetNewProspect(DateTime.Today.Date, PayloadManager.ProspectPayload.Prospect.ProspectID);
                        if (!PayloadManager.ProspectPayload.ShowEditModeButton)
                        {
                            PrintQuoteDlgtCmd.Execute(null);
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ProspectHome, CurrentViewName = ViewModelMappings.View.ProspectQuote, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = ViewModelMappings.TabView.ProspectHome.Quotes };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                            return;
                        }
                        if (IsEnablePickButton)
                        {
                            this.IsDirty = true; //data is saved but picking has not completed yet and hence re-setting the value to true
                        }
                        if (!this.CustomerQuote.IsNewQuote && PayloadManager.ProspectPayload.ShowEditModeButton)
                        {
                            string messagestr = PayloadManager.ProspectPayload.IsProspect ? "Prospect Product Quote saved successfully!" : "Customer Product Quote saved successfully!";
                            var alertMessage = new Helpers.AlertWindow { Header = "Save", Message = messagestr, MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }

                    }
                    else
                    {
                        if (param == null)
                        {
                            return;
                        }

                        if (param.Equals("NEW"))
                        {
                            this.CustomerQuote.IsNewQuote = true;
                        }
                        else if (param.Equals("OLD"))
                        {
                            this.CustomerQuote.IsNewQuote = false;
                        }

                        if (this.CustomerQuote.Items.Count <= 0)
                        {
                            var alertMessage = new Helpers.AlertWindow { Header = "Save", Message = Constants.Quote.CustomerQuoteDirtySaveAlertMessage, MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);

                            return;
                        }

                        manager.SaveQuote(this.CustomerQuote);

                        this.IsDirty = false; //data is saved and hence re-setting the value to false
                        this.CustomerQuote = this.CustomerQuote;

                        if (this.CustomerQuote.IsNewQuote)
                        {

                            PrintQuoteDlgtCmd.Execute(null);

                            this.UpdateActivity();

                            #region Update temp sequencing no.to show activity info on dashboard

                            DateTime todayDate = Convert.ToDateTime(PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd"));
                            ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                            PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                            #endregion

                            var moveToView = new Helpers.NavigateToView
                            {
                                NextViewName = ViewModelMappings.View.CustomerHome,
                                CurrentViewName = ViewModelMappings.View.CustomerQuote,
                                CloseCurrentView = true,
                                ShowAsModal = false,
                                Refresh = true,
                                SelectTab = ViewModelMappings.TabView.CustomerDashboard.Quotes
                            };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                        }

                        if (!this.CustomerQuote.IsNewQuote)
                        {
                            string messagestr = PayloadManager.ProspectPayload.IsProspect ? "Prospect Product Quote saved successfully!" : "Customer Product Quote saved successfully!";
                            var alertMessage = new Helpers.AlertWindow { Header = "Save", Message = messagestr, MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }
                    }


                    #endregion
                }
                );

            RemoveItemDlgtCmd = new DelegateCommand(
                (param) =>
                {
                    this.IsDirty = true;
                    //if selected item count=0 , return 
                    ObservableCollection<object> selectedItems = param as ObservableCollection<object>;

                    if (selectedItems == null || selectedItems.Count == 0)
                    {
                        return;
                    }

                    int selectedItemCount = selectedItems.Count;

                    //add item to list 
                    for (int i = 0; i < selectedItemCount; i++)
                    {
                        CustomerQuoteDetail detailItem = selectedItems[0] as CustomerQuoteDetail;
                        if (detailItem.PickedQty > 0)
                        {
                            PayloadManager.ProspectPayload.IsPickedItemRemoved = true;
                        }

                        Item item = this.RemovedSearchItemList.FirstOrDefault<Item>(o => o.ItemId.Equals(detailItem.ItemId));
                        if (item != null)
                        {
                            this.SearchItemList.Add(item);
                        }

                        this.RemovedSearchItemList.RemoveAll(o => o.ItemId.Equals(detailItem.ItemId));

                        if (this.CustomerQuote.Items.Count > 0)
                        {
                            this.CustomerQuote.Items.Remove(detailItem);
                        }

                    }
                    if (PayloadManager.ProspectPayload.IsPickedItemRemoved)
                    {
                        HasPickingPerformedForQuote = true;
                        IsEnableCompleteButton = false;
                        IsEnablePickButton = true;
                    }

                });

            OpenQuoteSetupDialogDlgCmd = new DelegateCommand(
                (param) =>
                {
                    if (param == null)
                    {
                        return;
                    }

                    if (param.Equals("master"))
                    {
                        this.PriceSetupDialogVM.IsOverrideView = false;
                        this.PriceSetupDialogVM.GetDescription();
                    }
                    else if (param.Equals("override"))
                    {
                        this.PriceSetupDialogVM.IsOverrideView = true;
                        this.PriceSetupDialogVM.GetDescription();
                    }

                    var dialog = new Helpers.DialogWindow { TemplateKey = "QuotePriceDialog", Title = "Quote Pricing Setup", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                });


            TransactionUMChange = new DelegateCommand((param) =>
            {
                //Save button enable disable 
                //AddLoadDetailsSelectedItem.SelectedQtyUOM  QtyUOMClass SelectedQtyUOM
                if (param != null)
                {
                    List<object> parameters = param as List<object>;

                    QtyUOMClass SelectedQtyUOM = (QtyUOMClass)parameters[0];
                    CustomerQuoteDetail obj = (CustomerQuoteDetail)parameters[1];

                    if (string.IsNullOrEmpty(obj.PrimaryUOM))
                    {
                        Item item = this.SearchItemList.FirstOrDefault<Item>(o => o.ItemId.Equals(obj.ItemId));

                        if (item != null)
                        {
                            obj.PrimaryUOM = item.PrimaryUM;
                        }
                    }

                    //Set the transaction UOM 
                    obj.TransactionUOM = SelectedQtyUOM.UMCode;
                    obj.IsVisibleOtherUM = false;
                    obj.IsVisibleTransactionUM = false;


                    if (SelectedQtyUOM.UMCode != obj.PricingUOM)
                    {
                        obj.IsVisibleTransactionUM = true;
                        obj.TransactionUOM = SelectedQtyUOM.UMCode;

                        obj.TransactionUOMPrice = Convert.ToDecimal(OrderManager.GetPriceByUomFactor(obj.ItemId, obj.PricingUOM, obj.TransactionUOM, Convert.ToDecimal(obj.PricingUOMPrice))); ;

                        //Update other UOM 
                        Item item = RemovedSearchItemList.FirstOrDefault<Item>(o => o.ItemId.Equals(obj.ItemId));

                        if (item != null)
                        {
                            for (int i = 0; i < item.QtyUOMCollection.Count; i++)
                            {
                                if (item.QtyUOMCollection[i].UMCode.Equals(Convert.ToString(obj.PricingUOM)) || item.QtyUOMCollection[i].UMCode.Equals(Convert.ToString(obj.TransactionUOM)))
                                {
                                    continue;
                                }

                                obj.OtherUOM = item.QtyUOMCollection[i].UMCode;
                                obj.OtherUOMPrice = Convert.ToDecimal(OrderManager.GetPriceByUomFactor(obj.ItemId, obj.PricingUOM, obj.OtherUOM, Convert.ToDecimal(obj.PricingUOMPrice))); ;
                                obj.IsVisibleOtherUM = true;
                                break;
                            }
                        }
                    }
                    manager.ApplyPricingToTemplates(this.CustomerQuote.Items, CommonNavInfo.UserBranch, !IsProspectCustomer ? CommonNavInfo.Customer.CustomerNo : PayloadManager.ProspectPayload.Prospect.ProspectID, this.CustomerQuote.OverridePriceSetup);

                    this.IsDirty = true;
                }

            });
            SampleUMChange = new DelegateCommand((param) =>
            {
                //Pick button enable disable 
                if (param != null)
                {
                    List<object> parameters = param as List<object>;
                    QtyUOMClass SelectedQtyUOM = (QtyUOMClass)parameters[0];
                    CustomerQuoteDetail obj = (CustomerQuoteDetail)parameters[1];
                    if (SelectedQtyUOM.UMCode != "loose")
                    {
                        //Update Sample Qty
                        obj.SampleQty = obj.AvailableQty == 0 ? 0 : (obj.SampleQty > Convert.ToInt32(obj.AvailableQty / SelectedQtyUOM.UMMultipler) ? Convert.ToInt32(obj.AvailableQty / SelectedQtyUOM.UMMultipler) : obj.SampleQty);
                    }
                    EnableDisableButtons();
                    this.IsDirty = true;
                }

            });

            UpdateAvailableQty = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    CustomerQuoteDetail obj = (CustomerQuoteDetail)param;
                    //UpdateAvailableQtyExecute(obj);
                }
            });

            PickDlgtCmd = new DelegateCommand((param) =>
            {
                IsBusy = true;
                //Save Quotes and navigate to Pick Screen
                this.CustomerQuote.IsNewQuote = ViewModelPayload.PayloadManager.QuotePayload.QuoteId == "NEW" ? true : false;

                manager.SaveQuote(this.CustomerQuote);
                ViewModelPayload.PayloadManager.QuotePayload.QuoteDate = DateTime.Now;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ProspectQuotePick, CurrentViewName = ViewModelMappings.View.ProspectQuote, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });

            UpdateSampleQty = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    CustomerQuoteDetail obj = (CustomerQuoteDetail)param;
                    if (obj.SampleQty == 0)
                    {
                        obj.SelectedQtyUOMSample = new QtyUOMClass();
                        obj.SampleUOM = "";

                    }

                    else if (obj.SelectedQtyUOMSample.UMCode == "")
                    {
                        if (obj.AvailableQty != 0 & obj.SelectedQtyUOMSample.UMCode != "loose")
                        {
                            obj.SelectedQtyUOMSample = obj.QtyUOMCollectionSample.FirstOrDefault(x => x.UMCode == obj.TransactionUOM);
                            obj.SampleUOM = obj.SelectedQtyUOMSample.UMCode;
                        }
                        else
                        {
                            obj.SelectedQtyUOMSample = obj.QtyUOMCollectionSample.FirstOrDefault(x => x.UMCode == "loose");
                            obj.SampleUOM = obj.SelectedQtyUOMSample.UMCode;
                        }
                    }


                    EnableDisableButtons();
                    this.IsDirty = true;
                }

            });

            PrintQuoteDlgtCmd = new DelegateCommand((param) =>
            {
                //
                if (PayloadManager.ProspectPayload.IsProspect && PayloadManager.ProspectPayload.IsPartialPicking)
                {
                    //Alert  for picking
                    string messagestr = Helpers.Constants.Quote.CustomerQuoteBackNavigationMessagePartialPick;
                    var alertMessage = new Helpers.AlertWindow { Header = "Alert", Message = messagestr, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                string confirmationMessage = string.Empty;

                if (this.CustomerQuote.IsPrinted)
                {
                    confirmationMessage = "Do you want to re-print this Quote?";
                }
                else
                {
                    confirmationMessage = "Do you want to print this Quote?";
                }
                ConfirmWindow confirmReadOnlyNavigation = new Helpers.ConfirmWindow { OkButtonContent = "Yes", CancelButtonContent = "No" };
                confirmReadOnlyNavigation.Message = confirmationMessage;
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmReadOnlyNavigation, MessageToken);

                if (confirmReadOnlyNavigation.Confirmed)
                {
                    //Mark IsPrinted in DB for this quote
                    if (!CustomerQuote.IsPrinted)
                    {
                        manager.UpdateIsPrinted(CustomerQuote);
                    }
                    PrintAsync();
                    IsQuoteEditable = false;
                }

            });
            #endregion

            #region Search grid delgate commands

            OpenExpander = new DelegateCommand((param) =>
            {

                switch (Convert.ToInt32(param))
                {
                    case 0:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    case 1:
                        if (!ToggleExpander)
                        {
                            ToggleExpander = true;
                        }
                        break;
                    default:
                        ToggleExpander = false;
                        break;

                }
            });

            // Add item from search to template list
            AddItemToQuote = new DelegateCommand((Items) =>
            {
                #region Add Item
                this.IsDirty = true;

                Managers.ItemManager itemManager = new Managers.ItemManager();
                ObservableCollection<object> selectedItems = Items as ObservableCollection<object>;
                if (selectedItems == null || selectedItems.Count == 0)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Constants.Common.SelectItemToAddAlert, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                int selectionCount = selectedItems.Count;
                int TotalItemsToAdd = selectedItems.Count;

                if (CustomerQuote.Items == null)
                {
                    CustomerQuote.Items = new ObservableCollection<CustomerQuoteDetail>();
                }

                //Enable complete button 
                IsEnableCompleteButton = true;

                for (int i = 0; i < selectedItems.Count; i++)
                {
                    CustomerQuoteDetail quoteDetail = new CustomerQuoteDetail();
                    quoteDetail.QuoteHeaderId = CustomerQuote.QuoteId;
                    quoteDetail.QuoteDetailId = ((this.CustomerQuote.Items.Count == 0 ? 0 : this.CustomerQuote.Items.Max(o => o.QuoteDetailId)) + 1);
                    quoteDetail.ItemNumber = ((Item)selectedItems[i]).ItemNumber;
                    quoteDetail.ItemId = ((Item)selectedItems[i]).ItemId;
                    quoteDetail.ItemDescription = ((Item)selectedItems[i]).ItemDescription;
                    quoteDetail.QuoteQuantity = 1;
                    quoteDetail.UOM = ((Item)selectedItems[i]).UM;
                    quoteDetail.CompetitorName = "";
                    quoteDetail.CompetitorUOM = "";
                    quoteDetail.CompetitorPrice = 0;
                    quoteDetail.AvailableQty = ((Item)selectedItems[i]).AvailableQty;
                    quoteDetail.SelectedQtyUOM = ((Item)selectedItems[i]).SelectedQtyUOM;
                    quoteDetail.QtyUOMCollection = ((Item)selectedItems[i]).QtyUOMCollection;

                    quoteDetail.SelectedQtyUOMSample = new QtyUOMClass();
                    quoteDetail.QtyUOMCollectionSample = ((Item)selectedItems[i]).QtyUOMCollection.Clone();
                    quoteDetail.QtyUOMCollectionSample.Add(new QtyUOMClass() { UMCode = "loose", UMMultipler = 1 });
                    quoteDetail.SampleUOM = "";
                    quoteDetail.SampleQty = 0;

                    quoteDetail.PricingUOM = ((Item)selectedItems[i]).UMPrice;
                    quoteDetail.PricingUOMPrice = Convert.ToDecimal(((Item)selectedItems[i]).UnitPriceByPricingUOM);
                    quoteDetail.PrimaryUOM = ((Item)selectedItems[i]).PrimaryUM;

                    //Unit price logic
                    if (quoteDetail.PricingUOM != quoteDetail.PrimaryUOM)
                    {
                        quoteDetail.IsVisibleTransactionUM = true;
                        quoteDetail.TransactionUOM = quoteDetail.PrimaryUOM;
                    }

                    //Get unit price for transaction UOM
                    quoteDetail.TransactionUOMPrice = 0;
                    quoteDetail.OtherUOMPrice = 0;
                    quoteDetail.PricingUOMPrice = Convert.ToDecimal(((Item)selectedItems[i]).UnitPriceByPricingUOM);


                    #region Set Conversion Factor
                    try
                    {
                        //Set conversion factor
                        List<QtyUOMClass> objRemove = new List<QtyUOMClass>();

                        foreach (QtyUOMClass x in quoteDetail.QtyUOMCollectionSample)
                        {
                            x.UMMultipler = new TemplateManager().SetConversionFactorForItem(quoteDetail.PrimaryUOM, x.UMCode, quoteDetail.ItemId, quoteDetail.ItemNumber);
                            if (x.UMMultipler > quoteDetail.AvailableQty && x.UMCode != "loose")
                            {
                                objRemove.Add(x);
                            }
                        }
                        foreach (var item1 in objRemove)
                        {
                            quoteDetail.QtyUOMCollectionSample.Remove(item1);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Application.Managers][ItemManager][SearchItem][Set Conversion Factor Region][ExceptionStackTrace = " + ex.StackTrace + "]");

                    }

                    #endregion

                    CustomerQuote.Items.Add(quoteDetail);

                    //Add items to items in process
                    this.RemovedSearchItemList.Add(((Item)selectedItems[i]));
                }

                foreach (Item item in RemovedSearchItemList)
                {
                    SearchItemList.Remove((Item)item);
                }

                this.ToggleUnitPriceVisibility();

                if (PayloadManager.ProspectPayload.IsProspect)
                {
                    manager.ApplyPricingToTemplates(this.CustomerQuote.Items, CommonNavInfo.UserBranch, PayloadManager.ProspectPayload.Prospect.ProspectID, this.CustomerQuote.OverridePriceSetup);

                }
                else
                {
                    manager.ApplyPricingToTemplates(this.CustomerQuote.Items, CommonNavInfo.UserBranch, CommonNavInfo.Customer.CustomerNo, this.CustomerQuote.OverridePriceSetup);

                }
                if (!IsEnablePickButton)
                {
                    EnableDisableButtons();
                }
                #endregion
            });


            SearchResultTextChange = new DelegateCommand((param) =>
            {
                //Item itm = param as Item;

                SearchItemsCount = Convert.ToInt32(param);
                OnPropertyChanged("SearchItemsCount");
                OnPropertyChanged("SearchResultText");
            });

            ClearSearchText = new DelegateCommand((param) =>
            {
                SearchText = "";
                SearchResultText = "";
                SearchItems(SearchText.ToString(), true);
            });

            SearchItemOnType = new DelegateCommand((searchTerm) =>
            {
                bool flag = searchTerm.ToString().Length == 0 ? true : false;
                SearchItems(searchTerm.ToString(), flag);
            });

            SearchItem = new DelegateCommand((searchTerm) =>
            {
                SearchItems(searchTerm.ToString(), true);
            });
            #endregion
        }

        private void ToggleUnitPriceVisibility()
        {
            foreach (CustomerQuoteDetail detailItem in this.CustomerQuote.Items)
            {
                //initially all item wilnot be visibile, if they adhere to the rule, then they will be made visible 
                detailItem.IsVisibleTransactionUM = false;
                detailItem.IsVisibleOtherUM = false;

                //Update other UOM 
                Item item = RemovedSearchItemList.FirstOrDefault<Item>(o => o.ItemId.Equals(detailItem.ItemId));

                //if trnsaction UOM is blank, then set the first UOM of the drop down
                if (string.IsNullOrEmpty(detailItem.TransactionUOM))
                {
                    if (item != null)
                    {
                        detailItem.TransactionUOM = item.QtyUOMCollection[0].UMCode;
                    }
                }

                if (detailItem.TransactionUOM == detailItem.PricingUOM)
                {
                    continue;
                }

                detailItem.IsVisibleTransactionUM = true;

                if (item != null)
                {
                    for (int i = 0; i < item.QtyUOMCollection.Count; i++)
                    {
                        if (item.QtyUOMCollection[i].UMCode.Equals(Convert.ToString(detailItem.PricingUOM)) || item.QtyUOMCollection[i].UMCode.Equals(Convert.ToString(detailItem.TransactionUOM)))
                        {
                            continue;
                        }

                        detailItem.OtherUOM = item.QtyUOMCollection[i].UMCode;
                        detailItem.IsVisibleOtherUM = true;
                        break;
                    }
                }
            }
        }

        private void EnableDisableButtons()
        {
            if (!PayloadManager.ProspectPayload.IsProspect)
            {
                return;
            }
            if (PayloadManager.ProspectPayload.IsPickedItemRemoved)
            {
                IsEnableCompleteButton = false;
                IsEnablePickButton = true;
                this.IsDirty = true;
                return;
            }
            if (CustomerQuote.Items.Count > 0)
            {
                foreach (CustomerQuoteDetail item in CustomerQuote.Items)
                {
                    if (item.SampleQty > 0)
                    {
                        if (item.SelectedQtyUOMSample.UMCode != "loose" && !HasPickingPerformedForQuote)
                        {
                            IsEnableCompleteButton = false;
                            IsEnablePickButton = true;
                            break;
                        }
                        else if (item.SelectedQtyUOMSample.UMCode == "loose" && !HasPickingPerformedForQuote)
                        {
                            IsEnableCompleteButton = true;
                            IsEnablePickButton = false;
                        }
                        else if (item.SelectedQtyUOMSample.UMCode != "loose" && HasPickingPerformedForQuote)
                        {
                            if (item.SampleQty != item.PickedQty)
                            {
                                IsEnableCompleteButton = false;
                                IsEnablePickButton = true;
                                break;
                            }
                            else if (item.SelectedQtyUOMSample.UMCode != item.PickedUOM)
                            {
                                IsEnableCompleteButton = false;
                                IsEnablePickButton = true;
                                break;
                            }
                            else
                            {
                                IsEnableCompleteButton = true;
                                IsEnablePickButton = false;
                            }
                        }
                        else if (item.SelectedQtyUOMSample.UMCode == "loose" && HasPickingPerformedForQuote)
                        {
                            if (item.PickedQty > 0)
                            {
                                IsEnableCompleteButton = false;
                                IsEnablePickButton = true;
                                break;
                            }
                            else
                            {
                                IsEnableCompleteButton = true;
                                IsEnablePickButton = false;
                            }
                        }
                    }
                    else if (item.SampleQty == 0)
                    {
                        if (item.PickedQty > 0)
                        {
                            IsEnableCompleteButton = false;
                            IsEnablePickButton = true;
                            break;
                        }
                        else
                        {
                            IsEnableCompleteButton = true;
                            IsEnablePickButton = false;
                        }
                    }

                }
            }

            if (IsEnablePickButton)
            {
                this.IsDirty = true;
            }

        }

        private void SetAvailableQty()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteViewModel][AsyncStart:SetAvailableQty]");

            try
            {
                foreach (CustomerQuoteDetail item in CustomerQuote.Items)
                {
                    item.AvailableQty = new CustomerQuoteManager().GetAvailableQtyForItem(item.ItemNumber);
                    Logger.Info("Item Number = " + item.ItemNumber + "\tQuantity=" + item.AvailableQty.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerQuoteViewModel][SetAvailableQty][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteViewModel][AsyncStart:SetAvailableQty]");
        }
        async void PrintAsync()
        {
            await Task.Run(() =>
            {
                new ReportViewModels.CustomerQuoteReport(CustomerQuote);
            });
        }
        /// <summary>
        /// Retrieves list of items for loading into search 
        /// </summary>
        private async void GetSearchItems()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteViewModel][AsyncStart:GetSearchItems]");
            await Task.Run(() =>
            {
                Managers.ItemManager itemManager = new Managers.ItemManager();

                try
                {
                    IsSearching = true;
                    SearchResultText = string.Empty;
                    SearchItemList = itemManager.SearchItem(string.Empty, CommonNavInfo.Customer == null ? "''" : CommonNavInfo.Customer.CustomerNo);

                    this.RemoveItemsFromSearchList();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerQuoteViewModel][GetSearchItems][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                finally
                {
                    IsSearching = false;
                    Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerQuoteViewModel][AsyncStart:GetSearchItems]");
                }
            });


        }

        /// <summary>
        /// Updates complete button enable/disable flag
        /// </summary>
        /// <param name="sender">Observable collection</param>
        /// <param name="e">Collection changed event argument</param>
        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.CustomerQuote.IsNewQuote)
            {
                if (PayloadManager.ProspectPayload.IsProspect)
                {
                    this.IsEnableCompleteButton = IsEnablePickButton ? false : (((ObservableCollection<CustomerQuoteDetail>)(sender)).Count > 0);

                }
                else
                {
                    this.IsEnableCompleteButton = (((ObservableCollection<CustomerQuoteDetail>)(sender)).Count > 0);

                }
            }
        }

        private void UpdateActivity()
        {
            try
            {
                #region Update Activity Count

                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.OrderPayload.Customer.StopID;
                }
                new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new CustomerManager().UpdateActivityCount(stopID, false, true);
                PayloadManager.OrderPayload.Customer.SaleStatus = "";
                PayloadManager.OrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                //PayloadManager.OrderPayload.Customer.PendingActivity += 1;
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                //PayloadManager.OrderPayload.Customer.HasActivity = true; 

                #endregion
            }
            catch (Exception)
            {
                //
            }

        }

        #endregion

        #region Overrided methods

        public override bool ConfirmSave()
        {
            this.IsDirty = false;
            CanNavigate = true;

            if (!this.CustomerQuote.IsNewQuote)
            {
                //Call the save procedure oly if it has at least one item
                if (CustomerQuote.Items.Count > 0)
                {
                    manager.SaveQuote(this.CustomerQuote);
                }
            }
            return base.ConfirmSave();
        }


        public override bool ConfirmCancel()
        {
            if (!PayloadManager.ProspectPayload.IsProspect)
            {
                if (!this.CustomerQuote.IsNewQuote)
                {
                    //If quote has any items, then navigate, else stik to the same screen 
                    //this is done to avoid back navigation from qute screen on cancel click of edit confirmation box 
                    if (CustomerQuote.Items.Count > 0)
                    {
                        IsDirty = false;
                        CanNavigate = true;
                    }
                    else
                    {
                        CanNavigate = false;
                    }
                }

                return base.ConfirmCancel();
            }
            else
            {
                if (!this.CustomerQuote.IsNewQuote)
                {
                    if (!PayloadManager.ProspectPayload.ShowEditModeButton)
                    {
                        return base.ConfirmCancel();
                    }
                    //If quote has any items, then navigate, else stik to the same screen 
                    //this is done to avoid back navigation from qute screen on cancel click of edit confirmation box 
                    if (CustomerQuote.Items.Count > 0)
                    {
                        IsDirty = false;
                        CanNavigate = true;
                    }
                    else
                    {
                        CanNavigate = false;
                    }
                }

                return base.ConfirmCancel();
            }


        }
        #endregion
    }
}