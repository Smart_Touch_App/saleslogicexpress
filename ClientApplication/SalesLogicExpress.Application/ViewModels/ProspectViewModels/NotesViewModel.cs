﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain.Prospect_Models;
using SalesLogicExpress.Domain;
using System.Media;
using Entities = SalesLogicExpress.Domain;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Globalization;
using SalesLogicExpress.Application.Managers.ProspectManagers;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;
using SalesLogicExpress.Application.Managers;


namespace SalesLogicExpress.Application.ViewModels.ProspectViewModels
{
    public class NotesViewModel : BaseViewModel, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ProspectViewModels");
        public event EventHandler<ProspectNotesVMArgs> ViewModelUpdated;
        string _NoteIDProspect, _NoteDetailsProspect, _DateTime;
        public static string ProspectNoVM = string.Empty;
        bool IsEmpty = false;
        private bool _SelectAllCheck = false;
        private bool _ToggleButton = false;


        Prospect prospect;
        public Prospect Prospect
        {
            get
            {
                return prospect;
            }
            set
            {


                prospect = value;
                OnPropertyChanged("Prospect");
            }
        }
        protected virtual void OnViewModelStateChanged(ProspectNotesVMArgs e)
        {
            EventHandler<ProspectNotesVMArgs> handler = ViewModelUpdated;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public bool ToggleButton
        {
            get
            {
                return _ToggleButton;
            }
            set
            {
                _ToggleButton = value;
                OnPropertyChanged("ToggleButton");
            }
        }
        public string NoteDetailsProspect
        {
            get
            {
                return _NoteDetailsProspect;
            }
            set
            {
                _NoteDetailsProspect = value;
                if (string.IsNullOrEmpty(_NoteDetailsProspect.Trim()))
                    IsEmpty = true;

                else
                {
                    IsEmpty = false;
                    ToggleButton = true;
                }
                OnPropertyChanged("NoteDetailsProspect");
            }
        }
        public string NoteIDProspect
        {
            get
            {
                return _NoteIDProspect;
            }
            set
            {
                _NoteIDProspect = value;
                OnPropertyChanged("NoteIDProspect");
            }
        }
        public bool SelectAllCheck
        {
            get
            {
                return _SelectAllCheck;
            }
            set
            {
                _SelectAllCheck = value;
                OnPropertyChanged("SelectAllCheck");
            }
        }

        #region Properties
        public Guid MessageToken { get; set; }
        public DelegateCommand SaveNewNoteProspect { get; set; }
        private ObservableCollection<ProspectNoteModel> _ProspectNoteList;
        public ObservableCollection<ProspectNoteModel> ProspectNoteList
        {
            get
            {
                return _ProspectNoteList;
            }
            set
            {
                _ProspectNoteList = value;
                OnPropertyChanged("ProspectNoteList");
            }
        }
        #endregion

        #region OnPropertyChanged

        #region Bool

        #endregion

        #region Int

        #endregion

        #region string


        #endregion

        #region Date

        #endregion

        #region Collection


        #endregion

        #endregion

        #region Commands Declaretion
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand AddNewNoteProspect { get; set; }
        public DelegateCommand SelectAllNotesProspect { get; set; }
        public DelegateCommand SetDefaultNoteProspect { get; set; }
        public DelegateCommand SelectNoteProspect { get; set; }
        public DelegateCommand DeleteNoteProspect { get; set; }
        #endregion

        void PropectNoteList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("ProspectNoteList");
        }
        #region Command Defination
        public void InitialiseCommands()
        {
            ClearSearchText = new DelegateCommand((param) =>
            {

            });

            #region AddNewNoteProspect
            AddNewNoteProspect = new DelegateCommand((param) =>
            {
                CustomerDashboard custDash = new CustomerDashboard();
                bool IsPreTripDone = custDash.GetPreTripDetailsForDate();
                if (IsPreTripDone)
                {
                    NoteDetailsProspect = string.Empty;
                    IsEmpty = false;
                    var parentVM = (ProspectsHome)this.ParentView;
                    if (!parentVM.DashboardVM.VerifyStopInfo(parentVM.DashboardVM.Prospect))
                        return;
                    var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewNoteProspect", Title = "Add New Note", Payload = this };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                }
                else
                {
                    var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.Common.PreTripErrorMsg, MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    if (alertMessage.Closed) return;
                    
                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteHome, CurrentViewName = ViewModelMappings.View.ProspectHome, Refresh = true, CloseCurrentView = true, ShowAsModal = false };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                }
            });
            #endregion

            #region SaveNewNoteProspect
            SaveNewNoteProspect = new DelegateCommand((param) =>
            {
                ProspectNoteModel noteProspect = new ProspectNoteModel();
                try
                {
                    noteProspect.ProspectNoteDetails = NoteDetailsProspect.Trim();
                    noteProspect.ProspectID = ProspectNoVM;
                    if (ProspectNoteList.Count == 0)
                    {
                        noteProspect.IsDefault = true;
                    }
                    NotesManager noteManager = new NotesManager();
                    noteManager.AddNewProspectNote(noteProspect);
                    GetProspectNotes();



                    #region Update Activity Count
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.ProspectPayload.Prospect.StopDate.Value.Date > DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ProspectPayload.Prospect.ProspectID, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.ProspectPayload.Prospect.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.ProspectPayload.Prospect.StopID;
                    }
                    new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                    new CustomerManager().UpdateActivityCount(stopID, false, true);

                    #endregion

                    var parentVM = (ProspectsHome)this.ParentView;

                    parentVM.ProspectActivityVM = new ProspectActivityViewModel(MessageToken, parentVM);
                    // Prospect date is taken as today, notes is created only for today date
                    PayloadManager.ProspectPayload.Prospect = new DashboardViewModel(MessageToken, parentVM).GetNewProspect(DateTime.Today.Date);
                    parentVM.DashboardVM = new DashboardViewModel(MessageToken, parentVM);
                    ToggleButton = false;
                }
                catch (Exception ex)
                {

                }
            });
            #endregion

            #region SetDefaultNoteProspect
            SetDefaultNoteProspect = new DelegateCommand((param) =>
            {
                ProspectNoteModel selectedNote = param as ProspectNoteModel;
                foreach (ProspectNoteModel noteProspect in ProspectNoteList)
                {
                    noteProspect.IsDefault = selectedNote.ProspectNoteID == noteProspect.ProspectNoteID ? true : false;
                }
                selectedNote.IsDefault = true;
                new NotesManager().SetDefaultNote(selectedNote);
                ProspectNotesVMArgs args = new ProspectNotesVMArgs();
                args.Note = selectedNote;
                OnViewModelStateChanged(args);
            });
            #endregion

            #region SelectAllNotesProspect
            SelectAllNotesProspect = new DelegateCommand((SelectAll) =>
            {
                bool select = (bool)SelectAll;
                foreach (ProspectNoteModel note in ProspectNoteList)
                {
                    note.IsSelected = select;
                }
            });
            #endregion

            #region SelectNote
            SelectNoteProspect = new DelegateCommand((Selected) =>
            {
                bool select = (bool)Selected;
                if (!select)
                {
                    SelectAllCheck = select;
                }
                else
                {
                    bool isAllSelected = true;
                    foreach (ProspectNoteModel note in ProspectNoteList)
                    {
                        if (!note.IsSelected)
                        {
                            isAllSelected = false;
                        }
                    }
                    SelectAllCheck = isAllSelected == true ? true : false;
                }
            });
            #endregion

            #region DeleteNote
            DeleteNoteProspect = new DelegateCommand((param) =>
            {
                List<ProspectNoteModel> noteList = ProspectNoteList.Where(note => note.IsSelected == true).ToList<ProspectNoteModel>();

                int selectedNotes = noteList.Count();

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Notes.DeleteNoteConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                switch (selectedNotes)
                {
                    case 0:
                        var alertMessage = new Helpers.AlertWindow { Message = "Please select note to delete!", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                        break;
                    case 1:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Notes.DeleteNoteConfirmation, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);

                        break;
                    default:
                        confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Notes.DeleteNoteConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                        Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                        break;
                }

                if (!confirmMessage.Confirmed) return;

                NotesManager noteManager = new NotesManager();
                ProspectNoteModel prospectNote = new ProspectNoteModel();
                prospectNote.ProspectID = ProspectNoVM;

                for (int index = 0; index < selectedNotes; index++)
                {
                    prospectNote.ProspectNoteID = noteList[index].ProspectNoteID;
                    noteManager.DeleteSelectedNote(prospectNote);
                    ProspectNoteList.Remove(ProspectNoteList.ElementAt(ProspectNoteList.IndexOf(ProspectNoteList.First(note => note.ProspectNoteID == prospectNote.ProspectNoteID))));
                }
                bool isDefaultNoteAvailable = Convert.ToBoolean(ProspectNoteList.Count(note => note.IsDefault == true));
                if (!isDefaultNoteAvailable && ProspectNoteList.Count > 0)
                {
                    ProspectNoteModel latestNote = ProspectNoteList.OrderByDescending(t => System.DateTime.ParseExact(t.DateTime, "MM/dd/yyyy hh:mmtt", System.Globalization.CultureInfo.InvariantCulture)).First();
                    ProspectNoteList.ElementAt(ProspectNoteList.IndexOf(latestNote)).IsDefault = true;
                    new NotesManager().SetDefaultNote(latestNote);
                    ProspectNotesVMArgs args = new ProspectNotesVMArgs();
                    args.Note = latestNote;
                    OnViewModelStateChanged(args);
                }
                if (ProspectNoteList.Count == 0)
                {
                    ProspectNotesVMArgs args = new ProspectNotesVMArgs();
                    args.Note = null;
                    OnViewModelStateChanged(args);
                }
                SelectAllCheck = false;
            });
            #endregion

        }
        #endregion

        // Initialise all the commands, then invoke the getProspect() method to display the existing notes.
        #region Constructor NotesViewModel()
        public NotesViewModel(Guid token, BaseViewModel Parent)
        {
            try
            {
                ProspectNoVM = PayloadManager.ProspectPayload.Prospect.ProspectID;
                InitialiseCommands();
                MessageToken = token;
                GetProspectNotes();
                this.ParentView = Parent;
            }
            catch (Exception ex)
            {
                Logger.Error("NotesViewModel error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels.ProspectViewModels][NotesViewModel][End:NotesViewModel]\t" + (DateTime.Now) + "");

        }
        #endregion

        async void GetProspectNotes()
        {
            await Task.Run(() =>
            {
                NotesManager noteManager = new NotesManager();
                ProspectNoteList = noteManager.GetProspectNotes(ProspectNoVM);
                if (ProspectNoteList.Count == 1)
                {
                    ProspectNoteList[0].IsDefault = true;
                }
                ProspectNotesVMArgs args = new ProspectNotesVMArgs();
                args.Note = ProspectNoteList.FirstOrDefault(addednote => addednote.IsDefault == true);
                OnViewModelStateChanged(args);
                SelectAllCheck = false;
                Prospect = PayloadManager.ProspectPayload.Prospect;
            });
        }

        public string Error
        {
            get
            {
                return "";
            }
        }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "NoteDetailsProspect" && IsEmpty)
                {
                    // result = Helpers.Constants.Common.NoteValidationMsg;
                    ToggleButton = false;
                }
                return result;
            }
        }
    }
    public class ProspectNotesVMArgs : EventArgs
    {
        public ProspectNoteModel Note { get; set; }

    }
}
