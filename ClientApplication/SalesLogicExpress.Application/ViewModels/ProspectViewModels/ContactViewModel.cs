﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using Entities = SalesLogicExpress.Domain;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Globalization;
using SalesLogicExpress.Application.Managers;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;


namespace SalesLogicExpress.Application.ViewModels.ProspectViewModels
{
    public class ContactViewModel : BaseViewModel
    {
        #region Properties & Variables
        public Guid MessageToken { get; set; }

        #endregion

        #region OnPropertyChanged

        #region Bool

      
        #endregion

        #region Int

        #endregion

        #region string


        #endregion

        #region Date

        #endregion

        #region Collection


        #endregion

        #endregion

        #region Commands Declaretion
        public DelegateCommand ClearSearchText { get; set; }


        #endregion

        #region Command Defination

        public void InitialiseCommands()
        {
            ClearSearchText = new DelegateCommand((param) =>
            {

            });

        }

        #endregion


        public ContactViewModel(Guid token, BaseViewModel Parent)
        {
            MessageToken = token;
            this.ParentView = Parent;

        }
    }
}
