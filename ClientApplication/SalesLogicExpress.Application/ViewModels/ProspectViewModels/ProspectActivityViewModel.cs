﻿using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ViewModels.ProspectViewModels
{
    public class ProspectActivityViewModel : BaseViewModel
    {
        Prospect prospect;
        public Prospect Prospect
        {
            get
            {
                return prospect;
            }
            set
            {


                prospect = value;
                OnPropertyChanged("Prospect");
            }
        }
        #region Prop's'
        private Guid MessageToken;
        private ProspectsHome ProspectsHome;
        #region Binded Prop's
        CustomerActivity _ProspectActivities = null;
        public CustomerActivity ProspectActivities
        {
            get
            {
                return _ProspectActivities;
            }
            set
            {
                _ProspectActivities = value;
                OnPropertyChanged("ProspectActivities");
            }
        }
        #endregion
        #endregion

        public ProspectActivityViewModel(Guid MessageToken, ProspectsHome ProspectsHome)
        {
            // TODO: Complete member initialization
            this.MessageToken = MessageToken;
            this.ProspectsHome = ProspectsHome;

            ProspectActivities = new CustomerActivity(this.ProspectsHome.Prospect.ProspectID);
            ProspectActivities.MessageToken = this.MessageToken;
            Prospect = PayloadManager.ProspectPayload.Prospect;
        }

        public ProspectActivityViewModel()
        {
            // TODO: Complete member initialization
        }
    }
}
