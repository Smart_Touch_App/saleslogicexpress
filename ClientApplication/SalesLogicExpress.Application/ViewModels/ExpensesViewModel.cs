﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using Telerik.Windows.Controls;
using System.Windows.Threading;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using System.ComponentModel;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ExpensesViewModel : BaseViewModel, IDataErrorInfo
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ExpensesViewModel");

        #region Properties
        private Guid _MessageToken = new System.Guid();
        public Guid MessageToken
        {

            get { return _MessageToken; }
            set
            {
                _MessageToken = value;
                OnPropertyChanged("MessageToken");
            }
        }

        private string routeID = string.Empty;
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; OnPropertyChanged("RouteID"); }
        }

        private string routeName;
        public string RouteName
        {
            get { return routeName; }
            set { routeName = value; OnPropertyChanged("RouteName"); }
        }
        private string route;
        public string Route
        {
            get { return route; }
            set { route = value; OnPropertyChanged("Route"); }
        }

        private string userRoute;
        public string UserRoute
        {
            get { return userRoute; }
            set { userRoute = value; OnPropertyChanged("UserRoute"); }
        }

        private Expenses expensesObject = new Domain.Expenses();
        public Expenses ExpensesObject
        {
            get { return expensesObject; }
            set { expensesObject = value; OnPropertyChanged("ExpensesObject"); }
        }

        private ObservableCollection<Expenses> expensesList = new ObservableCollection<Domain.Expenses>();

        public ObservableCollection<Expenses> ExpensesList
        {
            get { return expensesList; }
            set { expensesList = value; OnPropertyChanged("ExpensesList"); }
        }

        private ObservableCollection<ReasonCode> reasonCode = new ObservableCollection<ReasonCode>();
        public ObservableCollection<ReasonCode> ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; OnPropertyChanged("ReasonCode"); }
        }

        private ObservableCollection<ExpenseCategory> expenseCategoryList = new ObservableCollection<ExpenseCategory>();

        public ObservableCollection<ExpenseCategory> ExpenseCategoryList
        {
            get { return expenseCategoryList; }
            set { expenseCategoryList = value; OnPropertyChanged("ExpenseCategoryList"); }
        }

        private string cashOnHand = string.Empty;

        public string CashOnHand
        {
            get { return cashOnHand; }
            set { cashOnHand = value; OnPropertyChanged("CashOnHand"); }
        }
        private string amount = string.Empty;
        public string Amount
        {
            get { return amount; }
            set 
            { 
                amount = value;
                IsValidAmount = false;
                if (!string.IsNullOrEmpty(Amount) && (Convert.ToDecimal(0.00) < Convert.ToDecimal(Amount)) && Convert.ToDecimal(Amount) <= Convert.ToDecimal(CashOnHand))
                {
                    IsValidAmount = true;
                    if (!string.IsNullOrEmpty(Explanation.Trim()) && SelectedCategory > 0)
                    {
                        ToggleAddButton = true;
                    }
                    else ToggleAddButton = false;
                }
                else ToggleAddButton = false;
                OnPropertyChanged("Amount"); 
            }
        }

        private string explanation = string.Empty;
        public string Explanation
        {
            get { return explanation; }
            set 
            { 
                explanation = value;
                if (!string.IsNullOrEmpty(explanation.Trim()))
                {
                    if (!string.IsNullOrEmpty(Amount) && IsValidAmount && SelectedCategory > 0)
                    {
                        ToggleAddButton = true;
                    }
                    else ToggleAddButton = false;
                }
                else ToggleAddButton = false;
                OnPropertyChanged("Explanation"); 
            }
        }

        private int selectedCategory;
        public int SelectedCategory
        {
            get { return selectedCategory; }
            set 
            { 
                selectedCategory = value;
                if (selectedCategory > 0)
                {
                    if (!string.IsNullOrEmpty(Amount) && IsValidAmount && !string.IsNullOrEmpty(Explanation.Trim()))
                    {
                        ToggleAddButton = true;
                    }
                    else ToggleAddButton = false;
                }
                else ToggleAddButton = false;
                OnPropertyChanged("SelectedCategory"); 
            }
        }

        public bool ValidateCategory()
        {
            
           
            return ToggleAddButton;
        }
        
        private int selectedReasonCode = -1;
        public int SelectedReasonCode
        {
            get { return selectedReasonCode; }
            set
            {
                selectedReasonCode = value;
                if (selectedReasonCode > 0)
                {
                    ToggleReasonCodeButton = true;
                }
                else ToggleReasonCodeButton = false;
                OnPropertyChanged("SelectedReasonCode");
            }
        }

        private bool isEditEnabled  = false;
        public bool IsEditEnabled
        {
            get { return isEditEnabled; }
            set { isEditEnabled = value; OnPropertyChanged("IsEditEnabled"); }
        }

        private bool isVoidEnabled = false;
        public bool IsVoidEnabled
        {
            get { return isVoidEnabled; }
            set { isVoidEnabled = value; OnPropertyChanged("IsVoidEnabled"); }
        }

        private bool isAddEnabled = false;
        public bool IsAddEnabled
        {
            get { return isAddEnabled; }
            set { isAddEnabled = value; OnPropertyChanged("IsAddEnabled"); }
        }

        private bool toggleReasonCodeButton = false;
        public bool ToggleReasonCodeButton
        {
            get { return toggleReasonCodeButton; }
            set { toggleReasonCodeButton = value; OnPropertyChanged("ToggleReasonCodeButton"); }
        }

        private bool toggleAddButton;
        public bool ToggleAddButton
        {
            get { return toggleAddButton; }
            set { toggleAddButton = value; OnPropertyChanged("ToggleAddButton"); }
        }

        bool IsValidAmount = true;
        bool isMoveDialogOpened;
        public bool IsMoveDialogOpened
        {
            get
            {
                return isMoveDialogOpened;
            }
            set
            {


                isMoveDialogOpened = value;
                OnPropertyChanged("IsMoveDialogOpened");
            }
        }

        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }
        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "Amount" && !IsValidAmount)
                {
                    result = Helpers.Constants.Common.ExpenseAmountErrorMsg;
                }
                return result;
            }
        }
        #endregion

        #region DelegateCommands
        public DelegateCommand OpenEditDialogForExpense { get; set; }
        public DelegateCommand OpenVoidDialogForExpense { get; set; }
        public DelegateCommand OpenAddDialogForExpense { get; set; }
        public DelegateCommand SelectionItemInvoke { get; set; }
        public DelegateCommand VoidExpenses { get; set; }
        public DelegateCommand SaveNewExpense { get; set; }
        public DelegateCommand SaveEditedExpense { get; set; }
        #endregion

        #region Constructor
        public ExpensesViewModel()
        {
            InitializeCommands();
            LoadData();
        }
         
        #endregion

        #region Methods

        ExpensesManager expenseManager = new ExpensesManager();
        public void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][Start:InitializeCommands]");

            #region dialog box for add expense
            OpenAddDialogForExpense = new DelegateCommand((param) =>
            {
                ExpensesObject.ExpenseAmount = string.Empty;
                ExpensesObject.Explanation = string.Empty;
                ExpenseCategoryList = expenseManager.GetCategoryForExpenses();
                SelectedCategory = -1;
                Amount = string.Empty;
                Explanation = string.Empty;
                ToggleAddButton = false;
                IsValidAmount = true;
                IsMoveDialogOpened = true;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "AddExpenseDialog", Title = "Add New Expense" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
            });
            #endregion

            #region dialog box for void expense
            OpenVoidDialogForExpense = new DelegateCommand((param) =>
            {
                ReasonCode = expenseManager.GetVoidReasonCode();
                SelectedReasonCode = -1;
                ToggleReasonCodeButton = false;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "VoidExpenseDialog", Title = "Void Expense" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
            });
            #endregion

            #region dialog box for edit expense
            OpenEditDialogForExpense = new DelegateCommand((param) =>
            {
                CashOnHand = (Convert.ToDecimal(CashOnHand) + Convert.ToDecimal(ExpensesObject.ExpenseAmount)).ToString("F2");
                Explanation = string.IsNullOrEmpty(ExpensesObject.Explanation) ? "" : ExpensesObject.Explanation.Trim();
                Amount = string.IsNullOrEmpty(ExpensesObject.ExpenseAmount) ? "0.00" : Convert.ToDecimal(ExpensesObject.ExpenseAmount).ToString("F2").Trim();
                ExpenseCategoryList = expenseManager.GetCategoryForExpenses();
                SelectedCategory = Convert.ToInt32(ExpensesObject.CategotyID);
                ToggleAddButton = false;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "EditExpenseDialog", Title = "Edit Expense" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);

                if (dialogWindow.Cancelled)
                {
                    CashOnHand = (Convert.ToDecimal(CashOnHand) - Convert.ToDecimal(ExpensesObject.ExpenseAmount)).ToString("F2");
                }
            });
            #endregion

            #region Void Expenses
            VoidExpenses = new DelegateCommand((param) =>
            {
                try
                {
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    ExpensesObject.VoidReasonID = SelectedReasonCode.ToString();
                    string statusID = expenseManager.UpdateExpenses(ExpensesObject, false).ToString();

                    ExpensesObject.StatusID = string.IsNullOrEmpty(statusID.Trim()) ? ExpensesObject.StatusID : statusID.Trim();

                    ExpensesObject.Status = "VOID";
                    ExpensesObject.StatusCode = StatusTypesEnum.VOID.ToString();
                    LogActivityForExpense(ActivityKey.VoidExpense);
                    CashOnHand = (Convert.ToDecimal(CashOnHand) + Convert.ToDecimal(ExpensesObject.ExpenseAmount)).ToString("F2").Trim();
                    IsEditEnabled = IsVoidEnabled = false;
                    GetExpenseList();

                }
                catch (System.Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][VoidExpenses][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                    throw;
                }
            });
            #endregion

            #region Save new expense
            SaveNewExpense = new DelegateCommand((param) =>
            {
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                try
                {

                    ExpensesObject.Explanation = string.IsNullOrEmpty(Explanation) ? "" : Explanation.Trim();
                    ExpensesObject.ExpenseAmount = string.IsNullOrEmpty(Amount) ? "0.00" : (Convert.ToDecimal(Amount)).ToString("F2");
                    ExpensesObject.CategotyID = SelectedCategory.ToString();
                    foreach (ExpenseCategory item in ExpenseCategoryList)
                    {
                        if (item.CategoryID == SelectedCategory)
                        {
                            ExpensesObject.ExpenseCategory = item.Description;
                            break;
                        }
                    }

                    ExpensesObject.ExpenseDate = DateTime.Now;
                    ExpensesObject.VoidReasonID = string.Empty;
                    Expenses obj = new Domain.Expenses();
                    obj = expenseManager.AddExpenses(ExpensesObject);
                    if (obj != null)
                    {
                        ExpensesObject.StatusID = obj.StatusID;
                        ExpensesObject.ExpenseID = obj.ExpenseID;
                    }
                    ExpensesObject.Status = "UNSETTLED";
                    ExpensesObject.StatusCode = StatusTypesEnum.USTLD.ToString(); 
                    LogActivityForExpense(ActivityKey.CreateExpense);
                    IsMoveDialogOpened = false;

                    GetExpenseList();

                    CashOnHand = (Convert.ToDecimal(CashOnHand) - Convert.ToDecimal(ExpensesObject.ExpenseAmount)).ToString("F2");
                }
                catch (System.Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][SaveNewExpense][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                    throw;
                }
            });
            #endregion

            #region Save edited expense
            SaveEditedExpense = new DelegateCommand((param) =>
            {
                try
                {

                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    ExpensesObject.ExpenseAmount = string.IsNullOrEmpty(Amount.Trim()) ? "" : (Convert.ToDecimal(Amount)).ToString("F2").Trim();
                    ExpensesObject.Explanation = string.IsNullOrEmpty(Explanation.Trim()) ? "" : Explanation.Trim();
                    ExpensesObject.CategotyID = SelectedCategory.ToString();

                    expenseManager.UpdateExpenses(ExpensesObject, true);
                    CashOnHand = (Convert.ToDecimal(CashOnHand) - Convert.ToDecimal(ExpensesObject.ExpenseAmount)).ToString("F2");
                    LogActivityForExpense(ActivityKey.EditExpense);
                    IsEditEnabled = IsVoidEnabled = false;

                    GetExpenseList();
                }
                catch (System.Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][SaveEditedExpense][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                    throw;
                }
            });
            #endregion

            #region On Item selection
            SelectionItemInvoke = new DelegateCommand((param) =>
            {
                ExpensesObject = param as Expenses;

                if (ExpensesObject.Status == "UNSETTLED")
                {
                    IsEditEnabled = true; IsVoidEnabled = true;
                }
                else if (ExpensesObject.Status == "VOID" || ExpensesObject.Status == "SETTLED" || ExpensesObject.Status == "VOIDED / SETTLED")
                {
                    IsEditEnabled = false; IsVoidEnabled = false;
                }
                if (ExpensesObject.Status == "UNSETTLED" && ExpensesObject.IsMoneyOrderCharge)
                {
                    IsEditEnabled = false; IsVoidEnabled = false;
                }
            });
            #endregion

            Logger.Info("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][End:InitializeCommands]");

        }

        public void GetExpenseList()
        {
            ExpensesList = new ObservableCollection<Domain.Expenses>();
            ObservableCollection<Expenses> exp = expenseManager.GetExpensesList();
            //ExpensesList = exp;
            foreach (Expenses item in exp)
            {
                if (Convert.ToDecimal(item.ExpenseAmount) != Convert.ToDecimal(0))
                {
                    ExpensesList.Add(item);
                }
            }
            exp.Clear();
        }

        public void CalculateCashOnHand()
        {

        }
        private async void LoadData()
        {
            await Task.Run(() =>
            {
                this.MessageToken = CommonNavInfo.MessageToken;
                RouteID = CommonNavInfo.RouteID.ToString();
                UserRoute = Managers.UserManager.UserRoute;
                RouteName = Managers.UserManager.RouteName;
                Route = PayloadManager.ApplicationPayload.Route;
                CashOnHand = PayloadManager.RouteSettlementPayload.TotalCash.ToString("F2").Trim();
                if (Convert.ToDecimal(CashOnHand) > 0)
                {
                    IsAddEnabled = true;
                }
                GetExpenseList();
            });
        }

        public void LogActivityForExpense(ActivityKey key)
        {
            Activity ac = new Activity
            {
                ActivityType = key.ToString(),
                ActivityStart = DateTime.Now,
                ActivityEnd = DateTime.Now,
                IsTxActivity = true,
                ActivityDetailClass = ExpensesObject.ToString(),
                ActivityDetails = ExpensesObject.SerializeToJson(),
                RouteID = this.RouteID
            };
            if (key.ToString().ToLower() == "CreateExpense".ToString().ToLower())
            {
                ac.ActivityStatus = "USTLD";
            }
            else if (key.ToString().ToLower() == "VoidExpense".ToString().ToLower())
            {
                ac.ActivityStatus = "VOID";
            }

            //Get the parent activity id
            ac.ActivityHeaderID = this.GetParentActivity(this.ExpensesObject.ExpenseID.ToString());
            Activity activity = ResourceManager.Transaction.LogActivity(ac);
        }

        private string GetParentActivity(string expenseId)
        {
            string query = "";

            try
            {
                query = string.Format("SELECT TDID FROM BUSDTA.M50012 WHERE TDTYP='Expense' AND TDDTLS LIKE '%\"ExpenseID\":\"{0}\"%' AND TDPNTID=0", expenseId);

                return DbEngine.ExecuteScalar(query);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][GetParentActivity][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return string.Empty;    
            }
        }

        #endregion
    }
}
