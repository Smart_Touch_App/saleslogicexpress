﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ReportViewModels;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RouteSettlementSettlementsViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.RouteSettlementSettlementsViewModel");


        //AllSettlements
        #region Properties & Variables

        private ObservableCollection<RouteSettlementSettlementsModel> allSettlements;

        public ObservableCollection<RouteSettlementSettlementsModel> AllSettlements
        {
            get { return allSettlements; }
            set { allSettlements = value; OnPropertyChanged("AllSettlements"); }
        }
        public ObservableCollection<UsersForVerification> UsersForVerification { get; set; }
        public ObservableCollection<ViewModels.ReasonCode> ReasonCodeList { get; set; }
        private ViewModels.ReasonCode reasonCodeSelected = new ReasonCode();


        public Guid MessageToken { get; set; }
        private static int voidOrderReasonId;

        public static int VoidOrderReasonId
        {
            get { return RouteSettlementSettlementsViewModel.voidOrderReasonId; }
            set { RouteSettlementSettlementsViewModel.voidOrderReasonId = value; }
        }



        #endregion

        #region OnPropertyChanged

        private bool isEnabledPrint = false;

        public bool IsEnabledPrint
        {
            get { return isEnabledPrint; }
            set { isEnabledPrint = value; OnPropertyChanged("IsEnabledPrint"); }
        }

        private bool isEnabledVoid = false;

        public bool IsEnabledVoid
        {
            get { return isEnabledVoid; }
            set { isEnabledVoid = value; OnPropertyChanged("IsEnabledVoid"); }
        }

        private bool isEnabledVerification = false;

        public bool IsEnabledVerification
        {
            get { return isEnabledVerification; }
            set { isEnabledVerification = value; OnPropertyChanged("IsEnabledVerification"); }
        }

        private RouteSettlementSettlementsModel selectedSettlement;

        public RouteSettlementSettlementsModel SelectedSettlement
        {
            get { return selectedSettlement; }
            set
            {
                selectedSettlement = value;
                if (selectedSettlement != null)
                {
                    if (selectedSettlement.Status.ToLower() == "new")
                    {
                        IsEnabledVoid = IsEnabledVerification = IsEnabledPrint = true;
                    }
                    else
                    {
                        IsEnabledVoid = IsEnabledVerification = IsEnabledPrint = false;
                    }
                }

                OnPropertyChanged("SelectedSettlement");
            }
        }
        UsersForVerification selectedUserForVerification = new UsersForVerification();

        public UsersForVerification SelectedUserForVerification
        {
            get { return selectedUserForVerification; }
            set { selectedUserForVerification = value; OnPropertyChanged("SelectedUserForVerification"); }
        }


        private bool isSelfVerification = false;

        public bool IsSelfVerification
        {
            get { return isSelfVerification; }
            set
            {
                isSelfVerification = value;
                if (value)
                {
                    VerificationPassword = "";
                    IsEnableContinueVeriFication = true;
                }
                else
                {
                    if (String.IsNullOrEmpty(VerificationPassword))
                    {
                        IsEnableContinueVeriFication = false;
                    }

                } OnPropertyChanged("IsSelfVerification");
            }
        }

        private string verificationPassword;

        public string VerificationPassword
        {
            get { return verificationPassword; }
            set
            {
                verificationPassword = value;
                if (!string.IsNullOrEmpty(value))
                {
                    IsEnableContinueVeriFication = true;
                    IsSelfVerification = false;
                }
                else
                {
                    IsEnableContinueVeriFication = false;

                }
                OnPropertyChanged("VerificationPassword");
            }
        }

        private bool isEnableContinueVeriFication = false;

        public bool IsEnableContinueVeriFication
        {
            get { return isEnableContinueVeriFication; }
            set
            {
                isEnableContinueVeriFication = value;
                OnPropertyChanged("IsEnableContinueVeriFication");
            }
        }
        public ViewModels.ReasonCode ReasonCodeSelected
        {
            get { return reasonCodeSelected; }
            set { reasonCodeSelected = value; OnPropertyChanged("ReasonCodeSelected"); }
        }
        private string settlementNoPopup;

        public string SettlementNoPopup
        {
            get { return settlementNoPopup; }
            set { settlementNoPopup = value; OnPropertyChanged("SettlementNoPopup"); }
        }
        private string settlementAmountPopup;


        public string SettlementAmountPopup
        {
            get { return settlementAmountPopup; }
            set { settlementAmountPopup = value; OnPropertyChanged("SettlementAmountPopup"); }
        }

        private string exceptionAmountPopup;

        public string ExceptionAmountPopup
        {
            get { return exceptionAmountPopup; }
            set { exceptionAmountPopup = value; OnPropertyChanged("ExceptionAmountPopup"); }
        }
        private string originatorPopup;

        public string OriginatorPopup
        {
            get { return originatorPopup; }
            set { originatorPopup = value; OnPropertyChanged("OriginatorPopup"); }
        }
        private string datePopup;

        public string DatePopup
        {
            get { return datePopup; }
            set { datePopup = value; OnPropertyChanged("DatePopup"); }
        }
        #endregion

        #region Commands
        public DelegateCommand PrintSettlement { get; set; }
        public DelegateCommand VoidSettlement { get; set; }
        public DelegateCommand VerifySettlement { get; set; }
        public DelegateCommand VoidSettlementExecute { get; set; }

        public DelegateCommand ContinueVerification { get; set; }

        public DelegateCommand VoidOrder { get; set; }

        #endregion

        #region Constructor & Methods
        public RouteSettlementSettlementsViewModel()
        {
            IsBusy = true;
            LoadSettlemntTab();
        }
        public void LoadSettlemntTab()
        {
            InitialiseCommands();
            GetAllSettlements();
            GetUserForVerification();
            IsBusy = false;
        }
        public void InitialiseCommands()
        {
            PrintSettlement = new DelegateCommand((param) => {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:PrintSettlement]\t" + DateTime.Now + "");
                PrintSettlementAsync((RouteSettlementSettlementsModel)param); 
            });
            VerifySettlement = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:VerifySettlement]\t" + DateTime.Now + "");

                try
                {
                    RouteSettlementSettlementsModel objSelected = (RouteSettlementSettlementsModel)param;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID = objSelected.SettlementID;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementNo = objSelected.SettlementNo;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorName = objSelected.OriginatorName;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorID = objSelected.Originator;

                    var dialog = new Helpers.DialogWindow { TemplateKey = "SelectVerificationUser", Title = "Select Verification User" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel VerifySettlement( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:VerifySettlement]\t");

            });

            ContinueVerification = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:ContinueVerification]\t" + DateTime.Now + "");

                try
                {
                    UsersForVerification objUsr = (UsersForVerification)param;
                    if (IsSelfVerification)
                    {
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification = true;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.GrandTotal = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCash = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCheck = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalMoneyOrder = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalExpenses = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierID = ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorID;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierName = ViewModelPayload.PayloadManager.RouteSettlementPayload.OriginatorName;
                        //ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = true;

                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                        ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Settlement.ToString();

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.SettlementVerification, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                    }
                    else if (VerificationPassword != objUsr.Password && !IsSelfVerification)
                    {
                        //Password doesnot match
                        var alertMessage = new Helpers.AlertWindow { Message = Helpers.Constants.RouteSettlement.PasswordMismatch, MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    else if (VerificationPassword == objUsr.Password && !IsSelfVerification)
                    {

                        ViewModelPayload.PayloadManager.RouteSettlementPayload.IsForVerification = true;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.GrandTotal = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCash = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalCheck = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalMoneyOrder = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.TotalExpenses = 0;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierID = objUsr.UserID;
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.VerifierName = objUsr.UserName;
                        //ViewModelPayload.PayloadManager.RouteSettlementPayload.IsSettlementTabActive = true;

                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                        ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.EOD_Settlement.ToString();

                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.SettlementVerification, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel ContinueVerification( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:ContinueVerification]\t");



            });
            VoidSettlement = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:VoidSettlement]\t" + DateTime.Now + "");

                try
                {
                    RouteSettlementSettlementsModel objSelected = (RouteSettlementSettlementsModel)param;
                    ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID = objSelected.SettlementID;
                    DatePopup = objSelected.Date.ToString();
                    OriginatorPopup = objSelected.OriginatorName;
                    ExceptionAmountPopup = "$" + objSelected.ExceptionAmount.ToString();
                    SettlementAmountPopup = "$" + objSelected.SettlementAmount.ToString();
                    SettlementNoPopup = "Settlement No.: " + objSelected.SettlementNo;


                    var dialog = new Helpers.DialogWindow { TemplateKey = "VoidSettlement", Title = "Void Settlement" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);


                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel VoidSettlement( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:VoidSettlement]\t");



            });

            VoidSettlementExecute = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:VoidSettlementExecute]\t" + DateTime.Now + "");

                try
                {
                    if (param != null)
                    {
                        ReasonCode objSe = (ReasonCode)param;
                        SettlementConfirmationManager.VoidSettlement(ViewModelPayload.PayloadManager.RouteSettlementPayload.SettlementID, "VOID", objSe.Code);
                        ViewModelPayload.PayloadManager.RouteSettlementPayload.IsReinitialiseTransaction = true;
                        GetAllSettlements();
                        Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("RouteSettlementSettlementsViewModel VoidSettlementExecute( ) error: " + ex.Message);
                }

                Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:VoidSettlementExecute]\t");



            });

        }

        public void GetAllSettlements()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:GetAllSettlements]\t" + DateTime.Now + "");

            try
            {
                AllSettlements = new ObservableCollection<RouteSettlementSettlementsModel>();//RouteSettlementManager.GetAllSettlements();
                AllSettlements = RouteSettlementManager.GetAllSettlements();
                IsEnabledVoid = false;
                IsEnabledVerification = false;
                IsEnabledPrint = false;
                GetVoidReasons();
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel GetAllSettlements( ) error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:GetAllSettlements]\t");



        }
        public void GetUserForVerification()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:GetUserForVerification]\t" + DateTime.Now + "");

            try
            {
                UsersForVerification = new ObservableCollection<UsersForVerification>();

                List<UsersForVerification> vUsers = new List<UsersForVerification>();

                vUsers = SettlementConfirmationManager.GetUserForVerification();
                for (int i = 0; i < vUsers.Count; i++)
                {
                    UsersForVerification.Add(vUsers[i]);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel GetUserForVerification( ) error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:GetUserForVerification]\t");



        }

        public void GetVoidReasons()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][Start:GetVoidReasons]\t" + DateTime.Now + "");

            try
            {
                ReasonCodeList = new ObservableCollection<ReasonCode>();
                ReasonCodeList = SettlementConfirmationManager.GetReasonListForVoid("Void Settlement");
            }
            catch (Exception ex)
            {
                Logger.Error("RouteSettlementSettlementsViewModel GetVoidReasons( ) error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][RouteSettlementSettlementsViewModel][End:GetVoidReasons]\t");
        }
        async void PrintSettlementAsync(RouteSettlementSettlementsModel param)
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                //new RouteSettlementReport(param);
                 
                var payload = param;
                
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.RouteSettlementReport, CurrentViewName = ViewModelMappings.View.RouteSettlement, CloseCurrentView = false, Payload = payload, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
        }
        
        #endregion
    }
}
