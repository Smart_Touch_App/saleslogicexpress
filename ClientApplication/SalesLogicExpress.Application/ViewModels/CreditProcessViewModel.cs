﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using Telerik.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media;
using System.Windows.Threading;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using System.ComponentModel;
using System.Threading;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CreditProcessViewModel : BaseViewModel, IDataErrorInfo
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CreditProcessViewModel");

        #region Properties
        public bool IsUnplannedCreatedForFuture { get; set; }
        bool _IsSearching = false;
        public bool IsSearching
        {
            get
            {
                return _IsSearching;
            }
            set
            {
                _IsSearching = value;
                OnPropertyChanged("IsSearching");
            }
        }
        private Guid _MessageToken;
        public Guid MessageToken
        {

            get { return _MessageToken; }
            set
            {
                _MessageToken = value;
                OnPropertyChanged("MessageToken");
            }
        }

        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
            set { customerNo = value; OnPropertyChanged("CustomerNo"); }
        }

        private string routeID;
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; OnPropertyChanged("RouteID"); }
        }

        private string routeName;
        public string RouteName
        {
            get { return routeName; }
            set { routeName = value; OnPropertyChanged("RouteName"); }
        }

        private string creditNote;
        public string CreditNote
        {
            get { return creditNote; }
            set
            {
                creditNote = value;
                if (!string.IsNullOrEmpty(CreditNote.Trim()))
                {
                    CreditMemoObject.CreditNote = CreditNote.Trim();
                    if (!string.IsNullOrEmpty(CreditAmount) && IsValidAmount && SelectedReasonCode > 0)
                    {
                        ToggleReasonCodeButton = true;
                    }
                    else
                        ToggleReasonCodeButton = false;
                }
                else
                    ToggleReasonCodeButton = false;
                OnPropertyChanged("CreditNote");
            }
        }

        private string creditAmount;
        public string CreditAmount
        {
            get { return creditAmount; }
            set
            {
                creditAmount = value;
                if (IsAddCreditMemo)
                {
                    if (!string.IsNullOrEmpty(CreditAmount))
                    {
                        if ((Convert.ToDecimal(CreditAmount) > 0 && Convert.ToDecimal(CreditAmount) <= 100))
                        {
                            IsValidAmount = true;
                            CreditMemoObject.CreditAmount = Convert.ToDecimal(CreditAmount).ToString("F2");

                            if (SelectedReasonCode > 0 && !string.IsNullOrEmpty(CreditNote.Trim()))
                            {
                                ToggleReasonCodeButton = true;
                            }
                            else
                            {
                                ToggleReasonCodeButton = false;
                            }
                        }
                        else
                        {
                            IsValidAmount = false;
                            ToggleReasonCodeButton = false;
                        }
                    }
                    else
                    {
                        IsValidAmount = false;
                        ToggleReasonCodeButton = false;
                    }
                }
                else
                {
                    if (SelectedReasonCode > 0)
                    {
                        ToggleReasonCodeButton = true;
                    }
                    else
                        ToggleReasonCodeButton = false;
                }
                OnPropertyChanged("CreditAmount");
            }
        }

        private CreditProcess creditMemoObject = new CreditProcess();
        public CreditProcess CreditMemoObject
        {
            get { return creditMemoObject; }
            set { creditMemoObject = value; OnPropertyChanged("CreditMemoObject"); }
        }

        private ObservableCollection<CreditProcess> creditsMemoList = new ObservableCollection<CreditProcess>();
        private ObservableCollection<CreditProcess> creditsMemoFilteredList = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> CreditsMemoList
        {
            get { return creditsMemoList; }
            set { creditsMemoList = value; OnPropertyChanged("CreditsMemoList"); }
        }

        private ObservableCollection<CreditProcess> creditListForSearch = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> CreditListForSearch
        {
            get { return creditListForSearch; }
            set { creditListForSearch = value; OnPropertyChanged("CreditListForSearch"); }
        }

        private ObservableCollection<CreditProcess> temp = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> Temp
        {
            get { return temp; }
            set { temp = value; OnPropertyChanged("Temp"); }
        }

        private ObservableCollection<CreditProcess> tempCollection = new ObservableCollection<CreditProcess>();
        public ObservableCollection<CreditProcess> TempCollection
        {
            get { return tempCollection; }
            set { tempCollection = value; OnPropertyChanged("TempCollection"); }
        }
        CanvasViewModel _CanvasVM;
        public CanvasViewModel SignatureCanvasVM
        {
            get
            {
                return _CanvasVM;
            }
            set
            {
                _CanvasVM = value;
                OnPropertyChanged("SignatureCanvasVM");
            }
        }
        private ObservableCollection<ReasonCode> reasonCode = null;

        public ObservableCollection<ReasonCode> ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; OnPropertyChanged("ReasonCode"); }
        }

        private int selectedReasonCode;
        public int SelectedReasonCode
        {
            get { return selectedReasonCode; }
            set
            {
                selectedReasonCode = value;
                ToggleReasonCodeButton = false;
                if (SelectedReasonCode > 0)
                {
                    if (IsAddCreditMemo)
                    {
                        CreditMemoObject.CreditReasonID = SelectedReasonCode.ToString();
                        if (!string.IsNullOrEmpty(CreditAmount) && IsValidAmount && !string.IsNullOrEmpty(CreditNote.Trim()))
                        {
                            CreditMemoObject.CreditReasonID = SelectedReasonCode.ToString();
                            if (!string.IsNullOrEmpty(CreditAmount) && IsValidAmount && !string.IsNullOrEmpty(CreditNote.Trim()))
                            {
                                ToggleReasonCodeButton = true;
                            }
                            else ToggleReasonCodeButton = false;
                        }
                        else ToggleReasonCodeButton = false;
                    }
                    else
                        ToggleReasonCodeButton = true;
                }
                OnPropertyChanged("SelectedReasonCode");
            }
        }

        private string reasonDescription;
        public string ReasonDescription
        {
            get { return reasonDescription; }
            set { reasonDescription = value; OnPropertyChanged("ReasonDescription"); }
        }

        private int reasonCodeID;
        public int ReasonCodeID
        {
            get { return reasonCodeID; }
            set { reasonCodeID = value; OnPropertyChanged("ReasonCodeID"); }
        }

        private bool toggleReasonCodeButton = false;
        public bool ToggleReasonCodeButton
        {
            get { return toggleReasonCodeButton; }
            set { toggleReasonCodeButton = value; OnPropertyChanged("ToggleReasonCodeButton"); }
        }

        private bool toggleSignAndPrintButton;
        public bool ToggleSignAndPrintButton
        {
            get { return toggleSignAndPrintButton; }
            set { toggleSignAndPrintButton = value; OnPropertyChanged("ToggleSignAndPrintButton"); }
        }

        private bool isSwitchDisable = false;
        public bool IsSwitchDisable
        {
            get { return isSwitchDisable; }
            set { isSwitchDisable = value; OnPropertyChanged("IsSwitchDisable"); }
        }

        private bool isPrintButtonEnabled = false;
        public bool IsPrintButtonEnabled
        {
            get { return isPrintButtonEnabled; }
            set { isPrintButtonEnabled = value; OnPropertyChanged("IsPrintButtonEnabled"); }
        }

        private bool isVoidButtonEnabled = true;
        public bool IsVoidButtonEnabled
        {
            get
            {
                return isVoidButtonEnabled;
            }
            set
            {
                isVoidButtonEnabled = value;
                OnPropertyChanged("IsVoidButtonEnabled");
            }
        }

        private bool isAddCreditMemoEnabled = true;
        public bool IsAddCreditMemoEnabled
        {
            get
            {
                return isAddCreditMemoEnabled;
            }
            set
            {
                isAddCreditMemoEnabled = value;
                OnPropertyChanged("IsAddCreditMemoEnabled");
            }
        }

        private bool isVoidDialog = false;
        public bool IsVoidDialog
        {
            get { return isVoidDialog; }
            set { isVoidDialog = value; OnPropertyChanged("IsVoidDialog"); }
        }
        bool IsAddCreditMemo = false;
        bool IsSignPrintRequired = false;
        bool IsValidAmount = true;
        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }
        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "CreditAmount" && !IsValidAmount)
                {
                    result = Helpers.Constants.Common.CreditAmountErrorMessage;
                }
                return result;
            }
        }

        public static int generatedCreditNo = 0;
        #endregion

        #region DelegateCommands
        public DelegateCommand ClearSearchText { get; set; }
        public DelegateCommand OpenSignAndPrintDialog { get; set; }
        public DelegateCommand OpenDialogForAddCredit { get; set; }
        public DelegateCommand OpenDialogForVoidCredit { get; set; }
        public DelegateCommand PrintCreditMemo { get; set; }
        public DelegateCommand FinaliseCreditMemo { get; set; }
        public DelegateCommand ClearSign { get; set; }
        public DelegateCommand SelectionItemInvoke { get; set; }

        public DelegateCommand AddVoidPopUpGridUnloadDlgtCmd { get; set; }
        #endregion

        #region Constructor
        public CreditProcessViewModel(bool flag)
        {
            IsUnplannedCreatedForFuture = flag;
            InitializeCommands();
            LoadData();

        }
        #endregion

        #region Methods

        CreditProcessManager creditManager = new CreditProcessManager();
        public void InitializeCommands()
        {
            #region Grid unloaded events for oppening pop-up

            AddVoidPopUpGridUnloadDlgtCmd = new DelegateCommand((param) =>
            {
                if (this.IsSignPrintRequired)
                {
                    this.IsSignPrintRequired = false;
                    var dialogWindow = new Helpers.DialogWindow { TemplateKey = "SignAndPrint", Title = "Sign And Print" };
                    Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
                }
            });

            #endregion

            #region OpenDialogForAddCredit
            ClearSearchText = new DelegateCommand((param) =>
           {
               SearchText = string.Empty;
               RunSearch(SearchText);
           });
            OpenDialogForAddCredit = new DelegateCommand((param) =>
            {
                IsAddCreditMemo = true;
                ReasonCode = creditManager.GetCreditReasonCode();
                CreditMemoObject.ReceiptNumber = CreditMemoObject.ReceiptNumber == -1 ? creditManager.GetNewCreditMemoNumber() : CreditMemoObject.ReceiptNumber;
                CreditMemoObject.ReceiptID = CreditMemoObject.ReceiptNumber;
                RenderSign();
                SelectedReasonCode = -1;
                CreditAmount = "";
                IsValidAmount = true;
                ToggleReasonCodeButton = false;
                CreditNote = string.Empty;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "AddCreditDialog", Title = "Add Credit Memo" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);

            });
            #endregion

            PrintCreditMemo = new DelegateCommand((param) =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = "Do you want to print?", MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                RenderSign();
                new ReportViewModels.CreditMemoReport(CreditMemoObject);

            });

            #region OpenSignAndPrintDialog

            OpenSignAndPrintDialog = new DelegateCommand((param) =>
            {
                //TODO
                // GENERATE THE RANDOM UNIQUE NO. FROM NO. POOL AND ASSIGN IT TO RECEIPT NO.
                // SHOW ALL INFO INTO sign and print pop-up 
                //if (SignatureCanvasVM.CanvasStrokeCollection.StrokesChanged != null)
                //{
                //}
                RenderSign();
                SignatureCanvasVM.CanvasStrokeCollection.Clear();
                SignatureCanvasVM.IsCanvasEnable = true;
                SignatureCanvasVM.CanClearCanvas = false;
                SignatureCanvasVM.CanvasStrokeCollection.StrokesChanged += CanvasStrokeCollection_StrokesChanged;

                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                ToggleSignAndPrintButton = false;
                if (IsAddCreditMemo)
                {
                    IsVoidDialog = false;
                    //CreditMemoObject.ReceiptNumber = 554366;
                    CreditMemoObject.CreditReasonID = SelectedReasonCode.ToString();
                    foreach (ReasonCode item in ReasonCode)
                    {
                        if (item.Id == Convert.ToInt32(CreditMemoObject.CreditReasonID))
                        {
                            CreditMemoObject.CreditReason = item.Code.Trim();
                        }
                    }
                    CreditMemoObject.CreditMemoDate = DateTime.Now;
                }
                else
                {
                    IsVoidDialog = true;
                    CreditMemoObject.VoidReasonID = SelectedReasonCode.ToString();

                    foreach (ReasonCode item in ReasonCode)
                    {
                        if (item.Id == Convert.ToInt32(CreditMemoObject.VoidReasonID))
                        {
                            CreditMemoObject.VoidReason = item.Code.Trim();
                        }
                    }
                }

                this.IsSignPrintRequired = true; //Set flag to true to open te Sign print pop-up dialog grd unload event 
            });
            #endregion

            #region FinaliseCreditMemo

            FinaliseCreditMemo = new DelegateCommand((param) =>
            {
                if (IsAddCreditMemo)
                {
                    CreditMemoObject.StatusID = creditManager.GetStatusId("USTLD");
                    creditManager.AddCreditMemo(CreditMemoObject);
                    SignatureCanvasVM.UpdateSignatureToDB(true, true, true, "CreditMemoSign.jpg");
                    LogActivityForCreditMemo(ActivityKey.CreateCreditMemo);
                    CreditMemoObject.ReceiptNumber = -1;

                    #region update quantities
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.OrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.OrderPayload.Customer.StopID;
                    }

                    new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                    new CustomerManager().UpdateActivityCount(stopID, false, true);
                    PayloadManager.OrderPayload.Customer.CompletedActivity += 1;
                    PayloadManager.OrderPayload.Customer.SaleStatus = "";
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                    #endregion

                    if (!IsUnplannedCreatedForFuture)
                    {
                        DateTime todayDate = Convert.ToDateTime(tempStopDate);
                        ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                        PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                    }

                }
                else
                {
                    CreditMemoObject.VoidReasonID = SelectedReasonCode.ToString();
                    foreach (ReasonCode item in reasonCode)
                    {
                        if (item.Id == SelectedReasonCode)
                        {
                            CreditMemoObject.VoidReason = item.Code.Trim();
                            break;
                        }
                    }
                    new ARPaymentManager(CreditMemoObject.CustomerNo, CommonNavInfo.Customer.Company).VoidReceipt(CreditMemoObject.ReceiptID.ToString(), CreditMemoObject.VoidReason, CreditMemoObject.RouteID, false);
                    //creditManager.UpdateCreditMemoStatus(CreditMemoObject.ReceiptID, CreditMemoObject.CustomerNo, CreditMemoObject.RouteID);
                    LogActivityForCreditMemo(ActivityKey.VoidCreditMemo);
                    CreditMemoObject.ReceiptNumber = -1;
                }
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                new ReportViewModels.CreditMemoReport(CreditMemoObject);

                GetCreditMemoList();
            });
            #endregion

            #region OpenDialogForVoidCredit

            OpenDialogForVoidCredit = new DelegateCommand((param) =>
            {
                ReasonCode = creditManager.GetVoidReasonCode();
                SelectedReasonCode = -1;

                IsAddCreditMemo = false;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "VoidCreditProcess", Title = "Void Credit" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
            });
            #endregion

            #region SelectionItemInvoke

            SelectionItemInvoke = new DelegateCommand((param) =>
            {
                if (param == null)
                {
                    return;
                }

                CreditMemoObject = param as CreditProcess;
                if (CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.USTLD.ToString())
                {
                    IsVoidButtonEnabled = IsPrintButtonEnabled = true; IsAddCreditMemoEnabled = false;
                }
                else if (CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.VOID.ToString() ||
                    CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.STTLD.ToString() ||
                    CreditMemoObject.StatusCode.Trim() == StatusTypesEnum.VDSTLD.ToString())
                {
                    IsVoidButtonEnabled = false; IsPrintButtonEnabled = true;
                }

            });
            #endregion

        }

        private void RenderSign()
        {
            SignatureCanvasVM = new CanvasViewModel(Flow.Credits, SignType.CreditProcessSign, CreditMemoObject.ReceiptID)
            {
                Height = 250,
                Width = 300,
                FileName = "CreditMemo.jpg",
                FooterText = "Customer's Signature"
            };
        }
        private async void LoadData()
        {
            await Task.Run(() =>
            {
                this.MessageToken = CommonNavInfo.MessageToken;
                CustomerNo = PayloadManager.OrderPayload.Customer.CustomerNo;
                CreditMemoObject.CustomerName = PayloadManager.OrderPayload.Customer.Name.Trim();
                CreditMemoObject.CustomerNo = CustomerNo;
                GetCreditMemoList();

            });
        }
        public void GetCreditMemoList()
        {
            #region old code
            //Random rd = new Random();

            //string[] status = new string[] { "USTLD", "STTLD", "VOID", "VDSTLD" };
            //string[] reasons = new string[] {"items defective", "customer unhappy", "bla bla"};
            //string[] note = new string[] {"adshf", "sdfhadfhadgadgadf", "sdfasdfasdf"};
            //for (int i = 0; i < 20; i++)
            //{
            //    ReturnsAndCredits rc = new ReturnsAndCredits();
            //    rc.CreditMemoDate = DateTime.Now;
            //    rc.CreditMemoNumber = rd.Next(10000, 434433);
            //    rc.CreditNote = note[rd.Next(0, note.Length - 1)];
            //    rc.CreditReason = reasons[rd.Next(0, reasons.Length - 1)];
            //    rc.CreditAmount = rd.Next(20, 100);
            //    rc.CreditMemoStatusCode = status[rd.Next(0, status.Length - 1)];

            //    CreditsMemoList.Add(rc);
            //}
            #endregion
            //Todo
            //1. get the observable collection in temp collection
            //2.apply the sorting algorithm
            //3.assign that collection to main.

            CreditsMemoList = creditManager.GetCreditProcessList(CustomerNo);
            SequenceList(CreditsMemoList);
            foreach (CreditProcess item1 in CreditsMemoList)
            {
                Temp.Add(item1);
            }
            creditsMemoFilteredList = new ObservableCollection<CreditProcess>(CreditsMemoList.ToList<CreditProcess>());
            EnableDisableButtons();
        }

        public void SequenceList(ObservableCollection<CreditProcess> creditsList)
        {
            CreditsMemoList = new ObservableCollection<CreditProcess>(creditsList.OrderBy(x => x.CreditSeqID).ThenByDescending(x => x.CreditMemoDate).ToList());
            //CreditsMemoList = (ObservableCollection<CreditProcess>)TempCollection.OrderBy(x => x.CreditMemoDate);
            //creditsList.OrderBy(x => x.CreditMemoDate);

        }

        public void EnableDisableButtons()
        {
            //TODO
            //1. CHECK OBSERVABLE COLLECTION.
            // 2. ENABLE DISABLE ADD BUTTON ACC. TO THAT

            IsVoidButtonEnabled = IsPrintButtonEnabled = false;
            if (PayloadManager.OrderPayload.Customer.IsTodaysStop)
            {
                CreditProcess obj = CreditsMemoList.FirstOrDefault(x => (x.StatusCode.ToLower() == StatusTypesEnum.USTLD.ToString().ToLower()) && (DateTime.Compare(x.CreditMemoDate.Date, DateTime.Now.Date) == 0));
                if (obj != null)
                {
                    IsAddCreditMemoEnabled = false;
                }
                else
                    IsAddCreditMemoEnabled = true;
            }
            else
            {
                IsAddCreditMemoEnabled = false;
            }
        }
        void CanvasStrokeCollection_StrokesChanged(object sender, StrokeCollectionChangedEventArgs e)
        {
            try
            {
                if (SignatureCanvasVM.CanvasStrokeCollection.Count > 0)
                {
                    ToggleSignAndPrintButton = true;
                }
                else
                {
                    ToggleSignAndPrintButton = false;
                }
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }

        public void LogActivityForCreditMemo(ActivityKey key)
        {
            Activity ac = new Activity
            {
                CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo.ToString(),
                ActivityType = key.ToString(),
                ActivityStart = DateTime.Now,
                ActivityEnd = DateTime.Now,
                IsTxActivity = true,
                ActivityDetailClass = CreditMemoObject.ToString(),
                ActivityDetails = CreditMemoObject.SerializeToJson(),
                RouteID = this.RouteID
            };
            if (key.ToString().ToLower() == "CreateCreditMemo".ToString().ToLower())
            {
                ac.ActivityStatus = "USTLD";
            }
            else if (key.ToString().ToLower() == "VoidCreditMemo".ToString().ToLower())
            {
                ac.ActivityStatus = "VOID";
            }
            else if (key.ToString().ToLower() == "SettledCreditMemo".ToString().ToLower())
            {
                ac.ActivityStatus = "STTLD";
            }
            else if (key.ToString().ToLower() == "VDDSettledCreditMemo".ToString().ToLower())
            {
                ac.ActivityStatus = "VDSTLD";
            }

            //Get the parent activity id
            ac.ActivityHeaderID = this.GetParentActivity(this.CreditMemoObject.ReceiptID.ToString());
            Activity activity = ResourceManager.Transaction.LogActivity(ac);
        }

        public void Search(string text)
        {
            if (text == null) return;
            string searchText = text.ToString().ToLower();
            if (searchText.Trim().Length > 2 || searchText.Trim().Length == 0)
            {
                RunSearch(searchText);
            }
        }
        public void ResetSearch()
        {
            CreditsMemoList = new ObservableCollection<CreditProcess>(creditsMemoFilteredList.ToList<CreditProcess>());
        }
        CancellationTokenSource tokenForCancelTask = null;

        void RunSearch(string searchText)
        {
            IsSearching = true;
            if (tokenForCancelTask != null)
            {
                tokenForCancelTask.Cancel();
            }
            tokenForCancelTask = new CancellationTokenSource();
            Task.Run(async () => await SearchItem(searchText, tokenForCancelTask.Token), tokenForCancelTask.Token);
        }

        private async Task SearchItem(string searchText, CancellationToken token)
        {
            try
            {
                await Task.Run(() =>
                {
                    CreditsMemoList = new ObservableCollection<CreditProcess>(creditsMemoFilteredList.Where(item => (item.ReceiptID.ToString().ToLower().Contains(searchText) || item.CreditReason.ToLower().Contains(searchText) || item.CreditNote.ToLower().Contains(searchText))).ToList<CreditProcess>());
                    IsSearching = false;
                });
            }
            catch (Exception ex)
            {
            }
        }

        private string GetParentActivity(string creditReceiptId)
        {
            string query = "";

            try
            {
                query = string.Format("SELECT TDID FROM BUSDTA.M50012 WHERE TDAN8='{0}' AND TDTYP='CreditMemo' AND TDDTLS LIKE '%\"ReceiptID\":{1}%' AND TDPNTID=0", customerNo, creditReceiptId);

                return DbEngine.ExecuteScalar(query);

            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion

        //public enum CreditID
        //{

        //    [Description("USTLD")]
        //    ID = 1,
        //    [Description("VOID")]
        //    ID = 2,
        //    [Description("STTLD")]
        //    ID = 3,
        //    [Description("VDSTLD")]
        //    ID = 4
        //}

    }
}



