﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ReportViewModels;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerReturnsPickViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerReturnsPickViewModel");

        CustomerReturnsManager CR_Manager = new CustomerReturnsManager();

        #region Command's
        public DelegateCommand NavigateToAcceptAndPrint { get; set; }
        public DelegateCommand ScanByDeviceCommand { get; set; }
        public DelegateCommand ScanByDeviceExceptionCommand { get; set; }
        public DelegateCommand StartManualPick { get; set; }
        public DelegateCommand SelectionChanged { get; set; }
        public DelegateCommand UpdateOnManualCountCommand { get; set; }
        public DelegateCommand ManualPickCommand { get; set; }

        public DelegateCommand ManualPickContinueCommand { get; set; }
        public DelegateCommand RemoveExceptions { get; set; }

        public DelegateCommand VoidPickList { get; set; }
        public DelegateCommand HoldPickList { get; set; }

        #endregion
        #region Prop's
        int pickcounter = 0;
        public Guid MessageToken { get; set; }
        private List<ReturnItem> ItemReturnList = new List<ReturnItem>();
        private bool isVoidStarted = false;

        public bool IsVoidStarted
        {
            get { return isVoidStarted; }
            set
            {
                isVoidStarted = value;
                //if (value)
                //{
                //    IsEnableRemoveException = true;
                //}
            }
        }
        bool isExceptionGridEnabled = false;
        public bool IsExceptionGridEnabled
        {
            get
            {
                return isExceptionGridEnabled;
            }
            set
            {


                isExceptionGridEnabled = value;
                OnPropertyChanged("IsExceptionGridEnabled");
            }
        }

        bool isManualCountStarted;
        public bool IsManualCountStarted
        {
            get
            {
                return isManualCountStarted;
            }
            set
            {


                isManualCountStarted = value;
                OnPropertyChanged("IsManualCountStarted");
            }
        }


        TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();

        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get { return returnItems; }
            set { returnItems = value; OnPropertyChanged("ReturnItems"); }
        }

        private ObservableCollection<ReasonCode> manualPickReasonList = null;
        public ObservableCollection<ReasonCode> ManualPickReasonList
        {
            get
            {
                return manualPickReasonList;
            }
            set
            {
                manualPickReasonList = value;
                OnPropertyChanged("ManualPickReasonList");
            }
        }

        string _ReturnOrderNumber;
        public string ReturnOrderNumber
        {
            get
            {
                return _ReturnOrderNumber;
            }
            set
            {
                _ReturnOrderNumber = value;
                OnPropertyChanged("ReturnOrderNumber");
            }
        }
        bool isScannerOn = true;
        public bool IsScannerOn
        {
            get
            {
                return isScannerOn;
            }
            set
            {
                isScannerOn = value;
                OnPropertyChanged("IsScannerOn");
            }
        }

        ReturnItem selectedReturnItem = new ReturnItem();

        public ReturnItem SelectedReturnItem
        {
            get { return selectedReturnItem; }
            set
            {
                selectedReturnItem = value; OnPropertyChanged("SelectedReturnItem");
                if (SelectedReturnItem != null)
                {
                    IsTextBoxFocus = true;
                    SelectedExceptionItem = null;
                    IsEnableManualPick = true;
                    SelectedItemNumber = SelectedReturnItem.ItemNumber;
                    SelectedPrimaryUoM = SelectedReturnItem.PrimaryUM;
                    ManuallyPickCountOfSelected = SelectedReturnItem.ReturnQty;
                    IsEnableRemoveException = false;
                    SelectedReturnItem.IsAllowManualPick = false;
                    foreach (ReturnItem item in PickItems)
                    {
                        item.IsAllowManualPick = false;
                        item.IsEnableScannerInException = false;
                    }
                    foreach (ReturnItem item in PickItemsException)
                    {
                        item.IsAllowManualPick = false;
                        item.IsEnableScannerInException = false;
                    }
                    //EnableDisableScannerForSelectedItem();
                }
            }
        }

        private ReturnItem selectedExceptionItem = new ReturnItem();

        public ReturnItem SelectedExceptionItem
        {
            get { return selectedExceptionItem; }
            set
            {
                selectedExceptionItem = value; OnPropertyChanged("SelectedExceptionItem");
                if (SelectedExceptionItem != null)
                {
                    IsTextBoxFocus = true;
                    SelectedReturnItem = null;
                    SelectedItemNumber = SelectedExceptionItem.ItemNumber;
                    SelectedPrimaryUoM = SelectedExceptionItem.PrimaryUM;
                    ManuallyPickCountOfSelected = Convert.ToInt32(SelectedExceptionItem.ExceptionCount);
                    if (SelectedExceptionItem.IsEnableScannerInException)
                    {
                        IsEnableManualPick = true;
                        SelectedExceptionItem.IsAllowManualPick = false;
                        IsEnableRemoveException = false;
                    }
                    else
                    {
                        foreach (ReturnItem item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                            item.IsEnableScannerInException = false;
                        }
                        IsEnableManualPick = false;
                        IsEnableRemoveException = true;
                    }
                }
                else
                {
                    IsEnableRemoveException = false;
                }
            }
        }


        string selectedPrimaryUoM;
        public string SelectedPrimaryUoM
        {
            get
            {
                return selectedPrimaryUoM;
            }
            set
            {


                selectedPrimaryUoM = value;
                OnPropertyChanged("SelectedPrimaryUoM");
            }
        }


        string selectedItemNumber;
        public string SelectedItemNumber
        {
            get
            {
                return selectedItemNumber;
            }
            set
            {
                selectedItemNumber = value;
                OnPropertyChanged("SelectedItemNumber");
            }
        }


        bool isAcceptAndPrintEnabled;
        public bool IsAcceptAndPrintEnabled
        {
            get
            {
                return isAcceptAndPrintEnabled;
            }
            set
            {


                isAcceptAndPrintEnabled = value;
                OnPropertyChanged("IsAcceptAndPrintEnabled");
            }
        }


        bool isEnableManualPick = false;
        public bool IsEnableManualPick
        {
            get
            {
                return isEnableManualPick;
            }
            set
            {


                isEnableManualPick = value;
                OnPropertyChanged("IsEnableManualPick");
            }
        }


        bool isEnableRemoveException = false;
        public bool IsEnableRemoveException
        {
            get
            {
                return isEnableRemoveException;
            }
            set
            {
                //If exception list not null, then check item list count and update the value
                if (PickItemsException != null)
                {
                    isEnableRemoveException = (this.PickItemsException.Count > 0 ? true : value);
                }
                else
                {
                    IsEnableRemoveException = value;
                }
                OnPropertyChanged("IsEnableRemoveException");
            }
        }

        int manuallyPickCountOfSelected = 0;
        public int ManuallyPickCountOfSelected
        {
            get
            {
                return manuallyPickCountOfSelected;
            }
            set
            {
                manuallyPickCountOfSelected = value;
                OnPropertyChanged("ManuallyPickCountOfSelected");
            }
        }


        #region PickManager Prop's
        private PickItemManager pickMgr = null;
        ObservableCollection<dynamic> _PickItems = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItems
        {
            get { return _PickItems; }
            set
            {
                _PickItems = value;
                OnPropertyChanged("PickItems");
            }
        }
        ObservableCollection<dynamic> pickItemsException = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> PickItemsException
        {
            get { return pickItemsException; }
            set
            {
                pickItemsException = value;
                //if (pickItemsException.Count == 0)
                //    IsEnableRemoveException = false;
                //if (isVoidStarted && pickItemsException.Count > 0)
                //{
                //    IsEnableRemoveException = true;
                //}
                OnPropertyChanged("PickItemsException");
            }
        }

        private void PickItemsException_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.IsEnableRemoveException = ((((System.Collections.ObjectModel.ObservableCollection<object>)(sender))).Count > 0);
        }
        List<dynamic> items = null;

        #endregion

        ReasonCodeVM _ReasonCodes = new ReasonCodeVM();
        public ReasonCodeVM ReasonCodes
        {
            get
            {
                return _ReasonCodes;
            }
            set
            {
                _ReasonCodes = value;
                OnPropertyChanged("ReasonCodes");
            }
        }

        bool _IsInVoidState = false;
        public bool IsInVoidState
        {
            get
            {
                return _IsInVoidState;
            }
            set
            {


                _IsInVoidState = value;
                OnPropertyChanged("IsInVoidState");
            }
        }

        private bool isTextBoxFocus = true;
        public bool IsTextBoxFocus
        {
            get { return isTextBoxFocus; }
            set { isTextBoxFocus = value; OnPropertyChanged("IsTextBoxFocus"); }
        }
        #endregion
        public CustomerReturnsPickViewModel()
        {
            //IsBusy = true;
            // Checks for customer alteration if user invoked past activity.
            if (PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity)
            {
                PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity = false;
                PayloadManager.ReturnOrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.ReturnOrderPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.ReturnOrderPayload.Customer);
            }
            InitializeCommands();
            ManualPickReasonList = new PickManager().GetReasonListForManualPick();
            UnHoldOrder();
            ReturnItems = ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnItems;
            //ReturnItems = new CustomerReturnsManager().GetItemsForPick();
            ReturnOrderNumber = ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString();
            PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
            PayloadManager.ReturnOrderPayload.PickIsInProgress = true;

            LoadPickList();
            EnableDisableIsAcceptAndPrint();
            //IsBusy = false;
            PickItemsException.CollectionChanged += PickItemsException_CollectionChanged;
        }

        void UnHoldOrder()
        {
            try
            {
                if (PayloadManager.ReturnOrderPayload.IsOnHold)
                {
                    PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                    #region Update Activity Count
                    string tempStopDate = string.Empty;
                    string stopID = string.Empty;
                    if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                    {
                        tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                        stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                    }
                    else
                    {
                        tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                        stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                    }
                    new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                    new CustomerManager().UpdateActivityCount(stopID, true, false);
                    PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                    PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                    new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
                    #endregion
                    PayloadManager.ReturnOrderPayload.IsOnHold = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel UnHoldOrder() error: " + ex.Message);

            }

        }

        #region Routines
        public void InitializeCommands()
        {
            NavigateToAcceptAndPrint = new DelegateCommand(param =>
            {
                ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnItems = ReturnItems;
                PayloadManager.ReturnOrderPayload.NavigateToAcceptAndPrint = true;
                //PrintAsync();
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.AcceptAndPrintReturns, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
            });

            ScanByDeviceCommand = new DelegateCommand(param =>
            {
                if (param == null) return;
                ReturnItem pickedROItem = param as ReturnItem;
                ScanByDeviceExecute(pickedROItem);

            });
            ScanByDeviceExceptionCommand = new DelegateCommand(param =>
            {
                if (param == null) return;


                ReturnItem pickedROItem = param as ReturnItem;
                ScanByDeviceExceptionExecute(pickedROItem);

            });
            ManualPickCommand = new DelegateCommand(param =>
            {
                //if (param == null)
                //    return;
                ManualPickExecute();
                //var pickManager = new PickManager();
                //manualPickReasonList = pickManager.GetReasonListForManualPick();

                //var dialog = new Helpers.DialogWindow { TemplateKey = "ReasonCodeDialog", Title = "Reason Code" };
                //Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            SelectionChanged = new DelegateCommand(param =>
            {
                pickcounter = 0;
                if (param == null)
                    return;
                SelectedReturnItem = param as ReturnItem;
                SelectedItemNumber = SelectedReturnItem.ItemNumber;
                SelectedPrimaryUoM = SelectedReturnItem.PrimaryUM;
                ManuallyPickCountOfSelected = SelectedReturnItem.ReturnQty;

            });
            UpdateOnManualCountCommand = new DelegateCommand(param =>
            {
                if (param == null)
                    return;
                UpdateOnManualCountExecute();
                //SelectedReturnItem.ManuallyPickCount = ManuallyPickCountOfSelected = Convert.ToInt32(param);
                //switch (SelectedReturnItem.ExceptionReasonCode.ToLower())
                //{
                //    case "overpicked":
                //        if ((ManuallyPickCountOfSelected != 0 && ManuallyPickCountOfSelected <= SelectedReturnItem.ExceptionCount))
                //        {
                //            SelectedReturnItem.IsManuallyPicking = true;
                //            if (IsManualCountStarted || pickcounter > 0)
                //                pickMgr.UnPickItem(SelectedReturnItem);
                //        }
                //        break;
                //    default:
                //        if ((ManuallyPickCountOfSelected != 0 && ManuallyPickCountOfSelected <= SelectedReturnItem.ReturnQty))
                //            SelectedReturnItem.IsManuallyPicking = true;
                //        if (IsManualCountStarted || pickcounter > 0)
                //            pickMgr.PickItem(SelectedReturnItem);
                //        break;
                //}
                //PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
                //ManuallyPickCountOfSelected = 0;
            });
            StartManualPick = new DelegateCommand(param =>
            {
                SelectedReturnItem.ReasonCodeId = Convert.ToInt32(param);
                IsManualCountStarted = true;

            });
            RemoveExceptions = new DelegateCommand(param =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Constants.PickOrder.RemoveExceptionConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, CommonNavInfo.MessageToken);
                if (!confirmMessage.Confirmed) return;

                IsExceptionGridEnabled = true;
                foreach (ReturnItem item in PickItemsException)
                {
                    item.IsEnableScannerInException = true;
                }
                //SelectedExceptionItem.IsEnableScannerInException = true;
                SelectedReturnItem = null;
                IsEnableManualPick = true;
                IsEnableRemoveException = false;
            });

            VoidPickList = new DelegateCommand(param =>
            {


                ReasonCodes.SelectedReasonCode = null;
                var ConfirmWindowDialog = new Helpers.ConfirmWindow { TemplateKey = "ReasonCodeListDialog", Context = ReasonCodes, Header = "Void Return Order" };
                Messenger.Default.Send<ConfirmWindow>(ConfirmWindowDialog, MessageToken);
                if (ConfirmWindowDialog.Confirmed && ReasonCodes.SelectedReasonCode != null)
                {
                    IsAcceptAndPrintEnabled = false;
                    // Check added to disable transition after voiding pick screen
                    CommonNavInfo.IsBackwardNavigationEnabled = false;
                    pickMgr.VoidPick(ReasonCodes.SelectedReasonCode.ID.ToString());
                    CR_Manager.UpdateInventory(PayloadManager.ReturnOrderPayload.ReturnItems, PayloadManager.ReturnOrderPayload.ReturnOrderID, ReturnOrderActions.Void);
                    IsInVoidState = true;
                    bool returnValue = new CustomerReturnsManager().VoidOrder(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ReasonCodes.SelectedReasonCode.ID.ToString(), ActivityKey.VoidAtROPick);
                    if (returnValue)
                    {
                        // INventory updation logic
                        CustomerReturnsManager ItemReturnsManager = new CustomerReturnsManager();
                        //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.VoidAtROPick);

                        IsVoidStarted = true;
                        IsEnableRemoveException = false;
                        IsEnableManualPick = true;
                        foreach (ReturnItem item in PickItemsException)
                        {
                            item.IsEnableScannerInException = true;
                        }
                        IsEnableRemoveException = PickItemsException.Count > 0 ? true : false;
                        //if (PickItemsException.Count() > 0)
                        //    IsEnableRemoveException = true;
                        if (PickItemsException.Count == 0)
                        {
                            PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                            var SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.ReturnsProcess;
                            ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);

                        }
                    }
                    else
                    {
                        //iF no item has been picked then just complete the activity and return.
                        //PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                        //CR_Manager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.VoidAtROPick);
                        ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                    }
                }
            });
            HoldPickList = new DelegateCommand(param =>
           {
               var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
               Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
               if (!confirmMessage.Confirmed) return;
               if (confirmMessage.Confirmed)
               {
                   PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                   // INventory updation logic
                   CustomerReturnsManager ItemReturnsManager = new CustomerReturnsManager();
                   //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.HoldAtROPick);

                   PayloadManager.ReturnOrderPayload.IsOnHold = true;
                   bool returnValue = new CustomerReturnsManager().HoldOrder(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ActivityKey.HoldAtROPick);
                   if (returnValue)
                   {
                       pickMgr.HoldPick(true);
                       #region Update Activity Count
                       string tempStopDate = string.Empty;
                       string stopID = string.Empty;
                       if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                       {
                           tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                           stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                       }
                       else
                       {
                           tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                           stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                       }
                       new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                       new CustomerManager().UpdateActivityCount(stopID, true, true);
                       PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                       PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                       new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);

                       ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                       var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                       Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                       #endregion
                   }
               }
           });

            ManualPickContinueCommand = new DelegateCommand((param) =>
            {
                if (param != null)
                {
                    ManualPickContinueExecute();
                }
            });
        }

        public void ScanByDeviceExecute(ReturnItem param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ScanByDeviceExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                dynamic itm1 = PickItems.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);

                param.PickAdjusted = 0;
                param.IsManuallyPicking = false;
                pickMgr.PickItem(param);

                foreach (ReturnItem item in PickItems)
                {
                    item.IsAllowManualPick = false;
                }
                foreach (ReturnItem item in PickItemsException)
                {
                    item.IsAllowManualPick = false;
                }
                SelectedReturnItem = param; SelectedExceptionItem = null;

                if (SelectedReturnItem != null)
                {
                    SelectedReturnItem.IsAllowManualPick = true;
                }
                IsEnableRemoveException = false;
                // ****
                IsDirty = false;
                pickcounter++;
                PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
                IsTextBoxFocus = true;
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel ScanByDeviceExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ScanByDeviceExecute]\t");



        }


        public void ScanByDeviceExceptionExecute(ReturnItem param)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ScanByDeviceExceptionExecute]\t" + DateTime.Now + "");

            try
            {
                param.ManuallyPickCount = 0; ManuallyPickCountOfSelected = 0;
                //You can not unpick item which is not present in exception list
                dynamic itm1 = PickItemsException.FirstOrDefault(i => i.ItemId == param.ItemId && i.TransactionDetailID == param.TransactionDetailID);
                if (itm1 == null)
                {
                    return;
                }
                param.IsManuallyPicking = false;
                pickMgr.UnPickItem(param);

                //foreach (ReturnItem item in PickItemsException)
                //{
                //    item.IsAllowManualPick = false;
                //}
                //foreach (ReturnItem item in PickItems)
                //{
                //    item.IsAllowManualPick = false;
                //}
                SelectedExceptionItem = param; SelectedReturnItem = null;
                SelectedExceptionItem.IsAllowManualPick = true;

                // ****
                IsDirty = false;
                SortPickedItems();
                IsTextBoxFocus = true; IsEnableRemoveException = false;

            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel ScanByDeviceExceptionExecute(" + param.ItemNumber + ") error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ScanByDeviceExceptionExecute]\t");



        }


        public void UpdateOnManualCountExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:UpdateOnManualCountExecute]\t" + DateTime.Now + "");
            try
            {
                if (SelectedReturnItem != null)
                {
                    if (SelectedReturnItem.IsAllowManualPick)
                    {
                        SelectedReturnItem.ManuallyPickCount = ManuallyPickCountOfSelected;
                        if (SelectedReturnItem.ManuallyPickCount == SelectedReturnItem.PickedQty)
                        {
                            return;
                        }
                        if (SelectedReturnItem.ManuallyPickCount > SelectedReturnItem.TransactionQty)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }
                        SelectedReturnItem.PickAdjusted = 0;
                        SelectedReturnItem.IsManuallyPicking = true;

                        pickMgr.PickItem(SelectedReturnItem);
                        PayloadManager.ReturnOrderPayload.PickIsInProgress = true;
                        //Enable Disable buttons
                        ManuallyPickCountOfSelected = 0;
                        foreach (ReturnItem item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }
                        foreach (ReturnItem item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                    }
                    else if (!string.IsNullOrEmpty(SelectedReturnItem.ItemId))
                    {
                        if (SelectedExceptionItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        }
                    }
                }

                if (SelectedExceptionItem != null)
                {
                    if (!SelectedExceptionItem.IsEnableScannerInException)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Click Remove Exception button to enable scanner.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    if (SelectedExceptionItem.IsAllowManualPick && SelectedExceptionItem.IsEnableScannerInException)
                    {
                        SelectedExceptionItem.ManuallyPickCount = ManuallyPickCountOfSelected;
                        if (SelectedExceptionItem.ManuallyPickCount == 0)
                        {
                            return;
                        }
                        if (SelectedExceptionItem.ExceptionCount < SelectedExceptionItem.ManuallyPickCount)
                        {
                            //SelectedExceptionItem.ManuallyPickCount = SelectedExceptionItem.ExceptionCount;
                            //ManuallyPickCountOfSelected = SelectedExceptionItem.ExceptionCount;
                            var alertMessage = new Helpers.AlertWindow { Message = "Manual quantity exceeds load quantity", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                        }

                        SelectedExceptionItem.IsManuallyPicking = true;
                        pickMgr.UnPickItem(SelectedExceptionItem);
                        PayloadManager.ReturnOrderPayload.PickIsInProgress = true;
                        ManuallyPickCountOfSelected = 0;
                        //Enable Disable buttons
                        foreach (ReturnItem item in PickItemsException)
                        {
                            item.IsAllowManualPick = false;
                        }
                        foreach (ReturnItem item in PickItems)
                        {
                            item.IsAllowManualPick = false;
                        }


                    }
                    else if (!string.IsNullOrEmpty(SelectedExceptionItem.ItemId))
                    {
                        if (SelectedReturnItem == null)
                        {
                            var alertMessage = new Helpers.AlertWindow { Message = "Select reason for manual pick.", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                        }

                    }
                }
                if (SelectedExceptionItem == null && SelectedReturnItem == null)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select Item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken); return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel UpdateOnManualCountExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:UpdateOnManualCountExecute]\t");

        }


        public void ManualPickExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ManualPickExecute]\t" + DateTime.Now + "");

            try
            {
                IsTextBoxFocus = true;
                bool showAlert = false;
                if (SelectedReturnItem != null)
                {
                    if (!string.IsNullOrEmpty(SelectedReturnItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;
                    }
                }
                else if (SelectedExceptionItem != null)
                {

                    if (!string.IsNullOrEmpty(SelectedExceptionItem.ItemId))
                    {
                        var dialog = new Helpers.DialogWindow { TemplateKey = "ManualPickReason", Title = "Reason Code", Payload = null };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                    }
                    else
                    {
                        showAlert = true;
                    }
                }
                else
                {
                    showAlert = true;
                }
                if (showAlert)
                {
                    var alertMessage = new Helpers.AlertWindow { Message = "Select item for manual pick.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel ManualPickExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ManualPickExecute]\t");
        }

        public void ManualPickContinueExecute()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:ManualPickContinueExecute]\t" + DateTime.Now + "");

            try
            {
                IsTextBoxFocus = true;

                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                if (SelectedReturnItem != null)
                {
                    SelectedReturnItem.IsAllowManualPick = true;
                }
                if (SelectedExceptionItem != null)
                {
                    SelectedExceptionItem.IsAllowManualPick = true; SelectedExceptionItem.IsEnableScannerInException = true;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel ManualPickContinueExecute() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:ManualPickContinueExecute]\t");

        }
        private void LoadPickList()
        {
            try
            {
                items = new System.Collections.Generic.List<dynamic>();
                foreach (var RO_Item in ReturnItems)
                {
                    RO_Item.TransactionID = Convert.ToInt32(ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString());
                    items.Add(RO_Item);
                }

                // Values are passed statically , after implementing next number pool code is to be replaced
                pickMgr = new PickItemManager(CommonNavInfo.RouteID.ToString(), ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), PickItemManager.PickForTransaction.ItemReturn, false);
                pickMgr.PickUpdate += pickMgr_PickUpdate;
                pickMgr.PickItemUpdate += pickMgr_PickItemUpdate;
                Dictionary<PickItemManager.PickType, ObservableCollection<dynamic>> pickList = pickMgr.CreatePickList(items);
                PickItems = pickList[PickItemManager.PickType.Normal];
                PickItemsException = pickList[PickItemManager.PickType.Exception];
                if (PayloadManager.ReturnOrderPayload.IsNavigatedForVoid)
                {
                    pickMgr.VoidPick(PayloadManager.ReturnOrderPayload.ReasonCodeID.ToString()); // TODO : Replace this hardcoded value from the one which was selected during void
                    IsAcceptAndPrintEnabled = false;
                    IsInVoidState = true;
                    IsVoidStarted = true;
                    PayloadManager.ReturnOrderPayload.IsNavigatedForVoid = false;
                    CommonNavInfo.IsBackwardNavigationEnabled = false;
                }
                LoadReferencedata();
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel LoadPickList() error: " + ex.Message);

            }

        }

        void pickMgr_PickItemUpdate(object sender, PickItemManager.PickItemEventArgs e)
        {
            EnableDisableIsAcceptAndPrint();
        }

        void pickMgr_PickUpdate(object sender, PickItemManager.PickEventArgs e)
        {
            if (e.PickComplete)
            {
                PayloadManager.ReturnOrderPayload.PickCompleted = true;
                if (PickItemsException != null && PickItemsException.Count() == 0)
                    IsAcceptAndPrintEnabled = true;
                try
                {
                    if (IsVoidStarted)
                    {
                        PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                        ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                        var SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.ReturnsProcess;
                        CommonNavInfo.IsBackwardNavigationEnabled = true;

                        System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                            Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);//CommonNavInfo.
                        }));
                    }
                }
                catch (Exception eex)
                {
                    throw;
                }
            }

        }

        void EnableDisableIsAcceptAndPrint()
        {
            //IsEnableRemoveException = PickItemsException.Count() > 0 ? true : false;
            if (PayloadManager.ReturnOrderPayload.IsNavigatedForVoid)
                IsAcceptAndPrintEnabled = false;
            else
                IsAcceptAndPrintEnabled = (PickItems.All(item => item.ExceptionReasonCode.ToString().ToLower() == "complete")) && !IsVoidStarted && PickItemsException.Count() == 0 ? true : false;

        }

        private void SortPickedItems()
        {
            //var sorted_items = new ObservableCollection<ReturnItem>();
            //foreach (var r_item in PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "underpicked"))
            //{
            //    sorted_items.Add(r_item);
            //}
            //foreach (var r_item in PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "overpicked"))
            //{
            //    sorted_items.Add(r_item);
            //}
            //foreach (var r_item in PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "completed"))
            //{
            //    sorted_items.Add(r_item);
            //}
            //PickItems = new ObservableCollection<dynamic>(sorted_items);
            //    PickItems = new ObservableCollection<dynamic>(PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "underPicked"));
            //    PickItems = new ObservableCollection<dynamic>(PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower() == "overPicked"));
            //    PickItems = new ObservableCollection<dynamic>(PickItems.Where(x => x.ExceptionReasonCode.ToString().ToLower()=="completed"));
            //PickItems = new ObservableCollection<dynamic>(PickItems.OrderByDescending(x => x.ExceptionReasonCodeID).ThenBy(x => x.ItemNumber));

            //ExceptionItems = new ObservableCollection<dynamic>(ExceptionItems.OrderByDescending(x => x.ExceptionReasonCodeID).ThenBy(x => x.ItemNumber));
        }

        public override bool ConfirmCancel()
        {
            return base.ConfirmCancel();
        }
        public override bool ConfirmSave()
        {
            //var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ReturnOrderPickScreen, CloseCurrentView = true, ShowAsModal = false };
            //Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);

            IsDirty = false;
            return base.ConfirmSave();
        }
        public override bool ConfirmClosed()
        {

            return base.ConfirmClosed();
        }

        private async void LoadReferencedata()
        {
            await Task.Run(() =>
            {
                foreach (ReturnItem r_Item in PickItems)
                {
                    if (ItemReturnList.All(x => x.ItemNumber.Trim() != r_Item.ItemNumber.Trim()))
                        ItemReturnList.Add(r_Item.Clone());
                }
            });
        }

        private void EnableDisableScannerForSelectedItem()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][Start:EnableDisableScannerForSelectedItem]\t" + DateTime.Now + "");

            try
            {
                //Disable scanner when available qty is 0 AND item is Short adjusted
                #region Enable disable scanner
                if (SelectedReturnItem.PrimaryUM == SelectedReturnItem.TransactionUOM)
                {
                    if (SelectedReturnItem.AvailableQty == 0)
                    {
                        SelectedReturnItem.IsAllowManualPick = false;
                        //SelectedReturnItem.IsEnableScanner = false;
                    }
                    else if (SelectedReturnItem.ExceptionReasonCode != "Short")
                    {
                        SelectedReturnItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                else if (SelectedReturnItem.PrimaryUM != SelectedReturnItem.TransactionUOM)
                {
                    if (SelectedReturnItem.AvailableQty < Convert.ToInt32(SelectedReturnItem.ConversionFactor) || (SelectedReturnItem.AvailableQty == 0) || SelectedReturnItem.ExceptionReasonCode == "Short")
                    {
                        SelectedReturnItem.IsAllowManualPick = false;
                        //IsEnableManualPick = false; 
                        SelectedReturnItem.IsEnableScanner = false;

                    }
                    else if (SelectedReturnItem.ExceptionReasonCode != "Short")
                    {
                        SelectedReturnItem.IsEnableScanner = true;
                        //IsEnableManualPick = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerReturnsPickViewModel EnableDisableScannerForSelectedItem() error: " + ex.Message);
            }

            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsPickViewModel][End:EnableDisableScannerForSelectedItem]\t");

        }


        #endregion
    }
}
