﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using Entities = SalesLogicExpress.Domain;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;
using System.Globalization;
using SalesLogicExpress.Application.Managers;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;


namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerHome : BaseViewModel, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerHome");

        CustomerContactsViewModel _NewContact = null;
        public CustomerContactsViewModel NewContact
        {
            get
            {
                return _NewContact;
            }
            set
            {
                _NewContact = value;
                OnPropertyChanged("NewContact");
            }
        }

        CustomerNotesViewModel _CustomerNote = null;
        public CustomerNotesViewModel CustomerNote
        {
            get
            {
                return _CustomerNote;
            }
            set
            {
                _CustomerNote = value;
                OnPropertyChanged("CustomerNote");
            }
        }

        CustomerQuoteTabViewModel _CustomerQuoteTabVM = null;
        public CustomerQuoteTabViewModel CustomerQuoteTabVM
        {
            get
            {
                return _CustomerQuoteTabVM;
            }
            set
            {
                _CustomerQuoteTabVM = value;
                OnPropertyChanged("CustomerQuoteTabVM");
            }
        }

        CustomerActivity _CustomerActivity = null;
        public CustomerActivity CustomerActivities
        {
            get
            {
                return _CustomerActivity;
            }
            set
            {
                _CustomerActivity = value;
                OnPropertyChanged("CustomerActivities");
            }
        }

        private CustomerInfo _customerInfo;
        public CustomerInfo customerInfo
        {
            get { return _customerInfo; }
            set { _customerInfo = value; OnPropertyChanged("customerInfo"); }
        }

        PricingViewModel pricingVm = null;
        public PricingViewModel PricingVm
        {
            get
            {
                return this.pricingVm;
            }
            set
            {
                this.pricingVm = value;
                OnPropertyChanged("PricingVm");
            }
        }

        CustomerDashboard customerDashboardVM = null;
        public CustomerDashboard CustomerDashboardVM
        {
            get
            {
                return this.customerDashboardVM;
            }
            set
            {
                this.customerDashboardVM = value;
                OnPropertyChanged("CustomerDashboardVM");
            }
        }

        ItemRestrictionViewModel itemRestrictionVM = null;
        public ItemRestrictionViewModel ItemRestrictionVM
        {
            get
            {
                return this.itemRestrictionVM;
            }
            set
            {
                this.itemRestrictionVM = value;
                OnPropertyChanged("ItemRestrictionVM");
            }
        }
        protected override void OnSyncProgressChanged(SyncManager.SyncUpdatedEventArgs e)
        {
            base.OnSyncProgressChanged(e);
        }

        bool isactivitytabselected = false, isnotestabselected = false, isinformationtabselected = false, iscontactstabselected = false, ispricingtabselected = false, isitemrestrictionstabselected = false, isdashboardtabselected = false;
        bool _IsQuoteTabSelected = false;
        public bool IsActivityTabSelected
        {
            get
            {
                return isactivitytabselected;
            }
            set
            {
                if (value && IsDirty)
                {
                    if (!ActiveTabVM.ShowConfirmDialog("Do you want to save changes?", ActiveTabVM.MessageToken).HasValue)
                        return;
                }
                if (!IsDirty)
                {
                    isactivitytabselected = value;
                    if (isactivitytabselected && CustomerActivities == null)
                    {
                        IsBusy = true;
                        CustomerActivities = new CustomerActivity(Customer.CustomerNo);
                        ReinitializeOrderPayload();
                        CustomerActivities.MessageToken = this.MessageToken;
                        IsBusy = false;
                    }
                }
                if (value)
                {
                    ActiveTabVM = CustomerActivities;
                }
                OnPropertyChanged("IsActivityTabSelected");
            }
        }
        public bool IsNotesTabSelected
        {
            get
            {
                return isnotestabselected;
            }
            set
            {
                if (value && IsDirty)
                {
                    if (!ActiveTabVM.ShowConfirmDialog("Do you want to save changes?", ActiveTabVM.MessageToken).HasValue)
                        return;
                }
                
                    isnotestabselected = value;
                    if (isnotestabselected && CustomerNote == null)
                    {
                        IsBusy = true;
                        CustomerNote = new CustomerNotesViewModel(Customer.CustomerNo);
                        CustomerNote.ParentView = this;
                        CustomerNote.MessageToken = this.MessageToken;
                        CustomerNote.ViewModelUpdated += CustomerNote_ViewModelUpdated;
                        IsBusy = false;
                    }
                    if (value)
                    {
                        ActiveTabVM = CustomerNote;
                    }
                
                
                OnPropertyChanged("IsNotesTabSelected");
            }
        }
        BaseViewModel ActiveTabVM = null;
        public bool IsInformationTabSelected
        {
            get
            {
                return isinformationtabselected;
            }
            set
            {
                isinformationtabselected = value;
                if (isinformationtabselected)
                {
                    IsBusy = true;
                    customerInfo = new CustomerInfo();
                    customerInfo.PropertyChanged += customerInfo_PropertyChanged;
                    customerInfo.MessageToken = this.MessageToken;
                    IsBusy = false;
                }
                if (value)
                {
                    ActiveTabVM = customerInfo;
                }
                OnPropertyChanged("IsInformationTabSelected");
            }
        }

        void customerInfo_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsDirty")
            {
                IsDirty = ((CustomerInfo)sender).IsDirty;
            }
        }
        public bool IsContactsTabSelected
        {
            get
            {
                return iscontactstabselected;
            }
            set
            {
                if (value && IsDirty)
                {
                    if (!ActiveTabVM.ShowConfirmDialog("Do you want to save changes?", ActiveTabVM.MessageToken).HasValue)
                        return;
                }
                iscontactstabselected = value;
                if (!IsDirty && iscontactstabselected && NewContact == null)
                {
                    IsBusy = true;
                    NewContact = new CustomerContactsViewModel(this.CustomerDashboardVM);
                    NewContact.MessageToken = this.MessageToken;
                    NewContact.ViewModelUpdated += NewContact_ViewModelUpdated;
                    IsBusy = false;
                }
                if (value)
                {
                    ActiveTabVM = NewContact;
                }
                OnPropertyChanged("IsContactsTabSelected");
            }
        }
        public bool IsPricingTabSelected
        {
            get
            {
                return ispricingtabselected;
            }
            set
            {
                if (value && IsDirty)
                {
                    if (!ActiveTabVM.ShowConfirmDialog("Do you want to save changes?", ActiveTabVM.MessageToken).HasValue)
                        return;
                }
                ispricingtabselected = value;
                if (ispricingtabselected && PricingVm == null)
                {
                    IsBusy = true;
                    PricingVm = new PricingViewModel(Customer.CustomerNo);
                    IsBusy = false;
                }
                if (value)
                {
                    ActiveTabVM = PricingVm;
                }
                OnPropertyChanged("IsPricingTabSelected");
            }
        }
        public bool IsItemRestrictionsTabSelected
        {
            get
            {
                return isitemrestrictionstabselected;
            }
            set
            {
                if (value && IsDirty)
                {
                    if (!ActiveTabVM.ShowConfirmDialog("Do you want to save changes?", ActiveTabVM.MessageToken).HasValue)
                        return;
                }
                isitemrestrictionstabselected = value;
                if (isitemrestrictionstabselected && ItemRestrictionVM == null)
                {
                    IsBusy = true;
                    ItemRestrictionVM = new ItemRestrictionViewModel(Customer.CustomerNo);
                    IsBusy = false;
                }
                if (value)
                {
                    ActiveTabVM = ItemRestrictionVM;
                }
                OnPropertyChanged("IsItemRestrictionsTabSelected");
            }
        }
        public bool IsDashboardTabSelected
        {
            get
            {
                return isdashboardtabselected;
            }
            set
            {
                if (value && IsDirty)
                {
                    if (!ActiveTabVM.ShowConfirmDialog("Do you want to save changes?", ActiveTabVM.MessageToken).HasValue)
                        return;
                }
                
                    isdashboardtabselected = value;
                    if (isdashboardtabselected && CustomerDashboardVM == null)
                    {
                        CustomerDashboardVM = new CustomerDashboard(Customer, SelectedDate);
                        CustomerDashboardVM.MessageToken = this.MessageToken;

                    }
                    if (value)
                    {
                        ActiveTabVM = CustomerDashboardVM;
                    }
                
                
                OnPropertyChanged("IsDashboardTabSelected");
                OnPropertyChanged("IsInformationTabSelected");
            }
        }
        public bool IsQuoteTabSelected
        {
            get
            {
                return _IsQuoteTabSelected;
            }
            set
            {
                if (value && IsDirty)
                {
                    if (!ActiveTabVM.ShowConfirmDialog("Do you want to save changes?", ActiveTabVM.MessageToken).HasValue)
                        return;
                }
                _IsQuoteTabSelected = value;

                if (IsQuoteTabSelected)
                {
                    IsBusy = true;
                    if (CustomerQuoteTabVM == null)
                    {

                        CustomerQuoteTabVM = new CustomerQuoteTabViewModel(this);
                        CustomerQuoteTabVM.MessageToken = this.MessageToken;
                    }
                    else
                    {
                        CustomerQuoteTabVM.QuoteList = (new CustomerQuoteManager()).GetQuoteSummaryList();
                    }
                    IsBusy = false;
                }
                if (value)
                {
                    ActiveTabVM = CustomerQuoteTabVM;
                }
                OnPropertyChanged("IsQuoteTabSelected");
                OnPropertyChanged("IsInformationTabSelected");
            }
        }

        #region Properties and Delegates

        public DelegateCommand OpenPreOrderDialog { get; set; }
        public DelegateCommand MoveToPreOrder { get; set; }
        public DelegateCommand ShowPaymentAR { get; set; }

        private DateTime _selectedDate = DateTime.Now;
        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                OnPropertyChanged("SelectedDate");
            }
        }

        private int _CheckSelectedIndex = 0;
        public int CheckSelectedIndex
        {
            get { return _CheckSelectedIndex; }
            set { _CheckSelectedIndex = value; OnPropertyChanged("CheckSelectedIndex"); }
        }
        private bool _IsButtonEnabled = true;
        public bool IsButtonEnabled
        {
            get { return _IsButtonEnabled; }
            set { _IsButtonEnabled = value; OnPropertyChanged("IsButtonEnabled"); }
        }
        public Models.Customer Customer
        {
            get;
            set;
        }
        private CustomerContact _CustomerContact;

        public Models.CustomerContact CustomerContact
        {
            get
            {
                return _CustomerContact;
            }
            set
            {
                _CustomerContact = value;
                OnPropertyChanged("CustomerContact");
            }
        }

        private bool _IsBusy;
        public bool IsBusy
        {
            get
            {
                return _IsBusy;
            }
            set
            {
                _IsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }

        Guid _MessageToken;
        public Guid MessageToken
        {

            get
            {
                return _MessageToken;
            }
            set
            {
                _MessageToken = value;
            }

        }
        bool _ShowContactPanel;
        public bool ShowContactPanel
        {
            get
            {
                return _ShowContactPanel;
            }
            set
            {
                _ShowContactPanel = value;
                OnPropertyChanged("ShowContactPanel");
            }
        }

        #endregion

        #region Constructor And Methods

        public TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        public CustomerHome(Guid token)
        {
            this.IsBusy = true;
            CommonNavInfo.IsBackwardNavigationEnabled = true;
            DateTime timer = DateTime.Now;
            try
            {
                IsDirty = false;

                if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.ServiceRoute_Dailystop.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.ServiceRoute.DailyStop;
                }
                else if (ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab == NavigatedFromTab.ServiceRoute_Customer.ToString())
                {
                    SelectedTab = ViewModelMappings.TabView.ServiceRoute.Customers;
                }
                this.MessageToken = token;
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerHome][Start:CustomerHomeViewModel]\t" + DateTime.Now + "");
                // Checks for customer alteration if user invoked past activity.
                if (PayloadManager.OrderPayload.IsCustomerLoadedForPastActivity)
                {
                    PayloadManager.OrderPayload.IsCustomerLoadedForPastActivity = false;
                    PayloadManager.OrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.OrderPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.OrderPayload.Customer);
                }
                Customer = PayloadManager.OrderPayload.Customer.DeepCopy();

                DateTime selectedDate = DateTime.Now;
                if (PayloadManager.OrderPayload.StopDate != null)
                {
                    SelectedDate = Convert.ToDateTime(PayloadManager.OrderPayload.StopDate);
                }
                InitializeCommands();
                ItemRestrictionVM = new ItemRestrictionViewModel(Customer.CustomerNo);
                CustomerDashboardVM = new CustomerDashboard(Customer, SelectedDate);
                CustomerDashboardVM.MessageToken = this.MessageToken;
                IsBusy = false;
            }
            catch (Exception ex)
            {
                Logger.Error("CustomerHome error:" + ex.Message + " StackTrace:" + ex.StackTrace);
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerHome][End:CustomerHomeViewModel]\t" + (DateTime.Now - timer) + "");
        }
        public string ConfirmationDialog()
        {
            string flag = string.Empty;
            var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.CustomerInformation.InfoConfirmAlert, OkButtonContent = "Yes", CancelButtonContent = "No", MessageIcon = "Alert", Confirmed = false };
            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
            if (confirmMessage.Confirmed)
            {
                flag = "confirm";
                customerInfo.SaveInformationOnTabChange();
            }
            if (confirmMessage.Closed)
            {
                flag = "closed";
            }
            if (confirmMessage.Cancelled)
            {
                flag = "cancelled";
            }
            return flag;
        }
        private static void ReinitializeOrderPayload()
        {
            Entities.Customer SelectedCustomer = PayloadManager.OrderPayload.Customer;

            DateTime SelectedCalendarDate = PayloadManager.OrderPayload.StopDate;

            PayloadManager.OrderPayload = null;
            PayloadManager.PaymentPayload = null;

            PayloadManager.OrderPayload.Customer = SelectedCustomer;
            PayloadManager.OrderPayload.StopDate = SelectedCalendarDate;
            PayloadManager.OrderPayload.StopID = SelectedCustomer.StopID;
            PayloadManager.OrderPayload.RouteID = PayloadManager.ApplicationPayload.Route;

            PayloadManager.PaymentPayload.Customer = SelectedCustomer;
            PayloadManager.PaymentPayload.RouteID = PayloadManager.ApplicationPayload.Route;
        }
        public void InitializeCommands()
        {
            #region OpenPreOrderDialog
            OpenPreOrderDialog = new DelegateCommand((param) =>
            {
                IsDashboardTabSelected = true;
                var dialog = new Helpers.DialogWindow { TemplateKey = "OpenPreOrderDialog", Title = "Create Pre-Order" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
            });
            #endregion

            #region MoveToPreOrder
            MoveToPreOrder = new DelegateCommand((param) =>
            {
                PreOrderStops POMD = param as PreOrderStops;
                PayloadManager.OrderPayload.Customer = Customer;
                PayloadManager.OrderPayload.PreOrderStop = POMD;
                PayloadManager.PreOrderPayload.PreOrderStops = POMD;
                PayloadManager.PreOrderPayload.Customer = Customer;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pre-Order";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.SetSelectedCustomer(Customer);
                ViewModelPayload.PayloadManager.ProspectPayload.NavigatedFromTab = NavigatedFromTab.CustomerHome_DashBoard.ToString();
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.PreOrder, CurrentViewName = ViewModelMappings.View.CustomerHome, CloseCurrentView = false, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
            });
            #endregion

            #region ItemRestrictionTabActive
            ItemRestrictionTabActive = new DelegateCommand((param) =>
            {
                IsItemRestrictionsTabSelected = true;
            });
            #endregion
        }
        async void LoadDashboard()
        {
            await Task.Run(() =>
            {
                IsBusy = false;
            });
        }
        async void LoadData(DateTime selectedDate)
        {
            await Task.Run(() =>
            {
                try
                {
                    IsDashboardTabSelected = true;
                    CustomerNote.MessageToken = this.MessageToken;
                    IsBusy = false;
                }
                catch (Exception ex)
                {
                    IsBusy = false;
                    Logger.Error("CustomerHome LoadData(" + selectedDate + ") error: " + ex.Message);
                }
            });
        }
        void NewContact_ViewModelUpdated(object sender, ContactVMArgs e)
        {
            CustomerDashboardVM.SetDefaultContact(e.custContact);
        }
        void CustomerNote_ViewModelUpdated(object sender, NotesVMArgs e)
        {
            CustomerDashboardVM.SetDefaultNote(e.Note);
        }
        public override bool ConfirmCancel()
        {
            IsDirty = false;
            base.CanNavigate = true;
            return base.ConfirmCancel();
        }
        public override bool ConfirmSave()
        {
            IsDirty = false;
            customerInfo.SaveInformation();
            return base.ConfirmSave();
        }
        #endregion
        public string Error
        {
            get { return ""; }
        }
        public string this[string columnName]
        {
            get
            {
                if (columnName == "CheckSelectedIndex" && !(CheckSelectedIndex == -1))
                {
                    IsButtonEnabled = true;
                    return "";
                }
                else
                {
                    IsButtonEnabled = false;
                }
                return "";
            }
        }
        public DelegateCommand ItemRestrictionTabActive { get; set; }
    }
}
