﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class RequestAuthCodeViewModel : ViewModelBase
    {
        public readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #region Event Support for AuthCodeChange
        public event EventHandler<AuthCodeChangedEventArgs> AuthCodeChanged;
        protected virtual void OnAuthCodeChange(AuthCodeChangedEventArgs e)
        {
            EventHandler<AuthCodeChangedEventArgs> handler = AuthCodeChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class AuthCodeChangedEventArgs : EventArgs
        {
            public AuthCodeChangedEventArgs()
            {
                StateChangedTime = DateTime.SpecifyKind(StateChangedTime, DateTimeKind.Utc);
            }
            public bool IsValid { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        #endregion

        #region Properties
        string _RequestCode;
        public string RequestCode
        {
            get
            {
                return _RequestCode;
            }
            set
            {
                _RequestCode = value;
                OnPropertyChanged("RequestCode");
            }
        }

        string _AuthCode;
        public string Authcode
        {
            get
            {
                return _AuthCode;
            }
            set
            {
                _AuthCode = value;
                OnPropertyChanged("Authcode");
            }
        }


        bool _IsAuthCodeValid = false;
        public bool IsAuthCodeValid
        {
            get
            {
                return _IsAuthCodeValid;
            }
            set
            {


                _IsAuthCodeValid = value;
                OnPropertyChanged("IsAuthCodeValid");
            }
        }
        #endregion
        public RequestAuthCodeParams Params { get; set; }
        public DelegateCommand VerifyAuthCode { get; set; }
        AuthCodeManager authCodeManager = new AuthCodeManager();
        public RequestAuthCodeViewModel(RequestAuthCodeParams Parameters)
        {
            Params = Parameters;
            RequestCode = new AuthCodeManager().GetRequestCode(Parameters.Feature, Parameters.Amount, Parameters.UserID);
            VerifyAuthCode = new DelegateCommand((param) =>
            {
                IsAuthCodeValid = authCodeManager.VerifyAuthCode(RequestCode, Authcode);
                AuthCodeChangedEventArgs args = new AuthCodeChangedEventArgs();
                args.IsValid = IsAuthCodeValid;
                args.StateChangedTime = DateTime.Now;
                OnAuthCodeChange(args);

            });
        }
        public void LogAuthCode(string UserID, string RequestCode, string AuthCode, string RouteID, string Payload, string CustomerID, string Amount, string Feature)
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RequestAuthCodeViewModel][Start:LogAuthCode]");
            try
            {
                if (IsAuthCodeValid)
                {
                    AuthCodeManager.Log log = new AuthCodeManager.Log
                    {
                        Payload = Payload,
                        ApprovalCode = AuthCode,
                        RouteID = RouteID,
                        RequestCode = RequestCode,
                        RequestedByUser = UserID,
                        RequestedForCustomer = CustomerID == null ? "-1" : CustomerID,
                        Amount = Amount,
                        Feature = Feature
                    };
                    authCodeManager.LogAuthCode(log);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][RequestAuthCodeViewModel][LogAuthCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][RequestAuthCodeViewModel][End:LogAuthCode]");
        }
        public class RequestAuthCodeParams
        {
            public decimal Amount { get; set; }
            public string UserID { get; set; }
            public AuthCodeManager.Feature Feature { get; set; }
        }
    }
}
