﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModelPayload;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ReportViewModels;
using System.IO;
using System.Windows.Ink;
namespace SalesLogicExpress.Application.ViewModels
{
    /// <summary>
    /// ViewModel for ItemReturns screen and Accept and Print Screen
    /// </summary>
    public class ItemReturnScreenVM : BaseViewModel
    {
        private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ItemReturnScreenVM");
        CustomerReturnsManager ItemReturnsManager = new CustomerReturnsManager();
        #region DelegateCommands
        public DelegateCommand CompleteAndSign { get; set; }
        public DelegateCommand AcceptReturnOrder { get; set; }
        public DelegateCommand SaveReturnOrder { get; set; }

        public DelegateCommand HoldReturnOrder { get; set; }
        public DelegateCommand VoidReturnOrder { get; set; }
        public DelegateCommand ProceedToAppend { get; set; }
        public DelegateCommand ContinueReturnOrder { get; set; }
        public DelegateCommand HoldAtROPrint { get; set; }
        #endregion

        #region Prop's
        string strDateTime = DateTime.Now.ToString("M'/'dd'/'yyyy' - 'hh:mm ") + DateTime.Now.ToString("tt").ToLower();
        public string CurrentDate
        {
            get
            {
                return strDateTime;
            }
        }

        ReasonCodeVM _ReasonCodes = new ReasonCodeVM();
        public ReasonCodeVM ReasonCodes
        {
            get
            {
                return _ReasonCodes;
            }
            set
            {
                _ReasonCodes = value;
                OnPropertyChanged("ReasonCodes");
            }
        }
        private string customerID;
        public string CustomerID
        {
            get { return customerID; }
            set { customerID = value; OnPropertyChanged("CustomerID"); }
        }

        private string returnOrderID;
        public string ReturnOrderID
        {
            get { return returnOrderID; }
            set { returnOrderID = value; OnPropertyChanged("ReturnOrderID"); }
        }


        bool isCompleteAndSign = true;
        public bool IsCompleteAndSign
        {
            get
            {
                return isCompleteAndSign;
            }
            set
            {
                isCompleteAndSign = value;
                OnPropertyChanged("IsCompleteAndSign");
            }
        }

        public string OrderID { get; set; }
        TrulyObservableCollection<ReturnItem> returnItems = new TrulyObservableCollection<ReturnItem>();

        bool _IsAcceptEnable = true;
        public bool IsAcceptButtonEnabled
        {
            get
            {
                return _IsAcceptEnable;
            }
            set
            {


                _IsAcceptEnable = value;
                OnPropertyChanged("IsAcceptButtonEnabled");
            }
        }


        bool roCreated;
        public bool ROCreated
        {
            get
            {
                return roCreated;
            }
            set
            {


                roCreated = value;
                OnPropertyChanged("ROCreated");
            }
        }
        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get { return returnItems; }
            set { returnItems = value; OnPropertyChanged("ReturnItems"); }
        }

        public Guid MessageToken { get; set; }
        private bool _ToggleSignPanelVisibility = true;
        public bool ToggleSignPanelVisibility
        {
            get
            {
                return _ToggleSignPanelVisibility;

            }
            set
            {
                _ToggleSignPanelVisibility = value;
                OnPropertyChanged("ToggleSignPanelVisibility");
            }
        }
        private bool _ToggleCompleteAndSign = false;
        public bool ToggleCompleteAndSign
        {
            get
            {
                return _ToggleCompleteAndSign;

            }
            set
            {
                _ToggleCompleteAndSign = value;
                OnPropertyChanged("ToggleCompleteAndSign");
            }
        }

        CanvasViewModel _SignatureCanvasVM;
        public CanvasViewModel SignatureCanvasVM
        {
            get
            {
                return _SignatureCanvasVM;
            }
            set
            {
                _SignatureCanvasVM = value;
                OnPropertyChanged("SignatureCanvasVM");
            }
        }

        bool isOrderReturn;
        public bool IsOrderReturn
        {
            get
            {
                return isOrderReturn;
            }
            set
            {


                isOrderReturn = value;
                OnPropertyChanged("IsOrderReturn");
            }
        }
        # region Properties For Textbox Values

        float varTotalCoffeeValue, varTotalAlliedValue, varOrderTotalValue, varEnergySurchargeValue, varSalesTaxValue, varInvoiceTotalValue;
        private float _TotalCoffeeValue = 0.00f, _TotalAlliedValue = 0.00f,
                       _OrderTotalValue = 0.00f, _EnergySurchargeValue = 0.00f,
                       _SalesTaxValue = 0.00f, _InvoiceTotalValue = 0.00f;
        public float TotalCoffeeValue
        {
            get
            {
                return _TotalCoffeeValue;
            }
            set
            {
                _TotalCoffeeValue = value;
                OnPropertyChanged("TotalCoffeeValue");
            }
        }

        public float TotalAlliedValue
        {
            get { return _TotalAlliedValue; }
            set { _TotalAlliedValue = value; OnPropertyChanged("TotalAlliedValue"); }
        }

        public float OrderTotalValue
        {
            get { return _OrderTotalValue; }
            set
            {
                _OrderTotalValue = value;
                OnPropertyChanged("OrderTotalValue");
            }
        }

        public float EnergySurchargeValue
        {
            get { return _EnergySurchargeValue; }
            set
            {
                _EnergySurchargeValue = value;
                OnPropertyChanged("EnergySurchargeValue");
            }
        }
        public float SalesTaxValue
        {
            get { return _SalesTaxValue; }
            set { _SalesTaxValue = value; OnPropertyChanged("SalesTaxValue"); }
        }
        public float InvoiceTotalValue
        {
            get { return _InvoiceTotalValue; }
            set { _InvoiceTotalValue = value; OnPropertyChanged("InvoiceTotalValue"); }
        }

        #endregion
        #endregion

        public ItemReturnScreenVM()
        {
            SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.OrdersProcess;
            InitializeData();
            InitializeCommand();
            PopulateTotalFields();
        }

        private void InitializeData()
        {
            // Checks for customer alteration if user invoked past activity.
            if (PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity)
            {
                PayloadManager.ReturnOrderPayload.IsCustomerLoadedForPastActivity = false;
                PayloadManager.ReturnOrderPayload.Customer = new CustomerDashboard().GetNewCustomer(PayloadManager.ReturnOrderPayload.StopDateBeforeInvokingActivity.Value.Date, PayloadManager.ReturnOrderPayload.Customer);
            }
            CustomerID = PayloadManager.ReturnOrderPayload.Customer.CustomerNo;
            ReturnOrderID = ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString();
            ReturnItems = PayloadManager.ReturnOrderPayload.ReturnItems;
            // Set referrence collection to detect RO change
            if (PayloadManager.ReturnOrderPayload.RefReturnItems.Count() == 0)
                PayloadManager.ReturnOrderPayload.RefReturnItems = PayloadManager.ReturnOrderPayload.ReturnItems.Clone();

            ReturnItems.ItemPropertyChanged += ReturnItems_ItemPropertyChanged;

            ToggleCompleteAndSign = ReturnItems.Count(item => item.ReturnQty > 0) > 0 ? true : false;

            base.CanNavigate = !PayloadManager.ReturnOrderPayload.NavigateToAcceptAndPrint;

            UnHoldOrder();
            CreateReturnOrder(PayloadManager.ReturnOrderPayload.ReturnItems);

            if (PayloadManager.ReturnOrderPayload.IsReturnOrder)
                IsDirty = true;

            // Check is to show popup to add any more items to existing return items, while pick is in progess
            if (!PayloadManager.ReturnOrderPayload.IsReturnOrder && PayloadManager.ReturnOrderPayload.PickIsInProgress)
                IsDirty = true;

            //if (PayloadManager.ReturnOrderPayload.NavigateToAcceptAndPrint)
            //{
            //    bool IsSignPresent = new Managers.SignatureManager().GetSignature(Helpers.Flow.OrderReturn, Helpers.SignType.ReturnOrderSign, Convert.ToInt32(ReturnOrderID)).Count == 0 ? false : true;
            //}
            RenderSign();
            IsAcceptButtonEnabled = SignatureCanvasVM.CanvasStrokeCollection.Count() > 0 ? true : false;
            SignatureCanvasVM.CanvasChanged += SignatureCanvasVM_CanvasChanged;

            if (PayloadManager.ReturnOrderPayload.IsReturnOrder)
            {
                IsOrderReturn = PayloadManager.ReturnOrderPayload.IsReturnOrder;
                OrderID = ReturnItems[0].OrderID.ToString();

                foreach (var r_item in ReturnItems)
                {
                    r_item.ReturnQty = r_item.OrderQty;
                }
            }
            else
                IsOrderReturn = false;
            if (ReturnOrderID.Equals("-1"))
            {
                PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
            }
            //Check whether order is on hold or not. If navigated from activity then it is on hold, If it is then you can edit order.
            if (ViewModelPayload.PayloadManager.ReturnOrderPayload.IsFromActivityTab)
            {
                PayloadManager.ApplicationPayload.StartActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                ViewModelPayload.PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
                ViewModelPayload.PayloadManager.ReturnOrderPayload.IsOnHold = true;
                ReturnItems.ToList().ForEach(x => { x.IsCompleteAndSignPressed = true; });
                ViewModelPayload.PayloadManager.ReturnOrderPayload.IsFromActivityTab = false;
            }
            else
            {
                if (PayloadManager.ReturnOrderPayload.PickIsInProgress)
                    ViewModelPayload.PayloadManager.ReturnOrderPayload.ROIsInProgress = true;

            }
            ReturnItems.ToList().ForEach(x => { x.IsCompleteAndSignPressed = true; });

        }
        public void RenderSign()
        {
            SignatureCanvasVM = new CanvasViewModel(Flow.OrderReturn, SignType.ReturnOrderSign, PayloadManager.ReturnOrderPayload.ReturnOrderID)
            {
                Height = 250,
                Width = 300,
                FileName = "ReturnOrder.jpg",
                FooterText = "Sign here"
            };
        }

        private bool CreateReturnOrder(ObservableCollection<ReturnItem> returnItems)
        {
            if (!PayloadManager.ReturnOrderPayload.NavigateToAcceptAndPrint)
                if (ItemReturnsManager.CheckROExists() == 0) // Check for existing order if not creates it.
                {
                    PayloadManager.ReturnOrderPayload.ReturnOrderID = PayloadManager.ReturnOrderPayload.ReturnOrderID == -1 || PayloadManager.ReturnOrderPayload.ReturnOrderID == 0 ? ItemReturnsManager.GetRONumber() : PayloadManager.ReturnOrderPayload.ReturnOrderID;
                    bool flag = ItemReturnsManager.CreateReturnOrder(returnItems, ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString());
                    //SignatureCanvasVM.UpdateSignatureToDB(false, false, true, "ReturnOrder.jpg");
                    return flag;
                }
                else // 
                {
                    //ItemReturnsManager.CleanROItems();
                    //var RO_Items = ItemReturnsManager.GetROItems();
                    //foreach (var item in RO_Items) // If held order is modified then update committed qty according to difference.
                    //{
                    //    UoMConversion(item);
                    //    //ItemReturnsManager.UpdateCommitted(item.ReturnQty, item.ItemId);
                    //}
                    ////ItemReturnsManager.UpdateCommitted(updateHold: true); // Used to update only HoldCommitted flag.
                    return ItemReturnsManager.CreateReturnOrder(returnItems, ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString());
                }
            return false;
        }

        void UnHoldOrder()
        {
            if (PayloadManager.ReturnOrderPayload.IsOnHold)
            {
                #region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                }
                new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new CustomerManager().UpdateActivityCount(stopID, true, false);
                PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
                #endregion
                PayloadManager.ReturnOrderPayload.IsOnHold = false;
            }
        }

        void SignatureCanvasVM_CanvasChanged(object sender, CanvasViewModel.CanvasUpdatedEventArgs e)
        {
            IsAcceptButtonEnabled = SignatureCanvasVM.CanvasStrokeCollection.Count() > 0 ? true : false;
        }



        void ReturnItems_ItemPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("ReturnQty"))
            {
                ToggleCompleteAndSign = ReturnItems.Count(item => item.ReturnQty > 0) > 0 ? true : false;
                varOrderTotalValue = varTotalCoffeeValue = varTotalAlliedValue = 0.00f;
                PopulateTotalFields();
                if (!IsOrderReturn)
                    IsDirty = true;
            }
        }

        #region Methods
        private void PopulateTotalFields()
        {
            if (ReturnItems.Count > 0)
            {
                varOrderTotalValue = varTotalCoffeeValue = varTotalAlliedValue = varSalesTaxValue = 0.00f;

                foreach (ReturnItem item in ReturnItems)
                {
                    if (item.SalesCat1.Equals("COF"))
                    {
                        varTotalCoffeeValue += (float)((item.UnitPrice * item.ReturnQty));//+ (item.TaxAmount / item.OrderQty));
                    }
                    if (item.SalesCat1.Equals("ALL"))
                    {
                        varTotalAlliedValue += (float)((item.UnitPrice * item.ReturnQty));//+ (item.TaxAmount / item.OrderQty));
                    }

                    varSalesTaxValue += ((float)item.TaxAmount / item.OrderQty) * item.ReturnQty; // tax for each item * return qty
                    item.ROTaxAmount = ((decimal)item.TaxAmount / item.OrderQty) * item.ReturnQty;
                    item.ROExtendedPrice = (item.UnitPrice * item.ReturnQty);
                    EnergySurchargeValue = (float)item.EnergySurchargeAmt;
                }
                TotalCoffeeValue = varTotalCoffeeValue;
                TotalAlliedValue = varTotalAlliedValue;
                SalesTaxValue = varSalesTaxValue;

                OrderTotalValue = varOrderTotalValue = varTotalAlliedValue + varTotalCoffeeValue;
                InvoiceTotalValue = varInvoiceTotalValue = (varOrderTotalValue + (PayloadManager.ReturnOrderPayload.IsReturnOrder ? EnergySurchargeValue : 0) + varSalesTaxValue);

                PayloadManager.ReturnOrderPayload.TotalCoffeeAmount = Convert.ToDecimal(TotalCoffeeValue);
                PayloadManager.ReturnOrderPayload.TotalAlliedAmount = Convert.ToDecimal(TotalAlliedValue);
                PayloadManager.ReturnOrderPayload.OrderTotalAmount = Convert.ToDecimal(OrderTotalValue);
                PayloadManager.ReturnOrderPayload.ROSalesTax = SalesTaxValue;
                PayloadManager.ReturnOrderPayload.InvoiceTotalAmt = Convert.ToDecimal(InvoiceTotalValue);
                PayloadManager.ReturnOrderPayload.EnergySurcharge = Convert.ToDecimal(EnergySurchargeValue);

            }
        }

        private void InitializeCommand()
        {
            CompleteAndSign = new DelegateCommand((param) =>
            {
                //CreateReturnOrder(); ** call removed as now RO ID is get generated in ctor

                foreach (var item in ReturnItems)
                {
                    if (!string.Equals(item.UM, item.PrimaryUM, StringComparison.CurrentCultureIgnoreCase))
                    {
                        UoMConversion(item);
                    }
                    else
                    {
                        item.ReturnQtyInPrimaryUoM = item.ReturnQty;
                        item.NonSellableQtyInPrimaryUoM = item.NonSellableQty;

                    }
                }

                //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.CreateReturnOrder);
                if (!ToggleSignPanelVisibility)
                    ToggleSignPanelVisibility = true;
                else
                {
                    ToggleSignPanelVisibility = false;
                    ToggleCompleteAndSign = false;
                }

                if (IsOrderReturn)
                    ContinueReturnOrder.Execute(null);
                else
                {

                    if (!ReturnItems.All(x => x.OrderQty == x.ReturnQty))
                    {

                        var dialog = new Helpers.DialogWindow { TemplateKey = "AppendReturnItems", Title = "Warning", Payload = this };
                        Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

                    }
                    else
                        ContinueReturnOrder.Execute(null);
                }
                foreach (ReturnItem item in ReturnItems)
                {
                    item.IsCompleteAndSignPressed = false;
                }
                IsCompleteAndSign = false;

                // checks for the item return changes to clear the sign canvas
                if (PayloadManager.ReturnOrderPayload.ReturnItems.Count() == PayloadManager.ReturnOrderPayload.RefReturnItems.Count())
                {
                    foreach (var rItem in PayloadManager.ReturnOrderPayload.ReturnItems)
                    {
                        var refitem = PayloadManager.ReturnOrderPayload.RefReturnItems.FirstOrDefault(x => (x.OrderID == rItem.OrderID && x.ItemNumber == rItem.ItemNumber));
                        if (refitem != null)
                        {
                            if ((rItem.ReturnQty != refitem.ReturnQty) || (rItem.NonSellableQtyInPrimaryUoM != refitem.NonSellableQtyInPrimaryUoM))
                            {
                                PayloadManager.ReturnOrderPayload.IsROModified = true;
                            }
                        }
                        else // this states that the order item count is same but items are different
                        {
                            PayloadManager.ReturnOrderPayload.IsROModified = true;
                        }
                    }
                }
                else
                    PayloadManager.ReturnOrderPayload.IsROModified = true;

                if (PayloadManager.ReturnOrderPayload.IsROModified)
                {
                    SignatureCanvasVM.ClearCanvas.Execute(null);
                    PayloadManager.ReturnOrderPayload.IsROModified = false;
                }

            });
            AcceptReturnOrder = new DelegateCommand(param =>
            {
                foreach (ReturnItem item in ReturnItems)
                {
                    item.TransactionQty = item.ReturnQty;
                    item.TransactionUOM = item.UM;
                }


                PayloadManager.ReturnOrderPayload.ReturnItems = new TrulyObservableCollection<ReturnItem>();
                PayloadManager.ReturnOrderPayload.RefReturnItems = new TrulyObservableCollection<ReturnItem>();
                foreach (var r_item in ReturnItems.Where(x => x.ReturnQty != 0))
                {
                    PayloadManager.ReturnOrderPayload.ReturnItems.Add(r_item as ReturnItem);
                    PayloadManager.ReturnOrderPayload.RefReturnItems.Add(r_item as ReturnItem);
                }
                SignatureCanvasVM.UpdateSignatureToDB(false, false, true, "returnOrder.jpg");

                PopulateTotalFields();

                ItemReturnsManager.UpdateInventory(PayloadManager.ReturnOrderPayload.ReturnItems, PayloadManager.ReturnOrderPayload.ReturnOrderID, ReturnOrderActions.Accept);
                CreateReturnOrder(PayloadManager.ReturnOrderPayload.ReturnItems);

                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnOrderPickScreen, CurrentViewName = ViewModelMappings.View.ItemReturnsScreen, CloseCurrentView = true, ShowAsModal = false };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
            });

            SaveReturnOrder = new DelegateCommand(param =>
            {
                if (ReturnItems.Count == 0)
                {
                    return;
                }
                //string OrderId = ReturnItems.ElementAt(0).OrderID.ToString();

                if (ItemReturnsManager.SaveReturnOrder(ReturnItems, ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString()))
                {
                    PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                    IsAcceptButtonEnabled = false;
                    SignatureCanvasVM.UpdateSignatureToDB(false, false, true, "returnOrder.jpg");

                    this.CreateInvoiceAndReceipt(ViewModelPayload.PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), (decimal)InvoiceTotalValue);

                    SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.ReturnsProcess;

                    PrintAsync();
                    // INventory updation logic
                    //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.OrderReturned);
                    ItemReturnsManager.UpdateInventory(PayloadManager.ReturnOrderPayload.ReturnItems, PayloadManager.ReturnOrderPayload.ReturnOrderID, ReturnOrderActions.OrderReturned);


                    ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();

                    var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.AcceptAndPrintReturns, CloseCurrentView = true, ShowAsModal = false, SelectTab = SelectedTab };
                    Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                }
            });

            HoldReturnOrder = new DelegateCommand(param =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                if (confirmMessage.Confirmed)
                {
                    PayloadManager.ReturnOrderPayload.IsOnHold = true;
                    //CreateReturnOrder(); ** call removed as now RO ID is get generated in ctor
                    HoldRO();
                }
            });
            VoidReturnOrder = new DelegateCommand(param =>
            {
                ReasonCodes.SelectedReasonCode = null;
                var ConfirmWindowDialog = new Helpers.ConfirmWindow { TemplateKey = "ReasonCodeListDialog", Context = ReasonCodes, Header = "Void Return Order" };
                Messenger.Default.Send<ConfirmWindow>(ConfirmWindowDialog, MessageToken);
                if (ConfirmWindowDialog.Confirmed && ReasonCodes.SelectedReasonCode != null)
                {
                    //CreateReturnOrder(); ** call removed as now RO ID is get generated in ctor
                    VoidRO();
                    //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.VoidAtROEntry);

                }

            });
            ProceedToAppend = new DelegateCommand(param =>
            {
                PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
                SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.OrdersProcess;
                PayloadManager.ReturnOrderPayload.ReturnItems = ReturnItems;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ItemReturnsScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true, SelectTab = SelectedTab };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
            });
            ContinueReturnOrder = new DelegateCommand(param =>
            {

                //if (!ToggleSignPanelVisibility)
                //    ToggleSignPanelVisibility = true;
                //else
                //{
                //    ToggleSignPanelVisibility = false;
                //    ToggleCompleteAndSign = false;
                //}

            });
            HoldAtROPrint = new DelegateCommand(param =>
            {
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Common.HoldOrderConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;
                if (confirmMessage.Confirmed)
                {
                    PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                    PayloadManager.ReturnOrderPayload.IsOnHold = true;
                    bool returnValue = ItemReturnsManager.HoldOrder(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ActivityKey.HoldAtROPrint);
                    if (returnValue)
                    {
                        #region Update Activity Count
                        string tempStopDate = string.Empty;
                        string stopID = string.Empty;
                        if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                        {
                            tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                            stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                        }
                        else
                        {
                            tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                            stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                        }
                        new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                        new CustomerManager().UpdateActivityCount(stopID, true, true);
                        PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                        PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                        new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
                        #endregion
                        // INventory updation logic
                        //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.HoldAtROPrint);

                        ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                        SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.OrdersProcess;
                        var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.AcceptAndPrintReturns, CloseCurrentView = true, ShowAsModal = false, SelectTab = SelectedTab };
                        Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                    }
                }
            });
        }

        private void VoidRO()
        {
            if (ReasonCodes.SelectedReasonCode == null)
            {
                return;
            }
            ItemReturnsManager.UpdateInventory(PayloadManager.ReturnOrderPayload.ReturnItems, PayloadManager.ReturnOrderPayload.ReturnOrderID, ReturnOrderActions.Void);

            bool returnValue = ItemReturnsManager.VoidOrder(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ReasonCodes.SelectedReasonCode.ID.ToString(), ActivityKey.VoidAtROEntry);

            if (ItemReturnsManager.IsROPicked(PayloadManager.ReturnOrderPayload.ReturnOrderID))
            {
                PayloadManager.ReturnOrderPayload.IsNavigatedForVoid = true;
                PayloadManager.ReturnOrderPayload.ReasonCodeID = ReasonCodes.SelectedReasonCode.ID;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnOrderPickScreen, CurrentViewName = ViewModelMappings.View.ItemReturnsScreen, CloseCurrentView = true, ShowAsModal = false, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
                return;
            }
            //iF no pick has happened then just complete the activity and return.
            PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
            if (returnValue)
            {
                // INventory updation logic
                //ItemReturnsManager.UpdateInventory(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID, ActivityKey.VoidAtROEntry);

                SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.OrdersProcess;
                ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ItemReturnsScreen, CloseCurrentView = true, ShowAsModal = false, SelectTab = SelectedTab, Refresh = true };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);

            }
        }

        private void HoldRO()
        {

            bool returnValue = ItemReturnsManager.HoldOrder(ReturnItems.Clone(), PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString(), ActivityKey.HoldAtROEntry);
            if (returnValue)
            {
                PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.ReturnOrder);
                //Update Order Detail for hold items
                PayloadManager.ReturnOrderPayload.NavigateToAcceptAndPrint = false;
                //CreateReturnOrder();

                #region Update Activity Count
                string tempStopDate = string.Empty;
                string stopID = string.Empty;
                if (PayloadManager.ReturnOrderPayload.StopDate.Date > DateTime.Now.Date)
                {
                    tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.ReturnOrderPayload.Customer.CustomerNo, tempStopDate);
                }
                else
                {
                    tempStopDate = PayloadManager.ReturnOrderPayload.Customer.StopDate.Value.Date.ToString("yyyy-MM-dd");
                    stopID = PayloadManager.ReturnOrderPayload.Customer.StopID;
                }
                new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                new CustomerManager().UpdateActivityCount(stopID, true, true);
                PayloadManager.ReturnOrderPayload.Customer.SaleStatus = "";
                PayloadManager.ReturnOrderPayload.Customer.PendingActivity = new Managers.CustomerManager().GetPendingAtivityForStop(stopID);
                new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.ReturnOrderPayload.Customer.CustomerNo);
                #endregion

                ViewModelPayload.PayloadManager.ReturnOrderPayload.Reset();
                SelectedTab = ViewModelMappings.TabView.ReturnsAndCredits.OrdersProcess;
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ItemReturnsScreen, CloseCurrentView = true, ShowAsModal = false, SelectTab = SelectedTab };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
            }
        }

        private static void UoMConversion(ReturnItem item)
        {
            var factor = 0.0m;
            if (UoMManager.ItemUoMFactorList != null)
            {
                var itemUomConversion = UoMManager.ItemUoMFactorList.FirstOrDefault(x => (x.ItemID == item.ItemId.Trim()) && (x.FromUOM == item.UM.ToString()) && (x.ToUOM == item.PrimaryUM));
                factor = itemUomConversion == null ? Convert.ToDecimal(UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim())) : Convert.ToDecimal(itemUomConversion.ConversionFactor);
            }
            else
            {
                factor = Convert.ToDecimal(UoMManager.GetUoMFactor(item.UM, item.PrimaryUM, Convert.ToInt32(item.ItemId.Trim()), item.ItemNumber.Trim()));
            }
            item.ReturnQtyInPrimaryUoM = Convert.ToInt32(item.ReturnQty * factor);
            item.NonSellableQtyInPrimaryUoM = Convert.ToInt32(item.NonSellableQty * factor);
            item.ConversionFactor = Convert.ToDouble(factor);
        }
        async void PrintAsync()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                var payload = PayloadManager.ReturnOrderPayload.ReturnOrderID.ToString();
                new ItemReturnsReport(payload);
            });
        }
        public override bool ConfirmCancel()
        {
            ReasonCodes.SelectedReasonCode = null;
            if (PayloadManager.ReturnOrderPayload.IsReturnOrder)
            {
                var ConfirmWindowDialog = new Helpers.ConfirmWindow { TemplateKey = "ReasonCodeListDialog", Context = ReasonCodes, Header = "Void Return Order" };
                Messenger.Default.Send<ConfirmWindow>(ConfirmWindowDialog, MessageToken);
                if (ConfirmWindowDialog.Confirmed && ReasonCodes.SelectedReasonCode != null)
                {
                    VoidRO();
                }
            }

            return base.ConfirmCancel();
        }
        public override bool ConfirmSave()
        {
            PayloadManager.ReturnOrderPayload.ROIsInProgress = true;
            PayloadManager.ReturnOrderPayload.ReturnItems = ReturnItems;
            if (PayloadManager.ReturnOrderPayload.IsReturnOrder)
            {
                HoldRO();
            }
            else
            {
                var moveToView = new Helpers.NavigateToView { NextViewName = ViewModelMappings.View.ReturnsAndCredits, CurrentViewName = ViewModelMappings.View.ItemReturnsScreen, CloseCurrentView = true, ShowAsModal = false, SelectTab = SelectedTab };
                Messenger.Default.Send<Helpers.NavigateToView>(moveToView, CommonNavInfo.MessageToken);
            }
            IsDirty = false;
            return base.ConfirmSave();
        }
        public override bool ConfirmClosed()
        {
            if (PayloadManager.ReturnOrderPayload.IsReturnOrder)
            {
                VoidRO();
            }
            return base.ConfirmClosed();
        }

        /// <summary>
        ///  Creates Invoice and receipt entry for the order return
        /// </summary>
        /// <param name="orderId">Order id</param>
        /// <param name="orderAmount">total order amount</param>
        private void CreateInvoiceAndReceipt(string orderId, decimal orderAmount)
        {
            log.Info("[SalesLogicExpress.Application.ViewModels][ItemReturnScreenVM][CreateInvoiceAndReceipt-Start]");

            try
            {
                #region Invoice Entry

                InvoiceManager objInvoiceManager = new InvoiceManager();
                objInvoiceManager.CreateInvoice("Return", CommonNavInfo.RouteID.ToString(),
                    orderId, CommonNavInfo.Customer.CustomerNo.ToString(),
                    DateTime.Now, orderAmount, 0, orderId);
                #endregion

                #region Receipt Entry
                //Get the Return order status type code
                string reasonStatusCode = DbEngine.ExecuteScalar("SELECT ISNULL(StatusTypeID,0) AS StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD'");
                CollectionManager objReceiptManager = new CollectionManager();
                int iReceiptId = objReceiptManager.InsertReceiptHeader(new CashDelivery { PaymentAmount = orderAmount.ToString(), PaymentMode = false },
                    CommonNavInfo.Customer.CustomerNo.ToString(), CommonNavInfo.RouteID.ToString(), "", DateTime.Now.ToString("yyyy-MM-dd"), Convert.ToInt32(orderId), reasonStatusCode);
                objReceiptManager.InsertReceiptInCustomerLedger(CommonNavInfo.RouteID.ToString(),
                    iReceiptId, CommonNavInfo.Customer.CustomerNo.ToString(), orderAmount);

                #endregion
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.ViewModels][ItemReturnScreenVM][CreateInvoiceAndReceipt][Exception = " + ex.StackTrace + "]");
            }
            log.Info("[SalesLogicExpress.Application.ViewModels][ItemReturnScreenVM][CreateInvoiceAndReceipt-End]");
        }

        #endregion

    }
}
