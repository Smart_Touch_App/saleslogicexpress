﻿using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.Managers;
using System;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Telerik.Windows.Controls;
namespace SalesLogicExpress.Application.ViewModels
{
    public class CanvasViewModel : ViewModelBase
    {
        Managers.SignatureManager signatureManager = new SignatureManager();
        int _Width, _Height;
        double _ActualWidth, _ActualHeight;
        string _FileName =   ResourceManager.DomainPath + @"\Docs\" + "test.jpg", _FooterText, _SaveInFolderPath, _FilePath;
        bool _CanClearCanvas = false;
        bool _IsCanvasEnable = true;
        InkCanvas _Canvas;
        StrokeCollection _CanvasStrokeCollection = new StrokeCollection();
        public DelegateCommand ClearCanvas { set; get; }
        public DelegateCommand ModifyCanvas { set; get; }
        public bool IsCanvasEnable
        {
            get
            {
                return _IsCanvasEnable;

            }
            set
            {
                _IsCanvasEnable = value;
                OnPropertyChanged("IsCanvasEnable");
            }
        }
        public bool CanClearCanvas
        {
            get
            {

                return _CanClearCanvas;

            }
            set
            {
                _CanClearCanvas = value;
                OnPropertyChanged("CanClearCanvas");
            }
        }
        public double ActualWidth
        {
            get { return _ActualWidth; }
            set { _ActualWidth = value; OnPropertyChanged("ActualWidth"); }
        }
        public double ActualHeight
        {
            get { return _ActualHeight; }
            set { _ActualHeight = value; OnPropertyChanged("ActualHeight"); }
        }
        public int Width
        {
            get { return _Width; }
            set { _Width = value; OnPropertyChanged("Width"); }
        }
        public int Height
        {
            get { return _Height; }
            set { _Height = value; OnPropertyChanged("Height"); }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; OnPropertyChanged("FileName"); }
        }
        public string FooterText
        {
            get { return _FooterText; }
            set { _FooterText = value; OnPropertyChanged("FooterText"); }
        }
        public StrokeCollection CanvasStrokeCollection
        {
            get
            {
                return _CanvasStrokeCollection;
            }
            set
            {
                _CanvasStrokeCollection = value;
                OnPropertyChanged("CanvasStrokeCollection");
            }
        }

        public Flow LifeCycleFlow { get; set; }
        public SignType CanvasSignType { get; set; }
        public int RefId { get; set; }
        
        bool _SignatureAvailable=false;
        public bool SignatureAvailable
        {
            get
            {
                return _SignatureAvailable;
            }
            set
            {
                _SignatureAvailable = value;
                OnPropertyChanged("SignatureAvailable");
            }
        }
        public event EventHandler<CanvasUpdatedEventArgs> CanvasChanged;
        protected virtual void OnCanvasChange(CanvasUpdatedEventArgs e)
        {
            EventHandler<CanvasUpdatedEventArgs> handler = CanvasChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class CanvasUpdatedEventArgs : EventArgs
        {
            public CanvasUpdatedEventArgs()
            {
                StateChangedTime = DateTime.SpecifyKind(StateChangedTime, DateTimeKind.Utc);
            }
            public bool HasSignature { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        /// <summary>
        /// This class acts as a viewmmodel for signature canvas used in the UI
        /// </summary>
        /// <param name="lifeCycleFlow">Enum defining the particular flow for which the signature is associated. e.g OrderLifeCycle,OrderReturn,Replenishment,Credits etc</param>
        /// <param name="signType">Enum defining the type of sign in a flow, e.g a flow/activity lifecycle can include having multiple signs stored in db, hence which sign is to be reffered when using this class instance.</param>
        /// <param name="transactionID">This parameter defines the id for the transaction for which the sign is saved in database, e.g In Order Process this will be the Order ID</param>
        /// <param name="IsInReadOnlyMode">Whether the canvas should be displayed in readonly mode. i.e Non-Editable mode</param>
        public CanvasViewModel(Flow lifeCycleFlow, SignType signType, int transactionID, bool IsInReadOnlyMode = false)
        {
            _SaveInFolderPath = @"Resources\Images\";
            LifeCycleFlow = lifeCycleFlow;
            CanvasSignType = signType;
            RefId = transactionID;
            GetSignatureFromDB();

            if (IsInReadOnlyMode)
            {
                CanClearCanvas = false;
                IsCanvasEnable = false;
            }
            else
            {
                CanClearCanvas = CanvasStrokeCollection.Count != 0;
                IsCanvasEnable = CanvasStrokeCollection.Count == 0;
            }

            InitializeCommands();
        }

        /// <summary>
        /// This function act to save signature in DB
        /// </summary>
        /// <param name="DisableCanvasAfterSave">this defines whether we need to disable canvas after save or not </param>
        /// <param name="CanClearCanvasAfterSave">this defines whether we need to disable clear button after save or not </param>
        /// <param name="saveCanvasAsImage"> this accept value that weather we need to store canvas as image in app or not</param>
        /// <param name="imageName">if we need to save image in app then define the name for it in this parameter</param>
        public void UpdateSignatureToDB(bool DisableCanvasAfterSave, bool CanClearCanvasAfterSave, bool saveCanvasAsImage, string imageName = "")
        {
            StrokeCollection sc = this.CanvasStrokeCollection;

            using (MemoryStream ms = new MemoryStream())
            {
                sc.Save(ms);
                signatureManager.UpdateSignature(LifeCycleFlow, CanvasSignType, RefId, ms.ToArray(), saveCanvasAsImage, imageName);
            }
            CanClearCanvas = CanClearCanvasAfterSave;
            IsCanvasEnable = !DisableCanvasAfterSave;

            if (saveCanvasAsImage)
            {
                FileName = imageName;
                SaveCanvasAsImage();
            }

        }
        public void GetSignatureFromDB()
        {
            CanvasStrokeCollection = signatureManager.GetSignature(LifeCycleFlow, CanvasSignType, RefId);
        }
        public void ResetSignFromDB()
        {
            signatureManager.UpdateSignature(LifeCycleFlow, CanvasSignType, RefId, null, false);
        }
        void InitializeCommands()
        {
            CanvasStrokeCollection.StrokesChanged += CanvasStrokeCollection_StrokesChanged;
            ClearCanvas = new DelegateCommand((param) =>
             {
                 CanvasStrokeCollection.Clear();
                 IsCanvasEnable = true;
                 CanClearCanvas = false;
                 NotifyStrokeCollectionChange();
             });
            ModifyCanvas = new DelegateCommand((param) =>
             {
                 if (CanvasStrokeCollection.Count > 0)
                 {
                     OnPropertyChanged("CanClearCanvas");
                     CanClearCanvas = true;
                 }
             });
        }

        void CanvasStrokeCollection_StrokesChanged(object sender, StrokeCollectionChangedEventArgs e)
        {
            NotifyStrokeCollectionChange();
        }

        private void NotifyStrokeCollectionChange()
        {
            CanvasUpdatedEventArgs args = new CanvasUpdatedEventArgs();
            args.HasSignature = CanvasStrokeCollection.Count > 0 ? true : false;
            args.StateChangedTime = DateTime.Now;
            SignatureAvailable = args.HasSignature;
            OnCanvasChange(args);
        }

        public async void SaveCanvasAsImage()
        {
            await Task.Run(() =>
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    try
                    {
                        InkCanvas canvas = new InkCanvas();
                        canvas.LayoutTransform = null;

                        if (Width == 0 && Height == 0)
                        {
                            Height = 250;
                            Width = 300;
                        }

                        System.IO.Directory.CreateDirectory(ResourceManager.DomainPath +@"\Docs");
                        if (File.Exists(FileName))
                        {
                            File.Delete(FileName);
                        }
                        // Get the size of canvas
                        System.Windows.Size size = new System.Windows.Size(Width, Height);
                        // Measure and arrange the surface
                        // VERY IMPORTANT
                        canvas.MoveEnabled = false;
                        canvas.ResizeEnabled = false;
                        canvas.Measure(size);
                        canvas.Arrange(new Rect(size));
                        canvas.Strokes = CanvasStrokeCollection;
                        RenderTargetBitmap rtb = new RenderTargetBitmap((int)Width, (int)Height, 96d, 96d, PixelFormats.Pbgra32);
                        rtb.Render(canvas);
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(rtb));
                        FileStream file = new FileStream(FileName, FileMode.Create);
                        encoder.Save(file);
                        file.Close();
                    }
                    catch (System.Exception ex)
                    {

                    }
                }));
            });
        }

        public void GetImageOfCanvas(StrokeCollection CanvasStrokeCollection)
        {
            try
            {
                InkCanvas canvas = new InkCanvas();
                canvas.LayoutTransform = null;

                if (Width == 0 && Height == 0)
                {
                    Height = 250;
                    Width = 300;
                }

                System.IO.Directory.CreateDirectory(ResourceManager.DomainPath + @"\Docs");
                if (File.Exists(FileName))
                {
                    File.Delete(FileName);
                }
                // Get the size of canvas
                System.Windows.Size size = new System.Windows.Size(Width, Height);
                // Measure and arrange the surface
                // VERY IMPORTANT
                canvas.MoveEnabled = false;
                canvas.ResizeEnabled = false;
                canvas.Measure(size);
                canvas.Arrange(new Rect(size));
                canvas.Strokes = CanvasStrokeCollection;
                RenderTargetBitmap rtb = new RenderTargetBitmap((int)Width, (int)Height, 96d, 96d, PixelFormats.Pbgra32);
                rtb.Render(canvas);
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));
                FileStream file = new FileStream(FileName, FileMode.Create);
                encoder.Save(file);
                file.Close();
            }
            catch (System.Exception ex)
            {
                
                throw;
            }
            
        }
        public Byte[] GetCanvasAsImageByteArray()
        {
            Byte[] arr = null;
            try
            {
                string filePath = "";////SaveCanvasAsImage();
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                arr = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();
                //File.Delete(filePath);
            }
            catch (System.Exception ex)
            {
                arr = null;
            }
            return arr;
        }
    }
}
