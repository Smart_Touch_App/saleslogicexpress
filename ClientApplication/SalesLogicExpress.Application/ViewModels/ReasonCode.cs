﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.Helpers;
using GalaSoft.MvvmLight.Messaging;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using log4net;

namespace SalesLogicExpress.Application.ViewModels
{
    public class ReasonCode : ViewModelBase, IDataErrorInfo
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.ReasonCode");

        public ReasonCode()
        {

        }
        public ActivityKey ActivityKey { get; set; }
        public DelegateCommand VoidOrder { get; set; }

        public event EventHandler<OrderStateChangeArgs> StateChanged;

        public Guid MessageToken { get; set; }

        public string ErrorText { get; set; }
        async void LoadOrderReasonList()
        {
            await Task.Run(() =>
            {
                VoidOrderReasonList = new OrderManager().GetReasonListForVoidOrder();
            });
        }
        public ReasonCode(string Type)
        {
            try
            {
                LoadOrderReasonList();
                VoidOrder = new DelegateCommand((param) =>
               {
                   var objInv = new Managers.InventoryManager();

                   #region UpdateCommitedQuantity
                   if (PayloadManager.OrderPayload.Items != null)
                   {
                       // The UI may have been dirtied and there can be a difference in the value that were stored during order and the qty in the grid.
                       // So, Get the saved quantities from the db and update the orderqty and um in payload
                       List<OrderItem> items = new OrderManager().GetOrderItems(PayloadManager.OrderPayload.OrderID);
                       foreach (OrderItem item in items)
                       {
                           OrderItem orderItem = PayloadManager.OrderPayload.Items.FirstOrDefault(i => i.ItemId == item.ItemId);
                           if (orderItem != null)
                           {
                               orderItem.OrderQty = item.OrderQty;
                               orderItem.UM = item.UM;
                               orderItem.PrimaryUM = item.PrimaryUM;
                           }
                       }
                       objInv.UpdateCommittedQuantity(PayloadManager.OrderPayload.Items, StatusTypesEnum.VOID);
                   }
                   #endregion

                   ReasonCode rc = param as ReasonCode;

                   if (rc == null)
                   {
                       ErrorText = Constants.Common.SelectVoidOrderReasonAlert;
                       return;
                   }
                   else
                   {
                       //Set the void reason code
                       Order.VoidOrderReasonId = rc.Id;
                       ErrorText = "";
                   }

                   new Managers.OrderManager().SaveVoidOrderReasonCode(Order.OrderId.ToString(), rc.Id);
                   ObservableCollection<OrderItem> OrderItems = new ObservableCollection<OrderItem>();
                   Helpers.NavigateToView moveToView = new NavigateToView();
                   moveToView.CloseCurrentView = true;
                   moveToView.ShowAsModal = false;
                   Helpers.ResourceManager.CommonNavInfo.ParentViewModel = null;
                   OrderStateChangeArgs arg = new OrderStateChangeArgs();
                   arg.State = OrderState.Void;
                   OnStateChanged(arg);
                   System.Threading.Thread.Sleep(50);
                   LogActivity(ActivityKey);

                   #region update quantities
                   string tempStopDate = string.Empty;
                   string stopID = string.Empty;
                   if (PayloadManager.OrderPayload.StopDate.Date > DateTime.Now.Date)
                   {
                       tempStopDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                       stopID = new Managers.CustomerDashboardManager().GetStopIDForCust(PayloadManager.OrderPayload.Customer.CustomerNo, tempStopDate);
                   }
                   else
                   {
                       tempStopDate = PayloadManager.OrderPayload.StopDate.Date.ToString("yyyy-MM-dd");
                       stopID = PayloadManager.OrderPayload.StopID;
                   }

                   new CustomerManager().UpdateSequenceNo(stopID, tempStopDate);
                   new CustomerManager().UpdateActivityCount(stopID, false, true);
                   PayloadManager.OrderPayload.Customer.CompletedActivity += 1;
                   //PayloadManager.OrderPayload.Customer.HasActivity = true;
                   PayloadManager.OrderPayload.Customer.SaleStatus = "";
                   new Managers.OrderManager().UpdateNoSaleReason(stopID, PayloadManager.OrderPayload.Customer.CustomerNo);
                   #endregion

                   //UPDATE CUSTOMER'S TEMPERORY SEQUENCE NO. -
                   DateTime todayDate = Convert.ToDateTime(tempStopDate);
                   ViewModels.CustomerDashboard custDash = new CustomerDashboard();
                   PayloadManager.OrderPayload.Customer = custDash.GetNewCustomer(todayDate, PayloadManager.OrderPayload.Customer);
                   bool IsItemPickedForOrder = false;
                   IsItemPickedForOrder = new OrderManager().IsItemPickedForOrder(Order.OrderId.ToString());
                   if (ParentViewModel.GetType() == typeof(Order))
                   {
                       Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.VoidAtOrderEntry);
                       moveToView.CurrentViewName = ViewModelMappings.View.Order;
                   }
                   else if (ParentViewModel.GetType() == typeof(OrderTemplate))
                   {
                       Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.VoidAtOrderEntry);
                       moveToView.CurrentViewName = ViewModelMappings.View.OrderTemplate;
                   }
                   else
                   {
                       Managers.OrderManager.UpdateOrderStatus(Order.OrderId.ToString(), ActivityKey.VoidAtDeliverCustomer);
                       moveToView.CurrentViewName = ViewModelMappings.View.DeliveryScreen;
                       //update payment void
                       TransactionManager.VoidPayment(PayloadManager.PaymentPayload.TransactionID, rc.Id.ToString(), CommonNavInfo.Customer.CustomerNo.ToString(), CommonNavInfo.RouteID.ToString());
                   }
                   if (IsItemPickedForOrder)
                   {
                       // Navigate to pick order
                       moveToView.NextViewName = ViewModelMappings.View.PickOrder;
                       moveToView.Refresh = true;
                       if (PayloadManager.OrderPayload.Items != null)
                       {
                           PayloadManager.OrderPayload.Items.Clear();
                       }
                       Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                       Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                   }
                   else
                   {
                       PayloadManager.ApplicationPayload.CompleteActivity(ViewModelPayload.ApplicationPayload.CurrentActivity.Order);
                       // Navigate to customer home
                       moveToView.NextViewName = ViewModelMappings.View.CustomerHome;
                       moveToView.Refresh = true;
                       moveToView.CloseCurrentView = true;
                       Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow { }, MessageToken);
                       Messenger.Default.Send<Helpers.NavigateToView>(moveToView, MessageToken);
                   }

                   //Update the invoice status as Void. 
                   ResourceManager.Transaction.UpdateOrderInvoiceStatus(PayloadManager.OrderPayload.OrderID);
               });
            }
            catch (Exception ex)
            {
                Logger.Error("Error in ShowOrderTemplate" + ex.Message.ToString());
            }
        }
        void LogActivity(ActivityKey key)
        {
            PayloadManager.OrderPayload.OrderID = Order.OrderId.ToString();
            PayloadManager.OrderPayload.InvoiceNo = Order.OrderId.ToString();
            SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
            {
                ActivityHeaderID = PayloadManager.OrderPayload.TransactionID,
                CustomerID = PayloadManager.OrderPayload.Customer.CustomerNo,
                RouteID = PayloadManager.OrderPayload.RouteID,
                ActivityType = key.ToString(),
                ActivityStart = DateTime.Now,
                ActivityEnd = DateTime.Now,
                IsTxActivity = false,
                ActivityDetailClass = PayloadManager.OrderPayload.ToString(),
                ActivityDetails = PayloadManager.OrderPayload.SerializeToJson()
            };
            SalesLogicExpress.Domain.Activity a = ResourceManager.Transaction.LogActivity(ac);
            if (a != null && a.ActivityHeaderID != null)
            {
                PayloadManager.OrderPayload.TransactionID = a.ActivityHeaderID;
            }
        }
        private ObservableCollection<ReasonCode> _VoidOrderReasonList;
        public ObservableCollection<ReasonCode> VoidOrderReasonList
        {
            get
            {
                return _VoidOrderReasonList;
            }
            set
            {
                _VoidOrderReasonList = value;
                OnPropertyChanged("VoidOrderReasonList");
            }
        }
        public object ParentViewModel { get; set; }

        public int Id { get; set; }
        public string Code { get; set; }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }

        public void OnStateChanged(OrderStateChangeArgs e)
        {
            EventHandler<OrderStateChangeArgs> handler = StateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }


    }

    public class OrderStateChangeArgs : EventArgs
    {
        public OrderState State { get; set; }
        public DateTime StateChangedTime { get; set; }
    }
    public enum OrderState
    {
        Void,
        Hold,
        None
    }
}
