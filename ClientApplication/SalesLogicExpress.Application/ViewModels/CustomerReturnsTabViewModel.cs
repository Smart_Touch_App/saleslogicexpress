﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Managers;
using Telerik.Windows.Controls;
using SalesLogicExpress.Application.ReportViewModels;
using System.Threading;
using System.Windows.Threading;
using SalesLogicExpress.Application.ViewModelPayload;
namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerReturnsTabViewModel : BaseViewModel
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerReturnsTabViewModel");

        #region Delgates
        public DelegateCommand ToggleSelectedOrder { get; set; }
        public DelegateCommand PrintOrders { get; set; }
        #endregion
        #region Prop's
        bool _IsPrintButtonEnabled = false;
        public bool IsPrintButtonEnabled
        {
            get
            {
                return _IsPrintButtonEnabled;
            }
            set
            {
                _IsPrintButtonEnabled = value;
                OnPropertyChanged("IsPrintButtonEnabled");
            }
        }
        bool _TogglePrint;
        public bool TogglePrint
        {
            get
            {
                return _TogglePrint;
            }
            set
            {
                _TogglePrint = value;
                OnPropertyChanged("TogglePrint");
            }
        }
        public int SelectedOrderId { get; set; }

        TrulyObservableCollection<ReturnItem> returnItems;

        TrulyObservableCollection<ReturnOrder> returnOrderList = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnOrder> ReturnOrderList
        {
            get
            {
                return returnOrderList;
            }
            set
            {
                returnOrderList = value;
                OnPropertyChanged("ReturnOrderList");
            }
        }


        public TrulyObservableCollection<ReturnItem> ReturnItems
        {
            get
            {
                return returnItems;
            }
            set
            {
                returnItems = value;
                OnPropertyChanged("ReturnItems");
            }
        }

        TrulyObservableCollection<ReturnOrder> returnOrders = new TrulyObservableCollection<ReturnOrder>();
        public TrulyObservableCollection<ReturnOrder> ReturnOrders
        {
            get
            {
                return returnOrders;
            }
            set
            {

                returnOrders = value;
                OnPropertyChanged("ReturnOrders");
            }
        }
        #endregion

        public CustomerReturnsTabViewModel(BaseViewModel ParentView)
        {
            this.ParentView = ParentView;
            this.ParentView.IsBusy = true;
            LoadOrders();
            InitializeCommands();
        }

        private void InitializeCommands()
        {
            ToggleSelectedOrder = new DelegateCommand(param =>
            {
                if (param == null) return;
                List<object> parameters = param as List<object>;
                //foreach (var item in ReturnItems)
                //{
                //    if (item.ReturnOrderID.Equals(Convert.ToInt32(parameters[0])))
                //    {
                //        item.IsSelectedItem = Convert.ToBoolean(parameters[1]);
                //    }
                //}
                var selectedOrder = ReturnOrders.Where(ri => ri.ReturnOrderID == Convert.ToInt32(parameters[0])).FirstOrDefault();
                if (selectedOrder != null)
                {
                    selectedOrder.IsSelected = true;
                    ReturnOrders.Where(x => x.ReturnOrderID != selectedOrder.ReturnOrderID).ToList().ForEach(x => { x.IsSelected = false; });
                    ReturnOrders.Where(x => x.ReturnOrderID != Convert.ToInt32(parameters[0])).ToList().ForEach(x => { x.ReturnItems.ToList().ForEach(i => { i.IsSelectedItem = false; }); });

                    var orderItems = selectedOrder.ReturnItems;
                    foreach (var item in orderItems)
                    {
                        if (item.ReturnOrderID.Equals(Convert.ToInt32(parameters[0])))
                        {
                            item.IsSelectedItem = Convert.ToBoolean(parameters[1]);
                        }
                    }

                    bool enable = ReturnOrders.Count(i => i.IsSelected == true) > 0 ? true : false;

                    var TEMP = ReturnOrders.FirstOrDefault(i => i.IsSelected == true);
                    IsPrintButtonEnabled = enable;
                }
            });
            PrintOrders = new DelegateCommand(param =>
            {
                PrintAsync();
            });
        }

        async void PrintAsync()
        {
            await Task.Run(() =>
            {
                IsBusy = true;
                //var payload = ReturnItems.Where(s => s.IsSelectedItem).FirstOrDefault().ReturnOrderID.ToString();
                var payload = ReturnOrders.Where(s => s.IsSelected).FirstOrDefault().ReturnOrderID.ToString();
                new ItemReturnsReport(payload);
            });
        }

        async void LoadOrders()
        {
            await Task.Run(() =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][Start:LoadOrders]");
                try
                {
                    ReturnItems = new TrulyObservableCollection<ReturnItem>();
                    ReturnItems = new CustomerReturnsManager().GetItemsForReturnsTab();
                    //returnItemsList = new TrulyObservableCollection<ReturnItem>(ReturnItems.ToList<ReturnItem>());
                    //ReturnItems.ItemPropertyChanged += ReturnItems_ItemPropertyChanged;

                    var orderIDCollection = ReturnItems.GroupBy(x => x.ReturnOrderID).Distinct();
                    orderIDCollection = orderIDCollection.OrderByDescending(x => x.Key);
                    var isOR = false; // variable stores OrderStatusId field.

                    foreach (var item in orderIDCollection)
                    {
                        ReturnOrder ro_item = new ReturnOrder();
                        var ro_items = ReturnItems.Where(x => x.ReturnOrderID == Convert.ToInt32(item.Key)).Clone();
                        //var orderDetailCollection = new CustomerReturnsManager().GetOrderDetails(item.Key);

                        foreach (var ro in ro_items)
                        {
                            ro.OrderDateF = ReturnItems.Where(x => x.ReturnOrderID == item.Key && x.OrderID == ro.OrderID && x.ItemId == ro.ItemId).FirstOrDefault().OrderDateF;
                            ro_item.ReturnItems.Add(ro as ReturnItem);
                        }
                        ro_item.ReturnOrderID = item.Key;
                        var dict = new CustomerReturnsManager().GetOrderFlags(item.Key);
                        if (dict != null)
                        {
                            ro_item.IsSettled = dict["IsSettled"];
                        }
                        if (ro_item.ReturnItems.Count() > 0 && ro_item.ReturnItems != null)
                            isOR = ro_item.IsOrderReturned = ro_item.ReturnItems[0].OrderStateID == ActivityKey.OrderReturned.GetStatusIdFromDB();

                        ro_item.IsEnabled = !ro_item.ReturnItems.Any(x => x.OrderQty == x.ReturnQty);
                        if (ro_item.IsOrderReturned)
                        {
                            ro_item.Surcharge = Convert.ToDecimal(ro_item.ReturnItems[0].EnergySurchargeAmt);
                            ro_item.OrderTotal = ro_item.ReturnItems.Sum(x => x.ExtendedPrice) + (decimal)ro_item.ReturnItems[0].EnergySurchargeAmt + ro_item.ReturnItems.Sum(x => x.TaxAmount);

                        }
                        else
                        {
                            ro_item.Surcharge = 0;// Energy surcharge is taken at item level as it is common for all items which belongs to same order
                            ro_item.OrderTotal = ro_item.ReturnItems.Sum(x => x.ExtendedPrice) + ro_item.ReturnItems.Sum(x => x.TaxAmount);

                        }
                        ro_item.OrderDate = ro_item.ReturnItems[0].OrderDateF;
                        ro_item.IsVoid = ro_item.ReturnItems[0].OrderStateID == ActivityKey.VoidAtROEntry.GetStatusIdFromDB() || ro_item.ReturnItems[0].OrderStateID == ActivityKey.VoidAtROPick.GetStatusIdFromDB();
                        ro_item.IsOnHold = ro_item.ReturnItems[0].OrderStateID == ActivityKey.HoldAtROEntry.GetStatusIdFromDB() || ro_item.ReturnItems[0].OrderStateID == ActivityKey.HoldAtROPrint.GetStatusIdFromDB();

                        ro_item.IsHoldAtPick = ro_item.ReturnItems[0].OrderStateID == ActivityKey.HoldAtROPick.GetStatusIdFromDB();

                        ro_item.IsEnabled = true;
                        if (ro_item.IsOrderReturned)
                        {
                            ro_item.ReturnItems.ToList().ForEach(x => { x.IsItemEnabled = false; });
                        }

                        // intentionally set flag to false if order is on hold or void. To hide 'Order Returned' label on returns tab
                        ro_item.IsOrderReturned = ro_item.IsOnHold || ro_item.IsVoid || ro_item.IsHoldAtPick || !isOR ? false : true;

                        ro_item.TotalItems = ro_item.ReturnItems.Count();
                        ReturnOrders.Add(ro_item);
                    }

                    ReturnOrderList = new TrulyObservableCollection<ReturnOrder>(ReturnOrders.ToList<ReturnOrder>()).Clone();
                    foreach (var returnOrder in ReturnOrderList)
                    {
                        foreach (var rItem in returnOrder.ReturnItems)
                        {
                            rItem.OrderDateF = ReturnItems.FirstOrDefault(x => x.ItemId == rItem.ItemId && x.ReturnOrderID == rItem.ReturnOrderID).OrderDateF;

                        }
                    }

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][LoadOrders][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                finally
                {
                    this.ParentView.IsBusy = false;
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][CustomerReturnsTabViewModel][End:LoadOrders]");
            });

        }

        void ReturnItems_ItemPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(""))
            {
            }
        }

        internal void Search(string searchTerm)
        {
            if (searchTerm.Trim().Length > 2)
            {
                SearchItems(searchTerm);
            }
            if (searchTerm.Trim().Length == 0)
            {
                ResetSearch();
            }
        }
        CancellationTokenSource tokenForCancelTask = new CancellationTokenSource();
        public async void SearchItems(string searchTerm)
        {
            await Task.Run(() =>
           {
               if (searchTerm.Trim().Length > 2 || searchTerm.Trim().Length == 0)
               {
                   //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                   foreach (var returnOrder in ReturnOrders)
                   {
                       var refROItem = ReturnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID);
                       returnOrder.ReturnItems = new ObservableCollection<ReturnItem>(refROItem.ReturnItems.Where(c => c.ItemDescription.ToLower().Contains(searchTerm.ToLower()) || c.ItemNumber.ToLower().Contains(searchTerm.ToLower())).ToList<ReturnItem>());
                   }
               }
           }, tokenForCancelTask.Token);
        }

        internal void ResetSearch()
        {
            //ReturnItems = new TrulyObservableCollection<ReturnItem>(returnItemsList.ToList<ReturnItem>());
            foreach (var returnOrder in ReturnOrders)
            {
                returnOrder.ReturnItems.Clear();
                returnOrder.ReturnItems = ReturnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID).ReturnItems.Clone();
                for (int index = 0; index < returnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID).ReturnItems.Count(); index++)
                {
                    returnOrder.ReturnItems[index].OrderDateF = returnOrderList.FirstOrDefault(x => x.ReturnOrderID == returnOrder.ReturnOrderID).ReturnItems[index].OrderDateF;
                }
            }
        }
    }
}
