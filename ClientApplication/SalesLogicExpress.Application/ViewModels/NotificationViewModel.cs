﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using log4net;
using System.Collections.ObjectModel;
using System.Data;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ViewModels
{
    public class NotificationViewModel : BaseViewModel
    {
        public readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Properties
        TrulyObservableCollection<Notification> _Notifications = new TrulyObservableCollection<Notification>();
        public TrulyObservableCollection<Notification> Notifications
        {
            get
            {
                return _Notifications;
            }
            set
            {
                _Notifications = value;
                OnPropertyChanged("Notifications");
            }
        }


        bool _NotificationAvailable = false;
        public bool NotificationAvailable
        {
            get
            {
                return _NotificationAvailable ;
            }
            set
            {


                _NotificationAvailable = value;
                OnPropertyChanged("NotificationAvailable");
            }
        }
        int _NotificationCount = 0;
        public int NotificationCount
        {
            get
            {
                return _NotificationCount;
            }
            set
            {
                NotificationAvailable = value > 0 ? true : false;
                _NotificationCount = value;
                OnPropertyChanged("NotificationCount");
            }
        }
        #endregion

        #region Delegate Commands
        public DelegateCommand MarkAsRead { get; set; }
        #endregion

        #region Private Methods
        void InitializeCommands()
        {
            MarkAsRead = new DelegateCommand((param) =>
            {
                Logger.Info("[SalesLogicExpress.Application.ViewModels][NotificationViewModel][Start:MarkAsRead]");
                try
                {
                    if (param == null)
                    {
                        return;
                    }
                    Notification notification = param as Notification;
                    notification.Read = "Y";
                    notification.ModifiedDate = DateTime.Now;
                    bool updated = ResourceManager.NotificationManager.MarkNotificationAsRead(notification);
                    if (updated)
                    {
                        Notifications.Remove(notification);
                        NotificationCount = Notifications.Count(item => item.Read.Equals("N"));
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][NotificationViewModel][MarkAsRead][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                Logger.Info("[SalesLogicExpress.Application.ViewModels][NotificationViewModel][End:MarkAsRead]");

            });
        }
        #endregion

        #region Public Methods
        public NotificationViewModel()
        {
            InitializeCommands();
        }
        public void GetNotifications()
        {

            Logger.Info("[SalesLogicExpress.Application.ViewModels][NotificationViewModel][Start:GetNotifications]");
            try
            {
                Notifications = ResourceManager.NotificationManager.GetNotifications();
                NotificationCount = Notifications.Count(item => item.Read.Equals("N"));
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][NotificationViewModel][GetNotifications][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Application.ViewModels][NotificationViewModel][End:GetNotifications]");
        }
        #endregion

    }
}
