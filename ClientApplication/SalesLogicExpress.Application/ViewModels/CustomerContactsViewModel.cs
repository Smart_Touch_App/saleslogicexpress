﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.Helpers;
using Telerik.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using Managers = SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Domain;
using System.Media;
using System.Speech.Synthesis;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Text.RegularExpressions;
using log4net;
using SalesLogicExpress.Application.ViewModelPayload;
using System.Windows.Data;

namespace SalesLogicExpress.Application.ViewModels
{
    public class CustomerContactsViewModel : BaseViewModel, IDataErrorInfo
    {
        public event EventHandler<ContactVMArgs> ViewModelUpdated;
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel");

        #region Properties
        private CustomerContactsViewModel _CustomerContact;
        private int _Index;
        public bool ShowOnDashboard
        {
            get
            {
                return this.showOnDashboard;
            }
            set
            {
                this.showOnDashboard = value;
                OnPropertyChanged("ShowOnDashboard");
            }
        }
        public int Index
        {
            get
            {
                return _Index;
            }
            set
            {
                _Index = value;
                OnPropertyChanged("Index");
            }
        }
        public CustomerContactsViewModel CustomerContact
        {
            get { return _CustomerContact; }
            set { _CustomerContact = value; }
        }
        public DelegateCommand PropertyLostFocus { get; set; }

        private bool _ToggleAddButton = false, _IsTitleSelectionEnabled = true;
        private bool showOnDashboard;
        public bool IsTitleSelectionEnabled
        {
            get
            {
                return this._IsTitleSelectionEnabled;
            }
            set
            {
                this._IsTitleSelectionEnabled = value;
                OnPropertyChanged("IsTitleSelectionEnabled");
            }
        }
        private bool isDefaultCon;
        public bool IsDefaultCon
        {
            get
            {
                return this.isDefaultCon;
            }
            set
            {
                this.isDefaultCon = value;
                OnPropertyChanged("IsDefaultCon");
            }
        }
        public bool ToggleAddButton
        {
            get
            {
                return _ToggleAddButton;
            }
            set
            {
                _ToggleAddButton = value;
                OnPropertyChanged("ToggleAddButton");
            }
        }
        string _Title, _ContactID, _FirstName, _MiddleName, _LastName, _Phone, _PhonePart1, _PhonePart2, _PhonePart3, _SelectedPhoneType, _SelectedTitleType, _SelectedEmailType, _AreaCode, _Extension, _Email;
        public string ContactID
        {
            get
            {
                return _ContactID;
            }
            set
            {
                _ContactID = value;
                OnPropertyChanged("ContactID");
            }
        }
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
                if (string.IsNullOrEmpty(_FirstName))
                {
                    if (IsAddContact || IsEditContact)
                    {
                        IsValidFirstName = true;
                        IsFirstNameEmpty = false;
                        // IsAddContact = false;
                    }
                    else
                        IsFirstNameEmpty = true;
                }
                else
                {
                    IsFirstNameEmpty = false;
                    if (_FirstName.Length < 3)
                        IsValidFirstName = false;
                    else
                    {
                        if (letterOnlyRegex.IsMatch(_FirstName))
                        {
                            IsValidFirstName = true; //(!string.IsNullOrEmpty(LastName))
                            if ( IsValidMiddleName && IsValidLastName && IsTitleTypeSelected && IsValidEmail && IsValidAreaCode && IsValidPhone_1 && IsValidPhone_2 && IsValidExtension)
                            {
                                if (IsPhoneTypeSelected)
                                {
                                    if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                                    {
                                        ToggleAddButton = true;
                                    }
                                    else ToggleAddButton = false;
                                }
                                else if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                                {
                                    if (!IsPhoneTypeSelected) ToggleAddButton = false;
                                    else ToggleAddButton = true;
                                }
                                if ((!string.IsNullOrEmpty(PhonePart2) && (!string.IsNullOrEmpty(PhonePart1)) && (!string.IsNullOrEmpty(AreaCode)) && !IsPhoneTypeSelected) ||
                                    (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected))
                                    ToggleAddButton = false;
                                else
                                    ToggleAddButton = true;
                            }
                        }
                        else
                        {
                            IsValidFirstName = false;
                        }
                    }
                }
                OnPropertyChanged("FirstName");
            }
        }
        public string MiddleName
        {
            get
            {
                return _MiddleName;
            }
            set
            {
                _MiddleName = value;
                if (string.IsNullOrEmpty(MiddleName))
                {
                    IsValidMiddleName = true;
                    if (IsAddContact || IsEditContact)
                    {
                        IsMiddleNameEmpty = false;
                        // IsAddContact = false;
                    }
                    else
                        IsMiddleNameEmpty = true;
                }
                else
                {
                    if (letterOnlyRegex.IsMatch(MiddleName))
                    {
                        IsValidMiddleName = true; //&& (!string.IsNullOrEmpty(LastName)) 
                        if ((!string.IsNullOrEmpty(FirstName)) && IsValidFirstName && IsValidLastName && IsTitleTypeSelected && IsValidEmail && IsValidAreaCode && IsValidPhone_1 && IsValidPhone_2 && IsValidExtension)
                        {
                            if (IsPhoneTypeSelected)
                            {
                                if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                                {
                                    ToggleAddButton = true;
                                }
                                else ToggleAddButton = false;
                            }
                            else if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                            {
                                if (!IsPhoneTypeSelected) ToggleAddButton = false;
                                else ToggleAddButton = true;
                            }
                            if ((!string.IsNullOrEmpty(PhonePart2) && (!string.IsNullOrEmpty(PhonePart1)) && (!string.IsNullOrEmpty(AreaCode)) && !IsPhoneTypeSelected) ||
                                (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected))
                                ToggleAddButton = false;
                            else
                                ToggleAddButton = true;
                        }
                    }
                    else
                    {
                        IsValidMiddleName = false;
                    }
                }
                OnPropertyChanged("MiddleName");
            }
        }
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
                if (string.IsNullOrEmpty(_LastName))
                {
                    if (IsAddContact || IsEditContact)
                    {
                        IsValidLastName = true;
                        IsLastNameEmpty = false;
                        IsAddContact = false;
                    }
                    else
                        IsLastNameEmpty = true;
                }
                else
                {
                    //IsLastNameEmpty = false;
                    if (_LastName.Length < 3)
                        IsValidLastName = false;
                    else
                    {
                        if (letterOnlyRegex.IsMatch(_LastName))
                        {
                            IsValidLastName = true;
                            if ((!string.IsNullOrEmpty(FirstName)) && IsValidFirstName && IsValidMiddleName && IsTitleTypeSelected && IsValidEmail && IsValidAreaCode && IsValidPhone_1 && IsValidPhone_2 && IsValidExtension)
                            {
                                if ((!string.IsNullOrEmpty(PhonePart2) && (!string.IsNullOrEmpty(PhonePart1)) && (!string.IsNullOrEmpty(AreaCode)) && !IsPhoneTypeSelected) ||
                                    (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected))
                                    ToggleAddButton = false;
                                else
                                    ToggleAddButton = true;
                                if (IsPhoneTypeSelected)
                                {
                                    if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                                    {
                                        ToggleAddButton = true;
                                    }
                                    else ToggleAddButton = false;
                                }
                                else if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                                {
                                    if (!IsPhoneTypeSelected) ToggleAddButton = false;
                                    else ToggleAddButton = true;
                                }
                            }
                            else
                            {
                                OnPropertyChanged("SelectedTitleType");
                                ToggleAddButton = false;
                            }
                        }
                        else
                        {
                            IsValidLastName = false;
                        }
                    }
                }
                OnPropertyChanged("LastName");
            }
        }
        public string Phone
        {
            get
            {
                return PhonePart1 + "-" + PhonePart2;
            }
            set
            {
                _Phone = value;

                if (IsValidAreaCode && IsValidPhone_1 && IsValidPhone_2)
                {
                    IsValidPhone = true;
                }
                else
                    IsValidPhone = false;


                //else IsStringEmpty = true;

                OnPropertyChanged("Phone");
            }
        }
        public string PhonePart1
        {
            get
            {
                return _PhonePart1;
            }
            set
            {
                _PhonePart1 = value;
                if (_PhonePart1 == string.Empty)
                {
                    IsPhoneEmpty = false;
                    if (!string.IsNullOrEmpty(_AreaCode) || !string.IsNullOrEmpty(_PhonePart2))
                    {
                        ToggleAddButton = false; IsValidPhone_1 = false;
                    }
                }
                else
                {
                    IsPhoneEmpty = true;
                    if (numericOnlyRegex.IsMatch(_PhonePart1) && (_PhonePart1.Length == 3))
                    {
                        IsValidPhone_1 = true;   //&& !string.IsNullOrEmpty(LastName)
                        if (!string.IsNullOrEmpty(FirstName)  && IsValidLastName && IsTitleTypeSelected && IsValidFirstName && IsPhoneTypeSelected &&
                            !string.IsNullOrEmpty(_AreaCode) && !string.IsNullOrEmpty(_PhonePart1) && !string.IsNullOrEmpty(_PhonePart2) && IsValidAreaCode && IsValidPhone_2)
                        {
                            if (IsAddPhone)
                            {
                                ToggleAddButton = true;
                            }
                            else if (IsValidEmail)
                            {
                                if (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected)
                                    ToggleAddButton = false;
                                else
                                    ToggleAddButton = true;
                            }
                        }
                        else
                        {
                            OnPropertyChanged("SelectedPhoneType");
                            ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        ToggleAddButton = false;
                        IsValidPhone_1 = false;
                    }
                }
                OnPropertyChanged("PhonePart1");
                OnPropertyChanged("AreaCode");
                OnPropertyChanged("PhonePart2");
            }
        }
        public string PhonePart2
        {
            get
            {
                return _PhonePart2;
            }
            set
            {
                _PhonePart2 = value;
                if (_PhonePart2 == string.Empty)
                {
                    IsPhoneEmpty = false;
                    if (!string.IsNullOrEmpty(_PhonePart1) || !string.IsNullOrEmpty(_AreaCode))
                    {
                        ToggleAddButton = false; IsValidPhone_2 = false;
                    }
                }
                else
                {
                    IsPhoneEmpty = true;
                    if (numericOnlyRegex.IsMatch(_PhonePart2) && (_PhonePart2.Length == 4))
                    {
                        IsValidPhone_2 = true;   //&& !string.IsNullOrEmpty(LastName)
                        if (!string.IsNullOrEmpty(FirstName)  && IsValidLastName && IsValidFirstName && IsPhoneTypeSelected && IsTitleTypeSelected &&
                            !string.IsNullOrEmpty(_AreaCode) && !string.IsNullOrEmpty(_PhonePart1) && !string.IsNullOrEmpty(_PhonePart2) && IsValidAreaCode && IsValidPhone_1)
                        {
                            if (IsAddPhone)
                            {
                                ToggleAddButton = true;
                            }
                            else if (IsValidEmail)
                            {
                                if (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected)
                                    ToggleAddButton = false;
                                else
                                    ToggleAddButton = true;
                            }
                        }
                        else
                        {
                            OnPropertyChanged("SelectedPhoneType");
                            ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        IsValidPhone_2 = false;
                    }
                }
                OnPropertyChanged("PhonePart2");
                OnPropertyChanged("AreaCode");           
                OnPropertyChanged("PhonePart1");
            }
        }
        public string PhonePart3
        {
            get
            {
                return _PhonePart3;
            }
            set
            {
                _PhonePart3 = value;
                if (numericOnlyRegex.IsMatch(_PhonePart3))
                {
                    IsValidPhone_3 = true;
                    ToggleAddButton = false;
                }
                else
                {
                    IsValidPhone_3 = false;
                }
                OnPropertyChanged("PhonePart3");
            }
        }
        public string Extension
        {
            get
            {
                return _Extension;
            }
            set
            {

                _Extension = value;

                if (!alphaNumericRegex.IsMatch(value) && (value.Length < 8))
                {
                    IsValidExtension = true;   //&& !string.IsNullOrEmpty(LastName)
                    if ((!string.IsNullOrEmpty(FirstName)) && IsValidFirstName  && IsValidLastName && IsValidMiddleName && IsTitleTypeSelected && (!string.IsNullOrEmpty(AreaCode))
                        && (!string.IsNullOrEmpty(PhonePart1)) && (!string.IsNullOrEmpty(PhonePart2)) && IsPhoneTypeSelected)
                    {
                        if (IsAddPhone && IsValidAreaCode && IsValidPhone_1)
                        {
                            ToggleAddButton = true;
                            _Extension = value;
                        }
                        else
                        {
                            if (IsValidEmail && IsValidAreaCode && IsValidPhone_1 && IsValidPhone_2)
                            {
                                if (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected)
                                    ToggleAddButton = false;
                                else
                                {
                                    ToggleAddButton = true;
                                    _Extension = value;
                                }
                            }
                        }
                    }
                }
                else
                {
                    IsValidExtension = false;
                }
                OnPropertyChanged("Extension");
                //OnPropertyChanged("Phone");
            }
        }
        public string AreaCode
        {
            get
            {
                return _AreaCode;
            }
            set
            {
                _AreaCode = value;
                if (_AreaCode == string.Empty)
                {
                    IsPhoneEmpty = false;
                    if (!string.IsNullOrEmpty(_PhonePart1) || !string.IsNullOrEmpty(_PhonePart2))
                    {
                        ToggleAddButton = false; IsValidAreaCode = false;
                    }
                }
                else
                {
                    IsPhoneEmpty = true;
                    if (numericOnlyRegex.IsMatch(_AreaCode) && (_AreaCode.Length == 3))
                    {
                        IsValidAreaCode = true;
                        if (IsPhoneTypeSelected &&
                            !string.IsNullOrEmpty(_AreaCode) && !string.IsNullOrEmpty(_PhonePart1) && !string.IsNullOrEmpty(_PhonePart2) && IsValidPhone_2 && IsValidPhone_1)
                        {
                            if (IsAddPhone)
                            {
                                ToggleAddButton = true;
                            }
                            else if (!string.IsNullOrEmpty(FirstName) && IsValidLastName && IsValidEmail && IsTitleTypeSelected && IsValidFirstName)
                            {
                                //&& !string.IsNullOrEmpty(LastName) 
                                if (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected)
                                    ToggleAddButton = false;
                                else
                                    ToggleAddButton = true;
                            }
                        }
                        else
                        {
                            OnPropertyChanged("SelectedPhoneType");
                            ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        IsValidAreaCode = false;
                    }
                }
                OnPropertyChanged("AreaCode");
                OnPropertyChanged("PhonePart2");
                OnPropertyChanged("PhonePart1");
            }
        }
        public string EmailID
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
                if (string.IsNullOrEmpty(_Email))
                    IsEmailEmpty = false;

                else
                {
                    IsEmailEmpty = true;
                    if (emailRegex.IsMatch(_Email))
                    {
                        IsValidEmail = true;   // && !string.IsNullOrEmpty(LastName)
                        if (!string.IsNullOrEmpty(_FirstName) && IsValidFirstName && IsTitleTypeSelected && IsValidLastName && IsEmailTypeSelected)
                        {
                            if (IsAddEmail)
                            {
                                ToggleAddButton = true;
                            }
                            else
                            {
                                if (IsValidExtension && IsValidFirstName && IsValidMiddleName)
                                {
                                    if (IsPhoneTypeSelected)
                                    {
                                        if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                                        {
                                            ToggleAddButton = true;
                                        }
                                        else ToggleAddButton = false;
                                    }
                                    else if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                                    {
                                        if (!IsPhoneTypeSelected) ToggleAddButton = false;
                                        else ToggleAddButton = true;
                                    }
                                    else if (string.IsNullOrEmpty(PhonePart2) && string.IsNullOrEmpty(AreaCode) && string.IsNullOrEmpty(PhonePart1) && !IsPhoneTypeSelected)
                                    {
                                        ToggleAddButton = true;
                                    }
                                        
                                }
                            }
                        }
                        else
                        {
                            OnPropertyChanged("SelectedEmailType");
                        }
                    }
                    else
                        IsValidEmail = false;
                }
                OnPropertyChanged("EmailID");

            }
        }
        public string SelectedPhoneType
        {
            get
            {
                return _SelectedPhoneType;
            }
            set
            {
                _SelectedPhoneType = value;
                if (!string.IsNullOrEmpty(_SelectedPhoneType))
                {
                    IsPhoneTypeSelected = true;
                    ToggleAddButton = false;    //&& !string.IsNullOrEmpty(LastName)
                    if (!string.IsNullOrEmpty(_FirstName)  && IsValidLastName && IsTitleTypeSelected && IsValidFirstName && IsValidMiddleName)
                    {
                        if (IsAddPhone)
                        {
                            if ((!string.IsNullOrEmpty(PhonePart1) && (!string.IsNullOrEmpty(PhonePart2)) &&
                            (!string.IsNullOrEmpty(AreaCode)) && IsValidPhone_2 && IsValidExtension))
                                ToggleAddButton = true;
                        }
                        else if ((!string.IsNullOrEmpty(PhonePart1) && (!string.IsNullOrEmpty(PhonePart2)) &&
                               (!string.IsNullOrEmpty(AreaCode)) && IsValidEmail && IsValidPhone_2 && IsValidExtension))
                        {
                            if (!string.IsNullOrEmpty(EmailID) && IsEmailTypeSelected)
                            {
                                ToggleAddButton = true;
                            }
                            else
                            {
                                ToggleAddButton = false;
                            }
                        }
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                OnPropertyChanged("SelectedPhoneType");
            }
        }
        public string SelectedTitleType
        {
            get
            {
                return _SelectedTitleType;
            }
            set
            {
                _SelectedTitleType = value;

                if (!string.IsNullOrEmpty(_SelectedTitleType))
                {
                    IsTitleTypeSelected = true;
                    ToggleAddButton = false;  //&& (!string.IsNullOrEmpty(LastName)) 
                    if ((!string.IsNullOrEmpty(FirstName)) && IsValidFirstName && IsValidMiddleName && IsValidLastName && IsValidEmail && IsValidAreaCode && IsValidPhone_1 && IsValidPhone_2 && IsValidExtension)
                    {
                        if (IsPhoneTypeSelected)
                        {
                            if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                            {
                                ToggleAddButton = true;
                            }
                            else ToggleAddButton = false;
                        }
                        else if (!string.IsNullOrEmpty(PhonePart2) && !string.IsNullOrEmpty(AreaCode) && !string.IsNullOrEmpty(PhonePart1))
                        {
                            if (!IsPhoneTypeSelected) ToggleAddButton = false;
                            else ToggleAddButton = true;
                        }
                        if ((!string.IsNullOrEmpty(PhonePart2) && (!string.IsNullOrEmpty(PhonePart1)) && (!string.IsNullOrEmpty(AreaCode)) && !IsPhoneTypeSelected) ||
                            (!string.IsNullOrEmpty(EmailID) && !IsEmailTypeSelected))
                            ToggleAddButton = false;
                        else
                            ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }

                OnPropertyChanged("SelectedTitleType");
            }
        }
        public string SelectedEmailType
        {
            get
            {
                return _SelectedEmailType;
            }
            set
            {
                _SelectedEmailType = value;

                if (!string.IsNullOrEmpty(_SelectedEmailType))
                {
                    IsEmailTypeSelected = true;
                    ToggleAddButton = false;   //&& !string.IsNullOrEmpty(LastName) 
                    if (!string.IsNullOrEmpty(_FirstName) && IsValidLastName && IsTitleTypeSelected && IsValidFirstName && IsValidMiddleName)
                    {
                        if (IsAddEmail)
                        {
                            if ((!string.IsNullOrEmpty(EmailID)) && IsValidEmail)
                                ToggleAddButton = true;
                        }
                        else if ((!string.IsNullOrEmpty(EmailID)) && IsValidEmail)
                        {
                            if (IsPhoneTypeSelected)
                            {
                                if (!string.IsNullOrEmpty(_AreaCode) && !string.IsNullOrEmpty(_PhonePart1) && !string.IsNullOrEmpty(_PhonePart2))
                                {
                                    ToggleAddButton = true;
                                }
                                else
                                {
                                    ToggleAddButton = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }

                OnPropertyChanged("SelectedEmailType");
            }
        }
        private int _PhoneTypeID;
        public int PhoneTypeID
        {
            get
            {
                return _PhoneTypeID;
            }
            set
            {
                _PhoneTypeID = value;
                OnPropertyChanged("PhoneTypeID");
            }
        }
        private int _EmailTypeID;
        public int EmailTypeID
        {
            get
            {
                return _EmailTypeID;
            }
            set
            {
                _EmailTypeID = value;
                OnPropertyChanged("EmailTypeID");
            }
        }
        private int _TitleTypeID;
        public int TitleTypeID
        {
            get { return _TitleTypeID; }
            set { _TitleTypeID = value; OnPropertyChanged("TitleTypeID"); }
        }
        ContactType _SelectedContactType;
        public ContactType SelectedContactType
        {
            get
            {
                return _SelectedContactType;
            }
            set
            {
                _SelectedContactType = value;
                OnPropertyChanged("SelectedContactType");
            }
        }
        public ObservableCollection<ContactType> PhoneType { get; set; }
        public ObservableCollection<ContactType> EmailType { get; set; }
        public ObservableCollection<ContactType> TitleType { get; set; }
        string _TodaysDate;
        public string TodaysDate
        {
            get
            {
                return _TodaysDate;
            }
            set
            {
                _TodaysDate = value;
                OnPropertyChanged("TodaysDate");
            }
        }
        public Models.Customer Customer
        {
            get;
            set;
        }
        private TrulyObservableCollection<Models.CustomerContact> _CustomerContacts;
        public ObservableCollection<CustomerContact> CustomerContactList { get; set; }
        public TrulyObservableCollection<Models.CustomerContact> CustomerContacts
        {
            get
            {
                return _CustomerContacts;
            }
            set
            {
                _CustomerContacts = value;
                OnPropertyChanged("CustomerContacts");
            }
        }

        private bool _SelectAllCheck = false;
        public bool SelectAllCheck
        {
            get
            {
                return _SelectAllCheck;
            }
            set
            {
                _SelectAllCheck = value;
                OnPropertyChanged("SelectAllCheck");
            }
        }

        private Regex letterOnlyRegex = new Regex(@"^[a-zA-Z0-9 @\'&\`\-_#\(\)]*$");
        // private Regex letterOnlyRegex = new Regex(@"^[A-Za-z ](?![0-9])*$");
        private Regex numericOnlyRegex = new Regex(@"^[0-9]([0-9]{1,3})?$");
        private Regex alphaNumericRegex = new Regex(@"[^0-9a-zA-Z]");
        private Regex emailRegex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9_\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        bool IsEmailEmpty = false;
        bool IsValidPhone = true;
        bool IsStringEmpty = false;
        bool IsValidFirstName = true;
        bool IsValidMiddleName = true;
        bool IsValidLastName = true;
        bool IsValidEmail = true;
        bool IsValidPhone_1 = true;
        bool IsValidPhone_2 = true;
        //bool IsValidTitle = true;
        bool IsValidPhone_3 = true;
        bool IsValidExtension = true;
        bool IsValidAreaCode = true;
        bool IsPhoneEmpty = false;
        bool IsSamePhoneAlreadyExists = false;
        //bool IsTitleEmpty = false;
        bool IsFirstNameEmpty = false;
        bool IsMiddleNameEmpty = false;
        bool IsLastNameEmpty = false;
        public bool IsTitleTypeSelected = false;
        public bool IsEmailTypeSelected = false;
        public bool IsPhoneTypeSelected = false;
        public bool IsAddContact = false;
        public bool flag = false;
        public bool IsAddPhone = false;
        public bool IsAddEmail = false;
        public bool IsEditContact = false;
        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }

        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;

                if (columnName == "FirstName" && IsFirstNameEmpty)
                {
                    result = Helpers.Constants.Common.NameMandatoryMsg;
                    ToggleAddButton = false;
                }
                else if (columnName == "FirstName" && !IsValidFirstName)
                {
                    result = Helpers.Constants.Common.NameValidationMsg;
                    ToggleAddButton = false;
                }
                else if (columnName == "MiddleName" && !IsValidMiddleName)
                {
                    result = Helpers.Constants.Common.NameValidationMsg;
                    ToggleAddButton = false;
                }
                //else if (columnName == "LastName" && IsLastNameEmpty)
                //{
                //    result = Helpers.Constants.Common.NameMandatoryMsg;
                //    ToggleAddButton = false;
                //}
                else if (columnName == "LastName" && !IsValidLastName)
                {
                    result = Helpers.Constants.Common.NameValidationMsg;
                    ToggleAddButton = false;
                }
                else if (columnName == "EmailID" && IsEmailEmpty)
                {
                    if (!IsValidEmail)
                    {
                        result = Helpers.Constants.Common.EmailValidationMsg;
                        ToggleAddButton = false;
                    }
                }
                else if (columnName == "AreaCode" && !IsValidAreaCode)
                {
                    if (!IsValidAreaCode)
                    {
                        result = Helpers.Constants.Common.AreacodeValidationMsg;
                        //result = "";
                        ToggleAddButton = false;
                    }
                }
                else if (columnName == "PhonePart1" && IsPhoneEmpty)
                {
                    if (!IsValidPhone_1)
                    {
                        result = Helpers.Constants.Common.PhoneValidationMsg;
                    //result = "";
                        ToggleAddButton = false;
                    }
                }
                else if (columnName == "PhonePart2" && IsPhoneEmpty)
                {
                    if (!IsValidPhone_2)
                    {
                        result = Helpers.Constants.Common.PhoneValidationMsg;
                    //result = Helpers.Constants.Common.PhoneValidationMsg;
                        ToggleAddButton = false;
                    }
                }
                else if (columnName == "Extension" && !IsValidExtension)
                {
                    result = Helpers.Constants.Common.ExtensionValidationMsg;
                    ToggleAddButton = false;
                }
                else if (columnName == "SelectedPhoneType" && (!string.IsNullOrEmpty(PhonePart2)) && IsValidPhone_2 && !IsPhoneTypeSelected)
                {
                    result = Helpers.Constants.Common.PhoneTypeSelectionMsg;
                    ToggleAddButton = false;
                }
                else if (columnName == "SelectedEmailType" && (!string.IsNullOrEmpty(EmailID)) && IsValidEmail && !IsEmailTypeSelected)
                {
                    result = Helpers.Constants.Common.EmailTypeSelectionMsg;
                    ToggleAddButton = false;
                }
                else if (columnName == "SelectedTitleType" && (!string.IsNullOrEmpty(FirstName)) && IsValidFirstName && (!string.IsNullOrEmpty(LastName)) && IsValidLastName && IsValidMiddleName && !IsTitleTypeSelected)
                {
                    result = Helpers.Constants.Common.TitleValidationMsg;
                    ToggleAddButton = false;
                }
                return result;
            }
        }
        Guid _MessageToken;
        public Guid MessageToken
        {

            get
            {
                return _MessageToken;
            }
            set
            {
                _MessageToken = value;
            }

        }
        CustomerDashboard customerDashboardVM = null;
        public CustomerDashboard CustomerDashboardVM
        {
            get
            {
                return this.customerDashboardVM;
            }
            set
            {
                this.customerDashboardVM = value;
                OnPropertyChanged("CustomerDashboardVM");
            }
        }

        #endregion

        protected virtual void OnViewModelStateChanged(ContactVMArgs e)
        {
            EventHandler<ContactVMArgs> handler = ViewModelUpdated;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #region Delegates and constructors
        public DelegateCommand AddNewContact { get; set; }
        public DelegateCommand BeforePhoneChange { get; set; }
        public DelegateCommand AfterEmailChange { get; set; }
        public DelegateCommand BeforeEmailChange { get; set; }
        public DelegateCommand AfterPhoneChange { get; set; }
        public DelegateCommand ShowContactDetails { get; set; }
        public DelegateCommand AddNewPhone { get; set; }
        public DelegateCommand AddNewEmail { get; set; }
        public DelegateCommand SelectAllContacts { get; set; }
        public DelegateCommand SelectContact { get; set; }
        public DelegateCommand SaveNewContact { get; set; }
        public DelegateCommand SaveNewPhone { get; set; }
        public DelegateCommand SaveNewEmail { get; set; }
        public DelegateCommand EditContact { get; set; }
        public DelegateCommand EditPhone { get; set; }
        public DelegateCommand EditEmail { get; set; }
        public DelegateCommand SaveEditedContact { get; set; }
        public DelegateCommand SaveEditedEmail { get; set; }
        public DelegateCommand SaveEditedPhone { get; set; }
        public DelegateCommand DeleteEmail { get; set; }
        public DelegateCommand DeletePhone { get; set; }
        public DelegateCommand DeleteContactFromContactList { get; set; }

        public CustomerContactsViewModel(CustomerDashboard customerDashboard)
        {
            CustomerDashboardVM = customerDashboard;
            Customer = PayloadManager.OrderPayload.Customer.Clone();
            InitializeCommands();
            GetCustomerContacts();
            GetContactType();
        }
        #endregion

        #region Methods
        //CustomerDashboard CustomerDashboardVM = new CustomerDashboard();
        public TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        void InitializeCommands()
        {

            TodaysDate = DateTime.Now.ToString("dddd, MM/dd/yyyy");

            #region PropertyLostFocus
            PropertyLostFocus = new DelegateCommand((param) =>
            {
                if (string.IsNullOrEmpty(_FirstName))
                {
                    IsStringEmpty = true;
                    OnPropertyChanged("FirstName");
                }
                else IsStringEmpty = false;
            });
            #endregion

            #region CustomerContactsViewModel Selection for showing on Dashboard
            //ShowContactDetails = new DelegateCommand((CustomerContactsViewModel) =>
            //{
            //    List<CustomerContact> otherContacts = CustomerContactList.Where(item => item.ContactID != (CustomerContactsViewModel as CustomerContact).ContactID).ToList<CustomerContact>();
            //    foreach (CustomerContact contactItem in otherContacts)
            //    {
            //        CustomerContactList.ElementAt(CustomerContactList.IndexOf(contactItem)).ShowOnDashboard = false;
            //    }
            //});
            #endregion

            #region BeforePhoneChange

            BeforePhoneChange = new DelegateCommand((sender) =>
            {
                if (sender != null)
                {
                    selectedPhone = sender as Phone;
                }
                try
                {
                    List<CustomerContact> otherContacts = CustomerContactList.Where(item => item.ContactID != selectedPhone.ContactID).ToList<CustomerContact>();
                    CustomerContact selectedContact = CustomerContactList.First(item => item.ContactID == selectedPhone.ContactID);
                    foreach (CustomerContact contact in otherContacts)
                    {
                        foreach (Phone phone in CustomerContactList.ElementAt(CustomerContactList.IndexOf(contact)).PhoneList)
                        {
                            phone.IsSelectedPhone = false;
                        }
                        foreach (Email email in CustomerContactList.ElementAt(CustomerContactList.IndexOf(contact)).EmailList)
                        {
                            email.IsSelectedEmail = false;
                        }
                        if (CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).EmailList.Count > 0)
                        {
                            int selectedEmailInContact = CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).EmailList.Count(EM => EM.IsSelectedEmail == true);
                            if (selectedEmailInContact == 0)
                            {
                                CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).EmailList[0].IsSelectedEmail = true;
                                selectedContact.DefaultEmail = CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).EmailList[0];
                            }
                        }
                        CustomerContactList.ElementAt(CustomerContactList.IndexOf(contact)).ShowOnDashboard = false;
                        //contact.ShowOnDashboard = false;
                    }

                    if (sender != null)
                    {
                        selectedContact.DefaultPhone = selectedPhone;
                    }
                    //else
                      //  selectedContact.DefaultPhone = selectedContact.PhoneList[0];

                    selectedContact.ShowOnDashboard = true;
                    new Managers.ContactManager().SetContactAsDefault(selectedContact);
                    CustomerDashboardVM.SetDefaultContact(selectedContact);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][BeforePhoneChange][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region AfterEmailChange
            AfterEmailChange = new DelegateCommand((Selected) =>
            {
                selectedEmail = Selected as Email;
            });
            #endregion

            #region BeforeEmailChange

            BeforeEmailChange = new DelegateCommand((sender) =>
            {
                if (sender!=null)
                {
                    selectedEmail = sender as Email;
                }
                try
                {
                    List<CustomerContact> otherContacts = CustomerContactList.Where(item => item.ContactID != selectedEmail.ContactID).ToList<CustomerContact>();
                    CustomerContact selectedContact = CustomerContactList.First(item => item.ContactID == selectedEmail.ContactID);
                    foreach (CustomerContact contact in otherContacts)
                    {
                        foreach (Phone phone in CustomerContactList.ElementAt(CustomerContactList.IndexOf(contact)).PhoneList)
                        {
                            phone.IsSelectedPhone = false;
                        }
                        foreach (Email email in CustomerContactList.ElementAt(CustomerContactList.IndexOf(contact)).EmailList)
                        {
                            email.IsSelectedEmail = false;
                        }
                        contact.ShowOnDashboard = false;
                    }
                    if (CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).PhoneList.Count > 0)
                    {
                        int selectedPhoneInContact = CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).PhoneList.Count(ph => ph.IsSelectedPhone == true);
                        if (selectedPhoneInContact == 0)
                        {
                            CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).PhoneList[0].IsSelectedPhone = true;
                            selectedContact.DefaultPhone = CustomerContactList.ElementAt(CustomerContactList.IndexOf(selectedContact)).PhoneList[0];
                        }
                    }

                    if (sender != null)
                    {
                        selectedContact.DefaultEmail = sender as Email;
                    }
                    //else
                    //    selectedContact.DefaultEmail = selectedContact.EmailList[0];

                    selectedContact.ShowOnDashboard = true;
                    new Managers.ContactManager().SetContactAsDefault(selectedContact);
                    CustomerDashboardVM.SetDefaultContact(selectedContact);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][BeforeEmailChange][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region AfterPhoneChange
            AfterPhoneChange = new DelegateCommand((Selected) =>
            {
                selectedPhone = Selected as Phone;
            });
            #endregion

            #region SaveNewContact
            SaveNewContact = new DelegateCommand((Selected) =>
            {
                try
                {
                    Phone phoneDetail = new Phone();
                    phoneDetail.AreaCode = AreaCode;
                    phoneDetail.Extension = Extension;
                    phoneDetail.IsDefault = false;
                    phoneDetail.Value = Phone;
                    phoneDetail.Type = SelectedPhoneType;

                    Email emailDetail = new Email();
                    emailDetail.Value = EmailID;
                    emailDetail.Type = SelectedEmailType;
                    emailDetail.IsDefault = false;

                    //Title titleDetail = new Title();
                    //titleDetail.Type = SelectedTitleType;

                    Managers.ContactManager manager = new Managers.ContactManager();
                    Models.CustomerContact customerContactItem = new Models.CustomerContact();
                    //customerContactItem.TitleList = new ObservableCollection<Title>();
                    //if (titleDetail.Type != null)
                    //{
                    //    customerContactItem.TitleList.Add(titleDetail);
                    //}
                    customerContactItem.EmailList = new ObservableCollection<Email>();
                    if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                    {
                        customerContactItem.EmailList.Add(emailDetail);
                    }
                    customerContactItem.PhoneList = new ObservableCollection<Phone>();
                    if (phoneDetail.Value.Trim().Length > 1)
                    {
                        customerContactItem.PhoneList.Add(phoneDetail);
                    }

                    string firstName = textInfo.ToTitleCase(FirstName.ToLower());
                    customerContactItem.FirstName = firstName;
                    string middleName = textInfo.ToTitleCase(MiddleName.ToLower());
                    customerContactItem.MiddleName = middleName;
                    string lastName = textInfo.ToTitleCase(LastName.ToLower());
                    customerContactItem.LastName = lastName;
                    customerContactItem.ContactName = customerContactItem.FirstName + " " + customerContactItem.MiddleName + " " + customerContactItem.LastName;
                    customerContactItem.ContactTitle = SelectedTitleType;
                    customerContactItem.IsActive = true;
                    customerContactItem.ShowOnDashboard = false;
                    //customerContactItem.IsSelected = true;
                    customerContactItem.CustomerID = Customer.CustomerNo;
                    //customerContactItem.IsExpanded = true;
                    manager.AddCustomerContact(customerContactItem);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    customerContactItem.ContactID = (CustomerContactList.Count).ToString();
                    GetCustomerContacts(customerContactItem);
                    int id = Convert.ToInt32(customerContactItem.ContactID);
                    CustomerContactList[id].IsSelected = true;
                    ToggleAddButton = false;
                    if (CustomerContactList.Count == 1)
                    {
                        CustomerContactList[0].ShowOnDashboard = true;
                        CustomerDashboardVM.SetDefaultContact(CustomerContactList[0]);
                        //manager.SetContactAsDefault(CustomerContactList[0]);   
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][SaveNewContact][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });

            #endregion

            #region SaveNewPhone
            SaveNewPhone = new DelegateCommand((param) =>
            {
                try
                {
                    Models.CustomerContact customerContact = new Models.CustomerContact();
                    Managers.ContactManager ContactManager = new Managers.ContactManager();

                    string CustomerID = Customer.CustomerNo;
                    Phone phoneDetail = new Phone();
                    phoneDetail.ContactID = ContactID;
                    customerContact = ContactManager.GetCustomerContactDetails(CustomerID, ContactID);
                    Phone phn = customerContact.PhoneList.FirstOrDefault(x => (x.AreaCode == AreaCode && x.Value == Phone && x.TypeID == Convert.ToInt32(SelectedPhoneType)));
                    if (phn != null)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Phone number with same Type \nalready exists.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    phoneDetail.AreaCode = AreaCode;
                    phoneDetail.Extension = Extension;
                    phoneDetail.IsDefault = false;
                    phoneDetail.Value = Phone;
                    phoneDetail.Type = SelectedPhoneType;

                    ContactManager.SaveContactPhone(phoneDetail, CustomerID);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    customerContact.ContactID = ContactID;
                    int id = Convert.ToInt32(customerContact.ContactID);
                    GetCustomerContacts(customerContact);
                    CustomerContactList[id].IsSelected = true;
                    customerContact = CustomerContactList.FirstOrDefault(t => t.ContactID == ContactID);
                    CustomerDashboardVM.SetDefaultContact(customerContact);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][SaveNewPhone][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region SaveNewEmail
            SaveNewEmail = new DelegateCommand((param) =>
            {
                try
                {
                    Models.CustomerContact customerContact = new Models.CustomerContact();
                    Managers.ContactManager ContactManager = new Managers.ContactManager();

                    string CustomerID = Customer.CustomerNo;
                    customerContact = ContactManager.GetCustomerContactDetails(CustomerID, ContactID);
                    Email eml = customerContact.EmailList.FirstOrDefault(x => (x.Value == EmailID.ToLower() && x.TypeID == Convert.ToInt32(SelectedEmailType)));
                    if (eml != null)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Email with same type \nalready exists.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    Email emailDetail = new Email();
                    emailDetail.ContactID = ContactID;
                    emailDetail.Value = EmailID;
                    emailDetail.Type = SelectedEmailType;
                    emailDetail.IsDefault = false;

                    ContactManager.SaveContactEmail(emailDetail, CustomerID);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    customerContact.ContactID = ContactID;
                    int id = Convert.ToInt32(customerContact.ContactID);
                    GetCustomerContacts(customerContact);
                    CustomerContactList[id].IsSelected = true;
                    customerContact = CustomerContactList.FirstOrDefault(t => t.ContactID == ContactID);
                    CustomerDashboardVM.SetDefaultContact(customerContact);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][SaveNewEmail][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                //ResetNewContact();
                //Models.CustomerContact con = CustomerContactList.First(CT => CT.ContactID == ContactID && CT.CustomerID == CustomerID);
                //int index = CustomerContactList.IndexOf(con);
                //CustomerContactList[index].IsExpanded = true;
            });
            #endregion

            #region SelectContact
            SelectContact = new DelegateCommand((Selected) =>
            {
                bool select = (bool)Selected;
                if (!select)
                {
                    SelectAllCheck = select;
                }
                else
                {
                    bool isAllSelected = true;
                    foreach (CustomerContact cust in CustomerContactList)
                    {
                        if (!cust.IsSelected && !cust.IsDefault)
                        {
                            isAllSelected = false;
                        }
                    }
                    SelectAllCheck = isAllSelected == true ? true : false;
                }
            });
            #endregion

            #region SelectAllContacts
            SelectAllContacts = new DelegateCommand((SelectAll) =>
            {
                bool select = (bool)SelectAll;
                foreach (CustomerContact cust in CustomerContactList)
                {
                    if (!cust.IsDefault)
                    {
                        cust.IsSelected = select;
                    }
                }
            });
            #endregion

            #region DeleteContactFromContactList
            DeleteContactFromContactList = new DelegateCommand((param) =>
            {
                List<CustomerContact> contactList = CustomerContactList.Where(contact => contact.IsSelected == true).ToList<CustomerContact>();
                int selectedContacts = contactList.Count();
                try
                {
                    var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteContactConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                    switch (selectedContacts)
                    {
                        case 0:
                            var alertMessage = new Helpers.AlertWindow { Message = "Please select contact to delete!", MessageIcon = "Alert" };
                            Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                            return;
                            break;
                        case 1:
                            confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteContactConfirmation, MessageIcon = "Alert", Confirmed = false };
                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                            break;
                        default:
                            confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteContactConfirmationForMany, MessageIcon = "Alert", Confirmed = false };
                            Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                            break;
                    }

                    if (!confirmMessage.Confirmed) return;

                    Managers.ContactManager manager = new Managers.ContactManager();
                    Models.CustomerContact customerContact = new Models.CustomerContact();
                    customerContact.CustomerID = Customer.CustomerNo;

                    for (int index = 0; index < selectedContacts; index++)
                    {
                        if (contactList[index].ShowOnDashboard)
                        {
                            List<CustomerContact> contactListDefault = CustomerContactList.Where(contact => contact.IsDefault == true).ToList<CustomerContact>();
                            foreach (CustomerContact item in contactListDefault)
                            {
                                if (item.IsDefault)
                                {
                                    item.ShowOnDashboard = true;
                                    new Managers.ContactManager().SetContactAsDefault(item);
                                    CustomerDashboardVM.SetDefaultContact(item);
                                    manager.SetContactAsDefault(item);
                                }
                            }
                        }
                        customerContact.ContactID = contactList[index].ContactID;
                        manager.DeleteCustomerContact(customerContact);
                    }

                    GetCustomerContacts(customerContact);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][DeleteContactFromContactList][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }

            });
            #endregion

            #region AddNewContact
            AddNewContact = new DelegateCommand((param) =>
            {
                IsAddContact = true;
                IsPhoneTypeSelected = false;
                IsEmailTypeSelected = false;
                IsTitleTypeSelected = false;
                IsAddEmail = IsAddPhone = IsEditContact = false;
                ResetNewContact();
                ToggleAddButton = false;
                IsTitleSelectionEnabled = true;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewContact", Title = "Add New Contact" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion

            #region AddNewPhone
            AddNewPhone = new DelegateCommand((param) =>
            {
                IsAddContact = IsAddEmail = IsEditContact = false;
                IsAddPhone = true;
                IsPhoneTypeSelected = false;

                ResetNewContact();
                Title customerTitle = param as Title;
                Phone customerPhone = param as Phone;
                CustomerContact customerContact = param as CustomerContact;
                Managers.ContactManager manager = new Managers.ContactManager();
                string customerID = Customer.CustomerNo;

                //customerContact = manager.GetContactNameAndTitle(ContactID, customerID);
                ContactID = customerContact.ContactID;
                FirstName = customerContact.FirstName;
                MiddleName = customerContact.MiddleName;
                LastName = customerContact.LastName;

                string titleCode = customerContact.ContactTitle == null ? "" : (customerContact.ContactTitle);
                foreach (ContactType item in TitleType)
                {
                    if (item.Type == "CONTACT" && item.Description == titleCode)
                    {
                        SelectedTitleType = item.TypeID;
                        break;
                    }
                }

                PhonePart1 = string.Empty;
                PhonePart2 = string.Empty;
                Extension = string.Empty;
                AreaCode = string.Empty;
                ToggleAddButton = false;
                IsTitleSelectionEnabled = false;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewPhone", Title = "Add New Phone Number" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion

            #region AddNewEmail
            AddNewEmail = new DelegateCommand((param) =>
            {
                IsAddPhone = IsAddContact = IsEditContact = false;
                IsAddEmail = true;
                IsEmailTypeSelected = false;

                ResetNewContact();
                CustomerContact customerContact = param as CustomerContact;
                Managers.ContactManager manager = new Managers.ContactManager();
                string customerID = Customer.CustomerNo;

                ContactID = customerContact.ContactID;
                FirstName = customerContact.FirstName; //string.IsNullOrEmpty(customerContact.ContactName) ? "" : customerContact.ContactName.ToString();
                MiddleName = customerContact.MiddleName;
                LastName = customerContact.LastName;

                string titleCode = customerContact.ContactTitle == null ? "" : (customerContact.ContactTitle);
                foreach (ContactType item in TitleType)
                {
                    if (item.Type == "CONTACT" && item.Description == titleCode)
                    {
                        SelectedTitleType = item.TypeID;
                        break;
                    }
                }

                EmailID = string.Empty;
                ToggleAddButton = false;
                IsTitleSelectionEnabled = false;
                var dialog = new Helpers.DialogWindow { TemplateKey = "AddNewEmail", Title = "Add New Email" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);

            });
            #endregion

            #region EditContact
            // command on double tap for Edit CustomerContactsViewModel Pop-up
            EditContact = new DelegateCommand((param) =>
            {

                CustomerContact customerContact = param as CustomerContact;
                
                try
                {
                    IsDefaultCon = customerContact.IsDefault;
                    FirstName = customerContact.FirstName.Trim();
                    MiddleName = customerContact.MiddleName.Trim();
                    LastName = customerContact.LastName.Trim();
                    ContactID = customerContact.ContactID;

                    string titleCode = customerContact.ContactTitle == null ? "" : (customerContact.ContactTitle);
                    foreach (ContactType item in TitleType)
                    {
                        if (item.Type == "CONTACT" && item.Description == titleCode)
                        {
                            SelectedTitleType = item.TypeID;
                            break;
                        }
                    }

                    if (customerContact.PhoneList.Count > 0)
                    {
                        //if (customerContact.DefaultPhone != null)
                        //{
                        //    SelectedPhoneType = customerContact.DefaultPhone.TypeID.ToString();
                        //    AreaCode = customerContact.DefaultPhone.AreaCode;
                        //    PhonePart1 = customerContact.DefaultPhone.Value.Split('-')[0];
                        //    PhonePart2 = customerContact.DefaultPhone.Value.Split('-')[1];
                        //    Extension = customerContact.DefaultPhone.Extension;
                        //}
                        //else
                        //{
                            SelectedPhoneType = customerContact.PhoneList == null ? "" : (customerContact.PhoneList.Count == 0 ? "" : customerContact.PhoneList[0].TypeID.ToString());
                            AreaCode = customerContact.PhoneList[0].AreaCode;
                            PhonePart1 = customerContact.PhoneList[0].Value.Split('-')[0];
                            PhonePart2 = customerContact.PhoneList[0].Value.Split('-')[1];
                            Extension = customerContact.PhoneList[0].Extension;
                        //}
                    
                        //AreaCode = customerContact.PhoneList[0].AreaCode;
                        //PhonePart1 = customerContact.PhoneList[0].Value.Split('-')[0];
                        //PhonePart2 = customerContact.PhoneList[0].Value.Split('-')[1];
                        //Extension = customerContact.PhoneList[0].Extension;
                    }

                    if (customerContact.EmailList.Count > 0)
                    {
                        
                        //if (customerContact.DefaultEmail != null)
                        //{
                        //    SelectedEmailType = customerContact.DefaultEmail.TypeID.ToString();
                        //    EmailID = customerContact.DefaultEmail.Value;
                        //}
                        //else
                        //{
                            SelectedEmailType = customerContact.EmailList == null ? null : (customerContact.EmailList.Count == 0 ? "" : customerContact.EmailList[0].TypeID.ToString());
                            EmailID = customerContact.EmailList[0].Value;
                        //}
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][EditContact][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                IsTitleSelectionEnabled = true;
                ToggleAddButton = false;
                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingContact", Title = "Edit Contact" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                if (dialog.Cancelled || dialog.Confirmed)
                {
                    //SelectedContactType = null;
                    PhoneTypeID = -1;
                    EmailTypeID = -1;
                    TitleTypeID = -1;
                }
               // ResetNewContact();
            });
            #endregion

            #region EditPhone
            EditPhone = new DelegateCommand((param) =>
            {
                CustomerContact customerContact = new CustomerContact();
                Managers.ContactManager manager = new Managers.ContactManager();
                Phone customerPhone = param as Phone;
                selectedPhone = customerPhone;
                int PhoneTypeID = 0;
                string customerID = Customer.CustomerNo;
                try
                {
                    ContactID = customerPhone.ContactID;
                    customerContact = manager.GetContactNameAndTitle(ContactID, customerID);
                    Index = customerPhone.Index;
                    FirstName = string.IsNullOrEmpty(customerContact.FirstName) ? "" : customerContact.FirstName.ToString().Trim();
                    MiddleName = string.IsNullOrEmpty(customerContact.MiddleName) ? "" : customerContact.MiddleName.ToString().Trim();
                    LastName = string.IsNullOrEmpty(customerContact.LastName) ? "" : customerContact.LastName.ToString().Trim();
                    SelectedTitleType = customerContact.ContactTitle == null ? "" : (customerContact.ContactTitle);
                    SelectedPhoneType = customerPhone.TypeID.ToString();
                    string phoneCode = customerPhone.Type;
                    AreaCode = customerPhone.AreaCode;
                    PhonePart1 = customerPhone.Value.Split('-')[0];
                    PhonePart2 = customerPhone.Value.Split('-')[1];
                    Extension = customerPhone.Extension;
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][EditPhone][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                IsTitleSelectionEnabled = false;
                ToggleAddButton = false;
                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingPhone", Title = "Edit Phone" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                if (dialog.Cancelled || dialog.Confirmed)
                {
                    PhoneTypeID = -1;
                    EmailTypeID = -1;
                    TitleTypeID = -1;
                }
                IsTitleSelectionEnabled = false;
            });
            #endregion

            #region EditEmail
            EditEmail = new DelegateCommand((param) =>
            {
                Email customerEmail = param as Email;
                selectedEmail = customerEmail;
                CustomerContact customerContact = new CustomerContact();
                Managers.ContactManager manager = new Managers.ContactManager();
                int EmailTypeID = 0;
                try
                {
                    string customerID = Customer.CustomerNo;
                    ContactID = customerEmail.ContactID;
                    Index = customerEmail.Index;
                    customerContact = manager.GetContactNameAndTitle(ContactID, customerID);
                    FirstName = customerContact.FirstName.ToString().Trim();
                    MiddleName = customerContact.MiddleName.ToString().Trim();
                    LastName = customerContact.LastName.ToString().Trim();
                    SelectedEmailType = customerEmail.TypeID.ToString();
                    SelectedTitleType = customerContact.ContactTitle == null ? "" : (customerContact.ContactTitle);
                    EmailID = customerEmail.Value;

                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][EditEmail][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
                IsTitleSelectionEnabled = false;
                ToggleAddButton = false;
                var dialog = new Helpers.DialogWindow { TemplateKey = "EditExistingEmail", Title = "Edit Email" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialog, MessageToken);
                if (dialog.Cancelled || dialog.Confirmed)
                {
                    //SelectedContactType = null;
                    PhoneTypeID = -1;
                    EmailTypeID = -1;
                    TitleTypeID = -1;
                }
             //   ResetNewContact();
            });
            #endregion

            #region SaveEditedContact
            // command to save edited contact
            SaveEditedContact = new DelegateCommand((param) =>
            {
                //double result;
                //bool flag;
                IsAddEmail = IsAddContact = IsAddPhone = false;
                IsEditContact = true;
                Models.CustomerContact customerContactItem = new CustomerContact();
                Managers.ContactManager ContactManager = new Managers.ContactManager();

                customerContactItem.ContactID = ContactID;
                customerContactItem.FirstName = FirstName;
                customerContactItem.MiddleName = MiddleName;
                customerContactItem.LastName = LastName;
                customerContactItem.ContactName = customerContactItem.FirstName + " " + customerContactItem.MiddleName + " " + customerContactItem.LastName;
                customerContactItem.ContactTitle = SelectedTitleType;
                customerContactItem.CustomerID = Customer.CustomerNo;
                customerContactItem.ShowOnDashboard = false;

                CustomerContact customerContact = ContactManager.GetCustomerContactDetails(customerContactItem.CustomerID, ContactID);
                Phone phone = null;
                Email email = null;
                if (customerContact.PhoneList.Count > 0)
                {
                    customerContact.PhoneList.RemoveAt(0);
                    //Phone phn = customerContact.PhoneList.FirstOrDefault(x => (x.Index == Index));
                    //customerContact.PhoneList.Remove(phn);
                    phone = customerContact.PhoneList.FirstOrDefault(x => (x.AreaCode == AreaCode && x.Value == Phone && x.TypeID == Convert.ToInt32(SelectedPhoneType)));
                    //customerContact.PhoneList.Remove(phone);
                }
                if (customerContact.EmailList.Count > 0)
                {
                    customerContact.EmailList.RemoveAt(0);
                    //Email eml = customerContact.EmailList.FirstOrDefault(x => (x.Index == Index));
                    //customerContact.EmailList.Remove(eml);
                    email = customerContact.EmailList.FirstOrDefault(x => (x.Value == EmailID.ToLower() && x.TypeID == Convert.ToInt32(SelectedEmailType)));
                    //customerContact.EmailList.Remove(email);
                }
                if (phone != null || email != null)
                {
                    string msg = phone == null ? "Email" : "Phone number";
                    var alertMessage = new Helpers.AlertWindow { Message = msg + " with same Type \nalready exists.", MessageIcon = "Alert" };
                    Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                    return;
                }
                Phone phoneDetail = new Phone();
                phoneDetail.AreaCode = AreaCode;
                phoneDetail.Value = Phone;
                phoneDetail.Extension = Extension;
                phoneDetail.TypeID = Convert.ToInt32(SelectedPhoneType);

                try
                {
                    phoneDetail.IsDefault = false;

                    Email emailDetail = new Email();
                    emailDetail.Value = EmailID;
                    emailDetail.TypeID = Convert.ToInt32(SelectedEmailType);
                    emailDetail.IsDefault = false;
                    
                    customerContactItem.EmailList = new ObservableCollection<Email>();
                    if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                    {
                        customerContactItem.EmailList.Add(emailDetail);
                    }
                    customerContactItem.PhoneList = new ObservableCollection<Phone>();
                    if (phoneDetail.Value.Trim().Length > 1)
                    {
                        customerContactItem.PhoneList.Add(phoneDetail);
                    }
                    ContactManager.SaveEditedContact(customerContactItem);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    int id = Convert.ToInt32(customerContactItem.ContactID);
                    GetCustomerContacts(customerContactItem);
                    CustomerContactList[id].IsSelected = true;
                    ResetNewContact();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][SaveEditedContact][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region SaveEditedEmail
            SaveEditedEmail = new DelegateCommand((param) =>
            {
                try
                {
                    Models.CustomerContact customerContactItem = new Models.CustomerContact();
                    Managers.ContactManager ContactManager = new Managers.ContactManager();
                    customerContactItem.ContactID = ContactID;
                    customerContactItem.CustomerID = Customer.CustomerNo;
                    CustomerContact customerContact = ContactManager.GetCustomerContactDetails(customerContactItem.CustomerID, ContactID);
                    Email eml = customerContact.EmailList.FirstOrDefault(x => (x.Index == Index));
                    customerContact.EmailList.Remove(eml);
                    Email email = customerContact.EmailList.FirstOrDefault(x => (x.Value == EmailID.ToLower() && x.TypeID == Convert.ToInt32(SelectedEmailType)));
                    if (email != null)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Email with same type \nalready exists.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    
                    Email emailDetail = new Email();
                    emailDetail.Index = Index;
                    emailDetail.Value = EmailID;
                    emailDetail.TypeID = Convert.ToInt32(SelectedEmailType);
                    
                    ContactManager.SaveEditedEmail(customerContactItem, Index);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    int id = Convert.ToInt32(customerContactItem.ContactID);
                    GetCustomerContacts(customerContactItem);
                    CustomerContactList[id].IsSelected = true;
                    //ResetNewContact();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][SaveEditedEmail][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region SaveEditedPhone
            SaveEditedPhone = new DelegateCommand((param) =>
            {
                try
                {
                    Models.CustomerContact customerContactItem = new Models.CustomerContact();
                    Managers.ContactManager ContactManager = new Managers.ContactManager();
                    customerContactItem.CustomerID = Customer.CustomerNo;
                    customerContactItem.ContactID = ContactID;
                    CustomerContact customerContact = ContactManager.GetCustomerContactDetails(customerContactItem.CustomerID, ContactID);
                    Phone phn = customerContact.PhoneList.FirstOrDefault(x => (x.Index == Index));
                    customerContact.PhoneList.Remove(phn);
                    Phone phone = customerContact.PhoneList.FirstOrDefault(x => (x.AreaCode == AreaCode && x.Value == Phone && x.TypeID == Convert.ToInt32(SelectedPhoneType)));
                    if (phone != null)
                    {
                        var alertMessage = new Helpers.AlertWindow { Message = "Phone number with same Type \nalready exists.", MessageIcon = "Alert" };
                        Messenger.Default.Send<Helpers.AlertWindow>(alertMessage, MessageToken);
                        return;
                    }
                    Phone phoneDetail = new Phone();
                    phoneDetail.Index = Index;
                    phoneDetail.AreaCode = AreaCode;
                    phoneDetail.Value = Phone;
                    phoneDetail.Extension = Extension;
                    phoneDetail.TypeID = Convert.ToInt32(SelectedPhoneType);

                    customerContactItem.PhoneList = new ObservableCollection<Phone>();
                    if (phoneDetail.Value.Trim().Length > 1)
                    {
                        customerContactItem.PhoneList.Add(phoneDetail);
                    }
                    customerContactItem.ContactID = ContactID;
                    customerContactItem.CustomerID = Customer.CustomerNo;
                    ContactManager.SaveEditedPhone(customerContactItem, phoneDetail.Index);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    int id = Convert.ToInt32(customerContactItem.ContactID);
                    GetCustomerContacts(customerContactItem);
                    CustomerContactList[id].IsSelected = true;
                    //ResetNewContact();
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][SaveEditedPhone][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                }
            });
            #endregion

            #region DeleteEmail
            DeleteEmail = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContactItem = new Models.CustomerContact();
                Managers.ContactManager ContactManager = new Managers.ContactManager();

                Email emailDetail = new Email();
                //emailDetail.Index = Index;
                emailDetail.Value = EmailID;
                if (string.IsNullOrEmpty(SelectedEmailType))
                {
                    emailDetail.Type = "Email Address";
                }
                else
                {
                    emailDetail.Type = SelectedEmailType;
                }
                if (emailDetail.Value != null && emailDetail.Value.Trim().Length > 1)
                {
                    customerContactItem.EmailList = new ObservableCollection<Email>();
                    customerContactItem.EmailList.Add(emailDetail);
                }
                customerContactItem.ContactID = ContactID;
                customerContactItem.CustomerID = Customer.CustomerNo;
                //Guid MessageToken1 = Guid.NewGuid();
                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeleteEmailConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                ContactManager.DeleteContactEmail(customerContactItem, Index);

              //  ContactManager.UpdateIndexAfterDelete(customerContactItem, Index);

                Models.CustomerContact selectedContact = CustomerContactList.First(CT => CT.ContactID == ContactID);
                ObservableCollection<Email> email = selectedContact.EmailList;
                Email selectedEmail = email.First(p => p.Index == Index);
                selectedContact.EmailList.Remove(selectedEmail);
                if (selectedEmail.IsSelectedEmail && email.Count > 0)
                {
                    selectedContact.EmailList[0].IsSelectedEmail = true;
                    selectedContact.DefaultEmail = selectedContact.EmailList[0];
                }
                else if (selectedEmail.IsSelectedEmail)
                {
                    selectedContact.DefaultEmail = new Email();
                }
                ContactManager.SetContactAsDefault(selectedContact);

                ContactManager.UpdateEmailIndexAfterDelete(selectedContact, Index);
                CustomerDashboardVM.SetDefaultContact(selectedContact);
                Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                GetCustomerContacts(customerContactItem);
            });
            #endregion

            #region DeletePhone
            DeletePhone = new DelegateCommand((param) =>
            {
                Models.CustomerContact customerContactItem = new Models.CustomerContact();
                Managers.ContactManager ContactManager = new Managers.ContactManager();
                customerContactItem.CustomerID = Customer.CustomerNo;
                customerContactItem.ContactID = ContactID;

                var confirmMessage = new Helpers.ConfirmWindow { Message = Helpers.Constants.Contacts.DeletePhoneConfirmation, MessageIcon = "Alert", Confirmed = false };
                Messenger.Default.Send<Helpers.ConfirmWindow>(confirmMessage, MessageToken);
                if (!confirmMessage.Confirmed) return;

                ContactManager.DeleteContactPhone(customerContactItem, Index);
                Models.CustomerContact selectedContact = CustomerContactList.First(CT => CT.ContactID == ContactID);
                ObservableCollection<Phone> ph = selectedContact.PhoneList;
                Phone selectedPhone = ph.First(p => p.Index == Index);
                selectedContact.PhoneList.Remove(selectedPhone);
                if (selectedPhone.IsSelectedPhone && ph.Count > 0)
                {
                    selectedContact.PhoneList[0].IsSelectedPhone = true;
                    selectedContact.DefaultPhone = selectedContact.PhoneList[0];
                }
                else if (selectedPhone.IsSelectedPhone)
                {
                    selectedContact.DefaultPhone = new Phone();
                }
                ContactManager.SetContactAsDefault(selectedContact);
                CustomerDashboardVM.SetDefaultContact(selectedContact);

                ContactManager.UpdatePhoneIndexAfterDelete(selectedContact, Index);
                Messenger.Default.Send<CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                GetCustomerContacts(customerContactItem);
            });
            #endregion

        }
        void GetCustomerContacts()
        {
            Managers.ContactManager ContactManager = new Managers.ContactManager();
            Models.CustomerContact customerContact = new Models.CustomerContact();
            try
            {
                ObservableCollection<CustomerContact> customerContactCollection = ContactManager.GetCustomerContacts(Customer.CustomerNo);
                if (customerContactCollection.Count > 0)
                {
                    if (CustomerContactList == null || CustomerContactList.Count == 0)
                    {
                        CustomerContactList = new ObservableCollection<CustomerContact>();
                        CustomerContactList.CollectionChanged += CustomerContactList_CollectionChanged;
                    }
                    else
                    {
                        CustomerContactList.Clear();
                    }
                    foreach (CustomerContact item in customerContactCollection)
                    {
                        if (item.EmailList.Count == 3)
                        {
                            item.ToggleEmailImg = false;
                        }
                        foreach (Email em in item.EmailList)
                        {
                            em.PropertyChanged += EmailSelected;
                        }
                        if (item.PhoneList.Count == 4)
                        {
                            item.TogglePhoneImg = false;
                        }
                        foreach (Phone em in item.PhoneList)
                        {
                            em.PropertyChanged += PhoneSelected;
                        }
                        CustomerContactList.Add(item);
                    }
                    //CustomerContactList.CollectionChanged += CustomerContactList_CollectionChanged;
                    OnPropertyChanged("CustomerContactList");
                }
                else
                {
                    if (CustomerContactList == null || CustomerContactList.Count == 0)
                    {
                        CustomerContactList = new ObservableCollection<CustomerContact>();
                    }
                    else
                    {
                        CustomerContactList.Clear();
                    }
                }
                SelectAllCheck = false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][GetCustomerContacts][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }
        void GetCustomerContacts(CustomerContact cust)
        {
            Managers.ContactManager ContactManager = new Managers.ContactManager();
            Models.CustomerContact customerContact = new Models.CustomerContact();

            try
            {
                ObservableCollection<CustomerContact> customerContactCollection = ContactManager.GetCustomerContacts(Customer.CustomerNo);

                if (customerContactCollection.Count > 0)
                {
                    //customerContactCollection = Managers.ContactManager.GetCustomerContacts(Customer.CustomerNo);
                    if (customerContactCollection.Count > CustomerContactList.Count)
                    {
                        //Add new entry
                        CustomerContact contactToBeAdded = new CustomerContact();

                        foreach (CustomerContact item in customerContactCollection)
                        {

                            if (!CustomerContactList.Any(d => d.ContactID == item.ContactID))
                            {
                                if (item.EmailList.Count == 3)
                                {
                                    item.ToggleEmailImg = false;
                                }
                                foreach (Email em in item.EmailList)
                                {
                                    em.PropertyChanged += EmailSelected;
                                }
                                if (item.PhoneList.Count == 4)
                                {
                                    item.TogglePhoneImg = false;
                                }
                                foreach (Phone em in item.PhoneList)
                                {
                                    em.PropertyChanged += PhoneSelected;
                                }
                                item.IsExpanded = true;
                                contactToBeAdded = item;
                                break;
                            }
                        }
                        CustomerContactList.Add(contactToBeAdded);
                    }
                    else if (customerContactCollection.Count < CustomerContactList.Count)
                    {
                        //Remove deleted entry
                        List<string> contactIdToRemove = new List<string>();
                        foreach (CustomerContact item in CustomerContactList)
                        {
                            if (!customerContactCollection.Any(d => d.ContactID == item.ContactID))
                            {
                                contactIdToRemove.Add(item.ContactID);
                                if (item.ShowOnDashboard)
                                {
                                    var defaultCon = CustomerContactList.FirstOrDefault(t => t.IsDefault);
                                    if (defaultCon.PhoneList != null && defaultCon.PhoneList.Count > 0)
                                    {
                                        defaultCon.PhoneList[0].IsSelectedPhone = true;
                                    }
                                    if (defaultCon.EmailList != null && defaultCon.EmailList.Count > 0)
                                    {
                                        defaultCon.EmailList[0].IsSelectedEmail = true;
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < contactIdToRemove.Count; i++)
                        {
                            CustomerContactList.Where(t => t.ContactID == contactIdToRemove[i]).ToList().ForEach(a => CustomerContactList.Remove(a));
                        }
                        //CustomerContactList.Remove(contactToBeRemove);
                    }
                    else if (customerContactCollection.Count == CustomerContactList.Count)
                    {
                        CustomerContact contactUpdated = customerContactCollection.First(t => t.ContactID == cust.ContactID && t.CustomerID == Customer.CustomerNo);
                        CustomerContact contactToBeUpdated = CustomerContactList.First(t => t.ContactID == cust.ContactID && t.CustomerID == Customer.CustomerNo);
                        int index = CustomerContactList.IndexOf(contactToBeUpdated);
                        CustomerContactList.Where(t => t.ContactID == contactToBeUpdated.ContactID).ToList().ForEach(a => CustomerContactList.Remove(a));

                        if (contactUpdated.EmailList.Count == 3)
                        {
                            contactUpdated.ToggleEmailImg = false;
                        }
                        foreach (Email em in contactUpdated.EmailList)
                        {
                            em.PropertyChanged += EmailSelected;
                        }
                        if (contactUpdated.PhoneList.Count == 4)
                        {
                            contactUpdated.TogglePhoneImg = false;
                        }
                        foreach (Phone em in contactUpdated.PhoneList)
                        {
                            em.PropertyChanged += PhoneSelected;
                        }
                        contactUpdated.IsExpanded = true;
                        //CustomerContactList.Add(contactUpdated);
                        CustomerContactList.Insert(index, contactUpdated);
                    }
                    CustomerContactList.OrderBy(t => Convert.ToInt16(t.ContactID));
                    CustomerContact contactOnDashboard = CustomerContactList.FirstOrDefault(c => c.ShowOnDashboard == true);
                    //if (contactOnDashboard != null)
                    //{
                     CustomerDashboardVM.SetDefaultContact((contactOnDashboard.DefaultPhone != null || contactOnDashboard.DefaultEmail != null) ? contactOnDashboard : null);
                    //}
                    OnPropertyChanged("CustomerContactList");
                }
                //else {
                //    CustomerContactList.Clear();
                //}
                SelectAllCheck = false;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels.CustomerContactsViewModel.][GetCustomerContacts][customerID =" + Customer.CustomerNo + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
        }
        void GetContactType()
        {
            Managers.ContactManager manager = new Managers.ContactManager();
            ObservableCollection<ContactType> types = manager.GetContactTypes();
            //NewContact = new CustomerContactsViewModel();
            List<ContactType> list = types.Where(item => item.Type.Trim().Equals("CONTACT")).ToList<ContactType>();
            TitleType = new ObservableCollection<ContactType>();
            foreach (var item in list)
            {
                TitleType.Add(item);
            }

            list = types.Where(item => item.Type.Trim().Equals("PHONE")).ToList<ContactType>();
            PhoneType = new ObservableCollection<ContactType>();
            foreach (var item in list)
            {
                PhoneType.Add(item);
            }

            list = types.Where(item => item.Type.Trim().Equals("EMAIL")).ToList<ContactType>();
            EmailType = new ObservableCollection<ContactType>();
            foreach (var item in list)
            {
                EmailType.Add(item);
            }
        }
        void ResetNewContact()
        {
            //Title = string.Empty;
            FirstName = string.Empty;
            MiddleName = string.Empty;
            LastName = string.Empty;
            SelectedEmailType = string.Empty;
            SelectedPhoneType = string.Empty;
            SelectedTitleType = string.Empty;
            PhoneTypeID = -1;
            EmailTypeID = -1;
            TitleTypeID = -1;
            EmailID = string.Empty;
            PhonePart1 = string.Empty;
            PhonePart2 = string.Empty;
            Extension = string.Empty;
            AreaCode = string.Empty;
        }

        Phone selectedPhone = null;
        Email selectedEmail = null;
        void PhoneSelected(object sender, PropertyChangedEventArgs e)
        {

            //// if (selectedPhone == (sender as Phone)) return;
            //selectedPhone = sender as Phone;
            //if (selectedEmail != null)
            //    if (selectedPhone.ContactID != selectedEmail.ContactID)
            //    {
            //        CustomerContact ct = CustomerContactList.First(item => item.ContactID == selectedEmail.ContactID);
            //        foreach (Email m in CustomerContactList.ElementAt(CustomerContactList.IndexOf(ct)).EmailList)
            //        {
            //            if (m.Index == selectedEmail.Index && m.IsSelected)
            //            {
            //                m.IsSelected = false;
            //                selectedEmail = null;
            //            }
            //        }
            //    }
        }
        void EmailSelected(object sender, PropertyChangedEventArgs e)
        {
            //if (selectedEmail == (sender as Email)) return;
            //selectedEmail = sender as Email;
            //if (selectedPhone != null)
            //    if (selectedEmail.ContactID != selectedPhone.ContactID)
            //{
            //    CustomerContact ct = CustomerContactList.First(item => item.ContactID == selectedPhone.ContactID);
            //    foreach (Phone m in CustomerContactList.ElementAt(CustomerContactList.IndexOf(ct)).PhoneList) {
            //        if (m.Index == selectedPhone.Index && m.IsSelected)
            //        {
            //            m.IsSelected = false;
            //            selectedPhone = null;
            //        }
            //    }
            //}
        }
        #endregion

        void CustomerContactList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //if (e.Action == NotifyCollectionChangedAction.Remove)
            //{
            //    foreach (CustomerContact item in e.OldItems)
            //    {
            //        item.PropertyChanged -= EntityViewModelPropertyChanged;
            //    }
            //}
            //else if (e.Action == NotifyCollectionChangedAction.Add)
            //{
            //    foreach (CustomerContact item in e.NewItems)
            //    {
            //        item.PropertyChanged += EntityViewModelPropertyChanged;
            //    }
            //}
        }
    }

    public class ContactVMArgs : EventArgs
    {
        public CustomerContact custContact { get; set; }
    }
}
