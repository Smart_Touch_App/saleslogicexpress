﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Domain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using Telerik.Windows.Controls;
using System.Windows.Threading;
using SalesLogicExpress.Application.Managers;
using SalesLogicExpress.Application.ViewModelPayload;
using System.ComponentModel;
using System.Data;

namespace SalesLogicExpress.Application.ViewModels
{
    public class MoneyOrderViewModel : BaseViewModel, IDataErrorInfo
    {
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Application.ViewModels.MoneyOrderViewModel");

        #region Properties

        private Guid _MessageToken = new System.Guid();
        public Guid MessageToken
        {

            get { return _MessageToken; }
            set
            {
                _MessageToken = value;
                OnPropertyChanged("MessageToken");
            }
        }

        private string routeID = string.Empty;
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; OnPropertyChanged("RouteID"); }
        }

        private string route;
        public string Route
        {
            get { return route; }
            set { route = value; OnPropertyChanged("Route"); }
        }
        private string routeName;
        public string RouteName
        {
            get { return routeName; }
            set { routeName = value; OnPropertyChanged("RouteName"); }
        }

        private string userRoute;
        public string UserRoute
        {
            get { return userRoute; }
            set { userRoute = value; OnPropertyChanged("UserRoute"); }
        }

        private ObservableCollection<MoneyOrder> moneyOrderList = new ObservableCollection<Domain.MoneyOrder>();

        public ObservableCollection<MoneyOrder> MoneyOrderList
        {
            get { return moneyOrderList; }
            set { moneyOrderList = value; OnPropertyChanged("MoneyOrderList"); }
        }
        private MoneyOrder moneyOrderObject = new Domain.MoneyOrder();

        public MoneyOrder MoneyOrderObject
        {
            get { return moneyOrderObject; }
            set { moneyOrderObject = value; OnPropertyChanged("MoneyOrderObject"); }
        }

        private Expenses expense = new Domain.Expenses();
        public Expenses Expense
        {
            get { return expense; }
            set { expense = value; OnPropertyChanged("Expense"); }
        }

        private ExpensesViewModel expenseVM = new ExpensesViewModel();
        public ExpensesViewModel ExpenseVM
        {
            get { return expenseVM; }
            set { expenseVM = value; OnPropertyChanged("ExpenseVM"); }
        }

        private ObservableCollection<ReasonCode> reasonCode = new ObservableCollection<ReasonCode>();
        public ObservableCollection<ReasonCode> ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; OnPropertyChanged("ReasonCode"); }
        }

        private int selectedReasonCode = -1;
        public int SelectedReasonCode
        {
            get { return selectedReasonCode; }
            set
            {
                selectedReasonCode = value;
                if (selectedReasonCode > 0)
                {
                    ToggleReasonCodeButton = true;
                }
                else ToggleReasonCodeButton = false;
                OnPropertyChanged("SelectedReasonCode");
            }
        }

        private bool isEditEnabled = false;
        public bool IsEditEnabled
        {
            get { return isEditEnabled; }
            set { isEditEnabled = value; OnPropertyChanged("IsEditEnabled"); }
        }

        private bool isVoidEnabled = false;
        public bool IsVoidEnabled
        {
            get { return isVoidEnabled; }
            set { isVoidEnabled = value; OnPropertyChanged("IsVoidEnabled"); }
        }

        private bool isAddEnabled = false;
        public bool IsAddEnabled
        {
            get { return isAddEnabled; }
            set { isAddEnabled = value; OnPropertyChanged("IsAddEnabled"); }
        }

        private bool toggleReasonCodeButton;
        public bool ToggleReasonCodeButton
        {
            get { return toggleReasonCodeButton; }
            set { toggleReasonCodeButton = value; OnPropertyChanged("ToggleReasonCodeButton"); }
        }

        private bool toggleAddButton = false;

        public bool ToggleAddButton
        {
            get { return toggleAddButton; }
            set { toggleAddButton = value; OnPropertyChanged("ToggleAddButton"); }
        }
        #endregion

        #region Domain level Properties
        private string moneyOrderNumber = string.Empty;
        public string MoneyOrderNumber
        {
            get { return moneyOrderNumber; }
            set
            {
                moneyOrderNumber = value;
                IsValidMONo = IsEmptyMONo = false;
                if (!string.IsNullOrEmpty(moneyOrderNumber))
                {
                    IsValidMONo = IsEmptyMONo = true;
                    if (!string.IsNullOrEmpty(Amount) && IsValidAmount && IsValidFee)
                    {
                        ToggleAddButton = true;
                    }
                    else
                        ToggleAddButton = false;
                }
                else
                {
                    IsEmptyMONo = false; ToggleAddButton = false;
                }
                OnPropertyChanged("MoneyOrderNumber");
            }
        }

        private string cashOnHand;
        public string CashOnHand
        {
            get { return cashOnHand; }
            set { cashOnHand = value; OnPropertyChanged("CashOnHand"); }
        }

        private string amount = string.Empty;
        public string Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                IsValidAmount = false;
                if (!string.IsNullOrEmpty(amount) && (Convert.ToDecimal(amount) > 0) && (Convert.ToDecimal(amount) <= Convert.ToDecimal(CashOnHand)))
                {
                    IsValidAmount = true;
                    if (!string.IsNullOrEmpty(MoneyOrderNumber) && IsValidMONo && IsValidFee)
                    {
                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    IsValidAmount = false; ToggleAddButton = false;
                }
                OnPropertyChanged("Amount");
                CalculateTotal();

            }
        }

        private string feeAmount = string.Empty;
        public string FeeAmount
        {
            get { return feeAmount; }
            set
            {
                feeAmount = value; //IsValidFee = false;
                if (!string.IsNullOrEmpty(feeAmount) && !string.IsNullOrEmpty(Amount) && IsValidAmount && !string.IsNullOrEmpty(MoneyOrderNumber))
                {
                    if (Convert.ToDecimal(feeAmount) >= Convert.ToDecimal(Amount))
                    {
                        IsValidFee = false;
                        ToggleAddButton = false;
                    }
                    else
                    {
                        ToggleAddButton = true; IsValidFee = true;
                    }
                }
                CalculateTotal();
                OnPropertyChanged("FeeAmount");
            }
        }

        private string totalAmount = string.Empty;
        public string TotalAmount
        {
            get { return totalAmount; }
            set
            {
                totalAmount = value; IsValidTotal = false;
                if (!string.IsNullOrEmpty(TotalAmount))
                {
                    if (Convert.ToDecimal(TotalAmount) > Convert.ToDecimal(CashOnHand))
                    {
                        IsValidTotal = false; ToggleAddButton = false;
                    }
                    else
                    {
                        IsValidTotal = true;// ToggleAddButton = true;
                    }
                }
                OnPropertyChanged("TotalAmount");
            }
        }

        bool IsValidFee = true;
        bool IsValidAmount = true;
        bool IsValidMONo = true;
        bool IsAddMO = false;
        bool IsValidTotal = true;
        bool IsEmptyMONo = true;
        public string Error
        {
            get
            {
                return null;
                //throw new NotImplementedException(); 
            }
        }
        public string this[string columnName]
        {
            get
            {
                string result = string.Empty;
                if (columnName == "Amount" && !IsValidAmount)
                {
                    result = Helpers.Constants.Common.ExpenseAmountErrorMsg;
                }
                if (columnName == "FeeAmount" && !IsValidFee)
                {
                    result = Helpers.Constants.Common.FeeAmountErrorMsg;
                }
                if (columnName == "MoneyOrderNumber" && !IsValidMONo)
                {
                    result = Helpers.Constants.Common.MoneyOrderNoErrorMsg;
                }
                if (columnName == "MoneyOrderNumber" && !IsEmptyMONo)
                {
                    result = Helpers.Constants.Common.MoneyOrderNoEmptyMsg;
                }
                if (columnName == "TotalAmount" && !IsValidTotal)
                {
                    result = Helpers.Constants.Common.TotalAmountErrorMsg;
                }
                return result;
            }
        }

        #endregion

        #region DelegateCommands
        public DelegateCommand OpenEditDialogForMO { get; set; }
        public DelegateCommand OpenVoidDialogForMO { get; set; }
        public DelegateCommand OpenAddDialogForMO { get; set; }
        public DelegateCommand SaveNewMoneyOrder { get; set; }
        public DelegateCommand VoidMoneyOrder { get; set; }
        public DelegateCommand SaveEditedMoneyOrder { get; set; }
        public DelegateCommand SelectionItemInvoke { get; set; }
        #endregion

        #region Constructor
        public MoneyOrderViewModel()
        {
            InitializeCommands();
            LoadData();
        }
        #endregion

        #region Methods

        MoneyOrderManager mOManager = new MoneyOrderManager();
        public void CalculateTotal()
        {
            if (!string.IsNullOrEmpty(Amount))
            {
                if (string.IsNullOrEmpty(FeeAmount))
                    FeeAmount = "0.00";
                TotalAmount = (Convert.ToDecimal(Amount) + Convert.ToDecimal(FeeAmount)).ToString("F2");

            }
        }
        public void InitializeCommands()
        {
            Logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][Start:InitializeCommands]");

            #region Open dialog for add MO
            OpenAddDialogForMO = new DelegateCommand((param) =>
            {
                MoneyOrderNumber = string.Empty;
                Amount = string.Empty;
                TotalAmount = string.Empty;
                FeeAmount = string.Empty;
                IsAddMO = IsValidMONo = IsEmptyMONo = true;
                IsValidAmount = IsValidFee = IsValidTotal = true; ToggleAddButton = false;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "AddMoneyOrder", Title = "Add New Money Order" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
            });
            #endregion

            #region dialog box for edit MO
            OpenEditDialogForMO = new DelegateCommand((param) =>
            {
                IsAddMO = false;
                CashOnHand = (Convert.ToDecimal(CashOnHand) + Convert.ToDecimal(MoneyOrderObject.TotalAmount)).ToString("F2");
                MoneyOrderNumber = MoneyOrderObject.MoneyOrderNumber.ToString();
                Amount = Convert.ToDecimal(MoneyOrderObject.MoneyOrderAmount).ToString("F2");
                FeeAmount = string.IsNullOrEmpty(MoneyOrderObject.FeeAmount) ? "0.00" : Convert.ToDecimal(MoneyOrderObject.FeeAmount).ToString("F2");
                TotalAmount = string.IsNullOrEmpty(MoneyOrderObject.TotalAmount) ? "0.00" : Convert.ToDecimal(MoneyOrderObject.TotalAmount).ToString("F2");
                //IsValidAmount = false; 
                ToggleAddButton = false;
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "EditMoneyOrder", Title = "Edit Money Order" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
                if (dialogWindow.Cancelled)
                {
                    CashOnHand = (Convert.ToDecimal(CashOnHand) - Convert.ToDecimal(MoneyOrderObject.TotalAmount)).ToString("F2");
                }
            });
            #endregion

            #region dialog box for void MO
            OpenVoidDialogForMO = new DelegateCommand((param) =>
            {
                ReasonCode = mOManager.GetVoidReasonCode();
                var dialogWindow = new Helpers.DialogWindow { TemplateKey = "VoidMoneyOrder", Title = "Void Money Order" };
                Messenger.Default.Send<Helpers.DialogWindow>(dialogWindow, MessageToken);
            });
            #endregion

            #region Save new MO
            SaveNewMoneyOrder = new DelegateCommand((param) =>
            {
                try
                {
                    MoneyOrderObject.MoneyOrderNumber = MoneyOrderNumber.ToString();
                    IsValidMONo = mOManager.CheckMONumber(MoneyOrderObject.MoneyOrderNumber, true);
                    if (!IsValidMONo)
                    {
                        OnPropertyChanged("MoneyOrderNumber"); ToggleAddButton = false;
                        return;
                    }

                    MoneyOrderObject.MoneyOrderAmount = Convert.ToDecimal(Amount).ToString("F2");
                    MoneyOrderObject.FeeAmount = string.IsNullOrEmpty(FeeAmount) ? string.Empty : Convert.ToDecimal(FeeAmount).ToString("F2");
                    MoneyOrderObject.TotalAmount = (Convert.ToDecimal(Amount) + Convert.ToDecimal(FeeAmount)).ToString("F2");
                    MoneyOrderObject.Status = "UNSETTLED";
                    MoneyOrderObject.StatusCode = StatusTypesEnum.USTLD.ToString();
                    MoneyOrderObject.VoidReasonID = string.Empty;
                    MoneyOrderObject.VoidReason = string.Empty;
                    MoneyOrderObject.RouteSettlementId = string.Empty;
                    MoneyOrderObject.RouteID = CommonNavInfo.RouteID.ToString();
                    MoneyOrderObject.MoneyOrderDate = DateTime.Now;

                    MoneyOrder obj = new Domain.MoneyOrder();
                    obj = mOManager.AddMoneyOrder(MoneyOrderObject);
                    if (obj != null)
                    {
                        MoneyOrderObject.StatusID = obj.StatusID;
                        MoneyOrderObject.MoneyOrderID = obj.MoneyOrderID;
                    }

                    if (Convert.ToDecimal(MoneyOrderObject.FeeAmount) > Convert.ToDecimal(0))
                    {
                        GenerateExpense(MoneyOrderObject);
                        Expense = new Managers.ExpensesManager().AddExpenses(Expense);
                        ExpenseVM.ExpensesObject = Expense;
                        ExpenseVM.LogActivityForExpense(ActivityKey.CreateExpense);
                    }
                    LogActivity(ActivityKey.CreateMoneyOrder);
                    CashOnHand = (Convert.ToDecimal(CashOnHand) - Convert.ToDecimal(MoneyOrderObject.MoneyOrderAmount)).ToString("F2");
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);

                    GetMoneyOrderList();
                }
                catch (System.Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][SaveNewMoneyOrder][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");

                    throw;
                }
            });
            #endregion

            #region Void MO
            VoidMoneyOrder = new DelegateCommand((param) =>
            {
                try
                {
                    MoneyOrderObject.VoidReasonID = SelectedReasonCode.ToString();
                    string statusID = mOManager.UpdateMoneyOrder(MoneyOrderObject, false).ToString();
                    MoneyOrderObject.StatusID = string.IsNullOrEmpty(statusID) ? MoneyOrderObject.StatusID : statusID.Trim();
                    MoneyOrderObject.Status = "VOID";
                    MoneyOrderObject.StatusCode = StatusTypesEnum.VOID.ToString();

                    int count = Convert.ToInt32(DbEngine.ExecuteScalar("select count(isnull(ExpenseId, 0)) from BUSDTA.ExpenseDetails where ExpenseDetails.TransactionId = '" + MoneyOrderObject.MoneyOrderID + "'"));
                    if (count != 0)
                    {
                        Expense = mOManager.UpdateExpense(MoneyOrderObject, false);
                        ExpenseVM.ExpensesObject = Expense;
                        ExpenseVM.LogActivityForExpense(ActivityKey.VoidExpense);
                    }

                    LogActivity(ActivityKey.VoidMoneyOrder);
                    CashOnHand = (Convert.ToDecimal(CashOnHand) + Convert.ToDecimal(MoneyOrderObject.TotalAmount)).ToString("F2");
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    IsEditEnabled = IsVoidEnabled = false;
                    GetMoneyOrderList();

                }
                catch (System.Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][VoidExpenses][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                    throw;
                }
            });
            #endregion

            #region Save Edited MO
            SaveEditedMoneyOrder = new DelegateCommand((param) =>
            {
                try
                {
                    bool mOchanged = false;//string.Empty;
                    if (MoneyOrderObject.MoneyOrderNumber != MoneyOrderNumber.ToString())
                    {
                        mOchanged = true;
                    }

                    IsValidMONo = mOManager.CheckMONumber(MoneyOrderNumber, mOchanged);
                    if (!IsValidMONo)
                    {
                        OnPropertyChanged("MoneyOrderNumber"); ToggleAddButton = false;
                        return;
                    }
                    else
                        MoneyOrderObject.MoneyOrderNumber = MoneyOrderNumber.ToString();

                    MoneyOrderObject.MoneyOrderAmount = Convert.ToDecimal(Amount).ToString("F2");
                    MoneyOrderObject.FeeAmount = string.IsNullOrEmpty(FeeAmount) ? "0.00" : Convert.ToDecimal(FeeAmount).ToString("F2");
                    MoneyOrderObject.TotalAmount = (Convert.ToDecimal(Amount) + Convert.ToDecimal(FeeAmount)).ToString("F2");
                    mOManager.UpdateMoneyOrder(MoneyOrderObject, true).ToString();
                    CashOnHand = (Convert.ToDecimal(CashOnHand) - Convert.ToDecimal(MoneyOrderObject.TotalAmount)).ToString("F2");

                    int count = Convert.ToInt32(DbEngine.ExecuteScalar("select count(isnull(ExpenseId, 0)) from BUSDTA.ExpenseDetails where TransactionId = '" + MoneyOrderObject.MoneyOrderID + "'"));
                    if (count == 0)
                    {
                        if (Convert.ToDecimal(MoneyOrderObject.FeeAmount) > Convert.ToDecimal(0))
                        {
                            GenerateExpense(MoneyOrderObject);
                            Expense = new Managers.ExpensesManager().AddExpenses(Expense);
                        }
                    }
                    else
                    {
                        Expense = mOManager.UpdateExpense(MoneyOrderObject, true);
                    }
                    if (Expense != null)
                    {
                        ExpenseVM.ExpensesObject = Expense;
                        ExpenseVM.LogActivityForExpense(ActivityKey.EditExpense);
                    }

                    LogActivity(ActivityKey.EditMoneyOrder);
                    Messenger.Default.Send<Helpers.CloseDialogWindow>(new CloseDialogWindow(), MessageToken);
                    IsEditEnabled = IsVoidEnabled = false;

                    GetMoneyOrderList();
                }
                catch (System.Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][SaveEditedMoneyOrder][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                    throw;
                }
            });
            #endregion

            #region On Item selection
            SelectionItemInvoke = new DelegateCommand((param) =>
            {
                MoneyOrderObject = param as MoneyOrder;

                if (MoneyOrderObject.Status == "UNSETTLED")
                {
                    IsEditEnabled = true; IsVoidEnabled = true;
                }
                else if (MoneyOrderObject.Status == "VOID" || MoneyOrderObject.Status == "SETTLED" || MoneyOrderObject.Status == "VOIDED / SETTLED")
                {
                    IsEditEnabled = false; IsVoidEnabled = false;
                }

            });
            #endregion

            Logger.Info("[SalesLogicExpress.Application.ViewModels][MoneyOrderViewModel][End:InitializeCommands]");

        }
        private async void LoadData()
        {
            await Task.Run(() =>
            {
                this.MessageToken = CommonNavInfo.MessageToken;
                RouteID = CommonNavInfo.RouteID.ToString();
                Route = PayloadManager.ApplicationPayload.Route;
                UserRoute = Managers.UserManager.UserRoute;
                RouteName = Managers.UserManager.RouteName;
                CashOnHand = PayloadManager.RouteSettlementPayload.TotalCash.ToString("F2").Trim();
                if (Convert.ToDecimal(CashOnHand) > 0)
                {
                    IsAddEnabled = true;
                }
                GetMoneyOrderList();
            });
        }
        public void GetMoneyOrderList()
        {
            MoneyOrderList = new ObservableCollection<Domain.MoneyOrder>();
            ObservableCollection<MoneyOrder> moList = mOManager.GetMoneyOrderList();
            foreach (MoneyOrder item in moList)
            {
                if (Convert.ToDecimal(item.MoneyOrderAmount) != Convert.ToDecimal(0))
                {
                    MoneyOrderList.Add(item);
                }
            }
            moList.Clear();

        }
        public void LogActivity(ActivityKey key)
        {
            Activity ac = new Activity
            {
                ActivityType = key.ToString(),
                ActivityStart = DateTime.Now,
                ActivityEnd = DateTime.Now,
                IsTxActivity = true,
                ActivityDetailClass = MoneyOrderObject.ToString(),
                ActivityDetails = MoneyOrderObject.SerializeToJson(),
                RouteID = this.RouteID
            };
            if (key.ToString().ToLower() == "CreateMoneyOrder".ToString().ToLower())
            {
                ac.ActivityStatus = "USTLD";
            }
            else if (key.ToString().ToLower() == "VoidMoneyOrder".ToString().ToLower())
            {
                ac.ActivityStatus = "VOID";
            }

            //Get the parent activity id
            ac.ActivityHeaderID = this.GetParentActivity(this.MoneyOrderObject.MoneyOrderID.ToString());
            Activity activity = ResourceManager.Transaction.LogActivity(ac);
        }
        private string GetParentActivity(string moneyOrderID)
        {
            string query = "";

            try
            {
                query = string.Format("SELECT TDID FROM BUSDTA.M50012 WHERE TDTYP='MoneyOrder' AND TDDTLS LIKE '%\"MoneyOrderId\":\"{0}\"%' AND TDPNTID=0", moneyOrderID);

                return DbEngine.ExecuteScalar(query);

            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Application.ViewModels][ExpensesViewModel][GetParentActivity][ExceptionStackTrace = " + ex.StackTrace + "][Exception Message=" + ex.Message + "]");
                return string.Empty;
            }
        }
        public void GenerateExpense(MoneyOrder order)
        {

            Expense.TransactionId = string.IsNullOrEmpty(order.MoneyOrderID) ? string.Empty : order.MoneyOrderID;
            Expense.VoidReasonID = string.Empty;
            Expense.ExpenseAmount = order.FeeAmount;
            Expense.RouteSettlementId = string.Empty;
            Expense.Explanation = "Money order charges for " + order.MoneyOrderNumber;
            Expense.ExpenseDate = DateTime.Now;
            DataSet cat = DbEngine.ExecuteDataSet("select CategoryTypeID, CategoryTypeDESC from BUSDTA.Category_Type where CategoryTypeCD = 'MOCHR'");
            DataRow row = cat.Tables[0].Rows[0];
            Expense.CategotyID = string.IsNullOrEmpty(row["CategoryTypeID"].ToString()) ? string.Empty : row["CategoryTypeID"].ToString();
            Expense.ExpenseCategory = string.IsNullOrEmpty(row["CategoryTypeDESC"].ToString()) ? string.Empty : row["CategoryTypeDESC"].ToString();

        }

        #endregion

    }
}
