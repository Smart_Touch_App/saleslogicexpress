﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class LoadSuggetionsPickReport : CommanInReplenishmentReport
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.LoadSuggetionsPickReport]");
        #endregion
        #region Constructor
        public LoadSuggetionsPickReport(IList items)
        {
            try
            {
                log.Info("[Start:LoadSuggetionsPickReport]");
                ReportTitle = "ROUTE REPLENISHMENT-PICK/FINAL";
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                UserName = CommonNavInfo.UserName;
                Status = "IN PICKING";
                RouteNoAndName = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                TransferNo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                TransferFrom = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromBranch;
                TransferTo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToBranch;
                FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate.ToString("MM'/'dd'/'yyyy");
                ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate.ToString("MM'/'dd'/'yyyy");
                Items = items;
                if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                {
                    ReportSource = new ReportViewModel(this, Reports.LoadSuggetionsPickReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                else
                {
                    ReportSource = new ReportViewModel(this, Reports.LoadSuggetionsPickReportDC, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[LoadSuggetionsPickReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:LoadSuggetionsPickReport]");
        }
        #endregion
    }
}
