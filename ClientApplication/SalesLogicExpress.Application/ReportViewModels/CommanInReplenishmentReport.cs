﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class CommanInReplenishmentReport : ViewModels.BaseViewModel
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.CommanInReplenishmentReport]");
        public string UserName { get; set; }
        public string Status { get; set; }
        public string RouteNoAndName { get; set; }
        public string ReportTitle { get; set; }
        public string ReportDate { get; set; }
        public string TransferNo { get; set; }
        public string TransferFrom { get; set; }
        public string TransferTo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public IList Items { get; set; }
        public InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }

        #endregion

        #region Constructor
        public CommanInReplenishmentReport()
        {
            try
            {
                log.Info("[Start:CommanInReplenishmentReport]");
            }
            catch (Exception ex)
            {
                log.Error("[CommanInReplenishmentReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:CommanInReplenishmentReport]");
        }
        #endregion

    }
}
