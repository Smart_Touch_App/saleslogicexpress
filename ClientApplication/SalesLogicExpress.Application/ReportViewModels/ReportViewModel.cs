﻿using log4net;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ReportViewModels;
using SalesLogicExpress.Reporting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class ReportViewModel : ViewModelBase
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.ReportViewModel]");
        private int result;
        InstanceReportSource reportsource;

        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        #endregion

        #region Constructor
        public ReportViewModel(object objForReport, Reports reportType, bool PrintLabels, bool SaveInDB)
        {

            try
            {
                log.Info("[Start:ReportViewModel][Parameters:objForReport:" + objForReport.ToString() + ",reportType:" + reportType.ToString() + ",PrintLabels:" + PrintLabels.ToString() + ",SaveInDB:" + SaveInDB.ToString() + "]");
                Helpers.ReportDataSource ReportDetails = new Helpers.ReportDataSource(objForReport);
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("PrintLabels", PrintLabels);
                ReportSource = ReportManager.GetReport(reportType, ReportDetails, parameters);
                string fileName = "SO-" + ViewModels.Order.OrderId.ToString() + ".pdf";
                ReportManager.GenerateReportRunTime(reportType.ToString(), fileName, parameters);

                if (SaveInDB)
                {
                    SaveReportInDB("");
                }

                ///Tempararily commented
                //GenerateReportFromDB();
            }
            catch (Exception ex)
            {
                log.Error("[ReportViewModel][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:ReportViewModel]");
        }
        #endregion

        #region Methods
        public void SaveReportInDB(string fileName)
        {

            try
            {
                log.Info("[Start:SaveReportInDB][Parameters:fileName:" + fileName.ToString() + "]");
                string filePath = @"C:\Data\BitBucket\MobileSales2\ClientApplication\SalesLogicExpress.Presentation\bin\x64\Debug\PickSlipReport.pdf";
                Dictionary<string, object> param = new Dictionary<string, object>();

                FileStream fStream = File.OpenRead(filePath);
                byte[] contents = new byte[fStream.Length];
                fStream.Read(contents, 0, (int)fStream.Length);
                fStream.Close();
                param.Add("@data", contents);

                string query = "INSERT INTO BUSDTA.Doc_Master (Doc_Type,Ref_Id,Ref_Type,Doc_file) VALUES('Invoice',1,'invoice',?)";

                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand(query);
                iAnywhere.Data.SQLAnywhere.SAParameter parm = new iAnywhere.Data.SQLAnywhere.SAParameter();
                parm.ParameterName = "Doc_file";
                parm.SADbType = iAnywhere.Data.SQLAnywhere.SADbType.LongBinary;
                parm.Value = contents;
                command.Parameters.Add(parm);

                result = new DB_CRUD().InsertUpdateData(command);

            }
            catch (Exception ex)
            {
                log.Error("[SaveReportInDB][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:SaveReportInDB]");

        }
        public void GenerateReportFromDB()
        {


            try
            {
                log.Info("[Start:GenerateReportFromDB]");
                string ToSaveFileTo = @"C:\Data\BitBucket\MobileSales2\ClientApplication\SalesLogicExpress.Presentation\bin\x64\Debug\1PickSlipReport.pdf";

                iAnywhere.Data.SQLAnywhere.SACommand command = new iAnywhere.Data.SQLAnywhere.SACommand("select Doc_file from BUSDTA.Doc_Master where doc_masterid=1");

                using (iAnywhere.Data.SQLAnywhere.SADataReader sdr = new DB_CRUD().ExecuteReader(command))
                {
                    if (sdr.Read())
                    {
                        byte[] fileData = (byte[])sdr.GetValue(0);
                        using (System.IO.FileStream fs = new FileStream(ToSaveFileTo, FileMode.Create, FileAccess.ReadWrite))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                bw.Write(fileData);
                                bw.Close();
                            }
                        }
                    }
                    sdr.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error("[GenerateReportFromDB][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:GenerateReportFromDB]");
        }
        #endregion

    }
}

