﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using System.Collections.ObjectModel;
using System.Windows.Ink;
using SalesLogicExpress.Application.Helpers;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows.Media;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class PreTripInspectionReport : ViewModels.BaseViewModel
    {
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.PreTripInspectionReport]");
        
        #region Properties
        
        public string Route { get; set; }
        public string ReportTitle { get; set; }
        public string Branch { get; set; }
        public string RsrName { get; set; }
        public string VehicleNo { get; set; }
        public string VehicleMake { get; set; }
        public string OdometerReading { get; set; }
        public string AdditionalComments { get; set; }
        public string Date { get; set; }
        public string QuestionID { get; set; }
        public string QuestionTitle { get; set; }
        public string ResponseID { get; set; }
        public string Description { get; set; }
        public bool IsYesChecked { get; set; }
        public bool IsNoChecked { get; set; }
        public string NoPassReason { get; set; }
        public ObservableCollection<Models.PreTripInspectionFormList> QuestionList { get; set; }
        public IList Items { get; set; }
        public string ConfirmationStatement1 { get; set; }
        public string ConfirmationStatement2 { get; set; }
        public string ConfirmationStatement3 { get; set; }
        public string Signature { get; set; }
        private InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }

        #endregion

        public PreTripInspectionReport(Models.PreTripVehicleInspection preTripObj)
        {
            try
            {
                StrokeCollection strkCollection = new Managers.SignatureManager().GetSignature(Helpers.Flow.PreTripInspection, Helpers.SignType.PreTripInspection, Convert.ToInt32(preTripObj.PreTripInspectionID));
                new CanvasViewModel(Helpers.Flow.PreTripInspection, Helpers.SignType.PreTripInspection, Convert.ToInt32(preTripObj.PreTripInspectionID)).GetImageOfCanvas(strkCollection);
                bool IsSignPresent = strkCollection.Count == 0 ? false : true;

                ReportTitle = "PRE-TRIP VEHICLE INSPECTION FORM".Trim();
                Branch = CommonNavInfo.UserBranch.Trim();
                RsrName = CommonNavInfo.RouteUser.Trim();
                Date = preTripObj.PreTripDateTime.ToString("MM'/'dd'/'yyyy");
                VehicleNo = preTripObj.VehicleNumber.ToString().Trim();
                VehicleMake = preTripObj.VehicleModel.Trim();
                OdometerReading = preTripObj.OdometerReading.Trim();
                ConfirmationStatement1 = Helpers.Constants.PreTripInspection.ConfirmationStatement_1;
                ConfirmationStatement2 = Helpers.Constants.PreTripInspection.ConfirmationStatement_2;
                ConfirmationStatement3 = Helpers.Constants.PreTripInspection.ConfirmationStatement_3;
                Signature = IsSignPresent ? "test.jpg" : "blank.jpg";
                AdditionalComments = string.IsNullOrEmpty(preTripObj.AdditionalComments) ? string.Empty : preTripObj.AdditionalComments.Trim();
                QuestionList = preTripObj.QuestionList;
            }
            catch (Exception ex)
            {
                log.Error("[SalesLogicExpress.Application.ReportViewModels][PreTripInspectionReport][PreTripInspectionReport][ExceptionStackTrace=" + ex.StackTrace + "]");
            }
            
            ReportSource = new ReportViewModel(this, Reports.PreTripInspection, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
            System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
            Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            //telerikReportProcessor.PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
            telerikReportProcessor.PrintReport(ReportSource, printerSettings);
        }
    }
}
