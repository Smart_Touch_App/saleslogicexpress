﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class LoadSuggetionsReport : CommanInReplenishmentReport
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.LoadSuggetionsReport]");
        #endregion
        #region Constructor
        public LoadSuggetionsReport(ReplenishmentDetails selectedRplnshmnt, IList items)
        {
            try
            {
                log.Info("[Start:LoadSuggetionsReport]");
                ReportTitle = "ROUTE REPLENISHMENT-LOAD REQUEST";
                Status = selectedRplnshmnt.Status;
                UserName = CommonNavInfo.UserName;
                RouteNoAndName = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                TransferNo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                TransferFrom = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromBranch;
                TransferTo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToBranch;
                FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate.ToString("MM'/'dd'/'yyyy");
                ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate.ToString("MM'/'dd'/'yyyy");
                Items = items;
                if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                {
                    ReportSource = new ReportViewModel(this, Reports.LoadSuggetionsReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                else
                {
                    ReportSource = new ReportViewModel(this, Reports.LoadSuggetionsReportDC, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[LoadSuggetionsReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:LoadSuggetionsReport]");
        }
        #endregion
    }
}
