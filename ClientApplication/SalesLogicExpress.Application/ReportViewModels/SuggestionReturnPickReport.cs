﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SalesLogicExpress.Application.ReportViewModels
{
    class SuggestionReturnPickReport : CommanInReplenishmentReport
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.SuggestionReturnPickReport]");
        #endregion

        public SuggestionReturnPickReport( IList items)
        {
            try
            {
                log.Info("[Start:SuggestionReturnPickReport]");
                ReportTitle = "SUGGESTION RETURN-PICK FINAL";
                Status = "IN PICKING";
                UserName = CommonNavInfo.UserName;
                RouteNoAndName = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                TransferNo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                TransferFrom = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromBranch;
                TransferTo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToBranch;
                FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate.ToString();
                ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate.ToString();
                Items = items;
                if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                {
                    ReportSource = new ReportViewModel(this, Reports.SuggestionReturnPickReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                else
                {
                    ReportSource = new ReportViewModel(this, Reports.SuggestionReturnPickReportDC, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                //telerikReportProcessor.PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
                telerikReportProcessor.PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[SuggestionReturnPickReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:SuggestionReturnPickReport]");
        }
    }
}
