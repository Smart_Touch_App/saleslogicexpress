﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using System.Windows.Ink;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class CreditMemoReport : ViewModels.BaseViewModel
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.CreditMemoReport]");
        public string ReportTitle { get; set; }
        public string Route { get; set; }
        public string Billing { get; set; }
        public string ReceiptID { get; set; }
        public string Status { get; set; }
        public string CreditReason { get; set; }
        public string Note { get; set; }
        public string VoidReason { get; set; }
        public string CreditMemoDate { get; set; }
        public string Amount { get; set; }
        public string CustomerSignaturePath { get; set; }
        public IList Items { get; set; }
        private InstanceReportSource reportsource;
        private Models.Customer _Customer;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        public Models.Customer Customer
        {
            get
            {
                return _Customer;
            }
            set
            {
                _Customer = value;
            }
        }

        #endregion
        public CreditMemoReport(Models.CreditProcess creditProcess)
        {
            try
            {
                //bool IsSignPresent = new Managers.SignatureManager().GetSignature(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID).Count == 0 ? false : true;
                //new CanvasViewModel(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID).SaveCanvasAsImage();
                StrokeCollection strkCollection = new Managers.SignatureManager().GetSignature(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID);
                new CanvasViewModel(Helpers.Flow.Credits, Helpers.SignType.CreditProcessSign, creditProcess.ReceiptID).GetImageOfCanvas(strkCollection);
                bool IsSignPresent = strkCollection.Count == 0 ? false : true;
                log.Info("[Start:CreditMemoReport]");
                ReportTitle = "CREDIT MEMO REPORT";
                Status = creditProcess.StatusCode;
                ReceiptID = creditProcess.ReceiptID.ToString();
                Customer = CommonNavInfo.Customer;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }
                //CustomerName = creditProcess.CustomerName.Trim();
                Amount = "$" + Convert.ToDecimal(creditProcess.CreditAmount).ToString("F2");
                CreditMemoDate = creditProcess.CreditMemoDate.ToString("MM'/'dd'/'yyyy");
                Note = creditProcess.CreditNote;
                CreditReason = creditProcess.CreditReason;
                VoidReason = creditProcess.VoidReason;
                CustomerSignaturePath = IsSignPresent ? "test.jpg" : "blank.jpg";
                ReportSource = new ReportViewModel(this, Reports.CreditMemoReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                //telerikReportProcessor.PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
                telerikReportProcessor.PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[CreditMemoReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:CreditMemoReport]");

        }
    }
}
