﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Reporting;
using Telerik.Reporting;
using System.Configuration;
using System.ComponentModel;
using System.Windows.Data;
using log4net;
using System.Collections;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class QuotePickReport : ViewModels.BaseViewModel
    {
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.AddLoadPickReport]");
        public IList Items { get; set; }
        public string ReportTitle { get; set; }
        public string Status {get;set;}
        public string ReportDate {get;set;}
        public string QuoteNo { get; set; }
        public string QuoteDate { get; set; }
        public string ProspectNo { get; set; }
        public string ProspectName { get; set; }
        public string ManualPickReason { get; set; }
        public InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }

        public QuotePickReport(IList items)
        {
            try
            {
                log.Info("[Start:QuotePickReport]");
                ReportTitle = "QUOTE PICK LIST";
                QuoteNo = ViewModelPayload.PayloadManager.QuotePayload.QuoteId;
                QuoteDate = ViewModelPayload.PayloadManager.QuotePayload.QuoteDate.Date.ToString("MM'/'dd'/'yyyy");
                //ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                ProspectNo = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID;
                ProspectName = ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name;
                ManualPickReason = "Scanner not working";
                Items = items;
                
                ReportSource = new ReportViewModel(this, Reports.QuotePick, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[QuotePickReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:QuotePickReport]");
        }
    }
}
