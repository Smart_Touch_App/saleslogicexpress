﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using Telerik.Reporting;
using SalesLogicExpress.Reporting;
using SalesLogicExpress.Application.Helpers;
using System.Configuration;
using log4net;
using System.Data;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class ItemReturnsReport : ViewModels.BaseViewModel
    {
        #region Variables And Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.InvoiceReport]");
        InstanceReportSource reportsource;

        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }

        public Models.Customer Customer { get; set; }
        public string OrderNumber { get; set; }

        public string Billing { get; set; }
        public string Route { get; set; }
        public string VisitUsUrl { get; set; }
        public string RemitToName { get; set; }
        public string RemitToPOBox { get; set; }
        public string RemitToCityZip { get; set; }
        public string TaxAmount { get; set; }
        public string EnergySurcharge { get; set; }
        public string OrderDate { get; set; }
        public string TermOfSales { get; set; }
        public string CustomerSignImagePath { get; set; }
        private string _TotalAllied;
        public string TotalAllied
        {
            get
            {
                return _TotalAllied;
            }
            set
            {
                _TotalAllied = value;
                OnPropertyChanged("TotalAllied");
            }

        }
        public string TotalCoffee { get; set; }
        public string TotalDue { get; set; }
        public string LogoUrl { get; set; }


        public List<Models.ReturnItem> Items { get; set; }
        #endregion
        #region Constructor
        public ItemReturnsReport(string orderNumber, bool ShowPreview = false)
        {
            try
            {
                OrderNumber = orderNumber;

                Items = new Managers.CustomerReturnsManager().GetOrdersForOrderReturn(OrderNumber).ToList();
                DataTable orderInfoData = new Managers.OrderManager().GetOrderInfo(OrderNumber);

                OrderDate = Items.FirstOrDefault().OrderDate;
                EnergySurcharge = Convert.ToDecimal(orderInfoData.Rows[0]["EnergySurchargeAmt"].ToString()).ToString("0.00");
                TaxAmount = Convert.ToDecimal(orderInfoData.Rows[0]["SalesTaxAmt"].ToString()).ToString("0.##");

                bool IsSignPresent = string.IsNullOrEmpty(orderInfoData.Rows[0]["ordersign"].ToString()) ? false : true;

                if (IsSignPresent)
                {
                    //Draw sign on Image 
                    new CanvasViewModel(Flow.OrderReturn, SignType.ReturnOrderSign, Convert.ToInt32(OrderNumber)).UpdateSignatureToDB(false, false, true, "ReturnOrder.jpg");
                }

                log.Info("[Start:InvoiceReport][IsSignPresent:" + IsSignPresent + "]");
                Customer = CommonNavInfo.Customer;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }
                TotalAllied = ((decimal)Items.Where(s => s.SalesCat1 == "ALL").Sum(s => s.ExtendedPrice)).ToString("0.00");
                TotalCoffee = ((decimal)Items.Where(s => s.SalesCat1 == "COF").Sum(s => s.ExtendedPrice)).ToString("0.00");
                CustomerSignImagePath = IsSignPresent ? "ReturnOrder.jpg" : "blank.jpg";
                TermOfSales = CommonNavInfo.Customer.PaymentModeDescriptionForList.ToUpper();
                RemitToName = "FARMER BROTHERS";
                RemitToPOBox = "PO BOX 79705";
                RemitToCityZip = "CITY OF INDUSTRY CA 91716-9705";
                LogoUrl = @"Resources\Images\FarmerBrothers_Logo.png";
                EnergySurcharge = EnergySurcharge == null ? "0" : EnergySurcharge.Replace("$", "").ToString();
                TaxAmount = TaxAmount == null ? "0" : (Convert.ToDecimal(TaxAmount)).ToString("0.00");
                VisitUsUrl = @"Visit us at https://myaccount.farmerbros.com and register your account,
a convenient way to view your invoices, statement and make payments.";
                TotalDue = (((decimal)Items.Sum(s => s.ExtendedPrice)) + Convert.ToDecimal(EnergySurcharge.Replace("$", "").ToString()) + Convert.ToDecimal(TaxAmount)).ToString("0.00");
                ReportSource = new ReportViewModel(this, Reports.ItemReturnsReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;

                if (!ShowPreview)
                {
                    System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                    new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
                }
            }
            catch (Exception ex)
            {
                log.Error("[InvoiceReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:InvoiceReport]");
        }
        #endregion
    }
}
