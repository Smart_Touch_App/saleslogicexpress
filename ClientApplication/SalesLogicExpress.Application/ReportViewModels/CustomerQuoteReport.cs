﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Domain;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SalesLogicExpress.Application.ReportViewModels
{
    public class CustomerQuoteReport : QuoteReport
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.CustomerQuoteReport]");

        private string customerID = string.Empty;

        public string CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        private string customerName = string.Empty;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        #endregion


        public CustomerQuoteReport(CustomerQuoteHeader CustomerQuote)
        {
            try
            {
                log.Info("[Start:AddLoadRequestReport]");
                ReportTitle = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect?"PROSPECT PRODUCT QUOTE": "CUSTOMER PRODUCT QUOTE";
                UserName = CommonNavInfo.UserName;
                RouteNoAndName = CommonNavInfo.UserRoute;
                BranchName = CommonNavInfo.UserBranch;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy"); 
                PriceList.Add(CustomerQuote.OverridePriceSetup);
                PricingList = PriceList;
                Items = CustomerQuote.Items;
                BillTo = CustomerQuote.BillTo.Trim();
                Parent = CustomerQuote.Parent.Trim();
                CustomerID =  ViewModelPayload.PayloadManager.ProspectPayload.IsProspect?  ViewModelPayload.PayloadManager.ProspectPayload.Prospect.ProspectID:CustomerQuote.CustomerId.ToString();
                QuoteDate = CustomerQuote.QuoteDate.ToString("MM'/'dd'/'yyyy");
                CustomerName = ViewModelPayload.PayloadManager.ProspectPayload.IsProspect ? ViewModelPayload.PayloadManager.ProspectPayload.Prospect.Name : CommonNavInfo.Customer.Name;
                QuoteNo =  ViewModelPayload.PayloadManager.QuotePayload.QuoteId.ToString();
                PriceProtection = CustomerQuote.OverridePriceSetup.PriceProtection;
                ReportSource =ViewModelPayload.PayloadManager.ProspectPayload.IsProspect? 
                    new ReportViewModel(this, Reports.ProspectQuote, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource:
                    new ReportViewModel(this, Reports.CustomerQuote, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;

                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                //telerikReportProcessor.PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
                telerikReportProcessor.PrintReport(ReportSource, printerSettings);
            }
            catch (Exception)
            {
                
                throw;
            }

        }

    }
}
