﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Reporting;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Reporting;
using System.Configuration;
using SalesLogicExpress.Domain;
using System.Windows.Controls;
using log4net;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class ReceiptReport : ViewModels.BaseViewModel
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.ReceiptReport]");
        InstanceReportSource reportsource;
        public string RemitToName { get; set; }
        public string RemitToPOBox { get; set; }
        public string RemitToCityZip { get; set; }
        public string LogoUrl { get; set; }
        public string VisitUsUrl { get; set; }
        public string ReceiptNumber { get; set; }
        PaymentARModel.OpenAR objOpenAR = new PaymentARModel.OpenAR();

        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        public string Billing { get; set; }
        public string Route { get; set; }
        public string OrderDate { get; set; }
        public Customer Customer { get; set; }
        public PaymentARModel.OpenAR ObjOpenAR
        {
            get
            {
                return this.objOpenAR;
            }
            set
            {
                this.objOpenAR = value;
                OnPropertyChanged("ObjOpenAR");
            }
        }
        public List<Models.PaymentARModel.InvoiceDetails> Items { get; set; }
        #endregion

        #region Constructor
        public ReceiptReport(Models.PaymentARModel.OpenAR openAR)
        {
            try
            {
                log.Info("[Start:ReceiptReport][Parameters:openAR:" + openAR.ToString() + "]");
                ObjOpenAR = openAR;
                Customer = CommonNavInfo.Customer;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }

                if (Items != null)
                    Items.Clear();

                Items = ObjOpenAR.SelectedItems.ToList();


                RemitToName = "FARMER BROTHERS";
                RemitToPOBox = "PO BOX 79705";
                RemitToCityZip = "CITY OF INDUSTRY CA 91716-9705";
                LogoUrl = @"Resources\Images\FarmerBrothers_Logo.png";

                VisitUsUrl = @"Visit us at https://myaccount.farmerbros.com and register your account,
a convenient way to view your invoices, statement and make payments.";

                OrderDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                //TotalPickedQty = Items.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                ReportSource = new ReportViewModel(this, Reports.ReceiptReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;


                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[ReceiptReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:ReceiptReport]");
        }
        #endregion
    }
}
