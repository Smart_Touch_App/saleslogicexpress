﻿using log4net;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Reporting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class AddLoadRequestReport : CommanInReplenishmentReport
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.AddLoadRequestReport]");
        #endregion

        #region Constructor
        public AddLoadRequestReport(ReplenishmentDetails selectedRplnshmnt, IList items)
        {
            try
            {
                log.Info("[Start:AddLoadRequestReport]");
                ReportTitle = "ADD LOAD REQUEST";
                Status = selectedRplnshmnt.Status;
                UserName = CommonNavInfo.UserName;
                RouteNoAndName = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.RouteNameAddLoad;
                ReportDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                TransferNo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.SelectedReplnId;
                TransferFrom = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromBranch;
                TransferTo = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToBranch;
                FromDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.FromDate.ToString();
                ToDate = ViewModelPayload.PayloadManager.RouteReplenishmentPayload.ToDate.ToString();
                Items = items;
                if (ViewModelPayload.PayloadManager.RouteReplenishmentPayload.IsSalesBranch)
                {
                    ReportSource = new ReportViewModel(this, Reports.AddLoadRequestReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                else
                {
                    ReportSource = new ReportViewModel(this, Reports.AddLoadRequestReportDC, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
                }
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                Telerik.Reporting.Processing.ReportProcessor telerikReportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
                //telerikReportProcessor.PrintController.OnEndPrint((PrintDocument)reportsource.ReportDocument, new PrintEventArgs());
                telerikReportProcessor.PrintReport(ReportSource, printerSettings);
            }
            catch (Exception ex)
            {
                log.Error("[AddLoadRequestReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:AddLoadRequestReport]");
        }
        #endregion

    }
}
