﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using Telerik.Reporting;
using SalesLogicExpress.Reporting;
using SalesLogicExpress.Application.Helpers;
using System.Configuration;
using log4net;
using System.Data;
using SalesLogicExpress.Domain;
using System.Collections.ObjectModel;
using SalesLogicExpress.Application.Managers;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class RouteSettlementReport : ViewModels.BaseViewModel
    {
        #region Variables And Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.RouteSettlementReport]");
        InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        public string BranchNo { get; set; }
        public string RouteNo { get; set; }
        #endregion

        #region Constructor
        public RouteSettlementReport(RouteSettlementSettlementsModel routeSettlementSettlement, bool ShowPreview = false)
        {
            BranchNo = CommonNavInfo.UserBranch;
            RouteNo = CommonNavInfo.UserRoute;

            ReportSource = new ReportViewModel(this, Reports.RouteSettlementReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
           
            if (!ShowPreview)
            {
                System.Drawing.Printing.PrinterSettings printerSettings = new System.Drawing.Printing.PrinterSettings();
                new Telerik.Reporting.Processing.ReportProcessor().PrintReport(ReportSource, printerSettings);
            }
        }
        #endregion
    }
}
