﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Managers = SalesLogicExpress.Application.Managers;
using Models = SalesLogicExpress.Domain;
using SalesLogicExpress.Application.ViewModels;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using SalesLogicExpress.Reporting;
using Telerik.Reporting;
using System.Configuration;
using System.ComponentModel;
using System.Windows.Data;
using log4net;

namespace SalesLogicExpress.Application.ReportViewModels
{
    public class PickSlipReport : PickOrder
    {
        #region Variables & Properties
        private readonly ILog log = LogManager.GetLogger("[SalesLogicExpress.Application.ReportViewModels.PickSlipReport]");
        InstanceReportSource reportsource;
        public InstanceReportSource ReportSource
        {
            get
            {
                return reportsource;
            }
            set
            {
                reportsource = value;
                OnPropertyChanged("ReportSource");
            }
        }
        public string Billing { get; set; }
        public string Route { get; set; }
        public string OrderDate { get; set; }

        public List<Models.PickOrderItem> Items { get; set; }
        #endregion

        #region Constructor
        public PickSlipReport()
        {
            try
            {
                log.Info("[Start:PickSlipReport]");
                Customer = CommonNavInfo.Customer;
                Route = Customer.Route.Length > 3 ? Customer.Route.Substring(3) : Customer.Route;
                Billing = new Managers.OrderManager().GetSoldTo(Convert.ToInt32(Customer.CustomerNo));
                if (Billing == "")
                {
                    Billing = Customer.CustomerNo;
                }
                else
                {
                    if (Billing.Contains(","))
                    {
                        Billing = Billing.Split(',')[0];
                    }
                }

                if (Items != null)
                    Items.Clear();


                Items = PayloadManager.OrderPayload.PickOrderItemsForPrint;


                OrderDate = DateTime.Today.ToString("MM'/'dd'/'yyyy");
                //TotalPickedQty = Items.Sum(s => s.PickedQuantityInPrimaryUOM).ToString();
                ReportSource = new ReportViewModel(this, Reports.PickSlipReport, Convert.ToBoolean(ConfigurationManager.AppSettings["PrintLabels"]), false).ReportSource;
            }
            catch (Exception ex)
            {
                log.Error("[PickSlipReport][ExceptionStackTrace=" + ex.InnerException.Message + "]");
                throw;
            }
            log.Info("[End:PickSlipReport]");
        }
        #endregion
    }
}
