﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain;
using System.Configuration;
using SalesLogicExpress.Domain.Vertex;
using log4net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Xml;
using VertexWrapper;
namespace SalesLogicExpress.Application.Test.ViewModels
{
    [TestClass]
    public class VertexTaxUnitTest
    {
        /* TODO*/
        // SPECIFY FILE FLEXIBILITY TO FOLDER
        // make all paths configurable from app.config
        //call to test method standerdize it.
        //
        # region Variables
        //private readonly ILog log = LogManager.GetLogger("SalesLogicExpress.Application.Test");
        InvoiceObject VertexInoviceObj = new InvoiceObject();

        DirectoryInfo dirinfo;
        string Vertex_Database = string.Empty;
        double[] invoiceTax;
        double[] CalculatedInvoicetax;
        string Datasource = String.Empty;
        List<Invoice> InvoiceList = new List<Invoice>();
        class Invoice
        {

            public int ItemNumberCount = 0;
            public string CompCode = String.Empty;
            public string Customer = String.Empty;
            public string CustomerClassCode = String.Empty;
            public double GeoCode = 0;
            public string InvoiceNumber = String.Empty;
            public string ProductSetCode = String.Empty;
            public string[] ItemNumber;
            public string[] ProductCode;
            public string[] ComponentCode;
            public int[] ProductQty;
            public double[] ExtendedAmount;
            public double[] ItemTax;
            public double GrossAmt = 0;
        }

        #endregion


        #region DataInitialization
        [TestInitialize]
        public void TestInitialize()
        {

            try
            {
                string vertexPath = ConfigurationManager.AppSettings["VERTEX_PATH"].ToString();
                string JSON_Invoices = ConfigurationManager.AppSettings["VERTEX_TEST_INVOICES"].ToString();
                Vertex_Database = ConfigurationManager.AppSettings["VERTEX_DataSource"].ToString();
                dirinfo = new DirectoryInfo(JSON_Invoices);
                FileInfo[] fileinfo = dirinfo.GetFiles();
                invoiceTax = new double[fileinfo.Count()];
                CalculatedInvoicetax = new double[fileinfo.Count()];

                string getUserPath, GetProcessPath;
                getUserPath = Environment.GetEnvironmentVariable("PATH", System.EnvironmentVariableTarget.User);
                GetProcessPath = Environment.GetEnvironmentVariable("PATH", System.EnvironmentVariableTarget.Process);
                string SetUserPath, SetProcessPath;
                SetProcessPath = GetProcessPath + ";" + vertexPath;
                SetUserPath = getUserPath + ";" + vertexPath;
                Environment.SetEnvironmentVariable("PATH", SetUserPath, EnvironmentVariableTarget.User);
                Environment.SetEnvironmentVariable("PATH", SetProcessPath, EnvironmentVariableTarget.Process);


                DataContractJsonSerializer DCJS = new DataContractJsonSerializer(typeof(InvoiceObject));
                for (int index = 0; index < fileinfo.Count(); index++)
                {
                    Invoice InvoiceObject = new Invoice();
                    FileStream ofs = File.Open(fileinfo[index].FullName, FileMode.Open);
                    VertexInoviceObj = DCJS.ReadObject(ofs) as InvoiceObject;
                    InvoiceObject.ItemNumber = new string[VertexInoviceObj.InvoiceItemList.Count];
                    InvoiceObject.ExtendedAmount = new double[VertexInoviceObj.InvoiceItemList.Count];
                    InvoiceObject.ProductQty = new int[VertexInoviceObj.InvoiceItemList.Count];
                    InvoiceObject.ComponentCode = new string[VertexInoviceObj.InvoiceItemList.Count];
                    InvoiceObject.ItemTax = new double[VertexInoviceObj.InvoiceItemList.Count];
                    InvoiceObject.ProductCode = new string[VertexInoviceObj.InvoiceItemList.Count];
                    ofs.Close();
                    InvoiceObject.ItemNumberCount = VertexInoviceObj.InvoiceItemList.Count;
                    InvoiceObject.CompCode = "00800";
                    InvoiceObject.Customer = VertexInoviceObj.Customer;
                    InvoiceObject.GeoCode = VertexInoviceObj.GeoCode;
                    InvoiceObject.CustomerClassCode = VertexInoviceObj.CustomerClassCode.Trim();
                    InvoiceObject.InvoiceNumber = VertexInoviceObj.InvoiceNumber;
                    InvoiceObject.ProductSetCode = VertexInoviceObj.ProductSetCode;
                    for (int innerIndex = 0; innerIndex < VertexInoviceObj.InvoiceItemList.Count; innerIndex++)
                    {
                        InvoiceObject.ItemNumber[innerIndex] = VertexInoviceObj.InvoiceItemList[innerIndex].ItemNumber.Trim();
                        InvoiceObject.ProductCode[innerIndex] = VertexInoviceObj.InvoiceItemList[innerIndex].ProductCode.Trim();
                        InvoiceObject.ExtendedAmount[innerIndex] = VertexInoviceObj.InvoiceItemList[innerIndex].ExtendedAmount;
                        InvoiceObject.ProductQty[innerIndex] = VertexInoviceObj.InvoiceItemList[innerIndex].ProductQuantity;
                        InvoiceObject.ComponentCode[innerIndex] = "";
                    }
                    InvoiceList.Add(InvoiceObject);
                    invoiceTax[index] = VertexInoviceObj.InvoiceTax;
                }
            }
            catch (Exception e)
            {
                //log.Debug("Test initialization Exception: ", e);
            }


        }
        #endregion

        #region Test method

        [TestMethod]
        public void CalculateTax()
        {
            VertexApi vertexApiobj = new VertexApi();
            bool ReturnStatus;
            double InvoiceTax = 0;
            double GrossAmt = 0;
            Datasource = Vertex_Database;
            for (int index = 0; index < InvoiceList.Count; index++)
            {
                //Api calls to vertex library
                unsafe
                {
                    vertexApiobj.Initialize(&ReturnStatus, Datasource);
                    vertexApiobj.SetInvoice(InvoiceList[index].CompCode,
                                      InvoiceList[index].Customer,
                                      InvoiceList[index].CustomerClassCode,
                                      InvoiceList[index].GeoCode,
                                      InvoiceList[index].InvoiceNumber,
                                      InvoiceList[index].ProductSetCode,
                                      InvoiceList[index].ItemNumberCount,
                                      InvoiceList[index].ItemNumber,
                                      InvoiceList[index].ProductCode,
                                      InvoiceList[index].ComponentCode,
                                      InvoiceList[index].ProductQty,
                                      InvoiceList[index].ExtendedAmount,
                                      &ReturnStatus);
                    vertexApiobj.GetTax(InvoiceList[index].InvoiceNumber, &InvoiceTax, InvoiceList[index].ItemTax, &GrossAmt, &ReturnStatus);
                    vertexApiobj.Cleanup(&ReturnStatus);

                }

                InvoiceList[index].GrossAmt = GrossAmt;
                CalculatedInvoicetax[index] = InvoiceTax;
            }
            //TaxEquals();
        }

       
        public void TaxEquals()
        {
            for (int index = 0; index < invoiceTax.Count(); index++)
            {
                Assert.AreEqual(CalculatedInvoicetax[index], invoiceTax[index]);
            }
        }
    }
}
        #endregion