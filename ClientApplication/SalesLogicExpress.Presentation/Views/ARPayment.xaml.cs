﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ARPayment.xaml
    /// </summary>
    public partial class ARPayment : BaseWindow
    {
        viewmodels.ARPaymentViewModel viewModel;
        bool flag;
        public ARPayment(bool payload)
        {
            flag = payload;
            InitializeComponent();
            SetNavigationDefaults();
            this.Activated += ARPayment_Activated;
            this.Loaded += ARPayment_Loaded;
            busyIndicator.IsBusy = true;
            busyIndicator.Visibility = System.Windows.Visibility.Visible;
            LoadContext();
        }
        public ARPayment()
        {
            InitializeComponent();
            SetNavigationDefaults();
            this.Activated += ARPayment_Activated;
            this.Loaded += ARPayment_Loaded;
            busyIndicator.IsBusy = true;
            busyIndicator.Visibility = System.Windows.Visibility.Visible;
            LoadContext();
        }

        void ARPayment_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ServiceRoute_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults();
        }
        void ARPayment_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Customer Home";
            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Payments & AR";
            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.CustomerHome;
            //SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ARPayment;

        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                if (viewModel == null)
                {
                    viewModel = new viewmodels.ARPaymentViewModel(flag);
                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        NavigationHeader.DataContext = cminfo;
                        busyIndicator.IsBusy = false;
                    }));
                }

            });
        }

        private void KeyPadToggleButton_Click(object sender, RoutedEventArgs e)
        {
            KeyPadPanel.Visibility = KeyPadPanel.Visibility == System.Windows.Visibility.Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;

        }

        private void txt_PaymentAmt_Loaded(object sender, RoutedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            tb.SelectAll();
        }
    }
}
