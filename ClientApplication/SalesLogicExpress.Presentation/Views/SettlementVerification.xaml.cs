﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using ViewModelApplication = SalesLogicExpress.Application;
using System.Windows.Threading;
using SalesLogicExpress.Presentation.Helpers;
using SalesLogicExpress.Application.Helpers;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for SettlementVerification.xaml
    /// </summary>
    public partial class SettlementVerification : BaseWindow
    {
        viewmodels.SettlementConfirmationViewModel viewModel;

        public SettlementVerification()
        {
            InitializeComponent();
            InitView();
            LoadContext();
        }
        private void InitView()
        {
            this.Activated += SettlementVerification_Activated;
            this.Loaded += SettlementVerification_Loaded;
            this.TestInkCanvas.StrokeCollected += TestInkCanvas_StrokeCollected;

        }
        void TestInkCanvas_StrokeCollected(object sender, InkCanvasStrokeCollectedEventArgs e)
        {
            if (this.TestInkCanvas.Strokes.Count > 0)
            {
                //viewModel.MyStrokeCollection = new System.Windows.Ink.StrokeCollection();
                //viewModel.MyStrokeCollection = this.TestInkCanvas.Strokes;
                viewModel.MyCanvas = new InkCanvas();
                viewModel.MyCanvas = this.TestInkCanvas;
            }
        }

        void SettlementVerification_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("SettlementVerificaion_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults(); ;
        }

        void SettlementVerification_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e); ;
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                if (viewModel == null)
                {
                    viewModel = new viewmodels.SettlementConfirmationViewModel();
                    viewModel.MyCanvas = this.TestInkCanvas;

                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        NavigationHeader.DataContext = cminfo;
                    }));
                }


            });
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.SettlementVerification.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.SettlementVerification;

        }

        private void btnClearCanvas_Click(object sender, RoutedEventArgs e)
        {
            if (this.TestInkCanvas.Strokes.Count > 0)
            {
                this.TestInkCanvas.Strokes.Clear();
            }
        }
    }
}
