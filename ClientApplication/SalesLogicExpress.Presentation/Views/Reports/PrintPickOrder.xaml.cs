﻿using viewmodels = SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Reporting;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PrintPickOrder.xaml
    /// </summary>
    public partial class PrintPickOrder : BaseWindow
    {
        SalesLogicExpress.Application.ViewModels.PickOrder po;
        Dictionary<string, object> GParameters;

        public PrintPickOrder(object payload)
        {
            InitializeComponent();
            SetNavigationDefaults();
            SalesLogicExpress.Application.ReportViewModels.PickSlipReport Psr = new Application.ReportViewModels.PickSlipReport();
            this.DataContext = Psr;
            this.Activated += WindowActivated;
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
        }


        //private void LoadContext(object payload)
        //{
        //    SetNavigationDefaults();
        //    viewmodel = new viewmodels.PrintPickOrder(payload);
        //    viewmodel.MessageToken = this.Token;


        //}
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Print Pick Slip";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Pick Order";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.PickOrder;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PrintPickOrder;
        }
        public InstanceReportSource GetReportSource()
        {
            InstanceReportSource rs = new InstanceReportSource();

            //rPickOrderSlip rPickOrderSlip1 = new rPickOrderSlip();
            //rs.ReportDocument = rPickOrderSlip1;

            //Dictionary<string, object> rParameters = viewmodel.GetPickOrderInfo();

            //foreach (KeyValuePair<string, object> item in rParameters.ToList())
            //{
            //    rs.Parameters.Add(item.Key, item.Value);
            //}

            return rs;
        }




    }
}
