﻿using viewmodels = SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Reporting;
using SalesLogicExpress.Domain;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PrintRouteSettlement.xaml
    /// </summary>
    public partial class PrintRouteSettlement : BaseWindow
    {               
        public PrintRouteSettlement(object payload)
        {
            InitializeComponent();
            SetNavigationDefaults();
            SalesLogicExpress.Application.ReportViewModels.RouteSettlementReport rsr = new Application.ReportViewModels.RouteSettlementReport((RouteSettlementSettlementsModel)payload, true);
            this.DataContext = rsr;
            this.Activated += WindowActivated;
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
        }
        
       
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Print Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.RouteSettlementReport;
           
        }        

    }
}
