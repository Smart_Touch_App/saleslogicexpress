﻿using viewmodels = SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Reporting;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for PrintInvoice.xaml
    /// </summary>
    public partial class PrintInvoice : BaseWindow
    {
        SalesLogicExpress.Application.ViewModels.PickOrder po;
        Dictionary<string, object> GParameters;

        public PrintInvoice(object payload)
        {
            InitializeComponent();
            SetNavigationDefaults();
            SalesLogicExpress.Application.ReportViewModels.InvoiceReport invoiceReport = new Application.ReportViewModels.InvoiceReport(payload.ToString(), true);
            this.DataContext = invoiceReport;
            this.Activated += WindowActivated;
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
        }


        //private void LoadContext(object payload)
        //{
        //    SetNavigationDefaults();
        //    viewmodel = new viewmodels.PrintInvoice(payload);
        //    viewmodel.MessageToken = this.Token;
        //}


        string previousViewTitle;

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsBackwardNavigationEnabled = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Print Invoice";

            if (SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView != Application.Helpers.ViewModelMappings.View.PrintInvoice)
            {
                previousViewTitle = SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView == Application.Helpers.ViewModelMappings.View.PickOrder ? "Pick Order" : "Delivery Screen";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView;
            }
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = previousViewTitle;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.PrintInvoice;
        }
        public InstanceReportSource GetReportSource()
        {
            InstanceReportSource rs = new InstanceReportSource();

            //rPickOrderSlip rPickOrderSlip1 = new rPickOrderSlip();
            //rs.ReportDocument = rPickOrderSlip1;

            //Dictionary<string, object> rParameters = viewmodel.GetPickOrderInfo();

            //foreach (KeyValuePair<string, object> item in rParameters.ToList())
            //{
            //    rs.Parameters.Add(item.Key, item.Value);
            //}

            return rs;
        }




    }
}
