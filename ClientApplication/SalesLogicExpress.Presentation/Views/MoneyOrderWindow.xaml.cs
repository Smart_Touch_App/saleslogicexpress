﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress.Application.Helpers;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for MoneyOrderWindow.xaml
    /// </summary>
    public partial class MoneyOrderWindow : BaseWindow
    {
        viewmodels.MoneyOrderViewModel viewModel;
        public MoneyOrderWindow()
        {
            InitView();
            InitializeComponent();
            LoadContext();
        }

        private void InitView()
        {
            this.Activated += MoneyOrder_Activated;
            this.Loaded += MoneyOrder_Loaded;
        }

        void MoneyOrder_Loaded(object sender, RoutedEventArgs e)
        {
            // System.Diagnostics.Debug.WriteLine("PreTripInspection_Loaded, " + DateTime.Now.Millisecond);
            this.AlwaysShowKeyboard = true;
        }

        void MoneyOrder_Activated(object sender, EventArgs e)
        {
            //SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);

        }
        async void LoadContext()
        {

            await Task.Run(() =>
            {
                if (true)
                {
                    viewModel = new viewmodels.MoneyOrderViewModel();
                    SetNavigationDefaults();

                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        NavigationHeader.DataContext = cminfo;

                        //NavigationHeader.DataContext = cminfo;
                    }));
                }
            });
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Money Orders";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteSettlement;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.MoneyOrderWindow;

        }
    }
}
