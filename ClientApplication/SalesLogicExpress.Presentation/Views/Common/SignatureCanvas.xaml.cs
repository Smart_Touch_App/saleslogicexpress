﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SalesLogicExpress.Views.Common
{
    /// <summary>
    /// Interaction logic for SignatureCanvas.xaml
    /// </summary>
    public partial class SignatureCanvas : UserControl
    {
        public SignatureCanvas()
        {
            InitializeComponent();
            this.Loaded += SignatureCanvas_Loaded;
        }

        void SignatureCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            int h, w;
            if (this.DataContext != null)
            {
                h = ((SalesLogicExpress.Application.ViewModels.CanvasViewModel)this.DataContext).Height;
                w = ((SalesLogicExpress.Application.ViewModels.CanvasViewModel)this.DataContext).Width;
                CanvasControl.MinHeight = h - 45;
                CanvasControl.MinWidth = w;
                this.MinHeight = h;
                this.MinWidth = w;
            }
        }
        private void InkCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.DataContext != null)
            {
                ((SalesLogicExpress.Application.ViewModels.CanvasViewModel)this.DataContext).ActualHeight = CanvasControl.ActualHeight;
                ((SalesLogicExpress.Application.ViewModels.CanvasViewModel)this.DataContext).ActualWidth = CanvasControl.ActualWidth;
            }

            Rect CanvasBound = VisualTreeHelper.GetDescendantBounds(CanvasControl);
            Point MousePosition = e.GetPosition((IInputElement)sender);
            if ((MousePosition.X >= CanvasBound.Left && MousePosition.X < CanvasBound.Left + 10) ||
              (MousePosition.X > CanvasBound.Right - 10 && MousePosition.X < CanvasBound.Right) ||
              (MousePosition.Y >= CanvasBound.Top && MousePosition.Y < CanvasBound.Top + 10) ||
              (MousePosition.Y > CanvasBound.Bottom - 10 && MousePosition.Y < CanvasBound.Bottom))
            {
                e.Handled = true;
            }
        }

        private void CanvasControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // e.Handled = false;
        }

    }

    /*
        Please follow below steps while implementing new signature functionality,

        Usage Example for Order LIfe Cycle Order Sign
     
        XAML Code in View
        include     
            xmlns:UserControls="clr-namespace:SalesLogicExpress.Views.Common"
     
        and add following xaml code in view     
            <Border  DataContext="{Binding SignatureCanvasVM}" HorizontalAlignment="Left" BorderBrush="#c0c0c0" BorderThickness="5" VerticalAlignment="Top">
            <UserControls:SignatureCanvas />
            </Border> 
    
        Create a property SignatureCanvasVM in Viewmodel and initialize it like below     
        /// <param name="lifeCycleFlow">Enum defining the particular flow for which the signature is associated. e.g OrderLifeCycle,OrderReturn,Replenishment,Credits etc</param>
        /// <param name="signType">Enum defining the type of sign in a flow, e.g a flow/activity lifecycle can include having multiple signs stored in db, hence which sign is to be reffered when using this class instance.</param>
        /// <param name="transactionID">This parameter defines the id for the transaction for which the sign is saved in database, e.g In Order Process this will be the Order ID</param>
        /// <param name="IsInReadOnlyMode">Whether the canvas should be displayed in readonly mode. i.e Non-Editable mode</param>
        e.g.
            SignatureCanvasVM = new CanvasView(Flow.OrderLifeCycle, SignType.OrderSign, Order.OrderId)
            {
            Height = 280,
            Width = 300,
            FileName = "SmallPic.jpg",
            FooterText = "Customer Signature",
            };

        For saving the Sign in DB add below code ,
        /// <param name="DisableCanvasAfterSave">this defines whether we need to disable canvas after save or not </param>
        /// <param name="CanClearCanvasAfterSave">this defines whether we need to disable clear button after save or not </param>
        /// <param name="saveCanvasAsImage"> this accept value that weather we need to store canvas as image in app or not</param>
        /// <param name="imageName">if we need to save image in app then define the name for it in this parameter</param>
        e.g.
            SignatureCanvasVM.UpdateSignatureToDB(false, false, true,"test.jpg");

        For reset/remove sign from DB call following method:
        e.g.
            new CanvasViewModel(Flow.OrderLifeCycle, SignType.OrderSign, Order.OrderId).ResetSignFromDB();

        Note:
        For CRUD Operations to specific tables that need to be Specified based on flow and the Signtype in SignatureManager.cs at SalesLogicExpress.Application.Managers
        e.g.
            GetSignature(Flow lifeCycleFlow, SignType signType, int idToGet)
        and
            UpdateSignature(Flow lifeCycleFlow, SignType signType, int idToUpdate, byte[] signbyte, bool saveCanvasAsImage, string imageName = "")
     */
}
