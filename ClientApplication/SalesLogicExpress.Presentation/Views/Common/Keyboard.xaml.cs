﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Views.Common
{
    /// <summary>
    /// Interaction logic for Keyboard.xaml
    /// </summary>
    public partial class Keyboard : UserControl
    {

        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.Common");
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        private static extern short VkKeyScan(char ch);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        static extern short GetKeyState(int keyCode);

        private const uint KEYEVENTF_KEYUP = 0x2;  // Release key
        private const byte VK_4 = 0x34;
        private const byte VK_5 = 0x35;
        Dictionary<String, byte> keyMappings = new Dictionary<string, byte>();

        private bool IsScreenKeyBoardPressed = false;
        private bool IsShiftPressed = false;
        private bool IsShiftKeyInPressedState = false;

        public bool IsNumeric { get; set; }
        public bool IsSwitchDisable { get; set; }
        public bool IsKeyboardFade { get; set; }
        public Keyboard(bool IsNum)
        {
            InitializeComponent();
            Loaded += Keyboard_Loaded;
            IsNumeric = IsNum;
        }

        public Keyboard()
        {
            InitializeComponent();
            Loaded += Keyboard_Loaded;
            IsNumeric = false;



        }

        public DependencyObject Parent
        {
            get;
            set;
        }
        private void Keyboard_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                #region Action Keys
                keyMappings.Add("LEFT", 0x25);
                keyMappings.Add("RIGHT", 0x27);
                keyMappings.Add("TAB", 0x09);
                keyMappings.Add("SPACE", 0x20);
                keyMappings.Add("DELETE", 0x8);
                keyMappings.Add("*", 0x6A);
                keyMappings.Add(",", 0xBC);
                keyMappings.Add("<", 0xBC);
                keyMappings.Add(".", 0xBE);
                keyMappings.Add(">", 0xBE);
                keyMappings.Add("-", 0xBD);
                keyMappings.Add("/", 0xBF);
                keyMappings.Add("?", 0xBF);
                keyMappings.Add("ENTER", 0x0D);
                keyMappings.Add("'", 0xDE);
                keyMappings.Add(@"\", 0xDC);
                keyMappings.Add("=", 0xBB);
                keyMappings.Add("+", 0x6B);
                keyMappings.Add(";", 0xBA);

                #endregion

                #region Numbers
                keyMappings.Add("0", 0x30);
                keyMappings.Add("1", 0x31);
                keyMappings.Add("2", 0x32);
                keyMappings.Add("3", 0x33);
                keyMappings.Add("4", 0x34);
                keyMappings.Add("5", 0x35);
                keyMappings.Add("6", 0x36);
                keyMappings.Add("7", 0x37);
                keyMappings.Add("8", 0x38);
                keyMappings.Add("9", 0x39);
                #endregion

                #region Characters
                keyMappings.Add("A", 0x41);
                keyMappings.Add("B", 0x42);
                keyMappings.Add("C", 0x43);
                keyMappings.Add("D", 0x44);
                keyMappings.Add("E", 0x45);
                keyMappings.Add("F", 0x46);
                keyMappings.Add("G", 0x47);
                keyMappings.Add("H", 0x48);
                keyMappings.Add("I", 0x49);
                keyMappings.Add("J", 0x4A);
                keyMappings.Add("K", 0x4B);
                keyMappings.Add("L", 0x4C);
                keyMappings.Add("M", 0x4D);
                keyMappings.Add("N", 0x4E);
                keyMappings.Add("O", 0x4F);
                keyMappings.Add("P", 0x50);
                keyMappings.Add("Q", 0x51);
                keyMappings.Add("R", 0x52);
                keyMappings.Add("S", 0x53);
                keyMappings.Add("T", 0x54);
                keyMappings.Add("U", 0x55);
                keyMappings.Add("V", 0x56);
                keyMappings.Add("W", 0x57);
                keyMappings.Add("X", 0x58);
                keyMappings.Add("Y", 0x59);
                keyMappings.Add("Z", 0x5A);
                #endregion

                if (IsNumeric)
                {
                    ShowNumeric();
                }

                kbQuerty.AddHandler(Button.ClickEvent, new RoutedEventHandler(button_Click));
                kbNumeric.AddHandler(Button.ClickEvent, new RoutedEventHandler(button_Click));
                kbSpecialChar.AddHandler(Button.ClickEvent, new RoutedEventHandler(button_Click));

                // Default Set to CapsLOCK on
                bool CapsLock = (((ushort)GetKeyState(0x14)) & 0xffff) != 0;
                if (!CapsLock)
                {
                    /*Tempararily commented*/
                    keybd_event(0x14, 0, 0, (UIntPtr)0);
                    keybd_event(0x14, 0, KEYEVENTF_KEYUP, (UIntPtr)0);
                }

                //ToggleCaps();

                var window = Window.GetWindow(this);
                window.KeyDown += window_KeyDown;
                window.KeyUp += window_KeyUp;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Views.Common][Keyboard][Keyboard_Loaded][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

        }

        private void window_KeyUp(object sender, KeyEventArgs e)
        {
            if (System.Windows.Forms.Control.ModifierKeys == System.Windows.Forms.Keys.Shift)
            {
              IsShiftKeyInPressedState = false;
            }
        }

        void window_KeyDown(object sender, KeyEventArgs e)
        {
            if (IsScreenKeyBoardPressed)
            {
                IsScreenKeyBoardPressed = false;
                return;
            }

            if ((System.Windows.Forms.Control.ModifierKeys == System.Windows.Forms.Keys.Shift) && !IsShiftKeyInPressedState)
            {
                IsShiftKeyInPressedState = true;
            }
            else
            {
                this.window_KeyUp(sender, e);
            }

            if (e.Key == Key.Capital)
            {
                ToggleCaps();
            }

            if ((e.Key==Key.LeftShift || e.Key==Key.RightShift) && IsShiftKeyInPressedState)
            {
              

                //Invert the shift key status of screen key board 
                IsShiftPressed = !IsShiftPressed;

                UpdateKeyBoardLayoutOnShiftPress(ShiftKey);
            }
            //bool capsLock = Console.CapsLock;
            //string buttonTag = string.Empty;
            //foreach (RadButton btn in FindVisualChildren<RadButton>(kbQuerty))
            //{
            //    buttonTag = btn.Tag != null ? btn.Tag.ToString() : string.Empty;
            //    if (!string.IsNullOrEmpty(buttonTag)) continue;
            //    btn.Content = capsLock==true ? btn.Content.ToString().ToUpper() : btn.Content.ToString().ToLower();
            //}
        }
        private void InsertText(TextBox textBox, string input, bool moveRight, bool moveLeft, bool backspace)
        {
            try
            {

                int caterIndex = textBox.CaretIndex;
                if (moveLeft && textBox.CaretIndex != 0)
                {
                    textBox.CaretIndex = textBox.CaretIndex - 1;
                    return;
                }
                if (moveRight && textBox.CaretIndex != textBox.Text.Length)
                {
                    textBox.CaretIndex = textBox.CaretIndex + 1;
                    return;
                }
                if (backspace && textBox.Text.Length > 0)
                {
                    textBox.Text = textBox.Text.Remove(caterIndex - 1, 1);
                    textBox.CaretIndex = caterIndex - 1;
                    return;
                }
                if (!input.Equals(string.Empty))
                {
                    if (textBox.SelectedText == textBox.Text)
                    {
                        textBox.Text = string.Empty;
                        textBox.CaretIndex = 0;
                    }
                    textBox.Text = textBox.Text.Insert(caterIndex, input);
                    textBox.CaretIndex = caterIndex + input.Length;
                }

            }
            catch (Exception ex)
            {
            }
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                e.Handled = true;
                RadButton btn = (RadButton)e.Source;
                string buttonTag = btn.Tag != null ? btn.Tag.ToString() : string.Empty;
                if (buttonTag.Equals("TOGGLEKEYBOARD") && !IsSwitchDisable)
                {
                    if (kbQuerty.Visibility == System.Windows.Visibility.Collapsed)
                    {
                        kbQuerty.Visibility = System.Windows.Visibility.Visible;
                        kbSpecialChar.Visibility = System.Windows.Visibility.Collapsed; 
                        kbNumeric.Visibility = System.Windows.Visibility.Collapsed;
                        this.Width = 600;
                    }
                    else
                    {
                        kbNumeric.Visibility = System.Windows.Visibility.Visible;
                        kbSpecialChar.Visibility = System.Windows.Visibility.Collapsed; 
                        kbQuerty.Visibility = System.Windows.Visibility.Collapsed;
                        this.Width = 300;
                    }
                    return;
                }

                if(buttonTag.Equals("SPECIALKEYBOARD"))
                {
                    kbSpecialChar.Visibility = System.Windows.Visibility.Visible;
                    kbQuerty.Visibility = System.Windows.Visibility.Collapsed;
                    kbNumeric.Visibility = System.Windows.Visibility.Collapsed;
                    this.Width = 330;
                }
                Window Parent = Window.GetWindow(this);
                if (!(FocusManager.GetFocusedElement(Parent).GetType() == typeof(TextBox) || FocusManager.GetFocusedElement(Parent).GetType() == typeof(RadWatermarkTextBox) || FocusManager.GetFocusedElement(Parent).GetType() == typeof(PasswordBox) ||
                    FocusManager.GetFocusedElement(Parent).GetType() == typeof(Telerik.Windows.Controls.MaskedInput.PreviewInputTextBox)
                    ))
                {
                    return;
                }


                //TextBox focusedControl = (TextBox)FocusManager.GetFocusedElement(Parent);

                if (buttonTag.Equals(string.Empty))
                {
                    if (!keyMappings.ContainsKey(btn.Content.ToString().ToUpper())) return;
                    byte ch = keyMappings[btn.Content.ToString().ToUpper()];
                    keybd_event(ch, 0, 0, (UIntPtr)0);
                    keybd_event(ch, 0, KEYEVENTF_KEYUP, (UIntPtr)0);


                    //If pressed key is not shift, then release the shhift press button if pressed,
                    if (IsShiftPressed && !buttonTag.Equals("CAPS") && !buttonTag.Equals("SHIFT"))
                    {
                        IsShiftPressed = false;
                        UpdateKeyBoardLayoutOnShiftPress(ShiftKey);
                    }
                    return;
                }
                else
                {
                    switch (buttonTag)
                    {
                        case "BACKSPACE":
                            if (keyMappings.ContainsKey("DELETE"))
                            {
                                byte ch = keyMappings["DELETE"];
                                keybd_event(ch, 0, 0, (UIntPtr)0);
                                keybd_event(ch, 0, KEYEVENTF_KEYUP, (UIntPtr)0);
                            }
                            //InsertText(focusedControl, string.Empty, false, false, true);
                            break;
                        case "ENTER":
                            if (keyMappings.ContainsKey("ENTER"))
                            {
                                byte ch = keyMappings["ENTER"];
                                keybd_event(ch, 0, 0, (UIntPtr)0);
                                keybd_event(ch, 0, KEYEVENTF_KEYUP, (UIntPtr)0);
                            }
                            break;
                        case "SPACE":
                            if (keyMappings.ContainsKey("SPACE"))
                            {
                                byte ch = keyMappings["SPACE"];
                                keybd_event(ch, 0, 0, (UIntPtr)0);
                                keybd_event(ch, 0, KEYEVENTF_KEYUP, (UIntPtr)0);
                            }
                            break;
                        case "MOVERIGHT":
                            if (keyMappings.ContainsKey("RIGHT"))
                            {
                                byte ch = keyMappings["RIGHT"];
                                keybd_event(ch, 0, 0, (UIntPtr)0);
                                keybd_event(ch, 0, KEYEVENTF_KEYUP, (UIntPtr)0);
                            }
                            break;
                        case "MOVELEFT":
                            if (keyMappings.ContainsKey("LEFT"))
                            {
                                byte ch = keyMappings["LEFT"];
                                keybd_event(ch, 0, 0, (UIntPtr)0);
                                keybd_event(ch, 0, KEYEVENTF_KEYUP, (UIntPtr)0);
                            }
                            break;
                        case "TAB":
                            if (keyMappings.ContainsKey("TAB"))
                            {
                                byte ch = keyMappings["TAB"];
                                keybd_event(ch, 0, 0, (UIntPtr)0);
                                keybd_event(ch, 0, KEYEVENTF_KEYUP, (UIntPtr)0);
                            }
                            //btn.Style = (Style)FindResource("selected");
                            break;
                        case "SHIFT":
                            IsShiftPressed = false;
                            UpdateKeyBoardLayoutOnShiftPress(btn);
                            break;
                        case "CAPS":
                            keybd_event(0x14, 0, 0, (UIntPtr)0); // Caps Lock
                            keybd_event(0x14, 0, KEYEVENTF_KEYUP, (UIntPtr)0); // Caps Lock
                            ToggleCaps();
                            break;
                        case "LEFTROUNDBRACE":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x39, 0x9e, 0, (UIntPtr)0); // "9" Press
                            keybd_event(0x39, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "9" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;
                        case "RIGHTROUNDBRACE":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x30, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0x30, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;
                        case "EXCLAMATION":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x31, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0x31, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;

                        case "HASH":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x33, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0x33, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;
                        case "DOLLAR":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x34, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0x34, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;
                        case "PERCENT":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x35, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0x35, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;
                        case "@":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x32, 0x9e, 0, (UIntPtr)0); // "2" Press
                            keybd_event(0x32, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "2" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;
                        case "AND":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0x37, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0x37, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;

                        case "UNDERSCORE":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0xBD, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0xBD, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;

                        case "COLON":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0xBA, 0x9e, 0, (UIntPtr)0); // ":" Press
                            keybd_event(0xBA, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // ":" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;
                        case "DOUBLEQUOTES":
                            keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                            keybd_event(0xDE, 0x9e, 0, (UIntPtr)0); // "0" Press
                            keybd_event(0xDE, 0x9e, KEYEVENTF_KEYUP, (UIntPtr)0); // "0" Release
                            keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
                            break;

                        default:
                            break;
                    }
                }

                //If pressed key is not shift, then release the shhift press button if pressed,
                if (IsShiftPressed && !buttonTag.Equals("CAPS") && !buttonTag.Equals("SHIFT"))
                {
                    IsShiftPressed = false;
                    UpdateKeyBoardLayoutOnShiftPress(ShiftKey);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Views.Common][Keyboard][button_Click][ExceptionStackTrace = " + ex.StackTrace + "]");
            }

        }

        private void UpdateKeyBoardLayoutOnShiftPress(RadButton btn)
        {
            Brush darkBlue = (Brush)(new System.Windows.Media.BrushConverter()).ConvertFromString("#007ACC"); //Color.FromRgb("#007ACC")
            if (darkBlue.ToString() == btn.Background.ToString())
            {
                darkBlue = (Brush)(new System.Windows.Media.BrushConverter()).ConvertFromString("#fff"); //Color.FromRgb("#007ACC")
                btn.Background = darkBlue;
                keybd_event(0xA0, 0x9d, KEYEVENTF_KEYUP, (UIntPtr)0); // Shift Release
            }
            else
            {
                btn.Background = darkBlue;
                keybd_event(0xA0, 0x9d, 0, (UIntPtr)0); // Shift Press
                IsShiftPressed = true;
            }

            //keybd_event(0x14, 0, 0, (UIntPtr)0); // Caps Lock
            //keybd_event(0x14, 0, KEYEVENTF_KEYUP, (UIntPtr)0); // Caps Lock
            ToggleShift(IsShiftPressed);
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        public void AttachEvents(Grid ContainerGrid)
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(ContainerGrid))
            {
                if (tb.Focusable)
                {
                    tb.GotFocus += textGotFocus;
                    tb.LostFocus += textLostFocus;
                }
            }

        }

        private void textLostFocus(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void textGotFocus(object sender, RoutedEventArgs e)
        {

            this.Visibility = System.Windows.Visibility.Visible;
        }
        private void ToggleCaps()
        {
            try
            {
                bool capsLock = Console.CapsLock;
                string buttonTag = string.Empty;
                foreach (RadButton btn in FindVisualChildren<RadButton>(kbQuerty))
                {
                    buttonTag = btn.Tag != null ? btn.Tag.ToString() : string.Empty;
                    if (!string.IsNullOrEmpty(buttonTag)) continue;
                    btn.Content = capsLock == true ? btn.Content.ToString().ToUpper() : btn.Content.ToString().ToLower();
                    //if ((btn.Content.ToString() == ">" || btn.Content.ToString() == "<" || btn.Content.ToString() == "?"))
                    //{
                    //    switch (btn.Content.ToString())
                    //    {
                    //        case ">":
                    //            btn.Content = ".";
                    //            break;

                    //        case "<":
                    //            btn.Content = ",";
                    //            break;

                    //        case "?":
                    //            btn.Content = "/";
                    //            break;

                    //        default:
                    //            break;
                    //    }

                    //}
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void ToggleShift(bool isShiftPrssed)
        {
            try
            {
                bool capsLock = Console.CapsLock;
                if (isShiftPrssed) { capsLock = !capsLock; }
                string buttonTag = string.Empty;
                foreach (RadButton btn in FindVisualChildren<RadButton>(kbQuerty))
                {
                    buttonTag = btn.Tag != null ? btn.Tag.ToString() : string.Empty;
                    if (!string.IsNullOrEmpty(buttonTag)) continue;
                    btn.Content = capsLock ? btn.Content.ToString().ToUpper() : btn.Content.ToString().ToLower();
                    if ((btn.Content.ToString() == "." || btn.Content.ToString() == "," || btn.Content.ToString() == "/"))
                    {
                        switch (btn.Content.ToString())
                        {
                            case ".":
                                btn.Content = ">";
                                break;

                            case ",":
                                btn.Content = "<";
                                break;

                            case "/":
                                btn.Content = "?";
                                break;

                            default:
                                break;
                        }
                    }
                    else if ((btn.Content.ToString() == ">" || btn.Content.ToString() == "<" || btn.Content.ToString() == "?"))
                    {
                        switch (btn.Content.ToString())
                        {
                            case ">":
                                btn.Content = ".";
                                break;

                            case "<":
                                btn.Content = ",";
                                break;

                            case "?":
                                btn.Content = "/";
                                break;

                            default:
                                break;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
            }
        }
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        private void closeKeyBoardButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Hidden;
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (bool.Parse(e.NewValue.ToString()) == true && Convert.ToString(this.Tag) == "Numeric")
            {
                ShowNumeric();
            }
            else
            {
                ShowNonNumeric();
            }
        }

        private void ShowNonNumeric()
        {
            this.Width = 600;
            kbQuerty.Visibility = System.Windows.Visibility.Visible;
            kbNumeric.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void ShowNumeric()
        {
            kbQuerty.Visibility = System.Windows.Visibility.Collapsed;
            kbNumeric.Visibility = System.Windows.Visibility.Visible;

            this.Width = 300;
            kbNumeric.Width = 300;
        }

        private void RadButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            IsScreenKeyBoardPressed = true;
        }

        private void RadButton_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            IsScreenKeyBoardPressed = true;
        }
        private void RadButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void FadeButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsKeyboardFade)
            {
                IsKeyboardFade = true;
                this.Opacity = 0.3;
            }
            else
            {
                IsKeyboardFade = false;
                this.Opacity = 1;
            }
        }

    }
    static class Extensions
    {
        public static bool IsUpper(this string value)
        {
            // Consider string to be uppercase if it has no lowercase letters.
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsLower(value[i]))
                {
                    return false;
                }
            }
            return true;
        }
        public static bool IsLower(this string value)
        {
            // Consider string to be lowercase if it has no uppercase letters.
            for (int i = 0; i < value.Length; i++)
            {
                if (char.IsUpper(value[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}