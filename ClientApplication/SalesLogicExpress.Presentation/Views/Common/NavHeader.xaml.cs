﻿using System.Windows.Controls;
using System;
using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Presentation.Helpers;
namespace SalesLogicExpress.Views.Common
{
    /// <summary>
    /// Interaction logic for NavHeader.xaml
    /// </summary>
    public partial class NavHeader : UserControl
    {
        public NavHeader()
        {
            InitializeComponent();
            AddTagsToControls();
        }

        private void AddTagsToControls()
        {
            ButtonFullscreenWindow.Tag = Helpers.Constants.App_FullScreen;
            ButtonMaximizeWindow.Tag = Helpers.Constants.App_MaximizeScreen;
            ButtonMinimizeWindow.Tag = Helpers.Constants.App_MinimizeScreen;
            ButtonCloseWindow.Tag = Helpers.Constants.App_Close;
            ButtonViewSyncDetail.Tag = Helpers.Constants.App_ViewSyncDetail;
        }

        public void ToggleLastSyncLabel(bool show)
        {
            LastSyncedDateLabel.Visibility = show ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;


        }

        private void CloseDropdown_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //RadDropDownButtonactive.IsOpen = false;
            //RadDropDownButtondeactive.IsOpen = false;
        }

        private void MaximizeWindow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ResourceManager.AppWindowState = ResourceManager.ApplicationWindowState.Maximized;
            ResourceManager.ToggleWindowState();
            Taskbar.Show();
        }

        private void FullscreenWindow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ResourceManager.AppWindowState = ResourceManager.ApplicationWindowState.FullScreen;
            ResourceManager.ToggleWindowState();
            Taskbar.Hide();
        }

        private void MinimizeWindow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Taskbar.Show();
        }








    }
}
