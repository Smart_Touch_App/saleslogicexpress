﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using SalesLogicExpress.Domain;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Calendar;
using navbar = SalesLogicExpress.Views.Common;
using PayloadManager = SalesLogicExpress.Application.ViewModelPayload.PayloadManager;
using ViewModelApplication = SalesLogicExpress.Application;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Application.Helpers;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ServiceRoute.xaml
    /// </summary>
    public partial class ServiceRoute : BaseWindow
    {

        viewmodels.ServiceRoute viewModel;
        public ServiceRoute()
        {
            InitView();
        }

        private void InitView()
        {
            InitializeComponent();
            DefaultSeletedTab = "TabItem_ServiceRoute_DailyStop";
            ScreenID = Helpers.Constants.Title_ServiceRouteViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            this.Activated += WindowActivated;
            this.Loaded += ServiceRoute_Loaded;
            //busyIndicator.IsBusy = true;
            //busyIndicator.Visibility = System.Windows.Visibility.Visible;
            LoadContext();
            AddTagToControls();
        }

        private void AddTagToControls()
        {
            ButtonCreateStop.Tag = Helpers.Constants.SRV_ViewCreateStopDialog;
            ButtonStopSequencing.Tag = Helpers.Constants.SRV_ViewStopSequencing;
            ButtonAddProspect.Tag = Helpers.Constants.SRV_ViewAddProspectDialog;

            TabItemCustomers.Tag = Helpers.Constants.SRV_ViewCustomers;
            TabItemDailyStops.Tag = Helpers.Constants.SRV_ViewDailyStop;
            TabItemProspects.Tag = Helpers.Constants.SRV_ViewProspects;

        }

        protected override void dialogWindow_PreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            if (viewModel.IsDirty)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Content = "Do you really want to close?";
                bool? dialogResult = null;
                parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
                parameters.OkButtonContent = "OK";
                parameters.CancelButtonContent = "Cancel";
                parameters.WindowStyle = (Style)(this.Resources["RadWindowStyle"]);
                RadWindow.Confirm(parameters);

                if (dialogResult == null)
                {
                    e.Cancel = true;
                    return;
                }
                e.Cancel = !(bool)dialogResult.Value;
                viewModel.IsDirty = !(bool)dialogResult.Value;
            }
        }

        protected override void dialogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            viewModel.IsBusy = false;

            if (((System.Windows.Controls.HeaderedContentControl)(e.OriginalSource)).Header.ToString() == "Create Stop")
            {
                viewModel.IsCreateStopButtonIsEnabled = true;
                if (viewModel.IsDailyStopTabActive)
                {

                    if (viewModel.SelectedItem.StopType.ToLower() != "moved")
                        viewModel.IsMoveStopButtonEnabled = true;
                    if (viewModel.SelectedItem.HasActivity
                        || viewModel.SelectedItem.SaleStatus.ToLower() == "nosale" || viewModel.SelectedItem.CompletedActivity > 0 || viewModel.SelectedItem.PendingActivity > 0)
                        viewModel.IsMoveStopButtonEnabled = false;
                }

            }

            if (((System.Windows.Controls.HeaderedContentControl)(e.OriginalSource)).Header.ToString() == "Move Stop")
            {
                viewModel.IsMoveStopButtonEnabled = true;
                viewModel.IsCreateStopButtonIsEnabled = true;
            }
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                if (viewModel==null)
                {
                    viewModel = new viewmodels.ServiceRoute();
                    viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        NavigationHeader.DataContext = cminfo;
                        //viewModel.IsBusy = false;
                    }));
                }
                else
                {
                    viewModel.RefreshStops(viewModel.DailyStopDate);
                    //viewModel.IsBusy = false;
                }

            });
        }

        private void ServiceRoute_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ServiceRoute_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults();
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);

            // Refreshing the DataContext
            //TODO : find a better way of refreshing the context
            //viewmodels.ServiceRoute viewModel = new viewmodels.ServiceRoute(_payload);
            //viewModel.DailyStopDate = viewmodels.ServiceRoute.SelectedCalendarDate;
            //this.DataContext = viewModel;
            //viewModel.MessageToken = this.Token;
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Service Route";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ServiceRoute;

        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {

            }
        }
        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((viewmodels.ServiceRoute)this.DataContext).IsBusy = true;
            }
            CloseForwardedWindows();
            System.Diagnostics.Debug.WriteLine("ServiceRoute.xaml.cs > RefreshWindow --> payload = " + payload);
            LoadContext();
            base.RefreshWindow(payload);
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ToggleCreateStopButton();

            // MockCustomerListView.SelectedItem
        }

        private void ToggleCreateStopButton()
        {
            ButtonCreateStop.IsEnabled = false;

            if (MockProspectListView.SelectedItems.Count > 0 && rtc_ServiceRoute.SelectedIndex == 2)
            {
                ButtonCreateStop.IsEnabled = true;
                return;
            }

            if (MockCustomerListView.SelectedItems.Count > 0 && rtc_ServiceRoute.SelectedIndex == 1)
            {
                ButtonCreateStop.IsEnabled = true;
                return;
            }

            if (DailyStopList.SelectedItems.Count > 0 && rtc_ServiceRoute.SelectedIndex == 0)
            {
                ButtonCreateStop.IsEnabled = true;
                return;
            }
        }

        private void rtc_ServiceRoute_SelectionChanged(object sender, Telerik.Windows.Controls.RadSelectionChangedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ToggleCreateStopButton();
                MockProspectListView.SelectedItem = null;
                MockCustomerListView.SelectedItem = null;
                DailyStopList.SelectedItem = null;
                if (viewModel.SelectedItem != null)
                    viewModel.SelectedItem = null;
                viewModel.IsCreateStopButtonIsEnabled = false;
                viewModel.IsMoveStopButtonEnabled = false;
            }
        }

        private void CreateStopText_TextChanged(object sender, TextChangedEventArgs e)
        {
            MockProspectListView.SelectedItem = null;
            MockCustomerListView.SelectedItem = null;
            DailyStopList.SelectedItem = null;
            ToggleCreateStopButton();
        }

        public void CloseForwardedWindows()
        {
            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.CustomerHome);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PickOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            AssociatedWindows = WindowsToClosed;
            CloseAssociatedWindows();
        }

    }

    public class DisableWeekendsSelection : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            CalendarButtonContent content = item as CalendarButtonContent;

            if (content != null)
            {
            //************************************************************************************************
            // Comment: Commented to enable sunday and saturday.
            // created: Dec 25, 2015
            // Author: Vivensas (Rajesh,Yuvaraj)
            // Revisions: 
            //*************************************************************************************************
            //    if (content.Date.DayOfWeek == DayOfWeek.Saturday
            //        || content.Date.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        content.IsEnabled = false;
            //    }
            //*************************************************************************************************
            // Vivensas changes ends over here
            //**************************************************************************************************
            }
            return DefaultTemplate;
        }

        private DataTemplate defaultTemplate;
        public DataTemplate DefaultTemplate
        {
            get
            {
                return defaultTemplate;
            }
            set
            {
                this.defaultTemplate = value;
            }
        }

    }


}
