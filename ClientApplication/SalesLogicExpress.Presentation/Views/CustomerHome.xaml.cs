﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using GalaSoft.MvvmLight.Messaging;
using Telerik.Windows.Controls;
using SalesLogicExpress.Presentation.Helpers;
using ViewModels = SalesLogicExpress.Application.ViewModels;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using log4net;
using SalesLogicExpress.Application.Helpers;
using System.Windows.Controls.Primitives;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for CustomerHome.xaml
    /// </summary>
    public partial class CustomerHome : BaseWindow
    {
        private readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Views.CustomerHome");

        ViewModels.CustomerHome viewModelCustomerHome = null;

        object _payload;
        public CustomerHome()
        {
            Logger.Info("[SalesLogicExpress.Views.CustomerHome][CustomerHome][Enter in Constructor]");
            InitializeComponent();
            DefaultSeletedTab = "TabItemCustomerDashboard";
            ScreenID = Helpers.Constants.Title_CustomerHomeViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            this.DataContext = viewModelCustomerHome;
            LoadContext();
            this.Activated += WindowActivated;
            AddTagsToControls();
            CloseForwardedWindows();

        }

        private void AddTagsToControls()
        {
            ButtonOrderTemplate.Tag = Helpers.Constants.OT_ViewOrderTemplate;
            ButtonPreOrder.Tag = Helpers.Constants.PO_ViewPreOrder;
            ButtonSalesOrder.Tag = Helpers.Constants.CD_ViewCustomerSalesOrder;
            ButtonReturnsAndCredit.Tag = Helpers.Constants.CD_ViewReturnsAndCredit;
            ButtonPaymentAndAR.Tag = Helpers.Constants.CD_ViewPaymentAndAR;
            ButtonOrderHistory.Tag = Helpers.Constants.CD_ViewCustomerOrderHistory;
            ButtonCreateStopDashboard.Tag = Helpers.Constants.SRV_ViewCreateStopDialog;
            ButtonNoActivity.Tag = Helpers.Constants.CD_MarkNoActivity;

            TabItem_CustomerHome_Activity.Tag = Helpers.Constants.CD_ViewCustomerActivity;
            TabItemCustomerContacts.Tag = Helpers.Constants.CD_ViewCustomerContacts;
            TabItemCustomerDashboard.Tag = Helpers.Constants.CD_ViewCustomerDashboard;
            TabItemCustomerInformation.Tag = Helpers.Constants.CD_ViewCustomerInfo;
            TabItemCustomerItemRestrictions.Tag = Helpers.Constants.CD_ViewCustomerItemRestrictions;
            TabItemCustomerNotes.Tag = Helpers.Constants.CD_ViewCustomerNotes;
            TabItemCustomerPricing.Tag = Helpers.Constants.CD_ViewCustomerPricing;
            QuotesCustomerTab.Tag = Helpers.Constants.CD_ViewCustomerQuotes;


        }
       

        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                viewModelCustomerHome = new ViewModels.CustomerHome(this.Token);
                SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                cminfo.ParentViewModel = viewModelCustomerHome;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                      {
                          this.DataContext = viewModelCustomerHome;
                          NavigationHeader.DataContext = cminfo;
                          CustomerAddressPanel.DataContext = cminfo;
                          //LoadContextAsync();

                      }));
            });
        }
        async void LoadContextAsync()
        {
            await Task.Run(() =>
            {
                LoadContext();
            });
        }
        private void WindowActivated(object sender, EventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Views.CustomerHome][WindowActivated][Enter in WindowActivated]");

            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);

        }
        public override void RefreshWindow(object payload)
        {
            var oldContext = ((ViewModels.CustomerHome)this.DataContext);
            if (this.DataContext != null)
            {
                oldContext.IsBusy = true;
            }
            Logger.Info("[SalesLogicExpress.Views.CustomerHome][RefreshWindow][Enter in RefreshWindow]");
            CloseForwardedWindows();
            LoadContext();
            //RTC_MainTabControl.SelectedIndex = 0;
            
            //if(((ViewModels.CustomerHome)oldContext).IsQuoteTabSelected)
            //    RTC_MainTabControl.SelectedIndex = 7;


            //When trying to open activity of future stops,the NextStopDates property is getting changed 
            //because of the async load context, hence asking thread to sleep so that all the list of updated/populated
            System.Threading.Thread.Sleep(500);            

            base.RefreshWindow(payload);
        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Customer Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ServiceRoute;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.CustomerHome;

        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {

            }
        }

        protected override void dialogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ButtonMoveStopDashboard.IsEnabled = false;
            ButtonCreateStopDashboard.IsEnabled = false;
        }
        protected override void dialogWindow_PreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            ButtonMoveStopDashboard.IsEnabled = true;
            ButtonCreateStopDashboard.IsEnabled = true;

        }
        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (viewModelCustomerHome != null && viewModelCustomerHome.NewContact != null)
                viewModelCustomerHome.NewContact.PropertyLostFocus.Execute(sender);
            TextBox text = sender as TextBox;
            BindingOperations.GetBindingExpression(text, TextBox.TextProperty).UpdateSource();
        }

        private void OnTextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                TraversalRequest request = new TraversalRequest(FocusNavigationDirection.Next);
                MoveFocus(request);
            }
        }

        private void txt_ZipCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]([-][0-9]{1,3})?$");
            e.Handled = !regex.IsMatch(e.Text);
        }

        public void CloseForwardedWindows()
        {
            List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View> WindowsToClosed = new List<SalesLogicExpress.Application.Helpers.ViewModelMappings.View>();
            //WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.DeliveryScreen);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PickOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.PreviewOrder);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.Order);
            WindowsToClosed.Add(SalesLogicExpress.Application.Helpers.ViewModelMappings.View.OrderTemplate);
            AssociatedWindows = WindowsToClosed;
            CloseAssociatedWindows();
        }

        private void txt_ZipCode_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void CustomerActivityList_Sorting(object sender, GridViewSortingEventArgs e)
        {
            if (e.NewSortingState == SortingState.None)
            {
                e.NewSortingState = SortingState.Ascending;
            }
        }

        private void btn_OrderTemplate_Click(object sender, RoutedEventArgs e)
        {
        }


        private void RadTabItem_PreviewTouchDown(object sender, TouchEventArgs e)
        {
           
        }

        private void RadTabItem_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Helpers.CanvasHelper.ClearCanvas();
            //Logger.Error("Helpers.CanvasHelper.ClearCanvas()");

        }

        private void RadTabItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Helpers.CanvasHelper.ClearCanvas();
            //Logger.Error("Helpers.CanvasHelper.ClearCanvas()");
        }
        
        private void ContactListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Selector selector = sender as Selector;
            RadListBox rd = sender as RadListBox;
            
            if (rd != null)
            {
                Application.ViewModels.CustomerContactsViewModel ViewModel= rd.DataContext as Application.ViewModels.CustomerContactsViewModel;

                if (ViewModel!=null)
                {
                    var item = ViewModel.CustomerContactList.Where(x => x.FirstName.ToLower() == ViewModel.FirstName.ToLower()).FirstOrDefault();

                    if (item!=null)
                    {
                        int index = ViewModel.CustomerContactList.IndexOf(item);
                        //rd.UpdateLayout();
                        rd.ScrollIntoView(item);
                    }
                    
                }
            }
        }
    }
}
