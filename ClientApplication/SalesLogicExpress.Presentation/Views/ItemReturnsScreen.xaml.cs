﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SalesLogicExpress.Presentation.Helpers;
using ViewModels= SalesLogicExpress.Application.ViewModels;
using System.Windows.Threading;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Application.Helpers;
namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ItemReturnsScreen.xaml
    /// </summary>
    public partial class ItemReturnsScreen : BaseWindow
    {
        ViewModels.ItemReturnScreenVM viewModel = null;
        public ItemReturnsScreen()
        {
            InitializeComponent();
            LoadContext();
        }

        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {

        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = PayloadManager.ReturnOrderPayload.CurrentViewName== ViewModelMappings.View.OrderReturnsScreen .ToString()?
                    ViewModelMappings.View.OrderReturnsScreen.GetEnumDescription():ViewModelMappings.View.ItemReturnsScreen.GetEnumDescription();
            //if(PayloadManager.ReturnOrderPayload.IsReturnOrder)

                
            //else
            //    SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.ItemReturnsScreen.GetEnumDescription();

            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Returns And Credits";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ReturnsAndCredits;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ItemReturnsScreen;
        }

        private void btn_clear_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnDeliver_Click(object sender, RoutedEventArgs e)
        {

        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                viewModel = new ViewModels.ItemReturnScreenVM();
                viewModel.ModelChanged += viewModel_ModelChanged;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    viewModel.MessageToken = this.Token;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    NavigationHeader.DataContext = cminfo;
                    this.DataContext = viewModel;
                }));
            });
        }

        void viewModel_ModelChanged(object sender, ViewModels.BaseViewModel.ModelChangeArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
