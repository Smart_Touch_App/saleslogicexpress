﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using ViewModels = SalesLogicExpress.Application.ViewModels;



namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ReturnOrderPickScreen.xaml
    /// </summary>
    /// 
    public partial class ReturnOrderPickScreen : BaseWindow
    {
        ViewModels.CustomerReturnsPickViewModel viewModel;
        public ReturnOrderPickScreen()
        {
            InitializeComponent();
            this.DefaultFocusElement = ManualPickTextBox;
            LoadContext();
        }

        private void RadButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_PickOrder_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Item Returns Pick";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Item Return";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ItemReturnsScreen;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ReturnOrderPickScreen;
        }

        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                viewModel = new ViewModels.CustomerReturnsPickViewModel();
                viewModel.ModelChanged += viewModel_ModelChanged;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    viewModel.MessageToken = this.Token;
                    SalesLogicExpress.Application.ViewModels.CommonNavInfo cminfo = SalesLogicExpress.Application.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    NavigationHeader.DataContext = cminfo;
                    this.DataContext = viewModel;
                    this.AlwaysShowKeyboard = true;

                    base.AlwaysShowNumericKeyboard();
                }));
            });
        }

        private void viewModel_ModelChanged(object sender, ViewModels.BaseViewModel.ModelChangeArgs e)
        {
            //throw new NotImplementedException();
        }
        private void RadNumericUpDown_ValueChanged(object sender, Telerik.Windows.Controls.RadRangeBaseValueChangedEventArgs e)
        {

        }

        private void RCB_ManualPickReasonList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

      
        private void txt_OrderQty_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !(new Regex(@"^\d{0,4}(\d{0,2})?$").IsMatch(e.Text));
        }
        private void txt_OrderQty_GotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).SelectAll();
        }
        private void txt_OrderQty_KeyDown(object sender, KeyEventArgs e)
        {

        }

       
        private void btn_RemoveExceptions_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PickOrderGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {

        }

        private void ExceptionGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            ManualPickTextBox.Focus();
        }
    }
}
