﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using ViewModelApplication = SalesLogicExpress.Application;
using System.Windows.Threading;
using SalesLogicExpress.Application.Helpers;


namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for RouteSettlement.xaml
    /// </summary>
    public partial class RouteSettlement : BaseWindow
    {
        viewmodels.RouteSettlementViewModel viewModel;

        public RouteSettlement()
        {
            InitializeComponent();
            DefaultSeletedTab = "TabItem_RouteSettlement_Transactions";
            InitView();
            LoadContext();
        }
        private void InitView()
        {
            this.Activated += RouteSettlement_Activated;
            this.Loaded += RouteSettlement_Loaded;
        }

        void RouteSettlement_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("RouteSettlement_Loaded, " + DateTime.Now.Millisecond);
            SetNavigationDefaults();
        }

        void RouteSettlement_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
        //public void LoadContext()
        //{
        //    if (viewModel == null)
        //    {
        //        viewModel = new viewmodels.RouteSettlementViewModel();
        //        viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
        //        cminfo.ParentViewModel = viewModel;
        //        this.DataContext = viewModel;
        //        viewModel.MessageToken = this.Token;
        //        NavigationHeader.DataContext = cminfo;

        //    }
        //}

        async void LoadContext()
        {
            await Task.Run(() =>
            {
                if (true)
                {

                    viewModel = new viewmodels.RouteSettlementViewModel();
                    //viewmodels.ServiceRoute.SelectedCalendarDate = viewModel.DailyStopDate;
                    viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
                    cminfo.ParentViewModel = viewModel;
                    Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataContext = viewModel;
                        viewModel.MessageToken = this.Token;
                        if (viewModel.RouteSettlemenntTransationsVM != null)
                        {
                            viewModel.RouteSettlemenntTransationsVM.MessageToken = this.Token;

                        }
                        if (viewModel.RouteSettlementSettlementsVM != null)
                        {
                            viewModel.RouteSettlementSettlementsVM.MessageToken = this.Token;
                        }

                        NavigationHeader.DataContext = cminfo;
                        //viewModel.IsBusy = false;
                    }));
                }


            });
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Route Home";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Route Settlement";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.RouteHome;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.RouteSettlement;

        }

        public override void RefreshWindow(object payload)
        {
            //if (this.DataContext != null)
            //{
            //    ((viewmodels.RouteSettlementViewModel)this.DataContext).IsBusy = true;
            //}
            System.Diagnostics.Debug.WriteLine("RouteSettlement.xaml.cs > RefreshWindow --> payload = " + payload);
            LoadContext();
            base.RefreshWindow(payload);
        }

    }
}
