﻿using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using ViewModelApplication = SalesLogicExpress.Application;
using System.Windows.Threading;
using System.Windows.Input;
using System.Text.RegularExpressions;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for ProspectsHome.xaml
    /// </summary>
    public partial class ProspectsHome : BaseWindow
    {
        viewmodels.ProspectsHome viewModel = null;
        int Count = 0;
        public ProspectsHome()
        {
            InitializeComponent();
            DefaultSeletedTab = "TabItemDashboard";
            ScreenID = Helpers.Constants.Title_RouteHomeViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            this.Activated += WindowActivated;
            this.Loaded += ServiceRoute_Loaded;
            LoadContext();
            AddTagToControls();
            ButtonEditProspect.IsEnabled = false;
            ButtonDeleteProspect.IsEnabled = false;
            ButtonEditExpensed.IsEnabled = false;
            ButtonDeleteExpensed.IsEnabled = false;
        }
        private void AddTagToControls()
        {
            TabItemAgingSummary.Tag = Helpers.Constants.RH_ViewAgingSummary;
            TabItemDashboard.Tag = Helpers.Constants.SRV_ViewCustomerDashboard;
            TabItemInventory.Tag = Helpers.Constants.RH_ViewItemInventory;
            TabItemReplenishment.Tag = Helpers.Constants.RH_ViewItemReplenishment;
            TabItemProspectContacts.Tag = Helpers.Constants.RH_ViewCycleCount;
        }
        async void LoadContext()
        {
            await Task.Run(() =>
            {
                SetNavigationDefaults();
                 viewModel = new viewmodels.ProspectsHome();
                viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataContext = viewModel;
                    viewModel.MessageToken = this.Token;
                    NavigationHeader.DataContext = cminfo;
                }));
            });
        }
        private void ServiceRoute_Loaded(object sender, RoutedEventArgs e)
        {
            SetNavigationDefaults();
        }

        private void WindowActivated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ServiceRoute;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.ProspectsHome;

        }
        protected override void DisplaySettingsChanged(object sender, EventArgs e)
        {
            base.DisplaySettingsChanged(sender, e);
            if (WindowOrientation == WindowOrientation.Portrait)
            {

            }
            if (WindowOrientation == WindowOrientation.Landscape)
            {

            }
        }

        private void ProspectsHomeTabControl_SelectionChanged(object sender, Telerik.Windows.Controls.RadSelectionChangedEventArgs e)
        {
            if (ProspectsHomeTabControl.SelectedIndex == 7)
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Visible;
                rtc_Equipment.Visibility = System.Windows.Visibility.Collapsed;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Prospects Home";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Competitors";
                Count = 0;
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 8)
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Collapsed;
                rtc_Equipment.Visibility = System.Windows.Visibility.Visible;

                SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Prospects Home";
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Equipment";
                Count = 0;
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 0)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Dashboard";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 1)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Activity";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 2)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Notes";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 3)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Information";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 4)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Contacts";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 5)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Pricing";
            }
            else if (ProspectsHomeTabControl.SelectedIndex == 6)
            {
                SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = "Quotes";
            }
            else if (ProspectsHomeTabControl.SelectedIndex != 2 || viewModel.HasPendingCountRequests)
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Collapsed;
                rtc_Equipment.Visibility = System.Windows.Visibility.Collapsed;
            }
            
            else
            {
                rtc_Competitor.Visibility = System.Windows.Visibility.Collapsed;
                rtc_Equipment.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void CreateStopText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        public override void RefreshWindow(object payload)
        {
            if (this.DataContext != null)
            {
                ((viewmodels.ProspectsHome)this.DataContext).IsBusy = true;
            }
            System.Diagnostics.Debug.WriteLine("ProspectsHome.xaml.cs > RefreshWindow --> payload = " + payload);
            LoadContext();
            base.RefreshWindow(payload);
        }
        private void EquipmCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                Count += 1;
            }
            if(Count > 1)
            {
                ButtonDeleteProspect.IsEnabled = true;
                ButtonEditProspect.IsEnabled = false;
                ButtonAddProspect.IsEnabled = false;
            }
            else
            {
                ButtonDeleteProspect.IsEnabled = true;
                ButtonEditProspect.IsEnabled = true;
                ButtonAddProspect.IsEnabled = false;
            }
        }

        private void EquipmentCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == false)
            {
                Count -= 1;
            }
            if (Count > 1)
            {
                ButtonDeleteProspect.IsEnabled = false;
                ButtonEditProspect.IsEnabled = false;
                ButtonAddProspect.IsEnabled = true;
            }
            else if (Count == 0)
            {
                ButtonEditProspect.IsEnabled = false;
                ButtonDeleteProspect.IsEnabled = false;
                ButtonAddProspect.IsEnabled = true;
            }
            else
            {
                ButtonEditProspect.IsEnabled = true;
                ButtonDeleteProspect.IsEnabled = true;
                ButtonAddProspect.IsEnabled = false;
            }
        }

        private void ExpCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                Count += 1;
            }
            if (Count > 1)
            {
                ButtonDeleteExpensed.IsEnabled = true;
                ButtonEditExpensed.IsEnabled = false;
                ButtonAddExpensed.IsEnabled = false;
            }
            else
            {
                ButtonDeleteExpensed.IsEnabled = true;
                ButtonEditExpensed.IsEnabled = true;
                ButtonAddExpensed.IsEnabled = false;
            }
        }

        private void ExpCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == false)
            {
                Count -= 1;
            }
            if (Count > 1)
            {
                ButtonDeleteExpensed.IsEnabled = false;
                ButtonEditExpensed.IsEnabled = false;
                ButtonAddExpensed.IsEnabled = true;
            }
            else if (Count == 0)
            {
                ButtonEditExpensed.IsEnabled = false;
                ButtonDeleteExpensed.IsEnabled = false;
                ButtonAddExpensed.IsEnabled = true;
            }
            else
            {
                ButtonEditExpensed.IsEnabled = true;
                ButtonDeleteExpensed.IsEnabled = true;
                ButtonAddExpensed.IsEnabled = false;
            }
        }

        int CoffeeCount = 0;
        private void CoffeeCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                CoffeeCount += 1;
            }
            if (CoffeeCount > 1)
            {
                ButtonDeleteCoffeeProspect.IsEnabled = true;
                ButtonEditCoffeeProspect.IsEnabled = false;
                ButtonAddCoffeeProspect.IsEnabled = false;
            }
            else if (CoffeeCount == 1)
            {
                ButtonDeleteCoffeeProspect.IsEnabled = true;
                ButtonEditCoffeeProspect.IsEnabled = true;
                ButtonAddCoffeeProspect.IsEnabled = false;
            }
        }

        private void CoffeeCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == false)
            {
                CoffeeCount -= 1;
            }
            if (CoffeeCount > 1)
            {
                ButtonAddCoffeeProspect.IsEnabled = false;
                ButtonEditCoffeeProspect.IsEnabled = false;
                ButtonDeleteCoffeeProspect.IsEnabled = true;
            }
            else if (CoffeeCount == 0)
            {
                ButtonDeleteCoffeeProspect.IsEnabled = false;
                ButtonEditCoffeeProspect.IsEnabled = false;
                ButtonAddCoffeeProspect.IsEnabled = true;
            }
            else if (CoffeeCount == 1)
            {
                ButtonEditCoffeeProspect.IsEnabled = true;
                ButtonDeleteCoffeeProspect.IsEnabled = true;
                ButtonAddCoffeeProspect.IsEnabled = false;
            }
        }

        int AlliedCount = 0;
        private void AlliedCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == true)
            {
                AlliedCount += 1;
            }
            if (AlliedCount > 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = false;
                ButtonAddAllied.IsEnabled = false;
            }
            else if (AlliedCount == 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = true;
                ButtonAddAllied.IsEnabled = false;
            }
        }

        private void AlliedCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (sender as CheckBox);
            bool flag = checkbox.IsChecked.Value;
            if (flag == false)
            {
                AlliedCount -= 1;
            }
            if (AlliedCount > 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = false;
                ButtonAddAllied.IsEnabled = false;
            }
            else if (AlliedCount == 0)
            {
                ButtonDeleteAllied.IsEnabled = false;
                ButtonEditAllied.IsEnabled = false;
                ButtonAddAllied.IsEnabled = true;
            }
            else if (AlliedCount == 1)
            {
                ButtonDeleteAllied.IsEnabled = true;
                ButtonEditAllied.IsEnabled = true;
                ButtonAddAllied.IsEnabled = false;
            }
        }

        protected override void dialogWindow_PreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            if (viewModel.IsDirty)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Content = "Do you really want to close?";
                bool? dialogResult = null;
                parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
                parameters.OkButtonContent = "OK";
                parameters.CancelButtonContent = "Cancel";
                parameters.WindowStyle = (Style)(this.Resources["RadWindowStyle"]);
                RadWindow.Confirm(parameters);

                if (dialogResult == null)
                {
                    e.Cancel = true;
                    return;
                }
                e.Cancel = !(bool)dialogResult.Value;
                viewModel.IsDirty = !(bool)dialogResult.Value;
            }
        }
        private void OnTextBoxKeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Return)
            //{
            //    TraversalRequest request = new TraversalRequest(FocusNavigationDirection.Next);
            //    MoveFocus(request);
            //}
        }
        private void txt_ZipCode_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("^[0-9]([-][0-9]{1,3})?$");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void txt_ZipCode_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

    }
}