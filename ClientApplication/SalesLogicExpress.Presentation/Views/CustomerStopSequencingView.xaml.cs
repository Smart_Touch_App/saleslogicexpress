﻿using SalesLogicExpress.Application.ViewModels;
using SalesLogicExpress.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows;
using Telerik.Windows.DragDrop;
using viewmodels = SalesLogicExpress.Application.ViewModels;
using ViewModelApplication = SalesLogicExpress.Application;
using SalesLogicExpress.Application.Helpers;



namespace SalesLogicExpress.Views
{
    /// <summary>
    /// Interaction logic for CustomerStopSequencingView.xaml
    /// </summary>
    public partial class CustomerStopSequencingView : BaseWindow
    {
        public CustomerStopSequencingView()
        {
            InitializeComponent();
            ScreenID = Helpers.Constants.Title_StopSequencingViewTitle;
            ScreenLifeCycle = Helpers.Constants.ApplicationLifeCycle;
            this.Activated += CustomerStopSequencingView_Activated;

            CustomerStopSequencingViewModel vm = new CustomerStopSequencingViewModel();
            this.DataContext = vm;
            vm.MessageToken = this.Token;
            //SalesLogicExpress.Application.ViewModels.CommonNavInfo
            viewmodels.CommonNavInfo cminfo = ViewModelApplication.Helpers.ResourceManager.CommonNavInfo;// new viewmodels.CommonNavInfo();
            cminfo.ParentViewModel = vm;
            NavigationHeader.DataContext = cminfo;
        }

        void CustomerStopSequencingView_Activated(object sender, EventArgs e)
        {
            SetNavigationDefaults();
            base.DisplaySettingsChanged(sender, e);
        }
        private void SetNavigationDefaults()
        {
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.RefreshPreviousWindow = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousViewTitle = "Service Route";
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ViewTitle = ViewModelMappings.View.CustomerStopSequencingView.GetEnumDescription();
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.MessageToken = this.Token;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.ShowBackNavigation = true;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.PreviousView = Application.Helpers.ViewModelMappings.View.ServiceRoute;
            SalesLogicExpress.Application.ViewModels.CommonNavInfo.CurrentView = Application.Helpers.ViewModelMappings.View.CustomerStopSequencingView;

        }
    }
}
