setlocal
echo Executing CustomizeRemote.bat... >> setup.log

echo Start the Environment Variable Setup >> setup.log
call sqlanyenv.bat
echo End the Environment Variable Setup >> setup.log

echo Executing CustomizeRemote.sql... >> setup.log
REM -- "%__SABIN%\dbisql" -onerror exit -c "dsn=dsn_remote_%2" read "%~dp0CustomizeRemote.sql" [%1]
"%__SABIN%\dbisql" -onerror exit -c "UID=dba;PWD=sql;Server=remote_eng;DBN=remote_db;ASTART=No" read "CustomizeRemote.sql" [%1]
if errorlevel 1 goto RunTimeError
echo Successfully customized the user... >> setup.log
:done

:RunTimeError
echo ###Error: A step in the sample script failed with an error in CustomizeRemote.bat... >> setup.log
SET errorlevel=1
goto done

:done
SET __SA=
SET __SABIN=
exit /b %errorlevel%
endlocalSetu