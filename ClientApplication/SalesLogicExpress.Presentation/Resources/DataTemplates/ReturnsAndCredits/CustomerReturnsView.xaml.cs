﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace SalesLogicExpress.Resources.DataTemplates.ReturnsAndCredits
{
    /// <summary>
    /// Interaction logic for CustomerReturnsView.xaml
    /// </summary>
    public partial class CustomerReturnsView
    {

        public CustomerReturnsView()
        {
        }
     
        private void grd_Returns_Loaded(object sender, RoutedEventArgs e)
        {
            Telerik.Windows.Controls.RadGridView grd = sender as Telerik.Windows.Controls.RadGridView;
            GroupDescriptor orderDescriptor = new GroupDescriptor();
            orderDescriptor.Member = "ReturnOrderID";
            orderDescriptor.SortDirection = ListSortDirection.Descending;
            grd.GroupDescriptors.Add(orderDescriptor);
           
        }
       
    }
}
