﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
namespace SalesLogicExpress.Presentation.Converters
{
    public class ConvertToPascalCase : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strPascal = "";
            if (!(value == null || value.ToString() == ""))
            {
                string tempString = value.ToString();
                strPascal = tempString.ToLower().Trim();
                strPascal = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(strPascal);
                return strPascal;
            }
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString();
        }
    }
}
