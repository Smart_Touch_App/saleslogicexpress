﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class StopTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string SequenceNo = "";
            if (!(value == null || value.ToString() == ""))
            {
                try
                {
                    int number = System.Convert.ToInt32(value);
                  
                        if (number < 10)
                        {
                            SequenceNo = 0 + System.Convert.ToString(value);
                        }
                        else
                        {
                            SequenceNo = System.Convert.ToString(value);
                        }
                    
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            return SequenceNo;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }

    }
}
