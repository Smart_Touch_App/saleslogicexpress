﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class PhoneFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return "";
            string phoneno = Regex.Replace(value.ToString(), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            try
            {
                string fphoneno = (string.IsNullOrEmpty(phoneno) || phoneno == "NA" ? "" : phoneno);//" " + String.Format("{0:(###) ###-#### ##}", Double.Parse(phoneno)));
                fphoneno = fphoneno.IndexOf("()") > -1 ? fphoneno.Replace("()", "") : fphoneno;
                return fphoneno;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
