﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Threading.Tasks;

namespace SalesLogicExpress.Presentation.Converters
{
    public class WidthConverterCustStopSequnecing : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            try
            {
                return Double.Parse(value.ToString()) - (parameter != null ? System.Convert.ToInt32(parameter) : 10);

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }
}
