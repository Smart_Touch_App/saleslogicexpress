﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Presentation.Converters
{
    /// <summary>
    /// E:\Projects\saleslogicexpress\ClientApplication\SalesLogicExpress.Presentation\Converters\SelectOpenARInvoicesConverter.cs
    /// </summary>
    public class SelectOpenARInvoicesConverter : IMultiValueConverter 
    {
        private static CheckBox chkSelectAll = null;
        private static RadGridView InvoiceGrid = null;
        public object Convert(object[] Values, Type Target_Type, object Parameter, CultureInfo culture)
        {
            var commandParameters = new SelectOpenARInvoicesParameters();

            //commandParameters.IsHeaderCheckBox = System.Convert.ToBoolean(Values[2]);

            //if (commandParameters.IsHeaderCheckBox)
            //{
            //    commandParameters.CheckBoxControl = (CheckBox)(Values[0]);
            //    commandParameters.OpenInvoiceGrid = (RadGridView)(Values[1]);
            //    chkSelectAll = commandParameters.CheckBoxControl;
            //    InvoiceGrid = commandParameters.OpenInvoiceGrid;
            //}
            //else
            //{
            //    commandParameters.OpenInvoiceGrid = (InvoiceGrid==null?(RadGridView)(Values[1]):InvoiceGrid);;
            //    commandParameters.CheckBoxControl = (chkSelectAll==null?(CheckBox)(Values[0]):chkSelectAll);
            //}
            ////
            return commandParameters;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SelectOpenARInvoicesParameters
    {
        public bool IsHeaderCheckBox { get; set; }
        public CheckBox CheckBoxControl { get; set; }
        public RadGridView OpenInvoiceGrid { get; set; }
    }
}
