﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
    public class DateFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string orgdate="";
            if(!( value==null || value.ToString()=="" ))
            {
                DateTime dt = System.Convert.ToDateTime(value);
                orgdate = dt.ToString("MM'/'dd'/'yyyy ");
                return orgdate;
            }
            else
            {
                orgdate = "None";
            }
            return orgdate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
