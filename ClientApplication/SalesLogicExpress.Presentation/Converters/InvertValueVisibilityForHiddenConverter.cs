﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SalesLogicExpress.Presentation.Converters
{
  public   class InvertValueVisibilityForHiddenConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || value.ToString().Trim().Length == 0 || value.ToString().Trim() == "NA" || value.ToString().Trim() == "na" || value.ToString().Trim().ToLower() == "false"|| value.ToString().Trim().ToLower() == "0")
            {
                return System.Windows.Visibility.Visible;
            }
            if (value.ToString() == "")
            {
                return System.Windows.Visibility.Visible;
            }
            return System.Windows.Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
             return (bool)value;
        }
    }
}
