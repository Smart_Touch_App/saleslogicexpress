﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Presentation.Behaviors
{
    public class MyMultiSelectBehavior : Behavior<RadGridView>
    {
        private RadGridView Grid
        {
            get
            {
                return AssociatedObject as RadGridView;
            }
        }
        public INotifyCollectionChanged SelectedItems
        {
            get { return (INotifyCollectionChanged)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value);
            }
        }


        // Using a DependencyProperty as the backing store for SelectedItemsProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(INotifyCollectionChanged), typeof(MyMultiSelectBehavior), new PropertyMetadata(OnSelectedItemsPropertyChanged));


        private static void OnSelectedItemsPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs args)
        {
            var collection = args.NewValue as INotifyCollectionChanged;
            if (collection != null)
            {
                collection.CollectionChanged += ((MyMultiSelectBehavior)target).ContextSelectedItems_CollectionChanged;
            }
        }
        protected override void OnAttached()
        {
            base.OnAttached();

            Grid.SelectedItems.CollectionChanged += GridSelectedItems_CollectionChanged;
        }
        void ContextSelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UnsubscribeFromEvents();

            Transfer(SelectedItems as IList, Grid.SelectedItems);

            SubscribeToEvents();
        }
        CancellationTokenSource tokenForCancelTask = null;
        void RunAsAsync()
        {
            if (tokenForCancelTask != null)
            {
                tokenForCancelTask.Cancel();
            }
            tokenForCancelTask = new CancellationTokenSource();
            Task.Run(async () => await AsyncTask(tokenForCancelTask.Token), tokenForCancelTask.Token);
        }
        private async Task AsyncTask(CancellationToken token)
        {
            try
            {
                bool continueLoop = true;
                tokenForCancelTask.Token.Register(() =>
               {
                   continueLoop = false;
               });
                await Task.Delay(500);
                if (!continueLoop)
                {
                    return;
                }
                this.Dispatcher.BeginInvoke((Action)delegate
                {
                    UnsubscribeFromEvents();
                    Transfer(Grid.SelectedItems, SelectedItems as IList);
                    SubscribeToEvents();
                }, DispatcherPriority.Render);
            }
            catch (Exception ex)
            {
            }
        }
        void GridSelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            RunAsAsync();
            //if (!subscribedToEvents) {
            //    SearchItem();
            //}
            //System.Console.WriteLine("e.NewItems.Count : " + e.NewItems.Count);
            //UnsubscribeFromEvents();
            //Transfer(Grid.SelectedItems, SelectedItems as IList);
            //SubscribeToEvents();
        }
        bool subscribedToEvents = false;
        private void SubscribeToEvents()
        {
            Grid.SelectedItems.CollectionChanged += GridSelectedItems_CollectionChanged;
            if (SelectedItems != null)
            {
                SelectedItems.CollectionChanged += ContextSelectedItems_CollectionChanged;
            }
            subscribedToEvents = true;
        }
        private void UnsubscribeFromEvents()
        {
            Grid.SelectedItems.CollectionChanged -= GridSelectedItems_CollectionChanged;

            if (SelectedItems != null)
            {
                SelectedItems.CollectionChanged -= ContextSelectedItems_CollectionChanged;
            }
        }
        public static void Transfer(IList source, IList target)
        {
            if (source == null || target == null)
                return;
            try
            {
                target.Clear();

            }
            catch (Exception)
            {
                foreach (var item in target)
                {
                    target.Remove(item);
                }
            }

            foreach (var o in source)
            {
                target.Add(o);
            }
        }
    }

    //public class MyMultiSelectBehaviorForAddLoad : Behavior<RadGridView>
    //{
    //    private RadGridView Grid
    //    {
    //        get
    //        {
    //            return AssociatedObject as RadGridView;
    //        }
    //    }
    //    public INotifyCollectionChanged AddLoadDetailsSelectedItems
    //    {
    //        get { return (INotifyCollectionChanged)GetValue(SelectedItemsProperty); }
    //        set { SetValue(SelectedItemsProperty, value); }
    //    }

    //    // Using a DependencyProperty as the backing store for SelectedItemsProperty.  This enables animation, styling, binding, etc...
    //    public static readonly DependencyProperty SelectedItemsProperty =
    //        DependencyProperty.Register("SelectedItems", typeof(INotifyCollectionChanged), typeof(MyMultiSelectBehavior), new PropertyMetadata(OnSelectedItemsPropertyChanged));


    //    void ContextSelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    //    {
    //        UnsubscribeFromEvents();

    //        Transfer(AddLoadDetailsSelectedItems as IList, Grid.SelectedItems);

    //        SubscribeToEvents();
    //    }
    //    private static void OnSelectedItemsPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs args)
    //    {
    //        var collection = args.NewValue as INotifyCollectionChanged;
    //        if (collection != null)
    //        {
    //            collection.CollectionChanged += ((MyMultiSelectBehaviorForAddLoad)target).ContextSelectedItems_CollectionChanged;
    //        }
    //    }
    //    protected override void OnAttached()
    //    {
    //        base.OnAttached();

    //        Grid.SelectedItems.CollectionChanged += GridSelectedItems_CollectionChanged;
    //    }

    //    void GridSelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    //    {
    //        UnsubscribeFromEvents();

    //        Transfer(Grid.SelectedItems, AddLoadDetailsSelectedItems as IList);

    //        SubscribeToEvents();
    //    }
    //    private void SubscribeToEvents()
    //    {
    //        Grid.SelectedItems.CollectionChanged += GridSelectedItems_CollectionChanged;

    //        if (AddLoadDetailsSelectedItems != null)
    //        {
    //            AddLoadDetailsSelectedItems.CollectionChanged += ContextSelectedItems_CollectionChanged;
    //        }
    //    }
    //    private void UnsubscribeFromEvents()
    //    {
    //        Grid.SelectedItems.CollectionChanged -= GridSelectedItems_CollectionChanged;

    //        if (AddLoadDetailsSelectedItems != null)
    //        {
    //            AddLoadDetailsSelectedItems.CollectionChanged -= ContextSelectedItems_CollectionChanged;
    //        }
    //    }
    //    public static void Transfer(IList source, IList target)
    //    {
    //        if (source == null || target == null)
    //            return;
    //        try
    //        {
    //            target.Clear();

    //        }
    //        catch (Exception)
    //        {
    //            foreach (var item in target)
    //            {
    //                target.Remove(item);
    //            }
    //        }

    //        foreach (var o in source)
    //        {
    //            target.Add(o);
    //        }
    //    }
    //}
}
