﻿using SalesLogicExpress.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Telerik.Windows.Controls;
using SalesLogicExpress.Presentation.Helpers;
namespace SalesLogicExpress.Presentation.Controls
{
    public class CustomRadListBox : RadListBox
    {
        static FrameworkPropertyMetadata propertymetadata = new FrameworkPropertyMetadata(false,
 FrameworkPropertyMetadataOptions.BindsTwoWayByDefault |
FrameworkPropertyMetadataOptions.Journal, new
PropertyChangedCallback(MyCustom_PropertyChanged),
new CoerceValueCallback(MyCustom_CoerceValue),
false, UpdateSourceTrigger.PropertyChanged);
        public static readonly DependencyProperty IsBusyProperty =
              DependencyProperty.Register("IsBusy", typeof(bool), typeof(CustomRadListBox), propertymetadata);
        private static void MyCustom_PropertyChanged(DependencyObject dobj,
DependencyPropertyChangedEventArgs e)
        {
            //if (windowOverlay != null)
            //{
            //    windowOverlay.Visibility = Convert.ToBoolean(e.NewValue) ? Visibility.Visible : Visibility.Hidden;
            //}
            //To be called whenever the DP is changed.
            //   MessageBox.Show(string.Format("Property changed is fired : OldValue {0} NewValue : {1}", e.OldValue, e.NewValue));
        }

        private static object MyCustom_CoerceValue(DependencyObject dobj, object Value)
        {
            //called whenever dependency property value is reevaluated. The return value is the
            //latest value set to the dependency property
            // MessageBox.Show(string.Format("CoerceValue is fired : Value {0}", Value));
            return Value;
        }
        private static bool IsBusyChanged(object value)
        {

            return Convert.ToBoolean(value);
        }
        public bool IsBusy
        {
            get { return (bool)GetValue(IsBusyProperty); }
            set { SetValue(IsBusyProperty, value); }
        }
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation;
        static OverlayAdorner windowOverlay;
        public event EventHandler DoubleTouchDown;
        public CustomRadListBox()
        {

            this.PreviewTouchDown += CustomRadListBox_PreviewTouchDown;
            Telerik.Windows.DragDrop.DragDropManager.AddPreviewGiveFeedbackHandler(this, PreviewGiveFeedbackHandler);
            ////TODO: CODE FOR ADDING BUSY INDICATOR
            //IEnumerable<Grid> grid = this.FindVisualChildren<Grid>();
            //if (grid.Count() == 0) return;
            //UIElement mainPane = grid.ElementAt(0);
            //windowOverlay = new OverlayAdorner(mainPane);
            //windowOverlay.Name = "OverLayLoader";
            //windowOverlay.FontSize = 15;
            //windowOverlay.OverlayedText = "";
            //windowOverlay.Opacity = 0.8;
            //windowOverlay.Visibility = System.Windows.Visibility.Collapsed;
            //windowOverlay.Typeface = new Typeface(FontFamily, FontStyles.Italic,
            //    FontWeights.Bold, FontStretch);
            //System.Windows.Documents.AdornerLayer.GetAdornerLayer(mainPane).Add(windowOverlay);

        }
        private void PreviewGiveFeedbackHandler(object sender, Telerik.Windows.DragDrop.GiveFeedbackEventArgs e)
        {
            System.Drawing.Point mouseposition = System.Windows.Forms.Cursor.Position;
            Point p = this.GetScrollViewerPart().PointFromScreen(new Point(mouseposition.X, mouseposition.Y));
            if (p.Y < 10)
                this.GetScrollViewerPart().ScrollToVerticalOffset(this.GetScrollViewerPart().VerticalOffset - 1);
            if (p.Y > this.RenderSize.Height - 10)
                this.GetScrollViewerPart().ScrollToVerticalOffset(this.GetScrollViewerPart().VerticalOffset + 1);
        }
        protected virtual void OnDoubleTouchDown()
        {
            if (DoubleTouchDown != null)
                DoubleTouchDown(this, EventArgs.Empty);
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            double dis = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation);
            bool tapsAreCloseInDistance = dis < 20;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(.7));

            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        void CustomRadListBox_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            if (IsDoubleTap(e))
            {
                OnDoubleTouchDown();
            }
        }
    }

    public class CustomerRadExpander : RadExpander
    {
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation = new Point();

        public event EventHandler DoubleTouchDown;
        public CustomerRadExpander()
        {
            this.PreviewTouchDown += CustomerRadExpander_PreviewTouchDown;
        }
        protected virtual void OnDoubleTouchDown()
        {
            if (DoubleTouchDown != null)
                DoubleTouchDown(this, EventArgs.Empty);
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            double dis = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation);
            bool tapsAreCloseInDistance = dis < 20;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(.7));

            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        void CustomerRadExpander_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            if (IsDoubleTap(e))
            {
                OnDoubleTouchDown();
            }
        }
    }

    public class CustomRadioButton : RadRadioButton
    {
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation;

        public event EventHandler DoubleTouchDown;
        public CustomRadioButton()
        {
            this.PreviewTouchDown += CustomRadioButton_PreviewTouchDown;
        }
        protected virtual void OnDoubleTouchDown()
        {
            if (DoubleTouchDown != null)
                DoubleTouchDown(this, EventArgs.Empty);
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            double dis = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation);
            bool tapsAreCloseInDistance = dis < 20;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(.7));

            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        void CustomRadioButton_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            if (IsDoubleTap(e))
            {
                OnDoubleTouchDown();
            }
        }
    }

    public class CustomRadGridView : RadGridView
    {
        private readonly Stopwatch _doubleTapStopwatch = new Stopwatch();
        private Point _lastTapLocation = new Point();

        public event EventHandler DoubleTouchDown;
        public CustomRadGridView()
        {
            this.PreviewTouchDown += CustomRadGridView_PreviewTouchDown;
        }
        protected virtual void OnDoubleTouchDown()
        {
            if (DoubleTouchDown != null)
                DoubleTouchDown(this, EventArgs.Empty);
        }
        public static double GetDistanceBetweenPoints(Point p, Point q)
        {
            double a = p.X - q.X;
            double b = p.Y - q.Y;
            double distance = Math.Sqrt(a * a + b * b);
            return distance;
        }
        private bool IsDoubleTap(TouchEventArgs e)
        {
            Point currentTapPosition = e.GetTouchPoint(this).Position;
            double dis = GetDistanceBetweenPoints(currentTapPosition, _lastTapLocation);
            bool tapsAreCloseInDistance = dis < 20;
            _lastTapLocation = currentTapPosition;
            TimeSpan elapsed = _doubleTapStopwatch.Elapsed;
            _doubleTapStopwatch.Restart();
            bool tapsAreCloseInTime = (elapsed != TimeSpan.Zero && elapsed < TimeSpan.FromSeconds(.7));

            return tapsAreCloseInDistance && tapsAreCloseInTime;
        }
        void CustomRadGridView_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            if (IsDoubleTap(e))
            {
                OnDoubleTouchDown();
            }
        }
    }
}
