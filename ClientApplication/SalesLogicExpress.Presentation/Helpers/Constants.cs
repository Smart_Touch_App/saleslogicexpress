﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Helpers
{
    public class Constants
    {
        public class CustomerHomeTabs
        {
            public const string DashboardTab = "DashboardTab";
            public const string ActivityTab = "ActivityTab";
            public const string NotesTab = "NotesTab";
            public const string InformationTab = "InformationTab";
            public const string ContactsTab = "ContactsTab";
            public const string PricingTab = "PricingTab";
            public const string QuotesTab = "QuotesTab";
            public const string ItemRestrictionsTab = "ItemRestrictionsTab";
        }
        public class RouteSettlementTabs
        {
            public const string TransactionTab = "DashboardTab";
            public const string SettlementTab = "ActivityTab";
        }
        // Life cycles
        public const string OrderLifeCycle = "OrderLifeCycle";
        public const string ApplicationLifeCycle = "ApplicationLifeCycle";

        // Views in application
        public const string Title_OrderTemplateViewTitle = "Order Template View";
        public const string Title_CreateOrderViewTitle = "Create Order View";
        public const string Title_PreviewOrderViewTitle = "Preview Order View";
        public const string Title_PickOrderViewTitle = "Pick Order View";
        public const string Title_PreOrderViewTitle = "PreOrder View";
        public const string Title_CustomerHomeViewTitle = "Customer Home View";
        public const string Title_DeliverOrderViewTitle = "Deliver Order View";
        public const string Title_PreTripInspectionViewTitle = "Pre-TripInspection View";
        public const string Title_RouteHomeViewTitle = "Route Home View";
        public const string Title_ServiceRouteViewTitle = "Service Route View";
        public const string Title_UserLoginViewTitle = "User Login View";
        public const string Title_StopSequencingViewTitle = "Stop Sequencing View";
        public const string Title_AddLoadViewTitle = "Add Load View";
        public const string Title_CustomerQuoteTitle = "Customer Quote View";
        public const string Title_ProspectQuoteTitle = "Prospect Quote View";
        public const string Title_ProspectsHomeViewTitle = "Prospects Home View";


        // EVENT CONSTANTS
        /*****************************************************************************************************************/
        // Route Home View
        public const string RH_ViewPreTripInspection = "View Pre-Trip Inspection";
        public const string RH_ViewServiceRoute = "View Service Route";
        public const string RH_ViewEODSettlement = "View EOD Settlement";
        public const string RH_ViewAgingSummary = "View Aging Summary";
        public const string RH_ViewItemInventory = "View Item Inventory";
        public const string RH_ViewItemReplenishment = "View Item Replenishment";
        public const string RH_ViewCycleCount = "View Cycle Count";

        // Pre Trip Inspection View
        public const string PT_SavePreTripInspection = "Save Pre-Trip Inspection";

        // EOD Settlement View
        public const string EOD_SaveEODSettlement = "Save EOD Settlement";


        // Service Route View
        public const string SRV_ViewDailyStop = "View DailyStops";
        public const string SRV_ViewCustomers = "View Customers";
        public const string SRV_ViewProspects = "View Prospects";
        public const string SRV_ViewStopSequencing = "View StopSequencing";
        public const string SRV_ViewCustomerDashboard = "View Customer Dashboard";
        public const string SRV_SearchInServiceRoute = "Search In Service Route";
        public const string SRV_ViewCreateStopDialog = "Create Stop";
        public const string SRV_SaveCreateStop = "Save New Stop";
        public const string SRV_ViewMoveStopDialog = "Move Stop";
        public const string SRV_SaveMoveStop = "Save Moved Stop";
        public const string SRV_ViewAddProspectDialog = "View Add Prospect Dialog";


        //Customer Dashboard View
        public const string CD_ViewCustomerDashboard = "ViewCustomerDashboard";
        public const string CD_ViewCustomerActivity = "View Customer Activity";
        public const string CD_ViewCustomerNotes = "View Customer Notes";
        public const string CD_ViewCustomerInfo = "View Customer Info";
        public const string CD_ViewCustomerSalesOrder = "View Customer Info";
        public const string CD_ViewReturnsAndCredit = "View Customer Info";
        public const string CD_ViewPaymentAndAR = "ViewPaymentAndAR";
        public const string CD_ViewCustomerContacts = "View Customer Contacts";
        public const string CD_ViewCustomerPricing = "View Customer Pricing";
        public const string CD_ViewCustomerQuotes = "View Customer Quotes";
        public const string CD_ViewCustomerItemRestrictions = "View Customer Item-Restrictions";
        public const string CD_MarkNoActivity = "Mark No-Activity";
        public const string CD_ViewCustomerOrderHistory = "ViewCustomerOrderHistory";

        // Pre Order View
        public const string PO_ViewPreOrder = "View Pre-Order";
        public const string PO_DeleteFromPreOrder = "Delete From Pre-Order";
        public const string PO_SavePreOrder = "Save Pre-Order";
        public const string PO_PreOrderLoadBuiltupQty = "Pre-Order Load Builtup Qty";
        public const string PO_ResetPreOrderQty = "Reset Pre-Order Qty";


        // Order Template View
        public const string OT_ViewOrderTemplate = "View Order Template";
        public const string OT_SaveTemplate = "Save Template";
        public const string OT_AddFromSearchInTemplate = "AddFromSearchInTemplate";
        public const string OT_SearchInTemplate = "SearchInTemplate";
        public const string OT_PlaceOrder = "PlaceOrder";
        public const string OT_PrelaodWithBuiltUpQty = "PrelaodWithBuiltUpQty";
        public const string OT_ResetTemplateQty = "ResetTemplateQty";

        // Create Order
        public const string CO_HoldOrder = "HoldOrder";
        public const string CO_VoidOrder = "VoidOrder";

        // Create Order
        public const string CO_AddFromSearchInOrder = "AddFromSearchInOrder";
        public const string CO_SearchInTemplate = "SearchInTemplate";
        public const string CO_PreviewOrder = "PreviewOrder";
        public const string CO_DeleteItemFromOrder = "DeleteItemFromOrder";

        // Preview Order
        public const string PRO_CompleteAndSign = "CompleteAndSign";
        public const string PRO_AcceptSign = "AcceptSign";

        // Pick Screen
        public const string PSC_HoldOrder = "HoldOrder";
        public const string PSC_ManualPick = "ManualPick";
        public const string PSC_PrintPickSlip = "PrintPickSlip";
        public const string PSC_RemoveExceptions = "RemoveExceptions";
        public const string PSC_PrintInvoice = "PrintInvoice";
        public const string PSC_DeliverToCustomer = "DeliverToCustomer";

        // Deliver Screen
        public const string DO_PrintInvoice = "PrintInvoice";
        public const string DO_EmailInvoice = "EmailInvoice";
        public const string DO_HoldOrder = "HoldOrder";
        public const string DO_VoidOrder = "VoidOrder";
        public const string DO_AcceptOrder = "AcceptOrder";
        public const string DO_DeliverOrder = "DeliverOrder";


        public const string App_MaximizeScreen = "MaximizeScreen";
        public const string App_MinimizeScreen = "MinimizeScreen";
        public const string App_FullScreen = "FullScreen";
        public const string App_Close = "AppClose";
        public const string App_ViewActiveSyncDetail = "ViewActiveSyncDetail";
        public const string App_ViewSyncDetail = "ViewSyncDetail";

        public const string AppExit = "AppExit ";
        public const string UserLogin = "UserLogin";
        public const string UserLogout = "UserLogout";
        public const string PasswordIncorrect = "PasswordIncorrect ";
        public const string PasswordChange = "PasswordChange";
        public const string ViewInventory = "ViewInventory";
        public const string ViewReplenishment = "ViewReplenishment";


        //Prospect Home View
        public const string PD_ViewDashboard = "View Prospect Dashboard";
        public const string PD_ViewNotes = "View Prospect Notes";
        public const string PD_ViewInfo = "View Prospect Info";
        public const string PD_ViewContacts = "View Prospect Contacts";
        public const string PD_ViewPricing = "View Prospect Pricing";
        public const string PD_ViewQuotes = "View Prospect Quotes";
        public const string PD_ViewItemRestrictions = "View Prospect Competitor Information";
        public static readonly Dictionary<string, List<string>> BlackListedControls = new Dictionary<string, List<string>>() 
            //Black listed control list 
        {
            {"SalesLogicExpress.Views.ARPayment", new List<string>() 
                                            { "ReceiptHistoryEditButton", 
                                               "txt_PaymentAmt",
                                               "ReceiptHistoryPrintButton",
                                               "ReceiptHistoryVoidButton", 
                                               "chkCheckNo"}}, 
            {"SalesLogicExpress.Views.ReturnsAndCredit", new List<string>() 
                                            { "PrintOrderButton", 
                                               "ReturnOrderButton",
                                               "ReturnItemsButton"}}
        };
    }
}
