﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace SalesLogicExpress.Helpers
{
    // TextBoxHelper Contains Properties for TextBox
    public class TextBoxHelper : TextBox {
        public static bool GetIsNumeric(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsNumericProperty);
        }

        public static void SetIsNumeric(DependencyObject obj, bool value)
        {
            obj.SetValue(IsNumericProperty, value);
        }

        #region isnumeric property
        public static readonly DependencyProperty IsNumericProperty = DependencyProperty.RegisterAttached(
                                                                        "IsNumeric", typeof(bool), typeof(TextBoxHelper), new PropertyMetadata
                                                                                (false, new PropertyChangedCallback((ob, _event) =>
                                                                                {
                                                                                    TextBox targetTextbox = ob as TextBox;
                                                                                    if (targetTextbox != null)
                                                                                    {
                                                                                        if ((bool)_event.OldValue && !((bool)_event.NewValue))
                                                                                        {
                                                                                            targetTextbox.PreviewTextInput -= targetTextbox_PreviewTextInput;
                                                                                            targetTextbox.TextChanged -= targetTextbox_TextChanged;

                                                                                        }
                                                                                        if ((bool)_event.NewValue)
                                                                                        {
                                                                                            targetTextbox.PreviewTextInput += targetTextbox_PreviewTextInput;
                                                                                            targetTextbox.PreviewKeyDown += targetTextbox_PreviewKeyDown;
                                                                                            targetTextbox.TextChanged += targetTextbox_TextChanged;
                                                                                             }
                                                                                    }
                                                                                })));

        static void targetTextbox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

       

        private static void targetTextbox_PreviewKeyDown(object sender, KeyEventArgs e)
       {
            e.Handled = e.Key == Key.Space;
        }

        private static void targetTextbox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !(new Regex(@"^\d{0,3}(\.\d{0,2})?$").IsMatch(e.Text));
        }
        #endregion

         }


    // TextBoxHelper Contains Properties for ComboBox
     public class CombBoxHelper : ComboBox
     {
         public static readonly DependencyProperty SendSelectedProperty = DependencyProperty.RegisterAttached("DisableCombo", typeof(bool), typeof(CombBoxHelper),
                                                                                                                new PropertyMetadata(false,
                                                                                                                    new PropertyChangedCallback((ob, _event) =>
                                                                                                                    {
                                                                                                                        ComboBox cmb = ob as ComboBox;
                                                                                                                            cmb.IsEnabled = true;
                                                                                                                    })));

         public bool DisableCombo 
         {
             get { return (bool)GetValue(SendSelectedProperty); }
             set { SetValue(SendSelectedProperty, value);  }
         }
     }

   
 }



    


