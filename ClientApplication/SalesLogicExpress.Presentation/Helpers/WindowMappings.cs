﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Application.Helpers;
namespace SalesLogicExpress.Presentation.Helpers
{
    public class WindowMappings
    {
        static Dictionary<ViewModelMappings.View, WindowMap> windowMappings = new Dictionary<ViewModelMappings.View, WindowMap>();
        static Dictionary<string, string> TabViewMappings = new Dictionary<string, string>();
        static Dictionary<string, string> resourceMappings = new Dictionary<string, string>();
        public static System.Windows.Window currentWindow, nextWindow;
        private static void LoadMappings()
        {
            #region resourceMappings
            if (resourceMappings.Count == 0)
            {
                resourceMappings.Add("VerifyPickOrders", "ModalDialogForTemplate");
                resourceMappings.Add("ManualPickReason", "ModelDialogForManualPick");
                resourceMappings.Add("AllOpenOrdersForItem", "ModelDialogForOpenOrderList");
                resourceMappings.Add("VoidOrderReason", "ModelDialogForVoidOrderReason");
                resourceMappings.Add("AddNewContact", "ModalDialogForAddNewContact");
                resourceMappings.Add("CashDelivery", "ModelDialogForCashDelivery");
                resourceMappings.Add("ChargeOnAccount", "ModelDialogForChargeOnAccount");
                resourceMappings.Add("EditExistingContact", "ModalDialogForEditContact");
                resourceMappings.Add("EditExistingPhone", "ModalDialogForEditPhone");
                resourceMappings.Add("EditExistingEmail", "ModalDialogForEditEmail");
                resourceMappings.Add("AddNewPhone", "ModalDialogForAddPhone");
                resourceMappings.Add("AddNewEmail", "ModalDialogForAddEmail");
                resourceMappings.Add("OpenPreOrderDialog", "PreOrderDialog");
                resourceMappings.Add("AddNewNote", "ModalDialogForAddNewNote");
                resourceMappings.Add("AddNewProspect", "ModalDialogNewProspect");
                resourceMappings.Add("MoveStopDialog", "ModalDialogMoveStop");
                resourceMappings.Add("CreateStopDialog", "ModalDialogCreateStop");

                resourceMappings.Add("OpenAdjustmentDialog", "AdjustmentDialogTemplate");
                resourceMappings.Add("OpenParLevelDialog", "ParLevelDialogPopup");
                resourceMappings.Add("AdjustmentVerification", "AdjustmentVerificationPopup");

                resourceMappings.Add("EditReceiptDialog", "ModalDialogForEditReceipt");
                resourceMappings.Add("VoidReceiptDialog", "ModalDialogForVoidReceipt");
                resourceMappings.Add("VoidReceiptVerificationPopup", "VoidReceiptVerificationPopup");
                resourceMappings.Add("ConfirmCreateDialog", "ModalDialogForCreateStopOfFuture");
                resourceMappings.Add("ConfirmMoveorCreateDialog", "ModalDialogForCreateMoveStop");

                //************************************************************************************************
                // Comment: Added to remind the user to create a unplanned stop for today
                // Created: feb 03, 2016
                // Author: Vivensas (Rajesh,Yuvaraj)
                // Revisions: 
                //*************************************************************************************************
                resourceMappings.Add("ConfirmVoidandCreateDialog", "ModalDialogForVoidCreateStop");

                //*************************************************************************************************
                // Vivensas changes ends over here
                //**************************************************************************************************

                resourceMappings.Add("ConfirmMoveorCreateDialogForPast", "ModalDialogForCreateMoveStopPastStops");
                resourceMappings.Add("OpenSyncPopup", "SyncPopupDialog");
                resourceMappings.Add("OutOfBalanceApproval", "ModalDialogForOutOfBalance");
                resourceMappings.Add("SelectVerificationUser", "ModalDialogForSelectVerificationUser");
                resourceMappings.Add("SelectRejectionReason", "ModalDialogForRejectionReason");
                resourceMappings.Add("AcceptAndConfirm", "ModalDialogForConfirmInspection");
                resourceMappings.Add("VoidSettlement", "ModelDialogForVoidSettlement");
                resourceMappings.Add("PrintInspectionPrintSelection", "ModelDialogForPrintInspectionPrintSelection");
                resourceMappings.Add("RemoveExceptionInAddLoadPick", "ModelDialogForRemoveExceptionInAddLoadPick");
                resourceMappings.Add("DeleteSuggestion", "ModelDialogForDeleteSuggestion");
                resourceMappings.Add("ContinuePickSuggestion", "ModelDialogForContinuePickSuggestion");
                resourceMappings.Add("SaveSuggestion", "ModelDialogForSaveSuggestion");
                resourceMappings.Add("SaveSuggestionOnBackNaigation", "ModelDialogForSaveSuggestionOnBackNaigation");
                //dialogs for Cycle Count
                resourceMappings.Add("VoidCycleCount", "ModalDialogForVoidCycleCount");
                resourceMappings.Add("ReasonCodeDialog", "ModalDialogForReasonCode");
                resourceMappings.Add("SuggestionDialog", "ModelDialogForSuggestion");
                resourceMappings.Add("AddLoadDialog", "ModelDialogForAddLoad");

                resourceMappings.Add("SaveAddLoad", "ModelDialogForSaveAddLoad");
                resourceMappings.Add("ContinuePickAddLoad", "ModelDialogForContinuePickAddLoad");
                resourceMappings.Add("DeleteAddLoad", "ModelDialogForDeleteAddLoad");
                resourceMappings.Add("SaveAddLoadOnBackNaigation", "ModelDialogForSaveAddLoadOnBackNaigation");
                resourceMappings.Add("ConfirmationPickAddLoad", "ModelDialogForConfirmationPickAddLoad");
                resourceMappings.Add("RemoveExceptionInAddLoadPicks", "ModelDialogForRemoveExceptionInAddLoadPicks");
                resourceMappings.Add("VoidSettlementOnTransactionTab", "ModelDialogForVoidSettlementOnTransactionTab");
                resourceMappings.Add("VoidReplenishment", "ModelDialogForVoidReplenishment");
                resourceMappings.Add("ShortPickItem", "ModelDialogForShortPickItem");
                resourceMappings.Add("HoldPickItemAddLoad", "ModelDialogForHoldPickItemAddLoad");
                resourceMappings.Add("PrintReplenishment", "ModelDialogForPrintReplenishment");
                resourceMappings.Add("ExtraItemPickedTemplate", "ModalDialogForExtraPickedItem");
                resourceMappings.Add("TwoLineAlert", "ModelDialogForTwoLineAlert");
                resourceMappings.Add("ReleaseReplenishment", "ModelDialogForReleaseReplenishment");
                //
                resourceMappings.Add("OpenNotificationDialog", "NotificationDialog");
                resourceMappings.Add("StatusInforDialog", "ModalDialogForStatusInfo");
                #region Credit Process
                resourceMappings.Add("AddCreditDialog", "ModalDialogForAddCreditMemo");
                resourceMappings.Add("SignAndPrint", "ModalDialogForSignAndPrint");
                resourceMappings.Add("VoidCreditProcess", "ModalDialogForVoidCreditMemo");
                #endregion
                resourceMappings.Add("RegenerateSugg", "ModelDialogForRegenerateSugg");
                resourceMappings.Add("AppendReturnItems", "ModalDialogForAddExtraReturnItems");
                resourceMappings.Add("ReasonCodeListDialog", "ModalDialogForReasonCodeList");
                resourceMappings.Add("HoldVoidRODialog", "ModalDialogForHoldVoidRO");

                resourceMappings.Add("AddSerialised", "ModalDialogNewProspect");
                resourceMappings.Add("AddCoffee", "ModalDialogNewCoffee");
                resourceMappings.Add("AddExpensed","ModalDialogNewExpensed");
                resourceMappings.Add("AddAllied", "ModalDialogNewAllied");         
 
                resourceMappings.Add("AddExpenseDialog", "ModalDialogForAddNewExpense");
                resourceMappings.Add("VoidExpenseDialog", "ModalDialogForVoidExpenses");
                resourceMappings.Add("EditExpenseDialog", "ModalDialogForEditExpense");

                resourceMappings.Add("QuotePriceDialog", "QuotePriceSetupDialog");

                resourceMappings.Add("AddMoneyOrder", "ModalDialogForAddMoneyOrder");
                resourceMappings.Add("EditMoneyOrder", "ModalDialogForEditMoneyOrder");
                resourceMappings.Add("VoidMoneyOrder", "ModalDialogForVoidMoneyOrder");
                resourceMappings.Add("AddProspectContact", "ModalDialogForAddProspectContact");
                resourceMappings.Add("AddProspectPhoneContact", "ModalDialogForAddProspectPhone");
                resourceMappings.Add("AddProspectEmailContact", "ModalDialogForAddProspectEmail");
                resourceMappings.Add("EditExistingProspectPhone", "ModalDialogForEditProspectPhone");
                resourceMappings.Add("EditExistingProspectEmail", "ModalDialogForEditProspectEmail");
                //
                resourceMappings.Add("AddNewNoteProspect", "ModalDialogForAddNewNoteProspect");

                resourceMappings.Add("StopDialogForProspect", "ModalDialogForCreateMoveStopForProspect");
            }
            #endregion

            #region windowMappings

            if (windowMappings.Count == 0)
            {
                windowMappings.Add(ViewModelMappings.View.OrderTemplate, new WindowMap(typeof(Views.OrderTemplate), null, null));
                windowMappings.Add(ViewModelMappings.View.ServiceRoute, new WindowMap(typeof(Views.ServiceRoute), null, null));
                windowMappings.Add(ViewModelMappings.View.Order, new WindowMap(typeof(Views.Order), null, null));
                windowMappings.Add(ViewModelMappings.View.ConfirmAndSign, new WindowMap(typeof(Views.ConfirmAndSignOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.PreviewOrder, new WindowMap(typeof(Views.PreviewOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.PickOrder, new WindowMap(typeof(Views.PickOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.UserLogin, new WindowMap(typeof(Views.UserLogin), null, null));
                windowMappings.Add(ViewModelMappings.View.CustomerHome, new WindowMap(typeof(Views.CustomerHome), null, null));
                windowMappings.Add(ViewModelMappings.View.DeliveryScreen, new WindowMap(typeof(Views.DeliveryScreen), null, null));
                windowMappings.Add(ViewModelMappings.View.RouteHome, new WindowMap(typeof(Views.RouteHome), null, null));
                windowMappings.Add(ViewModelMappings.View.PreOrder, new WindowMap(typeof(Views.PreOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.CustomerStopSequencingView, new WindowMap(typeof(Views.CustomerStopSequencingView), null, null));
                windowMappings.Add(ViewModelMappings.View.ARPayment, new WindowMap(typeof(Views.ARPayment), null, null));
                windowMappings.Add(ViewModelMappings.View.PrintPickOrder, new WindowMap(typeof(Views.PrintPickOrder), null, null));
                windowMappings.Add(ViewModelMappings.View.PrintInvoice, new WindowMap(typeof(Views.PrintInvoice), null, null));
                //windowMappings.Add(ViewModelMappings.View.PreTripVehicleInspection, new WindowMap(typeof(Views.PreTripVehicleInspection), null, null));
                windowMappings.Add(ViewModelMappings.View.RouteSettlement, new WindowMap(typeof(Views.RouteSettlement), null, null));
                windowMappings.Add(ViewModelMappings.View.SettlementConfirmation, new WindowMap(typeof(Views.SettlementConfirmation), null, null));
                windowMappings.Add(ViewModelMappings.View.SettlementVerification, new WindowMap(typeof(Views.SettlementVerification), null, null));
                windowMappings.Add(ViewModelMappings.View.PreTripVehicleInspection, new WindowMap(typeof(Views.PreTripVehicleInspection), null, null));
                windowMappings.Add(ViewModelMappings.View.ReplenishAddLoadPickView, new WindowMap(typeof(Views.ReplenishAddLoadPickView), null, null));
                windowMappings.Add(ViewModelMappings.View.ReplenishAddLoadView, new WindowMap(typeof(Views.ReplenishAddLoadView), null, null));
                windowMappings.Add(ViewModelMappings.View.ReplenishSggestionView, new WindowMap(typeof(Views.ReplenishSggestionView), null, null));
                windowMappings.Add(ViewModelMappings.View.ReplenishSuggestionPickView, new WindowMap(typeof(Views.ReplenishSuggestionPickView), null, null));
                windowMappings.Add(ViewModelMappings.View.CycleCount, new WindowMap(typeof(Views.CycleCount), null, null));
                windowMappings.Add(ViewModelMappings.View.ReturnsAndCredits, new WindowMap(typeof(Views.ReturnsAndCredit), null, null));
                windowMappings.Add(ViewModelMappings.View.ItemReturnsScreen, new WindowMap(typeof(Views.ItemReturnsScreen), null, null));
                windowMappings.Add(ViewModelMappings.View.ReturnOrderPickScreen, new WindowMap(typeof(Views.ReturnOrderPickScreen), null, null));
                windowMappings.Add(ViewModelMappings.View.AcceptAndPrintReturns, new WindowMap(typeof(Views.AcceptAndPrintReturns), null, null));
                windowMappings.Add(ViewModelMappings.View.ExpensesWindow, new WindowMap(typeof(Views.ExpensesWindow), null, null));
                windowMappings.Add(ViewModelMappings.View.CustomerQuote, new WindowMap(typeof(Views.CustomerQuote), null, null));
                windowMappings.Add(ViewModelMappings.View.ProspectHome, new WindowMap(typeof(Views.ProspectHome), null, null));
                windowMappings.Add(ViewModelMappings.View.MoneyOrderWindow, new WindowMap(typeof(Views.MoneyOrderWindow), null, null));
                windowMappings.Add(ViewModelMappings.View.ProspectQuotePick, new WindowMap(typeof(Views.QuotePickView), null, null));
                windowMappings.Add(ViewModelMappings.View.ProspectQuote, new WindowMap(typeof(Views.CustomerQuote), null, null));
                windowMappings.Add(ViewModelMappings.View.RouteSettlementReport, new WindowMap(typeof(Views.PrintRouteSettlement), null, null));

            }
            #endregion

            #region TabViewMappings
            // naming convention for controls is controltype_ModuleName_Tab
            if (TabViewMappings.Count == 0)
            {
                TabViewMappings.Add(ViewModelMappings.TabView.CustomerDashboard.Activity, "TabItem_CustomerHome_Activity");
                TabViewMappings.Add(ViewModelMappings.TabView.CustomerDashboard.Dashboard, "TabItemCustomerDashboard");
                TabViewMappings.Add(ViewModelMappings.TabView.CustomerDashboard.Quotes, "QuotesCustomerTab");

                TabViewMappings.Add(ViewModelMappings.TabView.RouteSettlement.Settlement, "TabItem_RouteSettlement_Settlement");//TabItemReplenishment
                TabViewMappings.Add(ViewModelMappings.TabView.RouteSettlement.Transactions, "TabItem_RouteSettlement_Transactions");//TabItemReplenishment 

                TabViewMappings.Add(ViewModelMappings.TabView.RouteHome.Replenishment, "TabItemReplenishment");
                TabViewMappings.Add(ViewModelMappings.TabView.RouteHome.Dashboard, "TabItemDashboard");
                TabViewMappings.Add(ViewModelMappings.TabView.RouteHome.CycleCount, "TabItemCycleCount");
                TabViewMappings.Add(ViewModelMappings.TabView.RouteHome.ARAging, "TabItemAgingSummary");

                TabViewMappings.Add(ViewModelMappings.TabView.ReturnsAndCredits.CreditProcess, "CreditsTabItem");
                TabViewMappings.Add(ViewModelMappings.TabView.ReturnsAndCredits.OrdersProcess, "OrderTabItem");
                TabViewMappings.Add(ViewModelMappings.TabView.ReturnsAndCredits.ReturnsProcess, "ReturnsTabItem");

                TabViewMappings.Add(ViewModelMappings.TabView.ProspectHome.Dashboard, "DashboardProspectTab");
                TabViewMappings.Add(ViewModelMappings.TabView.ProspectHome.Quotes, "QuotesProspectTab");
                TabViewMappings.Add(ViewModelMappings.TabView.ProspectHome.Activity, "ProspectActivityTab");
                TabViewMappings.Add(ViewModelMappings.TabView.Prospects.Coffee, "CoffeeProspects");

                TabViewMappings.Add(ViewModelMappings.TabView.Prospects.Serialized, "SerializedProspects");  

                TabViewMappings.Add(ViewModelMappings.TabView.ServiceRoute.DailyStop, "TabItemDailyStops");
                TabViewMappings.Add(ViewModelMappings.TabView.ServiceRoute.ProspectTab, "TabItemProspects");
                TabViewMappings.Add(ViewModelMappings.TabView.ServiceRoute.Customers, "TabItemCustomers");


            }
            #endregion
        }
        public static string GetResourceName(string resourceKey)
        {
            LoadMappings();
            return resourceMappings[resourceKey];
        }
        public static Type GetWindow(ViewModelMappings.View viewName)
        {
            LoadMappings();
            return windowMappings[viewName].window;
        }
        public static string GetTabViewName(string tabView)
        {
            LoadMappings();
            if (string.IsNullOrEmpty(tabView))
            {
                return null;
            }
            return TabViewMappings.ContainsKey(tabView) ? TabViewMappings[tabView] : string.Empty;
        }
        /// <summary>
        /// This method makes sure that we have only one instance per window open/alive when users 
        /// navigates between different screens.
        /// 
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="action"></param>
        /// <returns>returns Window object which is reffered by the viewName parameters</returns>
        public static object WindowInstance(ViewModelMappings.View viewName, NavigateToView action)
        {
            LoadMappings();
            System.Windows.Window windowInstance;
            Type window = Helpers.WindowMappings.GetWindow(action.NextViewName);
            if (windowMappings[viewName].windowInstance != null)
            {
                return windowMappings[viewName].windowInstance;
            }
            else
            {
                if (action.Payload != null)
                {
                    windowInstance = (System.Windows.Window)Activator.CreateInstance(window, action.Payload);
                }
                else
                {
                    windowInstance = (System.Windows.Window)Activator.CreateInstance(window);
                }
                windowMappings[viewName].windowInstance = windowInstance;
                return windowInstance;
            }
        }
        public static object WindowInstance(ViewModelMappings.View viewName)
        {
            if (windowMappings[viewName].windowInstance != null)
            {
                return windowMappings[viewName].windowInstance;
            }
            return null;
        }
        internal static void DisposeWindowInstance(ViewModelMappings.View view)
        {
            if (windowMappings[view].windowInstance != null)
            {
                windowMappings[view].windowInstance = null; ;
            }
        }
    }
    internal class WindowMap
    {
        public Type window;
        public object payload;
        public object windowInstance;
        public WindowMap(Type window, object payload, object windowInstance)
        {
            this.window = window;
            this.payload = payload;
            this.windowInstance = windowInstance;
        }
    }
}
