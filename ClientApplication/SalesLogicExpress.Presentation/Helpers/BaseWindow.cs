﻿using GalaSoft.MvvmLight.Messaging;
using log4net;
using log4net.Repository.Hierarchy;
using SalesLogicExpress.Application.Helpers;
using SalesLogicExpress.Application.ViewModelPayload;
using SalesLogicExpress.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Presentation.Helpers
{
    public enum WindowOrientation
    {
        Landscape,
        Portrait
    }
    public class BaseWindow : System.Windows.Window
    {
        #region Variables
        // isRegistered Flag is usedto log events occuring within the window and is static ,
        // as the eventhandlers are attached to the Window class and are
        // to be attached only once.
        static bool isRegistered = false;
        TimeSpan sp = new TimeSpan(0, 0, 0, 0, 1000);
        public WindowOrientation WindowOrientation;
        public readonly ILog Logger = LogManager.GetLogger("SalesLogicExpress.Presentation.Helpers");
        public readonly Guid Token = Guid.NewGuid();
        Control keyboard = null;
        public Control DefaultFocusElement = null;
        OverlayAdorner windowOverlay = null;
        List<ViewModelMappings.View> _AssociatedWindow = new List<ViewModelMappings.View>();
        public RadBusyIndicator windowBusyIndicator { get; set; }
        public List<ViewModelMappings.View> AssociatedWindows
        {
            get
            {
                return _AssociatedWindow;
            }
            set
            {
                _AssociatedWindow = value;
            }
        }
        public string ScreenID { get; set; }
        public string ScreenLifeCycle { get; set; }
        #endregion

        #region Constructor
        public BaseWindow()
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Start:Constructor][WindowTitle=" + this.Title + "]");
            // Subscribe to messages for displaying UI dialogs and navigation changes
            Messenger.Default.Register<AlertWindow>(this, Token, (action) => AlertWindowExecute(action));
            Messenger.Default.Register<ConfirmWindow>(this, Token, (action) => ConfirmWindowExecute(action));
            Messenger.Default.Register<DialogWindow>(this, Token, (action) => DialogWindowExecute(action));
            Messenger.Default.Register<NavigateToView>(this, Token, (action) => NavigateToViewExecute(action));
            Messenger.Default.Register<CloseDialogWindow>(this, Token, (action) => CloseWindowExecute(action));

            log4net.Config.XmlConfigurator.Configure();
            Closing += Window_Closing;
            Loaded += Window_Loaded;
            Activated += BaseWindow_Activated;
            Microsoft.Win32.SystemEvents.DisplaySettingsChanged += DisplaySettingsChanged;
            StyleManager.ApplicationTheme = new Windows8TouchTheme();

            double width = SystemParameters.PrimaryScreenWidth;
            double height = SystemParameters.PrimaryScreenHeight;
            ////this.Width = 0;
            ////this.Height = 0;
            this.Left = 0;
            this.Top = 0;
            this.WindowStyle = System.Windows.WindowStyle.None;
            this.ResizeMode = System.Windows.ResizeMode.NoResize;


            this.WindowState = WindowState.Normal;
            ResourceManager.SetWindowState(this);
            //System.Diagnostics.Debug.WriteLine(string.Format("BaseWindow : this.Height {0}, this.Width {1}, this.Left {2}, this.Top{3},this.Title {4}", this.Height, this.Width, this.Left, this.Top, this.Title));

            // Hook keyboard control to all textbox elements in window
            // Make sure that all controls are loaded and then attach keyboard for textbox elements
            AttachKeyboard();
            HookupRoutedEventTracking();
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][End:Constructor][WindowTitle=" + this.Title + "][width=" + this.Width.ToString() + "][height=" + this.Height.ToString() + "]");
        }

        void BaseWindow_Activated(object sender, EventArgs e)
        {
            if (this.DataContext != null && this.DataContext.GetType().BaseType == typeof(Application.ViewModels.BaseViewModel))
            {
                var some = "";
            }
        }
        private void AttachLoadingAdorner()
        {
            IEnumerable<Grid> grid = (FindLogicalChildren<Grid>(this));
            if (grid.Count() == 0) return;
            UIElement mainPane = grid.ElementAt(0);
            windowOverlay = new OverlayAdorner(mainPane);
            windowOverlay.Name = "OverLayLoader";
            windowOverlay.FontSize = 15;
            windowOverlay.OverlayedText = "";
            windowOverlay.Opacity = 0.8;
            windowOverlay.Visibility = System.Windows.Visibility.Collapsed;
            windowOverlay.Typeface = new Typeface(FontFamily, FontStyles.Italic,
                FontWeights.Bold, FontStretch);
            System.Windows.Documents.AdornerLayer.GetAdornerLayer(mainPane).Add(windowOverlay);



        }
        void ToggleBusyIndicator(Window window, bool Visible)
        {
            RadBusyIndicator indicator = (window as BaseWindow).windowBusyIndicator;
            if (indicator != null)
            {
                System.Diagnostics.Debug.WriteLine("ToggleBusyIndicator START, window.Title," + window.Title + ", Visible, " + Visible + ", " + DateTime.Now.Millisecond);
                indicator.IsBusy = Visible;
                System.Diagnostics.Debug.WriteLine("ToggleBusyIndicator END, window.Title," + window.Title + ", Visible, " + Visible + ", " + DateTime.Now.Millisecond);
            }
        }
        void AttachRadBusyIndicator()
        {
            windowBusyIndicator = FindVisualChildren<RadBusyIndicator>(this).FirstOrDefault();
        }
        private void AttachKeyboard()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                try
                {
                    foreach (TextBox textbox in FindLogicalChildren<TextBox>(this))
                    {
                        textbox.GotFocus += textbox_GotFocus;
                        textbox.LostFocus += textbox_LostFocus;
                    }
                    foreach (TextBox textbox in FindVisualChildren<TextBox>(this))
                    {
                        textbox.GotFocus += textbox_GotFocus;
                        textbox.LostFocus += textbox_LostFocus;
                    }
                    foreach (PasswordBox pb in FindVisualChildren<PasswordBox>(this))
                    {
                        if (pb.Focusable)
                        {
                            pb.GotFocus += PasswordBox_GotFocus;
                            pb.LostFocus += PasswordBox_LostFocus;
                        }
                    }
                    // Grid view edit templates are not caught in either visual or logical child of the window
                    // So for dispalying keyboard on cell edit , tap the begin edit event of gridview to plug the keyboard
                    // toggle functionlaity
                    foreach (RadGridView gridView in FindVisualChildren<RadGridView>(this))
                    {
                        gridView.GotFocus += gridView_GotFocus;
                    }
                    foreach (RadGridView gridView in FindLogicalChildren<RadGridView>(this))
                    {
                        gridView.GotFocus += gridView_GotFocus;
                    }

                    foreach (RadListBox radListBox in FindVisualChildren<RadListBox>(this))
                    {
                        radListBox.GotFocus += gridView_GotFocus;
                    }
                    foreach (RadListBox radListBox in FindLogicalChildren<RadListBox>(this))
                    {
                        radListBox.GotFocus += gridView_GotFocus;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][AttachKeyboard][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }


            }));
        }

        private object CloseWindowExecute(CloseDialogWindow action)
        {
            Telerik.Windows.Controls.RadWindowManager.Current.CloseAllWindows();
            return null;
        }
        private void gridView_GotFocus(object sender, RoutedEventArgs e)
        {
            foreach (RadNumericUpDown numericDropdown in FindVisualChildren<RadNumericUpDown>(this))
            {
                foreach (RepeatButton rpt in FindVisualChildren<RepeatButton>(this))
                {
                    rpt.Focusable = false;
                }
            }
            ToggleKeyboardForGridCellEdit();
        }
        #endregion

        #region Helper Methods for View Navigation/Transition

        #region Events for Transition
        public event EventHandler<TransitionStateEventArgs> TransitionStateChanged;
        protected virtual void OnTransitionStateChange(TransitionStateEventArgs e)
        {
            EventHandler<TransitionStateEventArgs> handler = TransitionStateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        public class TransitionStateEventArgs : EventArgs
        {
            public TransitionState State { get; set; }
            public DateTime StateChangedTime { get; set; }
        }
        public enum TransitionState
        {
            Started,
            Complete
        }
        #endregion

        ViewModelMappings.View currentViewName = ViewModelMappings.View.None;
        async void NavigateToViewExecute(NavigateToView action)
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(() =>
                {
                    Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Start:NavigateToViewExecute][Window.Title=" + this.Title + "]");
                    try
                    {
                        TransitionStateEventArgs args = new TransitionStateEventArgs();
                        args.State = TransitionState.Started;
                        OnTransitionStateChange(args);
                        currentViewName = ViewModelMappings.View.None;
                        System.Windows.Window windowToShow, windowToClose;
                        windowToShow = (Window)Helpers.WindowMappings.WindowInstance(action.NextViewName, action);
                        if (action.CloseCurrentView)
                        {
                            windowToClose = (Window)Helpers.WindowMappings.WindowInstance(action.CurrentViewName);
                            if (windowToClose != null)
                            {
                                currentViewName = action.CurrentViewName;
                            }
                        }
                        else
                        {
                            currentViewName = ViewModelMappings.View.None;
                        }
                        WindowMappings.currentWindow = this;
                        WindowMappings.nextWindow = windowToShow;

                        if (windowToShow.IsLoaded)
                        {
                            if (action.Refresh)
                            {
                                (windowToShow as BaseWindow).RefreshWindow(action.Payload);
                            }

                            ShowWindow(windowToShow, action.SelectTab);


                            if (windowToShow.DataContext != null && windowToShow.DataContext.GetType().BaseType == typeof(Application.ViewModels.BaseViewModel))
                            {
                                ((Application.ViewModels.BaseViewModel)windowToShow.DataContext).OnActivate();
                            }
                            if (currentViewName != ViewModelMappings.View.None)
                            {
                                Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowTitle=" + WindowMappings.currentWindow.Title + "][Closed]");
                                Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
                                WindowMappings.currentWindow.Tag = "NOWARN";
                                CloseWindow(WindowMappings.currentWindow);
                                Helpers.WindowMappings.DisposeWindowInstance(currentViewName);
                            }

                            if ((WindowMappings.currentWindow != WindowMappings.nextWindow) && windowToShow.IsVisible)
                            {
                                HideWindow(WindowMappings.currentWindow);
                            }

                            //MovePrevious();
                        }
                        else
                        {
                            if (action.Refresh)
                            {
                                (windowToShow as BaseWindow).RefreshWindow(action.Payload);
                            }
                            //Note: Window.Show method is synchronous, which means it executes after the  window is loaded and ready for interaction.
                            ShowWindow(windowToShow, action.SelectTab);

                            if (currentViewName != ViewModelMappings.View.None)
                            {
                                Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowTitle=" + WindowMappings.currentWindow.Title + "][Closed]");
                                Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
                                WindowMappings.currentWindow.Tag = "NOWARN";
                                CloseWindow(WindowMappings.currentWindow);
                                Helpers.WindowMappings.DisposeWindowInstance(currentViewName);
                            }

                            HideWindow(WindowMappings.currentWindow);


                            // MoveNext();
                        }
                        Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][End:NavigateToViewExecute][Window.Title=" + this.Title + "]");
                        // return null;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][NavigateToViewExecute][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    }
                }));
            });


        }

        private void CloseWindow(Window window)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][CloseWindow][Window.Title : " + window.Title + "]");

            window.Close();
        }
        private void HideWindow(Window window)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][HideWindow][Window.Title : " + window.Title + "]");

            window.Hide();
        }

        private void ShowWindow(Window window, string selectView)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][ShowWindow][Window.Title : " + window.Title + "]");
            window.Show();
            HighlightViewInWindow = window;
            HighlightViewName = selectView;
            window.DataContextChanged += window_DataContextChanged;

        }
        Window HighlightViewInWindow = null;
        string HighlightViewName = string.Empty;
        void window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            bool datacontextLoaded = (sender as Window).DataContext == null ? false : true;
            if (datacontextLoaded)
            {

                HighlightView(HighlightViewInWindow, HighlightViewName);
            }
        }
        public string DefaultSeletedTab { get; set; }
        async void HighlightView(Window window, string selectView)
        {
            await Task.Run(() =>
            {
                try
                {
                    if (true)
                    {
                        window.Dispatcher.BeginInvoke((Action)delegate
                        {
                            if (!string.IsNullOrEmpty(selectView))
                            {
                                string tabViewName = Helpers.WindowMappings.GetTabViewName(selectView);
                                if (!string.IsNullOrEmpty(tabViewName))
                                {
                                    RadTabItem itemm = (RadTabItem)window.FindName(tabViewName);
                                    if (itemm != null)
                                    {
                                        itemm.IsSelected = true;
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(selectView) && !string.IsNullOrEmpty((window as BaseWindow).DefaultSeletedTab))
                            {
                                RadTabItem itemm = (RadTabItem)window.FindName((window as BaseWindow).DefaultSeletedTab);
                                if (itemm != null)
                                {
                                    itemm.IsSelected = true;
                                }
                            }
                        }, DispatcherPriority.Render);
                    }

                }
                catch (Exception ex)
                {
                }
            });
        }
        private void ShowDialog(RadWindow Rwindow)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][ShowDialog][Window.Header : " + Rwindow.Header.ToString() + "]");

            Rwindow.ShowDialog();
        }

        async void LoadWindow(Window window)
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        System.Diagnostics.Debug.WriteLine("Before  window.Show(), " + DateTime.Now.Millisecond);

                    }));
            });
        }
        void MoveNext()
        {
            Storyboard sb = new Storyboard();
            DoubleAnimation animation = new DoubleAnimation(SystemParameters.PrimaryScreenWidth, 0, new Duration(sp));
            animation.AccelerationRatio = .2;
            animation.DecelerationRatio = .8;
            animation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(animation, WindowMappings.nextWindow);
            Storyboard.SetTargetProperty(animation, new PropertyPath(LeftProperty));

            double width = SystemParameters.PrimaryScreenWidth;
            double height = SystemParameters.PrimaryScreenHeight;
            ResourceManager.SetWindowState(WindowMappings.nextWindow);
            //WindowMappings.nextWindow.Width = width;
            //WindowMappings.nextWindow.Height = height;
            WindowMappings.nextWindow.Left = width;

            DoubleAnimation nextanimation = new DoubleAnimation(0, -SystemParameters.PrimaryScreenWidth, new Duration(sp));
            nextanimation.AccelerationRatio = .2;
            nextanimation.DecelerationRatio = .8;
            nextanimation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(nextanimation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(nextanimation, new PropertyPath(LeftProperty));

            DoubleAnimation opacityAnimation = new DoubleAnimation(1, 0, new Duration(sp));
            opacityAnimation.AutoReverse = true;
            Storyboard.SetTarget(opacityAnimation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(OpacityProperty));

            sb.Children.Add(animation);
            sb.Children.Add(nextanimation);
            //sb.Children.Add(opacityAnimation);
            sb.Completed += MoveNext_Completed;
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
            sb.Begin(this);
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext][WindowMappings.currentWindow.Title=" + WindowMappings.currentWindow.Title + "][WindowMappings.nextWindow.Title=" + WindowMappings.nextWindow.Title + "]");
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext][NextWindowTitle=" + WindowMappings.nextWindow.Title + "][NextWindowWidth=" + WindowMappings.nextWindow.Width.ToString() + "][NextWindowHeight=" + WindowMappings.nextWindow.Height.ToString() + "]");

        }
        void MoveNext_Completed(object sender, EventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext_Completed][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
            HideWindow(WindowMappings.currentWindow);
            TransitionStateEventArgs args = new TransitionStateEventArgs();
            args.State = TransitionState.Complete;
            OnTransitionStateChange(args);
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MoveNext_Completed][WindowTitle=" + WindowMappings.currentWindow.Title + "][Hidden]");
        }
        void MovePrevious()
        {
            Storyboard sb = new Storyboard();
            WindowMappings.currentWindow.Left = 0;
            DoubleAnimation animation = new DoubleAnimation(0, SystemParameters.PrimaryScreenWidth, new Duration(sp));
            animation.AccelerationRatio = .2;
            animation.DecelerationRatio = .8;
            animation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(animation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(animation, new PropertyPath(LeftProperty));

            DoubleAnimation opacityAnimation = new DoubleAnimation(1, 0, new Duration(sp));
            opacityAnimation.AutoReverse = true;
            Storyboard.SetTarget(opacityAnimation, WindowMappings.currentWindow);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(OpacityProperty));

            double width = SystemParameters.PrimaryScreenWidth;
            double height = SystemParameters.PrimaryScreenHeight;
            ResourceManager.SetWindowState(WindowMappings.nextWindow);
            //WindowMappings.nextWindow.Width = width;
            //WindowMappings.nextWindow.Height = height;
            WindowMappings.nextWindow.Left = -width;

            DoubleAnimation nextanimation = new DoubleAnimation(-SystemParameters.PrimaryScreenWidth, 0, new Duration(sp));
            nextanimation.AccelerationRatio = .2;
            nextanimation.DecelerationRatio = .8;
            nextanimation.FillBehavior = FillBehavior.HoldEnd;
            Storyboard.SetTarget(nextanimation, WindowMappings.nextWindow);
            Storyboard.SetTargetProperty(nextanimation, new PropertyPath(LeftProperty));
            sb.Children.Add(animation);
            sb.Children.Add(nextanimation);
            //sb.Children.Add(opacityAnimation);
            sb.Completed += MovePrevious_Completed;
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
            sb.Begin(this);
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious][WindowMappings.currentWindow.Title=" + WindowMappings.currentWindow.Title + "][WindowMappings.nextWindow.Title=" + WindowMappings.nextWindow.Title + "]");
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious][NextWindowTitle=" + WindowMappings.nextWindow.Title + "][NextWindowWidth=" + WindowMappings.nextWindow.Width.ToString() + "][NextWindowHeight=" + WindowMappings.nextWindow.Height.ToString() + "]");
        }
        void MovePrevious_Completed(object sender, EventArgs e)
        {
            if (currentViewName != ViewModelMappings.View.None)
            {
                try
                {
                    Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowTitle=" + WindowMappings.currentWindow.Title + "][Closed]");
                    Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][WindowMappings.currentWindow][Left,Top]=[" + WindowMappings.currentWindow.Left + "," + WindowMappings.currentWindow.Top + "][WindowMappings.nextWindow][Left,Top]=[" + WindowMappings.nextWindow.Left + "," + WindowMappings.nextWindow.Top + "]");
                    WindowMappings.currentWindow.Tag = "NOWARN";

                    CloseWindow(WindowMappings.currentWindow);
                    Helpers.WindowMappings.DisposeWindowInstance(currentViewName);
                }
                catch (Exception ex)
                {
                    Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][MovePrevious_Completed][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                    throw;
                }
            }
        }
        #endregion

        #region Window events and virtual methods
        public void MinimizeWindow()
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Uri iconUri = new Uri("Resources/Images/appicon.ico", UriKind.RelativeOrAbsolute);
            //this.Icon = System.Windows.Media.Imaging.BitmapFrame.Create(iconUri);
            AlwaysShowNumericKeyboard();
            AttachLoadingAdorner();
            AttachRadBusyIndicator();
            System.Diagnostics.Debug.WriteLine("Window_Loaded(), " + DateTime.Now.Millisecond);

        }
        public void AlwaysShowNumericKeyboard()
        {
            if (AlwaysShowKeyboard)
            {
                ShowKeyboard("Numeric");
            }
        }

        public virtual void HideWindow()
        {

        }
        public virtual void RefreshWindow(object payload)
        {

        }
        public async virtual void LoadContext(Action RunAsync, Action UpdateUI)
        {
            await Task.Run(() =>
            {
                RunAsync();
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
              {
                  UpdateUI();
              }));
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Window_Closing][Window.Title=" + this.Title + "]");
            // Window close event is fired in tow cases, 
            // 1: when we click on close button of window
            // 2: when we do a back naviagtion.
            // the NOWARN tag is set when we do a backward navigation in application, where we want to 
            // close the existing window and move to previous one, and dont want to exit the application.
            // When we click the close button of any window, the window tag is not set and a warning is displayed
            // but when we do a back navigation we set the tag , indicating that we dont to close 
            // the app and just want to navigate backword
            if (((Window)sender).Tag != null && ((Window)sender).Tag.ToString() == "NOWARN")
            {
                CloseAssociatedWindows();
                return;
            }
            // Uncomment when app exit scenarios are to be handled
            if (!Application.ViewModelPayload.PayloadManager.ApplicationPayload.ActivityCompleted)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Content = Application.Helpers.Constants.Common.ActivityPending;
                bool? dialogResult = null;
                parameters.WindowStyle = (Style)(this.Resources["RadWindowStyle"]);
                parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
                RadWindow.Alert(parameters);
                e.Cancel = true;
            }
            else
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Content = Application.Helpers.Constants.Common.AppExitConfirmation;
                bool? dialogResult = null;
                parameters.WindowStyle = (Style)(this.Resources["RadWindowStyle"]);
                parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
                RadWindow.Confirm(parameters);
                if (dialogResult == true)
                {
                    var Processes = System.Diagnostics.Process.GetProcessesByName("dbeng16");
                    foreach (var dbmlProcess in Processes)
                    {
                        dbmlProcess.Kill();
                    }
                    System.Windows.Application.Current.Shutdown();
                }
                else
                {
                    e.Cancel = true;
                }
            }

        }

        public void CloseAssociatedWindows()
        {

            if (AssociatedWindows.Count > 0)
            {
                System.Windows.Window windowToClose = null;
                for (int windowIndex = 0; windowIndex < AssociatedWindows.Count; windowIndex++)
                {
                    try
                    {
                        windowToClose = (Window)Helpers.WindowMappings.WindowInstance(AssociatedWindows.ElementAt(windowIndex));
                        if (windowToClose != null)
                        {
                            windowToClose.Tag = "NOWARN";
                            CloseWindow(windowToClose);
                            Helpers.WindowMappings.DisposeWindowInstance(AssociatedWindows.ElementAt(windowIndex));
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Window_Closing][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        throw;
                    }
                }
                AssociatedWindows.Clear();
            }
        }
        #endregion

        #region Keyboard support methods
        public bool AlwaysShowKeyboard { get; set; }

        public void textbox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (keyboard != null)
            {
                if (!AlwaysShowKeyboard)
                {
                    keyboard.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        public void textbox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!AlwaysShowKeyboard)
            {
                ShowKeyboard(((TextBox)sender).Tag);
            }
        }
        void PasswordBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!AlwaysShowKeyboard)
            {
                ShowKeyboard(((PasswordBox)sender).Tag);
            }
        }
        void PasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (keyboard != null)
            {
                if (!AlwaysShowKeyboard)
                {
                    keyboard.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
        public void ShowKeyboard(object obj = null)
        {
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][Start:ShowKeyboard][Window.Title=" + this.Title + "]");
            try
            {
                if (keyboard == null)
                {
                    IEnumerable<SalesLogicExpress.Views.Common.Keyboard> keyboardInstance = FindVisualChildren<SalesLogicExpress.Views.Common.Keyboard>(this);
                    keyboard = keyboardInstance.Count() == 0 ? null : keyboardInstance.First();
                }
                if (keyboard != null)
                {
                    keyboard.Tag = obj;
                    keyboard.Visibility = System.Windows.Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][ShowKeyboard][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Presentation.Helpers][BaseWindow][End:ShowKeyboard][Window.Title=" + this.Title + "]");
        }

        private void GridViewEditStart(object sender, GridViewBeginningEditRoutedEventArgs e)
        {
            ToggleKeyboardForGridCellEdit();
        }
        async void ToggleKeyboardForGridCellEdit()
        {
            // wait for 100ms, let the grid render the edit template for the cell, 
            // and then find textbox instance inside the cell
            System.Threading.Thread.Sleep(100);

            // This call is async (non ui thread) so that the main method continues 
            // executing and this runs in background
            await Task.Run(() =>
            {
                // Since we want to access the keyboard which is in then UI thread, we will use the Application
                // Dispatcher to invoke UI related methods for accessing the currently focussed element
                // and hook keyboard if needed.
                System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action(() =>
                    {
                        FrameworkElement focussedElement = System.Windows.Input.Keyboard.FocusedElement as FrameworkElement;

                        if (focussedElement != null)
                        {
                            if (focussedElement.GetType() == typeof(TextBox))
                            {
                                ((TextBox)focussedElement).LostFocus -= textbox_LostFocus;
                                ((TextBox)focussedElement).LostFocus += textbox_LostFocus;
                                ((TextBox)focussedElement).GotFocus -= textbox_GotFocus;
                                ((TextBox)focussedElement).GotFocus += textbox_GotFocus;
                                ShowKeyboard(focussedElement.Tag);
                            }
                        }
                    }
                ));
            });

        }
        #endregion

        #region Orientation change support method
        protected virtual void DisplaySettingsChanged(object sender, EventArgs e)
        {

            if (SystemParameters.PrimaryScreenWidth > SystemParameters.PrimaryScreenHeight)
            {
                WindowOrientation = WindowOrientation.Landscape;
            }
            else
            {
                WindowOrientation = WindowOrientation.Portrait;

            }
            if (this.IsActive)
            {
                //  this.WindowState = System.Windows.WindowState.Maximized;
            }
            ResourceManager.SetWindowState(this);
        }
        #endregion

        #region Helper methods to find nested child controls
        public static IEnumerable<T> FindLogicalChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                foreach (object rawChild in LogicalTreeHelper.GetChildren(depObj))
                {
                    if (rawChild is DependencyObject)
                    {
                        DependencyObject child = (DependencyObject)rawChild;
                        if (child is T)
                        {
                            yield return (T)child;
                        }

                        foreach (T childOfChild in FindLogicalChildren<T>(child))
                        {
                            yield return childOfChild;
                        }
                    }
                }
            }
        }
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = System.Windows.Media.VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }
                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        #endregion

        #region Helper methods for showing modal dialogs/alerts/confirm dialogs
        private object DialogWindowExecute(DialogWindow action)
        {
            try
            {
                // we have different modal dialog templates in a resource file
                // Resource file is merged in my current window
                // using Resource manager, extract template with x:Name or x:Key as action.title
                // Set content to rad window
                string contentTemplate = WindowMappings.GetResourceName(action.TemplateKey);
                RadWindow dialogWindow = new RadWindow();
                dialogWindow.Style = (Style)(this.Resources["RadWindowStyle"]);
                dialogWindow.Owner = this;
                dialogWindow.Name = action.TemplateKey;
                dialogWindow.ModalBackground = Brushes.Gray;
                dialogWindow.MaxWidth = this.ActualWidth / 1.2;
                //dialogWindow.MaxHeight = this.ActualHeight / 1.1;
                dialogWindow.Header = action.Title;
                dialogWindow.ContentTemplate = this.FindResource(contentTemplate) as DataTemplate;
                dialogWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
                dialogWindow.WindowState = System.Windows.WindowState.Normal;
                dialogWindow.CanMove = false;
                dialogWindow.ResizeMode = System.Windows.ResizeMode.NoResize;
                dialogWindow.HideMaximizeButton = true;
                dialogWindow.HideMinimizeButton = true;
                dialogWindow.DataContext = action.Payload != null ? action.Payload : this.DataContext;
                dialogWindow.SetBinding(Window.ContentProperty, "");
                dialogWindow.Closed += dialogWindow_Closed;
                dialogWindow.Loaded += dialogWindow_Loaded;
                dialogWindow.PreviewClosed += dialogWindow_PreviewClosed;
                dialogWindow.DataContextChanged += dialogWindow_DataContextChanged;
                ToggleOverlayOnWindow(true);
                ShowDialog(dialogWindow);
                ToggleOverlayOnWindow(false);
                action.Cancelled = dialogWindow.DialogResult == null ? true : false;
                action.Closed = dialogWindow.DialogResult.HasValue && dialogWindow.DialogResult.Value == false ? true : false;
                action.Confirmed = dialogWindow.DialogResult == null ? false : dialogWindow.DialogResult.Value;
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][DialogWindowExecute][Window.Title=" + this.Title + "][action=" + action.SerializeToJson() + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                throw;
            }
            return null;
        }
        bool IsDirty = false;

        void dialogWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            IsDirty = true;

        }

        protected virtual void dialogWindow_PreviewClosed(object sender, WindowPreviewClosedEventArgs e)
        {
            if (IsDirty)
                e.Cancel = true;
        }
        void ToggleOverlayOnWindow(bool Visible)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                if (windowOverlay != null)
                    windowOverlay.Visibility = !Visible && Telerik.Windows.Controls.RadWindowManager.Current.GetWindows().Count == 0 ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                //windowOverlay.Visibility = !Visible ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;

            }));
        }
        protected virtual void dialogWindow_Loaded(object sender, RoutedEventArgs e)
        {

            AttachKeyboard();
        }


        private void dialogWindow_Closed(object sender, WindowClosedEventArgs e)
        {

            if (DefaultFocusElement != null)
                DefaultFocusElement.Focus();
        }
        private object ConfirmWindowExecute(ConfirmWindow action)
        {
            DialogParameters parameters = new DialogParameters();
            if (!string.IsNullOrEmpty(action.Header))
            {
                parameters.Header = action.Header;
            }
            if (!string.IsNullOrEmpty(action.TemplateKey))
            {
                string contentTemplate = WindowMappings.GetResourceName(action.TemplateKey);
                UserControl ctrl = new UserControl();
                ctrl.ContentTemplate = this.FindResource(contentTemplate) as DataTemplate;
                ctrl.DataContext = action.Context;
                ctrl.SetBinding(Window.ContentProperty, "");
                parameters.Content = ctrl;
                parameters.ContentStyle = (Style)(this.Resources["RadConfirmCustomContentStyle"]);
            }
            else
            {
                parameters.Content = action.Message;
            }

            bool? dialogResult = null;
            parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
            parameters.OkButtonContent = action.OkButtonContent != null ? action.OkButtonContent : "Ok";
            parameters.CancelButtonContent = action.CancelButtonContent != null ? action.CancelButtonContent : "Cancel";
            ToggleOverlayOnWindow(true);
            parameters.WindowStyle = (Style)(this.Resources["RadWindowStyle"]);
            RadWindow.Confirm(parameters);
            //action.Closed = dialogResult == null ? true : false;
            if (dialogResult == null)
            {
                action.Closed = true;
                ToggleOverlayOnWindow(false);
                return null;
            }
            action.Confirmed = (bool)dialogResult.Value;
            action.Cancelled = !action.Confirmed;
            ToggleOverlayOnWindow(false);
            return null;
        }
        private object AlertWindowExecute(SalesLogicExpress.Application.Helpers.AlertWindow action)
        {


            /*
             DialogParameters parameters = new DialogParameters();
            UserControl ctrl = new UserControl();
            DemoVM vm = new DemoVM();
            vm.Name = "From Datacontext";
            ctrl.ContentTemplate = this.FindResource("dt") as DataTemplate;
            ctrl.DataContext = vm;
            ctrl.SetBinding(Window.ContentProperty, "");
            parameters.Content = ctrl;
            bool? dialogResult = null;
            parameters.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
            parameters.ContentStyle = (Style)(this.Resources["RadConfirmStyle1"]);
            RadWindow.Confirm(parameters);
            var xx = dialogResult;
             */
            DialogParameters dp = new DialogParameters();
            dp.Header = action.Header;
            dp.Content = action.Message;

            bool? dialogResult = null;
            dp.Closed = (confirmDialog, eventArgs) => { dialogResult = eventArgs.DialogResult; };
            ToggleOverlayOnWindow(true);
            dp.WindowStyle = (Style)(this.Resources["RadWindowStyle"]);
            RadWindow.Alert(dp);
            if (dialogResult == null)
            {
                action.Closed = true;
                ToggleOverlayOnWindow(false);
                return null;
            }
            ToggleOverlayOnWindow(false);
            return null;
        }
        #endregion

        #region Event Tracking Methods
        List<string> trackEvents = new List<string>();
        void HookupRoutedEventTracking()
        {
            try
            {
                if (isRegistered)
                {
                    return;
                }
                trackEvents.Add("Click");
                trackEvents.Add("KeyUp");
                trackEvents.Add("MouseDoubleClick");
                trackEvents.Add("Checked");
                trackEvents.Add("Unchecked");
                trackEvents.Add("SelectionChanged");
                //trackEvents.Add("LostFocus");
                //trackEvents.Add("MouseMove");
                foreach (RoutedEvent routedEvent in EventManager.GetRoutedEvents())
                {
                    if (trackEvents.Contains(routedEvent.Name))
                    {
                        EventManager.RegisterClassHandler(typeof(Window), routedEvent, new RoutedEventHandler(RoutedEventHandler), true);
                    }
                }
                foreach (RoutedEvent routedEvent in EventManager.GetRoutedEvents())
                {
                    EventManager.RegisterClassHandler(typeof(RadWindow), routedEvent, new RoutedEventHandler(RoutedEventHandler), true);
                }

                isRegistered = true;
            }
            catch (Exception ex)
            {
            }
        }
        private static DependencyObject GetParentObject(DependencyObject child)
        {
            if (child == null)
            {
                return null;
            }

            // handle content elements separately
            ContentElement contentElement = child as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null)
                {
                    return parent;
                }

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            // also try searching for parent in framework elements (such as DockPanel, etc.)
            FrameworkElement frameworkElement = child as FrameworkElement;
            if (frameworkElement != null && frameworkElement.Parent != null)
            {
                return frameworkElement.Parent;
            }

            // if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child);
        }
        DependencyObject FindParentWithContext(object current)
        {
            DependencyObject depObj = current as DependencyObject;
            DependencyObject depParentObj = LogicalTreeHelper.GetParent(depObj);
            return depParentObj == null ? depObj : FindParentWithContext(depParentObj);
        }
        async void RoutedEventHandler(object sender, RoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                DependencyObject p;
                object senderInfo = sender;
                MouseButtonState pressedMouse = MouseButtonState.Pressed;
                Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
                {
                    string Control = string.Empty, Event = string.Empty, TextMsg = string.Empty, ControlText = string.Empty, WindowTitle = string.Empty;
                    BaseWindow win = senderInfo as BaseWindow;

                    #region Disable controls to avoid transaction creation

                    try
                    {
                        // Check if Pretrip is not done 
                        if (!SalesLogicExpress.Application.ViewModels.CommonNavInfo.IsPretripInspectionDone)
                        {
                            if (SalesLogicExpress.Helpers.Constants.BlackListedControls.ContainsKey(sender.GetType().FullName))
                            {
                                foreach (string controlName in SalesLogicExpress.Helpers.Constants.BlackListedControls[sender.GetType().FullName])
                                {
                                    try { ((Control)win.FindName(controlName)).IsEnabled = false; }
                                    catch (Exception) { /* Do Nothing*/}
                                }
                            }
                        }
                    }
                    catch (Exception) { }


                    #endregion

                    if (trackEvents.Contains(e.RoutedEvent.Name))
                    {
                        try
                        {
                            p = FindParentWithContext(e.OriginalSource as DependencyObject);
                            if (p != null)
                            {
                                if (e.OriginalSource.GetType() == typeof(RadTabControl))
                                {
                                    RadTabControl b = e.OriginalSource as RadTabControl;
                                    Control = "RadTabControl";
                                    Event = e.RoutedEvent.Name;
                                    ControlText = (e.OriginalSource as RadTabControl).SelectedItem == null ? "" : ((e.OriginalSource as RadTabControl).SelectedItem as RadTabItem).Tag + "";
                                }
                                if (false && e.OriginalSource.GetType() == typeof(RadWatermarkTextBox))
                                {
                                    RadWatermarkTextBox b = e.OriginalSource as RadWatermarkTextBox;
                                    Control = "RadWatermarkTextBox";
                                    Event = e.RoutedEvent.Name == "KeyUp" ? e.RoutedEvent.Name : "";
                                    ControlText = b.Text.ToString();
                                }
                                if (e.OriginalSource.GetType() == typeof(Button))
                                {
                                    Button b = e.OriginalSource as Button;
                                    Control = "Button";
                                    Event = e.RoutedEvent.Name;

                                    ControlText = b.Tag == null ? "" : b.Tag.ToString();
                                }
                                if (e.OriginalSource.GetType() == typeof(RadButton))
                                {
                                    RadButton b = e.OriginalSource as RadButton;
                                    Control = "RadButton";
                                    Event = e.RoutedEvent.Name;
                                    ControlText = b.Tag == null ? "" :
                                        (((b.Tag.ToString().ToUpper() == "TOGGLEKEYBOARD") || (b.Tag.ToString().ToUpper() == "SHIFT")
                                        || (b.Tag.ToString().ToUpper() == "ENTER")
                                        || (b.Tag.ToString().ToUpper() == "MOVELEFT") || (b.Tag.ToString().ToUpper() == "MOVERIGHT")
                                        || (b.Tag.ToString().ToUpper() == "@") || (b.Tag.ToString().ToUpper() == "SPACE")
                                        || (b.Tag.ToString().ToUpper() == "BACKSPACE") || (b.Tag.ToString().ToUpper() == "TAB")
                                        || (b.Tag.ToString().ToUpper() == "CAPS")) ? "" : b.Tag.ToString());


                                }
                                if (false && e.OriginalSource.GetType() == typeof(TextBox))
                                {
                                    TextBox b = e.OriginalSource as TextBox;
                                    Control = "TextBox";
                                    Event = e.RoutedEvent.Name == "KeyUp" ? e.RoutedEvent.Name : "";
                                    //Event = e.RoutedEvent.Name;
                                    ControlText = b.Text.ToString();
                                }
                                if (false && e.OriginalSource.GetType() == typeof(CheckBox))
                                {
                                    CheckBox b = e.OriginalSource as CheckBox;
                                    Control = "CheckBox";
                                    Event = e.RoutedEvent.Name == "Checked" || e.RoutedEvent.Name == "Unchecked" ? e.RoutedEvent.Name : "";
                                    ControlText = b.Tag == null ? "" : b.Tag.ToString() + "_" + b.IsChecked;
                                }
                                if (false && e.OriginalSource.GetType() == typeof(ComboBox))
                                {
                                    ComboBox b = e.OriginalSource as ComboBox;
                                    Control = "ComboBox";
                                    Event = e.RoutedEvent.Name;
                                    ControlText = b.SelectedItem.ToString();
                                }
                                WindowTitle = win == null ? "" : win.ScreenID;
                                if (!string.IsNullOrEmpty(Control) && !string.IsNullOrEmpty(Event) && !string.IsNullOrEmpty(ControlText))
                                {
                                    string description = "";
                                    if (win != null)
                                    {
                                        description = string.IsNullOrEmpty(win.ScreenLifeCycle) ? "" : win.ScreenLifeCycle + "|";
                                        description = description + (string.IsNullOrEmpty(win.ScreenID) ? "" : win.ScreenID + "");
                                    }
                                    else
                                    {
                                        if (senderInfo.GetType() == typeof(RadWindow))
                                        {
                                            RadWindow rwindow = ((RadWindow)(senderInfo));
                                            description = rwindow.HasHeader ? rwindow.Header.ToString() : "";
                                        }
                                    }
                                    SalesLogicExpress.Domain.Activity ac = new SalesLogicExpress.Domain.Activity
                                    {
                                        RouteID = SalesLogicExpress.Application.ViewModelPayload.PayloadManager.ApplicationPayload.Route,
                                        ActivityType = ControlText,
                                        ActivityStart = DateTime.Now,
                                        ActivityDescription = description,
                                        IsTxActivity = false
                                    };

                                    SalesLogicExpress.Application.Helpers.ResourceManager.Transaction.AddNonTxActivity(ac);
                                    //System.Diagnostics.Debug.WriteLine(string.Format("ScreenID : {0}, ScreenLifeCycle : {1} ", ScreenID, ScreenLifeCycle));
                                    //System.Diagnostics.Debug.WriteLine(string.Format("Control : {0}, Event : {1}, ControlText : {2}, WindowTitle : {3}, Handled {4}", Control, Event, ControlText, WindowTitle, e.Handled));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("[SalesLogicExpress.Presentation.Helpers][BaseWindow][RoutedEventHandler][Window.Title=" + this.Title + "][ExceptionStackTrace = " + ex.StackTrace + "]");
                        }
                    }
                }));
            });
        }
        #endregion

    }
    /// <summary>
    /// Taskbar Class is used to Show/Hide Windows Taskbar.
    /// </summary>
    public class Taskbar
    {
        [DllImport("user32.dll")]
        private static extern int FindWindow(string className, string windowText);

        [DllImport("user32.dll")]
        private static extern int ShowWindow(int hwnd, int command);

        [DllImport("user32.dll")]
        public static extern int FindWindowEx(int parentHandle, int childAfter, string className, int windowTitle);

        [DllImport("user32.dll")]
        private static extern int GetDesktopWindow();

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 1;

        protected static int Handle
        {
            get
            {
                return FindWindow("Shell_TrayWnd", "");
            }
        }

        protected static int HandleOfStartButton
        {
            get
            {
                int handleOfDesktop = GetDesktopWindow();
                int handleOfStartButton = FindWindowEx(handleOfDesktop, 0, "button", 0);
                return handleOfStartButton;
            }
        }

        private Taskbar()
        {
            // hide ctor
        }

        public static void Show()
        {
            return;
            ShowWindow(Handle, SW_SHOW);
            ShowWindow(HandleOfStartButton, SW_SHOW);
        }

        public static void Hide()
        {
            return;
            ShowWindow(Handle, SW_HIDE);
            ShowWindow(HandleOfStartButton, SW_HIDE);
        }
    }

    public class TextBoxInputRegExBehaviour : Behavior<System.Windows.Controls.TextBox>
    {
        #region DependencyProperties
        public static readonly DependencyProperty RegularExpressionProperty =
                DependencyProperty.Register("TextBoxInputRegExBehaviour", typeof(string), typeof(TextBoxInputRegExBehaviour), null);

        public string RegularExpression
        {
            get { return (string)GetValue(RegularExpressionProperty); }
            set { SetValue(RegularExpressionProperty, value); }
        }

        public static readonly DependencyProperty MaxLengthProperty =
            DependencyProperty.Register("MaxLength", typeof(int), typeof(TextBoxInputRegExBehaviour),
                                            new FrameworkPropertyMetadata(int.MinValue));
        bool _IsSpaceAllowed = true;
        public bool IsSpaceAllowed
        {
            get { return _IsSpaceAllowed; }
            set { _IsSpaceAllowed = value; }
        }

        bool _AllowedOnlyOneDecimal = true;
        public bool AllowedOnlyOneDecimal
        {
            get { return _AllowedOnlyOneDecimal; }
            set { _AllowedOnlyOneDecimal = value; }
        }

        public int MaxLength
        {
            get { return (int)GetValue(MaxLengthProperty); }
            set { SetValue(MaxLengthProperty, value); }
        }

        public static readonly DependencyProperty EmptyValueProperty =
            DependencyProperty.Register("EmptyValue", typeof(string), typeof(TextBoxInputRegExBehaviour), null);

        public string EmptyValue
        {
            get { return (string)GetValue(EmptyValueProperty); }
            set { SetValue(EmptyValueProperty, value); }
        }
        #endregion
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewTextInput += PreviewTextInputHandler;
            AssociatedObject.PreviewKeyDown += PreviewKeyDownHandler;
            AssociatedObject.LostFocus += AssociatedObject_LostFocus;
            AssociatedObject.IsVisibleChanged += AssociatedObject_IsVisibleChanged;
            DataObject.AddPastingHandler(AssociatedObject, PastingHandler);
        }

        void AssociatedObject_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (AssociatedObject.Visibility == Visibility.Visible)
            {
                this.Dispatcher.BeginInvoke((Action)delegate
                {
                    Keyboard.Focus(AssociatedObject);
                }, DispatcherPriority.Render);
            }
        }

        void AssociatedObject_LostFocus(object sender, RoutedEventArgs e)
        {
            BindingExpression binding = (sender as System.Windows.Controls.TextBox).GetBindingExpression(System.Windows.Controls.TextBox.TextProperty);
            if (binding != null)
            {
                binding.UpdateSource();
            }
        }
        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.PreviewTextInput -= PreviewTextInputHandler;
            AssociatedObject.PreviewKeyDown -= PreviewKeyDownHandler;
            DataObject.RemovePastingHandler(AssociatedObject, PastingHandler);
        }
        void PreviewTextInputHandler(object sender, TextCompositionEventArgs e)
        {
            string text;
            if (this.AssociatedObject.Text.Length < this.AssociatedObject.CaretIndex)
                text = this.AssociatedObject.Text;
            else
            {
                //  Remaining text after removing selected text.
                string remainingTextAfterRemoveSelection;

                text = TreatSelectedText(out remainingTextAfterRemoveSelection)
                    ? remainingTextAfterRemoveSelection.Insert(AssociatedObject.SelectionStart, e.Text)
                    : AssociatedObject.Text.Insert(this.AssociatedObject.CaretIndex, e.Text);
            }

            e.Handled = !ValidateText(text);
        }
        void PreviewKeyDownHandler(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.Space && !IsSpaceAllowed)
            {

                e.Handled = true;

                return;
            }
            string text = null;

            // Handle the Backspace key
            if (e.Key == Key.Back)
            {
                if (!this.TreatSelectedText(out text))
                {
                    if (AssociatedObject.SelectionStart > 0)
                        text = this.AssociatedObject.Text.Remove(AssociatedObject.SelectionStart - 1, 1);
                }
            }
            // Handle the Delete key
            else if (e.Key == Key.Delete)
            {
                // If text was selected, delete it
                if (!this.TreatSelectedText(out text) && this.AssociatedObject.Text.Length > AssociatedObject.SelectionStart)
                {
                    // Otherwise delete next symbol
                    text = this.AssociatedObject.Text.Remove(AssociatedObject.SelectionStart, 1);
                }
            }
            if (this.AssociatedObject.Text.Length >= this.AssociatedObject.MaxLength)
            {
                return;
            }
            if (text == string.Empty)
            {
                this.AssociatedObject.Text = this.EmptyValue;
                if (e.Key == Key.Back)
                    AssociatedObject.SelectionStart++;
                e.Handled = true;
            }
            if (AllowedOnlyOneDecimal)
            {
                string strkey = e.Key.ToString().Substring(e.Key.ToString().Length - 1,
                  e.Key.ToString().Length - (e.Key.ToString().Length - 1));

                if (e.Key >= Key.D0 && e.Key <= Key.D9 ||
                e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
                {
                    //Do not allow more than 2 digits after decimal point
                    TextBox tb = sender as TextBox;
                    int cursorPosLeft = tb.SelectionStart;
                    int cursorPosRight = tb.SelectionStart + tb.SelectionLength;
                    string result1 = tb.Text.Substring(0, cursorPosLeft) +
                          strkey + tb.Text.Substring(cursorPosRight);
                    string[] parts = result1.Split('.');
                    if (parts.Length > 1)
                    {
                        if (parts[1].Length > 2 || parts.Length > 2)
                        {
                            e.Handled = true;
                        }
                    }
                }
            }
        }
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(DataFormats.Text))
            {
                string text = Convert.ToString(e.DataObject.GetData(DataFormats.Text));

                if (!ValidateText(text))
                    e.CancelCommand();
            }
            else
                e.CancelCommand();
        }
        private bool ValidateText(string text)
        {
            if (this.RegularExpression == null)
            {
                return (MaxLength == 0 || text.Length <= MaxLength);
            }
            return (new Regex(this.RegularExpression, RegexOptions.IgnoreCase)).IsMatch(text) && (MaxLength == 0 || text.Length <= MaxLength);
        }
        private bool TreatSelectedText(out string text)
        {
            text = null;
            if (AssociatedObject.SelectionLength > 0)
            {
                text = this.AssociatedObject.Text.Remove(AssociatedObject.SelectionStart, AssociatedObject.SelectionLength);
                return true;
            }
            return false;
        }

    }

    public class ScrollIntoViewBehavior : Behavior<RadListBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += ScrollIntoView;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.SelectionChanged -= ScrollIntoView;
            base.OnDetaching();
        }

        private void ScrollIntoView(object o, SelectionChangedEventArgs e)
        {
            ListBox b = (ListBox)o;
            if (b == null) return;
            if (b.SelectedItem == null) return;

            ListBoxItem item = (ListBoxItem)((ListBox)o).ItemContainerGenerator.ContainerFromItem(((ListBox)o).SelectedItem);
            if (item != null) item.BringIntoView();
        }
    }

}
