﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Data;

namespace SalesLogicExpress.Presentation.Helpers
{
    public class CustomColumnAggregator : AggregateFunction<SalesLogicExpress.Domain.ReturnItem, string> 
    {
        public CustomColumnAggregator()
        {
            this.AggregationExpression = items => StdDev(items);
        }
        private string StdDev(IEnumerable<SalesLogicExpress.Domain.ReturnItem> source)
        {
            var itemCount = source.Count();
            if (itemCount > 1)
            {
                var values = source.Select(i => i.UnitPrice);
                var average = values.Average();
                return "thi is some aggregate function"; 
            }
            return "thi is some aggregate function";
        }
    }
}
