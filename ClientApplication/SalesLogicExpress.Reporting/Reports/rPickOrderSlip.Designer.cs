namespace SalesLogicExpress.Reporting
{
    partial class rPickOrderSlip
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule2 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.StyleRule styleRule3 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector1 = new Telerik.Reporting.Drawing.DescendantSelector();
            Telerik.Reporting.Drawing.StyleRule styleRule4 = new Telerik.Reporting.Drawing.StyleRule();
            Telerik.Reporting.Drawing.DescendantSelector descendantSelector2 = new Telerik.Reporting.Drawing.DescendantSelector();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.txt_InvoiceNo = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.textBox3 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8821221590042114D), Telerik.Reporting.Drawing.Unit.Cm(0.81749171018600464D));
            this.textBox4.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Courier New";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.StyleName = "Normal.TableHeader";
            this.textBox4.Value = "PICKED QTY";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(71.9999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.81749176979064941D));
            this.textBox26.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Courier New";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox26.StyleName = "Normal.TableHeader";
            this.textBox26.Value = "ORDER\r\nQTY";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2483110427856445D), Telerik.Reporting.Drawing.Unit.Cm(0.81749171018600464D));
            this.textBox9.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Courier New";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.StyleName = "Normal.TableHeader";
            this.textBox9.Value = "CODE";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.4578237533569336D), Telerik.Reporting.Drawing.Unit.Cm(0.81749171018600464D));
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Courier New";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.StyleName = "Normal.TableHeader";
            this.textBox16.Value = "PRODUCT DESCRIPTION";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(2.8002004623413086D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1});
            this.detail.KeepTogether = true;
            this.detail.Name = "detail";
            // 
            // table1
            // 
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.2339942455291748D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.6481279730796814D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.0847916603088379D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.82020759582519531D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2483108043670654D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.4578266143798828D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5115128755569458D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458325982093811D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.43555557727813721D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox18);
            this.table1.Body.SetCellContent(0, 4, this.textBox24);
            this.table1.Body.SetCellContent(0, 5, this.textBox25);
            this.table1.Body.SetCellContent(0, 1, this.textBox6);
            this.table1.Body.SetCellContent(1, 4, this.textBox7);
            this.table1.Body.SetCellContent(1, 5, this.textBox8);
            this.table1.Body.SetCellContent(1, 0, this.shape1, 1, 2);
            this.table1.Body.SetCellContent(2, 5, this.textBox22);
            this.table1.Body.SetCellContent(2, 0, this.textBox2, 1, 2);
            this.table1.Body.SetCellContent(2, 4, this.textBox52);
            this.table1.Body.SetCellContent(0, 2, this.textBox30);
            this.table1.Body.SetCellContent(0, 3, this.textBox5);
            this.table1.Body.SetCellContent(1, 2, this.shape2, 1, 2);
            this.table1.Body.SetCellContent(2, 2, this.textBox3, 1, 2);
            tableGroup2.Name = "group1";
            tableGroup3.Name = "group";
            tableGroup1.ChildGroups.Add(tableGroup2);
            tableGroup1.ChildGroups.Add(tableGroup3);
            tableGroup1.ReportItem = this.textBox4;
            tableGroup5.Name = "group5";
            tableGroup6.Name = "group7";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ChildGroups.Add(tableGroup6);
            tableGroup4.Name = "group4";
            tableGroup4.ReportItem = this.textBox26;
            tableGroup7.ReportItem = this.textBox9;
            tableGroup8.ReportItem = this.textBox16;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnHeadersPrintOnEveryPage = true;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox24,
            this.textBox25,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.shape1,
            this.textBox22,
            this.textBox2,
            this.textBox52,
            this.textBox30,
            this.textBox5,
            this.textBox4,
            this.textBox26,
            this.textBox9,
            this.textBox16,
            this.shape2,
            this.textBox3});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.table1.Name = "table1";
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "Detail";
            tableGroup10.Name = "group2";
            tableGroup11.Name = "group3";
            this.table1.RowGroups.Add(tableGroup9);
            this.table1.RowGroups.Add(tableGroup10);
            this.table1.RowGroups.Add(tableGroup11);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.493260383605957D), Telerik.Reporting.Drawing.Unit.Cm(2.0291435718536377D));
            this.table1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table1.StyleName = "Normal.TableNormal";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2339942455291748D), Telerik.Reporting.Drawing.Unit.Cm(0.5115128755569458D));
            this.textBox18.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Courier New";
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.StyleName = "Normal.TableBody";
            this.textBox18.Value = "=Fields.PickedQuantityInPrimaryUOM";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2483105659484863D), Telerik.Reporting.Drawing.Unit.Cm(0.5115128755569458D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Courier New";
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox24.StyleName = "Normal.TableBody";
            this.textBox24.Value = "=Fields.ItemNumber";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.45782470703125D), Telerik.Reporting.Drawing.Unit.Cm(0.5115128755569458D));
            this.textBox25.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Courier New";
            this.textBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.StyleName = "Normal.TableBody";
            this.textBox25.TextWrap = false;
            this.textBox25.Value = "=Fields.ItemDescription";
            // 
            // textBox6
            // 
            this.textBox6.CanShrink = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.6481279730796814D), Telerik.Reporting.Drawing.Unit.Cm(0.5115128755569458D));
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Courier New";
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.StyleName = "Normal.TableBody";
            this.textBox6.Value = "=Fields.PrimaryUM";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2483105659484863D), Telerik.Reporting.Drawing.Unit.Cm(0.26458317041397095D));
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Courier New";
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox7.StyleName = "Normal.TableBody";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.4578285217285156D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Courier New";
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox8.StyleName = "Normal.TableBody";
            // 
            // shape1
            // 
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.882122278213501D), Telerik.Reporting.Drawing.Unit.Cm(0.26458317041397095D));
            this.shape1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.shape1.Style.Font.Bold = true;
            this.shape1.Style.Font.Name = "Courier New";
            this.shape1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.shape1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.shape1.StyleName = "Normal.TableBody";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.45782470703125D), Telerik.Reporting.Drawing.Unit.Cm(0.43555560708045959D));
            this.textBox22.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Courier New";
            this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox22.StyleName = "Normal.TableBody";
            // 
            // textBox2
            // 
            this.textBox2.CanShrink = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.882122278213501D), Telerik.Reporting.Drawing.Unit.Cm(0.43555554747581482D));
            this.textBox2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Courier New";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.StyleName = "Normal.TableBody";
            this.textBox2.TextWrap = false;
            this.textBox2.Value = "=Sum(Fields.PickedQuantityInPrimaryUOM)";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2483105659484863D), Telerik.Reporting.Drawing.Unit.Cm(0.43555554747581482D));
            this.textBox52.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Name = "Courier New";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox52.StyleName = "Normal.TableBody";
            this.textBox52.Value = "TOTAL";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(41D), Telerik.Reporting.Drawing.Unit.Cm(0.5115128755569458D));
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Courier New";
            this.textBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox30.StyleName = "Normal.TableBody";
            this.textBox30.Value = "=Fields.OrderQtyInPrimaryUOM";
            // 
            // textBox5
            // 
            this.textBox5.CanShrink = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(30.999975204467773D), Telerik.Reporting.Drawing.Unit.Cm(0.5115128755569458D));
            this.textBox5.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Courier New";
            this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.StyleName = "Normal.TableBody";
            this.textBox5.Value = "=Fields.PrimaryUM";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.52960646152496338D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Pixel(215D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10,
            this.textBox13,
            this.textBox23,
            this.textBox15,
            this.textBox14,
            this.textBox11,
            this.textBox43,
            this.textBox37,
            this.txt_InvoiceNo,
            this.textBox31,
            this.textBox19,
            this.textBox20,
            this.textBox17,
            this.textBox40,
            this.textBox47,
            this.textBox46,
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(3D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6200082302093506D), Telerik.Reporting.Drawing.Unit.Cm(0.24496442079544067D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox10.Style.Color = System.Drawing.Color.Black;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Courier New";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Value = "PICK SLIP FOR INVOICE#";
            // 
            // textBox13
            // 
            this.textBox13.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.8200099468231201D), Telerik.Reporting.Drawing.Unit.Cm(2.9449646472930908D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Courier New";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox13.Value = "BRANCH";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.2202105522155762D), Telerik.Reporting.Drawing.Unit.Cm(2.9449646472930908D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1997991800308228D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Courier New";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox23.TextWrap = false;
            this.textBox23.Value = "=Fields.Customer.RouteBranch";
            // 
            // textBox15
            // 
            this.textBox15.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.6415605545043945D), Telerik.Reporting.Drawing.Unit.Cm(2.9449646472930908D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000020503997803D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Courier New";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Value = "ACCOUNT / BILLING";
            // 
            // textBox14
            // 
            this.textBox14.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.5200099945068359D), Telerik.Reporting.Drawing.Unit.Cm(2.9449646472930908D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Courier New";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.TextWrap = false;
            this.textBox14.Value = "RT.";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.3202109336853027D), Telerik.Reporting.Drawing.Unit.Cm(2.9449646472930908D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3211501836776733D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Courier New";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.TextWrap = false;
            this.textBox11.Value = "=Fields.Route";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.5415639877319336D), Telerik.Reporting.Drawing.Unit.Cm(3.3899290561676025D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.699999213218689D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Courier New";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox43.TextWrap = false;
            this.textBox43.Value = "=Fields.Billing";
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.6200094223022461D), Telerik.Reporting.Drawing.Unit.Cm(3.3899292945861816D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5998011827468872D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Name = "Courier New";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox37.TextWrap = false;
            this.textBox37.Value = "=Fields.Customer.CustomerNo";
            // 
            // txt_InvoiceNo
            // 
            this.txt_InvoiceNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(3.3899290561676025D));
            this.txt_InvoiceNo.Name = "txt_InvoiceNo";
            this.txt_InvoiceNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1999931335449219D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.txt_InvoiceNo.Style.Font.Bold = true;
            this.txt_InvoiceNo.Style.Font.Name = "Courier New";
            this.txt_InvoiceNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txt_InvoiceNo.TextWrap = false;
            this.txt_InvoiceNo.Value = "=Fields.OrderNumber + \' SO\'";
            // 
            // textBox31
            // 
            this.textBox31.CanShrink = true;
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.7999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(4.6268033981323242D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0002012252807617D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Courier New";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox31.TextWrap = false;
            this.textBox31.Value = "=Fields.Customer.City + \' \'+ Fields.Customer.State +\' \'+ Fields.Customer.Zip";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8001999855041504D), Telerik.Reporting.Drawing.Unit.Cm(3.8268027305603027D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(0.30020016431808472D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Courier New";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox19.TextWrap = false;
            this.textBox19.Value = "=Fields.Customer.Name";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8002002239227295D), Telerik.Reporting.Drawing.Unit.Cm(4.2268028259277344D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(0.30020034313201904D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Courier New";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox20.TextWrap = false;
            this.textBox20.Value = "=Fields.Customer.Address";
            // 
            // textBox17
            // 
            this.textBox17.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.920008659362793D), Telerik.Reporting.Drawing.Unit.Cm(3.8268027305603027D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0997995138168335D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Courier New";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.Value = "DATE";
            // 
            // textBox40
            // 
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.040005683898926D), Telerik.Reporting.Drawing.Unit.Cm(3.8268027305603027D));
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.30020037293434143D));
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Name = "Courier New";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox40.Value = "=Fields.OrderDate";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.040005683898926D), Telerik.Reporting.Drawing.Unit.Cm(4.2268028259277344D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Courier New";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox47.Value = "{PageNumber} of {PageCount}";
            // 
            // textBox46
            // 
            this.textBox46.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.920008659362793D), Telerik.Reporting.Drawing.Unit.Cm(4.2268028259277344D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0997995138168335D), Telerik.Reporting.Drawing.Unit.Cm(0.30020010471343994D));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Name = "Courier New";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox46.Value = "PAGE";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.2200117111206055D), Telerik.Reporting.Drawing.Unit.Cm(3.3899290561676025D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.32135209441185D), Telerik.Reporting.Drawing.Unit.Cm(0.299999862909317D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Courier New";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox1.TextWrap = false;
            this.textBox1.Value = "/";
            // 
            // shape2
            // 
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(71.9999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.26458325982093811D));
            this.shape2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.shape2.Style.Font.Bold = true;
            this.shape2.Style.Font.Name = "Courier New";
            this.shape2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.shape2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.shape2.StyleName = "Normal.TableBody";
            // 
            // textBox3
            // 
            this.textBox3.CanShrink = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(71.9999771118164D), Telerik.Reporting.Drawing.Unit.Cm(0.43555560708045959D));
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Courier New";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.StyleName = "Normal.TableBody";
            this.textBox3.TextWrap = false;
            this.textBox3.Value = "=Sum(Fields.OrderQtyInPrimaryUOM)";
            // 
            // rPickOrderSlip
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "rptPickOrderSlip";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Mm(10D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.Name = "PrintLabels";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Boolean;
            this.ReportParameters.Add(reportParameter1);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.Table), "Normal.TableNormal")});
            styleRule2.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule2.Style.Color = System.Drawing.Color.Black;
            styleRule2.Style.Font.Name = "Tahoma";
            styleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            descendantSelector1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableHeader")});
            styleRule3.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector1});
            styleRule3.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule3.Style.Font.Name = "Tahoma";
            styleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            styleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            descendantSelector2.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.Table)),
            new Telerik.Reporting.Drawing.StyleSelector(typeof(Telerik.Reporting.ReportItem), "Normal.TableBody")});
            styleRule4.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            descendantSelector2});
            styleRule4.Style.BorderColor.Default = System.Drawing.Color.Black;
            styleRule4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            styleRule4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            styleRule4.Style.Font.Name = "Tahoma";
            styleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1,
            styleRule2,
            styleRule3,
            styleRule4});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Pixel(732D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox txt_InvoiceNo;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox3;
    }
}