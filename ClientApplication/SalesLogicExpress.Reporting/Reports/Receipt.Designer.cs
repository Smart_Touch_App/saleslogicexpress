namespace SalesLogicExpress.Reporting
{
    partial class Receipt
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Receipt));
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.ReportParameter reportParameter1 = new Telerik.Reporting.ReportParameter();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.txt_InvoiceNo = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.table17 = new Telerik.Reporting.Table();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.table24 = new Telerik.Reporting.Table();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.lbl_Surcharge = new Telerik.Reporting.TextBox();
            this.lbl_TotalCoffee = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.lbl_TaxAmount = new Telerik.Reporting.TextBox();
            this.lbl_TotalAllied = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.fld_TaxAmount = new Telerik.Reporting.TextBox();
            this.fld_surcharge = new Telerik.Reporting.TextBox();
            this.fld_TotalCoffee = new Telerik.Reporting.TextBox();
            this.fld_TotalAllied = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.table5 = new Telerik.Reporting.Table();
            this.table4 = new Telerik.Reporting.Table();
            this.table7 = new Telerik.Reporting.Table();
            this.table6 = new Telerik.Reporting.Table();
            this.table2 = new Telerik.Reporting.Table();
            this.table3 = new Telerik.Reporting.Table();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.table10 = new Telerik.Reporting.Table();
            this.table11 = new Telerik.Reporting.Table();
            this.table12 = new Telerik.Reporting.Table();
            this.table13 = new Telerik.Reporting.Table();
            this.table14 = new Telerik.Reporting.Table();
            this.table15 = new Telerik.Reporting.Table();
            this.table16 = new Telerik.Reporting.Table();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3736188411712646D), Telerik.Reporting.Drawing.Unit.Cm(0.46515378355979919D));
            this.textBox126.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox126.Style.Font.Bold = true;
            this.textBox126.Style.Font.Name = "Courier New";
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox126.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox126.StyleName = "Normal.TableHeader";
            this.textBox126.Value = "Invoice No";
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5045499801635742D), Telerik.Reporting.Drawing.Unit.Cm(0.46515378355979919D));
            this.textBox127.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox127.Style.Font.Bold = true;
            this.textBox127.Style.Font.Name = "Courier New";
            this.textBox127.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox127.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox127.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox127.StyleName = "Normal.TableHeader";
            this.textBox127.Value = "Invoice Date";
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7549562454223633D), Telerik.Reporting.Drawing.Unit.Cm(0.46515378355979919D));
            this.textBox128.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox128.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox128.Style.Color = System.Drawing.Color.Black;
            this.textBox128.Style.Font.Bold = true;
            this.textBox128.Style.Font.Name = "Courier New";
            this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox128.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox128.StyleName = "Corporate.TableHeader";
            this.textBox128.Value = "Amount";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147574901580811D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Courier New";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.StyleName = "Normal.TableHeader";
            this.textBox8.Value = "QUANTITY";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox50.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Courier New";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.StyleName = "Normal.TableHeader";
            this.textBox50.Value = "CODE";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox4.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox4.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox4.Style.Color = System.Drawing.Color.Black;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Courier New";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.StyleName = "Corporate.TableHeader";
            this.textBox4.Value = "PRODUCT DESCRIPTION";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox5.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox5.Style.Color = System.Drawing.Color.Black;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Courier New";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox5.StyleName = "Corporate.TableHeader";
            this.textBox5.Value = "U/PRICE";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox6.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox6.Style.Color = System.Drawing.Color.Black;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Courier New";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox6.StyleName = "Corporate.TableHeader";
            this.textBox6.Value = "AMOUNT";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147574901580811D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Courier New";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox30.StyleName = "Normal.TableHeader";
            this.textBox30.Value = "QUANTITY";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Courier New";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.StyleName = "Normal.TableHeader";
            this.textBox29.Value = "CODE";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox26.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox26.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox26.Style.Color = System.Drawing.Color.Black;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Courier New";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.StyleName = "Corporate.TableHeader";
            this.textBox26.Value = "PRODUCT DESCRIPTION";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox25.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox25.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox25.Style.Color = System.Drawing.Color.Black;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Courier New";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox25.StyleName = "Corporate.TableHeader";
            this.textBox25.Value = "U/PRICE";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox23.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox23.Style.Color = System.Drawing.Color.Black;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Courier New";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox23.StyleName = "Corporate.TableHeader";
            this.textBox23.Value = "AMOUNT";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Pixel(189.22158813476563D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox75,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox82,
            this.textBox83,
            this.textBox84,
            this.textBox85,
            this.txt_InvoiceNo,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.Font.Bold = true;
            this.pageHeaderSection1.Style.Font.Name = "Courier New";
            this.pageHeaderSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox75
            // 
            this.textBox75.CanShrink = true;
            this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.0006246566772461D), Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D));
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.5324993133544922D), Telerik.Reporting.Drawing.Unit.Cm(0.47688433527946472D));
            this.textBox75.Style.Font.Bold = true;
            this.textBox75.Style.Font.Name = "Courier New";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox75.TextWrap = false;
            this.textBox75.Value = "=Fields.Customer.City + \' \'+ Fields.Customer.State +\' \'+ Fields.Customer.Zip";
            // 
            // textBox77
            // 
            this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(3.3999993801116943D));
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8000047206878662D), Telerik.Reporting.Drawing.Unit.Cm(0.47648093104362488D));
            this.textBox77.Style.Font.Bold = true;
            this.textBox77.Style.Font.Name = "Courier New";
            this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox77.Value = "{PageNumber} of {PageCount}";
            // 
            // textBox78
            // 
            this.textBox78.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(3.3999993801116943D));
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4998009204864502D), Telerik.Reporting.Drawing.Unit.Cm(0.47688141465187073D));
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.Font.Name = "Courier New";
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox78.Value = "PAGE";
            // 
            // textBox79
            // 
            this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.0902082920074463D), Telerik.Reporting.Drawing.Unit.Cm(2.8000004291534424D));
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7701973915100098D), Telerik.Reporting.Drawing.Unit.Cm(0.60332518815994263D));
            this.textBox79.Style.Font.Bold = true;
            this.textBox79.Style.Font.Name = "Courier New";
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox79.Value = "(661)942-7827";
            // 
            // textBox80
            // 
            this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.300000190734863D), Telerik.Reporting.Drawing.Unit.Cm(2.1999998092651367D));
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.29979977011680603D), Telerik.Reporting.Drawing.Unit.Cm(0.50250846147537231D));
            this.textBox80.Style.Font.Bold = true;
            this.textBox80.Style.Font.Name = "Courier New";
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox80.Value = "/";
            // 
            // textBox81
            // 
            this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.59999942779541D), Telerik.Reporting.Drawing.Unit.Cm(2.1999998092651367D));
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9891674518585205D), Telerik.Reporting.Drawing.Unit.Cm(0.50250846147537231D));
            this.textBox81.Style.Font.Bold = true;
            this.textBox81.Style.Font.Name = "Courier New";
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox81.Value = "=Fields.Billing";
            // 
            // textBox82
            // 
            this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(2.8999998569488525D));
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5269830226898193D), Telerik.Reporting.Drawing.Unit.Cm(0.50020068883895874D));
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.Font.Name = "Courier New";
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox82.Value = "=Fields.OrderDate";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.3000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(2.1999998092651367D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9087600708007813D), Telerik.Reporting.Drawing.Unit.Cm(0.50250846147537231D));
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Name = "Courier New";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox83.Value = "=Fields.Customer.CustomerNo";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.5998001098632812D), Telerik.Reporting.Drawing.Unit.Cm(1.6066666841506958D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5788452625274658D), Telerik.Reporting.Drawing.Unit.Cm(0.49313324689865112D));
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.Font.Name = "Courier New";
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox84.Value = "=Fields.Route";
            // 
            // textBox85
            // 
            this.textBox85.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.6002001762390137D), Telerik.Reporting.Drawing.Unit.Cm(1.6066666841506958D));
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1995999813079834D), Telerik.Reporting.Drawing.Unit.Cm(0.49313324689865112D));
            this.textBox85.Style.Font.Bold = true;
            this.textBox85.Style.Font.Name = "Courier New";
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox85.Value = "=Fields.Customer.RouteBranch";
            // 
            // txt_InvoiceNo
            // 
            this.txt_InvoiceNo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15D), Telerik.Reporting.Drawing.Unit.Cm(2.0035417079925537D));
            this.txt_InvoiceNo.Name = "txt_InvoiceNo";
            this.txt_InvoiceNo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7269828319549561D), Telerik.Reporting.Drawing.Unit.Cm(0.69896668195724487D));
            this.txt_InvoiceNo.Style.Font.Bold = true;
            this.txt_InvoiceNo.Style.Font.Name = "Courier New";
            this.txt_InvoiceNo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.txt_InvoiceNo.Value = "=Fields.ObjOpenAR.ReceiptId";
            // 
            // textBox86
            // 
            this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.0006246566772461D), Telerik.Reporting.Drawing.Unit.Cm(2.8999998569488525D));
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(0.50020068883895874D));
            this.textBox86.Style.Font.Bold = true;
            this.textBox86.Style.Font.Name = "Courier New";
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox86.Value = "=Fields.Customer.Name";
            // 
            // textBox87
            // 
            this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.0006246566772461D), Telerik.Reporting.Drawing.Unit.Cm(3.3999996185302734D));
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(0.47688126564025879D));
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.Font.Name = "Courier New";
            this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox87.Value = "=Fields.Customer.Address";
            // 
            // textBox88
            // 
            this.textBox88.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox88.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.0902082920074463D), Telerik.Reporting.Drawing.Unit.Cm(2.1999998092651367D));
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(0.50250846147537231D));
            this.textBox88.Style.Font.Bold = true;
            this.textBox88.Style.Font.Name = "Courier New";
            this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox88.Value = "CALL FOR SERVICE";
            // 
            // textBox89
            // 
            this.textBox89.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox89.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(2.8999998569488525D));
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0997995138168335D), Telerik.Reporting.Drawing.Unit.Cm(0.50020080804824829D));
            this.textBox89.Style.Font.Bold = true;
            this.textBox89.Style.Font.Name = "Courier New";
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox89.Value = "DATE";
            // 
            // textBox90
            // 
            this.textBox90.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox90.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.578540802001953D), Telerik.Reporting.Drawing.Unit.Cm(1.6066666841506958D));
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.499800443649292D), Telerik.Reporting.Drawing.Unit.Cm(0.37021681666374207D));
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Name = "Courier New";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox90.Value = "Receipt";
            // 
            // textBox91
            // 
            this.textBox91.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox91.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.5D), Telerik.Reporting.Drawing.Unit.Cm(1.6066666841506958D));
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000020503997803D), Telerik.Reporting.Drawing.Unit.Cm(0.49313324689865112D));
            this.textBox91.Style.Font.Bold = true;
            this.textBox91.Style.Font.Name = "Courier New";
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox91.Value = "ACCOUNT/BILLING";
            // 
            // textBox92
            // 
            this.textBox92.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox92.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.7999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(1.6066666841506958D));
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79960024356842041D), Telerik.Reporting.Drawing.Unit.Cm(0.49313324689865112D));
            this.textBox92.Style.Font.Bold = true;
            this.textBox92.Style.Font.Name = "Courier New";
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox92.Value = "RT.";
            // 
            // textBox93
            // 
            this.textBox93.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.1999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(1.6066666841506958D));
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Cm(0.49313336610794067D));
            this.textBox93.Style.Font.Bold = true;
            this.textBox93.Style.Font.Name = "Courier New";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox93.Value = "BRANCH";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Pixel(283.21939086914062D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox94,
            this.textBox95,
            this.textBox96,
            this.pictureBox7,
            this.table17,
            this.table24,
            this.textBox76,
            this.textBox98,
            this.textBox108,
            this.textBox109,
            this.textBox110,
            this.textBox100});
            this.detail.Name = "detail";
            this.detail.Style.Font.Bold = true;
            this.detail.Style.Font.Name = "Courier New";
            this.detail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox94
            // 
            this.textBox94.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(6.7236185073852539D));
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(21.404478073120117D));
            this.textBox94.Style.Font.Bold = true;
            this.textBox94.Style.Font.Name = "Courier New";
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox94.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox94.StyleName = "";
            this.textBox94.Value = "=Fields.RemitToCityZip";
            // 
            // textBox95
            // 
            this.textBox95.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(6.22361946105957D));
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(18.890018463134766D));
            this.textBox95.Style.Font.Bold = true;
            this.textBox95.Style.Font.Name = "Courier New";
            this.textBox95.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox95.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox95.StyleName = "";
            this.textBox95.Value = "=Fields.RemitToPOBox";
            // 
            // textBox96
            // 
            this.textBox96.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(5.7370834350585938D));
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(18.381221771240234D));
            this.textBox96.Style.Font.Bold = true;
            this.textBox96.Style.Font.Name = "Courier New";
            this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox96.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox96.StyleName = "";
            this.textBox96.Value = "=Fields.RemitToName";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Bindings.Add(new Telerik.Reporting.Binding("Value", "=Fields.LogoUrl"));
            this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(535D), Telerik.Reporting.Drawing.Unit.Pixel(181.172119140625D));
            this.pictureBox7.MimeType = "";
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(138.1544189453125D), Telerik.Reporting.Drawing.Unit.Pixel(102.0472412109375D));
            this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox7.Style.Font.Bold = true;
            this.pictureBox7.Style.Font.Name = "Courier New";
            this.pictureBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.pictureBox7.Value = "";
            // 
            // table17
            // 
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.46054762601852417D)));
            this.table17.Body.SetCellContent(0, 0, this.textBox103);
            tableGroup1.Name = "tableGroup3";
            this.table17.ColumnGroups.Add(tableGroup1);
            this.table17.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox103});
            this.table17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.0735418796539307D), Telerik.Reporting.Drawing.Unit.Cm(5.2236189842224121D));
            this.table17.Name = "table17";
            tableGroup3.Name = "group3";
            tableGroup2.ChildGroups.Add(tableGroup3);
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detailTableGroup1";
            this.table17.RowGroups.Add(tableGroup2);
            this.table17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.46054762601852417D));
            this.table17.Style.Font.Bold = true;
            this.table17.Style.Font.Name = "Courier New";
            this.table17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox103
            // 
            this.textBox103.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.46054762601852417D));
            this.textBox103.Style.Font.Bold = true;
            this.textBox103.Style.Font.Name = "Courier New";
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox103.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox103.Value = "PAID TO : ";
            // 
            // table24
            // 
            this.table24.Anchoring = Telerik.Reporting.AnchoringStyles.Top;
            this.table24.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(33.736179351806641D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(45.045490264892578D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(27.5495662689209D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.46515339612960815D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.66145831346511841D)));
            this.table24.Body.SetCellContent(0, 2, this.textBox111);
            this.table24.Body.SetCellContent(0, 1, this.textBox125);
            this.table24.Body.SetCellContent(0, 0, this.textBox114);
            this.table24.Body.SetCellContent(1, 0, this.textBox99, 1, 2);
            this.table24.Body.SetCellContent(1, 2, this.textBox102);
            tableGroup5.Name = "group5";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ReportItem = this.textBox126;
            tableGroup6.ReportItem = this.textBox127;
            tableGroup7.ReportItem = this.textBox128;
            this.table24.ColumnGroups.Add(tableGroup4);
            this.table24.ColumnGroups.Add(tableGroup6);
            this.table24.ColumnGroups.Add(tableGroup7);
            this.table24.ColumnHeadersPrintOnEveryPage = true;
            this.table24.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox111,
            this.textBox125,
            this.textBox114,
            this.textBox99,
            this.textBox102,
            this.textBox126,
            this.textBox127,
            this.textBox128});
            this.table24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.table24.Name = "table24";
            tableGroup9.Name = "group9";
            tableGroup8.ChildGroups.Add(tableGroup9);
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup8.Name = "Detail";
            tableGroup11.Name = "group7";
            tableGroup10.ChildGroups.Add(tableGroup11);
            tableGroup10.Name = "group4";
            this.table24.RowGroups.Add(tableGroup8);
            this.table24.RowGroups.Add(tableGroup10);
            this.table24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(106.33124542236328D), Telerik.Reporting.Drawing.Unit.Cm(1.591765284538269D));
            this.table24.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table24.Style.Font.Bold = true;
            this.table24.Style.Font.Name = "Courier New";
            this.table24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.table24.Style.LineColor = System.Drawing.Color.Transparent;
            this.table24.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.table24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.table24.StyleName = "";
            this.table24.ItemDataBinding += new System.EventHandler(this.table1_ItemDataBinding);
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7549562454223633D), Telerik.Reporting.Drawing.Unit.Cm(0.46515378355979919D));
            this.textBox111.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox111.Style.Font.Bold = true;
            this.textBox111.Style.Font.Name = "Courier New";
            this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox111.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox111.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox111.StyleName = "Corporate.TableBody";
            this.textBox111.Value = "=Format(\"{0:0.00}\", Fields.OpenAmountInvDtl)";
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5045499801635742D), Telerik.Reporting.Drawing.Unit.Cm(0.46515378355979919D));
            this.textBox125.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox125.Style.Font.Bold = true;
            this.textBox125.Style.Font.Name = "Courier New";
            this.textBox125.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox125.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox125.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox125.StyleName = "Normal.TableBody";
            this.textBox125.Value = "=Fields.DateInvDtl";
            // 
            // textBox114
            // 
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3736188411712646D), Telerik.Reporting.Drawing.Unit.Cm(0.46515378355979919D));
            this.textBox114.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox114.Style.Font.Bold = true;
            this.textBox114.Style.Font.Name = "Courier New";
            this.textBox114.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox114.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox114.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox114.StyleName = "Normal.TableBody";
            this.textBox114.Value = "=Fields.InvoiceNoInvDtl";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(78.78167724609375D), Telerik.Reporting.Drawing.Unit.Pixel(24.999992370605469D));
            this.textBox99.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.Font.Name = "Courier New";
            this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox99.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox99.StyleName = "Normal.TableBody";
            this.textBox99.Value = "Total Invoice Amount";
            // 
            // textBox102
            // 
            this.textBox102.CanShrink = true;
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(27.5495662689209D), Telerik.Reporting.Drawing.Unit.Pixel(24.999996185302734D));
            this.textBox102.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox102.Style.Font.Bold = true;
            this.textBox102.Style.Font.Name = "Courier New";
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox102.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox102.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox102.StyleName = "Corporate.TableBody";
            this.textBox102.TextWrap = false;
            this.textBox102.Value = "=Format(\"{0:0.00}\",Sum(Fields.OpenAmountInvDtl))";
            // 
            // textBox76
            // 
            this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.7999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(3.4238190650939941D));
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.233123779296875D), Telerik.Reporting.Drawing.Unit.Pixel(25.940273284912109D));
            this.textBox76.Style.Font.Bold = true;
            this.textBox76.Style.Font.Name = "Courier New";
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox76.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "=Fields.ObjOpenAR.PaymentInfo";
            // 
            // textBox98
            // 
            this.textBox98.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9156241416931152D), Telerik.Reporting.Drawing.Unit.Cm(1.6238192319869995D));
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(79.142219543457031D), Telerik.Reporting.Drawing.Unit.Pixel(25.210603713989258D));
            this.textBox98.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox98.Style.Font.Bold = true;
            this.textBox98.Style.Font.Name = "Courier New";
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox98.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox98.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox98.StyleName = "";
            this.textBox98.Value = "Total UnApplied Amount";
            // 
            // textBox108
            // 
            this.textBox108.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.830046653747559D), Telerik.Reporting.Drawing.Unit.Cm(1.6238192319869995D));
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(102.17112731933594D), Telerik.Reporting.Drawing.Unit.Pixel(25.210603713989258D));
            this.textBox108.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox108.Style.Font.Bold = true;
            this.textBox108.Style.Font.Name = "Courier New";
            this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox108.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox108.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox108.StyleName = "Corporate.TableBody";
            this.textBox108.Value = "= Fields.ObjOpenAR.UnapplidAmount";
            // 
            // textBox109
            // 
            this.textBox109.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9156241416931152D), Telerik.Reporting.Drawing.Unit.Cm(2.2910499572753906D));
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(79.142219543457031D), Telerik.Reporting.Drawing.Unit.Pixel(26.261045455932617D));
            this.textBox109.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox109.Style.Font.Bold = true;
            this.textBox109.Style.Font.Name = "Courier New";
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox109.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox109.StyleName = "";
            this.textBox109.Value = "Amount Paid";
            // 
            // textBox110
            // 
            this.textBox110.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.830046653747559D), Telerik.Reporting.Drawing.Unit.Cm(2.2910499572753906D));
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(27.032777786254883D), Telerik.Reporting.Drawing.Unit.Pixel(26.261045455932617D));
            this.textBox110.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox110.Style.Font.Bold = true;
            this.textBox110.Style.Font.Name = "Courier New";
            this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox110.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox110.StyleName = "";
            this.textBox110.Value = "=Format(\"{0:0.00}\",Fields.ObjOpenAR.PaymentAmount)";
            // 
            // textBox100
            // 
            this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9156241416931152D), Telerik.Reporting.Drawing.Unit.Cm(3.4238195419311523D));
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8401045799255371D), Telerik.Reporting.Drawing.Unit.Pixel(25.940273284912109D));
            this.textBox100.Style.Font.Bold = true;
            this.textBox100.Style.Font.Name = "Courier New";
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox100.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox100.StyleName = "";
            this.textBox100.Value = "Payment Mode :";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3.2000002861022949D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox97,
            this.textBox105,
            this.textBox104});
            this.pageFooterSection1.Name = "pageFooterSection1";
            this.pageFooterSection1.Style.Font.Bold = true;
            this.pageFooterSection1.Style.Font.Name = "Courier New";
            this.pageFooterSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox97
            // 
            this.textBox97.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.631458282470703D), Telerik.Reporting.Drawing.Unit.Cm(1.5999994277954102D));
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6712546348571777D), Telerik.Reporting.Drawing.Unit.Cm(0.39979955554008484D));
            this.textBox97.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox97.Style.Font.Bold = true;
            this.textBox97.Style.Font.Name = "Courier New";
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox97.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox97.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox97.Value = "= Now().ToString(\"MM/dd/yyyy hh:mm:ss \") + Now().ToString(\"tt\").ToLower()";
            // 
            // textBox105
            // 
            this.textBox105.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox105.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox105.Style.Font.Bold = false;
            this.textBox105.Style.Font.Name = "Courier New";
            this.textBox105.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox105.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox105.Value = "=Fields.VisitUsUrl";
            // 
            // textBox104
            // 
            this.textBox104.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox104.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.777082443237305D), Telerik.Reporting.Drawing.Unit.Cm(1.4000000953674316D));
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1227166652679443D), Telerik.Reporting.Drawing.Unit.Cm(0.61146622896194458D));
            this.textBox104.Style.Font.Bold = true;
            this.textBox104.Style.Font.Name = "Courier New";
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox104.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox104.Value = "Received By :";
            // 
            // textBox41
            // 
            this.textBox41.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Name = "Courier New";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox41.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox41.Value = "REMIT TO : ";
            // 
            // lbl_Surcharge
            // 
            this.lbl_Surcharge.Name = "lbl_Surcharge";
            this.lbl_Surcharge.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.lbl_Surcharge.Style.Font.Bold = true;
            this.lbl_Surcharge.Style.Font.Name = "Courier New";
            this.lbl_Surcharge.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.lbl_Surcharge.Value = "SURCHARGE";
            // 
            // lbl_TotalCoffee
            // 
            this.lbl_TotalCoffee.Name = "lbl_TotalCoffee";
            this.lbl_TotalCoffee.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.lbl_TotalCoffee.Style.Font.Bold = true;
            this.lbl_TotalCoffee.Style.Font.Name = "Courier New";
            this.lbl_TotalCoffee.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.lbl_TotalCoffee.Value = "TOTAL COFFEE";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Courier New";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox38.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox38.Value = "TOTAL DUE -------->>";
            // 
            // lbl_TaxAmount
            // 
            this.lbl_TaxAmount.Name = "lbl_TaxAmount";
            this.lbl_TaxAmount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.lbl_TaxAmount.Style.Font.Bold = true;
            this.lbl_TaxAmount.Style.Font.Name = "Courier New";
            this.lbl_TaxAmount.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.lbl_TaxAmount.Value = "TAX AMOUNT";
            // 
            // lbl_TotalAllied
            // 
            this.lbl_TotalAllied.Name = "lbl_TotalAllied";
            this.lbl_TotalAllied.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.lbl_TotalAllied.Style.Font.Bold = true;
            this.lbl_TotalAllied.Style.Font.Name = "Courier New";
            this.lbl_TotalAllied.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.lbl_TotalAllied.Value = "TOTAL ALLIED";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864589214324951D), Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D));
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Courier New";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox28.Value = "TERMS OF SALES:";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864589214324951D), Telerik.Reporting.Drawing.Unit.Pixel(18.897640228271484D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Courier New";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox22.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Courier New";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.StyleName = "Normal.TableBody";
            this.textBox3.Value = "=Fields.ItemNumber";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Courier New";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.StyleName = "Normal.TableBody";
            this.textBox1.Value = "TOTAL QTY";
            // 
            // textBox34
            // 
            this.textBox34.CanShrink = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147574901580811D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Courier New";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox34.StyleName = "Normal.TableBody";
            this.textBox34.TextWrap = false;
            this.textBox34.Value = "=Sum(Fields.OrderQty)";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox61.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox61.Style.Font.Bold = true;
            this.textBox61.Style.Font.Name = "Courier New";
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox61.StyleName = "Corporate.TableBody";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox60.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Name = "Courier New";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox60.StyleName = "Corporate.TableBody";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox59.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Name = "Courier New";
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox59.StyleName = "Corporate.TableBody";
            // 
            // shape1
            // 
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147574901580811D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.shape1.Style.BorderColor.Default = System.Drawing.Color.White;
            this.shape1.Style.Font.Bold = true;
            this.shape1.Style.Font.Name = "Courier New";
            this.shape1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.shape1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.shape1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.shape1.StyleName = "Normal.TableBody";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox57.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Name = "Courier New";
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox57.StyleName = "Corporate.TableBody";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox56.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.Font.Name = "Courier New";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox56.StyleName = "Corporate.TableBody";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox55.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Name = "Courier New";
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox55.StyleName = "Corporate.TableBody";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox53.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox53.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Name = "Courier New";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox53.StyleName = "Corporate.TableBody";
            // 
            // textBox7
            // 
            this.textBox7.CanShrink = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(27.197122573852539D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Courier New";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.StyleName = "Normal.TableBody";
            this.textBox7.Value = "=Fields.UM";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1951669454574585D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox48.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Courier New";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox48.StyleName = "Normal.TableBody";
            this.textBox48.Value = "=Fields.OrderQty";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Courier New";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox12.StyleName = "Corporate.TableBody";
            this.textBox12.Value = "= Fields.ExtendedPrice";
            this.textBox12.ItemDataBinding += new System.EventHandler(this.textBox12_ItemDataBinding);
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox11.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Courier New";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox11.StyleName = "Corporate.TableBody";
            this.textBox11.Value = "= Fields.UnitPrice";
            this.textBox11.ItemDataBinding += new System.EventHandler(this.textBox11_ItemDataBinding);
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Courier New";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox10.StyleName = "Corporate.TableBody";
            this.textBox10.Value = "= Fields.ItemDescription";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.6786456108093262D), Telerik.Reporting.Drawing.Unit.Cm(0.90000003576278687D));
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7997972965240479D), Telerik.Reporting.Drawing.Unit.Cm(1.2001990079879761D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox2.Style.Font.Bold = true;
            this.pictureBox2.Style.Font.Name = "Courier New";
            this.pictureBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // pictureBox1
            // 
            this.pictureBox1.Bindings.Add(new Telerik.Reporting.Binding("Value", "=Fields.CustomerSignImagePath"));
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.378646850585938D), Telerik.Reporting.Drawing.Unit.Cm(0.89979970455169678D));
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7997972965240479D), Telerik.Reporting.Drawing.Unit.Cm(1.2001990079879761D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox1.Style.Font.Bold = true;
            this.pictureBox1.Style.Font.Name = "Courier New";
            this.pictureBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.378645896911621D), Telerik.Reporting.Drawing.Unit.Cm(2.1002001762390137D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6712546348571777D), Telerik.Reporting.Drawing.Unit.Cm(0.39979955554008484D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Name = "Courier New";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox51.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox51.Value = "= Now().ToString(\"MM/dd/yyyy hh:mm:ss \") + Now().ToString(\"tt\").ToLower()";
            // 
            // textBox49
            // 
            this.textBox49.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.578644752502441D), Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.800001859664917D), Telerik.Reporting.Drawing.Unit.Cm(0.29999944567680359D));
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Font.Name = "Courier New";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox49.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox49.Value = "CUSTOMER";
            // 
            // textBox2
            // 
            this.textBox2.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.87864571809768677D), Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79979968070983887D), Telerik.Reporting.Drawing.Unit.Cm(0.29999944567680359D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Courier New";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox2.Value = "REP";
            // 
            // textBox21
            // 
            this.textBox21.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.878645658493042D), Telerik.Reporting.Drawing.Unit.Cm(0.1000002920627594D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Courier New";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Value = "=Fields.VisitUsUrl";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(7.1235337257385254D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(17.999998092651367D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Courier New";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox33.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "=Fields.RemitToCityZip";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Cm(6.7000002861022949D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(16.000003814697266D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Courier New";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox27.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "=Fields.RemitToPOBox";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(6.3000006675720215D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(14.215662956237793D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Courier New";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox24.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "=Fields.RemitToName";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(254.99996948242188D), Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Courier New";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.TermOfSales";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Bindings.Add(new Telerik.Reporting.Binding("Value", "=Fields.LogoUrl"));
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(510.23629760742188D), Telerik.Reporting.Drawing.Unit.Pixel(211.653564453125D));
            this.pictureBox3.MimeType = "";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(138.1544189453125D), Telerik.Reporting.Drawing.Unit.Pixel(102.0472412109375D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Style.Font.Bold = true;
            this.pictureBox3.Style.Font.Name = "Courier New";
            this.pictureBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.pictureBox3.Value = "";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(5.100001335144043D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0784449577331543D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Name = "Courier New";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox39.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox39.Value = "=Fields.TotalDue";
            // 
            // fld_TaxAmount
            // 
            this.fld_TaxAmount.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13D), Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D));
            this.fld_TaxAmount.Name = "fld_TaxAmount";
            this.fld_TaxAmount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1784439086914062D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.fld_TaxAmount.Style.Font.Bold = true;
            this.fld_TaxAmount.Style.Font.Name = "Courier New";
            this.fld_TaxAmount.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.fld_TaxAmount.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_TaxAmount.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_TaxAmount.Value = "=Fields.TaxAmount";
            // 
            // fld_surcharge
            // 
            this.fld_surcharge.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.000200271606445D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.fld_surcharge.Name = "fld_surcharge";
            this.fld_surcharge.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1782426834106445D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.fld_surcharge.Style.Font.Bold = true;
            this.fld_surcharge.Style.Font.Name = "Courier New";
            this.fld_surcharge.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.fld_surcharge.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_surcharge.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_surcharge.Value = "=Fields.EnergySurcharge";
            // 
            // fld_TotalCoffee
            // 
            this.fld_TotalCoffee.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.000199317932129D), Telerik.Reporting.Drawing.Unit.Cm(3.8999996185302734D));
            this.fld_TotalCoffee.Name = "fld_TotalCoffee";
            this.fld_TotalCoffee.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1782450675964355D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.fld_TotalCoffee.Style.Font.Bold = true;
            this.fld_TotalCoffee.Style.Font.Name = "Courier New";
            this.fld_TotalCoffee.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.fld_TotalCoffee.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_TotalCoffee.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_TotalCoffee.Value = "=Fields.TotalCoffee";
            // 
            // fld_TotalAllied
            // 
            this.fld_TotalAllied.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.927868843078613D), Telerik.Reporting.Drawing.Unit.Cm(3.5000004768371582D));
            this.fld_TotalAllied.Name = "fld_TotalAllied";
            this.fld_TotalAllied.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.250575065612793D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.fld_TotalAllied.Style.Font.Bold = true;
            this.fld_TotalAllied.Style.Font.Name = "Courier New";
            this.fld_TotalAllied.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.fld_TotalAllied.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.fld_TotalAllied.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.fld_TotalAllied.Value = "=Fields.TotalAllied";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox41);
            tableGroup12.Name = "tableGroup3";
            this.table8.ColumnGroups.Add(tableGroup12);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox41});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0786454677581787D), Telerik.Reporting.Drawing.Unit.Cm(6.0000009536743164D));
            this.table8.Name = "table8";
            tableGroup14.Name = "group3";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "detailTableGroup1";
            this.table8.RowGroups.Add(tableGroup13);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D));
            this.table8.Style.Font.Bold = true;
            this.table8.Style.Font.Name = "Courier New";
            this.table8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D)));
            this.table5.Body.SetCellContent(0, 0, this.lbl_Surcharge);
            tableGroup15.Name = "tableGroup3";
            this.table5.ColumnGroups.Add(tableGroup15);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_Surcharge});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(4.30000114440918D));
            this.table5.Name = "table5";
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "detailTableGroup1";
            this.table5.RowGroups.Add(tableGroup16);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.table5.Style.Font.Bold = true;
            this.table5.Style.Font.Name = "Courier New";
            this.table5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D)));
            this.table4.Body.SetCellContent(0, 0, this.lbl_TotalCoffee);
            tableGroup17.Name = "tableGroup3";
            this.table4.ColumnGroups.Add(tableGroup17);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_TotalCoffee});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(3.8995990753173828D));
            this.table4.Name = "table4";
            tableGroup18.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup18.Name = "detailTableGroup1";
            this.table4.RowGroups.Add(tableGroup18);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.table4.Style.Font.Bold = true;
            this.table4.Style.Font.Name = "Courier New";
            this.table4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox38);
            tableGroup19.Name = "tableGroup3";
            this.table7.ColumnGroups.Add(tableGroup19);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox38});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(5.1000008583068848D));
            this.table7.Name = "table7";
            tableGroup20.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup20.Name = "detailTableGroup1";
            this.table7.RowGroups.Add(tableGroup20);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.table7.Style.Font.Bold = true;
            this.table7.Style.Font.Name = "Courier New";
            this.table7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D)));
            this.table6.Body.SetCellContent(0, 0, this.lbl_TaxAmount);
            tableGroup21.Name = "tableGroup3";
            this.table6.ColumnGroups.Add(tableGroup21);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_TaxAmount});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(4.7000012397766113D));
            this.table6.Name = "table6";
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup22.Name = "detailTableGroup1";
            this.table6.RowGroups.Add(tableGroup22);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.table6.Style.Font.Bold = true;
            this.table6.Style.Font.Name = "Courier New";
            this.table6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D)));
            this.table2.Body.SetCellContent(0, 0, this.lbl_TotalAllied);
            tableGroup23.Name = "tableGroup3";
            this.table2.ColumnGroups.Add(tableGroup23);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lbl_TotalAllied});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.0998983383178711D), Telerik.Reporting.Drawing.Unit.Cm(3.5000009536743164D));
            this.table2.Name = "table2";
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup24.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup24);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.table2.Style.Font.Bold = true;
            this.table2.Style.Font.Name = "Courier New";
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0864591598510742D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table3.Body.SetCellContent(1, 0, this.textBox22);
            this.table3.Body.SetCellContent(0, 0, this.textBox28);
            tableGroup25.Name = "tableGroup1";
            this.table3.ColumnGroups.Add(tableGroup25);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox22,
            this.textBox28});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D));
            this.table3.Name = "table3";
            tableGroup27.Name = "group";
            tableGroup28.Name = "group1";
            tableGroup26.ChildGroups.Add(tableGroup27);
            tableGroup26.ChildGroups.Add(tableGroup28);
            tableGroup26.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup26.Name = "detailTableGroup";
            this.table3.RowGroups.Add(tableGroup26);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864591598510742D), Telerik.Reporting.Drawing.Unit.Cm(0.82305389642715454D));
            this.table3.Style.Font.Bold = true;
            this.table3.Style.Font.Name = "Courier New";
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table1
            // 
            this.table1.Anchoring = Telerik.Reporting.AnchoringStyles.Top;
            this.table1.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(11.951668739318848D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(7.19590425491333D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(22.9813232421875D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(70.412887573242188D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(21.822998046875D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(28.633176803588867D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458320021629333D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.370417058467865D)));
            this.table1.Body.SetCellContent(0, 3, this.textBox10);
            this.table1.Body.SetCellContent(0, 4, this.textBox11);
            this.table1.Body.SetCellContent(0, 5, this.textBox12);
            this.table1.Body.SetCellContent(0, 0, this.textBox48);
            this.table1.Body.SetCellContent(0, 1, this.textBox7);
            this.table1.Body.SetCellContent(1, 2, this.textBox53);
            this.table1.Body.SetCellContent(1, 3, this.textBox55);
            this.table1.Body.SetCellContent(1, 4, this.textBox56);
            this.table1.Body.SetCellContent(1, 5, this.textBox57);
            this.table1.Body.SetCellContent(1, 0, this.shape1, 1, 2);
            this.table1.Body.SetCellContent(2, 3, this.textBox59);
            this.table1.Body.SetCellContent(2, 4, this.textBox60);
            this.table1.Body.SetCellContent(2, 5, this.textBox61);
            this.table1.Body.SetCellContent(2, 0, this.textBox34, 1, 2);
            this.table1.Body.SetCellContent(2, 2, this.textBox1);
            this.table1.Body.SetCellContent(0, 2, this.textBox3);
            tableGroup30.Name = "group5";
            tableGroup31.Name = "group2";
            tableGroup29.ChildGroups.Add(tableGroup30);
            tableGroup29.ChildGroups.Add(tableGroup31);
            tableGroup29.ReportItem = this.textBox8;
            tableGroup32.ReportItem = this.textBox50;
            tableGroup33.ReportItem = this.textBox4;
            tableGroup34.ReportItem = this.textBox5;
            tableGroup35.ReportItem = this.textBox6;
            this.table1.ColumnGroups.Add(tableGroup29);
            this.table1.ColumnGroups.Add(tableGroup32);
            this.table1.ColumnGroups.Add(tableGroup33);
            this.table1.ColumnGroups.Add(tableGroup34);
            this.table1.ColumnGroups.Add(tableGroup35);
            this.table1.ColumnHeadersPrintOnEveryPage = true;
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox48,
            this.textBox7,
            this.textBox53,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.shape1,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox34,
            this.textBox1,
            this.textBox3,
            this.textBox8,
            this.textBox50,
            this.textBox4,
            this.textBox5,
            this.textBox6});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.878645658493042D), Telerik.Reporting.Drawing.Unit.Cm(-1.2616315814284462E-07D));
            this.table1.Name = "table1";
            tableGroup36.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup36.Name = "Detail";
            tableGroup37.Name = "group6";
            this.table1.RowGroups.Add(tableGroup36);
            this.table1.RowGroups.Add(tableGroup37);
            this.table1.Style.Font.Bold = true;
            this.table1.Style.Font.Name = "Courier New";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox13
            // 
            this.textBox13.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Courier New";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox13.Value = "REMIT TO : ";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Courier New";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Value = "SURCHARGE";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Courier New";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Value = "TOTAL COFFEE";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Courier New";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox16.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox16.Value = "TOTAL DUE -------->>";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Courier New";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox17.Value = "TAX AMOUNT";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Courier New";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Value = "TOTAL ALLIED";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864589214324951D), Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Courier New";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox19.Value = "TERMS OF SALES:";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864589214324951D), Telerik.Reporting.Drawing.Unit.Pixel(18.897640228271484D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Courier New";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox20.StyleName = "";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox31.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Courier New";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox31.StyleName = "Normal.TableBody";
            this.textBox31.Value = "=Fields.ItemNumber";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox32.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Courier New";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.StyleName = "Normal.TableBody";
            this.textBox32.Value = "TOTAL QTY";
            // 
            // textBox35
            // 
            this.textBox35.CanShrink = true;
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147574901580811D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox35.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Name = "Courier New";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox35.StyleName = "Normal.TableBody";
            this.textBox35.TextWrap = false;
            this.textBox35.Value = "=Sum(Fields.OrderQty)";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox36.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Courier New";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox36.StyleName = "Corporate.TableBody";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Name = "Courier New";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox37.StyleName = "Corporate.TableBody";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Pixel(14.000010490417481D));
            this.textBox40.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Name = "Courier New";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox40.StyleName = "Corporate.TableBody";
            // 
            // shape2
            // 
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9147574901580811D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.shape2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.shape2.Style.Font.Bold = true;
            this.shape2.Style.Font.Name = "Courier New";
            this.shape2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.shape2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.shape2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.shape2.StyleName = "Normal.TableBody";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox42.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Name = "Courier New";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox42.StyleName = "Corporate.TableBody";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox43.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Courier New";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox43.StyleName = "Corporate.TableBody";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox44.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox44.Style.Font.Bold = true;
            this.textBox44.Style.Font.Name = "Courier New";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox44.StyleName = "Corporate.TableBody";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2981324195861816D), Telerik.Reporting.Drawing.Unit.Pixel(9.9999961853027344D));
            this.textBox45.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox45.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Courier New";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox45.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox45.StyleName = "Corporate.TableBody";
            // 
            // textBox46
            // 
            this.textBox46.CanShrink = true;
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(27.197122573852539D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox46.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Name = "Courier New";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox46.StyleName = "Normal.TableBody";
            this.textBox46.Value = "=Fields.UM";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1951669454574585D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox47.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Courier New";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox47.StyleName = "Normal.TableBody";
            this.textBox47.Value = "=Fields.OrderQty";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8633177280426025D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox52.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Name = "Courier New";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox52.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox52.StyleName = "Corporate.TableBody";
            this.textBox52.Value = "= Fields.ExtendedPrice";
            this.textBox52.ItemDataBinding += new System.EventHandler(this.textBox12_ItemDataBinding);
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1822996139526367D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox54.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Font.Name = "Courier New";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox54.StyleName = "Corporate.TableBody";
            this.textBox54.Value = "= Fields.UnitPrice";
            this.textBox54.ItemDataBinding += new System.EventHandler(this.textBox11_ItemDataBinding);
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0412893295288086D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox58.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.Font.Name = "Courier New";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox58.StyleName = "Corporate.TableBody";
            this.textBox58.Value = "= Fields.ItemDescription";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.6786456108093262D), Telerik.Reporting.Drawing.Unit.Cm(0.90000003576278687D));
            this.pictureBox4.MimeType = "image/png";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7997972965240479D), Telerik.Reporting.Drawing.Unit.Cm(1.2001990079879761D));
            this.pictureBox4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox4.Style.Font.Bold = true;
            this.pictureBox4.Style.Font.Name = "Courier New";
            this.pictureBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // pictureBox5
            // 
            this.pictureBox5.Bindings.Add(new Telerik.Reporting.Binding("Value", "=Fields.CustomerSignImagePath"));
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.378646850585938D), Telerik.Reporting.Drawing.Unit.Cm(0.89979970455169678D));
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7997972965240479D), Telerik.Reporting.Drawing.Unit.Cm(1.2001990079879761D));
            this.pictureBox5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox5.Style.Font.Bold = true;
            this.pictureBox5.Style.Font.Name = "Courier New";
            this.pictureBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.378645896911621D), Telerik.Reporting.Drawing.Unit.Cm(2.1002001762390137D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6712546348571777D), Telerik.Reporting.Drawing.Unit.Cm(0.39979955554008484D));
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Name = "Courier New";
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox62.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox62.Value = "= Now().ToString(\"MM/dd/yyyy hh:mm:ss \") + Now().ToString(\"tt\").ToLower()";
            // 
            // textBox63
            // 
            this.textBox63.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.578644752502441D), Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.800001859664917D), Telerik.Reporting.Drawing.Unit.Cm(0.29999944567680359D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.Font.Name = "Courier New";
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox63.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox63.Value = "CUSTOMER";
            // 
            // textBox64
            // 
            this.textBox64.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.87864571809768677D), Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79979968070983887D), Telerik.Reporting.Drawing.Unit.Cm(0.29999944567680359D));
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.Font.Name = "Courier New";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox64.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox64.Value = "REP";
            // 
            // textBox65
            // 
            this.textBox65.Bindings.Add(new Telerik.Reporting.Binding("Visible", "= Parameters.PrintLabels.Value"));
            this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.878645658493042D), Telerik.Reporting.Drawing.Unit.Cm(0.1000002920627594D));
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.Font.Name = "Courier New";
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox65.Value = "=Fields.VisitUsUrl";
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(7.1235337257385254D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(17.999998092651367D));
            this.textBox66.Style.Font.Bold = true;
            this.textBox66.Style.Font.Name = "Courier New";
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox66.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox66.StyleName = "";
            this.textBox66.Value = "=Fields.RemitToCityZip";
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Cm(6.7000002861022949D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(16.000003814697266D));
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.Font.Name = "Courier New";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox67.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox67.StyleName = "";
            this.textBox67.Value = "=Fields.RemitToPOBox";
            // 
            // textBox68
            // 
            this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(6.3000006675720215D));
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Pixel(14.215662956237793D));
            this.textBox68.Style.Font.Bold = true;
            this.textBox68.Style.Font.Name = "Courier New";
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox68.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox68.StyleName = "";
            this.textBox68.Value = "=Fields.RemitToName";
            // 
            // textBox69
            // 
            this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D));
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(254.99996948242188D), Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D));
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Font.Name = "Courier New";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox69.StyleName = "";
            this.textBox69.Value = "=Fields.TermOfSales";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Bindings.Add(new Telerik.Reporting.Binding("Value", "=Fields.LogoUrl"));
            this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(510.23629760742188D), Telerik.Reporting.Drawing.Unit.Pixel(211.653564453125D));
            this.pictureBox6.MimeType = "";
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(138.1544189453125D), Telerik.Reporting.Drawing.Unit.Pixel(102.0472412109375D));
            this.pictureBox6.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox6.Style.Font.Bold = true;
            this.pictureBox6.Style.Font.Name = "Courier New";
            this.pictureBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.pictureBox6.Value = "";
            // 
            // textBox70
            // 
            this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(5.100001335144043D));
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0784449577331543D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Name = "Courier New";
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox70.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dashed;
            this.textBox70.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox70.Value = "=Fields.TotalDue";
            // 
            // textBox71
            // 
            this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13D), Telerik.Reporting.Drawing.Unit.Cm(4.7000002861022949D));
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1784439086914062D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.textBox71.Style.Font.Bold = true;
            this.textBox71.Style.Font.Name = "Courier New";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox71.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox71.Value = "=Fields.TaxAmount";
            // 
            // textBox72
            // 
            this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.000200271606445D), Telerik.Reporting.Drawing.Unit.Cm(4.3000001907348633D));
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1782426834106445D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.Font.Name = "Courier New";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox72.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox72.Value = "=Fields.EnergySurcharge";
            // 
            // textBox73
            // 
            this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.000199317932129D), Telerik.Reporting.Drawing.Unit.Cm(3.8999996185302734D));
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1782450675964355D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.textBox73.Style.Font.Bold = true;
            this.textBox73.Style.Font.Name = "Courier New";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox73.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox73.Value = "=Fields.TotalCoffee";
            // 
            // textBox74
            // 
            this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.927868843078613D), Telerik.Reporting.Drawing.Unit.Cm(3.5000004768371582D));
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.250575065612793D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.Font.Name = "Courier New";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox74.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox74.Value = "=Fields.TotalAllied";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox13);
            tableGroup38.Name = "tableGroup3";
            this.table9.ColumnGroups.Add(tableGroup38);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.0786454677581787D), Telerik.Reporting.Drawing.Unit.Cm(6.0000009536743164D));
            this.table9.Name = "table9";
            tableGroup40.Name = "group3";
            tableGroup39.ChildGroups.Add(tableGroup40);
            tableGroup39.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup39.Name = "detailTableGroup1";
            this.table9.RowGroups.Add(tableGroup39);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7D), Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D));
            this.table9.Style.Font.Bold = true;
            this.table9.Style.Font.Name = "Courier New";
            this.table9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox14);
            tableGroup41.Name = "tableGroup3";
            this.table10.ColumnGroups.Add(tableGroup41);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(4.30000114440918D));
            this.table10.Name = "table10";
            tableGroup42.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup42.Name = "detailTableGroup1";
            this.table10.RowGroups.Add(tableGroup42);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.29959678649902344D));
            this.table10.Style.Font.Bold = true;
            this.table10.Style.Font.Name = "Courier New";
            this.table10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D)));
            this.table11.Body.SetCellContent(0, 0, this.textBox15);
            tableGroup43.Name = "tableGroup3";
            this.table11.ColumnGroups.Add(tableGroup43);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15});
            this.table11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(3.8995990753173828D));
            this.table11.Name = "table11";
            tableGroup44.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup44.Name = "detailTableGroup1";
            this.table11.RowGroups.Add(tableGroup44);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8999991416931152D), Telerik.Reporting.Drawing.Unit.Cm(0.29979813098907471D));
            this.table11.Style.Font.Bold = true;
            this.table11.Style.Font.Name = "Courier New";
            this.table11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table12
            // 
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D)));
            this.table12.Body.SetCellContent(0, 0, this.textBox16);
            tableGroup45.Name = "tableGroup3";
            this.table12.ColumnGroups.Add(tableGroup45);
            this.table12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16});
            this.table12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5000009536743164D), Telerik.Reporting.Drawing.Unit.Cm(5.1000008583068848D));
            this.table12.Name = "table12";
            tableGroup46.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup46.Name = "detailTableGroup1";
            this.table12.RowGroups.Add(tableGroup46);
            this.table12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5425100326538086D), Telerik.Reporting.Drawing.Unit.Cm(0.29919630289077759D));
            this.table12.Style.Font.Bold = true;
            this.table12.Style.Font.Name = "Courier New";
            this.table12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table13
            // 
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D)));
            this.table13.Body.SetCellContent(0, 0, this.textBox17);
            tableGroup47.Name = "tableGroup3";
            this.table13.ColumnGroups.Add(tableGroup47);
            this.table13.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17});
            this.table13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(4.7000012397766113D));
            this.table13.Name = "table13";
            tableGroup48.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup48.Name = "detailTableGroup1";
            this.table13.RowGroups.Add(tableGroup48);
            this.table13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.899998664855957D), Telerik.Reporting.Drawing.Unit.Cm(0.29939654469490051D));
            this.table13.Style.Font.Bold = true;
            this.table13.Style.Font.Name = "Courier New";
            this.table13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table14
            // 
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D)));
            this.table14.Body.SetCellContent(0, 0, this.textBox18);
            tableGroup49.Name = "tableGroup3";
            this.table14.ColumnGroups.Add(tableGroup49);
            this.table14.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18});
            this.table14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.0998983383178711D), Telerik.Reporting.Drawing.Unit.Cm(3.5000009536743164D));
            this.table14.Name = "table14";
            tableGroup50.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup50.Name = "detailTableGroup1";
            this.table14.RowGroups.Add(tableGroup50);
            this.table14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8277697563171387D), Telerik.Reporting.Drawing.Unit.Cm(0.29999998211860657D));
            this.table14.Style.Font.Bold = true;
            this.table14.Style.Font.Name = "Courier New";
            this.table14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table15
            // 
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0864591598510742D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.32305383682250977D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table15.Body.SetCellContent(1, 0, this.textBox20);
            this.table15.Body.SetCellContent(0, 0, this.textBox19);
            tableGroup51.Name = "tableGroup1";
            this.table15.ColumnGroups.Add(tableGroup51);
            this.table15.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox20,
            this.textBox19});
            this.table15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842D));
            this.table15.Name = "table15";
            tableGroup53.Name = "group";
            tableGroup54.Name = "group1";
            tableGroup52.ChildGroups.Add(tableGroup53);
            tableGroup52.ChildGroups.Add(tableGroup54);
            tableGroup52.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup52.Name = "detailTableGroup";
            this.table15.RowGroups.Add(tableGroup52);
            this.table15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0864591598510742D), Telerik.Reporting.Drawing.Unit.Cm(0.82305389642715454D));
            this.table15.Style.Font.Bold = true;
            this.table15.Style.Font.Name = "Courier New";
            this.table15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // table16
            // 
            this.table16.Anchoring = Telerik.Reporting.AnchoringStyles.Top;
            this.table16.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(11.951668739318848D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(7.19590425491333D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(22.9813232421875D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(70.412887573242188D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(21.822998046875D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Mm(28.633176803588867D)));
            this.table16.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table16.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458320021629333D)));
            this.table16.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.370417058467865D)));
            this.table16.Body.SetCellContent(0, 3, this.textBox58);
            this.table16.Body.SetCellContent(0, 4, this.textBox54);
            this.table16.Body.SetCellContent(0, 5, this.textBox52);
            this.table16.Body.SetCellContent(0, 0, this.textBox47);
            this.table16.Body.SetCellContent(0, 1, this.textBox46);
            this.table16.Body.SetCellContent(1, 2, this.textBox45);
            this.table16.Body.SetCellContent(1, 3, this.textBox44);
            this.table16.Body.SetCellContent(1, 4, this.textBox43);
            this.table16.Body.SetCellContent(1, 5, this.textBox42);
            this.table16.Body.SetCellContent(1, 0, this.shape2, 1, 2);
            this.table16.Body.SetCellContent(2, 3, this.textBox40);
            this.table16.Body.SetCellContent(2, 4, this.textBox37);
            this.table16.Body.SetCellContent(2, 5, this.textBox36);
            this.table16.Body.SetCellContent(2, 0, this.textBox35, 1, 2);
            this.table16.Body.SetCellContent(2, 2, this.textBox32);
            this.table16.Body.SetCellContent(0, 2, this.textBox31);
            tableGroup56.Name = "group5";
            tableGroup57.Name = "group2";
            tableGroup55.ChildGroups.Add(tableGroup56);
            tableGroup55.ChildGroups.Add(tableGroup57);
            tableGroup55.ReportItem = this.textBox30;
            tableGroup58.ReportItem = this.textBox29;
            tableGroup59.ReportItem = this.textBox26;
            tableGroup60.ReportItem = this.textBox25;
            tableGroup61.ReportItem = this.textBox23;
            this.table16.ColumnGroups.Add(tableGroup55);
            this.table16.ColumnGroups.Add(tableGroup58);
            this.table16.ColumnGroups.Add(tableGroup59);
            this.table16.ColumnGroups.Add(tableGroup60);
            this.table16.ColumnGroups.Add(tableGroup61);
            this.table16.ColumnHeadersPrintOnEveryPage = true;
            this.table16.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox58,
            this.textBox54,
            this.textBox52,
            this.textBox47,
            this.textBox46,
            this.textBox45,
            this.textBox44,
            this.textBox43,
            this.textBox42,
            this.shape2,
            this.textBox40,
            this.textBox37,
            this.textBox36,
            this.textBox35,
            this.textBox32,
            this.textBox31,
            this.textBox30,
            this.textBox29,
            this.textBox26,
            this.textBox25,
            this.textBox23});
            this.table16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.878645658493042D), Telerik.Reporting.Drawing.Unit.Cm(-1.2616315814284462E-07D));
            this.table16.Name = "table1";
            tableGroup62.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup62.Name = "Detail";
            tableGroup63.Name = "group6";
            this.table16.RowGroups.Add(tableGroup62);
            this.table16.RowGroups.Add(tableGroup63);
            this.table16.Style.Font.Bold = true;
            this.table16.Style.Font.Name = "Courier New";
            this.table16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            // 
            // Receipt
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "Receipt";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            reportParameter1.Name = "PrintLabels";
            reportParameter1.Type = Telerik.Reporting.ReportParameterType.Boolean;
            this.ReportParameters.Add(reportParameter1);
            this.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(0D);
            this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Pixel(772D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox lbl_Surcharge;
        private Telerik.Reporting.TextBox lbl_TotalCoffee;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox lbl_TaxAmount;
        private Telerik.Reporting.TextBox lbl_TotalAllied;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox fld_TaxAmount;
        private Telerik.Reporting.TextBox fld_surcharge;
        private Telerik.Reporting.TextBox fld_TotalCoffee;
        private Telerik.Reporting.TextBox fld_TotalAllied;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.Table table12;
        private Telerik.Reporting.Table table13;
        private Telerik.Reporting.Table table14;
        private Telerik.Reporting.Table table15;
        private Telerik.Reporting.Table table16;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox txt_InvoiceNo;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.Table table17;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.Table table24;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox100;
    }
}