namespace SalesLogicExpress.Reporting
{
    partial class LoadSuggetionsPickDC
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(70.5512466430664D), Telerik.Reporting.Drawing.Unit.Pixel(29.858268737792969D));
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Courier New";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox29.StyleName = "Normal.TableHeader";
            this.textBox29.Value = "Item";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(207.55120849609375D), Telerik.Reporting.Drawing.Unit.Pixel(29.858268737792969D));
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Courier New";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox34.StyleName = "Normal.TableHeader";
            this.textBox34.Value = "Description\r\n[S.Cat1,S.Cat4,S.Cat5]\r\n";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(69.166633605957031D), Telerik.Reporting.Drawing.Unit.Pixel(29.858268737792969D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox30.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Courier New";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox30.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox30.StyleName = "Corporate.TableHeader";
            this.textBox30.Value = "Pickd Qty\r\n(Prm UOM)";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(53.8332405090332D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox39.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox39.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox39.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox39.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Name = "Courier New";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox39.StyleName = "Corporate.TableHeader";
            this.textBox39.Value = "Shipped Qty\r\n(Prm UOM)";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(52.166652679443359D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox28.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox28.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Courier New";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox28.StyleName = "Corporate.TableHeader";
            this.textBox28.Value = "Adj Qty\r\n(Prm UOM)";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(54.666610717773438D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox32.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox32.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Courier New";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox32.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox32.StyleName = "Corporate.TableHeader";
            this.textBox32.Value = "Shipped Qty\r\n(Trns UOM)";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(70.666694641113281D), Telerik.Reporting.Drawing.Unit.Pixel(29.858266830444336D));
            this.textBox43.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox43.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox43.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Courier New";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox43.StyleName = "Corporate.TableHeader";
            this.textBox43.Value = "Repln Qty\r\n(Trns UOM)";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(77.000007629394531D), Telerik.Reporting.Drawing.Unit.Pixel(29.858268737792969D));
            this.textBox36.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox36.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Courier New";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox36.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox36.StyleName = "Corporate.TableHeader";
            this.textBox36.Value = "OnHand Qty\r\n(Prm UOM)";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(69.999862670898438D), Telerik.Reporting.Drawing.Unit.Pixel(29.858268737792969D));
            this.textBox19.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox19.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox19.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox19.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox19.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Courier New";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox19.StyleName = "Corporate.TableHeader";
            this.textBox19.Value = "Avlbl Qty\r\n(Prm UOM)";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(3.0000002384185791D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox26,
            this.textBox25,
            this.textBox20,
            this.textBox14,
            this.textBox13,
            this.textBox12,
            this.textBox11,
            this.textBox10,
            this.textBox6,
            this.textBox5,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox4,
            this.textBox3,
            this.textBox2,
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(644D), Telerik.Reporting.Drawing.Unit.Pixel(30D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(89D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Courier New";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Value = "=Fields.ToDate";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(494D), Telerik.Reporting.Drawing.Unit.Pixel(30D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(42D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Courier New";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.Value = "FROM:";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(616D), Telerik.Reporting.Drawing.Unit.Pixel(30D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(28D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Courier New";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Value = "TO:";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(536D), Telerik.Reporting.Drawing.Unit.Pixel(30D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(80D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Courier New";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Value = "=Fields.FromDate";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(494D), Telerik.Reporting.Drawing.Unit.Pixel(49D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(110.43077850341797D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Courier New";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Value = "SUGGESTION ID:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(604.43829345703125D), Telerik.Reporting.Drawing.Unit.Pixel(49D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.488189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Courier New";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Value = "=Fields.TransferNo";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(494D), Telerik.Reporting.Drawing.Unit.Pixel(86D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(105.60641479492188D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Courier New";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Value = "REQUESTED BY:";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(599.61395263671875D), Telerik.Reporting.Drawing.Unit.Pixel(86D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.488189697265625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Courier New";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Value = "=Fields.UserName";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(494D), Telerik.Reporting.Drawing.Unit.Pixel(67D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(56.692962646484375D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Courier New";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Value = "STATUS:";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(551D), Telerik.Reporting.Drawing.Unit.Pixel(67D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(168.00009155273438D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Courier New";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Value = "=Fields.Status";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(29D), Telerik.Reporting.Drawing.Unit.Pixel(30D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(49.126304626464844D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Courier New";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.Value = "ROUTE:";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(78.638099670410156D), Telerik.Reporting.Drawing.Unit.Pixel(30D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(302.36221313476562D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Courier New";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Value = "=Fields.RouteNoAndName";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(29D), Telerik.Reporting.Drawing.Unit.Pixel(67D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(94.102020263671875D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Courier New";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Value = "TRANSFER TO:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(124D), Telerik.Reporting.Drawing.Unit.Pixel(67D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(257.00033569335938D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Courier New";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Value = "=Fields.TransferTo";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(139D), Telerik.Reporting.Drawing.Unit.Pixel(48D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(241.88980102539063D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Courier New";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.Value = "=Fields.TransferFrom";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(28D), Telerik.Reporting.Drawing.Unit.Pixel(48D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(109.60630798339844D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Courier New";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Value = "TRANSFER FROM:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(101D), Telerik.Reporting.Drawing.Unit.Pixel(11D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(536.69293212890625D), Telerik.Reporting.Drawing.Unit.Pixel(15.118110656738281D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Courier New";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "= Fields.ReportTitle";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table2});
            this.detail.Name = "detail";
            // 
            // table2
            // 
            this.table2.Bindings.Add(new Telerik.Reporting.Binding("DataSource", "=Fields.Items"));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(70.5512466430664D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(207.55137634277344D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(69.166633605957031D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(53.8332405090332D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(52.166645050048828D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(54.666603088378906D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(70.666702270507812D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(77.000038146972656D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Pixel(69.999847412109375D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D)));
            this.table2.Body.SetCellContent(0, 1, this.textBox24);
            this.table2.Body.SetCellContent(0, 0, this.textBox18);
            this.table2.Body.SetCellContent(1, 0, this.textBox37);
            this.table2.Body.SetCellContent(1, 1, this.textBox49);
            this.table2.Body.SetCellContent(0, 2, this.textBox40);
            this.table2.Body.SetCellContent(0, 3, this.textBox33);
            this.table2.Body.SetCellContent(0, 4, this.textBox35);
            this.table2.Body.SetCellContent(0, 5, this.textBox31);
            this.table2.Body.SetCellContent(0, 7, this.textBox17);
            this.table2.Body.SetCellContent(0, 8, this.textBox22);
            this.table2.Body.SetCellContent(1, 2, this.textBox15, 1, 2);
            this.table2.Body.SetCellContent(1, 4, this.textBox42);
            this.table2.Body.SetCellContent(1, 5, this.textBox16);
            this.table2.Body.SetCellContent(1, 7, this.textBox21);
            this.table2.Body.SetCellContent(1, 8, this.textBox23);
            this.table2.Body.SetCellContent(1, 6, this.textBox41);
            this.table2.Body.SetCellContent(0, 6, this.textBox27);
            tableGroup1.Name = "tableGroup";
            tableGroup1.ReportItem = this.textBox29;
            tableGroup2.Name = "tableGroup1";
            tableGroup2.ReportItem = this.textBox34;
            tableGroup3.Name = "group4";
            tableGroup3.ReportItem = this.textBox30;
            tableGroup4.Name = "group6";
            tableGroup4.ReportItem = this.textBox39;
            tableGroup5.Name = "group5";
            tableGroup5.ReportItem = this.textBox28;
            tableGroup6.Name = "group2";
            tableGroup6.ReportItem = this.textBox32;
            tableGroup7.Name = "group3";
            tableGroup7.ReportItem = this.textBox43;
            tableGroup8.Name = "group1";
            tableGroup8.ReportItem = this.textBox36;
            tableGroup9.Name = "group";
            tableGroup9.ReportItem = this.textBox19;
            this.table2.ColumnGroups.Add(tableGroup1);
            this.table2.ColumnGroups.Add(tableGroup2);
            this.table2.ColumnGroups.Add(tableGroup3);
            this.table2.ColumnGroups.Add(tableGroup4);
            this.table2.ColumnGroups.Add(tableGroup5);
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox24,
            this.textBox18,
            this.textBox37,
            this.textBox49,
            this.textBox40,
            this.textBox33,
            this.textBox35,
            this.textBox31,
            this.textBox17,
            this.textBox22,
            this.textBox15,
            this.textBox42,
            this.textBox16,
            this.textBox21,
            this.textBox23,
            this.textBox41,
            this.textBox27,
            this.textBox29,
            this.textBox34,
            this.textBox30,
            this.textBox39,
            this.textBox28,
            this.textBox32,
            this.textBox43,
            this.textBox36,
            this.textBox19});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Pixel(22.677164077758789D), Telerik.Reporting.Drawing.Unit.Pixel(0.0037841796875D));
            this.table2.Name = "table2";
            tableGroup11.Name = "group7";
            tableGroup12.Name = "group8";
            tableGroup10.ChildGroups.Add(tableGroup11);
            tableGroup10.ChildGroups.Add(tableGroup12);
            tableGroup10.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup10.Name = "detailTableGroup";
            this.table2.RowGroups.Add(tableGroup10);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(725.602294921875D), Telerik.Reporting.Drawing.Unit.Cm(1.8500000238418579D));
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(207.55125427246094D), Telerik.Reporting.Drawing.Unit.Pixel(20.031496047973633D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox24.Style.Font.Name = "Courier New";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox24.StyleName = "Corporate.TableBody";
            this.textBox24.Value = "= Fields.ItemDescription";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(70.5512466430664D), Telerik.Reporting.Drawing.Unit.Pixel(20.031496047973633D));
            this.textBox18.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox18.Style.Font.Bold = false;
            this.textBox18.Style.Font.Name = "Courier New";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.StyleName = "Normal.TableBody";
            this.textBox18.Value = "=Fields.ItemNumber";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(70.5512466430664D), Telerik.Reporting.Drawing.Unit.Pixel(20.031496047973633D));
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox37.Style.Font.Name = "Courier New";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox37.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox37.StyleName = "Normal.TableBody";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(207.55126953125D), Telerik.Reporting.Drawing.Unit.Pixel(20.031496047973633D));
            this.textBox49.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox49.Style.Font.Name = "Courier New";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox49.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox49.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox49.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox49.StyleName = "Corporate.TableBody";
            this.textBox49.Value = "=\'[\'+  Fields.SalesCat1 +\',\'+  Fields.SalesCat4 +\',\'+  Fields.SalesCat5 +\']\'";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(69.1666488647461D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox40.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox40.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox40.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.Style.Font.Bold = false;
            this.textBox40.Style.Font.Name = "Courier New";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox40.StyleName = "Corporate.TableBody";
            this.textBox40.Value = "= Fields.PickQtyPrimaryUOM+\' \'+Fields.PrimaryUM";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(53.8332405090332D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox33.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox33.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox33.Style.Font.Name = "Courier New";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox33.StyleName = "Corporate.TableBody";
            this.textBox33.Value = "= Fields.TransactionQtyPrimaryUOM +\' \'+Fields.PrimaryUM";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(52.166652679443359D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox35.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox35.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox35.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.Font.Name = "Courier New";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox35.StyleName = "Corporate.TableBody";
            this.textBox35.Value = "= Fields.AdjustmentQty +\' \'+Fields.PrimaryUM";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(54.666610717773438D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox31.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox31.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox31.Style.Font.Name = "Courier New";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox31.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox31.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox31.StyleName = "Corporate.TableBody";
            this.textBox31.Value = "= Fields.ShippedQty +\' \'+Fields.TransactionUOM";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(77.000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox17.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox17.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.Style.Font.Name = "Courier New";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox17.StyleName = "Corporate.TableBody";
            this.textBox17.Value = "= Fields.ActualQtyOnHand +\' \'+Fields.PrimaryUM";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(69.999870300292969D), Telerik.Reporting.Drawing.Unit.Cm(0.52999997138977051D));
            this.textBox22.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.Font.Name = "Courier New";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox22.StyleName = "Corporate.TableBody";
            this.textBox22.Value = "= Fields.AvailableQty+\' \'+Fields.PrimaryUM";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(122.99989318847656D), Telerik.Reporting.Drawing.Unit.Pixel(20.031497955322266D));
            this.textBox15.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox15.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox15.Style.Font.Bold = false;
            this.textBox15.Style.Font.Name = "Courier New";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox15.StyleName = "Corporate.TableHeader";
            this.textBox15.Value = "Manual Pick Reason:";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(52.166652679443359D), Telerik.Reporting.Drawing.Unit.Pixel(20.031497955322266D));
            this.textBox42.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox42.Style.Font.Name = "Courier New";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox42.StyleName = "Corporate.TableBody";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4463872909545898D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox16.Style.Font.Name = "Courier New";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox16.StyleName = "Corporate.TableBody";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0372920036315918D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox21.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox21.Style.Font.Name = "Courier New";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox21.StyleName = "Corporate.TableBody";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.85207998752594D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox23.Style.Font.Name = "Courier New";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox23.StyleName = "Corporate.TableBody";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(70.666694641113281D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox41.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox41.Style.Font.Name = "Courier New";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox41.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(3D);
            this.textBox41.StyleName = "Corporate.TableBody";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Pixel(70.666694641113281D), Telerik.Reporting.Drawing.Unit.Cm(0.53000003099441528D));
            this.textBox27.Style.BorderColor.Bottom = System.Drawing.Color.Black;
            this.textBox27.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox27.Style.Font.Name = "Courier New";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Pixel(4D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox27.StyleName = "Corporate.TableBody";
            this.textBox27.Value = "= Fields.TransactionQtyDisplay +\' \'+Fields.TransactionUOM";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.5D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // LoadSuggetionsPickDC
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "LoadSuggetionsPickDC";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D), Telerik.Reporting.Drawing.Unit.Pixel(10D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Pixel;
            this.Width = Telerik.Reporting.Drawing.Unit.Pixel(762D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox43;
    }
}