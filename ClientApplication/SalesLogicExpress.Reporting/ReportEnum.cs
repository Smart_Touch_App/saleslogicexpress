﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Reporting
{
    public enum Reports
    {
        [Description("Reports.OrderReport")]
        OrderReport,
        [Description("SalesLogicExpress.Reporting.rPickOrderSlip")]
        PickSlipReport,
        [Description("SalesLogicExpress.Reporting.Invoice")]
        InvoiceReport,
        [Description("SalesLogicExpress.Reporting.Receipt")]
        ReceiptReport,
        [Description("SalesLogicExpress.Reporting.CreditMemo")]
        CreditMemoReport,
        [Description("SalesLogicExpress.Reporting.AddLoadRequest")]
        AddLoadRequestReport,
        [Description("SalesLogicExpress.Reporting.AddLoadPick")]
        AddLoadPickReport,
        [Description("SalesLogicExpress.Reporting.LoadSuggetions")]
        LoadSuggetionsReport,
        [Description("SalesLogicExpress.Reporting.LoadSuggetionsPick")]
        LoadSuggetionsPickReport,
        [Description("SalesLogicExpress.Reporting.HeldReturnLoad")]
        HeldReturnLoadReport,
        [Description("SalesLogicExpress.Reporting.HeldReturnPick")]
        HeldReturnPickReport,
        [Description("SalesLogicExpress.Reporting.SuggesionReturnLoad")]
        SuggesionReturnLoadReport,
        [Description("SalesLogicExpress.Reporting.SuggestionReturnPick")]
        SuggestionReturnPickReport,
        [Description("SalesLogicExpress.Reporting.Unload")]
        UnloadReport,
        [Description("SalesLogicExpress.Reporting.UnloadPick")]
        UnloadPickReport,
        [Description("SalesLogicExpress.Reporting.AddLoadRequestDC")]
        AddLoadRequestReportDC,
        [Description("SalesLogicExpress.Reporting.AddLoadPickDC")]
        AddLoadPickReportDC,
        [Description("SalesLogicExpress.Reporting.LoadSuggetionsDC")]
        LoadSuggetionsReportDC,
        [Description("SalesLogicExpress.Reporting.LoadSuggetionsPickDC")]
        LoadSuggetionsPickReportDC,        
        [Description("SalesLogicExpress.Reporting.SuggesionReturnLoadDC")]
        SuggesionReturnLoadReportDC,
        [Description("SalesLogicExpress.Reporting.SuggestionReturnPickDC")]
        SuggestionReturnPickReportDC,
        [Description("SalesLogicExpress.Reporting.UnloadDC")]
        UnloadReportDC,
        [Description("SalesLogicExpress.Reporting.UnloadPickDC")]
        UnloadPickReportDC,
        [Description("SalesLogicExpress.Reporting.ItemReturns")]
        ItemReturnsReport,
        [Description("SalesLogicExpress.Reporting.QuotePick")]
        QuotePick,
        [Description("SalesLogicExpress.Reporting.CustomerQuote")]
        CustomerQuote,
        [Description("SalesLogicExpress.Reporting.ProspectQuote")]
        ProspectQuote,
        [Description("SalesLogicExpress.Reporting.PreTripInspection")]
        PreTripInspection,
        [Description("SalesLogicExpress.Reporting.RouteSettlementReport")]
        RouteSettlementReport
    }
}
