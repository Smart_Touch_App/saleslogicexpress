﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class PriceOverrideCode
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public static ObservableCollection<PriceOverrideCode> GetPriceOverrideCodes()
        {
            ObservableCollection<PriceOverrideCode> codes = new ObservableCollection<PriceOverrideCode>();
            codes.Add(new PriceOverrideCode { Id = 1, Name = "" });
            codes.Add(new PriceOverrideCode { Id = 2, Name = "CPR" });
            codes.Add(new PriceOverrideCode { Id = 3, Name = "PKN" });
            codes.Add(new PriceOverrideCode { Id = 4, Name = "SMP" });
            codes.Add(new PriceOverrideCode { Id = 5, Name = "TPR" });
            return codes;
        }
    }
}
