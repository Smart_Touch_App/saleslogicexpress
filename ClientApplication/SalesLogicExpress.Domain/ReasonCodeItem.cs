﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ReasonCodeItem : Helpers.ModelBase
    {
        int reasonCodeID;
        string reasonCode;
        [DataMember]
        public int ReasonCodeID
        {
            get
            {
                return this.reasonCodeID;
            }
            set
            {
                this.reasonCodeID = value;
                OnPropertyChanged("ReasonCodeID");
            }
        }
        [DataMember]
        public string ReasonCodeDescription
        {
            get
            {
                return this.reasonCode;
            }
            set
            {
                this.reasonCode = value;
                OnPropertyChanged("ReasonCode");
            }
        }
    }

}
