﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SalesLogicExpress.Domain
{
    [DebuggerDisplay("CycleCountID = {CycleCountID}")]
    public class CycleCount : Helpers.ModelBase
    {
        private int cycleCountID;
        public int CycleCountID
        {
            get { return cycleCountID; }
            set { cycleCountID = value; OnPropertyChanged("CycleCountID"); }
        }

        private bool isCountInitiated = false;

        public bool IsCountInitiated
        {
            get { return isCountInitiated; }
            set { isCountInitiated = value; OnPropertyChanged("IsCountInitiated"); }
        }

        private string cycleCountStatus;
        public string CycleCountStatus
        {
            get { return cycleCountStatus; }
            set { cycleCountStatus = value; OnPropertyChanged("CycleCountStatus"); }
        }

        private bool isCountEnable = true;

        public bool IsCountEnable
        {
            get { return isCountEnable; }
            set { isCountEnable = value; OnPropertyChanged("IsCountEnable"); }
        }
        private int cycleCountRevision;

        public int CycleCountRevision
        {
            get { return cycleCountRevision; }
            set { cycleCountRevision = value; OnPropertyChanged("CycleCountRevision"); }
        }
        
        private int cycleCountStatusID;
        public int CycleCountStatusID
        {
            get { return cycleCountStatusID; }
            set { cycleCountStatusID = value; OnPropertyChanged("CycleCountStatusID"); }
        }

        private string cycleCountStatusCD;
        public string CycleCountStatusCD
        {
            get { return cycleCountStatusCD; }
            set { cycleCountStatusCD = value; OnPropertyChanged("CycleCountStatusCD"); }
        }

        private string requestTypeStatusCD;

        public string RequestTypeStatusCD
        {
            get { return requestTypeStatusCD; }
            set { requestTypeStatusCD = value; OnPropertyChanged("RequestTypeStatusCD"); }
        }

        private string customerContact;
        public string CustomerContact
        {
            get { return customerContact; }
            set { customerContact = value; OnPropertyChanged("CustomerContact"); }
        }

        private DateTime cycleCountDate;
        public DateTime CycleCountDate
        {
            get { return cycleCountDate; }
            set { cycleCountDate = value; OnPropertyChanged("CycleCountDate"); }
        }

        private int itemsToCount;

        public int ItemsToCount
        {
            get { return itemsToCount; }
            set { itemsToCount = value; OnPropertyChanged("ItemsToCount"); }
        }

        private int itemsCounted;

        public int ItemsCounted
        {
            get { return itemsCounted; }
            set { itemsCounted = value; OnPropertyChanged("ItemsCounted"); }
        }

        private string customerContactName;

        public string CustomerContactName
        {
            get { return customerContactName; }
            set { customerContactName = value; OnPropertyChanged("CustomerContactName"); }
        }

        private string customerContactNumber;
        public string CustomerContactNumber
        {
            get { return customerContactNumber; }
            set { customerContactNumber = value; OnPropertyChanged("CustomerContactNumber"); }
        }

        private bool isItemsToCountVisible = false;
        public bool IsItemsToCountVisible
        {
            get { return isItemsToCountVisible; }
            set { isItemsToCountVisible = value; OnPropertyChanged("IsItemsToCountVisible"); }
        }

        private bool isItemsCountedVisible;
        public bool IsItemsCountedVisible
        {
            get { return isItemsCountedVisible; }
            set { isItemsCountedVisible = value; OnPropertyChanged("IsItemsCountedVisible"); }
        }

        private int itemsCountedAfterRecount;
        public int ItemsCountedAfterRecount
        {
            get { return itemsCountedAfterRecount; }
            set { itemsCountedAfterRecount = value; OnPropertyChanged("ItemsCountedAfterRecount"); }
        }


        bool isItemsVoidedVisible;
        public bool IsItemsVoidedVisible
        {
            get
            {
                return isItemsVoidedVisible;
            }
            set
            {


                isItemsVoidedVisible = value;
                OnPropertyChanged("IsItemsVoidedVisible");
            }
        }
        private bool isItemsCountedAfterRecountVisible;
        public bool IsItemsCountedAfterRecountVisible
        {
            get { return isItemsCountedAfterRecountVisible; }
            set { isItemsCountedAfterRecountVisible = value; OnPropertyChanged("IsItemsCountedAfterRecountVisible"); }
        }

        private bool isBorderVisible = false;
        public bool IsBorderVisible
        {
            get { return isBorderVisible; }
            set { isBorderVisible = value; OnPropertyChanged("IsBorderVisible"); }
        }

    }

    [DebuggerDisplay("ItemID = {ItemId} : ItemNumber={ItemNumber} : CountedQuantity={CountedQuantity}")]
    public class CycleCountItems : PickItem
    {
        private int reCountNumber;

        //public CycleCountItems(PickItem pickItem)
        //{

        //}

        public int ReCountNumber
        {
            get { return reCountNumber; }
            set { reCountNumber = value; OnPropertyChanged("ReCountNumber"); }
        }
        bool isForCount = false;
        public bool IsForCount
        {
            get { return isForCount; }
            set { isForCount = value; OnPropertyChanged("IsForCount"); }
        }

        private int cycleCountDetailID;
        public int CycleCountDetailID
        {
            get { return cycleCountDetailID; }
            set { cycleCountDetailID = value; OnPropertyChanged("CycleCountDetailID"); }
        }

        private int cycleCountID;
        public int CycleCountID
        {
            get { return cycleCountID; }
            set { cycleCountID = value; OnPropertyChanged("CycleCountID"); }
        }

        private int countedQuantity;
        public int CountedQuantity
        {
            get { return Convert.ToInt32(PickedQty); }
            set {  PickedQty = value; OnPropertyChanged("CountedQuantity"); }
        }

        private bool itemStatus;
        public bool ItemStatus
        {
            get { return itemStatus; }
            set { itemStatus = value; OnPropertyChanged("ItemStatus"); }
        }
        public DateTime CreatedDate { get; set; }

        private string countedQtyUoM;

        public string CountedQtyUoM
        {
            get { return TransactionUOM; }
            set { TransactionUOM = value; OnPropertyChanged("CountedQtyUoM"); }
        }

        private bool isVisible = true;
        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; OnPropertyChanged("IsVisible"); }
        }

    }
}
