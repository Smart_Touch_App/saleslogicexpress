﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ItemRestriction : Helpers.ModelBase
    {

        public Dictionary<string, Restriction> CustomerRestrictions { get; set; }

        public class Restriction
        {
            public string Type { get; set; }
            public List<Item> Items { get; set; }
        }
        public string CustomerID { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public bool IsRestricted { get; set; }
        public string RestrictionFlag { get; set; }
        public override string ToString()
        {
            return string.Concat("ItemNo:", ItemNo, "Description:", Description);
        }

    }
}
