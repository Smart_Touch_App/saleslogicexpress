﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class PreOrderStops
    {
        public string Stopdate { set; get; }
        public string Weekday { set; get; }
    }
}
