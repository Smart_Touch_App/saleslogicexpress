﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SalesLogicExpress.Domain.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;


namespace SalesLogicExpress.Domain
{
    public class ProspectContactVisitee : ValidateModelBase
    {
        public ProspectContactVisitee()
            : base()
        {

        }
        int _SequenceNo = -1;
        int _TempSequenceNo = 0;
        string _City;
        string _Zip;
        string _Phone;
        string _BusinessName;
        string _Phone1;
        string _Phone2;
        string _Phone3;
        string _PhoneExtention;
        string _State;
        string _Address;
        string _AddressLine1;
        string _AddressLine2;
        string _AddressLine3;
        string _AddressLine4;
        string _Name;
        public int SequenceNo
        {
            get { return this._SequenceNo; }
            set
            {
                if (value != this._SequenceNo)
                {
                    this._SequenceNo = value;
                    this.OnPropertyChanged("SequenceNo");
                }
            }
        }
        public int TempSequenceNo
        {
            get
            {
                return _TempSequenceNo;
            }
            set
            {
                _TempSequenceNo = value;
                this.OnPropertyChanged("TempSequenceNo");
            }
        }

        string result = string.Empty;

        Int32 _ProspectID = 0;
        public Int32 ProspectID
        {
            get
            {
                return _ProspectID;
            }
            set
            {
                _ProspectID = value;
                OnPropertyChanged("ProspectID");
            }
        }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "")]
        //[StringLengthRange(Minimum = 3, Maximum = 40, ErrorMessage = "Min 3 and max 40 charactors are allowed")]
        //[ExcludeChar("%!%^*", ErrorMessage = "special characters not allowed")]

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
                if (IsAddContact)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) &&
                        !string.IsNullOrWhiteSpace(Phone2) && !string.IsNullOrWhiteSpace(Phone3) && numericOnlyRegex.IsMatch(Phone1) && (Phone1.Length == 3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else if (IsAddPhone)
                {
                    if (!string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                    !string.IsNullOrWhiteSpace(Phone3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
                OnPropertyChanged("Name");
            }
        }

        public string Phone
        {
            get
            {
                return _Phone;
            }
            set
            {
                _Phone = value;
                OnPropertyChanged("Phone");
            }
        }
        [RegularExpression(@"[0-9]+$", ErrorMessage = " ")]
        [StringLengthRange(Minimum = 3, Maximum = 3, ErrorMessage = " ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        [StringLength(3, ErrorMessage = " ")]
        public string Phone1
        {
            get
            {
                return _Phone1;
            }
            set
            {
                _Phone1 = value;
                if (IsAddContact)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                        !string.IsNullOrWhiteSpace(Phone3) && numericOnlyRegex.IsMatch(Phone1) && (Phone1.Length == 3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else if (IsAddPhone)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                    !string.IsNullOrWhiteSpace(Phone3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
                OnPropertyChanged("Phone1");
            }
        }
        [RegularExpression(@"[0-9]+$", ErrorMessage = " ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        [StringLengthRange(Minimum = 3, Maximum = 3, ErrorMessage = " ")]
        [StringLength(3, ErrorMessage = " ")]
        public string Phone2
        {
            get
            {
                return _Phone2;
            }
            set
            {
                _Phone2 = value;
                if (IsAddContact)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                        !string.IsNullOrWhiteSpace(Phone3) && numericOnlyRegex.IsMatch(Phone2) && (Phone2.Length == 3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else if (IsAddPhone)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                    !string.IsNullOrWhiteSpace(Phone3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }

                OnPropertyChanged("Phone2");
            }
        }
        [RegularExpression(@"[0-9]+$", ErrorMessage = " ")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        [StringLengthRange(Minimum = 4, Maximum = 4, ErrorMessage = " ")]
        [StringLength(4, ErrorMessage = " ")]
        public string Phone3
        {
            get
            {
                return _Phone3;
            }
            set
            {
                _Phone3 = value;
                if (IsAddContact)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                        !string.IsNullOrWhiteSpace(Phone3) && numericOnlyRegex.IsMatch(Phone3) && (Phone3.Length == 4))
                    {
                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else if (IsAddPhone)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                   !string.IsNullOrWhiteSpace(Phone3))
                    {
                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
                OnPropertyChanged("Phone3");
            }
        }
        [StringLength(7, ErrorMessage = " ")]
        public string PhoneExtention
        {
            get
            {
                return _PhoneExtention;
            }
            set
            {
                _PhoneExtention = value;

                if (IsAddContact)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                        !string.IsNullOrWhiteSpace(Phone3))
                    {
                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else if (IsAddPhone)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) && !string.IsNullOrWhiteSpace(Phone2) &&
                    !string.IsNullOrWhiteSpace(Phone3))
                    {
                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
                OnPropertyChanged("PhoneExtention");
            }
        }
        private bool _ToggleAddButton = false;
        public bool ToggleAddButton
        {
            get
            {
                return _ToggleAddButton;
            }
            set
            {
                _ToggleAddButton = value;
                OnPropertyChanged("ToggleAddButton");
            }
        }

        string _SelectedPhoneType;
        public string SelectedPhoneType
        {
            get
            {
                return _SelectedPhoneType;
            }
            set
            {
                _SelectedPhoneType = value;

                if (IsAddContact)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) &&
                        !string.IsNullOrWhiteSpace(Phone2) && !string.IsNullOrWhiteSpace(Phone3))
                    {
                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else if (IsAddPhone)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) &&
                          !string.IsNullOrWhiteSpace(Phone2) && !string.IsNullOrWhiteSpace(Phone3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
            }
        }

        string _SelectedEmailType;
        public string SelectedEmailType
        {
            get
            {
                return _SelectedEmailType;
            }
            set
            {
                _SelectedEmailType = value;
                if (!string.IsNullOrEmpty(_SelectedEmailType) && !string.IsNullOrWhiteSpace(EmailID) && (emailRegex.IsMatch(EmailID)))
                {
                    if (IsAddContact)
                    {
                        if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) &&
                            !string.IsNullOrWhiteSpace(Phone2) && !string.IsNullOrWhiteSpace(Phone3))
                        {
                            ToggleAddButton = true;
                        }
                        else
                        {
                            ToggleAddButton = false;
                        }
                    }
                    else if (IsAddEmail)
                    {
                        if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedEmailType) && !string.IsNullOrWhiteSpace(EmailID) &&
                             emailRegex.IsMatch(EmailID))
                        {
                            ToggleAddButton = true;
                        }
                        else
                        {
                            ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
            }
        }

        string _EmailID;
        public string EmailID
        {
            get
            {
                return _EmailID;
            }
            set
            {
                _EmailID = value;
                if (emailRegex.IsMatch(EmailID) && !string.IsNullOrEmpty(_SelectedEmailType) && !string.IsNullOrWhiteSpace(EmailID))
                {
                    if (IsAddContact)
                    {
                        if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) &&
                                !string.IsNullOrWhiteSpace(Phone2) && !string.IsNullOrWhiteSpace(Phone3))
                        {
                            ToggleAddButton = true;
                        }
                        else
                        {
                            ToggleAddButton = false;
                        }
                    }
                    else if (IsAddEmail)
                    {
                        if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_SelectedEmailType) && !string.IsNullOrWhiteSpace(EmailID))
                        {
                            ToggleAddButton = true;
                        }
                        else
                        {
                            ToggleAddButton = false;
                        }
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
                OnPropertyChanged("EmailID");
            }
        }

        private Regex emailRegex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9_\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        private Regex numericOnlyRegex = new Regex(@"^[0-9]([0-9]{1,3})?$");

        public bool IsAddContact = false;
        public bool IsAddPhone = false;
        public bool IsAddEmail = false;
        public bool IsValidEmail = false;

        public string _Title;
        public string Title
        {
            get
            { return _Title; }

            set
            {
                _Title = value;

                if (IsAddContact)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) &&
                        !string.IsNullOrWhiteSpace(Phone2) && !string.IsNullOrWhiteSpace(Phone3) && numericOnlyRegex.IsMatch(Phone1) &&
                        (Phone1.Length == 3))
                    {

                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else if (IsAddPhone)
                {
                    if (!string.IsNullOrEmpty(_Name) && !string.IsNullOrEmpty(_Title) && !string.IsNullOrEmpty(_SelectedPhoneType) && !string.IsNullOrWhiteSpace(Phone1) &&
                          !string.IsNullOrWhiteSpace(Phone2) && !string.IsNullOrWhiteSpace(Phone3))
                    {
                        ToggleAddButton = true;
                    }
                    else
                    {
                        ToggleAddButton = false;
                    }
                }
                else
                {
                    ToggleAddButton = false;
                }
                OnPropertyChanged("Title");
            }
        }
    }
}
