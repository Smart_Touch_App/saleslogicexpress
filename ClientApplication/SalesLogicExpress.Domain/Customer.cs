﻿
﻿using System;
using SalesLogicExpress.Domain.Helpers;


namespace SalesLogicExpress.Domain
{
    public class Customer : Visitee
    {
        public Customer()
        {
            activityTimeIndex = DateTime.SpecifyKind(activityTimeIndex, DateTimeKind.Utc);
        }


        public string DeliveryCodeDescription
        {
            get;
            set;
        }

        //public string OriginalDate
        //{ get; set; }

        public string BillType
        {
            get;
            set;
        }


        public string Company
        {
            get;
            set;
        }

        public string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
            set
            {
                customerNo = value;
                this.VisiteeId = value;
            }
        }

        private string custName = string.Empty;

        public string CustName
        {
            get { return custName; }
            set { custName = value; }
        }    
        public string StopCodeDesc { get; set; }
        public string CustomerOrdTemplate
        {
            get;
            set;
        }
        public string DeliveryCode
        {
            get;
            set;
        }

        public string Route
        {
            get;
            set;
        }
        public string RouteBranch
        {
            get;
            set;
        }

        public bool CreditHold { get; set; }

        public string Shop
        {
            get;
            set;
        }

        public string Type
        { get; set; }

        public string SaleStatusReason
        {
            get;
            set;
        }
        public bool Activity { get; set; }
       
        public bool IsCustomerOnAccount { get; set; }
    

        bool _visible = true;
        public bool ItemVisible
        {
            get
            {
                return _visible;
            }
            set
            {
                _visible = value;
                OnPropertyChanged("ItemVisible");
            }
        }

        public string PaymentMode
        { get; set; }
        public string PaymentModeDescription
        { get; set; }
        public string PaymentModeForList
        { get; set; }
        public string PaymentModeDescriptionForList
        { get; set; }

        private bool isAllowCaptureDrag = false;
        public bool IsAllowCaptureDrag
        {
            get
            {
                return this.isAllowCaptureDrag;
            }
            set
            {
                this.isAllowCaptureDrag = value;
                OnPropertyChanged("IsAllowCaptureDrag");
            }
        }

        private bool isAllowDrop = false;

        public bool IsAllowDrop
        {
            get
            {
                return this.isAllowDrop;
            }
            set
            {
                this.isAllowDrop = value;
                OnPropertyChanged("IsAllowDrop");
            }
        }

        private System.DateTime activityTimeIndex;
        public System.DateTime ActivityTimeIndex
        {
            get
            {
                return this.activityTimeIndex;
            }
            set
            {
                this.activityTimeIndex = value;
                OnPropertyChanged("ActivityTimeIndex");
            }
        }
        public override string ToString()
        {
            return CustomerNo + ":" + Name;
        }

        public Customer DeepCopy()
        {
            Customer cust = (Customer)this.MemberwiseClone();
            return cust;
        }
    }
}