﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class TransactionSyncDetail
    {
        public int SyncID
        {
            get;
            set;
        }
        public int ActivityID
        {
            get;
            set;
        }
        public bool Synched
        {
            get;
            set;
        }
        public DateTime SyncTimestamp
        {
            get;
            set;
        }
        public string Priority
        {
            get;
            set;
        }
    }
}
