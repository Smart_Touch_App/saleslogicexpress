﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    [DataContract]
    public class PickOrderItem : OrderItem
    {
        int _OrderQty = 0, _PickQtyInPrimaryUOM = 0, _LastManualPickReasonCode = 0, _OnHandQuantity = 0, _OrderQtyInPrimaryUOM = 0, _ExceptionQtyInPrimaryUOM = 0, _ExceptionQtyInOrderUOM = 0, _AvailableQuantity = 0;
        string _ExceptionReason;
        char _ItemStatus;


        public PickOrderItem()
        {

        }
        public PickOrderItem(OrderItem orderItem)
        {
            this.ItemNumber = orderItem.ItemNumber;
            this.ItemDescription = orderItem.ItemDescription;
            this.UM = orderItem.UM;
            this.UMPrice = orderItem.UMPrice;
            this.UnitPrice = orderItem.UnitPrice;
            this.SalesCat1 = orderItem.SalesCat1;
            this.SalesCat5 = orderItem.SalesCat5;
            this.ActualQtyOnHand = orderItem.ActualQtyOnHand;
            this.OrderQty = orderItem.OrderQty;
            this.PickedQuantityInPrimaryUOM = 0;
            this.ItemStatus = 'U';
            this.UMConversionFactor = orderItem.UMConversionFactor;
            this.OrderQtyInPrimaryUOM = Convert.ToInt32(orderItem.OrderQty * orderItem.UMConversionFactor);
            this.PrimaryUM = orderItem.PrimaryUM;
            this.AvailableQty = orderItem.AvailableQty;
            this.QtyOnHand = orderItem.QtyOnHand;
            this.StkType = orderItem.StkType;
        }


        [DataMember]
        public int LastManualPickReasonCode
        {
            get
            {
                return _LastManualPickReasonCode;
            }
            set
            {
                _LastManualPickReasonCode = value;
            }
        }

        [DataMember]
        public int AvailableQuantity
        {
            get
            {
                return AvailableQty - PickedQuantityInPrimaryUOM;
            }
            set
            {
                _AvailableQuantity = value;
                OnPropertyChanged("AvailableQuantity");
            }
        }

        [DataMember]
        public int ExceptionQtyInOrderUOM
        {
            get
            {
                return Convert.ToInt32(_ExceptionQtyInPrimaryUOM / UMConversionFactor);
            }
            set
            {
                _ExceptionQtyInOrderUOM = value;
                OnPropertyChanged("ExceptionQtyInOrderUOM");
                OnPropertyChanged("ExceptionQtyInPrimaryUOM");
                OnPropertyChanged("ItemStatus");

            }
        }

        [DataMember]
        public int ExceptionQtyInPrimaryUOM
        {
            get
            {
                return _ExceptionQtyInPrimaryUOM;
            }
            set
            {
                _ExceptionQtyInPrimaryUOM = value;
                OnPropertyChanged("ExceptionQtyInPrimaryUOM");
                OnPropertyChanged("ExceptionQtyInOrderUOM");
                OnPropertyChanged("ItemStatus");

            }
        }

        [DataMember]
        public int OrderQtyInPrimaryUOM
        {
            get
            {
                return _OrderQtyInPrimaryUOM;
            }
            set
            {
                _OrderQtyInPrimaryUOM = value;
                OnPropertyChanged("OrderQtyInPrimaryUOM");
                OnPropertyChanged("ItemStatus");
            }
        }
        [DataMember]
        public int PickedQuantityInPrimaryUOM
        {
            get
            {
                return _PickQtyInPrimaryUOM;
            }
            set
            {
                _PickQtyInPrimaryUOM = value;
                OnPropertyChanged("PickedQuantityInPrimaryUOM");
                OnPropertyChanged("ItemStatus");
                OnPropertyChanged("AvailableQuantity");

            }
        }


        [DataMember]
        public string ExceptionReason
        {
            get
            {
                return _ExceptionReason;
            }
            set
            {
                _ExceptionReason = value;
                OnPropertyChanged("ExceptionReason");
            }
        }
        //U = UnderPick,O=OverPick,D=Done
        [DataMember]
        public char ItemStatus
        {
            get
            {
                if (PickedQuantityInPrimaryUOM > OrderQtyInPrimaryUOM)
                {
                    return 'O';
                }
                else if (PickedQuantityInPrimaryUOM < OrderQtyInPrimaryUOM)
                {
                    return 'U';
                }
                else if (PickedQuantityInPrimaryUOM == OrderQtyInPrimaryUOM)
                {
                    return 'D';
                }
                return 'U';
            }
            set
            {
                _ItemStatus = value;
                OnPropertyChanged("ItemStatus");
            }
        }
        public override string ToString()
        {
            return this.ItemNumber.ToString().Trim();
        }
    }

    public class PickItem : Domain.OrderItem, IComparable<PickItem>, IEquatable<PickItem>
    {
        #region Implementation for IComparable and IEquatable (Used for Sorting)
        public bool Equals(PickItem other)
        {
            if (this.ItemNumber.Equals(other.ItemNumber)) return true;
            return false;
        }
        public int CompareTo(PickItem other)
        {
            if (this.ItemNumber == other.ItemNumber) return 0;
            return (this.ItemNumber.CompareTo(other.ItemNumber));
        }
        #endregion

        #region Properties and Fields
        private string lastScanMode = string.Empty;

        public string LastScanMode
        {
            get { return lastScanMode; }
            set { lastScanMode = value; }
        }
        private int itemScanSeq = 0;

        public int ItemScanSeq
        {
            get { return itemScanSeq; }
            set { itemScanSeq = value; }
        }
        private string isOnHold = string.Empty;

        public string IsOnHold
        {
            get { return isOnHold; }
            set { isOnHold = value; }
        }
        private bool decrementAvailableQty = false;

        public bool DecrementAvailableQty
        {
            get { return decrementAvailableQty; }
            set { decrementAvailableQty = value; }
        }
        private int reasonCodeId = 0;

        public int ReasonCodeId
        {
            get { return reasonCodeId; }
            set { reasonCodeId = value; }
        }
        int _ManuallyPickCount = 0;
        public int ManuallyPickCount
        {
            get { return _ManuallyPickCount; }
            set { _ManuallyPickCount = value; OnPropertyChanged("ManuallyPickCount"); }
        }
        private double conversionFactor = 1;

        public double ConversionFactor
        {
            get { return conversionFactor; }
            set { conversionFactor = value; }
        }
        private int routeId = 0;

        public int RouteId
        {
            get { return routeId; }
            set { routeId = value; }
        }
        private int transactionID = 0;

        public int TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }

        private int transactionDetailID = 0;

        public int TransactionDetailID
        {
            get { return transactionDetailID; }
            set { transactionDetailID = value; }
        }
        private int transactionTypeId = 0;

        public int TransactionTypeId
        {
            get { return transactionTypeId; }
            set { transactionTypeId = value; }
        }

        private string transactionStatusId = string.Empty;


        public string TransactionStatusId
        {
            get { return transactionStatusId; }
            set { transactionStatusId = value; }
        }
        double _TransactionQty = 0;
        public double TransactionQty
        {
            get
            {
                return _TransactionQty;
            }
            set
            {
                _TransactionQty = value;
                OnPropertyChanged("TransactionQty");
            }
        }
        private string transactionUOM = string.Empty;

        public string TransactionUOM
        {
            get { return transactionUOM; }
            set { transactionUOM = value; }
        }
        int _PickedQty = 0;
        public int PickedQty
        {
            get
            {
                return _PickedQty;
            }
            set
            {
                _PickedQty = value;
                PickQtyPrimaryUOM = _PickedQty * ConversionFactor;
                if (!IsUnPickForVoid && ExceptionReasonCodeID != "3")
                {
                    if (value > TransactionQty)
                    {
                        ExceptionCount = Convert.ToInt32(value - (TransactionQty > AdjustmentQty / ConversionFactor ? TransactionQty : AdjustmentQty / ConversionFactor));
                    }
                    if (value < TransactionQty)
                    {
                        ExceptionCount = 0;
                    }
                    if (value == TransactionQty)
                    {
                        ExceptionCount = 0;
                    }
                }
                OnPropertyChanged("PickedQty");
            }
        }

        private double transactionQtyPrimaryUOM = 0;

        public double TransactionQtyPrimaryUOM
        {
            get { return transactionQtyPrimaryUOM; }
            set { transactionQtyPrimaryUOM = value; }
        }

        double _PickQtyPrimaryUOM = 0, _ExceptionCountInPrimaryUOM = 0;
        public double PickQtyPrimaryUOM
        {
            get
            {
                return _PickQtyPrimaryUOM;
            }
            set
            {
                _PickQtyPrimaryUOM = value;
                OnPropertyChanged("PickQtyPrimaryUOM");
            }
        }
        int _ExceptionCount = 0;
        public int ExceptionCount
        {
            get
            {
                return _ExceptionCount;// TransactionQty < PickedQty ? PickedQty - TransactionQty : 0;
            }
            set
            {
                _ExceptionCount = value;
                ExceptionCountInPrimaryUOM = _ExceptionCount * ConversionFactor;
                OnPropertyChanged("ExceptionCount");
            }
        }
        public double ExceptionCountInPrimaryUOM
        {
            get
            {
                return _ExceptionCountInPrimaryUOM;
            }
            set
            {
                _ExceptionCountInPrimaryUOM = value;
                OnPropertyChanged("ExceptionCountInPrimaryUOM");
            }
        }
        private bool isInException = false;

        public bool IsInException
        {
            get { return isInException; }
            set { isInException = value; }
        }
        string _ExceptionReasonCodeID = "1"; //Check the PickItemState enum for understanding
        string _ExceptionReasonCode = string.Empty;
        public string ExceptionReasonCode
        {
            get
            {
                return _ExceptionReasonCode;
            }
            set
            {
                _ExceptionReasonCode = value;
                OnPropertyChanged("ExceptionReasonCode");
            }
        }
        public string ExceptionReasonCodeID
        {
            get
            {
                return _ExceptionReasonCodeID;
            }
            set
            {
                _ExceptionReasonCodeID = value; OnPropertyChanged("ExceptionReasonCodeID");
            }
        }
        private string createdBy = string.Empty;

        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        private string createdDatetime = string.Empty;

        public string CreatedDatetime
        {
            get { return createdDatetime; }
            set { createdDatetime = value; }
        }
        private string updatedBy = string.Empty;

        public string UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }
        private string updatedDatetime = string.Empty;

        public string UpdatedDatetime
        {
            get { return updatedDatetime; }
            set { updatedDatetime = value; }
        }
        private double adjustmentQty = 0;
        public double AdjustmentQty
        {
            get { return adjustmentQty; }
            set { adjustmentQty = value; OnPropertyChanged("AdjustmentQty"); }
        }
        
        private int pickAdjusted = 0;
        public int PickAdjusted
        {
            get { return pickAdjusted; }
            set
            {
                pickAdjusted = value; OnPropertyChanged("PickAdjusted");
                if (PickAdjusted == 1)
                {
                    IsEnableScanner = false;
                }
            }
        }
        private bool isEnableScanner = true;

        public bool IsEnableScanner
        {
            get { return isEnableScanner; }
            set {
                //Visibility of scanner in pick screen is determined by this. here i m forcefully making it visible . Date:12/10/2015
                isEnableScanner = true;  //isEnableScanner = value; 
                OnPropertyChanged("IsEnableScanner"); 
            
            }
        }

        private bool isUnPickForVoid = false;

        public bool IsUnPickForVoid
        {
            get { return isUnPickForVoid; }
            set { isUnPickForVoid = value; OnPropertyChanged("IsUnPickForVoid"); }
        }


        private bool _isManuallyPicking = false;
        public bool IsManuallyPicking
        {
            get
            {
                return _isManuallyPicking;
            }
            set
            {
                _isManuallyPicking = value;
                OnPropertyChanged("IsManuallyPicking");
            }
        }
        private double shippedQty = 0;

        /// <summary>
        /// Get or set quantity shipped from server
        /// </summary>
        public double ShippedQty
        {
            get { return shippedQty; }
            set
            {
                shippedQty = value;
                OnPropertyChanged("ShippedQty");
            }
        }

        private double transactionQtyDisplay = 0;

        public double TransactionQtyDisplay
        {
            get { return transactionQtyDisplay; }
            set { transactionQtyDisplay = value; OnPropertyChanged("TransactionQtyDisplay"); }
        }

        //private int heldQty = 0;
        //public int HeldQty
        //{
        //    get { return heldQty; }
        //    set { heldQty = value; OnPropertyChanged("HeldQty"); }
        //}

        private int pickDetailID = 0;
        public int PickDetailID
        {
            get { return pickDetailID; }
            set { pickDetailID = value; OnPropertyChanged("PickDetailID"); }
        }

        private string exceptionMessage = string.Empty;
        public string ExceptionMessage
        {
            get { return exceptionMessage; }
            set { exceptionMessage = value; OnPropertyChanged("ExceptionMessage"); }
        }
        
        #endregion
    }
    public class PickOrderItemExt : PickItem, IComparable<PickOrderItemExt>, IEquatable<PickOrderItemExt>
    {
        #region Implementation for IComparable and IEquatable (Used for Sorting)
        public bool Equals(PickOrderItemExt other)
        {
            if (this.ItemNumber.Equals(other.ItemNumber)) return true;
            return false;
        }
        public int CompareTo(PickOrderItemExt other)
        {
            if (this.ItemNumber == other.ItemNumber) return 0;
            return (this.ItemNumber.CompareTo(other.ItemNumber));
        }
        #endregion

        public string Name { get; set; }
        public string Address { get; set; }
    }
}
