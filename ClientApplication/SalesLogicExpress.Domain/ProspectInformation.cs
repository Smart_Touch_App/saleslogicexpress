﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ProspectInformation : Prospect
    {
        string _Address1;
        string _Address2;
        string _Address3;
        string _Address4;
        string _BusinessName;
        string _BuyingGroup;
        string _LocationCount;

        public bool InitDisable
        {
            get;
            set;
        }
        public Boolean IsProspectSaveAllowed
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Address1) && !string.IsNullOrWhiteSpace(CustInfoCity) && !string.IsNullOrWhiteSpace(CustInfoState) && !string.IsNullOrWhiteSpace(CustInfoZip) && CustInfoZip.Length >= 5 && InitDisable == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set { OnPropertyChanged("IsProspectSaveAllowed"); }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        public string CustInfoZip
        {
            get
            {
                return _CustInfoZip;
            }
            set
            {
                InitDisable = true;
                _CustInfoZip = value;
                OnPropertyChanged("CustInfoZip");
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        public string CustInfoCity
        {
            get
            {
                return _CustInfoCity;
            }
            set
            {
                InitDisable = true;
                _CustInfoCity = value;
                OnPropertyChanged("CustInfoCity");
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]
        public string CustInfoState
        {
            get
            {
                return _CustInfoState;
            }
            set
            {
                InitDisable = true;
                _CustInfoState = value;
                OnPropertyChanged("CustInfoState");
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = " ")]

        public string BusinessName
        {
            get
            {
                return _BusinessName;
            }
            set
            {
                InitDisable = true;
                _BusinessName = value;
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string SegmentType
        {
            get
            {
                return _SegmentType;
            }
            set
            {
                InitDisable = true;
                _SegmentType = value;
                OnPropertyChanged("SegmentType");
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string BuyingGroup
        {
            get
            {
                return _BuyingGroup;
            }
            set
            {
                InitDisable = true;
                _BuyingGroup = value;
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string LocationCount
        {
            get
            {
                return _LocationCount;
            }
            set
            {
                InitDisable = true;
                _LocationCount = value;
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string CurrentDate { get; set; }

        public string Address1
        {
            get
            {
                return _Address1;
            }
            set
            {
                InitDisable = true;
                _Address1 = value;
                OnPropertyChanged("Address1");
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string Address2
        {
            get
            {
                return _Address2;
            }
            set
            {
                InitDisable = true;
                _Address2 = value;
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string Address3
        {
            get
            {
                return _Address3;
            }
            set
            {
                InitDisable = true;
                _Address3 = value;
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string Address4
        {
            get
            {
                return _Address4;
            }
            set
            {
                InitDisable = true;
                _Address4 = value;
                OnPropertyChanged("IsProspectSaveAllowed");
            }
        }
        public string MailingName { get; set; }
        public string TaxExemptCertificate { get; set; }

        public string SearchTypeCode { get; set; }
        public string OperatingUnitCode { get; set; }
        public string ParentCode { get; set; }
        public string RegionCode { get; set; }
        public string BillToCode { get; set; }
        public string DistrictCode { get; set; }
        public string RemitToCode { get; set; }
        public string BranchCode { get; set; }
        public string KAMNAMStreetCode { get; set; }
        public string ChainCode { get; set; }
        public string KindOfBusinessAccountCode { get; set; }
        public string RouteCode { get; set; }
        public string FreightHandlingCode { get; set; }
        public string StopCode { get; set; }

        public string SearchTypeDesc { get; set; }
        public string OperatingUnitDesc { get; set; }
        public string ParentDesc { get; set; }
        public string RegionDesc { get; set; }
        public string BillToDesc { get; set; }
        public string DistrictDesc { get; set; }
        public string RemitToDesc { get; set; }
        public string BranchDesc { get; set; }
        public string KAMNAMStreetDesc { get; set; }
        public string ChainDesc { get; set; }
        public string KindOfBusinessAccountDesc { get; set; }
        public string RouteDesc { get; set; }
        public string FreightHandlingCodeDesc { get; set; }
        public string StopCodeDesc { get; set; }

        string _SegmentType;
        string _CustInfoState;
        string _CustInfoCity;
        string _CustInfoZip;
    }
}
