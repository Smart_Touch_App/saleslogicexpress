﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class RouteSettlementSettlementsModel
    {

        private string settlementNo;

        public string SettlementNo
        {
            get { return settlementNo; }
            set { settlementNo = value; }
        }
        private string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        

        private DateTime date;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }
        private string originator;

        public string Originator
        {
            get { return originator; }
            set { originator = value; }
        }
        private string verifier;

        public string Verifier
        {
            get { return verifier; }
            set { verifier = value; }
        }

        private decimal settlementAmount;

        public decimal SettlementAmount
        {
            get { return settlementAmount; }
            set { settlementAmount = value; }
        }
        private decimal exceptionAmount;

        public decimal ExceptionAmount
        {
            get { return exceptionAmount; }
            set { exceptionAmount = value; }
        }
        private string comment;

        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        private string settlementID;

        public string SettlementID
        {
            get { return settlementID; }
            set { settlementID = value; }
        }

        private string originatorName;

        public string OriginatorName
        {
            get { return originatorName; }
            set { originatorName = value; }
        }
        private string verifierName;

        public string VerifierName
        {
            get { return verifierName; }
            set { verifierName = value; }
        }
    }
}
