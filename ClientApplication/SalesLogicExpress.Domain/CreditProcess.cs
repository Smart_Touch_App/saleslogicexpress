﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class CreditProcess : Helpers.ModelBase
    {
        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
            set { customerNo = value; OnPropertyChanged("CustomerNo"); }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; OnPropertyChanged("CustomerName"); }
        }
        private string routeID;
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; }
        }
        private int receiptID;
        public int ReceiptID
        {
            get { return receiptID; }
            set { receiptID = value; OnPropertyChanged("ReceiptID"); }
        }

        private int receiptNumber=-1;
        public int ReceiptNumber
        {
            get { return receiptNumber; }
            set { receiptNumber = value; OnPropertyChanged("ReceiptNumber"); }
        }

        private DateTime creditMemoDate;
        public DateTime CreditMemoDate
        {
            get { return creditMemoDate; }
            set { creditMemoDate = value; OnPropertyChanged("CreditMemoDate"); }
        }

        private int statusID;
        public int StatusID
        {
            get { return statusID; }
            set { statusID = value; OnPropertyChanged("StatusID"); }
        }

        private string statusCode;
        public string StatusCode
        {
            get { return statusCode; }
            set { statusCode = value; OnPropertyChanged("StatusCode"); }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }
        private string voidReasonID;
        public string VoidReasonID
        {
            get { return voidReasonID; }
            set { voidReasonID = value; OnPropertyChanged("VoidReasonID"); }
        }

        private string voidReason;
        public string VoidReason
        {
            get { return voidReason; }
            set { voidReason = value; OnPropertyChanged("VoidReason"); }
        }

        private string creditReasonID;
        public string CreditReasonID
        {
            get { return creditReasonID; }
            set { creditReasonID = value; OnPropertyChanged("CreditReasonID"); }
        }

        private string creditReason;
        public string CreditReason
        {
            get { return creditReason; }
            set { creditReason = value; OnPropertyChanged("CreditReason"); }
        }

        private string creditNote;
        public string CreditNote
        {
            get { return creditNote; }
            set { creditNote = value; OnPropertyChanged("CreditNote"); }
        }

        private string creditAmount;
        public string CreditAmount
        {
            get { return creditAmount; }
            set { creditAmount = value; OnPropertyChanged("CreditAmount"); }
        }

        private Byte[] customerSignature;

        private bool isVisible;
        public bool IsVisible
        {
            get
            {
                return this.isVisible;
            }
            set
            {
                this.isVisible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        private int creditSeqID = 1;

        public int CreditSeqID
        {
            get { return creditSeqID; }
            set { creditSeqID = value; OnPropertyChanged("CreditSeqID"); }
        }

        private string routeSettlementId;

        public string RouteSettlementId
        {
            get { return routeSettlementId; }
            set { routeSettlementId = value; OnPropertyChanged("RouteSettlementId"); }
        }
    }
}
