﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ReturnItem : PickItem
    {


        int previouslyReturnedQty = 0;
        [DataMember]
        public int PreviouslyReturnedQty
        {
            get
            {
                return previouslyReturnedQty;
            }
            set
            {
                previouslyReturnedQty = value;
                OnPropertyChanged("PreviouslyReturnedQty");
            }

        }
        int returnedQty = 0;
        [DataMember]
        public int ReturnedQty
        {
            get
            {
                return returnedQty;
            }
            set
            {


                returnedQty = value;
                OnPropertyChanged("ReturnedQty");
            }
        }


        int nonSellableQty;
        [DataMember]
        public int NonSellableQty
        {
            get
            {
                return nonSellableQty;
            }
            set
            {

                nonSellableQty = value;
                OnPropertyChanged("NonSellableQty");
            }
        }


        int returnHeldQty;
        [DataMember]
        public int ReturnHeldQty
        {
            get
            {
                return returnHeldQty;
            }
            set
            {


                returnHeldQty = value;
                OnPropertyChanged("ReturnHeldQty");
            }
        }

        int qtyAvailableToReturn = 0;
        public int QtyAvailableToReturn
        {
            get
            {
                return qtyAvailableToReturn;
            }
            set
            {
                qtyAvailableToReturn = value;
                if (qtyAvailableToReturn == 0)
                {
                    IsItemEnabled = false;
                }
                OnPropertyChanged("QtyAvailableToReturn");
            }
        }


        int returnQty;
        public int ReturnQty
        {
            get
            {
                return returnQty;
            }
            set
            {


                returnQty = value;
                OnPropertyChanged("ReturnQty");
            }
        }


        bool isSelectedItem;
        public bool IsSelectedItem
        {
            get
            {
                return isSelectedItem;
            }
            set
            {
                if (isSelectedItem != value)
                {
                    isSelectedItem = value;
                    OnPropertyChanged("IsSelectedItem");
                }
                if (QtyAvailableToReturn == 0)
                    isSelectedItem = false;
            }
        }

        bool _IsItemEnabled = true;
        public bool IsItemEnabled
        {
            get
            {
                return _IsItemEnabled;
            }
            set
            {


                _IsItemEnabled = value;
                OnPropertyChanged("IsItemEnabled");
            }
        }

        int _ReturnOrderId;
        public int ReturnOrderID
        {
            get
            {
                return _ReturnOrderId;
            }
            set
            {


                _ReturnOrderId = value;
                OnPropertyChanged("ReturnOrderID");
            }
        }



        int returnQtyInPrimaryUoM = 0;
        public int ReturnQtyInPrimaryUoM
        {
            get
            {
                return returnQtyInPrimaryUoM;
            }
            set
            {


                returnQtyInPrimaryUoM = value;
                OnPropertyChanged("ReturnQtyInPrimaryUoM");
            }
        }

        bool isCompleteAndSignPressed = true;
        public bool IsCompleteAndSignPressed
        {
            get
            {
                return isCompleteAndSignPressed;
            }
            set
            {
                isCompleteAndSignPressed = value;
                OnPropertyChanged("IsCompleteAndSignPressed");
            }
        }

        int nonSellableQtyInPrimaryUoM = 1;
        public int NonSellableQtyInPrimaryUoM
        {
            get
            {
                return nonSellableQtyInPrimaryUoM;
            }
            set
            {


                nonSellableQtyInPrimaryUoM = value;
                OnPropertyChanged("ReturnQtyInPrimaryUoM");
            }
        }


        int _OrderStateID;
        public int OrderStateID
        {
            get
            {
                return _OrderStateID;
            }
            set
            {
                _OrderStateID = value;
                OnPropertyChanged("OrderStateID");
            }
        }

        string _OrderState;
        public string OrderState
        {
            get
            {
                return _OrderState;
            }
            set
            {
                _OrderState = value;
                OnPropertyChanged("OrderState");
            }
        }

        int _SettlementID;
        public int SettlementID
        {
            get
            {
                return _SettlementID;
            }
            set
            {
                _SettlementID = value;
                OnPropertyChanged("SettlementID");
            }
        }
        double energySurchargeAmt = 0;

        public double EnergySurchargeAmt
        {
            get { return energySurchargeAmt; }
            set
            {
                energySurchargeAmt = value;
                OnPropertyChanged("EnergySurchargeAmt");
            }
        }



        decimal roExtendedPrice;
        public decimal ROExtendedPrice
        {
            get
            {
                return roExtendedPrice;
            }
            set
            {


                roExtendedPrice = value;
                OnPropertyChanged("ROExtendedPrice");
            }
        }


        decimal roTaxAmount;
        public decimal ROTaxAmount
        {
            get
            {
                return roTaxAmount;
            }
            set
            {


                roTaxAmount = value;
                OnPropertyChanged("ROTaxAmount");
            }
        }

        private bool isAllowManualPick = false;

        public bool IsAllowManualPick
        {
            get { return isAllowManualPick; }
            set { isAllowManualPick = value; OnPropertyChanged("IsAllowManualPick"); }
        }

        private bool isEnableScannerInException = false;

        public bool IsEnableScannerInException
        {
            get { return isEnableScannerInException; }
            set { isEnableScannerInException = value; OnPropertyChanged("IsEnableScannerInException"); }

        }

        private bool isFilterAllowed = false;

        public bool IsFilterAllowed
        {
            get { return isFilterAllowed; }
            set { isFilterAllowed = value; OnPropertyChanged("IsFilterAllowed"); }

        }
    }
}
