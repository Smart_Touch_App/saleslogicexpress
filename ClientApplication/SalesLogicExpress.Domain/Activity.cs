﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public class Activity
    {
        public Activity()
        {
            ActivityStart = DateTime.SpecifyKind(ActivityStart, DateTimeKind.Utc);
            ActivityEnd = DateTime.SpecifyKind(ActivityEnd, DateTimeKind.Utc);
            SettlementID = string.Empty;
        }

        public string ActivityID
        {
            get;
            set;
        }
        public bool IsTxActivity
        {
            get;
            set;
        }
        public string ActivityType
        {
            get;
            set;
        }
        public string ActivityDescription
        {
            get;
            set;
        }

        public string ActivityHeaderID
        {
            get;
            set;
        }

        public string RouteID
        {
            get;
            set;
        }

        public string ActivityDetailClass
        {
            get;
            set;
        }

        public string ActivityDetails
        {
            get;
            set;
        }

        public string ActivityStatus
        {
            get;
            set;
        }
        public DateTime ActivityStart
        {
            get;
            set;
        }
        public DateTime ActivityEnd
        {
            get;
            set;
        }

        public string StopInstanceID
        {
            get;
            set;
        }

        public string CustomerID
        {
            get;
            set;
        }

        public string SettlementID
        {
            get;
            set;
        }

        public int ParentID
        {
            get;
            set;
        }
        /// <summary>
        /// This Property stores the ID of the Flow/Transaction, against which the activity is logged e.g This may contain OrderID,ReturnOrderID,CreditMemoID
        /// </summary>
        public int ActivityFlowID
        {
            get;
            set;
        }
        /// <summary>
        /// Used in Junction with the ActivityFlowID, determines the Type of the Flow for which the ActivityFlowID maps to, e.g ActivityFlowID=1023,ActivityFlowTypeID=2 -> OrderID Type
        /// </summary>
        public int ActivityFlowTypeID
        {
            get;
            set;
        }
    }

}
