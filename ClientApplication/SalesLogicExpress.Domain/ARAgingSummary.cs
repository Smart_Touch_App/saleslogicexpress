﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ARAgingSummary
    {
        private string customerId;
        private string customerName;
        private string paymentTerm;
        private int openInvoiceCount;
        private decimal openInvoiceGrossAmt;
        private decimal openInvoiceOpenAmt;
        private string age;

        public string Age
        {
            get { return age; }
            set { age = value; }
        }

        public decimal OpenInvoiceOpenAmt
        {
            get { return openInvoiceOpenAmt; }
            set { openInvoiceOpenAmt = value; }
        }

        public decimal OpenInvoiceGrossAmt
        {
            get { return openInvoiceGrossAmt; }
            set { openInvoiceGrossAmt = value; }
        }

        public int OpenInvoiceCount
        {
            get { return openInvoiceCount; }
            set { openInvoiceCount = value; }
        }

        public string PaymentTerm
        {
            get { return paymentTerm; }
            set { paymentTerm = value; }
        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
        public string CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }
    }
}
