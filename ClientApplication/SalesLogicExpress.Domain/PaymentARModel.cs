﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;
using SalesLogicExpress.Domain.Helpers;
using Telerik.Windows.Controls.GridView;
using System.Windows;
using System.Windows.Controls;

namespace SalesLogicExpress.Domain
{
    public class PaymentARModel : Helpers.ModelBase
    {
        public class OpenAR : Helpers.ModelBase
        {
            #region Variable and object declaration

            private bool _SelectAllInvoices = false;
            private int receiptId;
            private decimal aR30Days;
            private decimal aR60Days;
            private decimal aR90Days;
            private decimal aR90MoreDays;
            private decimal openInvices;
            private decimal unappliedReceipts;
            private decimal currentUnappliedReceipts;
            private decimal totalBalance;
            private decimal selectedInvoiceTotal;
            private decimal newRemainingInvoceBalance;
            private decimal unapplidAmount;
            private string paymentAmount;
            private string checkNo = string.Empty;
            private string checkDate;
            private bool isChecked = false;
            private bool isMakePaymentEnable = false;
            private bool _ShowAllInvoices = false;
            private ObservableCollection<InvoiceDetails> invoiceDetailsList;
            private ObservableCollection<InvoiceDetails> allInvoiceDetailsList;
            private ObservableCollection<InvoiceDetails> openInvoiceDetailsList;
            private ObservableCollection<InvoiceDetails> selectedItems;
            private InvoiceDetails selectedItem;
            public DelegateCommand PrintReceipt { get; set; }

            #endregion

            #region Constructor

            public OpenAR()
            {
                this.InitializeCommands();
            }
            #endregion

            #region Properies

            /// <summary>
            /// Handles the toggle selection of the select all check box
            /// </summary>
            public DelegateCommand ToggleAllSelectOpenInvoices { get; set; }
            
            /// <summary>
            /// Handles the togle selection of individual rows
            /// </summary>
            public DelegateCommand ToggleSelectOpenInvoice { get; set; }

            /// <summary>
            /// Select / de-select all open invoice on customer ledger screen
            /// </summary>
            public bool SelectAllInvoices
            {
                get
                {
                    return _SelectAllInvoices;
                }
                set
                {
                    _SelectAllInvoices = value;
                    OnPropertyChanged("SelectAllInvoices");
                }
            }

            public InvoiceDetails SelectedItem
            {
                get
                {
                    return this.selectedItem;
                }
                set
                {
                    if (selectedItem != value)
                    {
                        this.selectedItem = value;
                        OnPropertyChanged("SelectedItem");
                    }
                }
            }
            public ObservableCollection<InvoiceDetails> SelectedItems
            {
                get
                {
                    if (selectedItems == null)
                    {
                        selectedItems = new ObservableCollection<InvoiceDetails>();
                    }

                    return this.selectedItems;
                }
                set
                {
                    this.selectedItems = value;
                    OnPropertyChanged("SelectedItems");
                }

            }
            public string CheckDate
            {
                get
                {
                    return this.checkDate; // = DateTime.Now.Date.ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                set
                {
                    this.checkDate = value;
                }
            }
            decimal tempPaymentAmt;
            public string PaymentAmount
            {
                get
                {
                    return this.paymentAmount;
                }
                set
                {
                    if(value != null)
                        this.paymentAmount = value.IndexOf(".") != value.Length - 1 ? Math.Round(Convert.ToDecimal(string.IsNullOrEmpty(value) ? "0" : value), 2).ToString() : value;

                    if (!string.IsNullOrEmpty(value))
                    {
                        UnapplidAmount = Math.Round(Convert.ToDecimal(this.PaymentAmount) > this.SelectedInvoiceTotal ? Convert.ToDecimal(this.PaymentAmount) - this.SelectedInvoiceTotal : 0, 2);
                    }
                    else
                    {
                        UnapplidAmount = 0;
                    }
                    OnPropertyChanged("PaymentAmount");
                    OnPropertyChanged("IsMakePaymentEnable");
                }
            }
            public string PaymentInfo
            {
                get
                {
                    return strPaymentInfo;
                }
                set
                {
                    strPaymentInfo = value;
                    OnPropertyChanged("PaymentInfo");
                }
            }
            public string CheckNo
            {
                get
                {
                    return this.checkNo;
                }
                set
                {
                    this.checkNo = value;
                    OnPropertyChanged("CheckNo");
                    OnPropertyChanged("IsMakePaymentEnable");
                }
            }
            public int ReceiptId
            {
                get
                {
                    return this.receiptId;
                }
                set
                {
                    this.receiptId = value;
                    OnPropertyChanged("ReceiptId");
                }
            }
            public decimal CurrentUnappliedReceipts
            {
                get
                {
                    return this.currentUnappliedReceipts;
                }
                set
                {
                    this.currentUnappliedReceipts = value;
                    OnPropertyChanged("CurrentUnappliedReceipts");
                }
            }
            public ObservableCollection<InvoiceDetails> AllInvoiceDetailsList
            {
                get
                {
                    return this.allInvoiceDetailsList;
                }
                set
                {
                    this.allInvoiceDetailsList = value;
                    OnPropertyChanged("AllInvoiceDetailsList");
                }

            }
            public ObservableCollection<InvoiceDetails> InvoiceDetailsList
            {
                get
                {
                    return this.invoiceDetailsList;
                }
                set
                {
                    this.invoiceDetailsList = value;
                    OnPropertyChanged("InvoiceDetailsList");
                    OnPropertyChanged("OpenInvoiceDetailsList");
                }
            }
            public bool ShowAllInvoices
            {
                get
                {
                    return this._ShowAllInvoices;
                }
                set
                {
                    this._ShowAllInvoices = value;

                    if (this._ShowAllInvoices)
                    {
                        InvoiceDetailsList = AllInvoiceDetailsList;
                    }
                    else
                    {
                        InvoiceDetailsList = OpenInvoiceDetailsList;
                    }
                    this.SelectAllInvoices = false;
                    OnPropertyChanged("ShowAllInvoices");
                }
            }


            public ObservableCollection<InvoiceDetails> OpenInvoiceDetailsList
            {
                get
                {
                    if (this.allInvoiceDetailsList != null)
                    {
                        return this.allInvoiceDetailsList.Where(item => Math.Round(item.OpenAmountInvDtl,2) != 0).ToObservableCollection<InvoiceDetails>();
                    }
                    else
                    {
                        return this.allInvoiceDetailsList;

                    }
                }

            }
            public bool IsMakePaymentEnable
            {
                get
                {
                    if (string.IsNullOrEmpty(this.PaymentAmount) || Convert.ToDecimal(this.PaymentAmount) == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return (Convert.ToDecimal(this.PaymentAmount) >= this.SelectedInvoiceTotal) && (IsChecked ? !string.IsNullOrEmpty(CheckNo) && CheckNo.Length > 0 : string.IsNullOrEmpty(CheckNo));
                    }
                }
                set
                {
                    this.isMakePaymentEnable = value;
                    OnPropertyChanged("IsMakePaymentEnable");
                }
            }
            public bool IsChecked
            {
                get
                {
                    return this.isChecked;
                }
                set
                {
                    this.isChecked = value;
                    if (!IsChecked)
                    {
                        CheckNo = string.Empty;
                    }
                    OnPropertyChanged("IsChecked");
                    OnPropertyChanged("IsMakePaymentEnable");
                }
            }

            public decimal UnapplidAmount
            {
                get
                {
                    return this.unapplidAmount;
                }
                set
                {
                    this.unapplidAmount = Math.Round(value, 2);
                    OnPropertyChanged("UnapplidAmount");
                }
            }
            public decimal NewRemainingInvoceBalance
            {
                get
                {
                    return this.newRemainingInvoceBalance;
                }
                set
                {
                    this.newRemainingInvoceBalance = Math.Round(value, 2);
                    OnPropertyChanged("NewRemainingInvoceBalance");
                }
            }
            public decimal SelectedInvoiceTotal
            {
                get
                {
                    return this.selectedInvoiceTotal;
                }
                set
                {
                    this.selectedInvoiceTotal = Math.Round(value, 2); ;
                    OnPropertyChanged("SelectedInvoiceTotal");
                    OnPropertyChanged("IsMakePaymentEnable");
                }
            }
            public decimal TotalBalance
            {
                get
                {
                    return this.totalBalance;
                }
                set
                {
                    this.totalBalance = Math.Round(value, 2);
                    OnPropertyChanged("TotalBalance");
                }
            }
            public decimal UnappliedReceipts
            {
                get
                {
                    return this.unappliedReceipts;
                }
                set
                {
                    this.unappliedReceipts = Math.Round(value, 2);
                    OnPropertyChanged("UnappliedReceipts");
                }
            }
            public decimal OpenInvices
            {
                get
                {
                    return this.openInvices;
                }
                set
                {
                    this.openInvices = Math.Round(value, 2);
                    OnPropertyChanged("OpenInvices");
                }
            }
            public decimal AR90MoreDays
            {
                get
                {
                    return this.aR90MoreDays;
                }
                set
                {
                    this.aR90MoreDays = value;
                    OnPropertyChanged("AR90MoreDays");
                }
            }
            public decimal AR90Days
            {
                get
                {
                    return this.aR90Days;
                }
                set
                {
                    this.aR90Days = value;
                    OnPropertyChanged("AR90Days");
                }
            }
            public decimal AR60Days
            {
                get
                {
                    return this.aR60Days;
                }
                set
                {
                    this.aR60Days = value;
                    OnPropertyChanged("AR60Days");
                }
            }
            public decimal AR30Days
            {
                get
                {
                    return this.aR30Days;
                }
                set
                {
                    this.aR30Days = value;
                    OnPropertyChanged("AR30Days");
                }
            }
            public string strPaymentInfo { get; set; }

            #endregion

            #region Methods

            private void InitializeCommands()
            {
                this.ToggleAllSelectOpenInvoices = new DelegateCommand((param) =>
                {
                    //Compare count of selected invoice and count of open invoices, 
                    //If they match then selecte all invoice, else continue;

                    if(invoiceDetailsList.Count>0)
                    {
                        foreach (InvoiceDetails item in this.InvoiceDetailsList)
                        {
                            if(item.OpenAmountInvDtl!=0)
                            {
                                item.IsSelected = this.SelectAllInvoices;
                            }
                        }
                    }
                    else
                    {
                        this.SelectAllInvoices = false;
                    }
                });

                this.ToggleSelectOpenInvoice = new DelegateCommand((param) =>
                {
                    //Compare count of selected invoice and count of open invoices, 
                    //If they match then selecte all invoice, else continue;
                    int openInvoiceCount = 0;
                    int selectedInvoiceCount = 0;

                    if (invoiceDetailsList.Count > 0)
                    {
                        openInvoiceCount = InvoiceDetailsList.Count(o => o.OpenAmountInvDtl != 0);
                        selectedInvoiceCount = InvoiceDetailsList.Count(o => o.IsSelected == true && o.OpenAmountInvDtl != 0);
    
                        if(openInvoiceCount==selectedInvoiceCount)
                        {
                            this.SelectAllInvoices = true;
                        }
                        else
                        {
                            this.SelectAllInvoices = false;
                        }
                    }
                });
            }

            #endregion
        }

        /// <summary>
        /// Represents the Receipt History data
        /// </summary>
        public class ReceiptHistory : Helpers.ModelBase
        {
            #region Variable and object declaration

            private VoidReceipt voidReceiptItem = null;
            private EditReceipt editReceiptItem = null;

            private bool _IsCash = false;
            private bool _IsCheck = false;
            private string _CheckNo = "";
            #endregion

            #region Properties

            public EditReceipt EditReceiptItem
            {
                get
                {
                    return this.editReceiptItem;
                }
                set
                {
                    this.editReceiptItem = value;
                    OnPropertyChanged("EditReceiptItem");
                }
            }

            public VoidReceipt VoidReceiptItem
            {
                get
                {
                    return this.voidReceiptItem;
                }
                set
                {
                    this.voidReceiptItem = value;
                    OnPropertyChanged("VoidReceiptItem");
                }
            }

            #endregion


            #region Properties for the Model

            public string ReceiptId
            {
                get;
                set;
            }


            public string ReceiptNo
            {
                get;
                set;
            }

            public decimal ReceiptAmount
            {
                get;
                set;
            }

            public string ReceiptType
            {
                get;
                set;
            }

            public string CheckNo
            {
                get
                {
                    if (_CheckNo == null)
                        _CheckNo = "";
                    return _CheckNo;
                }
                set
                {
                    _CheckNo = value;
                    OnPropertyChanged("CheckNo");
                    this.PaymentInfo = "Check No. " + this.CheckNo;
                }
            }

            public string CheckDate
            {
                get;
                set;
            }

            public string ReceiptDate
            {
                get;
                set;
            }

            public string Status
            {
                get;
                set;
            }
            public List<InvoiceDetails> PaidInvoices
            {
                get;
                set;
            }

            public bool IsCash
            {
                get { return _IsCash; }
                set
                {
                    _IsCash = value;
                    OnPropertyChanged("IsCash");

                    if (value)
                    {
                        this.PaymentInfo = "Cash";
                        this.IsCheck = !value;
                    }

                }
            }

            private string creditReason;

            public string CreditReason
            {
                get { return creditReason; }
                set { creditReason = value; OnPropertyChanged("CreditReason"); }
            }
            public bool IsCheck
            {
                get { return _IsCheck; }
                set
                {
                    _IsCheck = value;
                    OnPropertyChanged("IsCheck");

                    if (value)
                    {
                        this.IsCash = !value;
                        this.PaymentInfo = "Check No. " + this.CheckNo;
                    }
                }
            }

            private string strPaymentInfo = "";
            public string PaymentInfo
            {
                get
                {
                    return strPaymentInfo;
                }
                set
                {
                    strPaymentInfo = value;
                    OnPropertyChanged("PaymentInfo");
                }
            }

            private string _ReasonCode = string.Empty;

            public string ReasonCode
            {
                get { return _ReasonCode; }
                set { _ReasonCode = value; }
            }

            private string _SettlementId = string.Empty; 

            /// <summary>
            /// Get or set settlement id, blank if not settled
            /// </summary>
            public string SettlementId
            {
                get
                {
                    return _SettlementId;
                }
                set
                {
                    _SettlementId = value;
                    OnPropertyChanged("SettlementId");
                }
            }
            #endregion

            private bool isCreditMemo = false;
            public bool IsCreditMemo
            {
              get { return isCreditMemo; }
              set { isCreditMemo = value; OnPropertyChanged("IsCreditMemo");}
            }
        }

        /// <summary>
        /// Represents invoice details used in Receipts 
        /// </summary>
        public class InvoiceDetails : Helpers.ModelBase
        {
            #region Variable and object declaration

            string invoiceNoInvDtl;
            string dateInvDtl;
            decimal grossAmountInvDtl;
            decimal openAmountInvDtl;
            int agingInvDtl;
            int invoiceIdInvDtl;
            bool isVisible;

            public DelegateCommand SelectOpenInvoice { get; set; }
            #endregion

            #region Constructor and destructor

            public InvoiceDetails()
            {
                this.SelectOpenInvoice = new DelegateCommand((param) =>
                {
                    if (param != null)
                    {
                        bool IsHeaderCheckBox = (bool)param.GetType().InvokeMember("IsHeaderCheckBox", System.Reflection.BindingFlags.GetProperty, null, param, null);
                        CheckBox checkBoxControl = (CheckBox)param.GetType().InvokeMember("CheckBoxControl", System.Reflection.BindingFlags.GetProperty, null, param, null);
                        var gridViewRows = ((RadGridView)param.GetType().InvokeMember("OpenInvoiceGrid", System.Reflection.BindingFlags.GetProperty, null, param, null)).ChildrenOfType<GridViewRow>();

                        if (gridViewRows.Count() > 0)
                        {
                            int SelectOpenInvoices = (from o in gridViewRows
                                                      where ((o.Item!=null) && (o.Item as InvoiceDetails).OpenAmountInvDtl != 0 && o.IsSelected == true)
                                                      select o).Count();

                            int availableInvoices = (from o in gridViewRows
                                                     where (o.Item != null) && (o.Item as InvoiceDetails).OpenAmountInvDtl != 0
                                                     select o).Count();

                            if (SelectOpenInvoices == availableInvoices)
                                checkBoxControl.IsChecked = true;
                            else
                                checkBoxControl.IsChecked = false;
                        }
                        else
                        {
                            checkBoxControl.IsChecked = false;
                        }
                    }
                });
            }
            #endregion

            #region Properties

            public bool IsVisible
            {
                get
                {
                    return this.isVisible;
                }
                set
                {
                    this.isVisible = value;
                    OnPropertyChanged("IsVisible");
                }
            }

            private bool _IsEnabled = true;

            /// <summary>
            /// Get or set row enable status on customer ledger screen.  
            /// </summary>
            public bool IsEnabled
            {
                get
                {
                    return _IsEnabled;
                }
                set
                {
                    _IsEnabled = value;
                    OnPropertyChanged("IsEnabled");
                }
            }

            /// <summary>
            /// Get or set selection status of row on customer ledger screen.
            /// </summary>
             private bool _IsSelected = false;
             public bool IsSelected
             {
                 get
                 {
                     return _IsSelected;
                 }
                 set
                 {
                     _IsSelected = value;
                     OnPropertyChanged("IsSelected");
                 }
             }



            public int AgingInvDtl
            {
                get
                {
                    return this.agingInvDtl;
                }
                set
                {
                    this.agingInvDtl = value;
                }
            }

            public decimal GrossAmountInvDtl
            {
                get
                {
                    return this.grossAmountInvDtl;
                }
                set
                {
                    this.grossAmountInvDtl = Math.Round(value, 2);
                }
            }

            public decimal OpenAmountInvDtl
            {
                get
                {
                    return this.openAmountInvDtl;
                }
                set
                {
                    this.openAmountInvDtl = Math.Round(value, 2);
                }
            }

            public string DateInvDtl
            {
                get
                {
                    return this.dateInvDtl;
                }
                set
                {
                    this.dateInvDtl = value;
                }
            }

            public string InvoiceNoInvDtl
            {
                get
                {
                    return this.invoiceNoInvDtl;
                }
                set
                {
                    this.invoiceNoInvDtl = value;
                }
            }

            public int InvoiceIdInvDtl
            {
                get
                {
                    return this.invoiceIdInvDtl;
                }
                set
                {
                    this.invoiceIdInvDtl = value;
                }
            }

            public string StatusInvDtl
            {
                get;
                set;
            }
            public string InvoicePaymentType
            {
                get;
                set;
            }

            private bool _IsOpen = true;
            public bool IsOpen
            {
                get
                {
                    return _IsOpen;
                }
                set
                {
                    _IsOpen = value;

                    if (OpenAmountInvDtl == 0)
                    {
                        _IsOpen = false;
                    }
                    OnPropertyChanged("IsOpen");
                }
            }
            #endregion

        }

        /// <summary>
        /// This class facilitates voiding of receipts 
        /// </summary>
        public class VoidReceipt
        {
            #region Variable and object declaration

            private string receiptNo;
            private decimal amount;
            private string type;
            private string receiptDate;
            private string reasonCode;
            private bool isContinueEnabled = false;

            #endregion

            #region Properties

            /// <summary>
            ///Get or set the continue button enabled status 
            /// </summary>
            public bool IsContinueEnabled
            {
                get
                {
                    return this.isContinueEnabled;
                }
                set
                {
                    this.isContinueEnabled = value;
                }
            }

            /// <summary>
            /// Get or set reason code for voiding the receipt 
            /// </summary>
            public string ReasonCode
            {
                get
                {
                    return this.reasonCode;
                }
                set
                {
                    this.reasonCode = value;
                }
            }

            /// <summary>
            /// Get or set receipt date 
            /// </summary>
            public string ReceiptDate
            {
                get
                {
                    return this.receiptDate;
                }
                set
                {
                    this.receiptDate = value;
                }
            }

            /// <summary>
            /// Get or set receipt type 
            /// </summary>
            public string Type
            {
                get
                {
                    return this.type;
                }
                set
                {
                    this.type = value;
                }
            }

            /// <summary>
            /// Get or set receipt amount 
            /// </summary>
            public decimal Amount
            {
                get
                {
                    return this.amount;
                }
                set
                {
                    this.amount = value;
                }
            }

            /// <summary>
            /// Get or set receipt number
            /// </summary>
            public string ReceiptNo
            {
                get
                {
                    return this.receiptNo;
                }
                set
                {
                    this.receiptNo = value;
                }
            }

            #endregion

        }

        /// <summary>
        /// This class facilitates editing of payment types for a receipt 
        /// </summary>
        public class EditReceipt
        {
            #region Variable and object declaration

            private bool _IsCash = false;
            private bool _IsCheck = false;
            private string _CheckNo;
            private bool _IsContinueEnable = false;
            private DateTime _CheckDate;

            #endregion

            #region Properties

            /// <summary>
            /// Get or set continue button enabled status
            /// </summary>
            public bool IsContinueEnable
            {
                get
                {
                    return this._IsContinueEnable;
                }
                set
                {
                    this._IsContinueEnable = value;
                }
            }

            /// <summary>
            /// Get or set receipt check number 
            /// </summary>
            public string CheckNo
            {
                get
                {
                    return this._CheckNo;
                }
                set
                {
                    this._CheckNo = value;
                }
            }

            /// <summary>
            /// Get or set receipt check date 
            /// </summary>
            public DateTime CheckDate
            {
                get
                {
                    return this._CheckDate;
                }
                set
                {
                    this._CheckDate = value;
                }
            }

            /// <summary>
            /// Get or set the flag to determine whether the receipt is done through check
            /// </summary>
            public bool IsCheck
            {
                get
                {
                    return this._IsCheck;
                }
                set
                {
                    this._IsCheck = value;
                }
            }

            /// <summary>
            ///  Get or set the flag to determine whether the receipt is done through cash 
            /// </summary>
            public bool IsCash
            {
                get
                {
                    return this._IsCash;
                }
                set
                {
                    this._IsCash = value;
                }
            }

            #endregion
        }
    }
}
