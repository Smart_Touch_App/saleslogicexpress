﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain.Prospect_Models
{
    public class ProspectModel : Visitee
    {
        private string route = string.Empty;
        public string Route
        {
            get { return route; }
            set { route = value; }
        }

        private string prospectId = string.Empty;

        public string ProspectId
        {
            get { return prospectId; }
            set
            {
                prospectId = value;
                VisiteeId = prospectId; OnPropertyChanged("ProspectId");
            }
        }

        private bool itemVisible = false;

        public bool ItemVisible
        {
            get { return itemVisible; }
            set { itemVisible = value; OnPropertyChanged("ItemVisible"); }
        }

        private bool isAllowDrop = false;

        public bool IsAllowDrop
        {
            get { return isAllowDrop; }
            set { isAllowDrop = value; }
        }
        private bool isAllowCaptureDrag = false;


        #region Information Tab
        private string selectedAccSegmentType = string.Empty;

        public string SelectedAccSegmentType
        {
            get { return selectedAccSegmentType; }
            set { selectedAccSegmentType = value; }
        }

        private string buyingGroup = string.Empty;
        public string BuyingGroup
        {
            get { return buyingGroup; }
            set { buyingGroup = value; }
        }

        private string locationCount = string.Empty;
        public string LocationCount
        {
            get { return locationCount; }
            set { locationCount = value; }
        }
        #endregion

        private bool isTodaysStop = false;
        public bool IsTodaysStop
        {
            get
            {
                return isTodaysStop;
            }
            set
            {
                isTodaysStop = value;
                OnPropertyChanged("IsTodaysStop");
            }
        }

        public static explicit operator ProspectModel(Customer customer)
        {
            return new ProspectModel()
            {
                Address = customer.Address,
                AddressLine1 = customer.Address,
                AddressLine2 = customer.AddressLine2,
                AddressLine3 = customer.AddressLine3,
                AddressLine4 = customer.AddressLine4,
                City = customer.City,
                IsResquence = customer.IsResquence,
                IsTodaysStop = customer.IsTodaysStop,
                IsFromListingTab = customer.IsFromListingTab,
                CompletedActivity = customer.CompletedActivity,
                PendingActivity = customer.PendingActivity,
                SaleStatus = customer.SaleStatus,
                Name = customer.Name,
                HasActivity = customer.HasActivity,
                NextStop = customer.NextStop,
                OriginalDate = customer.OriginalDate,
                Phone = customer.Phone,
                Phone1 = customer.Phone1,
                Phone2 = customer.Phone2,
                Phone3 = customer.Phone3,
                PhoneExtention = customer.PhoneExtention,
                PreviousStop = customer.PreviousStop,
                ReferenceStopId = customer.ReferenceStopId,
                RescheduledDate = customer.RescheduledDate,
                SequenceNo = customer.SequenceNo,
                TempSequenceNo = customer.TempSequenceNo,
                VisiteeId = customer.CustomerNo,
                ProspectId = customer.CustomerNo,
                State = customer.State,
                StopDate = customer.StopDate,
                StopID = customer.StopID,
                StopType = customer.StopType,
                VisiteeType = customer.VisiteeType,
                Zip = customer.Zip,
                Route = customer.Route,

            };
        }
    }
}
