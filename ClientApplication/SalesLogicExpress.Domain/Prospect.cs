﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public class Prospect : Visitee
    {
        public Int32 EquipmentID { get; set; }

        string _ProspectID;
        public string ProspectID
        {
            get
            {
                return _ProspectID;
            }
            set
            {
                _ProspectID = value;
                this.VisiteeId = value;
                OnPropertyChanged("ProspectID");
                //OnPropertyChanged("IsSaveAllowed");
            }
        }
        public string EquipmentType { get; set; }
        public string EquipmentCategory { get; set; }
        public string EquipmentSubCategory { get; set; }
        public decimal? EquipmentQuantity { get; set; }
        public string EquipmentOwned { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedDatetimeText { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Int32 EquipmentCategoryID { get; set; }
        public bool IsSelected { get; set; }
        public string NotesDetails { get; set; }
        //
        public Int32 EquipmentSelectedID { get; set; }
        //For Text Change and Disable

        public string CategoryDropdown { get; set; }
        public string SubCategoryDropdown { get; set; }

        //public bool IsValid { get; set; }
        public string Route { get; set; }
        //
        public Prospect()
        {
            CreatedOn = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            ModifiedOn = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }
        public Prospect(Int32 _EquipmentNo)
        {
            try
            {
                EquipmentID = _EquipmentNo;
            }
            catch (Exception ex)
            {

            }
        }
        private bool itemVisible = false;

        public bool ItemVisible
        {
            get { return itemVisible; }
            set { itemVisible = value; OnPropertyChanged("ItemVisible"); }
        }

        public static explicit operator Prospect(Customer customer)
        {
            return new Prospect()
            {
                Address = customer.Address,
                AddressLine1 = customer.Address,
                AddressLine2 = customer.AddressLine2,
                AddressLine3 = customer.AddressLine3,
                AddressLine4 = customer.AddressLine4,
                City = customer.City,
                IsResquence = customer.IsResquence,
                IsTodaysStop = customer.IsTodaysStop,
                IsFromListingTab = customer.IsFromListingTab,
                CompletedActivity = customer.CompletedActivity,
                PendingActivity = customer.PendingActivity,
                SaleStatus = customer.SaleStatus,
                Name = customer.Name,
                HasActivity = customer.HasActivity,
                NextStop = customer.NextStop,
                OriginalDate = customer.OriginalDate,
                Phone = customer.Phone,
                Phone1 = customer.Phone1,
                Phone2 = customer.Phone2,
                Phone3 = customer.Phone3,
                PhoneExtention = customer.PhoneExtention,
                PreviousStop = customer.PreviousStop,
                ReferenceStopId = customer.ReferenceStopId,
                RescheduledDate = customer.RescheduledDate,
                SequenceNo = customer.SequenceNo,
                TempSequenceNo = customer.TempSequenceNo,
                VisiteeId = customer.CustomerNo,
                ProspectID = customer.CustomerNo,
                State = customer.State,
                StopDate = customer.StopDate,
                StopID = customer.StopID,
                StopType = customer.StopType,
                VisiteeType = customer.VisiteeType,
                Zip = customer.Zip,
                Route = customer.Route,

            };
        }

        public override string ToString()
        {
            return ProspectID + ":" + Name;
        }

        public Prospect DeepCopy()
        {
            Prospect pros = (Prospect)this.MemberwiseClone();
            return pros;
        }

    }
    public class ProspectViewModel
    {
        public Int32 EquipmentID { get; set; }
        public Int32 ProspectID { get; set; }
        public string EquipmentType { get; set; }
        public string EquipmentCategory { get; set; }
        public string EquipmentSubCategory { get; set; }
        public Int32 EquipmentQuantity { get; set; }
        public string EquipmentOwned { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        //public bool IsSelected { get; set; }
        public ProspectViewModel()
        {
            CreatedOn = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }
    }


    #region CoffeeProspect

    public class Competitor : Visitee
    {
        public Int32 CompetitorID { get; set; }
        public string CompetitorName { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }

        public Competitor()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }

        public Competitor(Int32 _competitorID, string _competitor)
        {
            try
            {
                CompetitorID = _competitorID;
                CompetitorName = _competitor;
                CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
                UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class CoffeeBlend : Visitee
    {
        public Int32 CoffeeBlendID { get; set; }
        public string CoffeeBlendName { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }

        public CoffeeBlend()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }

        public CoffeeBlend(Int32 _coffeeBlendID, string _coffeeBlend)
        {
            try
            {
                CoffeeBlendID = _coffeeBlendID;
                CoffeeBlendName = _coffeeBlend;
                CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
                UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class UOM : Visitee
    {
        public Int32 UOMID { get; set; }
        public string UOMLable { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }

        public UOM()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }

        public UOM(Int32 _uomID, string _uomLable)
        {
            try
            {
                UOMID = _uomID;
                UOMLable = _uomLable;
                CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
                UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class LiqCoffee : Visitee
    {
        public Int32 LiqCoffeeTypeID { get; set; }
        public string LiqCoffeeType { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }

        public LiqCoffee()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }

        public LiqCoffee(Int32 _liqCoffeeTypeID, string _liqCoffeeType)
        {
            try
            {
                LiqCoffeeTypeID = _liqCoffeeTypeID;
                LiqCoffeeType = _liqCoffeeType;
                CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
                UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            }
            catch (Exception ex)
            {

            }
        }
    }

    public class CoffeeProspect : Visitee
    {
        public Int32 CoffeeProspectID { get; set; }
        public Int32 CoffeeBlendID { get; set; }
        public Int32 CompetitorID { get; set; }
        public Int32 UOMID { get; set; }
        public Int32 PackSize { get; set; }
        public Int32 CS_PK_LB { get; set; }
        public Int32 UsageMeasurementID { get; set; }
        public Int32 LiqCoffeeTypeID { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }

        public CoffeeProspect()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }
    }
    public class CoffeeProspectVM : Visitee
    {
        public Int32 CoffeeProspectID { get; set; }
        public Int32 CoffeeBlendID { get; set; }
        public string CoffeeBlend { get; set; }
        public Int32 CompetitorID { get; set; }
        public string Competitor { get; set; }
        public Int32 UOMID { get; set; }
        public string UOM { get; set; }
        public Int32 PackSize { get; set; }
        public string PackSizeText { get; set; }
        public Int32 CS_PK_LB { get; set; }
        public string CS_PK_LBText { get; set; }
        public Int32 UsageMeasurementID { get; set; }
        public string CoffeeVolume { get; set; }
        public Int32 LiqCoffeeTypeID { get; set; }
        public string LiqCoffeeType { get; set; }
        public Int32? CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string CreatedDatetimeText { get; set; }
        public Int32? UpdatedBy { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime UpdatedDatetime { get; set; }
        public string UpdatedDatetimeText { get; set; }

        public bool IsSelected { get; set; }

        public CoffeeProspectVM()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }
    }

    #endregion

    #region AlliedProspect

    public class AlliedProspect : Visitee
    {
        public Int32 AlliedProspectID { get; set; }
        public Int32 CompetitorID { get; set; }
        public Int32 CategoryID { get; set; }
        public Int32 SubCategoryID { get; set; }
        public Int32 BrandID { get; set; }
        public Int32 UOMID { get; set; }
        public Int32 PackSize { get; set; }
        public Int32 CS_PK_LB { get; set; }
        public Int32 AverageVolumeID { get; set; }
        public Int32? CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Int32? UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }

        public AlliedProspect()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }
    }
    public class AlliedProspectVM : Visitee
    {

        public Int32 AlliedProspectID { get; set; }
        public Int32 CompetitorID { get; set; }
        public string Competitor { get; set; }
        public Int32 CategoryID { get; set; }
        public string Category { get; set; }
        public Int32 SubCategoryID { get; set; }
        public string SubCategory { get; set; }
        public Int32 BrandID { get; set; }
        public string Brand { get; set; }
        public Int32 UOMID { get; set; }
        public string UOM { get; set; }
        public Int32 PackSize { get; set; }
        public string PackSizeText { get; set; }
        public Int32 CS_PK_LB { get; set; }
        public string CSPKLBText { get; set; }
        public Int32 AverageVolumeID { get; set; }
        public string AverageVolume { get; set; }
        public Int32? CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string CreatedDatetimeText { get; set; }
        public Int32? UpdatedBy { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime UpdatedDatetime { get; set; }
        public string UpdatedDatetimeText { get; set; }
        public bool IsSelected { get; set; }

        public AlliedProspectVM()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }
    }

    #endregion

    public class ContactProspect : ProspectContactVisitee
    {
        public string LineID { get; set; }
        public string LineNo { get; set; }
        public string RelatedPersonID { get; set; }
        public ProspectEmail DefaultEmail { get; set; }
        public ObservableCollection<ProspectEmail> EmailList { get; set; }
        public ProspectPhone DefaultPhone { get; set; }
        public ObservableCollection<ProspectPhone> PhoneList { get; set; }
        string _ContactID;
        public string ContactID
        {
            get
            {
                return _ContactID;
            }
            set
            {
                _ContactID = value;
                OnPropertyChanged("ContactID");
            }
        }

        public DateTime CreatedDatetime { get; set; }
        public DateTime UpdatedDatetime { get; set; }
        public string ButtonTitle { get; set; }

        bool _IsSelected = false;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        bool _IsDefault { get; set; }
        public bool IsDefault
        {
            get
            {
                return _IsDefault;
            }
            set
            {
                _IsDefault = value;
                OnPropertyChanged("IsDefault");
            }
        }

        bool _IsActive { get; set; }
        public bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                _IsActive = value;
                OnPropertyChanged("IsActive");
            }
        }

        bool showOnDashboard;
        public bool ShowOnDashboard
        {
            get
            {
                return this.showOnDashboard;
            }
            set
            {
                this.showOnDashboard = value;
                OnPropertyChanged("ShowOnDashboard");
            }
        }

        bool _IsExpanded = false;
        public bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                _IsExpanded = value;
                OnPropertyChanged("IsExpanded");
            }
        }

        private bool _TogglePhoneImg = true;
        public bool TogglePhoneImg
        {
            get
            {
                return _TogglePhoneImg;
            }
            set
            {
                _TogglePhoneImg = value;
                OnPropertyChanged("TogglePhoneImg");
            }
        }

        private bool _ToggleEmailImg = true;
        public bool ToggleEmailImg
        {
            get
            {
                return _ToggleEmailImg;
            }
            set
            {
                _ToggleEmailImg = value;
                OnPropertyChanged("ToggleEmailImg");
            }
        }

        public ContactProspect()
        {
            CreatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
            UpdatedDatetime = DateTime.SpecifyKind(DateTime.Now.Date, DateTimeKind.Utc);
        }
    }

    public class ProspectEmail : Helpers.ModelBase
    {
        public Int32 ProspectID { get; set; }
        public ProspectEmail()
        {

        }
        public int Index { get; set; }
        public string ContactID { get; set; }
        public string FormattedValue
        {
            get
            {
                return Value.Trim();
            }
        }
        public string Value { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public bool IsDefault { get; set; }
        bool _IsSelected = false;
        public bool IsSelectedEmail
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelectedEmail");
            }
        }
        public override string ToString()
        {
            return string.Concat("Type:", Type, ", Value:", Value, ", IsDefault:", IsDefault, " ");
        }
        public ProspectEmail(int IndexVal, int ProsID, string ConID, string emal, string etp, int dflt, string PhoneType)
        {
            if (!string.IsNullOrWhiteSpace(ConID) && !string.IsNullOrWhiteSpace(emal) && !string.IsNullOrWhiteSpace(etp))
            {
                if (IndexVal > 0) { Index = IndexVal; }
                if (ProsID > 0) { ProspectID = ProsID; }
                if (!string.IsNullOrWhiteSpace(ConID)) { ContactID = ConID; }
                if (!string.IsNullOrWhiteSpace(emal)) { Value = emal; }
                if (!string.IsNullOrWhiteSpace(etp)) { TypeID = Convert.ToInt32(etp); }
                IsDefault = dflt == 0 ? false : true;
                if (!string.IsNullOrWhiteSpace(PhoneType)) { Type = PhoneType; }
            }
        }
    }

    public class ProspectPhone : Helpers.ModelBase
    {
        public Int32 ProspectID { get; set; }
        public ProspectPhone()
        {

        }
        public int Index { get; set; }
        public string ContactID { get; set; }
        public string FormattedValue
        {
            get
            {
                return (AreaCode.Trim().Length > 0 ? "(" + AreaCode + ")" : "") + Value.Trim() + (Extension.Trim().Length > 0 ? string.Format(" {0}", Extension) : "");
            }
        }
        public string Value { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public string Extension { get; set; }
        public string AreaCode { get; set; }
        public bool IsDefault { get; set; }
        bool _IsSelected;
        public bool IsSelectedPhone
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                OnPropertyChanged("IsSelectedPhone");
            }
        }
        public override string ToString()
        {
            return string.Concat("Type:", Type, ", Value:", Value, ", IsDefault:", IsDefault, ", AreaCode:", AreaCode, ", Extension:", Extension);
        }
        public ProspectPhone(int IndexVal, int ProsID, string ConID, string ar, string ph, string extn, string phtp, int dflt, string PhoneType)
        {
            if (!string.IsNullOrWhiteSpace(ConID) && !string.IsNullOrWhiteSpace(ar) && !string.IsNullOrWhiteSpace(ph) && !string.IsNullOrWhiteSpace(phtp))
            {
                if (IndexVal > 0) { Index = IndexVal; }
                if (ProsID > 0) { ProspectID = ProsID; }
                if (!string.IsNullOrWhiteSpace(ConID)) { ContactID = ConID; }
                if (!string.IsNullOrWhiteSpace(ar)) { AreaCode = ar; }
                if (!string.IsNullOrWhiteSpace(ph)) { Value = ph; }
                { Extension = extn; }
                if (!string.IsNullOrWhiteSpace(phtp)) { TypeID = Convert.ToInt32(phtp); }
                IsDefault = dflt == 0 ? false : true;
                if (!string.IsNullOrWhiteSpace(PhoneType)) { Type = PhoneType; }
            }
            else
            {
                if (IndexVal > 0) { Index = IndexVal; }
                if (ProsID > 0) { ProspectID = ProsID; }
                if (!string.IsNullOrWhiteSpace(ConID)) { ContactID = ConID; }
                if (!string.IsNullOrWhiteSpace(ar)) { AreaCode = ar; }
                if (!string.IsNullOrWhiteSpace(ph)) { Value = ph; }
                { Extension = extn; }
                if (!string.IsNullOrWhiteSpace(phtp)) { TypeID = Convert.ToInt32(phtp); }
                IsDefault = dflt == 0 ? false : true;
                if (!string.IsNullOrWhiteSpace(PhoneType)) { Type = PhoneType; }

            }
        }
    }
    
}
