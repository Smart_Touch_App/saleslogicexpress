﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class User : ModelBase
    {
        public string Name { get; set; }
        public string LastSync { get; set; }
        public string Route { get; set; }
        public string Branch { get; set; }
    }
}
