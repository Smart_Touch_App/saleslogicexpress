﻿using System;
using System.Runtime.Serialization;
namespace SalesLogicExpress.Domain
{
    [DataContract]
    [KnownType(typeof(SalesLogicExpress.Domain.PickOrderItem))]
    public class OrderItem : Item
    {
        private bool _IsUnitPriceDirty = false;
        private bool _IsReasonCodeProvided = false;
        private readonly decimal OriginalPrice;
        private decimal _UnitPrice;
        private double _UMConversionFactor = 1;
        int _ReasonCode;
        bool _InclOnDemand;


        public OrderItem()
        {
        }
        public OrderItem(Item item)
        {
            this.ItemNumber = item.ItemNumber;
            this.ItemDescription = item.ItemDescription;
            this.UnitPriceByPricingUOM = item.UnitPriceByPricingUOM;
            this.UM = item.UM;
            this.SalesCat1 = item.SalesCat1;
            this.SalesCat5 = item.SalesCat5;
            this.UMPrice = item.UMPrice;
            this.UnitPrice = item.UnitPrice;
            OriginalPrice = item.UnitPrice;
            this.StkType = item.StkType;
            this.ItemId = item.ItemId;
            this.PrimaryUM = item.PrimaryUM;
            this.ExtendedPrice = item.ExtendedPrice;
            this.PriceOverrideFlag = item.PriceOverrideFlag;
        }
        public OrderItem(TemplateItem item)
        {
            this.ItemNumber = item.ItemNumber;
            this.ItemDescription = item.ItemDescription;
            this.UM = item.UM;
            this.UMPrice = item.UMPrice;
            this.UnitPriceByPricingUOM = item.UnitPriceByPricingUOM;
            this.UnitPrice = item.UnitPrice;
            OriginalPrice = item.UnitPrice;
            this.ExtendedPrice = item.ExtendedPrice;
            this.SalesCat1 = item.SalesCat1;
            this.SalesCat5 = item.SalesCat5;
            this.OrderQty = item.OrderQty;
            this.ItemId = item.ItemId;
            this.PrimaryUM = item.PrimaryUM;
            this.UMConversionFactor = item.UMConversionFactor;
            this.ActualQtyOnHand = item.ActualQtyOnHand;
            this.LastComittedQty = item.LastComittedQty;
            this.ReasonCode = item.ReasonCode;
            this.AppliedUMS = item.AppliedUMS;
        }
        public OrderItem(string itemNumber, string itemDescription, int orderQuantity, decimal unitPrice, string umPrice, decimal extendedPrice, int reasonCode, int onHandQuantity, string um)
        {
            this.ItemNumber = itemNumber;
            this.ItemDescription = itemDescription;
            this.OrderQty = orderQuantity;
            this.UnitPrice = unitPrice;
            OriginalPrice = UnitPrice;
            this.UMPrice = umPrice;
            this.ExtendedPrice = extendedPrice;
            this.ReasonCode = reasonCode;
            this.QtyOnHand = onHandQuantity;
            this.UM = um;
        }

        public bool IsUnitPriceDirty
        {
            get
            {
                return _IsUnitPriceDirty;
            }
            set
            {

                if (value)
                {
                    //if original price an currently changed price is same, then reset the dirty value and reason code
                    if (OriginalPrice == UnitPrice)
                    {
                        _IsUnitPriceDirty = false;
                        this.ReasonCode = 1;
                    }
                    else
                    {
                        _IsUnitPriceDirty = true;
                    }
                }
                else
                {
                    _IsUnitPriceDirty = false;
                }

                if (_IsUnitPriceDirty && this.ReasonCode <= 1)
                {
                    base.AddError("ReasonCode", "Please provide valid price override reason code!");
                }
                else
                {
                    base.RemoveError("ReasonCode", "Please provide valid price override reason code!");
                }

                OnPropertyChanged("UnitPrice");
                OnPropertyChanged("ReasonCode");
            }
        }

        public bool IsReasonCodeProvided
        {
            get
            {
                return _IsReasonCodeProvided;
            }
            set
            {
                _IsReasonCodeProvided = value;

                if (_IsReasonCodeProvided)
                {
                    base.RemoveError("ReasonCode", "Please provide valid price override reason code!");
                }
                else
                {
                    base.AddError("ReasonCode", "Please provide valid price override reason code!");
                }

            }
        }

        [DataMember]
        public bool InclOnDemand
        {
            get
            {
                return _InclOnDemand;
            }
            set
            {
                _InclOnDemand = value;
            }
        }

        [DataMember]
        public int ReasonCode
        {
            get
            {
                return _ReasonCode;
            }
            set
            {
                _ReasonCode = value;

                //If Unit Price is dirty and Reason Code > 1 then remove the error 
                if (this.IsUnitPriceDirty)
                {
                    if (_ReasonCode <= 1)
                    {
                        base.AddError("ReasonCode", "Please provide valid price override reason code!");
                    }
                    else
                    {
                        base.RemoveError("ReasonCode", "Please provide valid price override reason code!");
                    }
                }

                this.OnPropertyChanged("ReasonCode");
                //If Unit Price is dirty and Reason Code > 1 then remove theN add the error error 
            }
        }


        [DataMember]
        public string OrderDate
        {
            get;
            set;
        }

        public override string ToString()
        {
            return ItemNumber;
        }


        int orderDetailID;
        [DataMember]
        public int OrderDetailID
        {
            get
            {
                return orderDetailID;
            }
            set
            {


                orderDetailID = value;
                OnPropertyChanged("OrderDetailID");
            }
        }

        int orderID;
        [DataMember]
        public int OrderID
        {
            get
            {
                return orderID;
            }
            set
            {

                orderID = value;
                OnPropertyChanged("OrderID");
            }
        }



        DateTime orderDateF;
        public DateTime OrderDateF
        {
            get
            {
                return orderDateF;
            }
            set
            {
                orderDateF = value;
                OnPropertyChanged("OrderDateF");
            }
        }
    }
}
