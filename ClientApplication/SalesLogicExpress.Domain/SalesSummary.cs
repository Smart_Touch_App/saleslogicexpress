﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class SalesSummary
    {
        public string Parameter { get; set; }
        public int Regular { get; set; }
        public int Promo { get; set; }

    }
}
