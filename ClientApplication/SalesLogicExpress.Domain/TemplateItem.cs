﻿using System.Collections.Generic;
namespace SalesLogicExpress.Domain
{
    public class TemplateItem : Item
    {
        int  _SeqNo = 0, _OldSeqNo = 0, _UsualQuantity = 1, _PreOrderQuantity = 1;
        string selectedUM = string.Empty;

       
        public TemplateItem()
        {
        }

        public TemplateItem(Item item)
        {
            this.ItemNumber = item.ItemNumber;
            this.ItemDescription = item.ItemDescription;
            this.UM = item.UM;
            this.UMPrice = item.UMPrice;
            this.UnitPrice = item.UnitPrice;
            this.SalesCat1 = item.SalesCat1;
            this.SalesCat5 = item.SalesCat5;
            this.StkType = item.StkType;
            this.ItemId = item.ItemId;
            this.PrimaryUM = item.PrimaryUM;
            // this.AverageStopQty = item.AverageStopQty;
        }

        public int SeqNo
        {
            get
            {
                return _SeqNo;
            }
            set
            {
                _SeqNo = value;
                OnPropertyChanged("SeqNo");
            }
        }
      
        public int UsualQty
        {
            get { return _UsualQuantity; }
            set
            {
                _UsualQuantity = value;
                OnPropertyChanged("UsualQty");
            }
        }
        public int PreOrderQty
        {
            get { return _PreOrderQuantity; }
            set
            {
                _PreOrderQuantity = value;
                OnPropertyChanged("PreOrderQty");
            }
        }
        public string EffectiveFrom { get; set; }
        public string EffectiveThru { get; set; }
        public bool InclOnTmplt { get; set; }
        public string SelectedUM
        {
            get { return selectedUM; }
            set { selectedUM = value;
            OnPropertyChanged("SelectedUM");
            }
        }
        public override string ToString()
        {
            return this.ItemNumber.ToString().Trim();
        }

        private bool isNumericPadEnabled = true;
        public bool IsNumericPadEnabled
        {
            get { return isNumericPadEnabled; }
            set { isNumericPadEnabled = value; OnPropertyChanged("IsNumericPadEnabled"); }
        }
    }
}
