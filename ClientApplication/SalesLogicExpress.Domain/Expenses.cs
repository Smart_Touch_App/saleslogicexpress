﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace SalesLogicExpress.Domain
{
    public class Expenses : Helpers.ModelBase
    {
        
        private string routeID;
        public string RouteID
        {
            get { return routeID; }
            set { routeID = value; }
        }

        private string expenseID;
        public string ExpenseID
        {
            get { return expenseID; }
            set { expenseID = value; OnPropertyChanged("ExpenseID"); }
        }

        private string statusID;
        public string StatusID
        {
            get { return statusID; }
            set { statusID = value; OnPropertyChanged("StatusID"); }
        }

        private string statusCode;
        public string StatusCode
        {
            get { return statusCode; }
            set { statusCode = value; OnPropertyChanged("StatusCode"); }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }

        private string categotyID;
        public string CategotyID
        {
            get { return categotyID; }
            set { categotyID = value; OnPropertyChanged("CategotyID"); }
        }

        private string categoryCode;

        public string CategoryCode
        {
            get { return categoryCode; }
            set { categoryCode = value; OnPropertyChanged("CategoryCode"); }
        }
        private string expenseCategory;
        public string ExpenseCategory
        {
            get { return expenseCategory; }
            set { expenseCategory = value; OnPropertyChanged("ExpenseCategory"); }
        }

        private string transactionId;
        public string TransactionId
        {
            get { return transactionId; }
            set { transactionId = value; OnPropertyChanged("TransactionId"); }
        }

        private string routeSettlementId;
        public string RouteSettlementId
        {
            get { return routeSettlementId; }
            set { routeSettlementId = value; OnPropertyChanged("RouteSettlementId"); }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
            set { explanation = value; OnPropertyChanged("Explanation"); }
        }

        private string expenseAmount;
        public string ExpenseAmount
        {
            get { return expenseAmount; }
            set { expenseAmount = value; OnPropertyChanged("ExpenseAmount"); }
        }
        
        private DateTime expenseDate;
        public DateTime ExpenseDate
        {
            get { return expenseDate; }
            set { expenseDate = value; OnPropertyChanged("ExpenseDate"); }
        }

        private string voidReasonID;
        public string VoidReasonID
        {
            get { return voidReasonID; }
            set { voidReasonID = value; OnPropertyChanged("VoidReasonID"); }
        }

        private string voidReason;
        public string VoidReason
        {
            get { return voidReason; }
            set { voidReason = value; OnPropertyChanged("VoidReason"); }
        }

        private bool isMoneyOrderCharge = false;

        public bool IsMoneyOrderCharge
        {
            get { return isMoneyOrderCharge; }
            set { isMoneyOrderCharge = value; OnPropertyChanged("IsMoneyOrderCharge"); }
        }

        private bool isRowSelectable = true;
        public bool IsRowSelectable
        {
            get { return isRowSelectable; }
            set { isRowSelectable = value; OnPropertyChanged("IsRowSelectable"); }
        }
    }

    public class ExpenseCategory : ViewModelBase
    {
        private int categoryID = -1;
        public int CategoryID
        {
            get { return categoryID; }
            set { categoryID = value; OnPropertyChanged("CategoryID"); }
        }

        private string code;
        public string Code
        {
            get { return code; }
            set { code = value; OnPropertyChanged("Code"); }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; OnPropertyChanged("Description"); }
        }
    }
}
