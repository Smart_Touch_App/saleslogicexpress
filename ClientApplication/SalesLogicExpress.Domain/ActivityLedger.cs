﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class ActivityLedger
    {
        public int ActivityID
        {
            get;
            set;
        }
        public int ActivityHeaderID
        {
            get;
            set;
        }
        public DateTime ActivityTimestamp
        {
            get;
            set;
        }
        public string ActivityDescription
        {
            get;
            set;
        }
        public string ActivityTypeKey
        {
            get;
            set;
        }
        public bool IsTxActivity
        {
            get;
            set;
        }
    }
}
