﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    [DataContract]
   public class BaseEntity
    {
        [DataMember]
       public string CreatedDate
       {
           get;
           set;
       }
    }
}
