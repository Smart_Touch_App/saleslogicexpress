﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class InventoryItem : Item
    {
        #region Private Fields
        string _defaultUOM, _PrimaryUOM, _PricingUOM, _selectedUoM;
        List<string> _otherUOM = new List<string>();
        int _committedQty = 0, _heldQty = 0, _availableQty = 0, _parLevel = 0, _onHandQty = 0;
        bool _isVisible = true, _IsApproved = false, _isApplied = false;
        private DateTime? _lastReceiptDate, _lastConsumeDate;
        #endregion

        #region Public Properties

        [DataMember]
        public string SelectedUom
        {
            get { return _selectedUoM; }
            set { _selectedUoM = value; OnPropertyChanged("SelectedUom"); }
        }
        [DataMember]
        public bool IsApproved
        {
            get
            {
                return this._IsApproved;
            }
            set
            {
                this._IsApproved = value;
                OnPropertyChanged("IsApproved");
            }
        }

        [DataMember]
        public bool IsApplied
        {
            get { return _isApplied; }
            set { _isApplied = value; OnPropertyChanged("IsApplied"); }
        }
        [DataMember]
        public int OnHandQty
        {
            get
            {
                return this._onHandQty;
            }
            set
            {
                this._onHandQty = value;
                OnPropertyChanged("OnHandQty");
            }
        }
        [DataMember]
        public string DefaultUOM
        {
            get
            {
                return this._defaultUOM;
            }
            set
            {
                this._defaultUOM = value;
                OnPropertyChanged("DefaultUOM");
            }
        }
        [DataMember]
        public List<string> OtherUOM
        {
            get
            {
                return this._otherUOM;
            }
            set
            {
                this._otherUOM = value;
                OnPropertyChanged("OtherUOM");
            }
        }
        [DataMember]
        public string PrimaryUOM
        {
            get
            {
                return this._PrimaryUOM;
            }
            set
            {
                this._PrimaryUOM = value;
                OnPropertyChanged("PrimaryUOM");
            }
        }
        [DataMember]
        public string PricingUOM
        {
            get
            {
                return this._PricingUOM;
            }
            set
            {
                this._PricingUOM = value;
                OnPropertyChanged("PricingUOM");
            }
        }
        public string OtherUOMString
        {
            get
            {
                return string.Join(",", this.OtherUOM.ToArray());
            }
        }

        [DataMember]
        public DateTime? LastReceiptDate
        {
            get { return _lastReceiptDate; }
            set { _lastReceiptDate = value; OnPropertyChanged("LastReceiptDate"); }
        }

        public DateTime? LastConsumeDate
        {
            get { return _lastConsumeDate; }
            set { _lastConsumeDate = value; OnPropertyChanged("LastConsumeDate"); }
        }
        #endregion

        public InventoryItem()
        {
          
        }
        public InventoryItem(Item ItemObject)
        {
            this.ItemNumber = ItemObject.ItemNumber;
            this.ItemDescription = ItemObject.ItemDescription;
            this.SalesCat1 = ItemObject.SalesCat1;
            this.SalesCat4 = ItemObject.SalesCat4;
            this.SalesCat5 = ItemObject.SalesCat5;
            this.StkType = ItemObject.StkType;
            this.DefaultUOM = ItemObject.UM;
            this.PrimaryUOM = this.PrimaryUM = ItemObject.PrimaryUM;
        }
        public InventoryItem(TemplateItem TemplateItemObject)
        {
            this.ItemNumber = TemplateItemObject.ItemNumber;
            this.ItemDescription = TemplateItemObject.ItemDescription;
            this.SalesCat1 = TemplateItemObject.SalesCat1;
            this.SalesCat4 = TemplateItemObject.SalesCat4;
            this.SalesCat5 = TemplateItemObject.SalesCat5;
            this.StkType = TemplateItemObject.StkType;
            this.DefaultUOM = TemplateItemObject.UM;
            this.PrimaryUOM = this.PrimaryUM = TemplateItemObject.PrimaryUM;
        }
    }
    [DataContract]
    public class InventoryAdjustment
    {
        [DataMember]
        public InventoryItem Item { get; set; }
        [DataMember]
        public string AdjustmentUOM { get; set; }
        [DataMember]
        public int AdjustmentQuantity { get; set; }

        [DataMember]
        public int InventoryAdjustmentID { get; set; }
        [DataMember]
        public double ConversionFactor { get; set; }
        [DataMember]
        public List<ReasonCodeItem> ReasonCodeList { get; set; }
        [DataMember]
        public ReasonCodeItem SelectedReasonCode { get; set; }
        public DateTime AdjustmentDate { get; set; }
    }
    [DataContract]
    public class ParLevelAdjustment
    {
        [DataMember]
        public InventoryItem Item { get; set; }
        [DataMember]
        public int ModifiedParlevel { get; set; }
        [DataMember]
        public string SelectedParlevelUOM { get; set; }
        [DataMember]
        public double ConversionFactor { get; set; }
    }
    public class ItemUOMConversion
    {
        public string ItemNumber { get; set; }
        public string ItemID { get; set; }
        public double ConversionFactor { get; set; }
        public string FromUOM { get; set; }
        public string ToUOM { get; set; }
    }
}
