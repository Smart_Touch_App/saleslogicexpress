﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
namespace SalesLogicExpress.Domain
{
    [DataContract]
    [KnownType(typeof(SalesLogicExpress.Domain.OrderItem))]
    public class Item : Helpers.ModelBase
    {
        #region Properties
        private int _ActualQtyOnHand = 0;
        string _ItemNumber = string.Empty, _UM = string.Empty, _UMPrice = string.Empty, _ItemDescription = string.Empty, _SalesCat1 = string.Empty, _SalesCat4 = string.Empty, _SalesCat5 = string.Empty, _StkType = string.Empty, _PrimaryUM = string.Empty, _itemId = string.Empty;
        string _defaultUOM = string.Empty, _otherUOM = string.Empty, __PrimaryUOM = string.Empty, _PriceOverrideFlag=string.Empty;


        decimal _ExtendedPrice = 0, _UnitPrice = 0, _UnitPriceByPricingUOM = 0;
        double umConversionFactor = 1;


        private int _QtyOnHand = 0, _availableQty = 0, _heldQty = 0, _committedQty = 0, _parLevel = 0, maxOrderQty = 0;


        double _AverageStopQty = 0;
        List<string> _AppliedUMS = new List<string>();
        bool _IsTaxable = false, isForRecount = false;
        int _OrderQty = 0;
        int _ReasonCode = 0;

        [DataMember]
        public int OrderQty
        {
            get
            {
                return _OrderQty;
            }
            set
            {
                _OrderQty = value;
                this.OnPropertyChanged("OrderQty");
                this.OnPropertyChanged("ExtendedPrice");
            }
        }

        [DataMember]
        public double UMConversionFactor
        {
            get { return umConversionFactor; }
            set { umConversionFactor = value; OnPropertyChanged("UMConversionFactor"); }
        }

        [DataMember]
        public int ActualQtyOnHand
        {
            get
            {

                return _ActualQtyOnHand;

            }
            set
            {
                _ActualQtyOnHand = value;
                OnPropertyChanged("ActualQtyOnHand");  // Trigger the change event if the value is changed!

            }
        }

        [DataMember]
        public bool IsTaxable
        {
            get { return _IsTaxable; }
            set
            {
                _IsTaxable = value;
                OnPropertyChanged("IsTaxable");
            }

        }
        [DataMember]
        public List<string> AppliedUMS
        {
            get { return _AppliedUMS; }
            set { _AppliedUMS = value; }
        }

        [DataMember]
        public double AverageStopQty
        {
            get { return _AverageStopQty; }
            set
            {
                _AverageStopQty = value;
                OnPropertyChanged("AverageStopQty");
            }
        }

        [DataMember]
        public int QtyOnHand
        {
            get
            {
                //return (this.ActualQtyOnHand - this.OrderQty) < 0 ? 0 : (this.ActualQtyOnHand - this.OrderQty);\
                return _QtyOnHand;
            }
            set
            {
                _QtyOnHand = value;
                OnPropertyChanged("QtyOnHand");  // Trigger the change event if the value is changed!

            }
        }
        [DataMember]
        public string ItemNumber
        {
            get
            {
                return _ItemNumber;
            }
            set
            {
                _ItemNumber = value;
                OnPropertyChanged("ItemNumber");
            }
        }

        [DataMember]
        public string ItemDescription
        {
            get
            {
                return _ItemDescription;
            }
            set
            {
                _ItemDescription = value;
                OnPropertyChanged("ItemDescription");
            }
        }
        [DataMember]
        public string SalesCat1
        {
            get
            {
                return _SalesCat1;
            }
            set
            {
                _SalesCat1 = value;
                OnPropertyChanged("SalesCat1");
            }
        }
        [DataMember]
        public string SalesCat5
        {
            get
            {
                return _SalesCat5;
            }
            set
            {
                _SalesCat5 = value;
                OnPropertyChanged("SalesCat5");
            }
        }
        [DataMember]
        public string SalesCat4
        {
            get
            {
                return _SalesCat4;
            }
            set
            {
                _SalesCat4 = value;
                OnPropertyChanged("SalesCat5");
            }
        }
        [DataMember]
        public string StkType
        {
            get
            {
                return _StkType;
            }
            set
            {
                _StkType = value;
                OnPropertyChanged("StkType");
            }
        }
        [DataMember]
        public string UM
        {
            get
            {
                return _UM;
            }
            set
            {
                _UM = value;
                OnPropertyChanged("UM");
                this.OnPropertyChanged("ExtendedPrice");

            }
        }
        [DataMember]
        public string UMPrice
        {
            get
            {
                return _UMPrice;
            }
            set
            {
                _UMPrice = value;
                OnPropertyChanged("UMPrice");
            }
        }
        [DataMember]
        public decimal ExtendedPrice
        {
            get
            {
                return _ExtendedPrice;
            }
            set
            {
                _ExtendedPrice = value;
                OnPropertyChanged("ExtendedPrice");
            }
        }
        [DataMember]
        public string PriceOverrideFlag
        {
            get
            {
                return _PriceOverrideFlag;
            }
            set
            {
                _PriceOverrideFlag = value;
            }
        }

        [DataMember]
        public decimal UnitPriceByPricingUOM
        {
            get
            {
                return _UnitPriceByPricingUOM;
            }
            set
            {
                _UnitPriceByPricingUOM = value;
                OnPropertyChanged("UnitPriceByPricingUOM");
            }
        }
        [DataMember]
        public decimal UnitPrice
        {
            get
            {
                return _UnitPrice;
            }
            set
            {

                _UnitPrice = value;
                OnPropertyChanged("UnitPrice");
                this.OnPropertyChanged("ExtendedPrice");
            }
        }
        [DataMember]
        public string ItemId
        {
            get
            {
                return _itemId;
            }
            set
            {
                _itemId = value;
                OnPropertyChanged("ItemId");
            }
        }

        [DataMember]
        public string PrimaryUM
        {
            get
            {
                return _PrimaryUM;
            }
            set
            {
                _PrimaryUM = value;
                OnPropertyChanged("PrimaryUM");
            }
        }

        [DataMember]
        public int ReasonCode
        {
            get
            {
                return _ReasonCode;
            }
            set
            {
                _ReasonCode = value;
                OnPropertyChanged("ReasonCode");
            }
        }

        /// <summary>
        /// stores available qty of item
        /// ** If NEGATIVE caps it to 0 ("ZERO")
        /// </summary>
        [DataMember]
        public int AvailableQty
        {
            get { return _availableQty; }
            set
            {
                _availableQty = ActualAvailableQty = value;
                if (value < 0)
                    _availableQty = 0;
                OnPropertyChanged("AvailableQty");
                MaxOrderQty = value;

            }
        }
        int _ActualavailableQty = 0;
        [DataMember]
        public int ActualAvailableQty
        {
            get { return _ActualavailableQty; }
            set
            {
                _ActualavailableQty = value;
                OnPropertyChanged("ActualAvailableQty");
            }
        }
        [DataMember]
        public int MaxOrderQty
        {
            get { return maxOrderQty; }
            set
            {
                maxOrderQty = value;
                OnPropertyChanged("AvailableQty");
            }
        }
        decimal taxAmount;
        [DataMember]
        public decimal TaxAmount
        {
            get { return taxAmount; }
            set
            {
                taxAmount = value;
                OnPropertyChanged("TaxAmount");
            }
        }

        bool _IsVisible = true;
        public bool IsVisible
        {
            get
            {
                return _IsVisible;
            }
            set
            {
                _IsVisible = value;
                OnPropertyChanged("IsVisible");
            }
        }

        [DataMember]
        public int HeldQty
        {
            get { return _heldQty; }
            set
            {
                _heldQty = value;
                OnPropertyChanged("HeldQty");
            }
        }
        [DataMember]
        public int CommittedQty
        {
            get { return _committedQty; }
            set
            {
                _committedQty = value;
                OnPropertyChanged("CommittedQty");
            }
        }

        [DataMember]
        public int ParLevel
        {
            get { return _parLevel; }
            set { _parLevel = value; OnPropertyChanged("ParLevel"); }
        }
        bool _IsValidForOrder = true;
        [DataMember]
        public bool IsValidForOrder
        {
            get { return _IsValidForOrder; }
            set { _IsValidForOrder = value; OnPropertyChanged("IsValidForOrder"); }
        }

        int _LastComittedQty = 0;
        [DataMember]
        public int LastComittedQty
        {
            get
            {
                return _LastComittedQty;
            }
            set
            {
                _LastComittedQty = value;
                OnPropertyChanged("LastComittedQty");
            }
        }

        private ObservableCollection<QtyUOMClass> qtyUOMCollection = new ObservableCollection<QtyUOMClass>();
        public ObservableCollection<QtyUOMClass> QtyUOMCollection
        {
            get { return qtyUOMCollection; }
            set { qtyUOMCollection = value; }
        }



        private QtyUOMClass selectedQtyUOM = new QtyUOMClass();

        public QtyUOMClass SelectedQtyUOM
        {
            get { return selectedQtyUOM; }
            set
            {
                selectedQtyUOM = value; OnPropertyChanged("SelectedQtyUOM");
            }
        }
        #endregion

        public Item()
        {

        }
    }
}
