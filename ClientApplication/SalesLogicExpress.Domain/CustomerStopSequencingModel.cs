﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using WpfApplicationForStudy.Behavior;
//using WpfApplicationForStudy.ViewModel;

namespace SalesLogicExpress.Domain
{
    public class CustomerStopSequencingModel : Helpers.ModelBase
    {
        private string name;
        private string dayOfWeek;
        private string dayNo;
        private string customerCode;
        private string idCode;
        private string idLable;
        private string listID;

        public string DayNo
        {
            get
            {
                return this.dayNo;
            }
            set
            {
                this.dayNo = value;
                OnPropertyChanged("DayNo");
            }
        }

        public string ListID
        {
            get
            {
                return this.listID;
            }
            set
            {
                this.listID = value;
                OnPropertyChanged("ListID");
            }
        }

        public string IdLable
        {
            get
            {
                return this.idLable;
            }
            set
            {
                this.idLable = value;
                OnPropertyChanged("IdLable");
            }
        }

        public string IdCode
        {
            get
            {
                return this.idCode;
            }
            set
            {
                this.idCode = value;
                OnPropertyChanged("IdCode");
            }
        }

        public string CustomerCode
        {
            get
            {
                return this.customerCode;
            }
            set
            {
                this.customerCode = value;
                OnPropertyChanged("CustomerCode");
            }
        }

        public string DayOfWeek
        {
            get
            {
                return this.dayOfWeek;
            }
            set
            {
                this.dayOfWeek = value;
                OnPropertyChanged("DayOfWeek");
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                OnPropertyChanged("Name");
            }
        }

        //Type IDragable.DataType
        //{
        //    //get { throw new NotImplementedException(); }
        //    get { return typeof(CustomerStopSequencingModel); }
        //}

        //void IDragable.Remove(object i)
        //{
        //    CustomerStopSequencingViewModel.Instance().Remove(this);
        //}
    }
}
