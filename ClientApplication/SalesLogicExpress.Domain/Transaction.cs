﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesLogicExpress.Domain
{
    public class Transaction
    {
        public int TransactionID
        {
            get;
            set;
        }

        public string RouteID
        {
            get;
            set;
        }

        public string TransactionType
        {
            get;
            set;
        }

        public string TransactionDetailClass
        {
            get;
            set;
        }

        public string TransactionDetails
        {
            get;
            set;
        }

        public DateTime TransactionStart
        {
            get;
            set;
        }

        public DateTime TransactionEnd
        {
            get;
            set;
        }

        public string StopInstanceID
        {
            get;
            set;
        }

        public string CustomerID
        {
            get;
            set;
        }

        public string SettlementID
        {
            get;
            set;
        }

        public int ParentTransactionID
        {
            get;
            set;
        }
    }

    public class TransactionSyncDetail
    {
        public TransactionSyncDetail()
        {
            SyncTimestamp = DateTime.SpecifyKind(SyncTimestamp, DateTimeKind.Utc);
        }
        public int SyncID
        {
            get;
            set;
        }
        public int TransactionID
        {
            get;
            set;
        }
        public string TransactionKey
        {
            get;
            set;
        }
        public bool Synced
        {
            get;
            set;
        }
        public DateTime SyncTimestamp
        {
            get;
            set;
        }
        public string Priority
        {
            get;
            set;
        }

    }
}
