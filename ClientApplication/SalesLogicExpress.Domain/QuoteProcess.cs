﻿using SalesLogicExpress.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class QuoteSummary:ModelBase
    {
        #region Properties 

        /// <summary>
        /// Get or set the quote Id
        /// </summary>
        public int QuoteId { get; set; }


        /// <summary>
        /// Get or set the count of items included in quote 
        /// </summary>
        public int QuotedItemCount { get; set; }

        /// <summary>
        /// Get or set the pricing set-up i.e. Master/Override
        /// </summary>
        public string PricingSetup { get; set; }

        /// <summary>
        /// Get or set pricing details used in the qoute 
        /// </summary>
        public PricingDetails PriceDetail { get; set; }

        private bool _IsPrinted = false; 
        /// <summary>
        /// Get or set flag to indicate whether qoute is printed or not 
        /// </summary>
        public bool IsPrinted
        {
            get { return _IsPrinted; }
            set
            {
                _IsPrinted = value;
                OnPropertyChanged("IsPrinted");
            }
        }

        /// <summary>
        /// Get or set the flag to determine whether the prospect is provided with sample or not 
        /// </summary>
        private bool isSampled = false;

        public bool IsSampled
        {
            get { return isSampled; }
            set { isSampled = value; OnPropertyChanged("IsSampled"); }
        }

        private int sampleQty = -1;

        public int SampleQty
        {
            get { return sampleQty; }
            set { sampleQty = value;
            IsSampled = value > 0 ? true : false;
            OnPropertyChanged("SampleQty");
            }
        }
        
        /// <summary>
        /// Get or set the quote date 
        /// </summary>
        public DateTime QouteDate { get; set; }

        /// <summary>
        /// Get or set the customer/Prospect id
        /// </summary>
        public int CustomerId { get; set; }


        #endregion 

    }

    public class PricingDetails : Helpers.ModelBase
    {
        #region Properties

        private int quoteId = -1;

        public int QuoteId
        {
            get { return quoteId; }
            set { quoteId = value; OnPropertyChanged("QuoteId"); }
        }

        private int customerQuoteId = -1;

        public int CustomerQuoteId
        {
            get { return customerQuoteId; }
            set
            {
                customerQuoteId = value;
                OnPropertyChanged("CustomerQuoteId");
            }

        }


        private int prospectQuoteId = -1;

        public int ProspectQuoteId
        {
            get { return prospectQuoteId; }
            set
            {
                prospectQuoteId = value;
                OnPropertyChanged("ProspectQuoteId");
            }
        }
        private string priceProtection = "";

        public string PriceProtection
        {
            get { return priceProtection; }
            set
            {
                priceProtection = value;
                OnPropertyChanged("PriceProtection");
            }
        }

        private string alliedProductLineOverall = "";

        public string AlliedProductLineOverall
        {
            get { return alliedProductLineOverall; }
            set { alliedProductLineOverall = value; OnPropertyChanged("AlliedProductLineOverall"); }
        }
        private string alliedProductLine1 = "";

        public string AlliedProductLine1
        {
            get { return alliedProductLine1; }
            set { alliedProductLine1 = value; OnPropertyChanged("AlliedProductLine1"); }
        }


        private string alliedProductLine2 = "";

        public string AlliedProductLine2
        {
            get { return alliedProductLine2; }
            set { alliedProductLine2 = value; OnPropertyChanged("AlliedProductLine2"); }
        }


        private string alliedProductLine3 = "";

        public string AlliedProductLine3
        {
            get { return alliedProductLine3; }
            set { alliedProductLine3 = value; OnPropertyChanged("AlliedProductLine3"); }
        }


        private string alliedProductLine4 = "";

        public string AlliedProductLine4
        {
            get { return alliedProductLine4; }
            set { alliedProductLine4 = value; OnPropertyChanged("AlliedProductLine4"); }
        }


        private string liquidBracket = "";

        public string LiquidBracket
        {
            get { return liquidBracket; }
            set { liquidBracket = value; OnPropertyChanged("LiquidBracket"); }
        }


        private string coffVolume = "";

        public string CoffVolume
        {
            get { return coffVolume; }
            set { coffVolume = value; OnPropertyChanged("CoffVolume"); }
        }


        private string equipProg = "";

        public string EquipProg
        {
            get { return equipProg; }
            set { equipProg = value; OnPropertyChanged("EquipProg"); }
        }


        private string pOSUpCharge = "";

        public string POSUpCharge
        {
            get { return pOSUpCharge; }
            set { pOSUpCharge = value; OnPropertyChanged("POSUpCharge"); }
        }


        private string specialCCP = "";

        public string SpecialCCP
        {
            get { return specialCCP; }
            set { specialCCP = value; OnPropertyChanged("SpecialCCP"); }
        }

        private string taxGroup = "";

        public string TaxGroup
        {
            get { return taxGroup; }
            set { taxGroup = value; OnPropertyChanged("TaxGroup"); }
        }
        private bool _IsAlliedProductLineOverallOverriden=false;
        private bool _IsAlliedProductLine1Overriden = false;
        private bool _IsAlliedProductLine2Overriden = false;
        private bool _IsAlliedProductLine3Overriden = false;
        private bool _IsAlliedProductLine4Overriden = false;
        private bool _IsLiquidBracketOverriden = false;
        private bool _IsCoffVolumeOverriden = false;
        private bool _IsEquipProgOverriden = false;
        private bool _IsPOSUpChargeOverriden = false;
        private bool _IsSpecialCCPOverriden = false;


        public bool IsAlliedProductLineOverallOverriden
        {
            get { return _IsAlliedProductLineOverallOverriden; }
            set { _IsAlliedProductLineOverallOverriden = value; OnPropertyChanged("IsAlliedProductLineOverallOverriden"); }
        }

        public bool IsAlliedProductLine1Overriden
        {
            get { return _IsAlliedProductLine1Overriden; }
            set { _IsAlliedProductLine1Overriden = value; OnPropertyChanged("IsAlliedProductLine1Overriden"); }
        }

        public bool IsAlliedProductLine2Overriden
        {
            get { return _IsAlliedProductLine2Overriden; }
            set { _IsAlliedProductLine2Overriden = value; OnPropertyChanged("IsAlliedProductLine2Overriden"); }
        }

        public bool IsAlliedProductLine3Overriden
        {
            get { return _IsAlliedProductLine3Overriden; }
            set { _IsAlliedProductLine3Overriden = value; OnPropertyChanged("IsAlliedProductLine3Overriden"); }
        }

        public bool IsAlliedProductLine4Overriden
        {
            get { return _IsAlliedProductLine4Overriden; }
            set { _IsAlliedProductLine4Overriden = value; OnPropertyChanged("IsAlliedProductLine4Overriden"); }
        }

        public bool IsLiquidBracketOverriden
        {
            get { return _IsLiquidBracketOverriden; }
            set { _IsLiquidBracketOverriden = value; OnPropertyChanged("IsLiquidBracketOverriden"); }
        }

        public bool IsCoffVolumeOverriden
        {
            get { return _IsCoffVolumeOverriden; }
            set { _IsCoffVolumeOverriden = value; OnPropertyChanged("IsCoffVolumeOverriden"); }
        }

        public bool IsEquipProgOverriden
        {
            get { return _IsEquipProgOverriden; }
            set { _IsEquipProgOverriden = value; OnPropertyChanged("IsEquipProgOverriden"); }
        }

        public bool IsPOSUpChargeOverriden
        {
            get { return _IsPOSUpChargeOverriden; }
            set { _IsPOSUpChargeOverriden = value; OnPropertyChanged("IsPOSUpChargeOverriden"); }
        }

        public bool IsSpecialCCPOverriden
        {
            get { return _IsSpecialCCPOverriden; }
            set { _IsSpecialCCPOverriden = value; OnPropertyChanged("IsSpecialCCPOverriden"); }
        }

        #endregion 

    }

}
