﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesLogicExpress.Domain
{
    public class AddLoadPick : PickItem
    {
        

        private string replnId = string.Empty;

        public string ReplnId
        {
            get { return replnId; }
            set { replnId = value; }
        }

        private bool isAllowManualPick = false;

        public bool IsAllowManualPick
        {
            get { return isAllowManualPick; }
            set { isAllowManualPick = value; OnPropertyChanged("IsAllowManualPick"); }
        }

        //private bool isEnableScanner = true;
        //public bool IsEnableScanner
        //{
        //    get { return isEnableScanner; }
        //    set { isEnableScanner = value; OnPropertyChanged("IsEnableScanner"); }
        //}
        private bool isEnableScannerInException = false;

        public bool IsEnableScannerInException
        {
            get { return isEnableScannerInException; }
            set { isEnableScannerInException = value; OnPropertyChanged("IsEnableScannerInException"); }
        }
        private bool hasUpdateOnHand = false;

        public bool HasUpdateOnHand
        {
            get { return hasUpdateOnHand; }
            set { hasUpdateOnHand = value; OnPropertyChanged("HasUpdateOnHand"); }
        }
    }
}
