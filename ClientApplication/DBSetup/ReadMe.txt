Pre-Requisites:
1. Consolidated DB :
2. Setup Files

What this setup does?
	Creates fresh RemoteDB, when scripts are run
    Name of remote DB: remote_POCDB_<Route> For E.g: remote_POCDB_FBM109
	
To understand the run process, consider location of scripts is 
          D:\POSTPOC\SQL2012SYNC\User_Onboarding\Demo

Main batch file : setup.bat

To run this file, go to command prompt ,syntax is follows
D:\POSTPOC\SQL2012SYNC\User_Onboarding\Demo>setup.bat remote1 FBM109
Here, 
        'remote1' is user_name
        'FBM109' is route_number

First run will create a fresh setup for the route.
All intermediate Run would do incremental sync.

Note :
1. Only 3 users for following routes are are subscribed
'remote1' 109, 'sarvesh' 783, 'anirudha' 057
2. To run for different user, update the entry for respective user in table "Route_Devife_map"  to 
Device_id = '005056AE7078'
For E.g.: to sync for user "remote1" changes its device_id column to '005056AE7078' and make the device_id for other users different.
Currently a sync for user 'sarvesh' on route '783' is demonstrated in the folder 'D:\POSTPOC\SQL2012SYNC\User_Onboarding\Demo'.
