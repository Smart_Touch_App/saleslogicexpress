﻿using System.Web;
using System.Web.Optimization;

namespace SLEWebPortal
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {



            // Vendor scripts
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.1.1.min.js",
                        "~/Scripts/jquery-migrate-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryold").Include(
                        "~/Scripts/cmpatibility/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.8.20.js"));



            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/respond.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //         "~/Scripts/jquery.unobtrusive*",
            //         "~/Scripts/jquery.validate*"));

          //  bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
 //"~/Scripts/jquery-1.11.1.js",           // jQuery itself
// "~/Scripts/jquery-migrate-1.2.1.js")); // jQuery migrate




            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js"));



            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.validate.min.js",
            "~/Scripts/jquery.unobtrusive*",
                     "~/Scripts/jquery.validate*"));

            //bundles.Add(new ScriptBundle("~/bundles/filterjs").Include(
            //          "~/Scripts/Filter/bootstrap-editable.min.js",
            //          "~/Scripts/Filter/bootstrap-filterable.js",
            //           "~/Scripts/Filter/core.test.js",
            //            "~/Scripts/Filter/filterable-cell.js",
            //            "~/Scripts/Filter/filterable-row.js",
            //             "~/Scripts/Filter/filterable-utils.js",
            //              "~/Scripts/Filter/filterable.js",
            //              "~/Scripts/Filter/jquery.min.js",
            //          "~/Scripts/Filter/options.test.js"));

            //bundles.Add(new StyleBundle("~/Content/filtercss").Include(
            //          "~/Scripts/Filter/bootstrap-editable.css",
            //          "~/Scripts/Filter/bootstrap-editable.css",
            //          "~/fonts/font-awesome/css/font-awesome.min.css"));

            // Inspinia script
            bundles.Add(new ScriptBundle("~/bundles/inspinia").Include(
                      "~/Scripts/app/inspinia.js"));

            // SlimScroll
            bundles.Add(new ScriptBundle("~/plugins/slimScroll").Include(
                      "~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js"));

            // jQuery plugins
            bundles.Add(new ScriptBundle("~/plugins/metsiMenu").Include(
                      "~/Scripts/plugins/metisMenu/jquery.metisMenu.js"));

            bundles.Add(new ScriptBundle("~/plugins/pace").Include(
                      "~/Scripts/plugins/pace/pace.min.js"));

            // CSS style (bootstrap/inspinia)
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/style.css"));

            bundles.Add(new StyleBundle("~/fonts/font-awesome/css").Include(
                      "~/fonts/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform()));
            // Flot chart
            bundles.Add(new ScriptBundle("~/plugins/flot").Include(
                      "~/Scripts/plugins/flot/jquery.flot.js",
                      "~/Scripts/plugins/flot/jquery.flot.tooltip.min.js",
                      "~/Scripts/plugins/flot/jquery.flot.resize.js",
                      "~/Scripts/plugins/flot/jquery.flot.pie.js",
                      "~/Scripts/plugins/flot/jquery.flot.time.js",
                      "~/Scripts/plugins/flot/jquery.flot.spline.js"));


            // vectorMap 
            bundles.Add(new ScriptBundle("~/plugins/vectorMap").Include(
                      "~/Scripts/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                      "~/Scripts/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"));

            // dataTables 
            bundles.Add(new ScriptBundle("~/plugins/dataTables").Include(
                      "~/Scripts/plugins/dataTables/jquery.dataTables.js",
                      "~/Scripts/plugins/dataTables/dataTables.bootstrap.js",
                      "~/Scripts/plugins/dataTables/dataTables.responsive.js"));

            bundles.Add(new StyleBundle("~/jquery-ui.css").Include(
                     "~/Content/jquery-ui.css"));

           

            bundles.Add(new ScriptBundle("~/bundles/js/daterangepicker").Include(
                     "~/Scripts/Daterangepicker/moment.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/rangepicker").Include(
                     "~/Scripts/Daterangepicker/daterangepicker.js"));
                      bundles.Add(new StyleBundle("~/bundles/css/Drangepicker").Include(
                     "~/Content/rangepicker.css"));
   //         bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
     //                   "~/Scripts/jquery-{version}.js"));

       //     bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
         //               "~/Scripts/jquery-ui-{version}.js"));

         

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


        }
    }
}
