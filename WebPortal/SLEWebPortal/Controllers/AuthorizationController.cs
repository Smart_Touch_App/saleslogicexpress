﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SLEWebPortal.Models;

namespace SLEWebPortal.Controllers
{
    [Authorize]
    [SessionExpire]
    public class AuthorizationController : Controller
    {
        [SessionExpire]
        // GET: Authorization
        public ActionResult Index()
        {
            RequestAuthorizeViewModel model = new RequestAuthorizeViewModel();
            model.Codes = new Managers.AuthCodeManager().GetCodeDetails();
            return View(model);
        }
        [HttpPost]
        public ActionResult GetRequestCodeDetails(string requestCode)
        {
            RequestAuthorizeViewModel model = new RequestAuthorizeViewModel();
            model.RequestDetails = new RequestCodeDetails();
            model.Codes = new Managers.AuthCodeManager().GetCodeDetails();
            Managers.AuthCodeManager.RequestCodeDetails details = new Managers.AuthCodeManager().GetRequestDetails(requestCode);
            if (details == null)
            {
                model.RequestDetails = null;
                model.Codes = null;
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            model.RequestDetails.Amount = details.Amount;
            model.RequestDetails.Feature = details.Feature;
            model.RequestDetails.User = details.User;
            model.RequestDetails.Route = details.Route;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAuthorizationCode(string requestCode)
        {
            RequestAuthorizeViewModel model = new RequestAuthorizeViewModel();
            model.RequestDetails = new RequestCodeDetails();
            model.Codes = new Managers.AuthCodeManager().GetCodeDetails();
            model.AuthorizationCode = new Managers.AuthCodeManager().GetAuthCode(requestCode); ;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}