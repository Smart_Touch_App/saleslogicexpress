﻿using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Controllers
{
    [Authorize]
    [SessionExpire]
    public class UserController : Controller
    {
        readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: User
        public ActionResult Index()
        {

            return View(GetModel());
        }

        public ActionResult MapUser()
        {
            try
            {
                Logger.Info("UserController MapUser");
                UserViewModel model = GetModel();
                model.RouteList = new Managers.RouteManager().GetRouteSelectableList();
                model.UserList = new Managers.UserManager().GetApplciationUserSelectList();
                return View("MapUser", model);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UserController MapUser" + ex.Message);
                throw ex;
            }
        }
        UserViewModel GetModel()
        {
            try
            {
                Logger.Info("UserController GetModel");
                UserViewModel model = new UserViewModel();
                model.Users = new Managers.UserManager().GetApplciationUsers();
                model.TotalUsers = model.Users.Count;
                model.ActiveUsers = model.Users.Count(u => u.Active == true);
                model.PendingUsers = model.Users.Count(u => u.Active == false);
                model.UserRoles = new Managers.UserManager().GetUserRoles();
                model.AddUser = new LoginViewModel();
                return model;
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UserController GetModel" + ex.Message);
                throw ex;
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddUser(UserViewModel model)
        {
            try
            {
                Logger.Info("UserController AddUser Parameters: " + model);
                if (ModelState.IsValid)
                {
                    var displayName = model.DisplayName;
                    var userName = model.UserName;
                    var password = model.Password;
                    var selectedUserRole = model.SelectedUserRoleId;
                    Managers.UserManager userManager = new Managers.UserManager();
                    string userID = userManager.AddApplciationUser(displayName, userName, password);
                    if (Convert.ToInt32(userID) > 0)
                    {
                        userManager.AddApplciationUserRole(userID, selectedUserRole);
                    }
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    model.Users = new Managers.UserManager().GetApplciationUsers();
                    model.TotalUsers = model.Users.Count;
                    model.ActiveUsers = model.Users.Count(u => u.Active == true);
                    model.PendingUsers = model.Users.Count(u => u.Active == false);
                    model.UserRoles = new Managers.UserManager().GetUserRoles();

                    return View("Index", model);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UserController AddUser" + ex.Message);
                //  throw ex;
                return RedirectToAction("Index", "User");
            }
        }

        [HttpGet]
        public ActionResult EditUser(int? UserID)
        {
            UserViewModel user = new UserViewModel();
            Logger.Info("[SalesLogicExpress.Controllers.User][UserController][Start:EditUser]");
            try
            {
                string ID = UserID.HasValue ? UserID.ToString() : "-1";
                List<LoginViewModel> Users = new Managers.UserManager().GetApplciationUsers();
                user.AddUser = Users.FirstOrDefault(u => u.ID == ID);
                user.Active = user.AddUser.Active;
                user.UserRoles = new Managers.UserManager().GetUserRoles();
                ViewBag.ItemsSelect = new SelectList((user.UserRoles), "Text", "Value",user.AddUser.Role);
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Controllers.User][UserController][EditUser][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Controllers.User][UserController][End:EditUser]");
            return PartialView("_EditUser", user);
        }
        [HttpPost]
        public ActionResult EditUser(UserViewModel model)
        {
            UserViewModel UserModel = new UserViewModel();
            UserViewModel user = new UserViewModel();
            Logger.Info("[SalesLogicExpress.Controllers.User][UserController][Start:EditUser]");
            try
            {
                
                var displayName = model.DisplayName;
                var userName = model.UserName;
                var password = model.Password;
                var selectedUserRole = model.SelectedUserRoleId;
                int flag = Convert.ToInt32(model.Active);
                int id=Convert.ToInt32( model.ID);
                Managers.UserManager userManager = new Managers.UserManager();
                string userID = userManager.EditApplciationUser(displayName, userName, password,flag,id);
               
                    userManager.EditApplciationUserRole(id, selectedUserRole);
                
                return RedirectToAction("Index", "User");
            }
            catch (Exception ex)
            {
                Logger.Error("[SalesLogicExpress.Controllers.User][UserController][EditUser][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SalesLogicExpress.Controllers.User][UserController][End:EditUser]");
            return RedirectToAction("Index", "User");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MapUserToRoute(FormCollection collection)
        {
            bool flag = false;
            string routes = "";
            try
            {

                Logger.Info("UserController MapUserToRoute Parameters: " + collection);
                var userID = collection["SelectedUser"];
                string routeID = collection["Select Route"];
                Managers.UserManager userManager = new Managers.UserManager();
                int count = routeID.Split(',').Count();
                string[] arr = routeID.Split(',');

                for (int i = 0; i < count; i++)
                {
                    flag = userManager.MapApplciationUserToRoute(userID, arr[i].Trim());

                    if (flag == false)
                    {
                        routes = routes + " " + arr[i].Trim();
                    }

                }

                if (routes.Length > 1)
                {

                    TempData["alertMessage"] = "User has already been mapped to route " + routes + "";
                    return RedirectToAction("MapUser", "User");
                }
                else
                {
                    return RedirectToAction("MapUser", "User");

                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UserController MapUserToRoute" + ex.Message);
                throw ex;
            }


        }
        public ActionResult ToggleActive(string State, string UserID)
        {
            try
            {
                Logger.Info("UserController ToggleActive Parameters: " + State + " " + UserID);
                new Managers.UserManager().UpdateApplciationUser(State, UserID);
                return RedirectToAction("Index", "User");
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UserController ToggleActive" + ex.Message);
                throw ex;
            }
        }
    }
}