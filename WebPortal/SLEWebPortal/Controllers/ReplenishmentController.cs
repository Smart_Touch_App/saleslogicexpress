﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Models;
using System.Web.Mvc;

namespace SLEWebPortal.Controllers
{
    public class ReplenishmentController : Controller
    {
           
        // GET: Replenishment
      [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            Replenishment replenishment = new Replenishment();
            replenishment.replenishmentheader = new Managers.ReplenishmentManager().GetReplenishmentList();
            replenishment.routelistmodels = new Managers.ReplenishmentManager().GetRoutesforReplenishmentAll();
            replenishment.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            replenishment.ReplType = new Managers.RouteManager().GetTypeSelectableListNew();
            return View(replenishment);
        }
        public ActionResult ReplenishmentDetails(int replid, int typeid, int routeid,string status,string DCorSales)
        {
            Replenishment replenishment = new Replenishment();
            replenishment.replenishmentheader = new Managers.ReplenishmentManager().GetReplenishmentListByID(replid,routeid);
            replenishment.routelistmodels = new Managers.ReplenishmentManager().GetRoutesforReplenishmentByID(replid,routeid);
            if (DCorSales == "SalesBranch")
            {
                replenishment.replenishmentdetails = new Managers.ReplenishmentManager().GetReplenishmentDetailListByID(replid, typeid, routeid, status);
            }
            else{
                replenishment.replenishmentdetails = new Managers.ReplenishmentManager().GetReplenishmentDetailListByIDForDC(replid, typeid, routeid, status);
            }
            //replenishment.replenishmentdetails = new Managers.ReplenishmentManager().GetReplenishmentDetailListByID(replid, typeid,routeid,status);

            return View(replenishment);
        }
        public ActionResult ReplenishmentForRoute(string id,string id2)
        {
            Replenishment replenishment = new Replenishment();
            replenishment.replenishmentheader = new Managers.ReplenishmentManager().GetReplenishmentListByRoute(id,id2);
            replenishment.routelistmodels = new Managers.ReplenishmentManager().GetRoutesforReplenishmentByRouteID(id,id2);
           

            return PartialView("_ReplenishmentView",replenishment);
        }

        public ActionResult ReplenishmentForType(string id,string id2)
        {
            Replenishment replenishment = new Replenishment();
           
                replenishment.replenishmentheader = new Managers.ReplenishmentManager().GetReplenishmentListByType(id,id2);
                replenishment.routelistmodels = new Managers.ReplenishmentManager().GetRoutesforReplenishmentByTypeID(id,id2);
            
            


            return PartialView("_ReplenishmentView", replenishment);
        }
        public bool VoidReplenishment(int id, int routeid,string repltype)
        {

            bool flag;
            flag=new Managers.ReplenishmentManager().VoidRequest(id.ToString(),routeid,repltype);
            return flag;

          
        }

        public bool CompletedReplenishment(int id,int routeid)
        {

            bool flag;
            flag = new Managers.ReplenishmentManager().CompleteRequest(id.ToString(),routeid);
            return flag;


        }
        public bool ShipRequest(string[] arr,string[] arr2,int id,int routeid)
        {

            bool flag;
            flag = new Managers.ReplenishmentManager().ShipRequest(arr, arr2,id,routeid);
            return flag;
        }
    }
}