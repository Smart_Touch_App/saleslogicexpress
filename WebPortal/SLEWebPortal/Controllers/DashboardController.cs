﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using SLEWebPortal.Managers;
using SLEWebPortal.Models;
namespace SLEWebPortal.Controllers
{
    [Authorize]
    [SessionExpire]
    public class DashboardController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Dashboard
        public ActionResult Index()
        {
            Models.DashboardModels dashboardModel = new Models.DashboardModels();
            try
            {
                logger.Info("DashboardController Index");
           
            dashboardModel.Orders = new Managers.OrderManager().GetOrderList();
            dashboardModel.RecentOrders = new Managers.OrderManager().GetRecentOrders();
            dashboardModel.VoidOrders = new Managers.OrderManager().GetVoidOrders();
            dashboardModel.VoidOrdersForToday = new Managers.OrderManager().GetVoidOrdersForToday("");
            dashboardModel.CreditOrders = new OrderManager().GetCreditOrderList();
            dashboardModel.CreditOrdersForToday = new OrderManager().GetCreditOrdersForToday("");
                dashboardModel.TotalOrders = dashboardModel.Orders.Count();
                dashboardModel.TotalOrdersForToday = dashboardModel.RecentOrders.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));
                dashboardModel.TotalVoidOrders = dashboardModel.VoidOrders.Count();
                dashboardModel.TotalVoidOrdersForToday = dashboardModel.VoidOrdersForToday.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));
                dashboardModel.TotalCreditOrders = dashboardModel.CreditOrders.Count<Order>();
                dashboardModel.TotalCreditOrdersForToday = dashboardModel.CreditOrdersForToday.Count((Order item) => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));

                dashboardModel.TotalCustomers = Convert.ToInt32(new Managers.CustomerManager().GetAllCustomersCount());
                dashboardModel.TotalProspects = Convert.ToInt32(new Managers.ProspectManager().GetAllProspectsCount());
                dashboardModel.graphorders = new Managers.OrderManager().GetTodayCoffee();
           //     today();
                dashboardModel.RecentOrders = dashboardModel.RecentOrders.Where(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy")).ToList();
                dashboardModel.TotalPaymentAmountForToday=new Managers.OrderManager().GetTotalPayment();
                dashboardModel.TotalPaymentAmountForToday=Convert.ToDouble(String.Format("{0:0.00}", dashboardModel.TotalPaymentAmountForToday));
              //  dashboardModel.totalcoffee = new Managers.OrderManager().GetTotalCoffee();


            }
            catch (Exception ex)
            {
               // logger.Error("Error in CustomerController Index" + ex.Message);
                throw ex;
            }
            return View(dashboardModel);
        }



        public List<Models.Order> SetOrderStates(List<Models.Order> Orders)
        {
            foreach (Models.Order item in Orders)
            {
                if (item.OrderState.ToLower().Contains("hold") )
                {
                    item.OrderSubState = "../content/images/Hold.png";
                }
                else if (item.OrderState.ToLower().Contains("void"))
                {
                    item.OrderSubState = "../content/images/Void.png";
           //         item.OrderState = "Cancelled";
                }                

                //if (item.OrderState == OrderStatus.OrderTemplate.ToString())
                //{
                //    item.OrderState = "In Progress";
                //}
                //else if (item.OrderState == OrderStatus.OrderEntry.ToString())
                //{
                //    item.OrderState = "In Progress";
                //}
                //else if (item.OrderState == OrderStatus.OrderPreview.ToString())
                //{
                //    item.OrderState = "In Progress";
                //}
                //else if (item.OrderState == OrderStatus.OrderAcceptance.ToString())
                //{
                //    item.OrderState = "In Progress";
                //}
                //else if (item.OrderState == OrderStatus.PickOrder.ToString())
                //{
                //    item.OrderState = "In Progress";
                //}
                //else if (item.OrderState == OrderStatus.CashCollection.ToString())
                //{
                //    item.OrderState = "In Progress";
                //}
                if (item.OrderState.Trim() == OrderStatus.OrderDelivered.ToString())
                {
                    item.OrderState = "Delivered";
                }
                else if (item.OrderState.ToLower().Contains("void"))
                {
                    item.OrderState = "Cancelled";
                }
                else {
                    item.OrderState = "In Progress";
                }
              

                /*else
                {
                    item.OrderState = "Delivered";
                }*/
            }
            return Orders;
        }
        public ActionResult OrderList()
        {
            try
            {
                logger.Info("DashboardController OrderList");
                Models.DashboardModels dashboardModel = new Models.DashboardModels();
                List<Models.Order> Orders = new Managers.OrderManager().GetOrderList();
                dashboardModel.Orders = SetOrderStates(Orders);
                return View("OrderList", dashboardModel);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DashboardController OrderList" + ex.Message);
                throw ex;
            }
        }

        public ActionResult yearly()
        {
            Models.DashboardModels dashboardModel = new Models.DashboardModels();
            try
            {
                logger.Info("DashboardController yearly");
                
                dashboardModel.Orders = new Managers.OrderManager().GetOrderList();
     //           dashboardModel.RecentOrders = new Managers.OrderManager().GetRecentOrders();
     //           dashboardModel.VoidOrders = new Managers.OrderManager().GetVoidOrders();
     //           dashboardModel.VoidOrdersForToday = new Managers.OrderManager().GetVoidOrdersForToday("");

     //           dashboardModel.TotalOrders = dashboardModel.Orders.Count();
     //           dashboardModel.TotalOrdersForToday = dashboardModel.RecentOrders.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));
     //           dashboardModel.TotalVoidOrders = dashboardModel.VoidOrders.Count();
     //           dashboardModel.TotalVoidOrdersForToday = dashboardModel.VoidOrdersForToday.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));


    //            dashboardModel.TotalCustomers = new Managers.CustomerManager().GetAllCustomers().Count();
                dashboardModel.graphorders = new Managers.OrderManager().GetYearlyCoffee();
     //           dashboardModel.TotalPaymentAmountForToday = dashboardModel.Orders.Sum(item => Convert.ToInt32(item.OrderTotal));
              //  dashboardModel.graphorders = new Managers.OrderManager().GetYearlyCoffee();
                
            }
            catch (Exception ex)
            {
             //   logger.Error("Error in DashboardController OrderList" + ex.Message);
                //throw ex;
            }
        //    return View("Index",dashboardModel);
            return PartialView("_graph", dashboardModel);
        }
       
        public ActionResult monthly()
        {
            Models.DashboardModels dashboardModel = new Models.DashboardModels();
            try
            {
                logger.Info("DashboardController yearly");

                dashboardModel.Orders = new Managers.OrderManager().GetOrderList();
 //               dashboardModel.RecentOrders = new Managers.OrderManager().GetRecentOrders();
  //              dashboardModel.VoidOrders = new Managers.OrderManager().GetVoidOrders();
  //              dashboardModel.VoidOrdersForToday = new Managers.OrderManager().GetVoidOrdersForToday("");

  //              dashboardModel.TotalOrders = dashboardModel.Orders.Count();
  //              dashboardModel.TotalOrdersForToday = dashboardModel.RecentOrders.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));
  //              dashboardModel.TotalVoidOrders = dashboardModel.VoidOrders.Count();
  //              dashboardModel.TotalVoidOrdersForToday = dashboardModel.VoidOrdersForToday.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));


  //              dashboardModel.TotalCustomers = new Managers.CustomerManager().GetAllCustomers().Count();
                dashboardModel.graphorders = new Managers.OrderManager().GetMonthlyCoffee();
           //     dashboardModel.TotalPaymentAmountForToday = dashboardModel.Orders.Sum(item => Convert.ToInt32(item.OrderTotal));
                //  dashboardModel.graphorders = new Managers.OrderManager().GetYearlyCoffee();

            }
            catch (Exception ex)
            {
                //   logger.Error("Error in DashboardController OrderList" + ex.Message);
                //throw ex;
            }
         //  return View("Index", dashboardModel);
            return PartialView("_graph", dashboardModel);
        }

        public ActionResult today()
        {
            Models.DashboardModels dashboardModel = new Models.DashboardModels();
            try
            {
                logger.Info("DashboardController yearly");

                dashboardModel.Orders = new Managers.OrderManager().GetOrderList();
           //     dashboardModel.RecentOrders = new Managers.OrderManager().GetRecentOrders();
           //     dashboardModel.VoidOrders = new Managers.OrderManager().GetVoidOrders();
           //     dashboardModel.VoidOrdersForToday = new Managers.OrderManager().GetVoidOrdersForToday("");

           //     dashboardModel.TotalOrders = dashboardModel.Orders.Count();
            //    dashboardModel.TotalOrdersForToday = dashboardModel.RecentOrders.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));
          //      dashboardModel.TotalVoidOrders = dashboardModel.VoidOrders.Count();
          //      dashboardModel.TotalVoidOrdersForToday = dashboardModel.VoidOrdersForToday.Count(item => Convert.ToDateTime(item.OrderDate).ToString("dd/MMM/yyyy") == DateTime.Now.ToString("dd/MMM/yyyy"));


          //      dashboardModel.TotalCustomers = new Managers.CustomerManager().GetAllCustomers().Count();
                dashboardModel.graphorders = new Managers.OrderManager().GetTodayCoffee();
            //    dashboardModel.TotalPaymentAmountForToday = dashboardModel.Orders.Sum(item => Convert.ToInt32(item.OrderTotal));
                //  dashboardModel.graphorders = new Managers.OrderManager().GetYearlyCoffee();

            }
            catch (Exception ex)
            {
                //   logger.Error("Error in DashboardController OrderList" + ex.Message);
                //throw ex;
            }
          //  return View("Index", dashboardModel);
            return PartialView("_graph", dashboardModel);
        }

        enum OrderStatus
        {
            None,
            AppStart,
            AppExit,
            UserLogin,
            UserLogout,
            PasswordIncorrect,
            PasswordChange,
            AcceptOrderOnPreview,
            CreateOrder,
            HoldAtCashCollection,
            HoldAtDeliverToCustomer,
            HoldAtOrderEntry,
            HoldAtPick,
            OrderDelivered,
            PickComplete,
            PickItem,
            PickOrder,
            SettlementCancelled,
            SettlementSettlement,
            SettlementStarted,
            SettlementSubmitted,
            SettlementVerified,
            VoidAtCashCollection,
            VoidAtDeliverCustomer,
            VoidAtOrderEntry,
            Payment,
            PaymentVoid,
            OrderTemplate,
            NoActivity,
            PreOrder,
            Change, //Added to record the changes
            DeliverToCustomer,
            AcceptOrderOnDelivery,
            HoldAtROEntry,
            HoldAtROPrint,
            VoidAtROEntry,
            VoidAtROPick,
            OrderReturned

        }

        public enum StatusTypesEnum
        {
            OPEN, // Open
            CLSD, // Closed
            STTLD, // Settled
            USTLD, //UnSettled,
            CMTD, //Committed,
            VOID, //Void,
            HOLD, //Hold,
            INPRG, //InProgress,
            VERF, //Verified
            REJCT,
            NEW,
            DLVRD,
            ACCEPT,
            FAIL,
            PASS,
            PHYSCL,
            WKLY,
            RECNT,
            SUBMIT,
            PNDNG,
            CMPLT,
            RTP,
            HOP,
            INPRS,
            RELS,
            INFO,
            NWARN,
            NOPTN,
            NMAND,
            VDSTLD,
            RETORD,
            SALORD
        }

        enum OrderSubStatus
        {
            Hold,
            Void,
            None
        }

        [HttpPost]
        public ActionResult OrderDetails(string orderID)
        {
            try
            {
                logger.Info("DashboardController OrderDetails Parameters: " + orderID);
                Models.DashboardModels dashboardModel = new Models.DashboardModels();
                dashboardModel.Orders = new Managers.OrderManager().GetOrderDetails(orderID);
                return PartialView("_OrderDetails", dashboardModel);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DashboardController Index" + ex.Message);
                throw ex;
            }
        }

        public ActionResult OrdersForToday()
        {
            try
            {
                logger.Info("DashboardController OrdersForToday");
                Models.DashboardModels dashboardModel = new Models.DashboardModels();
                List<Models.Order> Orders = new Managers.OrderManager().GetOrdersForToday("");
                dashboardModel.Orders = SetOrderStates(Orders);
                return View("OrderList", dashboardModel);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DashboardController OrdersForToday" + ex.Message);
                throw ex;
            
            }
        }


        public ActionResult Prospects(string routeID)
        {
            try
            {
                logger.Info("DashBoardController Prospects Parameters: " + routeID);
                ProspectModels model = new ProspectModels();
                model.Prospects = new Managers.ProspectManager().GetProspectsForRoute(routeID);
                return View("Prospects", model);
            }
            catch (Exception ex)
            {
                logger.Error("Error in RouteController RouteCustomers" + ex.Message);
                throw ex;
            }
        }
        public ActionResult VoidOrderList()
        {
            try
            {
                logger.Info("DashboardController VoidOrderList");
                Models.DashboardModels dashboardModel = new Models.DashboardModels();
                List<Models.Order> VoidOrders = new Managers.OrderManager().GetVoidOrderList();
                dashboardModel.VoidOrders = SetOrderStates(VoidOrders);
                return View("VoidOrderList", dashboardModel);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DashboardController VoidOrderList" + ex.Message);
                throw ex;
            }
        }

        public ActionResult VoidOrdersForToday()
        {
            try {
                logger.Info("DashboardController VoidOrdersForToday");
            Models.DashboardModels dashboardModel = new Models.DashboardModels();
            List<Models.Order> VoidOrders = new Managers.OrderManager().GetVoidOrdersForToday("");
            dashboardModel.VoidOrders = SetOrderStates(VoidOrders);
            return View("VoidOrderList", dashboardModel);
                }
            catch(Exception ex)
            {
                logger.Error("Error in DashboardController VoidOrdersForToday" + ex.Message);
                throw ex;
            }
        }

        public ActionResult CreditOrderList()
        {
            ActionResult result;
            try
            {
                this.logger.Info("DashboardController CreditOrderList");
                Models.DashboardModels dashboardModels = new Models.DashboardModels();
                List<Models.Order> creditOrderList = new OrderManager().GetCreditOrderList();
                dashboardModels.CreditOrders = this.SetOrderStates(creditOrderList);
                result = base.View("CreditOrderList", dashboardModels);
            }
            catch (Exception ex)
            {
                this.logger.Error("Error in DashboardController OrderList" + ex.Message);
                throw ex;
            }
            return result;
        }

        public ActionResult CreditOrdersForToday()
        {
            ActionResult result;
            try
            {
                this.logger.Info("DashboardController CreditOrdersForToday");
                Models.DashboardModels dashboardModels = new Models.DashboardModels();
                List<Models.Order> creditOrdersForToday = new OrderManager().GetCreditOrdersForToday("");
                dashboardModels.CreditOrders = this.SetOrderStates(creditOrdersForToday);
                result = base.View("CreditOrderList", dashboardModels);
            }
            catch (Exception ex)
            {
                this.logger.Error("Error in DashboardController OrdersForToday" + ex.Message);
                throw ex;
            }
            return result;
        }
    }
}