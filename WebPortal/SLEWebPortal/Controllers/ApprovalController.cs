﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SLEWebPortal.Models;
using System.Web.UI;

namespace SLEWebPortal.Controllers
{
    [SessionExpire]
    public class ApprovalController : Controller
    {
        [SessionExpire]
        // GET: Approval
        public ActionResult Index()
        {
            
            return View();
        }
        public void tempreq()
        { }

        [HttpPost]
        public ActionResult IndexWeekly(string selectedValue)
        {
            Cyclemodel ct = new Cyclemodel();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            //  List<CyclecountModels> cd = cyclecountmnager.GetCycleCountListByRoute(selectedValue);
            ct.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            //  List<RouteListModels> rd = cyclecountmnager.GetRouteList(selectedValue);

            ct.cyclecountmodels = cyclecountmnager.GetCycleCountListByRouteWeekly(selectedValue);

            ct.routelistmodels = cyclecountmnager.GetRouteListWeekly(selectedValue);
            return PartialView("_CycleCountView", ct);
            // do whatever you want with `selectedValue`
        }

        public ActionResult IndexPhysical(string selectedValue)
        {
            Cyclemodel ct = new Cyclemodel();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            //  List<CyclecountModels> cd = cyclecountmnager.GetCycleCountListByRoute(selectedValue);
            ct.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            //  List<RouteListModels> rd = cyclecountmnager.GetRouteList(selectedValue);

            ct.physicalcyclecountmodels = cyclecountmnager.GetCycleCountListByRoutePhysical(selectedValue);

            ct.physicalroutelistmodels = cyclecountmnager.GetRouteListPhysical(selectedValue);
            return PartialView("_PhysicalCycleCountView", ct);
            // do whatever you want with `selectedValue`
        }
        public ActionResult ShowDetails(int id, string routename)
        {
            CyclecountdetailsModels ccd = new CyclecountdetailsModels();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            //      ccd = cyclecountmnager.GetAllWeeklyCountItemsList(id);
            //     ccd.cyclecountmodels = cyclecountmnager.GetCycleCountListByCountID(id);

            //    ccd.routelistmodels = cyclecountmnager.GetRouteList(routename);
            return View("WeeklyCountRequest", ccd);
            // do whatever you want with `selectedValue`
        }
        public ActionResult WeeklyCountRequest(Cyclemodel model)
        {
            AddCycleCountModel addccd = new AddCycleCountModel();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();

            if (model.SelectedUser.Trim() == "All" || model.SelectedUser == "")
            {
                addccd = cyclecountmnager.GetUserDetails();
                addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsListAll();
            }
            else
            {
                
                int id = Convert.ToInt16(model.SelectedUser);
                addccd = cyclecountmnager.GetUserDetails(id);
                addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsList(id);
            }


            return View("WeeklyCountRequest", addccd);
        }

        public ActionResult WeeklyCountRequestPhysical(Cyclemodel model)
        {
            AddCycleCountModel addccd = new AddCycleCountModel();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();

            if (model.SelectedUser.Trim() == "All" || model.SelectedUser == "")
            {
                addccd = cyclecountmnager.GetUserDetails();
                addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsListAll();
            }
            else
            {
                int id = Convert.ToInt16(model.SelectedUser);
                addccd = cyclecountmnager.GetUserDetails(id);
                addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsList(id);
            }


            return View("WeeklyCountRequestPhysical", addccd);
        }
        [ValidateInput(false)]
        public ActionResult WeeklyCountRequest2(string countid)
        {
            // int i = 0;
            string selectedValue = countid;
            AddCycleCountModel addccd = new AddCycleCountModel();
            List<CyclecountdetailsModels> ccd2 = new List<CyclecountdetailsModels>();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            int id = Convert.ToInt16(selectedValue);
            addccd = cyclecountmnager.GetRecountDetails(id);
            addccd.Reasons = new Managers.RouteManager().GetReasonSelectableListNew();
            if (addccd.Status.Trim() == "NEW")
            {
              addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsList(Convert.ToInt16(addccd.RouteID));
            }
            if (addccd.Status.Trim() == "SUBMIT")
            {
                ccd2 = cyclecountmnager.GetWeeklyCountItemsListByCountForSubmit(id);
            }
            else {
                ccd2 = cyclecountmnager.GetWeeklyCountItemsListByCount(id);
            }
            
            
            ViewData["Items"] = ccd2;
            return View("WeeklyCountRequest", addccd);
        }
        //public ActionResult Additems(string[] selected)
        //{
        //    AddCycleCountModel addccd = new AddCycleCountModel();
        //      Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
        //      addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsListMultiple(selected);
        //      Session["Models"] = addccd.ccd;
        //      return PartialView("_NewCountRequest",addccd.ccd);
        //}
        //public ActionResult Deleteitems(decimal[] selected)
        //{

        //    List<CyclecountdetailsModels> cd = new List<CyclecountdetailsModels>();
        //    cd = (List<CyclecountdetailsModels>)Session["Models"];
        //    foreach (decimal s in selected)
        //    { 
        //       CyclecountdetailsModels removeitem=cd.Where(i=>i.ItemId==s).FirstOrDefault();
        //       cd.Remove(removeitem);

        //    }
        //    Session["Models"] = cd;
        //    return PartialView("_NewCountRequest", cd);
        //}

        public string SaveItems(string[] itemids, string routeid, string cycleCountDate)
        {
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            AddCycleCountModel addccd = new AddCycleCountModel();
            addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsListMultiple(itemids);
            addccd.CycleCountDatetime = Convert.ToDateTime(cycleCountDate, new System.Globalization.CultureInfo("en-US", true));
            if (Convert.ToInt16(routeid) == 0)
            {

                addccd.RouteID = Convert.ToDecimal(routeid);
                cyclecountmnager.AddNewWeeklyCountHeaderAll(addccd, cycleCountDate);
            }
            else
            {
                Page p = new Page();
                p.ClientScript.RegisterClientScriptBlock(typeof(Page), "alert", addccd.ccd.Count.ToString());
                addccd.RouteID = Convert.ToDecimal(routeid);
                cyclecountmnager.AddNewWeeklyCountHeader(addccd, routeid, cycleCountDate);
            }
            return addccd.ccd.Count.ToString();
          //  ViewBag.tab2 = 1;
            //   cyclecountmnager.AddNewWeeklyCountDetails(addccd);
            //    return View("Index");
        }


        public string NewSaveItems(string[] itemids, string countid, string cycleCountDate)
        {
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            AddCycleCountModel addccd = new AddCycleCountModel();
            addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsListMultiple(itemids);
            addccd.CycleCountID = Convert.ToDecimal(countid);
            addccd.CycleCountDatetime = Convert.ToDateTime(cycleCountDate, new System.Globalization.CultureInfo("en-US", true));
            cyclecountmnager.DeleteItemsInDetails(countid);
            cyclecountmnager.UpdateCountDetails(addccd);
            //if (Convert.ToInt16(routeid) == 0)
            //{

            //    cyclecountmnager.AddNewWeeklyCountHeaderAll(addccd, cycleCountDate);
            //}
            //else
            //{
                
            //    cyclecountmnager.AddNewWeeklyCountHeader(addccd, routeid, cycleCountDate);
            //}
            return addccd.ccd.Count.ToString();
            //  ViewBag.tab2 = 1;
            //   cyclecountmnager.AddNewWeeklyCountDetails(addccd);
            //    return View("Index");
        }
        public bool RecountItems(string[] itemids, decimal countid)
        {
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            // bool flag = true;
            bool flag = cyclecountmnager.UpdateRecount(itemids, countid);
            return flag;
        }

        public bool Release(decimal countid)
        {
            bool flag=false;
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            int noofitems = cyclecountmnager.countitems(countid);
            if (noofitems == 0)
            {
                return flag;
            }
            else { 
            flag = cyclecountmnager.ReleaseRequest(countid.ToString());

            return flag;
            }
            
        }
        public ActionResult VoidRequest(FormCollection frm)
        {
            string countid = frm["voidrequestid"].ToString();
            string reasonid = frm["SelectedUser"].ToString();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            bool flag = cyclecountmnager.VoidRequest(countid, reasonid);


            Cyclemodel ct = new Cyclemodel();

            List<CyclecountModels> cd = cyclecountmnager.GetCycleCountList();
            List<RouteListModels> rd = cyclecountmnager.GetAllRouteList();
            ct.cyclecountmodels = cd.Where(i => i.StatusType.Trim() == "WKLY").ToList();
            ct.physicalcyclecountmodels = cd.Where(i => i.StatusType.Trim() == "PHYSCL").ToList();
            ct.routelistmodels = rd.Where(i => i.StatusType.Trim() == "WKLY").ToList();
            ct.physicalroutelistmodels = rd.Where(i => i.StatusType.Trim() == "PHYSCL").ToList();
            ct.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            ct.Reasons = new Managers.RouteManager().GetReasonSelectableListNew();
            ViewBag.tab2 = true;
            return View("Index", ct);
        }

        public ActionResult AcceptRequest(FormCollection frm)
        {
            string countid = frm["acceptrequestid"].ToString();
            string routeid = frm["acceptrequestidroute"].ToString();
            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            bool flag = cyclecountmnager.AcceptRequest(countid, routeid);

            Cyclemodel ct = new Cyclemodel();

            List<CyclecountModels> cd = cyclecountmnager.GetCycleCountList();
            List<RouteListModels> rd = cyclecountmnager.GetAllRouteList();
            ct.cyclecountmodels = cd.Where(i => i.StatusType.Trim() == "WKLY").ToList();
            ct.physicalcyclecountmodels = cd.Where(i => i.StatusType.Trim() == "PHYSCL").ToList();
            ct.routelistmodels = rd.Where(i => i.StatusType.Trim() == "WKLY").ToList();
            ct.physicalroutelistmodels = rd.Where(i => i.StatusType.Trim() == "PHYSCL").ToList();
            ct.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            ct.Reasons = new Managers.RouteManager().GetReasonSelectableListNew();
            ViewBag.tab2 = true;
            return View("Index", ct);


        }
        [HttpPost]
        public void InitiateFullPhysicalCycleCount(FormCollection form, string[] itemids, string routeid, string cycleCountDate)
        {
            //Managers.CycleCountManager cyclecountManager = new Managers.CycleCountManager();
            //cyclecountManager.InitiateFullPhysicalCycleCount();
            //return new EmptyResult();

            Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
            AddCycleCountModel addccd = new AddCycleCountModel();
           // addccd.ccd = cyclecountmnager.GetAllWeeklyCountItemsListMultiple(itemids);
            addccd.CycleCountDatetime = Convert.ToDateTime(cycleCountDate, new System.Globalization.CultureInfo("en-US", true));
            if (Convert.ToInt16(routeid) == 0)
            {

                cyclecountmnager.AddNewWeeklyCountHeaderAllPhysical(addccd, cycleCountDate);
            }
            else
            {
                cyclecountmnager.AddNewWeeklyCountHeaderPhysical(addccd, routeid, cycleCountDate);
            }

            ViewBag.tab2 = 1;
        }
    }
}