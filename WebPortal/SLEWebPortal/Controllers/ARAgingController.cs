﻿using SLEWebPortal.Managers;
using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Controllers
{
    public class ARAgingController : Controller
    {
        // GET: ARAging
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            return View(this.GetCustomerAging(""));
        }

        public void tempreq()
        { }

        [HttpPost]
        public ActionResult FilterByRoute(string selectedValue)
        {
            return PartialView("_RouteAgingView", this.GetCustomerAging(selectedValue));
        }

        public ActionResult GetAgeDetails(string ageCategory, string routeId, string routeName)
        {
            ARAgingManager objManager = new ARAgingManager();
            RouteAging objRouteAging = objManager.GetAgingDetailsByRouteAndAge(routeId, ageCategory);
            ViewData["ageCategory"] = ageCategory;
            objRouteAging.RouteId = routeId;
            objRouteAging.RouteName = routeName;
            return View("AgingDetails", objRouteAging);

        }

        /// <summary>
        /// Gets the customer aging object
        /// </summary>
        /// <param name="filterRouteId">route id to filter the aging data</param>
        /// <returns>Customer Aging</returns>
        private RouteARAging GetCustomerAging(string filterRouteId)
        {
            RouteARAging objCustomerAging = new RouteARAging();

            try
            {
                Managers.CycleCountManager cyclecountmnager = new Managers.CycleCountManager();
                objCustomerAging.SelectedUser = "";
                objCustomerAging.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();

                ARAgingManager objAgingManager = new ARAgingManager();

                if(!string.IsNullOrEmpty(filterRouteId))
                {
                    objCustomerAging.RouteAgingList = objAgingManager.GetAgingDetails().Where(o=>o.RouteId.Equals(filterRouteId)).ToList();
                }
                else
                {
                    objCustomerAging.RouteAgingList = objAgingManager.GetAgingDetails();                   
                }
                 this.AddDummyAgeData(ref objCustomerAging);
                 this.UpdateAgingData(ref objCustomerAging, filterRouteId);
            }
            catch (Exception)
            {
                //
            }

            return objCustomerAging;
        }

        /// <summary>
        /// Adds dummy aging data into list
        /// </summary>
        /// <param name="customerAging">Customer aging record list</param>
        private void AddDummyAgeData(ref RouteARAging customerAging)
        {
            try
            {
                if (customerAging == null)
                {
                    return;
                }

              
               List<RouteUnappliedReceipt> unappliedReceiptByRouteList = (new ARAgingManager()).GetUnappliedReceiptsByRoute();
               

                foreach (RouteAging item in customerAging.RouteAgingList)
                {
                    if (item.RouteARAgingDetails==null)
                    {
                        continue;
                    }

                    for (int i = 1; i <= 4; i++)
                    {
                        switch (i)
                        {
                            case 1: //90+
                                if (item.RouteARAgingDetails.Count(o => o.AgeCategory.Equals("90+ Days")) > 0)
                                {
                                    continue;
                                }
                                item.RouteARAgingDetails.Insert(0, new RouteAgingDetail() { AgeCategory = "90+ Days",RouteId=item.RouteId,TotalAmount=0,TotalCustomer=0 });
                                break;
                            case 2: //90
                                if (item.RouteARAgingDetails.Count(o => o.AgeCategory.Equals("90 Days")) > 0)
                                {
                                    continue;
                                }
                                item.RouteARAgingDetails.Insert(1, new RouteAgingDetail() { AgeCategory = "90 Days",RouteId=item.RouteId,TotalAmount=0,TotalCustomer=0 });
                                break;
                            case 3://60
                                if (item.RouteARAgingDetails.Count(o => o.AgeCategory.Equals("60 Days")) > 0)
                                {
                                    continue;
                                }
                                item.RouteARAgingDetails.Insert(2, new RouteAgingDetail() { AgeCategory = "60 Days",RouteId=item.RouteId,TotalAmount=0,TotalCustomer=0 });
                                break;
                            case 4://30
                                if (item.RouteARAgingDetails.Count(o => o.AgeCategory.Equals("30 Days")) > 0)
                                {
                                    continue;
                                }
                                item.RouteARAgingDetails.Insert(3, new RouteAgingDetail() { AgeCategory = "30 Days",RouteId=item.RouteId,TotalAmount=0,TotalCustomer=0 });
                                break;
                            default:
                                break;
                        }
                    }

                    if (!unappliedReceiptByRouteList.Any(x => x.RouteId.Equals(item.RouteId)))
                    {
                        item.RouteARAgingDetails.Add(new RouteAgingDetail() { AgeCategory = "Unapplied Amt", RouteId = item.RouteId, TotalAmount = 0, TotalCustomer = 0 });
                    }
                    else
                    {
                        item.TotalAmount = item.TotalAmount + unappliedReceiptByRouteList.Where(x => x.RouteId.Equals(item.RouteId)).First().UnappliedAmt;
                        item.RouteARAgingDetails.Add(new RouteAgingDetail() { AgeCategory = "Unapplied Amt", RouteId = item.RouteId, TotalAmount = unappliedReceiptByRouteList.Where(x => x.RouteId.Equals(item.RouteId)).First().UnappliedAmt, TotalCustomer = unappliedReceiptByRouteList.Where(x => x.RouteId.Equals(item.RouteId)).First().TotalCustomers });
                    }
                }
            }
            catch (Exception)
            {
                
                //throw;
            }
        }

        /// <summary>
        /// Merges the data from return order and standard customer aging, if any
        /// </summary>
        /// <param name="customerAging"></param>
        private void UpdateAgingData(ref RouteARAging customerAging, string routeId = "")
        {
            try
            {
                List<RouteUnappliedReceipt> unappliedReceiptByRouteList = (new ARAgingManager()).GetUnappliedReceiptsByRoute();

                if (!string.IsNullOrEmpty(routeId))
                {
                    unappliedReceiptByRouteList.RemoveAll(o => !o.RouteId.Equals(routeId));
                }

                foreach (RouteUnappliedReceipt item in unappliedReceiptByRouteList)
                {
                    if (customerAging.RouteAgingList.Any(x => x.RouteId.Equals(item.RouteId)))
                    {
                        continue;
                    }
                    List<RouteAgingDetail> routeAgingList = new List<RouteAgingDetail>();

                    routeAgingList.Add(new RouteAgingDetail() { AgeCategory = "90+ Days", RouteId = item.RouteId, TotalAmount = 0, TotalCustomer = 0 });

                    routeAgingList.Add(new RouteAgingDetail() { AgeCategory = "90 Days", RouteId = item.RouteId, TotalAmount = 0, TotalCustomer = 0 });

                    routeAgingList.Add(new RouteAgingDetail() { AgeCategory = "60 Days", RouteId = item.RouteId, TotalAmount = 0, TotalCustomer = 0 });

                    routeAgingList.Add(new RouteAgingDetail() { AgeCategory = "30 Days", RouteId = item.RouteId, TotalAmount = 0, TotalCustomer = 0 });

                    routeAgingList.Add(new RouteAgingDetail() { AgeCategory = "Unapplied Amt", RouteId = item.RouteId, TotalAmount = item.UnappliedAmt, TotalCustomer = item.TotalCustomers });

                    customerAging.RouteAgingList.Add(new RouteAging { CustomersOnHold = 0, RouteId = item.RouteId, RouteName = item.RouteName, RouteARAgingDetails = routeAgingList, TotalAmount=item.UnappliedAmt });
                }

                //Re-order list based on the route id
                customerAging.RouteAgingList = customerAging.RouteAgingList.OrderBy(o => o.RouteId).ToList();
            }
            catch (Exception)
            {
                //throw;
            }
        }
    }
}