﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SLEWebPortal.Models;

namespace SLEWebPortal.Controllers
{
    public class QuotesController : Controller
    {
        // GET: Quotes
         [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
            Quotes qm = new Quotes();
            qm.quotemodels = new Managers.QuotesManager().GetQuoteList();
            qm.routelists = new Managers.QuotesManager().GetAllRouteList();
            qm.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            return View(qm);
        }
        public ActionResult IndexAsDate(string fromdate,string todate)
        {
            Quotes qm = new Quotes();
            qm.quotemodels = new Managers.QuotesManager().GetQuoteListAsDate(fromdate, todate);
            qm.routelists = new Managers.QuotesManager().GetAllRouteListAsDate(fromdate,todate);
           
            return  PartialView ("_QuotesView",qm);
        }
        public ActionResult GetQuotesForRoute(string id,string type)
        {
            Quotes qm = new Quotes();
            if(type=="Customer")
            {
                qm.quotemodels = new Managers.QuotesManager().GetQuoteListForRoute(Convert.ToInt32(id));
                qm.quotemodels = qm.quotemodels.Where(i => i.Quotetype == "Customer").ToList();
                qm.routelists = new Managers.QuotesManager().GetRouteListCustomer();
                qm.routelists = qm.routelists.Where(i => i.Routeid.ToString() == id).ToList();
            }
            else if (type == "All Types")
            {
                qm.quotemodels = new Managers.QuotesManager().GetQuoteListForRoute(Convert.ToInt32(id));
                qm.routelists = new Managers.QuotesManager().GetRouteListByID(Convert.ToString(id));
            }
            else
            {
                qm.quotemodels = new Managers.QuotesManager().GetQuoteListForRoute(Convert.ToInt32(id));
                qm.quotemodels = qm.quotemodels.Where(i => i.Quotetype == "Prospect").ToList();
                qm.routelists = new Managers.QuotesManager().GetRouteListProspect();
                qm.routelists = qm.routelists.Where(i => i.Routeid.ToString() == id).ToList();
            }
            
           
            return PartialView("_QuotesView",qm);
        }
        
        public ActionResult GetQuotesForType(string type,string id)
        {
            Quotes qm = new Quotes();
            if (id == "All")
            {
                if (type == "Customer")
                {
                    qm.quotemodels = new Managers.QuotesManager().GetQuoteListForCustomer();
                    qm.routelists = new Managers.QuotesManager().GetRouteListCustomer();
                }
                else
                {
                    qm.quotemodels = new Managers.QuotesManager().GetQuoteListForProspect();
                    qm.routelists = new Managers.QuotesManager().GetRouteListProspect();
                }
            }
            else {
                if (type == "Customer")
                {
                    qm.quotemodels = new Managers.QuotesManager().GetQuoteListForCustomer();
                    qm.quotemodels = qm.quotemodels.Where(i => i.RouteId == id).ToList();
                    qm.routelists = new Managers.QuotesManager().GetRouteListCustomer();
                    qm.routelists = qm.routelists.Where(i => i.Routeid.ToString() == id).ToList();
                }
                else
                {
                    qm.quotemodels = new Managers.QuotesManager().GetQuoteListForProspect();
                    qm.quotemodels = qm.quotemodels.Where(i => i.RouteId == id).ToList();
                    qm.routelists = new Managers.QuotesManager().GetRouteListProspect();
                    qm.routelists = qm.routelists.Where(i => i.Routeid.ToString() == id).ToList();
                }
            }
          

            return PartialView("_QuotesView", qm);
        }
        public ActionResult QuotesDetails(string type, string BillToId, string ParentId, string RouteId,string quoteid)
        {
            Quotes qm = new Quotes();
            if (type == "Customer")
            {
                qm.quotesinfo = new Managers.QuotesManager().GetQuoteInfoListForIDCustomer(BillToId, ParentId, RouteId, quoteid);
                qm.items = new Managers.QuotesManager().GetItemsForCustomer(quoteid);
                qm.quotesinfo[0].Quotetype = "Customer";
            }
            else
            {
                qm.quotesinfo = new Managers.QuotesManager().GetQuoteInfoListForIDProspect(ParentId, RouteId, quoteid);
                qm.items = new Managers.QuotesManager().GetItemsForProspect(quoteid);
                qm.quotesinfo[0].Quotetype = "Prospect";
             
            }
            
            return View(qm);
        }
        public ActionResult QuotesInfo(string type,string BillToId, string ParentId, string RouteId)
        {
            Quotes qm = new Quotes();
            if (type == "Customer")
            {
                qm.quotesinfo = new Managers.QuotesManager().GetQuoteInfoListForIDCustomer(BillToId, ParentId, RouteId,"0");
                qm.quotemodels = new Managers.QuotesManager().GetQuoteListByIDCustomer(BillToId, ParentId, RouteId);
                qm.quotemodels[0].Quotetype = "Customer";
            }
            else {
                qm.quotesinfo = new Managers.QuotesManager().GetQuoteInfoListForIDProspect( ParentId, RouteId,"0");
                qm.quotemodels = new Managers.QuotesManager().GetQuoteListByIDProspect( ParentId, RouteId);
                qm.quotemodels[0].Quotetype = "Prospect";
            }
            qm.routelists = new Managers.QuotesManager().GetRouteListByID(RouteId);
            return View(qm);
        }

        public ActionResult QuotesInfoAsPerDate(string type, string BillToId, string ParentId, string RouteId,string fromdate,string todate)
        {
            Quotes qm = new Quotes();
            if (type == "Customer")
            {
                qm.quotesinfo = new Managers.QuotesManager().GetQuoteInfoListForIDCustomerAsDate(BillToId, ParentId, RouteId, "0",fromdate,todate);
                qm.quotemodels = new Managers.QuotesManager().GetQuoteListByIDCustomerAsDate(BillToId, ParentId, RouteId,fromdate,todate);
                qm.quotemodels[0].Quotetype = "Customer";
            }
            else
            {
                qm.quotesinfo = new Managers.QuotesManager().GetQuoteInfoListForIDProspectAsDate(ParentId, RouteId, "0",fromdate,todate);
                qm.quotemodels = new Managers.QuotesManager().GetQuoteListByIDProspectAsDate(ParentId, RouteId,fromdate,todate);
                qm.quotemodels[0].Quotetype = "Prospect";
            }
            qm.routelists = new Managers.QuotesManager().GetRouteListByID(RouteId);
            return PartialView("_QuotesInfoView",qm);
        }
    }
}