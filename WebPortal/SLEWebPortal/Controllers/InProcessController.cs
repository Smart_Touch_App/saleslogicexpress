﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Controllers
{
    public class InProcessController : Controller
    {
        [SessionExpire]
        // GET: InProcess
        public ActionResult Index()
        {
            return View();
        }
    }
}