﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SLEWebPortal.Models;

namespace SLEWebPortal.Controllers
{
    [SessionExpire]
    public class DSADashboardController : Controller
    {
        //[SessionExpire]
        // GET: DSADashboard
            [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index()
        {
           
            return View();
        }

       
        public ActionResult ProductReturnsforRoute(int id)
        {
            Managers.DSADashboardManager manager = new Managers.DSADashboardManager();
            Returns returns = new Returns();

            returns.productreturns = manager.GetProductReturnsforRoute(id);
            returns.routelistmodel = manager.GetRoutesforReturnsforRoute(id);
         
            return PartialView("_ProductReturnsView", returns);
        }
        public void tempreq()
        { }
            [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult CreditMemo()
        {
            Managers.DSADashboardManager manager = new Managers.DSADashboardManager();
            Credits credit = new Credits();
            credit.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            credit.routelistmodel = manager.GetRoutesforCreditMemoAll();
            credit.creditmemo = manager.GetReceiptsforCreditMemoAll();
            return View(credit);
        }
        public ActionResult CreditMemoForRoute(int i)
        {
            Managers.DSADashboardManager manager = new Managers.DSADashboardManager();
            Credits credit = new Credits();
           
            credit.routelistmodel = manager.GetRoutesforCreditMemoForRoute(i);
            credit.creditmemo = manager.GetReceiptsforCreditMemoForRoute(i);
            return PartialView("_CreditMemoView",credit);
        }
            [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Returns()
        {
            Managers.DSADashboardManager manager = new Managers.DSADashboardManager();
            Returns returns = new Returns();

            returns.productreturns = manager.GetProductReturnsAll();
            returns.routelistmodel = manager.GetRoutesforReturnsAll();
            returns.UserRoles = new Managers.RouteManager().GetRouteSelectableListNew();
            return View(returns);
        }
            [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult ReturnsDetails(int i)
        {
            Returns returns = new Returns();
            List<Products> prod = new List<Products>();
            List<ProductReturnsModels> prodmodels = new List<ProductReturnsModels>();
            List<RouteListModels> route=new List<RouteListModels>();
            Managers.DSADashboardManager productsmanager = new Managers.DSADashboardManager();
            prod = productsmanager.GetProducts(i);
            prodmodels = productsmanager.GetProductReturnsforOrderID(i);
            returns.products = prod;
            returns.productreturns = prodmodels;
            returns.routelistmodel = productsmanager.GetRoutesforReturnsforOrderID(i);
            return View(returns);
        }
    }
}