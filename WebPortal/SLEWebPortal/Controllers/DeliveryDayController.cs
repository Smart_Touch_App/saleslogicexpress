﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SLEWebPortal.Models;
using System.Web.Script.Serialization;

namespace SLEWebPortal.Controllers
{
    [SessionExpire]
    public class DeliveryDayController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: DeliveryDay
        public ActionResult Index()
        {
            try
            {
                logger.Info("DeliveryDayController Index");
                SLEWebPortal.Models.DelieveryViewModel model = new Models.DelieveryViewModel();
                model.TotalCodes = new Managers.DayCodeManager().GetAllMessages();
                model.TotalCodesForToday = new Managers.DayCodeManager().GetDayCodeListForToday();
                return View(model);
               
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeliveryDayController Index" + ex.Message);
                throw ex;
            }
        }
        public ActionResult Configuration(DelieveryViewModel mod)
        {
            //int number = (int)TempData["weeks"];
            //if (mod.Stops == 0)
            //{

            //    string[] dayweeks = new Managers.DayCodeManager().GetConfig(mod.DelieveryCode);
            //    TempData["dayweeks"] = "hi";
            //    return View(mod);
                
            //}
            //else
            //{
                return View(mod);

         //   }
           
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddDayCode(DelieveryViewModel mod)
        {
            try
            {
                logger.Info("DeliveryDayController AddDayCode Parameters: " + mod);
                if (!ModelState.IsValid)
                {
                    SLEWebPortal.Models.DelieveryViewModel model = new Models.DelieveryViewModel();
                    model.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();

                    return View("Index", model);
                }

                var daycode = mod.DelieveryCode;
                var weeks = mod.Weeks;
                var desc = mod.Description;
                
                new Managers.DayCodeManager().AddDayCode(daycode, weeks, desc.Trim());
                //var response = new Response(true, "Day Code Added!");
                //return Json(response);
                SLEWebPortal.Models.DelieveryViewModel model2 = new Models.DelieveryViewModel();
                model2.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();
                model2.TotalCodesForToday = new Managers.DayCodeManager().GetDayCodeListForToday();
                //////model2.TotalDevices = model2.Devices.Count;
                //////model2.ActiveDevices = Convert.ToInt32(model2.Devices.Count(dev => dev.Active == 1));
                TempData["Weeks"] = mod.Weeks;
                TempData["daycode"] = mod.DelieveryCode;
                TempData["desc"] = mod.Description.Trim();
                return View("Index", model2);

            }
            catch (Exception ex)
            {
                logger.Error("Error in DeliveryDayController AddDayCode" + ex.Message);
             //   Response.Write("<script>alert('Day Code Exists!!!');</script>");
             //   throw ex;
                SLEWebPortal.Models.DelieveryViewModel model2 = new Models.DelieveryViewModel();
                model2.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();
                model2.TotalCodesForToday = new Managers.DayCodeManager().GetDayCodeListForToday();
                //model2.TotalDevices = model2.Devices.Count;
                //model2.ActiveDevices = Convert.ToInt32(model2.Devices.Count(dev => dev.Active == 1));
                TempData["Weeks"] = mod.Weeks;
                TempData["daycode"] = mod.DelieveryCode;
                TempData["desc"] = mod.Description.Trim();
                return View("Index", model2);

                //var response = new Response(true, "Day Code Exists!");
                //return Json(response);

            }
        }

        public JsonResult check(string deliverycode)
        {
            SLEWebPortal.Models.DelieveryViewModel model1 = new Models.DelieveryViewModel();
            model1.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();
            var code = model1.TotalCodes.Find(i => i.DelieveryCode.ToUpper() == deliverycode.ToUpper());
            if (code == null)
            {
                var response = new Response(true, "Y");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var response = new Response(true, "N");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Configuration2( DelieveryViewModel model)
        {
            model.stopsandweeks = new Managers.DayCodeManager().GetConfiguration(model.DelieveryCode);
            return View("Configuration2",model);
        }
        public ActionResult RenderView()
        {
            SLEWebPortal.Models.DelieveryViewModel model = new Models.DelieveryViewModel();
            model.TotalCodes = new Managers.DayCodeManager().GetAllMessages();
            model.TotalCodesForToday = new Managers.DayCodeManager().GetDayCodeListForToday();
            return PartialView("codes",model);
        }

        [HttpPost]
        public ActionResult AddConfig(string[] array,string Code,string Description,string Weeks)
        {
            try
            {

                logger.Info("DeliveryDayController AddConfig Parameters: " + array+","+Code+","+Description+","+Weeks);
                //if (!ModelState.IsValid)
                //{
                //    SLEWebPortal.Models.DelieveryViewModel model = new Models.DelieveryViewModel();
                //    model.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();
                
                //    return View("Index", model);
                //}

                //var daycode = mod.DelieveryCode;
                //var weeks = mod.Weeks;
                //var desc = mod.Description;

              //  bool check = new Managers.DayCodeManager().RemoveConfig(Code);

             //   bool check = new Managers.DayCodeManager().RemoveConfig(Code);
                bool check = new Managers.DayCodeManager().checkifexists(Code);
                if (check == true)
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        int weekno = Convert.ToInt32(array[i].Substring(0, (array[i].IndexOf('-'))));
                        int dayno = Convert.ToInt32(array[i].Substring((array[i].IndexOf('-') + 1), 1));
                        new Managers.DayCodeManager().AddConfig(Code, weekno, dayno, Description);

                    }

                    new Managers.DayCodeManager().UpdateStatus(Code, Convert.ToInt16(Weeks));
                }
                else {
                    int value = 1 / int.Parse("0");
                }
                //  SLEWebPortal.Models.DelieveryViewModel model2 = new Models.DelieveryViewModel();
                // model2.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();
                //model2.TotalDevices = model2.Devices.Count;
                //model2.ActiveDevices = Convert.ToInt32(model2.Devices.Count(dev => dev.Active == 1));
               // TempData["Weeks"] = mod.Weeks;
          
                return RedirectToAction("Configuration");
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeliveryDayController AddConfig" + ex.Message);
                throw ex;
            }
        }

        public static class JavaScript
        {
            public static string Serialize(object o)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(o);
            }
        }
        [HttpPost]
   //     public ActionResult EditConfig(string Code, int Stops, int Weeks,string Description)
        public ActionResult EditConfig(DelieveryViewModel mod)
        {
            try
            {
                logger.Info("DeliveryDayController EditConfig Parameters: " + mod );
                var Code = Request["editdaycode2"].ToString();
          //      var Stops =Convert.ToInt16(Request["weekspercycle"].ToString());
           //     var Weeks =Convert.ToInt16(Request["weekspercycle"].ToString());

                var Stops = Convert.ToInt16(Request["stopspercycle"].ToString());
                var Weeks = mod.Weeks;
                if (Convert.ToInt16(Weeks) > 53)
                {
                    ModelState.AddModelError("", "Can Not Update");
                    return View("index");
                }
                else
                { 
                var Description = Request["editdesc2"].ToString();
             //   logger.Info("DeliveryDayController EditConfig Parameters: " + mod);
                //if (!ModelState.IsValid)
                //{
                //    SLEWebPortal.Models.DelieveryViewModel model = new Models.DelieveryViewModel();
                //    model.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();

                //    return View("Index", model);
                //}

                //var daycode = mod.DelieveryCode;
                //var weeks = mod.Weeks;
                //var desc = mod.Description;
              //  new Managers.DayCodeManager().RemoveDayCode(Code);
           //     new Managers.DayCodeManager().RemoveConfig(Code);
              //  new Managers.DayCodeManager().AddDayCode(Code, Weeks, Description);


                  SLEWebPortal.Models.DelieveryViewModel model2 = new Models.DelieveryViewModel();
                // model2.TotalCodes = new Managers.DayCodeManager().GetDayCodeList();
                //model2.TotalDevices = model2.Devices.Count;
                //model2.ActiveDevices = Convert.ToInt32(model2.Devices.Count(dev => dev.Active == 1));
                // TempData["Weeks"] = mod.Weeks;
                model2.DelieveryCode=Code;
                model2.Description=Description.Trim();
                model2.Weeks=Weeks;
                model2.Stops=Stops;
                TempData["Weeks"] = Weeks;
                TempData["daycode"] = Code;
                TempData["desc"] = Description.Trim();
                string[] dayweeks = new Managers.DayCodeManager().GetConfig(Code);
                JavaScriptSerializer js = new JavaScriptSerializer();
                Json(dayweeks);
                var dayweeksarray = Json(dayweeks).Data;
                TempData["dayweeks"] = dayweeks;
                return View("Configuration",model2);
           //     return new JsonResult() { Data = "http://localhost:59911/DeliveryDay/Configuration", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            //    return null;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in DeliveryDayController EditConfig" + ex.Message);
                throw ex;
            }
        }
    }
}