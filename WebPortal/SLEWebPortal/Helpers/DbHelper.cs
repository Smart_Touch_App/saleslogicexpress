﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using log4net;

namespace SLEWebPortal.Helpers
{
    public static class DbHelper
    {
       static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataSet ExecuteDataSet(string Query)
        {
            try
            {
                logger.Info("DbHelper ExecuteDataSet Parameters: " + Query);
                return ExecuteDataSet(Query, null, false);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DbHelper ExecuteDataSet" + ex.Message);
                throw ex;
            }
        }
        public static DataSet ExecuteDataSet(string Query, Dictionary<string, object> Parameters)
        {
            try
            {
                logger.Info("DbHelper ExecuteDataSet Parameters: " + Query + " " + Parameters);
                return ExecuteDataSet(Query, Parameters, false);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DbHelper ExecuteDataSet" + ex.Message);
                throw ex;
            }
        }
        public static DataSet ExecuteDataSet(string Query, Dictionary<string, object> Parameters, bool IsProcedure)
        {

            string ConnectionString = string.Empty, err = string.Empty;
            DataSet result = new DataSet();
            try
            {
                logger.Info("DbHelper ExecuteDataSet Parameters: " + Query + " " + Parameters + " " + IsProcedure);
                ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                err = err + ConnectionString;
                SqlConnection dbConnection = new SqlConnection(ConnectionString);
                err = err + "</br> connection initialized";
                SqlCommand command = new SqlCommand(string.Empty, dbConnection);
                err = err + "</br> command initialized";
                {
                    command.CommandType = IsProcedure ? CommandType.StoredProcedure : CommandType.Text;
                    command.CommandText = Query;
                    if (Parameters != null)
                    {
                        foreach (KeyValuePair<string, object> param in Parameters)
                        {
                            command.Parameters.AddWithValue(param.Key, param.Value);
                        }
                    }
                    err = err + "</br> command parameters added";
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(result);
                        err = err + "</br> adapter filled";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in DbHelper ExecuteDataSet" + ex.Message);
                throw ex;
            }
            return result;
        }
        public static int ExecuteNonQuery(string Query)
        {
            try
            {
                logger.Info("DbHelper ExecuteNonQuery Parameters: "+Query);
                return ExecuteNonQuery(Query, null);
            }
            catch (Exception ex)
            {
                logger.Error("Error in DbHelper ExecuteNonQuery" + ex.Message);
                throw ex;
            }
        }
        public static int ExecuteNonQuery(string Query, Dictionary<string, object> Parameters)
        {
            try {
                logger.Info("DbHelper ExecuteNonQuery Parameters: " + Query + " " + Parameters);
            return ExecuteNonQuery(Query, Parameters, false);
                }
            catch(Exception ex)
            {

                logger.Error("Error in DbHelper ExecuteNonQuery" + ex.Message);
                throw ex;
            }
        }
        public static int ExecuteNonQuery(string Query, Dictionary<string, object> Parameters, bool isStoreProcedure)
        {
            int result = 0;
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            try
            {
                logger.Info("DbHelper ExecuteNonQuery Parameters: " + Query + " " + Parameters + " " + isStoreProcedure);
                SqlConnection dbConnection = new SqlConnection(ConnectionString);
                using (SqlCommand command = new SqlCommand(string.Empty, dbConnection))
                {
                    command.CommandType = isStoreProcedure ? CommandType.StoredProcedure : CommandType.Text;
                    command.CommandText = Query;
                    if (Parameters != null)
                    {
                        foreach (KeyValuePair<string, object> param in Parameters)
                        {
                            command.Parameters.AddWithValue(param.Key, param.Value);
                        }
                    }
                    dbConnection.Open();
                    result = command.ExecuteNonQuery();
                    dbConnection.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in DbHelper ExecuteNonQuery" + ex.Message);
                throw ex;
            }
            return result;
        }
        public static string ExecuteScalar(string Query)
        {
            return ExecuteScalar(Query, null);
        }
        public static string ExecuteScalar(string Query, Dictionary<string, object> Parameters, bool isStoreProcedure)
        {
            return ExecuteScalar(Query,Parameters,true,null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Query"></param>
        /// <param name="Parameters"></param>
        /// <param name="isStoredProcedure">true if the query input is the name of a procedure</param>
        /// <param name="ParameterType">True for in, False for out</param>
        /// <returns></returns>
        public static string ExecuteScalar(string Query, Dictionary<string, object> Parameters, bool isStoredProcedure, Dictionary<string, bool> ParameterType)
        {
            string result = string.Empty;
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            try
            {
                logger.Info("DbHelper ExecuteScalar Parameters: " + Query + " " + Parameters + " " + isStoredProcedure+" "+ParameterType);
                SqlConnection dbConnection = new SqlConnection(ConnectionString);
                using (SqlCommand command = new SqlCommand(string.Empty, dbConnection))
                {
                    command.CommandType = isStoredProcedure ? CommandType.StoredProcedure : CommandType.Text;
                    command.CommandText = Query;
                    SqlParameter param;
                    if (Parameters != null)
                    {
                        foreach (KeyValuePair<string, object> parameterItem in Parameters)
                        {
                            param = command.CreateParameter();
                            if (ParameterType!=null && ParameterType.ContainsKey(parameterItem.Key))
                            {
                                param.Direction = ParameterType[parameterItem.Key] ? ParameterDirection.Input : ParameterDirection.Output;
                            }
                            param.Value = parameterItem.Value;
                            param.ParameterName = parameterItem.Key;
                            command.Parameters.Add(param);
                        }
                    }
                    dbConnection.Open();
                    result = command.ExecuteScalar().ToString();
                    dbConnection.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in DbHelper ExecuteScalar" + ex.Message);
                throw ex;
            }
            return result;
        }
        public static string ExecuteScalar(string Query, Dictionary<string, object> Parameters)
        {
            string result = string.Empty;
            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
            try
            {
                logger.Info("DbHelper ExecuteScalar Parameters: " + Query + " " + Parameters);
                SqlConnection dbConnection = new SqlConnection(ConnectionString);
                using (SqlCommand command = new SqlCommand(string.Empty, dbConnection))
                {
                    command.CommandType = CommandType.Text;
                    command.CommandText = Query;
                    if (Parameters != null)
                    {
                        foreach (KeyValuePair<string, object> param in Parameters)
                        {
                            command.Parameters.AddWithValue(param.Key, param.Value);
                        }
                    }
                    dbConnection.Open();
                    result = command.ExecuteScalar().ToString();
                    dbConnection.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in DbHelper ExecuteScalar" + ex.Message);
                throw ex;
            }
            return result;
        }
    }
}