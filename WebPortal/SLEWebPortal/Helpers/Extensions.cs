﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using SLEWebPortal.Models;


namespace SLEWebPortal.Helpers
{
    public static class Extensions
    {
        static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static bool HasData(this DataSet data)
        {
            if (!(data == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0))
            {
                return true;
            }
            return false;
        }
        public static bool HasValue(this DataTable data, string column, string value)
        {
            try
            {
                logger.Info("Extensions HasValue Parameters: " + data + " " + column + " " + value);
                data.DefaultView.RowFilter = column + "='" + value + "'";
                int count = data.DefaultView.ToTable().Rows.Count;
                data.DefaultView.RowFilter = "";

                return count > 0 ? true : false;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Extensions HasValue" + ex.Message);
                throw ex;
            }
        }
        public static List<T> GetEntityList<T>(this DataSet ds) where T : new()
        {
            try
            {
                logger.Info("Extensions GetEntityList Parameters: " + ds);
                List<T> entityList = new List<T>();
                var castToEntity = new T();
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (castToEntity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                if (ds.HasData())
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var entity = new T();
                        foreach (DataColumn col in row.Table.Columns)
                        {
                            var tru = properties.FirstOrDefault(i => i.Name.ToLower() == col.ColumnName.ToLower());
                            if (tru != null)
                            {
                                if (tru.PropertyType == typeof(string))
                                {
                                    tru.SetValue(entity, row[tru.Name].ToString());
                                    continue;
                                }
                                else if (tru.PropertyType == typeof(DateTime))
                                {
                                    if (row[tru.Name] != DBNull.Value)
                                    {
                                        tru.SetValue(entity, Convert.ToDateTime(row[tru.Name]));
                                    }
                                }
                                else if (tru.PropertyType == typeof(Int16))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToInt16(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(Int32))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToInt32(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(string))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToString(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(decimal))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToDecimal(row[tru.Name]));
                                }
                                else if (tru.PropertyType == typeof(double))
                                {
                                    if (!string.IsNullOrEmpty(row[tru.Name].ToString()))
                                        tru.SetValue(entity, Convert.ToDouble(row[tru.Name].ToString()));
                                }
                                else
                                {
                                    tru.SetValue(entity, row[tru.Name]);
                                }
                            }
                        }
                        entityList.Add(entity);
                    }
                }

                return entityList;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Extensions ExecuteDataSet" + ex.Message);
                throw ex;
            }
        }

        public static T GetEntity<T>(this DataSet ds) where T : new()
        {
            try
            {
                logger.Info("Extensions GetEntityList Parameters: " + ds);
                T entityList = new T();
                var castToEntity = new T();
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (castToEntity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                if (ds.HasData())
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var entity = new T();
                        foreach (DataColumn col in row.Table.Columns)
                        {
                            var tru = properties.FirstOrDefault(i => i.Name == col.ColumnName);
                            if (tru != null)
                            {
                                if (tru.PropertyType == typeof(string))
                                {
                                    tru.SetValue(entity, row[tru.Name].ToString());
                                    continue;
                                }
                                if (tru.PropertyType == typeof(DateTime))
                                {
                                    if (row[tru.Name] != DBNull.Value)
                                    {
                                        tru.SetValue(entity, Convert.ToDateTime(row[tru.Name]));
                                    }
                                    continue;
                                }
                                tru.SetValue(entity, row[tru.Name]);
                            }
                        }
                        entityList = entity;
                    }
                }

                return entityList;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Extensions ExecuteDataSet" + ex.Message);
                throw ex;
            }
        }
        public static T GetEntity<T>(DataRow row) where T : new()
        {
            try
            {
                logger.Info("Extensions GetEntity");
                var entity = new T();
                var properties = typeof(T).GetProperties();
                PropertyInfo[] propertiess = (entity.GetType()).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                foreach (DataColumn col in row.Table.Columns)
                {
                    var tru = properties.FirstOrDefault(i => i.Name == col.ColumnName);
                    if (tru != null)
                    {
                        tru.SetValue(entity, row[tru.Name]);
                    }
                }
                return entity;
            }
            catch (Exception ex)
            {
                logger.Error("Error in Extensions GetEntity" + ex.Message);
                throw ex;
            }
        }
        public static T ParseEnum<T>(this string value)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
        public static int GetStatusIdFromDB(this Enum enumValue)
        {
            //Logger.Info("[SalesLogicExpress.Apllication.Helpers][Utils][Start:GetStatusTypeIdFromDB][EnumValue:" + enumValue.ToString() + "]");
            int StatusTypeId = 0;
            try
            {
                if (enumValue.GetType() == typeof(StatusTypesEnum))
                {
                    StatusTypeId = Convert.ToInt32(DbHelper.ExecuteScalar("SELECT StatusTypeID FROM BUSDTA.Status_Type where StatusTypeCD='" + enumValue.ToString() + "'"));
                }
                else if (enumValue.GetType() == typeof(OrderStatus))
                {
                    StatusTypeId = Convert.ToInt32(DbHelper.ExecuteScalar("SELECT TTID FROM BUSDTA.M5001 where TTKEY='" + enumValue.ToString() + "'"));
                }

            }
            catch (Exception ex)
            {
                //Logger.Error("[SalesLogicExpress.Apllication.Helpers][Utils][GetStatusTypeIdFromDB][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            //Logger.Info("[SalesLogicExpress.Apllication.Helpers][Utils][End:GetStatusTypeIdFromDB]");
            return StatusTypeId;
        }


    }
}