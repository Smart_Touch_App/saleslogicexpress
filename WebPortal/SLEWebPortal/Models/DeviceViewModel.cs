﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Models
{
    public class DeviceViewModel : Device
    {
        public List<Device> Devices;
        public List<RouteDevice> RouteDevices;
        public Device AddDevice = new Device();
        public int TotalDevices;
        public int ActiveDevices;
    }
    public class Device
    {
        [Required(ErrorMessage = "Please Enter Device ID")]
        [Display(Name="Device ID")]
      //  [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Invalid Format.")]
        [StringLength(14, ErrorMessage = "{0} must contain at least {2} characters and maximum 14 characters.", MinimumLength = 10)]
        public string DeviceID { get; set; }

        [Required(ErrorMessage = "Please Enter the Model Name")]
        [StringLength(20, ErrorMessage = "{0} must contain at least {2} characters and maximum 20 characters.", MinimumLength = 2)]
    //    [RegularExpression(@"(\S)+", ErrorMessage = "Space is not allowed.")]
        public string Model { get; set; }
         [StringLength(20, ErrorMessage = "{0} must contain at least {2} characters and maximum 20 characters.", MinimumLength = 2)]
          [RegularExpression(@"^[a-zA-Z0-9 ]+$", ErrorMessage = "Alphabets only!!!")]
        public string Manufacturer { get; set; }
        public int Active { get; set; }
    }
    public class RouteDevice :Device
    {
        public string Route { get; set; }
    }
}