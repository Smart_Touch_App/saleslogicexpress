﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class Response
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

        public Response(bool isSuccess, string message)
        {
            IsSuccess = isSuccess;
            Message = message;
        }
    }

    public class DelieveryViewModel:DelieveryModel
    {
        public List<DelieveryModel> TotalCodes { get; set; }

        public List<DelieveryModel> TotalCodesForToday { get; set; }

        public List<Stopsandweeks> stopsandweeks { get; set; }
    }
    public class Stopsandweeks
    {
        public decimal stop { get; set; }

        public decimal week { get; set; }
    }

    public class DelieveryModel
    {
        [Required(ErrorMessage = "Enter Stop Code")]
        [RegularExpression(@"^[a-zA-Z0-9_@.-]*$", ErrorMessage = "Special Characters not allowed in Delivery Code!!!")]
        [StringLength(3, ErrorMessage = "Stop Code must contain at least {2} characters and maximum 3 characters.", MinimumLength = 1)]
     //   [Display(Prompt = "numbers only")]
        public string DelieveryCode { get; set; }
        [Required(ErrorMessage = "Enter Description")]
        [StringLength(50, ErrorMessage = "Description must contain at least {2} characters and maximum 50 characters.", MinimumLength = 1)]
        public string Description { get; set; }
     //   [Display(Prompt = "numbers only")]
        [Required(ErrorMessage = "Enter Weeks")]
     //   [RegularExpression(@"^[a-zA-Z1-9][a-zA-Z0-9.,$;]+$", ErrorMessage = "Weeks can not start with 0!!!")]
        [Range(1, 53, ErrorMessage = "{0} must be between {1} and {2}")]
        [Display(Name = "Weeks", Prompt = "Enter Weeks")]
        public int Weeks { get; set; }

        public DateTime dateofcreation { get; set; }

        public Boolean Status { get; set; }

        public int Stops { get; set; }
    }
}