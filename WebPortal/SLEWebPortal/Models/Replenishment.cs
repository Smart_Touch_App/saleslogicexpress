﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Models
{
    public class Replenishment
    {
        public string SelectedUser { get; set; }

        public string SelectedType { get; set; }
        public IEnumerable<SelectListItem> UserRoles { get; set; }

       

        public IEnumerable<SelectListItem> ReplType { get; set; }

        public List<RouteListModels> routelistmodels { get; set; }

        public List<ReplenishmentHeader> replenishmentheader { get; set; }

        public List<ReplenishmentDetails> replenishmentdetails { get; set; }

    }
    public class ReplenishmentHeader
    {
        public decimal ReplenishmentID { get; set; }
        public decimal RouteId { get; set; }

        public decimal StatusId { get; set; }

        public string Status { get; set; }

        public decimal ReplenishmentTypeID { get; set; }

        public string ReplenishmentType { get; set; }

        public DateTime CreatedDatetime { get; set; }
        public DateTime TransDateFrom { get; set; }

        public DateTime TransDateTo { get; set; }
        public decimal RequestedBy { get; set; }

        public string RequestedByName { get; set; }

        public decimal FromBranchId { get; set; }
        public String FromBranch { get; set; }

        public decimal ToBranchId { get; set; }

        public string ToBranch { get; set; }

        public decimal CreatedBy { get; set; }

    }

    public class ReplenishmentDetails
    {
        public decimal ReplenishmentID { get; set; }

        public decimal ReplenishmentDetailID { get; set; }

        public decimal ItemId { get; set; }

        public string ItemNo { get; set; }

        public string ItemDescription { get; set; }

        public decimal RouteId { get; set; }

        public string PrimaryUOM { get; set; }

        public string ReplnUOM { get; set; }

        public decimal ReplenishmentQty { get; set; }

        public decimal SuggestedQty { get; set; }

        public decimal ReplnQuantity { get; set; }

        public decimal PickedStatusTypeID { get; set; }

        public decimal AvailableQty { get; set; }

        public decimal CurrentAvailability { get; set; }

        public decimal DemandQty { get; set; }

        public decimal ParLevel { get; set; }

        public decimal OpenReplnQty { get; set; }

        public decimal OnHandQty { get; set; }

        public decimal? CommittedQty { get; set; }

        public decimal HeldQty { get; set; }

        public decimal ReturnQty { get; set; }

        public string ReturnUOM { get; set; }
   //     public decimal AdjustmentQty { get; set; }

        public decimal ShippedQty { get; set; }


    }
}