﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class RequestAuthorizeViewModel
    {
        public RequestCodeDetails RequestDetails { get; set; }
        public string RequestCode { get; set; }
        public string AuthorizationCode { get; set; }
        public List<ApprovalCodeDetail> Codes { get; set; }
    }
    public class ApprovalCodeDetail
    {
        public int LogID { get; set; }
        public string ApprovalCode { get; set; }
        public string RequestCode { get; set; }
        public string RouteID { get; set; }
        public string Feature { get; set; }
        public string Amount { get; set; }
        public string RequestedByUser { get; set; }
        public string RequestedForCustomer { get; set; }
        public string Payload { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}