﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Models
{
    public class UserViewModel:LoginViewModel
    {
        public List<LoginViewModel> Users;
        public LoginViewModel AddUser;
        public int TotalUsers;
        public int ActiveUsers;
        public int PendingUsers;
        public IEnumerable<SelectListItem> UserRoles;


        //[Required(ErrorMessage = "Please Select Role")]
        //[Display(Name = "Role")]
        public string SelectedUserRoleId { get; set; }

        public IEnumerable<SelectListItem> RouteList;
        public string SelectedRoute { get; set; }
        public IEnumerable<SelectListItem> UserList;
        public string SelectedUser { get; set; }
    }
}