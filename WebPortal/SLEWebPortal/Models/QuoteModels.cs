﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Models
{
    public class Quotes
    {
        public string SelectedUser { get; set; }
        public List<QuoteModels> quotemodels { get; set; }
        public List<RouteListModels> routelists { get; set; }
        
        public List<QuotesInfo> quotesinfo { get; set; }
        public List<Items> items { get; set; }
        public IEnumerable<SelectListItem> UserRoles { get; set; }
    }
    public class QuotesInfo
    {

        public string BillToId { get; set; }

        public string CustomerId { get; set; }

        public string BillTo { get; set; }

        public string Customer { get; set; }
        public string QuoteId { get; set; }
        public DateTime PriceDate { get; set; }

        public string QuotedItems { get; set; }

        public string Quotetype { get; set; }
        public string PriceSetup { get; set; }

        public string AIAC15CategoryCode15 { get; set; }
        public string AIAC15CategoryCode15Mst { get; set; }

        public string AIAC16CategoryCode16 { get; set; }
        public string AIAC16CategoryCode16Mst { get; set; }

        public string AIAC17CategoryCode17 { get; set; }
        public string AIAC17CategoryCode17Mst { get; set; }

        public string AIAC18CategoryCode18 { get; set; }
        public string AIAC18CategoryCode18Mst { get; set; }

        public string AIAC22POSUpCharge { get; set; }
        public string AIAC22POSUpChargeMst { get; set; }
        public string AIAC23LiquidCoffee { get; set; }
        public string AIAC23LiquidCoffeeMst { get; set; }
        public string AIAC24PriceProtection { get; set; }
        public string AIAC24PriceProtectionMst { get; set; }
        public string AIAC27AlliedDiscount { get; set; }
        public string AIAC27AlliedDiscountMst { get; set; }
        public string AIAC28CoffeeVolume { get; set; }
        public string AIAC28CoffeeVolumeMst { get; set; }
        public string AIAC29EquipmentProgPts { get; set; }
        public string AIAC29EquipmentProgPtsMst { get; set; }
        public string AIAC30SpecialCCP { get; set; }
        public string AIAC30SpecialCCPMst { get; set; }
        public string IsSampled { get; set; }
        public string IsPrinted { get; set; }

    
    }
    public class QuoteModels
    {
        public string Quotetype { get; set; }
        public string ParentNumber { get; set; }

        public string RouteId { get; set; }

        public string Parent { get; set; }

        public string BillToNumber { get; set; }

        public string BillTo { get; set; }

        public int Printed { get; set; }

        public int NotPrinted { get; set; }

        public int TotalQuotes { get; set; }

        public int NumberOfQuotes { get; set; }

    }
    public class Items
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string QuoteQty { get; set; }
        public string UOM { get; set; }
        public string Pricing { get; set; }
        public string PricingUOM { get; set; }
        public string Trans { get; set; }
        public string TransUOM { get; set; }
        public string Other { get; set; }
        public string OtherUOM { get; set; }
        public string CompetitorName { get; set; }
        public string Price { get; set; }
        public string PriceUOM { get; set; }
        public string SampleQty { get; set; }
        public string SampleUM { get; set; }
    }
    
}