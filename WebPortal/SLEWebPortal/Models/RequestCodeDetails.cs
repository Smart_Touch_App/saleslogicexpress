﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class RequestCodeDetails
    {
        public string Feature { get; set; }
        public string Amount { get; set; }
        public string User { get; set; }
        public string Route { get; set; }
    }
}