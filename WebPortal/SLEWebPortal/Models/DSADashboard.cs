﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Models
{
    public class DSADashboard
    {

    }

    public class Returns
    {

        public string SelectedUser { get; set; }
        public List<ProductReturnsModels> productreturns { get; set; }

        public List<Products> products { get; set; }

        public List<RouteListModels> routelistmodel { get; set; }

        public IEnumerable<SelectListItem> UserRoles { get; set; }
    
    }
    public class ProductReturnsModels
    {

        public decimal OrderID { get; set; }
        public decimal RouteID { get; set; }

        public string Status { get; set; }

        public string StatusType { get; set; }
        public decimal OrderTypeid { get; set; }

        public int TotalItems { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal Surcharge { get; set; }

        public decimal Cucstomerid { get; set; }
        public string CustomerName { get; set; }
        public string CustomerContact { get; set; }

        public string ItemNumber { get; set; }

        public DateTime OrderDatetime { get; set; }

    //    public decimal? ReasonCodeid { get; set; }

        // public List<CyclecountdetailsModels> cyclecountdetails { get; set; }
    }

    public class Products
    {
        public string OrderNo { get; set; }

        public string ItemNo { get; set; }

        public string ItemDesc { get; set; }

        public string OrderQty { get; set; }

        public string OrderUOM { get; set; }

        public string QtyReturned { get; set; }

        public decimal UnitPrice { get; set; }

        public decimal TaxAmount { get; set; }

        public decimal ExtendedPrice { get; set; }



    }

    public class CreditMemo
    {
        public string CustomerId { get; set; }

        public string RouteId { get; set; }

        public string Status { get; set; }

        public string JDEStatusId { get; set; }

        public string CustomerName { get; set; }

        public string CreditMemoNo { get; set; }

        public string Reason { get; set; }

        public string CreditMemoNote { get; set; }

        public DateTime ReceiptDate { get; set; }

        public decimal Amount { get; set; }


    }

    public class Credits {

        public string SelectedUser { get; set; }

        public List<CreditMemo> creditmemo { get; set; }

        public List<RouteListModels> routelistmodel { get; set; }

        public IEnumerable<SelectListItem> UserRoles { get; set; }
    }
    
}