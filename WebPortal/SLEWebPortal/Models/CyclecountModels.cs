﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SLEWebPortal.Models
{

    public class Cyclemodel
    {
        public string SelectedUser { get; set; }
        public IEnumerable<SelectListItem> UserRoles { get; set; }

        public IEnumerable<SelectListItem> Reasons { get; set; }

        public List<CyclecountModels> cyclecountmodels { get; set; }

        public List<CyclecountModels> physicalcyclecountmodels { get; set; }


        public List<RouteListModels> routelistmodels { get; set; }

        public List<RouteListModels> physicalroutelistmodels { get; set; }

    }

    public class RouteListModels
    {

        public decimal Routeid { get; set; }

        public string RouteName { get; set; }

        public string RouteDescription { get; set; }

        public string StatusType { get; set; }
    
    }

    public class CyclecountModels
    {

        public decimal CycleCountID { get; set; }
        public decimal Routeid { get; set; }

        public string Status { get; set; }

        public string StatusType { get; set; }
        public decimal CycleCountTypeid { get; set; }

        public int ItemsToCount { get; set; }

        public int recountitemstotaltorecount { get; set; }

        public int recountitemstotalrecounted { get; set; }
       
        public decimal Initiatorid { get; set; }
        public string InitiatorName { get; set; }
        public string InitiatorContact { get; set; }

        public string ItemNumber { get; set; }

        public DateTime CycleCountDatetime { get; set; }

        public decimal? ReasonCodeid { get; set; }

       // public List<CyclecountdetailsModels> cyclecountdetails { get; set; }
    }

    public class CyclecountdetailsModels
    {

        public string CountAccepted { get; set; }

        public decimal ItemId { get; set; }

        public string ItemDescription { get; set; }

        public decimal CountedQty { get; set; }

        public string LastCountedQty { get; set; }

        public Boolean ItemStatus { get; set; }

        public string JDE_Qty { get; set; }
        public string MobileSystemQty { get; set; }
        public string NonSettledQty { get; set; }

        public string VarianceQty { get; set; }
        public string SalesCat1 { get; set; }
        public string SalesCat2 { get; set; }
        public string SalesCat5 { get; set; }

        public string ItemNumber { get; set; }
        public string UOM { get; set; }


    }

    public class CustomViewModel
    {
        public string temproute { get; set; }
        public string tempcycleid { get; set; }
    //    public string hidden1 { get; set; }
    }

    public class AddCycleCountModel {

        public string SelectedUser { get; set; }
     public  List<CyclecountdetailsModels> ccd { get; set; }
     public string Status { get; set; }

     public string StatusType { get; set; }
     public decimal CycleCountID { get; set; }

     public decimal RouteID { get; set; }

     public decimal StatusId { get; set; }

     public decimal CycleCountTypeId { get; set; }

     public decimal InitiatorId { get; set; }

     public string InitiatorName { get; set; }

     public string InitiatorContact { get; set; }


     public DateTime CycleCountDatetime { get; set; }

     public decimal ReasonCodeId { get; set; }

     public IEnumerable<SelectListItem> Reasons { get; set; }

     public string RouteName { get; set; }

     public string RouteDescription { get; set; }
    
    }
}