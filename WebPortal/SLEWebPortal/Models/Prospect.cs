﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class Prospect
    {
        public string ProspectNo
        {
            set;
            get;
        }

        public string ProspectName
        {
            get;
            set;
        }

        public string AddressLine1
        {
            get;
            set;
        }
        public string AddressLine2
        {
            get;
            set;
        }
        public string AddressLine3
        {
            get;
            set;
        }
        public string AddressLine4
        {
            get;
            set;
        }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string ZipCode { get; set; }

        public string AreaCode1 { get; set; }
        public string Phone1 { get; set; }
        public string AreaCode2 { get; set; }
        public string Phone2 { get; set; }
        public string AreaCode3 { get; set; }
        public string Phone3 { get; set; }
        public string AreaCode4 { get; set; }
        public string Phone4 { get; set; }
        public string EmailID1 { get; set; }       
        public string EmailID2 { get; set; }
        public string EmailID3 { get; set; }
    }
}