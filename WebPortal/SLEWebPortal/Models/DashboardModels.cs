﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class DashboardModels
    {
        public List<Order> Orders;
        public List<Order> RecentOrders;
        public List<Order> VoidOrders;
        public List<Order> VoidOrdersForToday;
        public List<graphorder> graphorders;
        public string coffee;
        public int TotalOrdersForToday;
        public int TotalOrders;
        public int TotalVoidOrdersForToday;
        public int TotalVoidOrders;
        public int TotalCreditOrdersForToday;
        public int TotalCreditOrders;
        public int TotalPaymentsForToday;
        public double TotalPaymentAmountForToday;
        public int TotalCustomers;
        public int TotalProspects;
        public List<Order> CreditOrders;
        public List<Order> CreditOrdersForToday;
    }

    public class graphorder
    {
        public string graphcoffee { get; set; }
        public string graphallie { get; set; }
        public string graphordertotal { get; set; }
        public string graphCreditordertotal { get; set; }
        public string graphtotalcoffee { get; set; }
    
    }
}