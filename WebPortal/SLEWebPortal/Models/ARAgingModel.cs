﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Models
{
    public class RouteARAging
    {
        private IList<RouteAging> _RouteAgingList=new List<RouteAging>();

        /// <summary>
        /// Get or set selected user
        /// </summary>
        public string SelectedUser { get; set; }

        /// <summary>
        /// Add or remove available routes
        /// </summary>
        public IEnumerable<System.Web.Mvc.SelectListItem> UserRoles { get; set; }

        /// <summary>
        /// Add or remove Route aging list
        /// </summary>
        public IList<RouteAging> RouteAgingList
        {
            get { return _RouteAgingList; }
            set { _RouteAgingList = value; }
        }
        
    }

    public class RouteAging
    {
        #region Variable and object declaration

        private string _RouteId ="";
        private string _RouteName = "";
        private int _CustomersOnHold = 0;
        private decimal _TotalAmount = 0;
        private IList<RouteAgingDetail> _RouteARAgingDetails = new List<RouteAgingDetail>();
        private IList<RouteAgingByCustomers> _RouteAgingByCustomersList = new List<RouteAgingByCustomers>();

        #endregion 

        #region Properties

        /// <summary>
        /// Get or set on hold customers count
        /// </summary>
        public int CustomersOnHold
        {
            get { return _CustomersOnHold; }
            set { _CustomersOnHold = value; }
        }

        /// <summary>
        /// Get or set route id
        /// </summary>
        public string RouteId
        {
            get { return _RouteId; }
            set { _RouteId = value; }
        }

        /// <summary>
        /// Get or set the route name 
        /// </summary>
        public string RouteName
        {
            get { return _RouteName; }
            set { _RouteName = value; }
        }

        /// <summary>
        /// Get or set total aging amount for route
        /// </summary>
        public decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }

        public IList<RouteAgingDetail> RouteARAgingDetails
        {
            get { return _RouteARAgingDetails; }
            set { _RouteARAgingDetails = value; }
        }

        /// <summary>
        /// Get or set the route aging by customers
        /// </summary>
        public IList<RouteAgingByCustomers> RouteAgingByCustomersList
        {
            get { return _RouteAgingByCustomersList; }
            set { _RouteAgingByCustomersList = value; }
        }

        #endregion 

    }

    public class RouteAgingDetail
    {
        #region Object and variable declaration

        private string _RouteId = "";
        private string _AgeCategory = "";
        private int _TotalCustomer = 0;
        private decimal _TotalAmount = 0;

        #endregion 

        #region Properties

        /// <summary>
        /// Get or set route id
        /// </summary>
        public string RouteId
        {
            get { return _RouteId; }
            set { _RouteId = value; }
        }

        /// <summary>
        /// Get or set age category
        /// </summary>
        public string AgeCategory
        {
            get { return _AgeCategory; }
            set { _AgeCategory = value; }
        }

        /// <summary>
        /// Get or set total AR age customer count for the route id
        /// </summary>
        public int TotalCustomer
        {
            get { return _TotalCustomer; }
            set { _TotalCustomer = value; }
        }

        /// <summary>
        /// Get or set total Aging amount for route 
        /// </summary>
        public decimal TotalAmount
        {
            get { return _TotalAmount; }
            set { _TotalAmount = value; }
        }
        #endregion 

    }

    public class RouteAgingByCustomers
    {
        #region Variable and object declaration 

        private string _RouteId = "";
        private string _CustomerNo = "";
        private string _CustomerName = "";
        private string _PaymentTerm = "";
        private int _OpenInvoices = 0;
        private decimal _OriginalAmount = 0;
        private decimal _OpenAmount = 0;

        #endregion 

        #region Properties 

        /// <summary>
        /// Get or set route Id
        /// </summary>
        public string RouteId
        {
            get { return _RouteId; }
            set { _RouteId = value; }
        }

        /// <summary>
        /// Get or set customer no
        /// </summary>
        public string CustomerNo
        {
            get { return _CustomerNo; }
            set { _CustomerNo= value; }
        }

        /// <summary>
        /// Get or set customer name
        /// </summary>
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }

        /// <summary>
        /// Get or set payment term
        /// </summary>
        public string PaymentTerm
        {
            get { return _PaymentTerm; }
            set { _PaymentTerm = value; }
        }

        /// <summary>
        /// Get or set open Invoice count
        /// </summary>
        public int OpenInvoices
        {
            get { return _OpenInvoices; }
            set { _OpenInvoices = value; }
        }

        /// <summary>
        /// Get or set orginal amount
        /// </summary>
        public decimal OriginalAmount
        {
            get { return _OriginalAmount; }
            set { _OriginalAmount = value; }
        }

        /// <summary>
        /// Get or set open invoices amount for the customer 
        /// </summary>
        public decimal OpenAmount
        {
            get { return _OpenAmount; }
            set { _OpenAmount = value; }
        }

        #endregion 
    }

    public class RouteUnappliedReceipt
    {

        #region Variable and object declaration 
 
        #endregion 

        #region Properties 

        public string RouteId { get; set; }

        public string RouteName { get; set; }

        public decimal UnappliedAmt { get; set; }

        public int TotalCustomers { get; set; }

        #endregion 

    }
}