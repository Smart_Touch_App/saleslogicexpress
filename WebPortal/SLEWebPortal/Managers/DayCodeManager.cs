﻿using SLEWebPortal.Helpers;
using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using log4net;
using System.Data.SqlClient;
using System.Configuration;
using SLEWebPortal.Hubs;


namespace SLEWebPortal.Managers
{
    public class DayCodeManager
    {
        readonly string _connString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<DelieveryModel> GetDayCodeList()
        {
            try
            {
                logger.Info("DayCodeManager GetDayCodeList");
                string query = string.Empty;
                query = "select d1.DMDDC as DelieveryCode,d1.DMDDCD as Description,d1.DMWPC as Weeks,d1.DMSTAT as Status,d1.DMCRDT as dateofcreation,count(d2.DCDDC) as Stops from Busdta.M56M0001 d1 left join Busdta.M56M0002 d2 on d2.DCDDC=d1.DMDDC group by d1.DMDDC,d1.DMDDCD,d1.DMWPC,d1.DMSTAT,d1.DMCRDT,d1.last_modified order by d1.last_modified desc ";
            //    var connection = new SqlConnection(_connString);
            //    var command = new SqlCommand(query, connection);
            //    var dependency = new SqlDependency(command);
             //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<DelieveryModel>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager GetDayCodeList" + ex.Message);
                throw ex;
            }
        }

        public bool checkifexists(string Code)
        {
            try
            {
                logger.Info("DayCodeManager GetDayCodeList");
                string query = string.Empty;
                query = "select * from busdta.M56M0002 where DCDDC='"+Code+"'";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);
                if (dsDevices.Tables[0].Rows.Count > 0)
                {
                    return false;
                }
                else {
                    return true;
                }
            //    return dsDevices.GetEntityList<DelieveryModel>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager GetDayCodeList" + ex.Message);
                throw ex;
            }
        }


        public List<Stopsandweeks> GetConfiguration(string code)
        {
            try
            {
                logger.Info("DayCodeManager GetConfiguration: "+code);
                string query = string.Empty;
                query = "select DCWN as week,DCDN as stop from Busdta.M56M0002 Where DCDDC='"+ code+"' ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<Stopsandweeks>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager GetConfiguration" + ex.Message);
                throw ex;
            }

        }
        public List<DelieveryModel> GetAllMessages()
        {
           // var messages = new List<Messages>();
            using (var connection = new SqlConnection(_connString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"SELECT [DMDDC],[DMDDCD],[DMSTAT],[DMWPC],[DMCRBY],[DMCRDT],[DMUPBY],[DMUPDT],[last_modified]  FROM [BUSDTA].[M56M0001]", connection))
                {
                    string query = string.Empty;
                    query = "select d1.DMDDC as DelieveryCode,d1.DMDDCD as Description,d1.DMWPC as Weeks,d1.DMSTAT as Status,d1.DMCRDT as dateofcreation,count(d2.DCDDC) as Stops from Busdta.M56M0001 d1 left join Busdta.M56M0002 d2 on d2.DCDDC=d1.DMDDC group by d1.DMDDC,d1.DMDDCD,d1.DMWPC,d1.DMSTAT,d1.DMCRDT,d1.last_modified order by d1.last_modified desc ";

                    command.Notification = null;

                    var dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    var reader = command.ExecuteReader();
                    while (reader.Read())   
                    {
                        //  messages.Add(item: new Messages { MessageID = (int)reader["MessageID"], Message = (string)reader["Message"], EmptyMessage = reader["EmptyMessage"] != DBNull.Value ? (string)reader["EmptyMessage"] : "", MessageDate = Convert.ToDateTime(reader["Date"]) });
                    }
                    DataSet dsDevices = DbHelper.ExecuteDataSet(query);
                    return dsDevices.GetEntityList<DelieveryModel>();
                   
                }

            }
           // return messages;
            


        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                MessagesHub.SendMessages();
            }
        }

        public List<DelieveryModel> GetDayCodeListForToday()
        {
            try
            {
                logger.Info("DayCodeManager GetDayCodeListForToday");
                string query = string.Empty;
                query = "select d1.DMDDC as DelieveryCode,d1.DMDDCD as Description,d1.DMWPC as Weeks,d1.DMSTAT as Status,d1.DMCRDT as dateofcreation,count(d2.DCDDC) as Stops from Busdta.M56M0001 d1 left join Busdta.M56M0002 d2 on d2.DCDDC=d1.DMDDC Where CONVERT(date, d1.DMCRDT)=CONVERT(date, getdate()) group by d1.DMDDC,d1.DMDDCD,d1.DMWPC,d1.DMSTAT,d1.DMCRDT order by d1.DMCRDT desc ";
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);
                return dsDevices.GetEntityList<DelieveryModel>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager GetDayCodeListForToday" + ex.Message);
                throw ex;
            }
        }

        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        public bool AddDayCode(string daycode, int week, string desc)
        {
            string query = string.Empty;
            int result = -1;
            LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
            try
            {
                logger.Info("DayCodeManager AddDayCode Parameters: " + daycode + " " + week + " " + desc);
                //query = "select count(Device_Id) from Busdta.synDeviceMaster where Device_Id='{0}'";
                //query = string.Format(query, deviceID);
                //result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                //if (result == 0)
                //{
                query = "insert into Busdta.M56M0001 values('{0}','{1}',0,'{2}','{3}',getdate(),'{4}',getdate(),getdate())";
                query = string.Format(query, daycode, UppercaseFirst(desc),week,authenticatedUser.ID,authenticatedUser.ID);
                    result = DbHelper.ExecuteNonQuery(query);
                //}

            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager AddDayCode" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool RemoveDayCode(string daycode)
        {
            string query = string.Empty;
            int result = -1;
            LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
            try
            {
                logger.Info("DayCodeManager RemoveDayCode Parameters: " + daycode );
                //query = "select count(Device_Id) from Busdta.synDeviceMaster where Device_Id='{0}'";
                //query = string.Format(query, deviceID);
                //result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                //if (result == 0)
                //{
           //     query = "insert into Busdta.M56M0001 values('{0}','{1}',0,'{2}','{3}',getdate(),'{4}',getdate())";
                query = "Delete from Busdta.M56M0001 where DMDDC='{0}'";
                query = string.Format(query, daycode);
                result = DbHelper.ExecuteNonQuery(query);
                //}

            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager RemoveDayCode" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool RemoveConfig(string daycode)
        {
            string query = string.Empty;
            int result = -1;
            LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
            try
            {
                logger.Info("DayCodeManager RemoveConfig Parameters: " + daycode);
                //query = "select count(Device_Id) from Busdta.synDeviceMaster where Device_Id='{0}'";
                //query = string.Format(query, deviceID);
                //result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                //if (result == 0)
                //{
                //     query = "insert into Busdta.M56M0001 values('{0}','{1}',0,'{2}','{3}',getdate(),'{4}',getdate())";
                query = "Delete from Busdta.M56M0002 where DCDDC='{0}'";
                query = string.Format(query, daycode);
                result = DbHelper.ExecuteNonQuery(query);
                //}

            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager RemoveConfig" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool AddConfig(string daycode, int weekno,int dayno, string desc)
        {
            string query = string.Empty;
            int result = -1;
            LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
            try
            {
                logger.Info("DayCodeManager AddConfig Parameters: " + daycode + " " + weekno + " " + desc + " " + dayno);
                //query = "select count(Device_Id) from Busdta.synDeviceMaster where Device_Id='{0}'";
                //query = string.Format(query, deviceID);
                //result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                //if (result == 0)
                //{
                query = "insert into Busdta.M56M0002 values('{0}','{1}','{2}',1,'{3}',getdate(),'{4}',getdate(),getdate())";
                query = string.Format(query, daycode, weekno,dayno, authenticatedUser.ID, authenticatedUser.ID);
                result = DbHelper.ExecuteNonQuery(query);
                //}

            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager AddConfig" + ex.Message);
          //      throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool UpdateStatus(string daycode,int weeks)
        {
            string query = string.Empty;
            int result = -1;
            LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
            try
            {
                logger.Info("DayCodeManager UpdateStatus Parameters: " + daycode );
                //query = "select count(Device_Id) from Busdta.synDeviceMaster where Device_Id='{0}'";
                //query = string.Format(query, deviceID);
                //result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                //if (result == 0)
                //{
                query = "Update Busdta.M56M0001 set DMSTAT=1,DMWPC="+weeks+",last_modified=getdate() where DMDDC='{0}'";
                query = string.Format(query, daycode);
                result = DbHelper.ExecuteNonQuery(query);

                //}

            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager UpdateStatus" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public string[] GetConfig(string daycode)
        {
            string query = string.Empty;
            int result = -1;
            string[] dayweeks=null;
            LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
            try
            {
                logger.Info("DayCodeManager GetConfig: "+daycode);
                //query = "select count(Device_Id) from Busdta.synDeviceMaster where Device_Id='{0}'";
                //query = string.Format(query, deviceID);
                //result = Convert.ToInt32(DbHelper.ExecuteScalar(query));
                //if (result == 0)
                //{
                query = "Select DCWN,DCDN from Busdta.M56M0002 where DCDDC='"+daycode+"'";

                DataSet dsDayList = DbHelper.ExecuteDataSet(query);
                int counter = 0;
                if (dsDayList.HasData())
                {
                    foreach (DataRow dr in dsDayList.Tables[0].Rows)
                    {
                        counter++;
                    }

                    dayweeks = new string[counter];
                    int i = 0;
                    foreach (DataRow dr in dsDayList.Tables[0].Rows)
                    {
                        dayweeks[i] = (dr["DCWN"] + "-" + dr["DCDN"]);
                        i++;
                    }
                    return dayweeks;
                    //}return dayweeks;
                }
                else
                {
                    return dayweeks;
                }
                

            }
            catch (Exception ex)
            {
                logger.Error("Error in DayCodeManager GetConfig" + ex.Message);
                throw ex;
            }
           
        }
    }
}