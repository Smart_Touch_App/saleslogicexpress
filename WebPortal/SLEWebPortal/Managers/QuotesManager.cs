﻿using SLEWebPortal.Helpers;
using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Managers
{
    public class QuotesManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<QuoteModels> GetQuoteList()
        {
            try
            {
                logger.Info("QuotesManager GetQuoteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select 'Customer' as 'Quotetype', c.RouteId,c1.ABALPH as 'Parent',c.CustomerId as 'ParentNumber',c2.ABALPH as 'BillTo',c.BillToId as 'BillToNumber',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=1 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'Printed',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=0 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'NotPrinted' from busdta.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId)union(Select 'Prospect' as 'Quotetype',p.RouteId,c1.ProspectName as 'Parent',p.ProspectId as 'ParentNumber','NA' as 'BillTo',0 as 'BillToNumber',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=1 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'Printed',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=0 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'NotPrinted' from busdta.Prospect_Quote_Header p,busdta.Prospect_Master c1 where c1.ProspectId=p.ProspectId and c1.RouteID=p.RouteId)";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteList" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListAsDate(string fromdate,string todate)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteListAsDate: "+fromdate+" "+todate+"");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select 'Customer' as 'Quotetype', c.RouteId,c1.ABALPH as 'Parent',c.CustomerId as 'ParentNumber',c2.ABALPH as 'BillTo',c.BillToId as 'BillToNumber',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=1 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'Printed',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=0 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'NotPrinted' from busdta.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId and c.PriceDate between '" + fromdate + "' and '" + todate + "' )union (Select 'Prospect' as 'Quotetype',p.RouteId,c1.ProspectName as 'Parent',p.ProspectId as 'ParentNumber','NA' as 'BillTo',0 as 'BillToNumber',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=1 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'Printed',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=0 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'NotPrinted' from busdta.Prospect_Quote_Header p,busdta.Prospect_Master c1 where c1.ProspectId=p.ProspectId and c1.RouteID=p.RouteId and p.PriceDate between '" + fromdate + "' and '" + todate + "' ) ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteList" + ex.Message);
                throw ex;
            }
        }

        public List<Items> GetItemsForProspect(string id)
        {
            try
            {
                logger.Info("QuotesManager GetItemsForProspect: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select i.IMLITM as 'ItemNo',i.IMDSC1 as 'Description',p.OtherAmt as 'Other',p.OtherUM as 'OtherUOM',p.PricingAmt as 'Pricing',p.PricingUM as 'PricingUOM',p.ProspectQuoteId,p.QuoteQty,p.SampleQty,p.SampleUM,p.TransactionAmt as 'Trans',p.TransactionUM as 'TransUOM',p.UnitPriceAmt as 'Price',p.UnitPriceUM as 'PriceUOM',p.CompetitorName from busdta.Prospect_Quote_Detail p,busdta.F4101 i where i.IMITM=p.ItemId and p.ProspectQuoteId=" + id + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<Items>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetItemsForProspect" + ex.Message);
                throw ex;
            }
        }

        public List<Items> GetItemsForCustomer(string id)
        {
            try
            {
                logger.Info("QuotesManager GetItemsForProspect: " + id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select c.CustomerQuoteId,i.IMLITM as 'ItemNo',i.IMDSC1 as 'Description',c.OtherAmt as 'Other',c.OtherUM as 'OtherUOM',c.PricingAmt as 'Pricing',c.PricingUM as 'PricingUOM',c.QuoteQty,c.TransactionAmt as 'Trans',c.TransactionUM as 'TransUOM',c.UnitPriceAmt as 'Price',c.UnitPriceUM as 'PriceUOM',c.CompetitorName from  BUSDTA.Customer_Quote_Detail c,BUSDTA.F4101 i where c.ItemId=i.IMITM and c.CustomerQuoteId=" + id + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<Items>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetItemsForProspect" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListByIDCustomer(string BillToId, string ParentId, string RouteId)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select c.RouteId,c1.ABALPH as 'Parent',c.CustomerId as 'ParentNumber',c2.ABALPH as 'BillTo',c.BillToId as 'BillToNumber',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=1 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'Printed',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=0 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'NotPrinted' from busdta.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId and c.RouteId=" + RouteId + " and c.CustomerId=" + ParentId + " and c.BillToId=" + BillToId + ")";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteList" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListByIDCustomerAsDate(string BillToId, string ParentId, string RouteId,string fromdate,string todate)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select c.RouteId,c1.ABALPH as 'Parent',c.CustomerId as 'ParentNumber',c2.ABALPH as 'BillTo',c.BillToId as 'BillToNumber',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=1 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'Printed',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=0 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'NotPrinted' from busdta.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId and c.RouteId=" + RouteId + " and c.CustomerId=" + ParentId + " and c.BillToId=" + BillToId + ")";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteList" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListByIDProspect( string ParentId, string RouteId)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select p.RouteId,c1.ProspectName as 'Parent',p.ProspectId as 'ParentNumber','NA' as 'BillTo',0 as 'BillToNumber',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=1 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'Printed',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=0 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'NotPrinted' from busdta.Prospect_Quote_Header p,busdta.Prospect_Master c1 where c1.ProspectId=p.ProspectId and p.ProspectId=" + ParentId + " and p.RouteId=" + RouteId + ")";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteList" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListByIDProspectAsDate(string ParentId, string RouteId,string fromdate,string todate)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select p.RouteId,c1.ProspectName as 'Parent',p.ProspectId as 'ParentNumber','NA' as 'BillTo',0 as 'BillToNumber',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=1 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'Printed',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=0 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'NotPrinted' from busdta.Prospect_Quote_Header p,busdta.Prospect_Master c1 where c1.ProspectId=p.ProspectId and p.ProspectId=" + ParentId + " and p.RouteId=" + RouteId + ")";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteList" + ex.Message);
                throw ex;
            }
        }

        public List<QuotesInfo> GetQuoteInfoListForIDCustomer(string BillToId, string ParentId, string RouteId,string id)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteInfoListFor");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                if(id=="0")
                {
                    query = "SELECT c.RouteId, c.BillToId,c1.ABALPH as 'Customer',c2.ABALPH as 'BillTo', c.CustomerQuoteId as 'QuoteId', c.CustomerId, c.IsPrinted,c.IsSampled, c.AIAC15CategoryCode15, c.AIAC15CategoryCode15Mst, c.AIAC16CategoryCode16, c.AIAC16CategoryCode16Mst,c.AIAC17CategoryCode17, c.AIAC17CategoryCode17Mst, c.AIAC18CategoryCode18, c.AIAC18CategoryCode18Mst, c.AIAC22POSUpCharge, c.AIAC22POSUpChargeMst,c.AIAC23LiquidCoffee, c.AIAC23LiquidCoffeeMst, c.AIAC24PriceProtection, c.AIAC24PriceProtectionMst, c.AIAC27AlliedDiscount, c.AIAC27AlliedDiscountMst,c.AIAC28CoffeeVolumeMst, c.AIAC28CoffeeVolume, c.AIAC29EquipmentProgPts, c.AIAC29EquipmentProgPtsMst, c.AIAC30SpecialCCP, c.AIAC30SpecialCCPMst, c.PriceDate,(Select Count(*) from busdta.Customer_Quote_Detail cq where cq.CustomerQuoteId=c.CustomerQuoteId) as 'QuotedItems' FROM BUSDTA.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c.BillToId=" + BillToId + " and c.CustomerId=" + ParentId + " and c.RouteId=" + RouteId + " and c1.ABAN8=c.BillToId and c2.ABAN8=c.CustomerId";
                }
                else
                {
                    query = "SELECT c.RouteId, c.BillToId,c1.ABALPH as 'Customer',c2.ABALPH as 'BillTo', c.CustomerQuoteId as 'QuoteId', c.CustomerId, c.IsPrinted,c.IsSampled, c.AIAC15CategoryCode15, c.AIAC15CategoryCode15Mst, c.AIAC16CategoryCode16, c.AIAC16CategoryCode16Mst,c.AIAC17CategoryCode17, c.AIAC17CategoryCode17Mst, c.AIAC18CategoryCode18, c.AIAC18CategoryCode18Mst, c.AIAC22POSUpCharge, c.AIAC22POSUpChargeMst,c.AIAC23LiquidCoffee, c.AIAC23LiquidCoffeeMst, c.AIAC24PriceProtection, c.AIAC24PriceProtectionMst, c.AIAC27AlliedDiscount, c.AIAC27AlliedDiscountMst,c.AIAC28CoffeeVolumeMst, c.AIAC28CoffeeVolume, c.AIAC29EquipmentProgPts, c.AIAC29EquipmentProgPtsMst, c.AIAC30SpecialCCP, c.AIAC30SpecialCCPMst, c.PriceDate,(Select Count(*) from busdta.Customer_Quote_Detail cq where cq.CustomerQuoteId=c.CustomerQuoteId) as 'QuotedItems' FROM BUSDTA.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c.BillToId=" + BillToId + " and c.CustomerId=" + ParentId + " and c.RouteId=" + RouteId + " and c.CustomerQuoteId=" + id + " and c1.ABAN8=c.BillToId and c2.ABAN8=c.CustomerId";
                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuotesInfo>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteListForCustomer" + ex.Message);
                throw ex;
            }
        }

        public List<QuotesInfo> GetQuoteInfoListForIDCustomerAsDate(string BillToId, string ParentId, string RouteId, string id,string fromdate,string todate)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteInfoListFor");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                if (id == "0")
                {
                    query = "SELECT c.RouteId, c.BillToId,c1.ABALPH as 'Customer',c2.ABALPH as 'BillTo', c.CustomerQuoteId as 'QuoteId', c.CustomerId, c.IsPrinted,c.IsSampled, c.AIAC15CategoryCode15, c.AIAC15CategoryCode15Mst, c.AIAC16CategoryCode16, c.AIAC16CategoryCode16Mst,c.AIAC17CategoryCode17, c.AIAC17CategoryCode17Mst, c.AIAC18CategoryCode18, c.AIAC18CategoryCode18Mst, c.AIAC22POSUpCharge, c.AIAC22POSUpChargeMst,c.AIAC23LiquidCoffee, c.AIAC23LiquidCoffeeMst, c.AIAC24PriceProtection, c.AIAC24PriceProtectionMst, c.AIAC27AlliedDiscount, c.AIAC27AlliedDiscountMst,c.AIAC28CoffeeVolumeMst, c.AIAC28CoffeeVolume, c.AIAC29EquipmentProgPts, c.AIAC29EquipmentProgPtsMst, c.AIAC30SpecialCCP, c.AIAC30SpecialCCPMst, c.PriceDate,(Select Count(*) from busdta.Customer_Quote_Detail cq where cq.CustomerQuoteId=c.CustomerQuoteId) as 'QuotedItems' FROM BUSDTA.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c.BillToId=" + BillToId + " and c.CustomerId=" + ParentId + " and c.RouteId=" + RouteId + " and c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId and c.PriceDate between '" + fromdate + "' and '" + todate + "'";
                }
                else
                {
                    query = "SELECT c.RouteId, c.BillToId,c1.ABALPH as 'Customer',c2.ABALPH as 'BillTo', c.CustomerQuoteId as 'QuoteId', c.CustomerId, c.IsPrinted,c.IsSampled, c.AIAC15CategoryCode15, c.AIAC15CategoryCode15Mst, c.AIAC16CategoryCode16, c.AIAC16CategoryCode16Mst,c.AIAC17CategoryCode17, c.AIAC17CategoryCode17Mst, c.AIAC18CategoryCode18, c.AIAC18CategoryCode18Mst, c.AIAC22POSUpCharge, c.AIAC22POSUpChargeMst,c.AIAC23LiquidCoffee, c.AIAC23LiquidCoffeeMst, c.AIAC24PriceProtection, c.AIAC24PriceProtectionMst, c.AIAC27AlliedDiscount, c.AIAC27AlliedDiscountMst,c.AIAC28CoffeeVolumeMst, c.AIAC28CoffeeVolume, c.AIAC29EquipmentProgPts, c.AIAC29EquipmentProgPtsMst, c.AIAC30SpecialCCP, c.AIAC30SpecialCCPMst, c.PriceDate,(Select Count(*) from busdta.Customer_Quote_Detail cq where cq.CustomerQuoteId=c.CustomerQuoteId) as 'QuotedItems' FROM BUSDTA.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c.BillToId=" + BillToId + " and c.CustomerId=" + ParentId + " and c.RouteId=" + RouteId + " and c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId and c.CustomerQuoteId=" + id + " and c.PriceDate between '" + fromdate + "' and '" + todate + "'";
                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuotesInfo>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteListForCustomer" + ex.Message);
                throw ex;
            }
        }

        public List<QuotesInfo> GetQuoteInfoListForIDProspect(string ProspectId, string RouteId,string id)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteInfoListFor");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                if(id=="0")
                {
                    query = "SELECT p.RouteId,  p.ProspectQuoteId as 'QuoteId', p.ProspectId as 'CustomerId',c1.ProspectName as 'Customer', IsPrinted,IsSampled, AIAC15CategoryCode15, AIAC15CategoryCode15Mst, AIAC16CategoryCode16, AIAC16CategoryCode16Mst,AIAC17CategoryCode17, AIAC17CategoryCode17Mst, AIAC18CategoryCode18, AIAC18CategoryCode18Mst, AIAC22POSUpCharge, AIAC22POSUpChargeMst,AIAC23LiquidCoffee, AIAC23LiquidCoffeeMst, AIAC24PriceProtection, AIAC24PriceProtectionMst, AIAC27AlliedDiscount, AIAC27AlliedDiscountMst,AIAC28CoffeeVolumeMst, AIAC28CoffeeVolume, AIAC29EquipmentProgPts, AIAC29EquipmentProgPtsMst, AIAC30SpecialCCP, AIAC30SpecialCCPMst, PriceDate,(Select Count(*) from busdta.Prospect_Quote_Detail p where p.ProspectQuoteId=ProspectQuoteId) as 'QuotedItems' FROM BUSDTA.Prospect_Quote_Header p,busdta.Prospect_Master c1 where p.ProspectId=" + ProspectId + " and p.RouteId=" + RouteId + " and c1.ProspectId=p.ProspectId";
                }
                else
                {
                    query = "SELECT p.RouteId,  p.ProspectQuoteId as 'QuoteId', p.ProspectId as 'CustomerId',c1.ProspectName as 'Customer', IsPrinted, AIAC15CategoryCode15, AIAC15CategoryCode15Mst, AIAC16CategoryCode16, AIAC16CategoryCode16Mst,AIAC17CategoryCode17, AIAC17CategoryCode17Mst, AIAC18CategoryCode18, AIAC18CategoryCode18Mst, AIAC22POSUpCharge, AIAC22POSUpChargeMst,AIAC23LiquidCoffee, AIAC23LiquidCoffeeMst, AIAC24PriceProtection, AIAC24PriceProtectionMst, AIAC27AlliedDiscount, AIAC27AlliedDiscountMst,AIAC28CoffeeVolumeMst, AIAC28CoffeeVolume, AIAC29EquipmentProgPts, AIAC29EquipmentProgPtsMst, AIAC30SpecialCCP, AIAC30SpecialCCPMst, PriceDate,(Select Count(*) from busdta.Prospect_Quote_Detail p where p.ProspectQuoteId=ProspectQuoteId) as 'QuotedItems' FROM BUSDTA.Prospect_Quote_Header p,busdta.Prospect_Master c1 where p.ProspectId=" + ProspectId + " and p.RouteId=" + RouteId + " and ProspectQuoteId=" + id + " and c1.ProspectId=p.ProspectId";
                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuotesInfo>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteListForCustomer" + ex.Message);
                throw ex;
            }
        }

        public List<QuotesInfo> GetQuoteInfoListForIDProspectAsDate(string ProspectId, string RouteId, string id,string fromdate,string todate)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteInfoListFor");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                if (id == "0")
                {
                    query = "SELECT RouteId,  ProspectQuoteId as 'QuoteId', ProspectId as 'ParentId', IsPrinted,IsSampled, AIAC15CategoryCode15, AIAC15CategoryCode15Mst, AIAC16CategoryCode16, AIAC16CategoryCode16Mst,AIAC17CategoryCode17, AIAC17CategoryCode17Mst, AIAC18CategoryCode18, AIAC18CategoryCode18Mst, AIAC22POSUpCharge, AIAC22POSUpChargeMst,AIAC23LiquidCoffee, AIAC23LiquidCoffeeMst, AIAC24PriceProtection, AIAC24PriceProtectionMst, AIAC27AlliedDiscount, AIAC27AlliedDiscountMst,AIAC28CoffeeVolumeMst, AIAC28CoffeeVolume, AIAC29EquipmentProgPts, AIAC29EquipmentProgPtsMst, AIAC30SpecialCCP, AIAC30SpecialCCPMst, PriceDate,(Select Count(*) from busdta.Prospect_Quote_Detail p where p.ProspectQuoteId=ProspectQuoteId) as 'QuotedItems' FROM BUSDTA.Prospect_Quote_Header where ProspectId=" + ProspectId + " and RouteId=" + RouteId + " and PriceDate between '" + fromdate + "' and '" + todate + "'";
                }
                else
                {
                    query = "SELECT RouteId,  ProspectQuoteId as 'QuoteId', ProspectId as 'ParentId', IsPrinted, AIAC15CategoryCode15, AIAC15CategoryCode15Mst, AIAC16CategoryCode16, AIAC16CategoryCode16Mst,AIAC17CategoryCode17, AIAC17CategoryCode17Mst, AIAC18CategoryCode18, AIAC18CategoryCode18Mst, AIAC22POSUpCharge, AIAC22POSUpChargeMst,AIAC23LiquidCoffee, AIAC23LiquidCoffeeMst, AIAC24PriceProtection, AIAC24PriceProtectionMst, AIAC27AlliedDiscount, AIAC27AlliedDiscountMst,AIAC28CoffeeVolumeMst, AIAC28CoffeeVolume, AIAC29EquipmentProgPts, AIAC29EquipmentProgPtsMst, AIAC30SpecialCCP, AIAC30SpecialCCPMst, PriceDate,(Select Count(*) from busdta.Prospect_Quote_Detail p where p.ProspectQuoteId=ProspectQuoteId) as 'QuotedItems' FROM BUSDTA.Prospect_Quote_Header where ProspectId=" + ProspectId + " and RouteId=" + RouteId + " and ProspectQuoteId=" + id + " and PriceDate between '" + fromdate + "' and '" + todate + "'";
                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuotesInfo>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteListForCustomer" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListForCustomer()
        {
            try
            {
                logger.Info("QuotesManager GetQuoteListForCustomer");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select distinct 'Customer' as 'Quotetype', c.RouteId,c1.ABALPH as 'Parent',c.CustomerId as 'ParentNumber',c2.ABALPH as 'BillTo',c.BillToId as 'BillToNumber',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=1 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'Printed',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=0 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'NotPrinted' from busdta.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId) ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteListForCustomer" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListForProspect()
        {
            try
            {
                logger.Info("QuotesManager GetQuoteListForCustomer");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select distinct 'Prospect' as 'Quotetype',p.RouteId,c1.ProspectName as 'Parent',p.ProspectId as 'ParentNumber','NA' as 'BillTo',0 as 'BillToNumber',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=1 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'Printed',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=0 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'NotPrinted' from busdta.Prospect_Quote_Header p,busdta.Prospect_Master c1 where c1.ProspectId=p.ProspectId and c1.RouteID=p.RouteId)";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteListForCustomer" + ex.Message);
                throw ex;
            }
        }

        public List<QuoteModels> GetQuoteListForRoute(int id)
        {
            try
            {
                logger.Info("QuotesManager GetQuoteListForRoute: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "Select c.CustomerQuoteId,c.RouteId,c1.ABALPH as 'Parent',c.ParentId as 'ParentNumber',c2.ABALPH as 'BillTo',c.BillToId as 'BillToNumber',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=1 and RouteId=c.RouteId and ParentId=c.ParentId and BillToId=c.BillToId) as 'Printed',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=0 and RouteId=c.RouteId and ParentId=c.ParentId and BillToId=c.BillToId) as 'NotPrinted' from busdta.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c1.ABAN8=c.ParentId and c2.ABAN8=c.BillToId and c.RouteId="+id+"";
                query = "(Select distinct 'Customer' as 'Quotetype', c.RouteId,c1.ABALPH as 'Parent',c.CustomerId as 'ParentNumber',c2.ABALPH as 'BillTo',c.BillToId as 'BillToNumber',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=1 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'Printed',(Select Count(isPrinted) from busdta.Customer_Quote_Header where IsPrinted=0 and RouteId=c.RouteId and CustomerId=c.CustomerId and BillToId=c.BillToId) as 'NotPrinted' from busdta.Customer_Quote_Header c,busdta.F0101 c1,busdta.F0101 c2 where c1.ABAN8=c.CustomerId and c2.ABAN8=c.BillToId and c.RouteId=" + id + ")union (Select 'Prospect' as 'Quotetype',p.RouteId,c1.ProspectName as 'Parent',p.ProspectId as 'ParentNumber','NA' as 'BillTo',0 as 'BillToNumber',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=1 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'Printed',(Select Count(isPrinted) from busdta.Prospect_Quote_Header where IsPrinted=0 and RouteId=p.RouteId and ProspectId=p.ProspectId) as 'NotPrinted' from busdta.Prospect_Quote_Header p,busdta.Prospect_Master c1 where c1.ProspectId=p.ProspectId and c1.RouteID=p.RouteId and p.RouteId=" + id + ") ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<QuoteModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetQuoteListForRoute" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetAllRouteList()
        {
            try
            {
                logger.Info("QuotesManager GetAllRouteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and s.StatusTypeID=c.CycleCountTypeId";
                query = "Select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,busdta.Customer_Quote_Header c where r.RouteMasterID=c.RouteId Union Select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,busdta.Prospect_Quote_Header c where r.RouteMasterID=c.RouteId";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetAllRouteList" + ex.Message);
                throw ex;
            }
        }


        public List<RouteListModels> GetAllRouteListAsDate(string fromdate,string todate)
        {
            try
            {
                logger.Info("QuotesManager GetAllRouteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and s.StatusTypeID=c.CycleCountTypeId";
               // query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c,BUSDTA.Prospect_Quote_Header p where r.RouteMasterID=p.RouteId or r.RouteMasterID=c.RouteId and (p.PriceDate between '" + fromdate + "' and '" + todate + "' or c.PriceDate between '" + fromdate + "' and '" + todate + "')";
                query = "Select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,busdta.Customer_Quote_Header c where r.RouteMasterID=c.RouteId and r.RouteMasterID=c.RouteId and c.PriceDate between '" + fromdate + "' and '" + todate + "' Union Select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,busdta.Prospect_Quote_Header c where r.RouteMasterID=c.RouteId and r.RouteMasterID=c.RouteId and c.PriceDate between '" + fromdate + "' and '" + todate + "'";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetAllRouteList" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRouteListByID(string id)
        {
            try
            {
                logger.Info("QuotesManager GetAllRouteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and s.StatusTypeID=c.CycleCountTypeId";
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r where r.RouteMasterID="+id+"";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetAllRouteList" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRouteListCustomer()
        {
            try
            {
                logger.Info("QuotesManager GetAllRouteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and s.StatusTypeID=c.CycleCountTypeId";
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c where r.RouteMasterID=c.RouteId ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetAllRouteList" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRouteListProspect()
        {
            try
            {
                logger.Info("QuotesManager GetAllRouteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and s.StatusTypeID=c.CycleCountTypeId";
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,BUSDTA.Prospect_Quote_Header c where r.RouteMasterID=c.RouteId ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetAllRouteList" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRouteListForRoute(int id)
        {
            try
            {
                logger.Info("QuotesManager GetRouteListForRoute: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and s.StatusTypeID=c.CycleCountTypeId";
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName from BUSDTA.Route_Master r,BUSDTA.Customer_Quote_Header c where r.RouteMasterID=c.RouteId and c.RouteId="+id+"";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in QuotesManager GetRouteListForRoute" + ex.Message);
                throw ex;
            }
        }
    }
}