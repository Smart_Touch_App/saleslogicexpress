﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SLEWebPortal.Models;
using System.Data;
using SLEWebPortal.Helpers;

namespace SLEWebPortal.Managers
{

    public class DSADashboardManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List< ProductReturnsModels> GetProductReturnsAll()
        {
            try
            {
                logger.Info("DSADashboardManager GetProductReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
             //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = @"Select 
o.OrderID
,o.RouteID
,o.OrderTypeid
,Convert(decimal(10, 2),o.OrderTotalAmt) as 'TotalAmount'
,Convert(decimal(10, 2),IsNull(o.EnergySurchargeAmt,0)) as 'Surcharge'
,o.CustomerId,'1224516' as 'CustomerContact',cm.ABALPH as 'CustomerName'
,s.StatusTypeCD as 'Status'
,Count(od.OrderDetailId) as 'TotalItems',o.OrderDate as 'OrderDatetime' from busdta.Order_Header o 
left outer join BUSDTA.Order_Detail od on o.OrderId=od.OrderID 
left outer join busdta.f0101 cm on o.CustomerId = cm.ABAN8
left outer join BUSDTA.Status_Type s on o.OrderStateId=s.StatusTypeID
where o.OrderTypeId = (Select StatusTypeID from busdta.Status_Type where StatusTypeCD='RETORD') -- ReturnOrder
group by o.OrderID,o.RouteID,o.OrderTypeid,o.OrderTotalAmt,o.EnergySurchargeAmt,o.CustomerId,cm.ABALPH,o.OrderDate,s.StatusTypeCD";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ProductReturnsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetProductReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public List<Products> GetProducts(int id)
        {
            try
            {
                logger.Info("DSADashboardManager GetProductReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select o.OrderID as 'OrderNo',im.IMLITM as 'ItemNo',im.IMDSC1 as 'ItemDesc',o.OrderQty,o.OrderUM as 'OrderUOM',o.ReturnHeldQty as 'QtyReturned',Convert(decimal(10, 2),o.UnitPriceAmt) as 'UnitPrice',Convert(decimal(10, 2),o.ItemSalesTaxAmt) as 'TaxAmount',Convert(decimal(10, 2),o.ExtnPriceAmt) as 'ExtendedPrice' from busdta.Order_Detail o,busdta.F4101 im where o.OrderID=" + id + " and im.IMITM=o.ItemId";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<Products>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetProductReturnsAll" + ex.Message);
                throw ex;
            }
        }
        public List<ProductReturnsModels> GetProductReturnsforRoute(int id)
        {
            try
            {
                logger.Info("DSADashboardManager GetProductReturnsforRoute: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = @"Select 
o.OrderID
,o.RouteID
,o.OrderTypeid
,Convert(decimal(10, 2),o.OrderTotalAmt) as 'TotalAmount'
,Convert(decimal(10, 2),IsNull(o.EnergySurchargeAmt,0)) as 'Surcharge'
,o.CustomerId,'1224516' as 'CustomerContact',cm.ABALPH as 'CustomerName'
,s.StatusTypeCD as 'Status'
,Count(od.OrderDetailId) as 'TotalItems',o.OrderDate as 'OrderDatetime' from busdta.Order_Header o 
left outer join BUSDTA.Order_Detail od on o.OrderId=od.OrderID 
left outer join busdta.f0101 cm on o.CustomerId = cm.ABAN8
left outer join BUSDTA.Status_Type s on o.OrderStateId=s.StatusTypeID
where o.OrderTypeId = (Select StatusTypeID from busdta.Status_Type where StatusTypeCD='RETORD') and o.RouteId="+id+@"
group by o.OrderID,o.RouteID,o.OrderTypeid,o.OrderTotalAmt,o.EnergySurchargeAmt,o.CustomerId,cm.ABALPH,o.OrderDate,s.StatusTypeCD";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ProductReturnsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetProductReturnsforRoute" + ex.Message);
                throw ex;
            }
        }

        public List<ProductReturnsModels> GetProductReturnsforOrderID(int id)
        {
            try
            {
                logger.Info("DSADashboardManager GetProductReturnsforRoute: " + id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = @"Select 
o.OrderID
,o.RouteID
,o.OrderTypeid
,Convert(decimal(10, 2),o.OrderTotalAmt) as 'TotalAmount'
,Convert(decimal(10, 2),IsNull(o.EnergySurchargeAmt,0)) as 'Surcharge'
,o.CustomerId,'1224516' as 'CustomerContact',cm.ABALPH as 'CustomerName'
,s.StatusTypeCD as 'Status'
,Count(od.OrderDetailId) as 'TotalItems',o.OrderDate as 'OrderDatetime' from busdta.Order_Header o 
left outer join BUSDTA.Order_Detail od on o.OrderId=od.OrderID 
left outer join busdta.f0101 cm on o.CustomerId = cm.ABAN8
left outer join BUSDTA.Status_Type s on o.OrderStateId=s.StatusTypeID
where o.OrderTypeId = (Select StatusTypeID from busdta.Status_Type where StatusTypeCD='RETORD') and o.OrderID="+id+@"
group by o.OrderID,o.RouteID,o.OrderTypeid,o.OrderTotalAmt,o.EnergySurchargeAmt,o.CustomerId,cm.ABALPH,o.OrderDate,s.StatusTypeCD";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ProductReturnsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetProductReturnsforRoute" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforReturnsAll()
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid' from busdta.Route_Master r,BUSDTA.Order_Header o where o.RouteID=r.RouteMasterID and o.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='RETORD') ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforCreditMemoAll()
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforCreditMemoAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid' from busdta.Route_Master r,BUSDTA.Receipt_Header rh where rh.RouteId=r.RouteMasterID ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforCreditMemoAll" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforCreditMemoForRoute(int id)
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforCreditMemoForRoute: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid' from busdta.Route_Master r,BUSDTA.Receipt_Header rh where rh.RouteId=r.RouteMasterID and r.RouteMasterID="+id+"";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforCreditMemoForRoute" + ex.Message);
                throw ex;
            }
        }

        public List<CreditMemo> GetReceiptsforCreditMemoAll()
        {
            try
            {
                logger.Info("DSADashboardManager GetReceiptsforCreditMemoAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select rh.CustomerId,rh.RouteId,rh.ReceiptID,rh.ReceiptNumber as 'CreditMemoNo',rh.ReceiptDate,Convert(decimal(10, 2),rh.TransactionAmount) as 'Amount',rh.CreditMemoNote,r.ReasonCodeDescription as 'Reason',a.ABALPH as 'CustomerName',s.StatusTypeCD as 'Status',IsNull(rh.JDEStatusId,0) as 'JDEStatusId' from busdta.Receipt_Header rh,busdta.f0101 a,busdta.Status_Type s,busdta.ReasonCodeMaster r where rh.Statusid=s.StatusTypeID and rh.CustomerId=a.ABAN8 and rh.CreditReasonCodeId=r.ReasonCodeId ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CreditMemo>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetReceiptsforCreditMemoAll" + ex.Message);
                throw ex;
            }
        }

        public List<CreditMemo> GetReceiptsforCreditMemoForRoute(int id)
        {
            try
            {
                logger.Info("DSADashboardManager GetReceiptsforCreditMemoAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select rh.CustomerId,rh.RouteId,rh.ReceiptID,rh.ReceiptNumber as 'CreditMemoNo',rh.ReceiptDate,Convert(decimal(10, 2),rh.TransactionAmount) as 'Amount',rh.CreditMemoNote,r.ReasonCodeDescription as 'Reason',a.ABALPH as 'CustomerName',s.StatusTypeCD as 'Status' from busdta.Receipt_Header rh,busdta.f0101 a,busdta.Status_Type s,busdta.ReasonCodeMaster r where rh.Statusid=s.StatusTypeID and rh.CustomerId=a.ABAN8 and rh.CreditReasonCodeId=r.ReasonCodeId and rh.RouteId=" + id + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CreditMemo>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetReceiptsforCreditMemoAll" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforReturnsforRoute(int id)
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid' from busdta.Route_Master r,BUSDTA.Order_Header o where o.RouteID=r.RouteMasterID and r.RouteMasterID="+id+"";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforReturnsforOrderID(int id)
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid' from busdta.Route_Master r,BUSDTA.Order_Header o where o.RouteID=r.RouteMasterID and o.OrderID=" + id + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public List<ProductReturnsModels> GetProducts()
        {
            try
            {
                logger.Info("DSADashboardManager GetProductReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ProductReturnsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetProductReturnsAll" + ex.Message);
                throw ex;
            }
        }
    }
}