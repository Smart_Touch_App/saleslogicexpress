﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using log4net;
namespace SLEWebPortal.Managers
{
    public class CustomerManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Customer> GetCustomersForRoute(string routeID)
        {

            try
            {
                logger.Info("CustomerManager GetOrdersForCustomer Parameters: " + routeID);
                string query = string.Empty;
                //query = query + "SELECT distinct a.aban8,AIZON,AISTOP as DeliveryCode,convert(varchar,a.aban8) AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.cdar1+c.cdph1) AS Phone# , (d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip FROM busdta.F0101 AS a JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 LEFT JOIN busdta.M0111 AS c ON a.aban8 =c.cdan8 and c.CDIDLN=1 join busdta.F0116 as d on a.aban8 =d.alan8 LEFT OUTER JOIN BUSDTA.Customer_Route_Map crm on aian8 = crm.CustomerShipToNumber   where AIZON>' ' and AISTOP>' '  and cs.AISTOP <> '7' and crm.IsActive = 'Y' ";
                //query = query + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
                //query = query + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
                //query = query + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip ";
                //query = query + "FROM busdta.F0101 AS a ";
                //query = query + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
                //query = query + "JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 ";
                //query = query + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
                //query = query + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
                //query = query + "where AIZON>' ' and AISTOP>' '  ";
                query = @"SELECT distinct  a.aban8,t.phone# as Phone,AIZON,AISTOP as DeliveryCode,convert(varchar,a.aban8) AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , 
                        a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type,
                        (d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip FROM busdta.F0101 AS a JOIN busdta.F03012 AS cs 
                        ON cs.aian8 =a.aban8 JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 LEFT JOIN busdta.M0111 AS c ON a.aban8 =c.cdan8 and c.CDIDLN=1 join busdta.F0116 as d on
                        a.aban8 =d.alan8 LEFT OUTER JOIN BUSDTA.Customer_Route_Map crm on aian8 = crm.CustomerShipToNumber 
                        left join (select  (cdar1+cdph1)  phone#,cdan8,cdidln from BUSDTA.m0111) as t  on a.aban8 =t.cdan8 
                        where AIZON>' ' and AISTOP>' '  and cs.AISTOP <> '7' and crm.IsActive = 'Y'  ";
                query = query + " and a.abac03 ='{0}' and (t.cdidln=0 or t.CDIDLN is null)";
                query = string.Format(query, routeID.Replace("FBM", ""));
                List<Customer> customersList = new List<Customer>();
                DataSet dsCustomersForRoute = DbHelper.ExecuteDataSet(query);
                return dsCustomersForRoute.GetEntityList<Customer>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CustomerManager GetCustomersForRoute" + ex.Message);
                throw ex;
            }
        }
        public List<Customer> GetAllCustomers()
        {
            try {
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string route = authenticatedUser.Route;
                logger.Info("CustomerManager GetAllCustomers");
            string query = string.Empty;
       //     query = query + "SELECT distinct top 1000 a.aban8,AIZON,AISTOP as DeliveryCode,convert(varchar,a.aban8) AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.cdar1+c.cdph1) AS Phone# , (d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip FROM busdta.F0101 AS a JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 LEFT JOIN busdta.M0111 AS c ON a.aban8 =c.cdan8 and c.CDIDLN=1 join busdta.F0116 as d on a.aban8 =d.alan8  where AIZON>' ' and AISTOP>' ' ";
            query = query + "SELECT distinct top 1000 a.aban8,AIZON,AISTOP as DeliveryCode,convert(varchar,a.aban8) AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.cdar1+c.cdph1) AS Phone# , (d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip FROM busdta.F0101 AS a JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 LEFT JOIN busdta.M0111 AS c ON a.aban8 =c.cdan8 and c.CDIDLN=1 join busdta.F0116 as d on a.aban8 =d.alan8  where AIZON>' ' and AISTOP>' ' ";
            //query = query + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
            //query = query + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
            //query = query + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip ";
            //query = query + "FROM busdta.F0101 AS a ";
            //query = query + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
            //query = query + "JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 ";
            //query = query + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
            //query = query + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
            //query = query + "where AIZON>' ' and AISTOP>' '  ";
            List<Customer> customersList = new List<Customer>();
            DataSet dsCustomersForRoute = DbHelper.ExecuteDataSet(query);
            return dsCustomersForRoute.GetEntityList<Customer>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CustomerManager GetAllCustomers" + ex.Message);
                throw ex;
            }
        }

        public string GetAllCustomersCount()
        {
            try
            {
                logger.Info("CustomerManager GetAllCustomers");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string route = authenticatedUser.UserName;
                string query = "with result as(SELECT distinct a.aban8 FROM busdta.F0101 AS a JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 LEFT JOIN busdta.M0111 AS c ON a.aban8 =c.cdan8 and c.CDIDLN=1 join busdta.F0116 as d on a.aban8 =d.alan8 LEFT OUTER JOIN BUSDTA.Customer_Route_Map crm on aian8 = crm.CustomerShipToNumber   where AIZON>' ' and AISTOP>' '  and cs.AISTOP <> '7' and crm.IsActive = 'Y' ";
                //     query = query + "SELECT distinct top 1000 a.aban8,AIZON,AISTOP as DeliveryCode,convert(varchar,a.aban8) AS CustomerNo, a.abalph AS Name, a.abat1 AS Address_type , a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.cdar1+c.cdph1) AS Phone# , (d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip FROM busdta.F0101 AS a JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 LEFT JOIN busdta.M0111 AS c ON a.aban8 =c.cdan8 and c.CDIDLN=1 join busdta.F0116 as d on a.aban8 =d.alan8  where AIZON>' ' and AISTOP>' ' ";
                //query = query + "SELECT count(a.aban8) FROM busdta.F0101 AS a JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 LEFT JOIN busdta.M0111 AS c ON a.aban8 =c.cdan8 and c.CDIDLN=1 join busdta.F0116 as d on a.aban8 =d.alan8 LEFT OUTER JOIN BUSDTA.Customer_Route_Map crm on aian8 = crm.CustomerShipToNumber where AIZON>' ' and AISTOP>' ' and cs.AISTOP <> '7' and crm.IsActive = 'Y' ";
                //query = query + "a.abac03 AS Route, a.abmcu AS Route_Branch , a.aban81 AS Bill_To , ";
                //query = query + "b.abalph AS Bill_To_Name, b.abat1 AS Billto_Type, (c.wpar1+c.wpph1) AS Phone# , ";
                //query = query + "(d.aladd1 + d.aladd2) as Address , d. alcty1 as City, d.aladds as State, d. aladdz as Zip ";
                //query = query + "FROM busdta.F0101 AS a ";
                //query = query + "JOIN busdta.F03012 AS cs ON cs.aian8 =a.aban8 ";
                //query = query + "JOIN busdta.F0101 AS b ON a.aban81 =b.aban8 ";
                //query = query + "LEFT JOIN busdta.f0115 AS c ON a.aban8 =c.wpan8 and c.wpphtp = ' '  and c.WPIDLN=0 ";
                //query = query + "join busdta.F0116 as d on a.aban8 =d.alan8  ";
                //query = query + "where AIZON>' ' and AISTOP>' '  ";
                //  List<Customer> customersList = new List<Customer>();
                query = query + "and a.abac03 ='{0}' ) select count(1)  as total from result ";
                query = string.Format(query, route.Replace("RSR", ""));
                string total = DbHelper.ExecuteScalar(query);
                //   DataSet dsCustomersForRoute = DbHelper.ExecuteDataSet(query);
                return total;
            }
            catch (Exception ex)
            {
                logger.Error("Error in CustomerManager GetAllCustomers" + ex.Message);
                throw ex;
            }
        }               
    }
}