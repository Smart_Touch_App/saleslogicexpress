﻿using SLEWebPortal.Helpers;
using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Managers
{
    public class ReplenishmentManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<ReplenishmentHeader> GetReplenishmentList()
        {
            try
            {
                //   logger.Info("CycleCountManager GetCycleCountList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID Order by re.ReplenishmentID desc";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ReplenishmentHeader>();
            }
            catch (Exception ex)
            {
                //   logger.Error("Error in CycleCountManager GetCycleCountList" + ex.Message);
                throw ex;
            }
        }

        public List<ReplenishmentHeader> GetReplenishmentListByRoute(string id,string id2)
        {
            try
            {
                //   logger.Info("CycleCountManager GetCycleCountList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                if (id2 == "All" && id!="All")
                {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID and re.RouteId=" + id + "";
                }
                else if (id2 != "All" && id == "All")
                {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID and re.ReplenishmentTypeId=" + id2 + "";
                }
                else if (id2 == "All" && id == "All")
                {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID ";
                }
                else {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID and re.RouteId=" + id + " and re.ReplenishmentTypeId="+id2+"";
                }
               
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ReplenishmentHeader>();
            }
            catch (Exception ex)
            {
                //   logger.Error("Error in CycleCountManager GetCycleCountList" + ex.Message);
                throw ex;
            }
        }

        public List<ReplenishmentHeader> GetReplenishmentListByType(string id,string id2)
        {
            try
            {
                //   logger.Info("CycleCountManager GetCycleCountList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                if (id2 == "All" && id !="All")
                {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID and re.ReplenishmentTypeId=" + id + " ";
                }
                else if (id2 != "All" && id == "All")
                {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID and re.RouteId=" + id2 + "";
                }
                else if (id2 == "All" && id == "All")
                {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID ";
                }
                else
                {
                    query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u  where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID and re.ReplenishmentTypeId=" + id + " and re.RouteId=" + id2 + "";
                }
               
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ReplenishmentHeader>();
            }
            catch (Exception ex)
            {
                //   logger.Error("Error in CycleCountManager GetCycleCountList" + ex.Message);
                throw ex;
            }
        }

        public List<ReplenishmentHeader> GetReplenishmentListByID(int id, int routeid)
        {
            try
            {
                //   logger.Info("CycleCountManager GetCycleCountList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select re.ReplenishmentID,re.RouteId,re.StatusId,re.ReplenishmentTypeId as 'ReplenishmentTypeID',re.TransDateFrom ,re.TransDateTo ,re.RequestedBy,re.FromBranchId,re.ToBranchId,re.CreatedBy,t.TTKEY as 'ReplenishmentType',s.StatusTypeCD as 'Status',u.Name as 'RequestedByName',re.CreatedDatetime from busdta.Route_Replenishment_Header re,busdta.M5001 t, BUSDTA.Status_Type s, BUSDTA.synUserMaster u where re.StatusId=s.StatusTypeID and re.RequestedBy=u.App_user_id and re.ReplenishmentTypeId=t.TTID and re.ReplenishmentID=" + id + " and re.RouteId=" + routeid + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ReplenishmentHeader>();
            }
            catch (Exception ex)
            {
                //   logger.Error("Error in CycleCountManager GetCycleCountList" + ex.Message);
                throw ex;
            }
        }

        public List<ReplenishmentDetails> GetReplenishmentDetailListByID(int id, int typeid, int routeid, string status)
        {
            try
            {
                //   logger.Info("CycleCountManager GetCycleCountList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select TTKEY from busdta.M5001 where TTID=" + typeid + "";
                string ttkey = DbHelper.ExecuteScalar(query);
                if (ttkey.Trim() == "AddLoad")
                {
                    if (status.Trim() == "CMPLT" )
                    {

                    query= @"    select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                                , inventory.RouteId,IsNull(A.ParLevelqty,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
                                ISNULL(A.ShippedQty,0) as 'ShippedQty',
                                isnull(A.AvailaibilityAtTime,0)-isnull(A.CurrentAvailability,0)-isnull(A.SequenceNumber ,0) as 'AvailableQty'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(A.PickedQty,0) as 'ReplenishmentQty',IsNull(A.AdjustmentQty,0) as 'SuggestedQty', 
                                A.ReplenishmentQtyUM as 'ReplnUOM',IsNull(A.PickedQty,0) as 'PickedQty'
                                ,IsNull(A.DemandQty,0) AS 'DemandQty',IsNull(A.OpenReplnQty,0) AS  'OpenReplnQty'
                                , IsNull(A.AvailaibilityAtTime,0) AS 'OnHandQty',IsNull(A.CurrentAvailability,0) AS 'CommittedQty'
                                ,IsNull(A.SequenceNumber,0) AS 'HeldQty'
                                ,isnull(im.IMLNTY ,'')as 'StkType'
                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                                from busdta.Route_Replenishment_Detail A 
                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + id + "' And inventory.RouteId="+routeid+" And A.RouteId=" + routeid + @"
                                order by A.ReplenishmentDetailID asc";

//                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
//                    , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
//                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
//                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
//                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', RRD.ReplenishmentQty as 'LoadQuantity',RRD.AdjustmentQty as 'SuggestedQty'
//                    ,RRD.ReplenishmentQtyUM as 'ReplnUOM',RRD.PickedQty as 'ReplenishmentQty',RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty'
//                    ,RRD.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty'
//                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType'
//                    from busdta.Route_Replenishment_Detail RRD 
//                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
//                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
//                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
//                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
//                    order by RRD.ReplenishmentDetailID asc";

                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,IsNull(inventory.ParLevel,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(RRD.ReplenishmentQty,0) as 'LoadQuantity',IsNull(RRD.AdjustmentQty,0) as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReplnUOM',IsNull(RRD.ReplenishmentQty,0) as 'ReplenishmentQty',IsNull(RRD.PickedQty,0) as 'PickedQty',IsNull(RRD.DemandQty,0) AS 'DemandQty'
                    ,IsNull(RRD.OpenReplnQty,0) AS  'OpenReplnQty', IsNull(inventory.OnHandQuantity,0) AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }

                }
                else if (ttkey.Trim() == "Suggestion")
                {
                    if (status.Trim() == "CMPLT")
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,RRD.ParLevelqty AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', RRD.ReplenishmentQty as 'LoadQuantity',RRD.AdjustmentQty as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReplnUOM',RRD.PickedQty as 'ReplenishmentQty',RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty'
                    ,IsNull(RRD.OpenReplnQty,0) AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,IsNull(inventory.HeldQuantity,0) AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType'
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and inventory.RouteId="+routeid+" and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";

                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', RRD.ReplenishmentQty as 'LoadQuantity',RRD.AdjustmentQty as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReplnUOM',RRD.ReplenishmentQty as 'ReplenishmentQty',RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty'
                    ,RRD.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }
                
                }
                else if (ttkey.Trim() == "Sugg. Return")
                {
                  
                    if (status.Trim() == "CMPLT")
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,RRD.ParLevelqty AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',RRD.AdjustmentQty as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReturnUOM',RRD.PickedQty as 'ReturnQty',RRD.DemandQty AS 'DemandQty'
                    ,RRD.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType'
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and inventory.RouteId=" + routeid + " and  RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";

                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', RRD.ReplenishmentQty as 'ReturnQty',RRD.AdjustmentQty as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReturnUOM',RRD.ReplenishmentQty as 'ReplenishmentQty',RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty'
                    ,RRD.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }
//                    query = @" Select PD.TransactionID,PD.ItemID	AS 'Itemid', PD.PickedQty as 'ReturnQty' 
//                                ,INV.OnHandQuantity as 'OnHandQty',
//                                INV.CommittedQuantity as 'CommittedQty',INV.HeldQuantity AS 'HeldQty'
//                            ,(INV.OnHandQuantity - INV.CommittedQuantity-INV.HeldQuantity)  AS 'AvailableQty'
//                            , LTRIM(RTRIM(INV.ItemNumber)) AS 'ItemNo',isnull( im.IMDSC1,'') as 'ItemDescription'
//                            ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',RRD.OpenReplnQty AS  'OpenReplnQty'
//                            ,PD.TransactionUOM as 'ReturnUOM',RRD.DemandQty AS 'DemandQty',ISNULL(RRD.ShippedQty,0) AS 'ShippedQty'  
//                            from busdta.Pick_Detail PD 
//                            JOIN BUSDTA.Inventory INV ON PD.ItemID=INV.ItemId  AND PD.RouteId=INV.RouteId 
//                            JOIN BUSDTA.Route_Replenishment_Detail RRD ON RRD.ITEMID=PD.ItemID AND RRD.RouteId=PD.RouteId AND RRD.ReplenishmentID=PD.TransactionID 
//                            left outer join busdta.F4101 im on LTRIM(RTRIM(INV.ItemNumber)) = im.IMLITM  
//                            left outer join busdta.ItemConfiguration iconfig on INV.ItemId = iconfig.ItemID AND PD.ItemID=iconfig.ItemID 
//                            where PD.TransactionID=" + id + " AND PD.RouteId=" + routeid + " and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 AND RRD.ReplenishmentQty!=0 order by  RRD.ReplenishmentDetailID asc";
                }
                else if (ttkey.Trim() == "Unload")
                {
                    if (status.Trim() == "CMPLT")
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,RRD.ParLevelqty AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',RRD.AdjustmentQty as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReturnUOM',RRD.PickedQty as 'ReturnQty',RRD.DemandQty AS 'DemandQty'
                    ,RRD.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType'
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and inventory.RouteId=" + routeid + " and  RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";

                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', RRD.ReplenishmentQty as 'ReturnQty',RRD.AdjustmentQty as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReturnUOM',RRD.ReplenishmentQty as 'ReplenishmentQty',RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty'
                    ,RRD.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }
//                    query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
//                            , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
//                            ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
//                            , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
//                            ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', PD.PickedQty  as 'LoadQuantity'
//                            ,RRD.AdjustmentQty as 'SuggestedQty',RRD.ReplenishmentQtyUM as 'ReplnUOM',PD.PickedQty as 'ReplenishmentQty'
//                            ,RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty',RRD.OpenReplnQty AS  'OpenReplnQty'
//                            , inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty',inventory.HeldQuantity AS 'HeldQty'
//                            ,isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5'
//                            , isnull(im.IMSRP4 ,'')as 'SalesCat4' 
//                            from busdta.Route_Replenishment_Detail RRD 
//                            join BUSDTA.Pick_Detail PD ON RRD.ItemId=PD.ItemID  AND RRD.ReplenishmentID=PD.TransactionID AND RRD.ROUTEID=PD.RouteId
//                            left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
//                            left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
//                            left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
//                            where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId 
//                            and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + " order by RRD.ReplenishmentDetailID asc";
                }
                else if (ttkey.Trim() == "Held Return")
                {
                    query = @"SELECT OH.OrderDate, OD.OrderID,  RRD.ReplenishmentDetailID AS 'TransactionDetailID',
                        isnull( im.IMDSC1,'') as 'ItemDescription',
                        isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',
                        IsNull(OD.ReturnHeldQty,0) 'ReturnQty',OD.OrderUM as 'ReturnUOM',OD.ItemId,
                        LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo',
                        inventory.ItemId AS 'ItemId',inventory.RouteId,
                        IsNull(RRD.AvailaibilityAtTime,0) AS 'OnHandQty'
                        ,IsNull(RRD.CurrentAvailability,0) AS 'CommittedQty', 
                        IsNull(RRD.ParLevelqty,0) AS 'ParLevel'
                        ,IsNull(RRD.SequenceNumber,0) AS 'HeldQty',
                        isnull(RRD.AvailaibilityAtTime,0)-isnull(RRD.CurrentAvailability,0)-isnull(RRD.SequenceNumber ,0)  as 'AvailableQty'
                        
                        FROM BUSDTA.Route_Replenishment_Detail  RRD
                        JOIN busdta.Order_Detail OD ON RRD.OrderID=OD.OrderID  AND RRD.ItemId=OD.ItemId AND RRD.RouteId=od.RouteID
                        JOIN busdta.ORDER_HEADER OH on OD.OrderID=OH.OrderID AND OD.RouteId=OH.RouteId
                        join BUSDTA.Inventory inventory on inventory.ItemId=OD.ItemId
                        join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                        join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 
                        and iconfig.allowSearch=1  AND RRD.ReplenishmentID=" + id + " AND inventory.RouteId=" + routeid + " and inventory.RouteId=" + routeid + " and inventory.RouteId=" + routeid + " and  RRD.RouteId=" + routeid + "";
                }
                else
                {
                    query = "select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',ISNULL(A.ShippedQty,0) as 'ShippedQty', ISNULL(a.CurrentAvailability,0) as 'AvailableQty', isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',A.ReplenishmentDetailID, A.PickedQty as 'ReplnQuantity', A.AdjustmentQty as 'SuggestedQty', A.ReplenishmentQtyUM as 'ReplnUOM' ,A.DemandQty AS 'DemandQty',A.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty' ,inventory.HeldQuantity AS 'HeldQty' from busdta.Route_Replenishment_Detail A left join busdta.Inventory inventory on A.ItemId=inventory.ItemId left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where inventory.RouteId=a.RouteId and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID=" + id + " and A.RouteId=" + routeid + " order by A.ReplenishmentDetailID asc";
                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ReplenishmentDetails>();
            }
            catch (Exception ex)
            {
                //   logger.Error("Error in CycleCountManager GetCycleCountList" + ex.Message);
                throw ex;
            }
        }

        public List<ReplenishmentDetails> GetReplenishmentDetailListByIDForDC(int id, int typeid, int routeid, string status)
        {
            try
            {
                //   logger.Info("CycleCountManager GetCycleCountList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select TTKEY from busdta.M5001 where TTID=" + typeid + "";
                string ttkey = DbHelper.ExecuteScalar(query);
                if (ttkey.Trim() == "AddLoad")
                {
                    if (status.Trim() == "CMPLT" )
                    {

                        query = @"    select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                                , inventory.RouteId,IsNull(A.parlevelqty,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
                                ISNULL(A.ShippedQty,0) as 'ShippedQty',
                                isnull(A.AvailaibilityAtTime,0)-isnull(A.CurrentAvailability,0)-isnull(A.SequenceNumber ,0) as 'AvailableQty'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', isnull(A.PickedQty,0) as 'ReplenishmentQty',IsNull(A.AdjustmentQty,0) as 'SuggestedQty', 
                                A.ReplenishmentQtyUM as 'ReplnUOM',isnull(A.PickedQty,0) as 'PickedQty'
                                ,isnull(A.DemandQty,0) AS 'DemandQty',isnull(A.OpenReplnQty,0) AS  'OpenReplnQty'
                                , IsNull(A.AvailaibilityAtTime,0) AS 'OnHandQty',IsNull(A.CurrentAvailability,0) AS 'CommittedQty'
                                ,IsNull(A.SequenceNumber,0) AS 'HeldQty'
                                ,isnull(im.IMLNTY ,'')as 'StkType'
                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                                from busdta.Route_Replenishment_Detail A 
                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + id + "' And inventory.RouteId=" + routeid + " and  A.RouteId=" + routeid + @"
                                order by A.ReplenishmentDetailID asc";

                        //                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                        //                    , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                        //                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                        //                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                        //                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', RRD.ReplenishmentQty as 'LoadQuantity',RRD.AdjustmentQty as 'SuggestedQty'
                        //                    ,RRD.ReplenishmentQtyUM as 'ReplnUOM',RRD.PickedQty as 'ReplenishmentQty',RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty'
                        //                    ,RRD.OpenReplnQty AS  'OpenReplnQty', inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty'
                        //                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType'
                        //                    from busdta.Route_Replenishment_Detail RRD 
                        //                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                        //                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                        //                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                        //                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                        //                    order by RRD.ReplenishmentDetailID asc";

                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(RRD.ReplenishmentQty,0) as 'LoadQuantity',IsNull(RRD.AdjustmentQty,0) as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReplnUOM',IsNull(RRD.ReplenishmentQty,0) as 'ReplenishmentQty',IsNull(RRD.PickedQty,0) as 'PickedQty',IsNull(RRD.DemandQty,0) AS 'DemandQty'
                    ,IsNull(RRD.OpenReplnQty,0) AS  'OpenReplnQty', IsNull(inventory.OnHandQuantity,0) AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,IsNull(inventory.HeldQuantity,0) AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }

                }
                else if (ttkey.Trim() == "Suggestion")
                {
                    if (status.Trim() == "CMPLT" )
                    {
                        query = @"    select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                                , inventory.RouteId,A.parlevelqty AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
                                ISNULL(A.ShippedQty,0) as 'ShippedQty',
                                isnull(A.AvailaibilityAtTime,0)-isnull(A.CurrentAvailability,0)-isnull(A.SequenceNumber ,0) as 'AvailableQty'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(A.PickedQty,0) as 'ReplenishmentQty',IsNull(A.AdjustmentQty,0) as 'SuggestedQty', 
                                A.ReplenishmentQtyUM as 'ReplnUOM',IsNull(A.PickedQty,0) as 'PickedQty'
                                ,IsNull(A.DemandQty,0) AS 'DemandQty',IsNull(A.OpenReplnQty,0) AS  'OpenReplnQty'
                                , IsNull(A.AvailaibilityAtTime,0) AS 'OnHandQty',IsNull(A.CurrentAvailability,0) AS 'CommittedQty'
                                ,IsNull(A.SequenceNumber,0) AS 'HeldQty'
                                ,isnull(im.IMLNTY ,'')as 'StkType'
                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                                from busdta.Route_Replenishment_Detail A 
                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + id + "' And inventory.RouteId=" + routeid + " and  A.RouteId=" + routeid + @"
                                order by A.ReplenishmentDetailID asc";

                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(RRD.ReplenishmentQty,0) as 'LoadQuantity',IsNull(RRD.AdjustmentQty,0) as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReplnUOM',IsNull(RRD.ReplenishmentQty,0) as 'ReplenishmentQty',IsNull(RRD.PickedQty,0) as 'PickedQty',IsNull(RRD.DemandQty,0) AS 'DemandQty'
                    ,IsNull(RRD.OpenReplnQty,0) AS  'OpenReplnQty', IsNull(inventory.OnHandQuantity,0) AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,IsNull(inventory.HeldQuantity,0) AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }

                }
                else if (ttkey.Trim() == "Sugg. Return")
                {

                    if (status.Trim() == "CMPLT" || status.Trim() == "RELS")
                    {
                        query = @"    select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                                , inventory.RouteId,IsNull(A.parlevelqty,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
                                ISNULL(A.ShippedQty,0) as 'ShippedQty',
                                isnull(A.AvailaibilityAtTime,0)-isnull(A.CurrentAvailability,0)-isnull(A.SequenceNumber ,0) as 'AvailableQty'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(A.PickedQty,0) as 'ReturnQty',IsNull(A.AdjustmentQty,0) as 'SuggestedQty', 
                                A.ReplenishmentQtyUM as 'ReturnUOM',IsNull(A.PickedQty,0) as 'PickedQty'
                                ,IsNull(A.DemandQty,0) AS 'DemandQty',IsNull(A.OpenReplnQty,0) AS  'OpenReplnQty'
                                , IsNull(A.AvailaibilityAtTime,0) AS 'OnHandQty',IsNull(A.CurrentAvailability,0) AS 'CommittedQty'
                                ,IsNull(A.SequenceNumber,0) AS 'HeldQty'
                                ,isnull(im.IMLNTY ,'')as 'StkType'
                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                                from busdta.Route_Replenishment_Detail A 
                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + id + "' And inventory.RouteId=" + routeid + " and  A.RouteId=" + routeid + @"
                                order by A.ReplenishmentDetailID asc";

                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,IsNull(inventory.ParLevel,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(RRD.ReplenishmentQty,0) as 'ReturnQty',IsNull(RRD.AdjustmentQty,0) as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReturnUOM',IsNull(RRD.ReplenishmentQty,0) as 'ReplenishmentQty',IsNull(RRD.PickedQty,0) as 'PickedQty',IsNull(RRD.DemandQty,0) AS 'DemandQty'
                    ,IsNull(RRD.OpenReplnQty,0) AS  'OpenReplnQty', IsNull(inventory.OnHandQuantity,0) AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,IsNull(inventory.HeldQuantity,0) AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }
                    //                    query = @" Select PD.TransactionID,PD.ItemID	AS 'Itemid', PD.PickedQty as 'ReturnQty' 
                    //                                ,INV.OnHandQuantity as 'OnHandQty',
                    //                                INV.CommittedQuantity as 'CommittedQty',INV.HeldQuantity AS 'HeldQty'
                    //                            ,(INV.OnHandQuantity - INV.CommittedQuantity-INV.HeldQuantity)  AS 'AvailableQty'
                    //                            , LTRIM(RTRIM(INV.ItemNumber)) AS 'ItemNo',isnull( im.IMDSC1,'') as 'ItemDescription'
                    //                            ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',RRD.OpenReplnQty AS  'OpenReplnQty'
                    //                            ,PD.TransactionUOM as 'ReturnUOM',RRD.DemandQty AS 'DemandQty',ISNULL(RRD.ShippedQty,0) AS 'ShippedQty'  
                    //                            from busdta.Pick_Detail PD 
                    //                            JOIN BUSDTA.Inventory INV ON PD.ItemID=INV.ItemId  AND PD.RouteId=INV.RouteId 
                    //                            JOIN BUSDTA.Route_Replenishment_Detail RRD ON RRD.ITEMID=PD.ItemID AND RRD.RouteId=PD.RouteId AND RRD.ReplenishmentID=PD.TransactionID 
                    //                            left outer join busdta.F4101 im on LTRIM(RTRIM(INV.ItemNumber)) = im.IMLITM  
                    //                            left outer join busdta.ItemConfiguration iconfig on INV.ItemId = iconfig.ItemID AND PD.ItemID=iconfig.ItemID 
                    //                            where PD.TransactionID=" + id + " AND PD.RouteId=" + routeid + " and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 AND RRD.ReplenishmentQty!=0 order by  RRD.ReplenishmentDetailID asc";
                }
                else if (ttkey.Trim() == "Unload")
                {
                    if (status.Trim() == "CMPLT" || status.Trim() == "RELS")
                    {
                        query = @"    select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                                , inventory.RouteId,IsNull(A.parlevelqty,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
                                ISNULL(A.ShippedQty,0) as 'ShippedQty',
                                isnull(A.AvailaibilityAtTime,0)-isnull(A.CurrentAvailability,0)-isnull(A.SequenceNumber ,0) as 'AvailableQty'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(A.PickedQty,0) as 'ReturnQty',IsNull(A.AdjustmentQty,0) as 'SuggestedQty', 
                                A.ReplenishmentQtyUM as 'ReturnUOM',A.PickedQty as 'PickedQty'
                                ,IsNull(A.DemandQty,0) AS 'DemandQty',IsNull(A.OpenReplnQty,0) AS  'OpenReplnQty'
                                , IsNull(A.AvailaibilityAtTime,0) AS 'OnHandQty',IsNull(A.CurrentAvailability,0) AS 'CommittedQty'
                                ,IsNull(A.SequenceNumber,0) AS 'HeldQty'
                                ,isnull(im.IMLNTY ,'')as 'StkType'
                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                                from busdta.Route_Replenishment_Detail A 
                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + id + "' And inventory.RouteId=" + routeid + " and  A.RouteId=" + routeid + @"
                                order by A.ReplenishmentDetailID asc";
                    }
                    else
                    {
                        query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    , inventory.RouteId,IsNull(inventory.ParLevel,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(RRD.ReplenishmentQty,0) as 'ReturnQty',IsNull(RRD.AdjustmentQty,0) as 'SuggestedQty'
                    ,RRD.ReplenishmentQtyUM as 'ReturnUOM',IsNull(RRD.ReplenishmentQty,0) as 'ReplenishmentQty',IsNull(RRD.PickedQty,0) as 'PickedQty',IsNull(RRD.DemandQty,0) AS 'DemandQty'
                    ,IsNull(RRD.OpenReplnQty,0) AS  'OpenReplnQty', IsNull(inventory.OnHandQuantity,0) AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty'
                    ,inventory.HeldQuantity AS 'HeldQty',isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1'
                    , isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    from busdta.Route_Replenishment_Detail RRD 
                    left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + @" 
                    order by RRD.ReplenishmentDetailID asc";
                    }
                    //                    query = @"select RRD.ReplenishmentDetailID, inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                    //                            , inventory.RouteId,inventory.ParLevel AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription'
                    //                            ,ISNULL(RRD.ShippedQty,0) as 'ShippedQty'
                    //                            , (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                    //                            ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', PD.PickedQty  as 'LoadQuantity'
                    //                            ,RRD.AdjustmentQty as 'SuggestedQty',RRD.ReplenishmentQtyUM as 'ReplnUOM',PD.PickedQty as 'ReplenishmentQty'
                    //                            ,RRD.PickedQty as 'PickedQty',RRD.DemandQty AS 'DemandQty',RRD.OpenReplnQty AS  'OpenReplnQty'
                    //                            , inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty',inventory.HeldQuantity AS 'HeldQty'
                    //                            ,isnull(im.IMLNTY ,'')as 'StkType', isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5'
                    //                            , isnull(im.IMSRP4 ,'')as 'SalesCat4' 
                    //                            from busdta.Route_Replenishment_Detail RRD 
                    //                            join BUSDTA.Pick_Detail PD ON RRD.ItemId=PD.ItemID  AND RRD.ReplenishmentID=PD.TransactionID AND RRD.ROUTEID=PD.RouteId
                    //                            left join busdta.Inventory inventory on RRD.ItemId=inventory.ItemId 
                    //                            left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM 
                    //                            left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID  
                    //                            where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and inventory.RouteId=RRD.RouteId 
                    //                            and RRD.ReplenishmentID=" + id + " and RRD.RouteId=" + routeid + " order by RRD.ReplenishmentDetailID asc";
                }
                else if (ttkey.Trim() == "Held Return")
                {
                    if (status.Trim() == "CMPLT" || status.Trim() == "RELS")
                    {
                        query = @"    select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo'
                                , inventory.RouteId,IsNull(A.parlevelqty,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',
                                ISNULL(A.ShippedQty,0) as 'ShippedQty',
                                isnull(A.AvailaibilityAtTime,0)-isnull(A.CurrentAvailability,0)-isnull(A.SequenceNumber ,0) as 'AvailableQty'
                                ,isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM', IsNull(A.PickedQty,0) as 'ReturnQty',IsNull(A.AdjustmentQty,0) as 'SuggestedQty', 
                                A.ReplenishmentQtyUM as 'ReturnUOM',IsNull(A.PickedQty,0) as 'PickedQty'
                                ,IsNull(A.DemandQty,0) AS 'DemandQty',IsNull(A.OpenReplnQty,0) AS  'OpenReplnQty'
                                , IsNull(A.AvailaibilityAtTime,0) AS 'OnHandQty',IsNull(A.CurrentAvailability,0) AS 'CommittedQty'
                                ,IsNull(A.SequenceNumber,0) AS 'HeldQty'
                                ,isnull(im.IMLNTY ,'')as 'StkType'
                                , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                                from busdta.Route_Replenishment_Detail A 
                                left join busdta.Inventory inventory on A.ItemId=inventory.ItemId
                                left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                                left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID='" + id + "' And inventory.RouteId=" + routeid + " and  A.RouteId=" + routeid + @"
                                order by A.ReplenishmentDetailID asc";
                    }
                    else {
                        query = @"SELECT OH.OrderDate, OD.OrderID,  RRD.ReplenishmentDetailID AS 'TransactionDetailID',            
                        isnull( im.IMDSC1,'') as 'ItemDescription',
                        isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',
                        OD.ReturnHeldQty as 'ReturnQty',OD.OrderUM as 'ReturnUOM',OD.ItemId,
                        LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo',
                        inventory.ItemId AS 'ItemId',inventory.RouteId,
                        inventory.OnHandQuantity AS 'OnHandQty',inventory.CommittedQuantity AS 'CommittedQty', 
                        inventory.ParLevel AS 'ParLevel',inventory.HeldQuantity AS 'HeldQty',
                        (isnull( inventory.OnHandQuantity ,0) -( isnull(inventory.CommittedQuantity ,0)+ isnull(inventory.HeldQuantity,0))) as 'AvailableQty'
                        , isnull (im.IMSRP1,'')   AS 'SalesCat1', isnull(im.IMSRP5 ,'')as 'SalesCat5', isnull(im.IMSRP4 ,'')as 'SalesCat4'
                        FROM BUSDTA.Route_Replenishment_Detail  RRD
                        JOIN busdta.Order_Detail OD ON RRD.OrderID=OD.OrderID  AND RRD.ItemId=OD.ItemId and rrd.RouteId=OD.RouteID

                        JOIN busdta.ORDER_HEADER OH on OD.OrderID=OH.OrderID  and oh.RouteID=od.RouteID
                        join BUSDTA.Inventory inventory on inventory.ItemId=OD.ItemId and inventory.RouteId=RRD.RouteId
                        join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM
                        join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where iconfig.RouteEnabled = 1 
                        and iconfig.allowSearch=1  AND RRD.ReplenishmentID="+id+" and RRD.RouteId="+routeid+"";
                    }
                }
                else
                {
                    query = "select inventory.ItemId AS 'ItemId', LTRIM(RTRIM(inventory.ItemNumber)) AS 'ItemNo', inventory.RouteId,IsNull(inventory.ParLevel,0) AS 'ParLevel',isnull( im.IMDSC1,'') as 'ItemDescription',ISNULL(A.ShippedQty,0) as 'ShippedQty', ISNULL(a.CurrentAvailability,0) as 'AvailableQty', isnull (iconfig.PrimaryUM ,'')as 'PrimaryUOM',A.ReplenishmentDetailID, IsNull(A.PickedQty,0) as 'ReplnQuantity', IsNull(A.AdjustmentQty,0) as 'SuggestedQty', A.ReplenishmentQtyUM as 'ReplnUOM' ,IsNull(A.DemandQty,0) AS 'DemandQty',IsNull(A.OpenReplnQty,0) AS  'OpenReplnQty', IsNull(inventory.OnHandQuantity,0) AS 'OnHandQty',IsNull(inventory.CommittedQuantity,0) AS 'CommittedQty' ,IsNull(inventory.HeldQuantity,0) AS 'HeldQty' from busdta.Route_Replenishment_Detail A left join busdta.Inventory inventory on A.ItemId=inventory.ItemId left outer join busdta.F4101 im on LTRIM(RTRIM(inventory.ItemNumber)) = im.IMLITM left outer join busdta.ItemConfiguration iconfig on inventory.ItemId = iconfig.ItemID where inventory.RouteId=a.RouteId and iconfig.RouteEnabled = 1 and iconfig.allowSearch=1 and A.ReplenishmentID=" + id + " and inventory.RouteId=" + routeid + " and  A.RouteId=" + routeid + " order by A.ReplenishmentDetailID asc";
                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<ReplenishmentDetails>();
            }
            catch (Exception ex)
            {
                //   logger.Error("Error in CycleCountManager GetCycleCountList" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforReplenishmentAll()
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforReplenishmentByID(int id, int routeid)
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID and re.ReplenishmentID=" + id + " and re.RouteId=" + routeid + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforReplenishmentByRouteID(string id,string id2)
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                if (id2 == "All" && id!="All")
                {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID and re.RouteId=" + id + "";
                }
                else if (id2 != "All" && id == "All")
                {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID and re.ReplenishmentTypeId=" + id2 + "";
                }
                else if (id2 == "All" && id == "All")
                {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID ";
                }
                else
                {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID and re.RouteId=" + id + " and re.ReplenishmentTypeId="+id2+"";

                }
                
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRoutesforReplenishmentByTypeID(string id,string id2)
        {
            try
            {
                logger.Info("DSADashboardManager GetRoutesforReturnsAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //   query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                if (id2 == "All" && id!="All")
                {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID and re.ReplenishmentTypeId=" + id + "";
                }
                else if (id2 != "All" && id == "All")
                {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID and re.RouteId=" + id2 + "";
                }
                else if (id2 == "All" && id == "All")
                {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID ";
                }
                else {
                    query = "Select distinct r.RouteName,r.RouteDescription,r.RouteMasterID as 'Routeid',r.RepnlBranchType as 'StatusType' from busdta.Route_Master r,BUSDTA.Route_Replenishment_Header re where re.RouteId=r.RouteMasterID and re.ReplenishmentTypeId=" + id + " and re.RouteId="+id2+"";
                }
               
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in DSADashboardManager GetRoutesforReturnsAll" + ex.Message);
                throw ex;
            }
        }

        public bool VoidRequest(string replid, int routeid, string repltype)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager VoidRequest: " + replid);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;

                //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                query = "Update BUSDTA.Route_Replenishment_Header set StatusId=(Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='VOID'),UpdatedDatetime=GetDate() where ReplenishmentID=" + replid + " and RouteId=" + routeid + "";
                result = DbHelper.ExecuteNonQuery(query);
                if (repltype == "Unload" || repltype == "Sugg. Return")
                {
                    DataSet ds = new DataSet();
                    string Updatequery = "";
                    string Collectionquery = "Select ItemID,PickQtyPrimaryUOM,routeID from BUSDTA.Pick_Detail where TransactionID=" + replid + " and RouteId=" + routeid + "";
                    ds = DbHelper.ExecuteDataSet(Collectionquery);
                    if (ds.Tables.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            int pickqty = Convert.ToInt32(ds.Tables[0].Rows[i]["PickQtyPrimaryUOM"].ToString());
                            string itemid = ds.Tables[0].Rows[i]["ItemID"].ToString();

                            Updatequery = "Update busdta.inventory set OnHandQuantity=OnHandQuantity+" + pickqty + " where ItemId=" + itemid + " and RouteId=" + routeid + "";
                            int cnt = DbHelper.ExecuteNonQuery(Updatequery);
                        }
                    }
                }
                else
                { }
               



                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager VoidRequest" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;

        }

        public bool CompleteRequest(string replid, int routeid)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager VoidRequest: " + replid);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;

                //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                query = "Update BUSDTA.Route_Replenishment_Header set StatusId=(Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='CMPLT'),UpdatedDatetime=GetDate() where ReplenishmentID=" + replid + " and RouteId=" + routeid + "";
                result = DbHelper.ExecuteNonQuery(query);




                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager VoidRequest" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;

        }

        public bool ShipRequest(string[] list, string[] list2, int id, int routeid)
        {
            int result = -1;
            try
            {
                int i = 0;
                logger.Info("CycleCountManager ShipRequest: " + list);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;

                //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";


                foreach (string s in list)
                {
                    query = "Update BUSDTA.Route_Replenishment_Detail set ShippedQty=" + s + ",UpdatedDatetime=GetDate() where ReplenishmentDetailID=" + list2[i] + "  and routeid=" + routeid + "";
                    result = DbHelper.ExecuteNonQuery(query);
                    i++;

                }

                query = "Update BUSDTA.Route_Replenishment_Header set StatusId=(Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='RTP'),UpdatedDatetime=GetDate() where ReplenishmentID=" + id + " and RouteId=" + routeid + "";
                result = DbHelper.ExecuteNonQuery(query);






                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager ShipRequest" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;

        }

    }
}