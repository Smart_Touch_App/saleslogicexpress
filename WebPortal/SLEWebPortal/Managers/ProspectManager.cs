﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using log4net;
namespace SLEWebPortal.Managers
{
    public class ProspectManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Prospect> GetProspectsForRoute(string routeID)
        {
           
            try
            {
                logger.Info("CustomerManager GetOrdersForCustomer Parameters: " + routeID);
                logger.Info("CustomerManager GetOrdersForCustomer Parameters: " + routeID);
                logger.Info("CustomerManager GetAllProspets");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                DataSet dsProspectForRoute = DbHelper.ExecuteDataSet("Select mas.ProspectId ProspectNo,ProspectName, Adr.AddressLine1, Adr.AddressLine2, Adr.AddressLine3, Adr.AddressLine4, Adr.CityName, Adr.StateName, Adr.ZipCode, "+
                                                " pc.AreaCode1,pc.Phone1,pc.AreaCode2,pc.Phone2,pc.AreaCode3,pc.Phone3,pc.AreaCode4,pc.Phone4,pc.EmailID1,pc.EmailID2,pc.EmailID3" +
                                                " from Busdta.Prospect_Master mas join BUSDTA.Prospect_Address Adr " +
                                                " on mas.ProspectId = Adr.ProspectId inner join BUSDTA.Route_Master rm on mas.RouteId = rm.RouteMasterID " +
                                                " left join BUSDTA.Prospect_Contact pc on mas.ProspectId = pc.ProspectId " +
                                                " where pc.ProspectContactId = 1 and rm.RouteName = '" + authenticatedUser.UserName.Replace("RSR", "FBM") + "'");
                return dsProspectForRoute.GetEntityList<Prospect>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in ProspectManager GetCustomersForRoute" + ex.Message);
                throw ex;
            }
        }
         
        public string GetAllProspectsCount()
        {
            try
            {
                logger.Info("CustomerManager GetAllProspets");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];                         
                return DbHelper.ExecuteScalar("SELECT count(1) FROM busdta.Prospect_Master pm inner join BUSDTA.Route_Master rm on pm.RouteId = rm.RouteMasterID where rm.RouteName = '" + authenticatedUser.UserName.Replace("RSR", "FBM") + "'"); 
            }
            catch (Exception ex)
            {
                logger.Error("Error in ProspectManager GetAllProspets" + ex.Message);
                throw ex;
            }
        }
    }
}