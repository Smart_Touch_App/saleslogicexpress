﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SLEWebPortal.Helpers;
using System.Data;
using SLEWebPortal.Models;
using log4net;


namespace SLEWebPortal.Managers
{
    public class OrderManager
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Order> GetRecentOrders()
        {
            try
            {
                logger.Info("OrderManager GetRecentOrders");
                Models.LoginViewModel model = (Models.LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.OrderID as OrderID ";
                query = query + ",h.OrderDate as OrderDate ";
                query = query + ",h.TotalCoffeeAmt as TotalCoffee ";
                query = query + ",h.TotalAlliedAmt as TotalAllied ";
                query = query + ",h.EnergySurchargeAmt as EnergySurcharge ";
                query = query + ",h.OrderTotalAmt as OrderTotal ";
                query = query + ",h.SalesTaxAmt as SalesTaxAmount ";
                query = query + ",h.InvoiceTotalAmt as InvoiceTotal ";
                query = query + ",h.SurchargeReasonCodeId as SurchargeReasonCodeId ";
                query = query + ",m.TTKEY as OrderState ";
              //  query = query + ",s.StatusTypeDESC as OrderSubState ";
                query = query + ",h.UpdatedDatetime as Updated ";
                query = query + ",a.aban8 AS CustomerNo ";

                query = query + ", a.abalph AS Name ";
                query = query + ", a.abat1 AS Address_type ";
                query = query + ",a.abac03 AS Route ";
                query = query + ", a.abmcu AS Route_Branch  ";
                query = query + ", a.aban81 AS Bill_To  ";
                //query = query + ", h.Payment_Type AS PaymentType  ";
                //query = query + ", h.Payment_ID AS PaymentID  ";
                query = query + ", a.aban81 AS Bill_To  ";
                query = query + "from BUSDTA.F0101 a  ";
                query = query + "left outer join  ";
                query = query + "BUSDTA.Order_Header h  ";
                query = query + "on h.CustomerId = a.aban8 inner join busdta.M5001 m on m.TTID=h.OrderStateId ";
                query = query + "where ";
                query = query + "OrderDate between dateadd(day, datediff(day, 0 ,getdate())-30, 0) and getdate() and h.CreatedBy=" + model.ID + " and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') order by h.last_modified desc";
                query = query + "";
                DataSet dsRecentOrders = DbHelper.ExecuteDataSet(query);
                return dsRecentOrders.GetEntityList<Order>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetRecentOrders" + ex.Message);
                throw ex;
            }
        }



        public List<graphorder> GetYearlyCoffee()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "ROUND(sum(h.TotalCoffeeAmt),2)as graphcoffee";
                query = query + ",ROUND(sum(h.TotalAlliedAmt),2)as graphallie";
                query = query + ",convert(varchar,isnull(Round(sum(h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)),2),0)) as graphordertotal";
                query = query + " from BUSDTA.Order_Header h,busdta.M5001 m where h.CreatedBy=" + model.ID + " and h.OrderStateId=m.TTID and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and m.TTKEY='OrderDelivered' and DATEPART(yy,h.OrderDate)=DATEPART(yy,Getdate()) ";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<graphorder> orders = new List<graphorder>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<graphorder>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }

        public double GetTotalPayment()
        {
            double res;
            try
            {
                logger.Info("OrderManager GetTotalPayment");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //  query = "SELECT sum(PDPAMT) FROM BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.M03042,BUSDTA.Order_Header WHERE FFAN8=SMSLSM and SMAN8=CRCUAN8 and CRCUAN8=Customer_Id and Order_Sub_State<>'Void' and PDAN8=CRCRAN8 and FFUSER= (select route_id from BUSDTA.route_user_map where app_user_id =(select app_user_id from BUSDTA.synUserMaster where app_user = '" + model.UserName + "'))";
                //   query = "SELECT sum(PDPAMT) FROM BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.M03042 WHERE FFAN8=SMSLSM and SMAN8=CRCUAN8 and PDAN8=CRCRAN8 and FFUSER= (select route_id from BUSDTA.route_user_map where app_user_id =(select app_user_id from BUSDTA.synUserMaster where app_user = '"+model.UserName+"'))";
                query = "SELECT sum(TransactionAmount) FROM BUSDTA.Receipt_Header where RouteId = (select RouteMasterID from busdta.Route_Master where routename = (select route_id from BUSDTA.route_user_map where app_user_id =(select app_user_id from BUSDTA.synUserMaster where app_user = '" + model.UserName + "')) and Cast(ReceiptDate as date) = Cast(getdate() as date) ) and StatusId!= (select statustypeid from busdta.Status_Type where StatusTypeCD='VOID') and TransactionMode!=1 ";
                try
                {
                    res = Convert.ToDouble(DbHelper.ExecuteScalar(query));
                }
                catch
                {
                    res = 0.0;
                }
                return res;
            }
            catch (Exception ex)
            {
                //  res = 0.0;
                logger.Error("Error in OrderManager GetTotalPayment" + ex.Message);
                throw ex;
            }

        }

        public List<graphorder> GetMonthlyCoffee()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "ROUND(sum(h.TotalCoffeeAmt),2)as graphcoffee";
                query = query + ",ROUND(sum(h.TotalAlliedAmt),2)as graphallie";
                query = query + ",convert(varchar,isnull(Round(sum(h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),2),0)) as graphordertotal";
                query = query + " from BUSDTA.Order_Header h,busdta.M5001 m where h.CreatedBy=" + model.ID + " and h.OrderStateId=m.TTID and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and m.TTKEY='OrderDelivered'  and  DATEPART(mm,h.OrderDate)=DATEPART(mm,Getdate()) ";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<graphorder> orders = new List<graphorder>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<graphorder>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }

        public List<graphorder> GetTodayCoffee()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "ROUND(sum(h.TotalCoffeeAmt),2)as graphcoffee";
                query = query + ",ROUND(sum(h.TotalAlliedAmt),2)as graphallie";
                query = query + ",convert(varchar,isnull(Round(sum(h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),2),0)) as graphordertotal";
                query = query + " from BUSDTA.Order_Header h,busdta.M5001 m where h.CreatedBy=" + model.ID + "and h.OrderStateId=m.TTID and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and m.TTKEY='OrderDelivered' and DATEPART(dd,h.OrderDate)=DATEPART(dd,Getdate()) ";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<graphorder> orders = new List<graphorder>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<graphorder>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }

        //public Order GetMonthlyCoffee()
        //{
        //    try
        //    {
        //        logger.Info("GetOrderList");
        //        LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
        //        string query = string.Empty;
        //        query = "select ";
        //        query = query + "sum(h.TotalCoffee)";
        //        query = query + "from BUSDTA.Order_Header h where h.Created_By=" + model.ID + " and DATEPART(mm,OrderDate)=DATEPART(mm,Getdate()) order by OrderDate desc";
        //        string coffee = DbHelper.ExecuteScalar(query);
        //        //List<Order> orders = new List<Order>();
        //        //if (dsOrderList.HasData())
        //        //{
        //        //    orders = dsOrderList.GetEntityList<Order>();
        //        //}
        //        return coffee;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("Error in GetOrderList" + ex.Message);
        //        throw ex;
        //    }
        //}


        public List<Order> GetOrderList()
        {
            try
            {
                logger.Info("OrderManager GetOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.OrderID as OrderID ";
                query = query + ",h.CustomerId as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.OrderDate as OrderDate ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalCoffeeAmt),0)) as TotalCoffee  ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalAlliedAmt),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),0)) as OrderTotal  ";
                query = query + ",m.TTKEY as OrderState ";
               // query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f,busdta.M5001 m where h.CustomerId=f.ABAN8 And h.CreatedBy=" + model.ID + " and m.TTID=h.OrderStateId and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and h.OrderStateId!=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') order by h.last_modified desc";
                DataSet dsOrderList = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsOrderList.HasData())
                {
                    orders = dsOrderList.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
        }
        public List<Order> GetOrdersForLast30Days()
        {
            try
            {
                logger.Info("OrderManager GetOrdersForLast30Days");
                string query = string.Empty;
                query = "select * from BUSDTA.Order_Header where OrderDate between dateadd(day, datediff(day, 0 ,getdate())-30, 0) and OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and getdate() order by last_modified desc";
                DataSet dsRouteOrders = DbHelper.ExecuteDataSet(query);
                return dsRouteOrders.GetEntityList<Order>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrdersForLast30Days" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetOrderDetails(string orderID)
        {
            try
            {
                logger.Info("OrderManager GetOrderDetails Parameters: " + orderID);
                string query = string.Empty;
                query = "select ";
                query = query + "g.OrderID as OrderID ";
                query = query + ",m.IMLITM as ItemNumber ";
                //query = query + ",IMDSC1 as ItemDescription ";
                query = query + ",g.OrderQty as Quantity ";
                query = query + ",g.OrderUM as UOM ";
                query = query + ",convert(varchar,Convert(decimal(10, 2),g.UnitPriceAmt)) as UnitPrice ";
                query = query + ",convert(varchar,Convert(decimal(10, 2),g.ExtnPriceAmt)) as ExtnPrice ";
                query = query + "from BUSDTA.Order_Detail g,busdta.F4101 m ";
                query = query + "where g.ItemId=m.IMITM and ";
                query = query + "OrderId = " + orderID;
                DataSet dsOrderDetails = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsOrderDetails.HasData())
                {
                    orders = dsOrderDetails.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrderDetails" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetVoidOrderList()
        {
            try
            {
                logger.Info("OrderManager GetVoidOrderList");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.OrderID as OrderID ";
                query = query + ",h.CustomerId as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.OrderDate as OrderDate ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalCoffeeAmt),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalAlliedAmt),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),0)) as OrderTotal ";
                query = query + ",m.TTKEY as OrderState ";
              //  query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f,busdta.M5001 m where h.CustomerId=f.ABAN8 and h.OrderStateId=m.TTID and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and m.TTKEY like 'Void%' and h.CreatedBy=" + model.ID + " order by h.last_modified desc ";
                DataSet dsVoidOrderList = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsVoidOrderList.HasData())
                {
                    orders = dsVoidOrderList.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrderList" + ex.Message);
                throw ex;
            }
        }
        public List<Order> GetOrdersForToday(string orderID)
        {
            try
            {
                logger.Info("OrderManager GetOrdersForToday Parameters: " + orderID);
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select ";
                query = query + "h.OrderID as OrderID ";
                query = query + ",h.CustomerId as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.OrderDate as OrderDate ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalCoffeeAmt),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalAlliedAmt),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),0)) as OrderTotal  ";
                query = query + ",m.TTKEY as OrderState ";
              //  query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f,busdta.M5001 m where h.CustomerId=f.ABAN8 ";
                query = query + "and h.CreatedBy=" + model.ID + " and h.OrderStateId=m.TTID and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and ";
                query = query + "OrderDate = CONVERT (date, GETDATE()) order by h.last_modified desc";
                DataSet dsOrdersForToday = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsOrdersForToday.HasData())
                {
                    orders = dsOrdersForToday.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrderList" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetVoidOrders()
        {
            try
            {
                logger.Info("OrderManager GetVoidOrders");
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.OrderID as OrderID ";
                query = query + ",h.CustomerId as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.OrderDate as OrderDate ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalCoffeeAmt),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalAlliedAmt),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),0)) as OrderTotal ";
                query = query + ",m.TTKEY as OrderState ";
              //  query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f,busdta.M5001 m where h.CustomerId=f.ABAN8 And h.OrderStateId=m.TTID And h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') And h.CreatedBy=" + model.ID + " And m.TTKEY like 'Void%' order by h.last_modified desc";
                DataSet dsVoidOrders = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsVoidOrders.HasData())
                {
                    orders = dsVoidOrders.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrders" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetVoidOrdersForToday(string orderID)
        {
            try
            {
                logger.Info("OrderManager GetVoidOrdersForToday Parameters: " + orderID);
                LoginViewModel model = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;
                query = "select ";
                query = query + "h.OrderID as OrderID ";
                query = query + ",h.CustomerId as CustomerNo ";
                query = query + ",substring(f.ABALPH,1,20) as CustomerName ";
                query = query + ",h.OrderDate as OrderDate ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalCoffeeAmt),0)) as TotalCoffee ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalAlliedAmt),0)) as TotalAllied ";
                query = query + ",convert(varchar,isnull(Convert(decimal(10, 2),h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),0)) as OrderTotal ";
                query = query + ",m.TTKEY as OrderState ";
              //  query = query + ",h.Order_Sub_State as OrderSubState ";
                query = query + "from BUSDTA.Order_Header h, BUSDTA.F0101 f,busdta.M5001 m where h.CustomerId=f.ABAN8 ";
                query = query + "and h.CreatedBy=" + model.ID + " and  m.TTKEY like 'Void%' and h.OrderStateId=m.TTID and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and ";
                query = query + "OrderDate = CONVERT (date, GETDATE()) order by h.last_modified desc";
                DataSet dsVoidOrdersForToday = DbHelper.ExecuteDataSet(query);
                List<Order> orders = new List<Order>();
                if (dsVoidOrdersForToday.HasData())
                {
                    orders = dsVoidOrdersForToday.GetEntityList<Order>();
                }
                return orders;
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetVoidOrdersForToday" + ex.Message);
                throw ex;

            }
        }

        public void GetOrdersForCustomer()
        {
            try
            {
                logger.Info("OrderManager GetOrdersForCustomer");
                string query = string.Empty;
                DataSet dsCustomerOrders = DbHelper.ExecuteDataSet(query);
                if (dsCustomerOrders.HasData())
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrdersForCustomer" + ex.Message);
                throw ex;
            }
        }
        public void GetOrdersForRoute()
        {
            try
            {
                logger.Info("OrderManager GetOrdersForRoute");
                string query = string.Empty;
                DataSet dsRouteOrders = DbHelper.ExecuteDataSet(query);
                if (dsRouteOrders.HasData())
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in OrderManager GetOrdersForRoute" + ex.Message);
                throw ex;
            }
        }

        public List<Order> GetCreditOrderList()
        {
            List<Order> result;
            try
            {
                this.logger.Info("OrderManager GetOrderList");
                LoginViewModel loginViewModel = (LoginViewModel)HttpContext.Current.Session["user"];
                string text = string.Empty;
                text = "select ";
                text += "h.OrderID as OrderID ";
                text += ",h.CustomerId as CustomerNo ";
                text += ",substring(f.ABALPH,1,20) as CustomerName ";
                text += ",h.OrderDate as OrderDate ";
                text += ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalCoffeeAmt),0)) as TotalCoffee  ";
                text += ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalAlliedAmt),0)) as TotalAllied ";
                text += ",convert(varchar,isnull(Convert(decimal(10, 2),h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),0)) as OrderTotal  ";
                text += ",m.TTKEY as OrderState ";
                text = text + "from BUSDTA.Order_Header h, BUSDTA.F0101 f,busdta.M5001 m, BUSDTA.Invoice_Header i where i.OrderId = h.OrderID and i.InvoicePaymentType='Credit' and h.CustomerId=f.ABAN8 And h.CreatedBy=" + loginViewModel.ID + " and m.TTID=h.OrderStateId and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and h.OrderStateId!=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') order by h.last_modified desc";
                DataSet dataSet = DbHelper.ExecuteDataSet(text);
                List<Order> list = new List<Order>();
                if (dataSet.HasData())
                {
                    list = dataSet.GetEntityList<Order>();
                }
                result = list;
            }
            catch (Exception ex)
            {
                this.logger.Error("Error in OrderManager GetOrderList" + ex.Message);
                throw ex;
            }
            return result;
        }
        public List<Order> GetCreditOrdersForToday(string orderID)
        {
            List<Order> result;
            try
            {
                this.logger.Info("OrderManager GetOrdersForToday Parameters: " + orderID);
                LoginViewModel loginViewModel = (LoginViewModel)HttpContext.Current.Session["user"];
                string text = string.Empty;
                text = "select ";
                text += "h.OrderID as OrderID ";
                text += ",h.CustomerId as CustomerNo ";
                text += ",substring(f.ABALPH,1,20) as CustomerName ";
                text += ",h.OrderDate as OrderDate ";
                text += ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalCoffeeAmt),0)) as TotalCoffee ";
                text += ",convert(varchar,isnull(Convert(decimal(10, 2),h.TotalAlliedAmt),0)) as TotalAllied ";
                text += ",convert(varchar,isnull(Convert(decimal(10, 2),h.OrderTotalAmt+isnull(h.EnergySurchargeAmt,0)+isnull(h.SalesTaxAmt,0)),0)) as OrderTotal  ";
                text += ",m.TTKEY as OrderState ";
                text += "from BUSDTA.Order_Header h, BUSDTA.F0101 f,busdta.M5001 m, BUSDTA.Invoice_Header i  where i.OrderId = h.OrderID and i.InvoicePaymentType='Credit' and h.CustomerId=f.ABAN8 ";
                text = text + "and h.CreatedBy=" + loginViewModel.ID + " and h.OrderStateId=m.TTID and h.OrderTypeId=(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='SALORD') and ";
                text += "OrderDate = CONVERT (date, GETDATE()) order by h.last_modified desc";
                DataSet dataSet = DbHelper.ExecuteDataSet(text);
                List<Order> list = new List<Order>();
                if (dataSet.HasData())
                {
                    list = dataSet.GetEntityList<Order>();
                }
                result = list;
            }
            catch (Exception ex)
            {
                this.logger.Error("Error in OrderManager GetVoidOrderList" + ex.Message);
                throw ex;
            }
            return result;
        }
    }
}