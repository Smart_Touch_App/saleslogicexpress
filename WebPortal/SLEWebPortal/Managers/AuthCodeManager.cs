﻿using log4net;
using SLEWebPortal.Helpers;
using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SLEWebPortal.Managers
{
    public class AuthCodeManager
    {
        public readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public enum Feature
        {
            ChargeOnAccount,
            VoidReceipt,
            CashPopup
        }
        enum Key
        {
            F,
            U,
            A
        }
        Feature feature { get; set; }
        double amount { get; set; }
        string userID { get; set; }
        string featureCodeID { get; set; }
        string amountCodeID { get; set; }
        string formatCodeID { get; set; }
        public bool VerifyAuthCode(string RequestCode, string AuthCode)
        {
            bool returnValue = false;
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][Start:VerifyAuthCode]");
            try
            {
                returnValue = AuthCode.Trim().Equals(GetAuthCode(RequestCode).Trim());
            }
            catch (Exception ex)
            {
                returnValue = false;
                Logger.Error("[SLEWebPortal.Managers][AuthCodeManager][VerifyAuthCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][End:VerifyAuthCode]");
            return returnValue;
        }
        public List<ApprovalCodeDetail> GetCodeDetails()
        {
            List<ApprovalCodeDetail> codeList = new List<ApprovalCodeDetail>();
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][Start:GetCodeDetails]");
            try
            {
                string query = @"SELECT RouteName as RouteID,ApprovalCode,RequestCode ,Feature,Amount,Name as RequestedByUser
                ,abalph as RequestedForCustomer,al.CreatedDatetime FROM BUSDTA.ApprovalCodeLog al
                left outer join busdta.f0101 on RequestedForCustomer=aban8
                left outer join busdta.synUserMaster on RequestedByUser= app_user_id
                LEFT outer join busdta.Route_master on RouteID=RouteMasterID";
                DataSet dsData = new DataSet();
                dsData = DbHelper.ExecuteDataSet(query);
                if (dsData.HasData())
                {
                    codeList = dsData.GetEntityList<ApprovalCodeDetail>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("[SLEWebPortal.Managers][AuthCodeManager][GetCodeDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SLEWebPortal.Managers.SubModule][AuthCodeManager][End:GetCodeDetails]");
            return codeList;
        }
        public string GetAuthCode(string RequestCode)
        {
            string returnValue = string.Empty;
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][Start:GetAuthCode]");
            try
            {
                string resultValue = string.Empty, authCodeFormat = string.Empty, authCodeFormatID = string.Empty, formatCodeID = string.Empty;
                string amountCodeID = string.Empty, featureCodeID = string.Empty, userID = string.Empty;
                RequestAuthorizationFormat format = null;
                // Get the details of the request code format
                formatCodeID = RequestCode.Substring(0, 1);
                string query = "SELECT * FROM BUSDTA.Request_Authorization_Format where RequestFormatCode = {0}";
                query = string.Format(query, formatCodeID);
                DataSet dsData = new DataSet();
                dsData = DbHelper.ExecuteDataSet(query);
                if (dsData.HasData())
                {
                    #region Get the Codes for the feature/amount/userid from the given RequestCode
                    format = dsData.GetEntityList<RequestAuthorizationFormat>()[0];
                    switch (format.Key1.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = RequestCode.Substring(1, 2);
                            break;
                        case Key.A:
                            amountCodeID = RequestCode.Substring(1, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(1, 3);
                            break;
                    }
                    switch (format.Key2.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(3, 2) : RequestCode.Substring(4, 2);
                            break;
                        case Key.A:
                            amountCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(3, 2) : RequestCode.Substring(4, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(3, 3);
                            break;
                    }
                    switch (format.Key3.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(5, 2) : RequestCode.Substring(6, 2);
                            break;
                        case Key.A:
                            amountCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(5, 2) : RequestCode.Substring(6, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(5, 3);
                            break;
                    }
                    #endregion

                    #region Build the new AuthCode based on the AuthorizationFormatCode of the RequestCode
                    query = "SELECT AuthorizationFormatCode FROM BUSDTA.Feature_Code_Mapping where RequestCode={0}";
                    query = string.Format(query, featureCodeID);
                    featureCodeID = Convert.ToString(DbHelper.ExecuteScalar(query)).PadLeft(2, '0');

                    query = "SELECT AuthorizationFormatCode FROM BUSDTA.Amount_Range_Index where RequestCode={0}";
                    query = string.Format(query, amountCodeID);
                    amountCodeID = Convert.ToString(DbHelper.ExecuteScalar(query)).PadLeft(2, '0');
                    // Get the details of the request code format pointed by the value in AuthorizationFormatCode
                    query = "SELECT * FROM BUSDTA.Request_Authorization_Format where RequestFormatCode = {0}";
                    query = string.Format(query, format.AuthorizationFormatCode);
                    dsData = DbHelper.ExecuteDataSet(query);
                    if (dsData.HasData())
                    {
                        format = dsData.GetEntityList<RequestAuthorizationFormat>()[0];
                        returnValue = returnValue + format.AuthorizationFormatCode;
                        switch (format.Key1.Trim().ParseEnum<Key>())
                        {
                            case Key.F:
                                returnValue = returnValue + featureCodeID;
                                break;
                            case Key.A:
                                returnValue = returnValue + amountCodeID;
                                break;
                            case Key.U:
                                returnValue = returnValue + userID;
                                break;
                        }
                        switch (format.Key2.Trim().ParseEnum<Key>())
                        {
                            case Key.F:
                                returnValue = returnValue + featureCodeID;
                                break;
                            case Key.A:
                                returnValue = returnValue + amountCodeID;
                                break;
                            case Key.U:
                                returnValue = returnValue + userID;
                                break;
                        }
                        switch (format.Key3.Trim().ParseEnum<Key>())
                        {
                            case Key.F:
                                returnValue = returnValue + featureCodeID;
                                break;
                            case Key.A:
                                returnValue = returnValue + amountCodeID;
                                break;
                            case Key.U:
                                returnValue = returnValue + userID;
                                break;
                        }

                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                returnValue = string.Empty;
                Logger.Error("[SLEWebPortal.Managers][AuthCodeManager][GetAuthCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][End:GetAuthCode]");
            return returnValue;
        }
        public string GetRequestCode(Feature Feature, double Amount, string UserID)
        {
            string returnValue = string.Empty;
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][Start:GetRequestCode]");
            try
            {
                this.feature = Feature;
                this.amount = Amount;
                this.userID = UserID;
                RequestAuthorizationFormat format = null;
                formatCodeID = new Random().Next(1, 5).ToString();
                string query = "SELECT * FROM BUSDTA.Request_Authorization_Format where RequestFormatCode = {0}";
                query = string.Format(query, formatCodeID);
                DataSet dsData = new DataSet();
                dsData = DbHelper.ExecuteDataSet(query);
                if (dsData.HasData())
                {
                    format = dsData.GetEntityList<RequestAuthorizationFormat>()[0];
                }
                query = "SELECT RequestCode FROM BUSDTA.Feature_Code_Mapping where Feature='{0}'";
                query = string.Format(query, Feature.ToString());
                featureCodeID = Convert.ToString(DbHelper.ExecuteScalar(query)).PadLeft(2, '0');

                query = "SELECT * FROM BUSDTA.Amount_Range_Index where rangestart <= {0} and rangeEnd >={0}";
                query = string.Format(query, Amount);
                amountCodeID = Convert.ToString(DbHelper.ExecuteScalar(query)).PadLeft(2, '0');

                returnValue = returnValue + formatCodeID;
                switch (format.Key1.Trim().ParseEnum<Key>())
                {
                    case Key.F:
                        returnValue = returnValue + featureCodeID;
                        break;
                    case Key.A:
                        returnValue = returnValue + amountCodeID;
                        break;
                    case Key.U:
                        returnValue = returnValue + userID;
                        break;
                }
                switch (format.Key2.Trim().ParseEnum<Key>())
                {
                    case Key.F:
                        returnValue = returnValue + featureCodeID;
                        break;
                    case Key.A:
                        returnValue = returnValue + amountCodeID;
                        break;
                    case Key.U:
                        returnValue = returnValue + userID;
                        break;
                }
                switch (format.Key3.Trim().ParseEnum<Key>())
                {
                    case Key.F:
                        returnValue = returnValue + featureCodeID;
                        break;
                    case Key.A:
                        returnValue = returnValue + amountCodeID;
                        break;
                    case Key.U:
                        returnValue = returnValue + userID;
                        break;
                }
            }
            catch (Exception ex)
            {
                returnValue = string.Empty;
                Logger.Error("[SLEWebPortal.Managers][AuthCodeManager][GetRequestCode][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][End:GetRequestCode]");
            return returnValue;
        }
        public RequestCodeDetails GetRequestDetails(string RequestCode)
        {
            RequestCodeDetails details = null;
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][Start:GetRequestDetails]");
            try
            {
                string returnValue = string.Empty, queryOP = string.Empty;
                string resultValue = string.Empty, authCodeFormat = string.Empty, authCodeFormatID = string.Empty, formatCodeID = string.Empty;
                string amountCodeID = string.Empty, featureCodeID = string.Empty, userID = string.Empty;
                RequestAuthorizationFormat format = null;
                // Get the details of the request code format
                formatCodeID = RequestCode.Substring(0, 1);
                string query = "SELECT * FROM BUSDTA.Request_Authorization_Format where RequestFormatCode = {0}";
                query = string.Format(query, formatCodeID);
                DataSet dsData = new DataSet();
                dsData = DbHelper.ExecuteDataSet(query);
                if (dsData.HasData())
                {
                    #region Get the Codes for the feature/amount/userid from the given RequestCode
                    format = dsData.GetEntityList<RequestAuthorizationFormat>()[0];
                    //4 020 25  26
                    switch (format.Key1.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = RequestCode.Substring(1, 2);
                            break;
                        case Key.A:
                            amountCodeID = RequestCode.Substring(1, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(1, 3);
                            break;
                    }
                    switch (format.Key2.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(3, 2) : RequestCode.Substring(4, 2);
                            break;
                        case Key.A:
                            amountCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(3, 2) : RequestCode.Substring(4, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(3, 3);
                            break;
                    }
                    switch (format.Key3.Trim().ParseEnum<Key>())
                    {
                        case Key.F:
                            featureCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(5, 2) : RequestCode.Substring(6, 2);
                            break;
                        case Key.A:
                            amountCodeID = string.IsNullOrEmpty(userID) ? RequestCode.Substring(5, 2) : RequestCode.Substring(6, 2);
                            break;
                        case Key.U:
                            userID = RequestCode.Substring(5, 3);
                            break;
                    }
                    #endregion

                    details = new RequestCodeDetails();
                    query = "select Description from [BUSDTA].[Feature_Code_Mapping] where RequestCode={0}";
                    query = string.Format(query, featureCodeID);
                    details.Feature = DbHelper.ExecuteScalar(query);

                    query = "select  CAST(RangeStart AS VARCHAR) + '|' + CAST(RangeEnd AS VARCHAR) as Range  from [BUSDTA].[Amount_Range_Index] where RequestCode={0}";
                    query = string.Format(query, amountCodeID);
                    queryOP = DbHelper.ExecuteScalar(query);
                    details.Amount = "Between $" + queryOP.Split('|')[0] + " to $" + queryOP.Split('|')[1];

                    query = "select CAST(App_User AS VARCHAR) + '|' + CAST(Name AS VARCHAR) AS UserWithRoute from busdta.synUserMaster where App_user_id={0}";
                    query = string.Format(query, userID);
                    queryOP = DbHelper.ExecuteScalar(query);
                    details.User = queryOP.Split('|')[0];
                    details.Route = queryOP.Split('|')[1];
                }
            }
            catch (Exception ex)
            {
                details = null;
                Logger.Error("[SLEWebPortal.Managers][AuthCodeManager][GetRequestDetails][ExceptionMessage = " + ex.Message + "][ExceptionStackTrace = " + ex.StackTrace + "]");
            }
            Logger.Info("[SLEWebPortal.Managers][AuthCodeManager][End:GetRequestDetails]");
            return details;
        }
        public class RequestCodeDetails
        {
            public string Feature { get; set; }
            public string Amount { get; set; }
            public string User { get; set; }
            public string Route { get; set; }
        }
        public class RequestAuthorizationFormat
        {
            public string RequestFormatCode { get; set; }
            public string AuthorizationFormatCode { get; set; }
            public string Key1 { get; set; }
            public string Key2 { get; set; }
            public string Key3 { get; set; }
        }
    }
}