﻿using SLEWebPortal.Helpers;
using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace SLEWebPortal.Managers
{
    public class CycleCountManager
    {

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<CyclecountModels> GetCycleCountList()
        {
            try
            {
                logger.Info("CycleCountManager GetCycleCountList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetCycleCountList" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetAllRouteList()
        {
            try
            {
                logger.Info("CycleCountManager GetAllRouteList");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Cycle_Count_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and s.StatusTypeID=c.CycleCountTypeId";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetAllRouteList" + ex.Message);
                throw ex;
            }
        }

        public AddCycleCountModel GetUserDetails(int id)
        {

            try
            {
                logger.Info("CycleCountManager GetUserDetails: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select r.RouteName,r.RouteDescription,r.RouteMasterID as RouteID,u.Name as InitiatorName,'401-223-3091' as InitiatorContact,GetDate() as CycleCountDatetime from busdta.Route_Master r,busdta.synUserMaster u where u.App_user_id=" + authenticatedUser.ID + " and r.RouteMasterID=" + id + " ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntity<AddCycleCountModel>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetUserDetails" + ex.Message);
                throw ex;
            }
        }

        public AddCycleCountModel GetUserDetails()
        {

            try
            {
                logger.Info("CycleCountManager GetUserDetails");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select u.Name as InitiatorName,'401-223-4901' as InitiatorContact,GetDate() as CycleCountDatetime from busdta.synUserMaster u where u.App_user_id=" + authenticatedUser.ID + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntity<AddCycleCountModel>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetUserDetails" + ex.Message);
                throw ex;
            }
        }

        public AddCycleCountModel GetRecountDetails(int id)
        {

            try
            {
                logger.Info("CycleCountManager GetRecountDetails: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select r.RouteName,r.RouteDescription,r.RouteMasterID as RouteID,u.Name as InitiatorName,'401-223-4901' as InitiatorContact,c.CycleCountID,c.CycleCountDatetime,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType from busdta.Route_Master r,busdta.synUserMaster u,busdta.Cycle_Count_Header c,BUSDTA.Status_Type s,BUSDTA.Status_Type s2 where c.CycleCountID=" + id + " and c.RouteId=r.RouteMasterID  and c.InitiatorId=u.App_user_id and s.StatusTypeID=c.StatusId and s2.StatusTypeID=c.CycleCountTypeId";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntity<AddCycleCountModel>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetRecountDetails" + ex.Message);
                throw ex;
            }
        }

        public List<CyclecountdetailsModels> GetAllWeeklyCountItemsList(int id)
        {
            try
            {
                logger.Info("CycleCountManager GetAllWeeklyCountItemsList: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select RouteName from busdta.Route_Master where RouteMasterID=" + id + "";
                string routename = DbHelper.ExecuteScalar(query);
            //    query = "select top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM from busdta.Itemconfiguration iconfig inner join busdta.F4101 im on iconfig.itemid = im.imitm inner join busdta.F4102 f on f.IBITM=iconfig.itemid where f.IBMCU=" + branch + " and iconfig.RouteEnabled=1";
                query = "select top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM from busdta.F4101 im inner join  busdta.Itemconfiguration iconfig on iconfig.itemid = im.imitm inner join busdta.F4102 f on f.IBITM=iconfig.itemid join busdta.F56M0001 r on r.FFMCU = f.IBMCU where r.FFUSER='" + routename + "' and iconfig.RouteEnabled=1 and iconfig.AllowSearch = 1 order by im.imlitm asc";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountdetailsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetAllWeeklyCountItemsList" + ex.Message);
                throw ex;
            }
        }

        public List<CyclecountdetailsModels> GetWeeklyCountItemsListByCount(int id)
        {
            try
            {
                logger.Info("CycleCountManager GetWeeklyCountItemsListByCount: "+id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "Select BranchNumber from busdta.Route_Master where RouteMasterID=" + id + "";
                //string branch = DbHelper.ExecuteScalar(query);
                //  query = "select distinct top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM,c.CountAccepted from busdta.Itemconfiguration iconfig inner join busdta.F4101 im on iconfig.itemid = im.imitm inner join busdta.Cycle_Count_Detail c on c.ItemId=iconfig.ItemID where c.CycleCountID=" + id + "";
                query = "select distinct top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM,c.CountAccepted,c.ReCountNumber from busdta.F4101 im inner join busdta.Itemconfiguration iconfig on iconfig.itemid = im.imitm inner join busdta.Cycle_Count_Detail c on c.ItemId=iconfig.ItemID where c.CycleCountID=" + id + " and c.ReCountNumber =(select MAX(ReCountNumber) from busdta.Cycle_Count_Detail where itemId = iconfig.ItemID and CycleCountID=" + id + ") order by im.imlitm asc";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountdetailsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetWeeklyCountItemsListByCount" + ex.Message);
                throw ex;
            }
        }

        public List<CyclecountdetailsModels> GetWeeklyCountItemsListByCountForSubmit(int id)
        {
            try
            {
                logger.Info("CycleCountManager GetWeeklyCountItemsListByCountForSubmit: " + id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "(Select (Max(ReCountNumber)-1) from busdta.Cycle_Count_Detail where CycleCountID=23 )";
                string recntnumber = DbHelper.ExecuteScalar(query);
                //query = "Select BranchNumber from busdta.Route_Master where RouteMasterID=" + id + "";
                //string branch = DbHelper.ExecuteScalar(query);
                //  query = "select distinct top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM,c.CountAccepted from busdta.Itemconfiguration iconfig inner join busdta.F4101 im on iconfig.itemid = im.imitm inner join busdta.Cycle_Count_Detail c on c.ItemId=iconfig.ItemID where c.CycleCountID=" + id + "";
                //query = "select distinct top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, iconfig.PrimaryUM as UOM,c.CountAccepted,c.ReCountNumber from busdta.Itemconfiguration iconfig inner join busdta.F4101 im on iconfig.itemid = im.imitm inner join busdta.Cycle_Count_Detail c on c.ItemId=iconfig.ItemID where c.CycleCountID=" + id + " and c.ReCountNumber =(select MAX(ReCountNumber) from busdta.Cycle_Count_Detail where itemId = iconfig.ItemID and CycleCountID=" + id + ")";
                if (recntnumber == "0")
                {
                    query = "select distinct iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, iconfig.PrimaryUM as UOM,0 as JDE_Qty,c.CountAccepted, c.OnHandQty as MobileSystemQty,(inn.OnHandQuantity-c.CountedQty)as VarianceQty,c.CountedQty,0 as LastCountedQty from busdta.F4101 im inner join  busdta.Itemconfiguration iconfig on iconfig.itemid = im.imitm inner join busdta.Cycle_Count_Detail c on c.ItemId=iconfig.ItemID inner join busdta.Inventory inn on inn.itemId=c.ItemId inner join busdta.Cycle_Count_Header ch on ch.CycleCountID=c.CycleCountID where c.CycleCountID=" + id + " and ch.RouteId=inn.RouteId and c.ReCountNumber =(select MAX(ReCountNumber) from busdta.Cycle_Count_Detail where itemId = iconfig.ItemID and CycleCountID=" + id + ") order by im.imlitm asc";
                }
                else {
                    query = "select distinct iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, iconfig.PrimaryUM as UOM,0 as JDE_Qty,c.CountAccepted, c.OnHandQty as MobileSystemQty,(inn.OnHandQuantity-c.CountedQty)as VarianceQty,c.CountedQty,(Select top 1 c.CountedQty from busdta.Cycle_Count_Detail c where c.CycleCountID=" + id + " and c.ItemId=iconfig.ItemId and c.ReCountNumber=(Select case (Max(ReCountNumber)-1) when 0 then Max(ReCountNumber) else (Max(ReCountNumber)-1) end from busdta.Cycle_Count_Detail where CycleCountID=" + id + " and ItemId=iconfig.ItemId )) as LastCountedQty from busdta.F4101 im inner join busdta.Itemconfiguration iconfig on iconfig.itemid = im.imitm inner join busdta.Cycle_Count_Detail c on c.ItemId=iconfig.ItemID inner join busdta.Inventory inn on inn.itemId=c.ItemId inner join busdta.Cycle_Count_Header ch on ch.CycleCountID=c.CycleCountID where c.CycleCountID=" + id + " and ch.RouteId=inn.RouteId and c.ReCountNumber =(select MAX(ReCountNumber) from busdta.Cycle_Count_Detail where itemId = iconfig.ItemID and CycleCountID=" + id + ") order by im.imlitm asc";
                }
                
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountdetailsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetWeeklyCountItemsListByCountForSubmit" + ex.Message);
                throw ex;
            }
        }

        public int countitems(decimal id)
        {
            try
            {
                logger.Info("CycleCountManager countitems: " + id);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //query = "Select BranchNumber from busdta.Route_Master where RouteMasterID=" + id + "";
                //string branch = DbHelper.ExecuteScalar(query);
                //  query = "select distinct top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM,c.CountAccepted from busdta.Itemconfiguration iconfig inner join busdta.F4101 im on iconfig.itemid = im.imitm inner join busdta.Cycle_Count_Detail c on c.ItemId=iconfig.ItemID where c.CycleCountID=" + id + "";
                query = "select count(c.CycleCountDetailID) from busdta.Cycle_Count_Detail c where c.CycleCountID=" + id + " ";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                string countitems = DbHelper.ExecuteScalar(query);

                return Convert.ToInt16(countitems);
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager countitems" + ex.Message);
                throw ex;
            }
        }
        public List<CyclecountdetailsModels> GetAllWeeklyCountItemsListAll()
        {
            try
            {
                logger.Info("CycleCountManager GetAllWeeklyCountItemsListAll");
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
              //  query = "select top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM from busdta.Itemconfiguration iconfig inner join busdta.F4101 im on iconfig.itemid = im.imitm where iconfig.RouteEnabled=1 and iconfig.AllowSearch = 1 order by im.imlitm asc";
                query = "select top 1000 iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM from busdta.F4101 im join busdta.Itemconfiguration iconfig on iconfig.itemid = im.imitm where iconfig.RouteEnabled=1 and iconfig.AllowSearch = 1 and iconfig.Sellable = 1 order by im.imlitm asc";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountdetailsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetAllWeeklyCountItemsListAll" + ex.Message);
                throw ex;
            }
        }

        public List<CyclecountdetailsModels> GetAllWeeklyCountItemsListMultiple(string[] names)
        {
            try
            {
                logger.Info("CycleCountManager GetAllWeeklyCountItemsListMultiple: "+names);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                string itemnames = "";
                if (names.Count() > 1)
                {
                    itemnames = names[0];
                    for (int i = 1; i < names.Count(); i++)
                    {
                        itemnames = itemnames + "," + names[i];
                    }
                }
                else
                {
                    itemnames = names[0];
                }
                query = "select distinct iconfig.ItemId,im.imlitm as ItemNumber, im.IMDSC1 as ItemDescription, im.IMSRP1  as SalesCat1, im.IMSRP2 as SalesCat2,im.IMSRP5 as SalesCat5,iconfig.PrimaryUM as UOM from  busdta.F4101 im inner join busdta.Itemconfiguration iconfig on iconfig.itemid = im.imitm where iconfig.ItemId in (" + itemnames + ") order by im.imlitm asc";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountdetailsModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetAllWeeklyCountItemsListMultiple" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRouteList(string selectedValue)
        {
            try
            {
                logger.Info("CycleCountManager GetRouteList "+selectedValue);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Cycle_Count_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and r.RouteMasterID=" + selectedValue + " and s.StatusTypeID=c.CycleCountTypeId";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetRouteList" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRouteListWeekly(string selectedValue)
        {
            try
            {
                logger.Info("CycleCountManager GetRouteListWeekly "+selectedValue);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Cycle_Count_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and r.RouteMasterID=" + selectedValue + " and s.StatusTypeID=c.CycleCountTypeId and s.StatusTypeCD='WKLY'";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetRouteListWeekly" + ex.Message);
                throw ex;
            }
        }

        public List<RouteListModels> GetRouteListPhysical(string selectedValue)
        {
            try
            {
                logger.Info("CycleCountManager GetRouteListPhysical "+selectedValue);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select distinct r.RouteMasterID as Routeid,r.RouteDescription as RouteDescription,r.RouteName as RouteName,s.StatusTypeCD as StatusType from BUSDTA.Route_Master r,BUSDTA.Cycle_Count_Header c,BUSDTA.Status_Type s where r.RouteMasterID=c.RouteId and c.InitiatorId=" + authenticatedUser.ID + " and r.RouteMasterID=" + selectedValue + " and s.StatusTypeID=c.CycleCountTypeId and s.StatusTypeCD='PHYSCL'";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<RouteListModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetRouteListPhysical" + ex.Message);
                throw ex;
            }
        }

        public bool AddNewWeeklyCountHeaderAll(AddCycleCountModel model, string cycleCountDate)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager AddNewWeeklyCountHeaderAll " + model + " " + cycleCountDate);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;

                query = "Select RouteMasterID from BUSDTA.Route_Master";
                DataSet ds = DbHelper.ExecuteDataSet(query);
                DataTable dt = ds.Tables[0];

                query = "Select IsNull(Max(CycleCountID),0) from BUSDTA.Cycle_Count_Header";
                string maxheaderid = DbHelper.ExecuteScalar(query);
                int maxheader = Convert.ToInt16(maxheaderid);
                maxheader = maxheader + 1;

                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='NEW'";
                string statusid = DbHelper.ExecuteScalar(query);
                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='WKLY'";
                string statustypeid = DbHelper.ExecuteScalar(query);

                foreach (DataRow dr in dt.Rows)
                {

                   
                    
                    //query = "Select max(CycleCountID) from BUSDTA.Cycle_Count_Header";
                    //string id = DbHelper.ExecuteScalar(query);
                    query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                    string maxdetailid = DbHelper.ExecuteScalar(query);
                    int maxid = Convert.ToInt16(maxdetailid);
                    maxid = maxid + 1;
                    var onhandqty = ""; var heldqty = "";
                    int check = 0;
                    check = 0;
                    foreach (var item in model.ccd)
                    {
                        
                        
                        int? itemid=0;
                        try { 
                        query = "select IsNull(iconfig.ItemId,0) from busdta.Itemconfiguration iconfig inner join busdta.F4101 im on iconfig.itemid = im.imitm inner join busdta.F4102 f on f.IBITM=iconfig.itemid join busdta.F56M0001 r on r.FFMCU = f.IBMCU where r.FFUSER=(Select RouteName from busdta.Route_Master where RouteMasterID=" + dr["RouteMasterID"].ToString() + ") and iconfig.RouteEnabled=1 and iconfig.AllowSearch = 1 and iconfig.ItemId='" + item.ItemId + "'";
                        itemid =Convert.ToInt32( DbHelper.ExecuteScalar(query));
                            }
                        catch
                        {
                            itemid = 0;
                        }
                        if (itemid == 0)
                        {
                           // check = 0;
                        }
                        else{
                            query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId=" + item.ItemId + "";
                            ds = DbHelper.ExecuteDataSet(query);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                onhandqty = ds.Tables[0].Rows[0]["OnHandQuantity"].ToString();
                                heldqty = ds.Tables[0].Rows[0]["HeldQuantity"].ToString();
                            }
                            else
                            {
                                onhandqty = "0";
                                heldqty = "0";
                            }

                            query = "Insert into BUSDTA.Cycle_Count_Detail values( " + maxheader + "," + maxid + ",1,"+itemid+",0,0, " + onhandqty + "," + heldqty + ",0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate()) ";
                            result = DbHelper.ExecuteNonQuery(query);
                            check = 1;
                            maxid++;
                        } 
                        
                        
                        //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                       

                       
                      
                        
                    }
                    if (check == 1)
                    {
                        query = "Insert into BUSDTA.Cycle_Count_Header values (" + maxheader + "," + dr["RouteMasterID"].ToString() + "," + statusid + ",0," + statustypeid + "," + authenticatedUser.ID + ",'" + cycleCountDate + "',0,0,0,0,0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                        
                        result = DbHelper.ExecuteNonQuery(query);

                        query = "Insert into BUSDTA.Notification values(" + model.RouteID + ",'Cycle Count','Items to count : " +  model.ccd.Count + "',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='NEW'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + model.CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                        result = DbHelper.ExecuteNonQuery(query);

                    }
                    //query = "Insert into BUSDTA.Notification values(" + model.RouteID + ",'Added Cycle Count Detail for all','Added Cycle Count Detail for all',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='NEW'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + model.CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                    //result = DbHelper.ExecuteNonQuery(query);
                    maxheader++;

                }

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager AddNewWeeklyCountHeaderAll" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool AddNewWeeklyCountHeader(AddCycleCountModel model, string routeid, string cycleCountDate)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager AddNewWeeklyCountHeader: "+model+" "+routeid+" "+cycleCountDate);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select IsNull(Max(CycleCountID),0) from BUSDTA.Cycle_Count_Header";
                string maxid = DbHelper.ExecuteScalar(query);
                int mid = Convert.ToInt16(maxid);
                mid = mid + 1;
                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='NEW'";
                string statusid = DbHelper.ExecuteScalar(query);
                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='WKLY'";
                string statustypeid = DbHelper.ExecuteScalar(query);

                query = "Insert into BUSDTA.Cycle_Count_Header values (" + mid + "," + routeid + "," + statusid + ",0," + statustypeid + "," + authenticatedUser.ID + ",'" + cycleCountDate + "',0,0,0,0,0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                result = DbHelper.ExecuteNonQuery(query);

                query = "Insert into BUSDTA.Notification values(" + model.RouteID + ",'Cycle Count','Items to count : " + model.ccd.Count +"',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='NEW'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + model.CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                result = DbHelper.ExecuteNonQuery(query);
                //query = "Select max(CycleCountID) from BUSDTA.Cycle_Count_Header";
                //string id = DbHelper.ExecuteScalar(query);

                query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                string maxdetailid = DbHelper.ExecuteScalar(query);
                int maxdtid = Convert.ToInt16(maxdetailid);
                maxdtid = maxdtid + 1;
                DataSet ds;
                var onhandqty = ""; var heldqty = "";
                foreach (var item in model.ccd)
                {
                   
                    
                    
                  //  p.ClientScript.RegisterClientScriptBlock(p.GetType(), "alert", item, true /* addScriptTags */);
                    query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId="+item.ItemId+"";
                    ds = DbHelper.ExecuteDataSet(query);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        onhandqty = ds.Tables[0].Rows[0]["OnHandQuantity"].ToString();
                        heldqty = ds.Tables[0].Rows[0]["HeldQuantity"].ToString();
                    }
                    else
                    {
                        onhandqty = "0";
                        heldqty = "0";
                    }
                    //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                    query = "Insert into BUSDTA.Cycle_Count_Detail values( " + mid + "," + maxdtid + ",1," + item.ItemId + ",0,0, "+onhandqty+","+heldqty+",0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                    result = DbHelper.ExecuteNonQuery(query);
                    maxdtid++;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager AddNewWeeklyCountHeader" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool AddNewWeeklyCountDetails(AddCycleCountModel model)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager AddNewWeeklyCountDetails: "+model);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select IsNull(max(CycleCountID),0) from BUSDTA.Cycle_Count_Header";
                string id = DbHelper.ExecuteScalar(query);

                query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                string maxdetailid = DbHelper.ExecuteScalar(query);
                int maxid = Convert.ToInt16(maxdetailid);
                DataSet ds;
                var onhandqty="";var heldqty="";
                foreach (var item in model.ccd)
                {
                    query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId="+item.ItemId+"";
                    ds = DbHelper.ExecuteDataSet(query);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        onhandqty = ds.Tables[0].Rows[0]["OnHandQuantity"].ToString();
                        heldqty = ds.Tables[0].Rows[0]["HeldQuantity"].ToString();
                    }
                    else
                    {
                        onhandqty = "0";
                        heldqty = "0";
                    }
                    //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                    query = "Insert into BUSDTA.Cycle_Count_Detail values ( " + id + "," + maxid + ",1," + item.ItemId + ",0,0, "+onhandqty+","+heldqty+",0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                    maxid++;
                    result = DbHelper.ExecuteNonQuery(query);
                    query = "Insert into BUSDTA.Notification values(" + model.RouteID + ",'Added Cycle Count Detail','Added Cycle Count Detail',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='NEW'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + model.CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                    result = DbHelper.ExecuteNonQuery(query);

                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager AddNewWeeklyCountDetails" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool UpdateCountDetails(AddCycleCountModel model)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager UpdateCountDetails: "+model);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
               

                query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                string maxdetailid = DbHelper.ExecuteScalar(query);
                int maxid = Convert.ToInt16(maxdetailid);
                DataSet ds;
                var onhandqty = ""; var heldqty = "";
                foreach (var item in model.ccd)
                {
                    query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId=" + item.ItemId + "";
                    ds = DbHelper.ExecuteDataSet(query);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        onhandqty = ds.Tables[0].Rows[0]["OnHandQuantity"].ToString();
                        heldqty = ds.Tables[0].Rows[0]["HeldQuantity"].ToString();
                    }
                    else
                    {
                        onhandqty = "0";
                        heldqty = "0";
                    }
                    //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                    query = "Insert into BUSDTA.Cycle_Count_Detail values ( " + model.CycleCountID + "," + maxid + ",1," + item.ItemId + ",0,0, " + onhandqty + "," + heldqty + ",0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                    maxid++;
                    result = DbHelper.ExecuteNonQuery(query);
                    //query = "Insert into BUSDTA.Notification values(" + model.RouteID + ",'Added Cycle Count Detail','Added Cycle Count Detail',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='NEW'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + model.CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                    //result = DbHelper.ExecuteNonQuery(query);

                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager UpdateCountDetails" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool AddNewWeeklyCountHeaderAllPhysical(AddCycleCountModel model, string cycleCountDate)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager AddNewWeeklyCountHeaderAllPhysical: "+model+" "+cycleCountDate);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;

                query = "Select RouteMasterID from BUSDTA.Route_Master";
                DataSet ds = DbHelper.ExecuteDataSet(query);
                DataTable dt = ds.Tables[0];

                query = "Select IsNull(Max(CycleCountID),0) from BUSDTA.Cycle_Count_Header";
                string maxheaderid = DbHelper.ExecuteScalar(query);
                int maxheader = Convert.ToInt16(maxheaderid);
                maxheader = maxheader + 1;

                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='NEW'";
                string statusid = DbHelper.ExecuteScalar(query);
                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='PHYSCL'";
                string statustypeid = DbHelper.ExecuteScalar(query);

                foreach (DataRow dr in dt.Rows)
                {

                    query = "Insert into BUSDTA.Cycle_Count_Header values (" + maxheader + "," + dr["RouteMasterID"].ToString() + "," + statusid + ",0," + statustypeid + "," + authenticatedUser.ID + ",'" + cycleCountDate + "',0,0,0,0,0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                    //    var connection = new SqlConnection(_connString);
                    //    var command = new SqlCommand(query, connection);
                    //    var dependency = new SqlDependency(command);
                    //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                    result = DbHelper.ExecuteNonQuery(query);
                    //query = "Select max(CycleCountID) from BUSDTA.Cycle_Count_Header";
                    //string id = DbHelper.ExecuteScalar(query);
                    ////////////////////detail shld not be sent//////////////
                    //query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                    //string maxdetailid = DbHelper.ExecuteScalar(query);
                    //int maxid = Convert.ToInt16(maxdetailid);
                    //maxid = maxid + 1;
                    //foreach (var item in model.ccd)
                    //{

                        //query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId=" + item.ItemId + "";
                        //ds = DbHelper.ExecuteDataSet(query);
                        //var onhandqty = ds.Tables[0].Columns["OnHandQuantity"].ToString();
                        //var heldqty = ds.Tables[0].Columns["HeldQuantity"].ToString();
                        //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                        /////////////////////detail shld not be sent //////////////////
                      //  query = "Insert into BUSDTA.Cycle_Count_Detail Select " + maxheader + "," + maxid + ",1," + item.ItemId + ",0,0, OnHandQuantity,HeldQuantity,0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate() from BUSDTA.Inventory where ItemId=" + item.ItemId + " and RouteId=" + dr["RouteMasterID"].ToString() + "";
                      //  result = DbHelper.ExecuteNonQuery(query);
                    //    maxid++;
                    //}
                    maxheader++;

                }

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager AddNewWeeklyCountHeaderAllPhysical: " + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool AddNewWeeklyCountHeaderPhysical(AddCycleCountModel model, string routeid, string cycleCountDate)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager AddNewWeeklyCountHeaderPhysical: "+model+" "+routeid+" "+cycleCountDate);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select IsNull(Max(CycleCountID),0) from BUSDTA.Cycle_Count_Header";
                string maxid = DbHelper.ExecuteScalar(query);
                int mid = Convert.ToInt16(maxid);
                mid = mid + 1;
                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='NEW'";
                string statusid = DbHelper.ExecuteScalar(query);
                query = "Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='PHYSCL'";
                string statustypeid = DbHelper.ExecuteScalar(query);

                query = "Insert into BUSDTA.Cycle_Count_Header values (" + mid + "," + routeid + "," + statusid + ",0," + statustypeid + "," + authenticatedUser.ID + ",'" + cycleCountDate + "',0,0,0,0,0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                result = DbHelper.ExecuteNonQuery(query);
                //query = "Select max(CycleCountID) from BUSDTA.Cycle_Count_Header";
                //string id = DbHelper.ExecuteScalar(query);

                //query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                //string maxdetailid = DbHelper.ExecuteScalar(query);
                //int maxdtid = Convert.ToInt16(maxdetailid);
                //maxdtid = maxdtid + 1;

                //foreach (var item in model.ccd)
                //{
                    //query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId=" + item.ItemId + "";
                    //ds = DbHelper.ExecuteDataSet(query);
                    //var onhandqty = ds.Tables[0].Columns["OnHandQuantity"].ToString();
                    //var heldqty = ds.Tables[0].Columns["HeldQuantity"].ToString();
                    //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                    ///////////////detail shld not be sent///////////
                    //query = "Insert into BUSDTA.Cycle_Count_Detail Select " + mid + "," + maxdtid + ",1," + item.ItemId + ",0,0, OnHandQuantity,HeldQuantity,0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate() from BUSDTA.Inventory where ItemId=" + item.ItemId + " and RouteId=" + routeid + "";
                    //result = DbHelper.ExecuteNonQuery(query);
                    //maxdtid++;
                //}




            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager AddNewWeeklyCountHeaderPhysical" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public bool AddNewWeeklyCountDetailsPhysical(AddCycleCountModel model)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager AddNewWeeklyCountDetailsPhysical: "+model);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "Select IsNull(max(CycleCountID),0) from BUSDTA.Cycle_Count_Header";
                string id = DbHelper.ExecuteScalar(query);
                ////////////////detail shld not be sent///////////////
                //query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                //string maxdetailid = DbHelper.ExecuteScalar(query);
                //int maxid = Convert.ToInt16(maxdetailid);
                foreach (var item in model.ccd)
                {
                    //query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId="+item.ItemId+"";
                    //DataSet ds = DbHelper.ExecuteDataSet(query);
                    //var onhandqty = ds.Tables[0].Columns["OnHandQuantity"].ToString();
                    //var heldqty = ds.Tables[0].Columns["HeldQuantity"].ToString();
                    //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";

                    //////////////detail shld not be sent/////////////////
                    //query = "Insert into BUSDTA.Cycle_Count_Detail values ( Select " + id + "," + maxid + ",1," + item.ItemId + ",0,0, OnHandQuantity,HeldQuantity,0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate() from BUSDTA.Inventory where ItemId=" + item.ItemId + ")";
                    //maxid++;
                    //result = DbHelper.ExecuteNonQuery(query);
                }
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager AddNewWeeklyCountDetailsPhysical" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;
        }

        public List<CyclecountModels> GetCycleCountListByRoute(string selectedvalue)
        {
            try
            {
                logger.Info("CycleCountManager GetCycleCountListByRoute: "+selectedvalue);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //     query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,u.Contact as InitiatorContact,s.StatusTypeDESC as Status,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount from BUSDTA.Status_Type s,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd,BUSDTA.Route_Master r where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.InitiatorId="+authenticatedUser.ID+" and c.RouteId=r.RouteMasterID and r.RouteMasterID="+selectedvalue+"  group by c.CycleCountID,c.Routeid,u.Name,u.Contact,s.StatusTypeDESC,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime";
                query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-223-3091' as InitiatorContact,s.StatusTypeDESC as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber=(Select c.ReCountNumber from BUSDTA.Cycle_Count_Detail c where c.CountAccepted=1 group by c.ReCountNumber) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber<>(Select c.ReCountNumber from BUSDTA.Cycle_Count_Detail c where c.CountAccepted=1 group by c.ReCountNumber) and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " and c.Routeid=" + selectedvalue + " group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeDESC,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetCycleCountListByRoute" + ex.Message);
                throw ex;
            }
        }

        public List<CyclecountModels> GetCycleCountListByRouteWeekly(string selectedvalue)
        {
            try
            {
                logger.Info("CycleCountManager GetCycleCountListByRouteWeekly: "+selectedvalue);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //     query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,u.Contact as InitiatorContact,s.StatusTypeDESC as Status,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount from BUSDTA.Status_Type s,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd,BUSDTA.Route_Master r where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.InitiatorId="+authenticatedUser.ID+" and c.RouteId=r.RouteMasterID and r.RouteMasterID="+selectedvalue+"  group by c.CycleCountID,c.Routeid,u.Name,u.Contact,s.StatusTypeDESC,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime";
                query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " and c.Routeid=" + selectedvalue + " and s2.StatusTypeCD='WKLY' group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetCycleCountListByRouteWeekly" + ex.Message);
                throw ex;
            }
        }

        public List<CyclecountModels> GetCycleCountListByRoutePhysical(string selectedvalue)
        {
            try
            {
                logger.Info("CycleCountManager GetCycleCountListByRoutePhysical: "+selectedvalue);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                //     query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,u.Contact as InitiatorContact,s.StatusTypeDESC as Status,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount from BUSDTA.Status_Type s,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd,BUSDTA.Route_Master r where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.InitiatorId="+authenticatedUser.ID+" and c.RouteId=r.RouteMasterID and r.RouteMasterID="+selectedvalue+"  group by c.CycleCountID,c.Routeid,u.Name,u.Contact,s.StatusTypeDESC,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime";
                query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-220-1231' as InitiatorContact,s.StatusTypeCD as Status,s2.StatusTypeCD as StatusType,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,COALESCE((Select Count(ccd.CycleCountID) from BUSDTA.Cycle_Count_Detail ccd where ccd.CycleCountID=c.CycleCountID), 0) as ItemsToCount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.ReCountNumber in (Select Max(c1.ReCountNumber) from BUSDTA.Cycle_Count_Detail c1 where c1.CycleCountID=c.CycleCountID ) and cd.CycleCountID=c.CycleCountID) as recountitemstotaltorecount,(Select Count(cd.CycleCountDetailID) as ItemsCounted from BUSDTA.Cycle_Count_Detail cd where cd.CountAccepted=1 and cd.CycleCountID=c.CycleCountID) as recountitemstotalrecounted from BUSDTA.Status_Type s,BUSDTA.Status_Type s2,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c,busdta.Cycle_Count_Detail ccd where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.CycleCountTypeId=s2.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " and c.Routeid=" + selectedvalue + " and s2.StatusTypeCD='PHYSCL' group by c.CycleCountID,c.Routeid,u.Name,s.StatusTypeCD,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime,s2.StatusTypeCD";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetCycleCountListByRoutePhysical" + ex.Message);
                throw ex;
            }
        }

        public List<CyclecountModels> GetCycleCountListByCountID(int ID)
        {
            try
            {
                logger.Info("CycleCountManager GetCycleCountListByCountID: "+ID);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                query = "select c.CycleCountID,c.Routeid,u.Name as InitiatorName,'401-223-3091' as InitiatorContact,s.StatusTypeDESC as Status,c.CycleCountTypeid,c.Initiatorid,c.ReasonCodeid,c.CycleCountDatetime from BUSDTA.Status_Type s,BUSDTA.Route_Master r,BUSDTA.synUserMaster u,BUSDTA.Cycle_Count_Header c where c.Initiatorid=u.App_user_id and c.Statusid=s.StatusTypeID and c.InitiatorId=" + authenticatedUser.ID + " and c.CycleCountID=" + ID + "";
                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                DataSet dsDevices = DbHelper.ExecuteDataSet(query);

                return dsDevices.GetEntityList<CyclecountModels>();
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager GetCycleCountListByCountID" + ex.Message);
                throw ex;
            }
        }

        public bool UpdateRecount(string[] itemids, decimal countid)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager UpdateRecount: "+itemids+ " "+countid);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;
                string itemnames = "";
                if (itemids.Count() > 1)
                {
                    itemnames = itemids[0];
                    for (int i = 1; i < itemids.Count(); i++)
                    {
                        itemnames = itemnames + "," + itemids[i];
                    }
                }
                else
                {
                    itemnames = itemids[0];
                }


                query = "Update BUSDTA.Cycle_Count_Detail set CountAccepted=1 where CycleCountID=" + countid + " and ItemId not in (" + itemnames + ") and ReCountNumber=(Select Max(ReCountNumber) from busdta.Cycle_Count_Detail where CycleCountID=" + countid + ") ";
                result = DbHelper.ExecuteNonQuery(query);

                query = "Update BUSDTA.Cycle_Count_Detail set ItemStatus=0 where CycleCountID=" + countid + " and ItemId in (" + itemnames + ") and ReCountNumber=(Select Max(ReCountNumber) from busdta.Cycle_Count_Detail where CycleCountID=" + countid + ") ";
                result = DbHelper.ExecuteNonQuery(query);

                query = "Select IsNull(Max(CycleCountDetailID),0) from BUSDTA.Cycle_Count_Detail";
                string maxdetailid = DbHelper.ExecuteScalar(query);
                int maxid = Convert.ToInt16(maxdetailid);
                maxid = maxid + 1;


                var onhandqty = ""; var heldqty = "";
                var pickedqty = ""; var pickdetailid = "";
                foreach (var item in itemids)
                {
                    query = "Select Max(ReCountNumber) as MaxRecountNumber from BUSDTA.Cycle_Count_Detail where ItemId=" + item + " and CycleCountID=" + countid + "";


                    DataSet ds = DbHelper.ExecuteDataSet(query);
                    var CountNumber = ds.Tables[0].Rows[0]["MaxRecountNumber"].ToString();
                    int ReCountNumber = Convert.ToInt16(CountNumber);
                    ReCountNumber = ReCountNumber + 1;

                    query = "Select OnHandQuantity,HeldQuantity from BUSDTA.Inventory where ItemId=" + item + "";
                    ds = DbHelper.ExecuteDataSet(query);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        onhandqty = ds.Tables[0].Rows[0]["OnHandQuantity"].ToString();
                        heldqty = ds.Tables[0].Rows[0]["HeldQuantity"].ToString();
                    }
                    else
                    {
                        onhandqty = "0";
                        heldqty = "0";
                    }

                    //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                    query = "Insert into BUSDTA.Cycle_Count_Detail values(" + countid + "," + maxid + "," + ReCountNumber + "," + item + ",0,0, "+onhandqty+","+heldqty+",0," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate() )";
                    result = DbHelper.ExecuteNonQuery(query);
                    maxid++;

                   // query = "Select p.PickDetailID,p.PickedQty from busdta.Pick_Detail p where p.TransactionID=" + countid + " and ItemID="+item+" and RouteId=(Select RouteId from busdta.Cycle_Count_Header where CycleCountID="+countid+")";
                   
                    query = "update busdta.Pick_Detail set AdjustedQty=(Select p.PickedQty from busdta.Pick_Detail p where p.TransactionID=" + countid + " and ItemID=" + item + " and RouteId=(Select RouteId from busdta.Cycle_Count_Header where CycleCountID=" + countid + ")),PickedQty=0 where TransactionID=" + countid + " and ItemID=" + item + " and RouteId=(Select RouteId from busdta.Cycle_Count_Header where CycleCountID=" + countid + ")";
                    result = DbHelper.ExecuteNonQuery(query);
                }
                query = "Update BUSDTA.Cycle_Count_Header set StatusId=(Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='RECNT'),UpdatedDatetime=GetDate(),IsCountInitiated=0 where CycleCountID=" + countid + "";
                result = DbHelper.ExecuteNonQuery(query);

               

                System.Collections.Generic.List<CyclecountModels> mod = GetCycleCountListByCountID(Convert.ToInt16(countid));

                query = "Insert into BUSDTA.Notification values(" + mod[0].Routeid + ",'Cycle Recount','Cycle count recounted: " + mod[0].CycleCountID + "',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='RECNT'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + mod[0].CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                result = DbHelper.ExecuteNonQuery(query);


                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager UpdateRecount" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;

        }

        public bool ReleaseRequest(string countid)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager ReleaseRequest "+countid);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;

                //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                query = "Update BUSDTA.Cycle_Count_Header set StatusId=(Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='RELS'),UpdatedDatetime=GetDate() where CycleCountID=" + countid + "";
                result = DbHelper.ExecuteNonQuery(query);

                System.Collections.Generic.List<CyclecountModels> mod = GetCycleCountListByCountID(Convert.ToInt16(countid));

                query = "Insert into BUSDTA.Notification values(" + mod[0].Routeid + ",'Cycle Count Release','Cycle count released: " + mod[0].CycleCountID + "',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='RELS'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + mod[0].CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                result = DbHelper.ExecuteNonQuery(query);

                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager ReleaseRequest" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;

        }
        public bool VoidRequest(string countid, string reasonid)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager VoidRequest: "+countid+ " "+reasonid);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];

                string query = string.Empty;

                //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                query = "Update BUSDTA.Cycle_Count_Header set ReasonCodeId =" + reasonid + ",StatusId=(Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='VOID'),UpdatedDatetime=GetDate() where CycleCountID=" + countid + "";
                result = DbHelper.ExecuteNonQuery(query);

                System.Collections.Generic.List<CyclecountModels> mod = GetCycleCountListByCountID(Convert.ToInt16(countid));

                query = "Insert into BUSDTA.Notification values(" + mod[0].Routeid + ",'Cycle Count Void','Cycle count Void: " + mod[0].CycleCountID + "',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='VOID'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + mod[0].CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                result = DbHelper.ExecuteNonQuery(query);

                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager VoidRequest" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;

        }

        public bool AcceptRequest(string countid,string routeid)
        {
            int result = -1;
            try
            {

                logger.Info("CycleCountManager AcceptRequest: "+countid);
                LoginViewModel authenticatedUser = (LoginViewModel)HttpContext.Current.Session["user"];
                string query = string.Empty;

                query = "Select c.ItemId,c.CountedQty from busdta.Cycle_Count_Detail c where c.CycleCountID="+countid+"";
                DataSet ds = DbHelper.ExecuteDataSet(query);
                DataTable dt = ds.Tables[0];
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach ( DataRow dr in dt.Rows)
                    {
                        query = "Update busdta.Inventory set OnHandQuantity=" + dr["CountedQty"].ToString() + " where ItemId=" + dr["ItemId"].ToString() + " and RouteId="+routeid+"";
                        int i = DbHelper.ExecuteNonQuery(query);
                    }
                }

                //query = "Insert into BUSDTA.Cycle_Count_Detail values ("+id+",1,"+item.ItemId+",0,"+onhandqty+","+heldqty+",0,"+authenticatedUser.ID+",GetDate(),"+authenticatedUser.ID+",GetDate(),0)";
                query = "Update BUSDTA.Cycle_Count_Header set StatusId=(Select StatusTypeID from BUSDTA.Status_Type where StatusTypeCD='ACCEPT'),UpdatedDatetime=GetDate(),IsCountInitiated=0 where CycleCountID=" + countid + "";
                result = DbHelper.ExecuteNonQuery(query);

                System.Collections.Generic.List<CyclecountModels> mod = GetCycleCountListByCountID(Convert.ToInt16(countid));

                query = "Insert into BUSDTA.Notification values(" + mod[0].Routeid + ",'Cycle Count Accept','Cycle count accepted: " + mod[0].CycleCountID + "',(Select StatusTypeID from busdta.Status_Type where StatusTypeCD='ACCEPT'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),(Select TTID from busdta.M5001 where TTKEY='WeeklyCycleCount'),'N','" + mod[0].CycleCountDatetime + "','N'," + authenticatedUser.ID + "," + authenticatedUser.ID + ",GetDate()," + authenticatedUser.ID + ",GetDate(),GetDate())";
                result = DbHelper.ExecuteNonQuery(query);

                //    var connection = new SqlConnection(_connString);
                //    var command = new SqlCommand(query, connection);
                //    var dependency = new SqlDependency(command);
                //   dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager AcceptRequest" + ex.Message);
                throw ex;
            }
            return result > 0 ? true : false;

        }
        public bool DeleteItemsInDetails(string countid)
        {
            bool returnValue = false;
            try
            {
                logger.Info("CycleCountManager DeleteItemsInDetails: " + countid);
                string query = string.Empty;
                query = "Delete from busdta.Cycle_Count_Detail where CycleCountID="+countid+"";
               
                returnValue = Convert.ToBoolean(DbHelper.ExecuteNonQuery(query));
                returnValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager DeleteItemsInDetails" + ex.Message);
                returnValue = false;
            }
            return returnValue;
        }
        public bool InitiateFullPhysicalCycleCount()
        {
            bool returnValue = false;
            try
            {
                logger.Info("CycleCountManager InitiateFullPhysicalCycleCount");
                string query = string.Empty;
                query = "INSERT INTO 'BUSDTA'.'Cycle_Count_Header' ('CycleCountID','RouteId','StatusId','IsCountInitiated','CycleCountTypeId','InitiatorId','CycleCountDatetime','ReasonCodeId','JDEQty','JDEUM','JDECycleCountNum','JDECycleCountDocType','CreatedBy','CreatedDatetime','UpdatedBy','UpdatedDatetime') VALUES";
                query = query + "(17,4,11,'1',16,14,'2015-08-24 12:29:18.597',0,0,'0 ',0,0,14,'2015-08-24 12:29:18.597',14,'2015-08-24 12:29:18.597') ";
                //returnValue = Convert.ToBoolean(DbHelper.ExecuteNonQuery(query));
                returnValue = true;
            }
            catch (Exception ex)
            {
                logger.Error("Error in CycleCountManager InitiateFullPhysicalCycleCount" + ex.Message);
                returnValue = false;
            }
            return returnValue;
        }

    }
}