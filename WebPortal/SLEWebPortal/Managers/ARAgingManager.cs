﻿using SLEWebPortal.Helpers;
using SLEWebPortal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace SLEWebPortal.Managers
{
    public class ARAgingManager
    {
        #region Variable and obect declaration 

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion 

        #region Methods

        /// <summary>
        /// Get list of aging summary for all routes
        /// </summary>
        /// <returns>List of aging records for each route</returns>
        public IList<RouteAging> GetAgingDetails()
        {
            logger.Info("ARAgingManager GetAgingDetails");
            IList<RouteAging> objAgingDetails = new List<RouteAging>();
            StringBuilder objQueryBuilder = new StringBuilder();

            try
            {
                objQueryBuilder.Append(" \nSELECT RouteId, RouteName, Age, COUNT(TotalCustomer) AS TotalCustomer,");
                objQueryBuilder.Append(" \nSUM(CustomersOnHold) AS CustomersOnHold, SUM(TotalAmount) AS TotalAmount");
                objQueryBuilder.Append(" \nFROM (Select rm.RouteName AS RouteId, rm.RouteDescription AS RouteName, cl.CustomerId AS TotalCustomer,");
                objQueryBuilder.Append(" \nCOUNT(CASE cm.AIEXHD WHEN 'Y' THEN cm.AIEXHD ELSE NULL END) AS CustomersOnHold,");
                objQueryBuilder.Append(" \nsum(isnull(cl.InvoiceOpenAmt,0)) - sum(isnull(cl.ConcessionAmt,0)) - sum(isnull(cl.receiptappliedamt,0)) AS TotalAmount,");
                objQueryBuilder.Append(" \n(case");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) <= 30 then '30 Days'");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 30 and DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=60 then '60 Days'");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 60 and DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=90 then '90 Days'");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 90 then '90+ Days'");
                objQueryBuilder.Append(" \nend) AS Age");
                objQueryBuilder.Append(" \nfrom busdta.Customer_Ledger  cl join busdta.F03012 cm on cl.CustomerId=cm.aian8");
                objQueryBuilder.Append(" \njoin busdta.F0101 cma on cm.aian8 = cma.ABAN8");
                objQueryBuilder.Append(" \njoin busdta.Invoice_Header ih on cl.InvoiceId=ih.InvoiceID AND cl.ROuteId=ih.RouteId");
                objQueryBuilder.Append(" \njoin busdta.Order_Header oh on oh.OrderId=ih.Orderid AND oh.ROuteId=ih.RouteId");
                objQueryBuilder.Append(" \njoin BUSDTA.Route_Master rm on cl.RouteId=rm.RouteMasterID");
                objQueryBuilder.Append(" \nwhere cl.IsActive=1 and ih.ARStatusID not in (select statustypeid from busdta.Status_Type where statustypecd IN ('VOID', 'CLSD'))");
                objQueryBuilder.Append(" \nand ih.DeviceStatusID not in (select statustypeid from busdta.Status_Type where statustypecd in ('VOID','CLSD'))");
                objQueryBuilder.Append(" \nAND oh.OrderTypeId <> (SELECT ISNULL(StatusTypeId,0) FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD')");
                objQueryBuilder.Append(" \ngroup by  rm.RouteName, rm.RouteDescription, cl.CustomerId, cm.AIEXHD, (case");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) <= 30 then '30 Days'");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 30 and DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=60 then '60 Days'");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 60 and DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=90 then '90 Days'");
                objQueryBuilder.Append(" \nwhen DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 90 then '90+ Days' end) ) A");
                objQueryBuilder.Append(" \nWHERE TotalAmount>0");
                objQueryBuilder.Append(" \nGROUP BY RouteId, RouteName, Age order by 1, 3 DESC ");


                DataSet objAgingData = DbHelper.ExecuteDataSet(objQueryBuilder.ToString());

                if (objAgingData.HasData())
                {
                    objAgingDetails = (from row in objAgingData.Tables[0].AsEnumerable()
                                       group row by new { RouteId = row.Field<string>("RouteId"), RouteName = row.Field<string>("RouteName"), CustomersOnHold = row.Field<int>("CustomersOnHold") } into grp
                                       select new RouteAging
                                       {
                                           RouteId = grp.Key.RouteId,
                                           RouteName = grp.Key.RouteName,
                                           CustomersOnHold = grp.Key.CustomersOnHold,
                                           TotalAmount = grp.Sum(r => r.Field<Decimal>("TotalAmount"))
                                       }).ToList<RouteAging>();

                    foreach (RouteAging item in objAgingDetails)
                    {
                        item.RouteARAgingDetails = (from o in objAgingData.Tables[0].AsEnumerable()
                                                    where o.Field<string>("RouteId").Equals(item.RouteId)
                                                    select new RouteAgingDetail
                                                    {
                                                        RouteId = item.RouteId,
                                                        AgeCategory = o.Field<string>("Age"),
                                                        TotalCustomer = o.Field<int>("TotalCustomer"),
                                                        TotalAmount = o.Field<decimal>("TotalAmount")
                                                    }).ToList();
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in ARAgingManager GetAgingDetails" + ex.ToString());
                throw;
            }

            return objAgingDetails;
        }

        /// <summary>
        /// Retrives route aging with customer wise aging details 
        /// </summary>
        /// <returns>List of aging records for each route and age bracket</returns>
        public RouteAging GetAgingDetailsByRouteAndAge(string routeId, string ageCategory)
        {
            logger.Info("ARAgingManager GetAgingDetailsByRouteAndAge");
            RouteAging objRouteAging = null;
            StringBuilder objQueryBuilder = new StringBuilder();

            try
            {
                objQueryBuilder.Append(" \nSELECT cl.CustomerId AS CustomerId, cma.abalph AS CustomerName,");
                objQueryBuilder.Append(" \nLTRIM(RTRIM(pn.pnptd)) +' ('+ cm.AITRAR + ')' AS PaymentTerm,");
                objQueryBuilder.Append(" \nCOUNT(cl.InvoiceId) AS OpenInvoiceCount, SUM(cl.InvoiceGrossAmt) AS OpenInvoiceGrossAmt,");
                objQueryBuilder.Append(" \nSUM(ISNULL(cl.InvoiceOpenAmt,0)) - SUM(ISNULL(cl.ConcessionAmt,0)) - SUM(ISNULL(cl.receiptappliedamt,0)) AS OpenInvoiceOpenAmt,");
                objQueryBuilder.Append(" \n(CASE");
                objQueryBuilder.Append(" \n\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) <= 30 THEN '30 Days'");
                objQueryBuilder.Append(" \n\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 30 AND DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=60 THEN '60 Days'");
                objQueryBuilder.Append(" \n\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 60 AND DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=90 THEN '90 Days'");
                objQueryBuilder.Append(" \n\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 90 THEN '90+ Days'");
                objQueryBuilder.Append(" \nEND) AS Age");
                objQueryBuilder.Append(" \nFROM BUSDTA.Customer_Ledger cl");
                objQueryBuilder.Append(" \nINNER JOIN BUSDTA.F03012 cm ON cl.CustomerId=cm.aian8");
                objQueryBuilder.Append(" \nINNER JOIN BUSDTA.F0101 cma ON cm.aian8 = cma.ABAN8");
                objQueryBuilder.Append(" \nINNER JOIN BUSDTA.f0014 pn ON cm.AITRAR= pn.pnptc");
                objQueryBuilder.Append(" \nINNER JOIN BUSDTA.Invoice_Header ih ON cl.InvoiceId=ih.InvoiceID AND cl.RouteId=ih.RouteId");
                objQueryBuilder.Append(" \nINNER JOIN BUSDTA.Order_Header oh ON oh.OrderId=ih.OrderId AND oh.RouteId=ih.RouteId");
                objQueryBuilder.Append(" \nWHERE cl.IsActive=1");
                objQueryBuilder.Append(" \nAND ih.ARStatusID NOT IN (SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD IN ('VOID','CLSD'))");
                objQueryBuilder.Append(" \nAND ih.DeviceStatusID NOT IN (SELECT StatusTypeID FROM BUSDTA.Status_Type WHERE StatusTypeCD IN ('VOID','CLSD'))");
                objQueryBuilder.Append(" \nAND cl.RouteId=(SELECT RouteMasterID FROM BUSDTA.Route_Master WHERE RouteName='"+routeId+"')");
                objQueryBuilder.Append(" \nAND oh.OrderTypeId <>(SELECT ISNULL(StatusTypeID,0) FROM BUSDTA.Status_Type WHERE StatusTypeCD='RETORD')");
                objQueryBuilder.Append(" \nAND (CASE");
                objQueryBuilder.Append(" \n\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) <= 30 THEN '30 Days'");
                objQueryBuilder.Append(" \n\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 30 AND DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=60 THEN '60 Days'");
                objQueryBuilder.Append(" \n\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 60 AND DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=90 THEN '90 Days'");
                objQueryBuilder.Append(" \n\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 90 THEN '90+ Days' END) ='" +ageCategory + "'");
                objQueryBuilder.Append(" \n\t\tGROUP BY cl.CustomerId,cma.abalph,cm.AITRAR,(CASE");
                objQueryBuilder.Append(" \n\t\t\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) <= 30 THEN '30 Days'");
                objQueryBuilder.Append(" \n\t\t\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 30 AND DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=60 THEN '60 Days'");
                objQueryBuilder.Append(" \n\t\t\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 60 AND DATEDIFF(day,ih.DeviceInvoiceDate,Getdate())<=90 THEN '90 Days'");
                objQueryBuilder.Append(" \n\t\t\t\tWHEN DATEDIFF(day,ih.DeviceInvoiceDate,Getdate()) > 90 THEN '90+ Days'");
                objQueryBuilder.Append(" \n\t\t\t\tEND), pn.pnptd");
                objQueryBuilder.Append(" \n\t\tHAVING (SUM(ISNULL(cl.InvoiceOpenAmt,0)) - SUM(ISNULL(cl.ConcessionAmt,0)) - SUM(ISNULL(cl.receiptappliedamt,0))) >0");
                objQueryBuilder.Append(" \nORDER BY Age, OpenInvoiceOpenAmt DESC");
                
                DataSet objAgingData = DbHelper.ExecuteDataSet(objQueryBuilder.ToString());

                if (objAgingData.HasData())
                {
                    objRouteAging = new RouteAging();
                    objRouteAging.RouteAgingByCustomersList = (from row in objAgingData.Tables[0].AsEnumerable()
                                                               select new RouteAgingByCustomers
                                                               {
                                                                   CustomerNo = row["CustomerId"].ToString(),
                                                                   CustomerName = row.Field<string>("CustomerName"),
                                                                   PaymentTerm = row.Field<string>("PaymentTerm"),
                                                                   OpenInvoices = row.Field<int>("OpenInvoiceCount"),
                                                                   OriginalAmount = row.Field<decimal>("OpenInvoiceGrossAmt"),
                                                                   OpenAmount = row.Field<decimal>("OpenInvoiceOpenAmt")
                                                               }).ToList<RouteAgingByCustomers>();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in ARAgingManager GetAgingDetailsByRouteAndAge" + ex.ToString());
                throw;
            }

            return objRouteAging;
        }

        /// <summary>
        /// Retrives unapplied receipts by routes
        /// </summary>
        /// <returns>List of aging records for each route and age bracket</returns>
        public List<RouteUnappliedReceipt> GetUnappliedReceiptsByRoute()
        {
            logger.Info("ARAgingManager GetUnappliedReceiptsByRoute");
            List<RouteUnappliedReceipt> routeAging = new List<RouteUnappliedReceipt>();
            StringBuilder objQueryBuilder = new StringBuilder();

            try
            {
                objQueryBuilder.Append("SELECT RM.RouteName AS RouteId, RouteDescription AS RouteName, COUNT(CL.CustomerId) AS TotalCustomers, SUM(ISNULL(CL.receiptunappliedamt,0)) - SUM(ISNULL(CL.receiptappliedamt,0)) AS UnappliedAmount");
                objQueryBuilder.Append(" \nFROM BUSDTA.Customer_Ledger CL");
                objQueryBuilder.Append(" \nINNER JOIN busdta.Receipt_Header RH ON cl.Receiptid=RH.Receiptid AND cl.RouteId=RH.RouteId");
                objQueryBuilder.Append(" \nINNER JOIN BUSDTA.Route_Master RM ON CL.RouteId=RM.RouteMasterID");
                objQueryBuilder.Append(" \nWHERE cl.isactive='1' GROUP BY RouteName, RouteDescription");
                objQueryBuilder.Append(" \nHAVING SUM(ISNULL(CL.receiptunappliedamt,0)) - SUM(ISNULL(CL.receiptappliedamt,0))<>0");

                DataSet objAgingData = DbHelper.ExecuteDataSet(objQueryBuilder.ToString());

                if (objAgingData.HasData())
                {
                    foreach (DataRow row in objAgingData.Tables[0].Rows)
                    {
                        routeAging.Add(new RouteUnappliedReceipt
                            {
                               RouteId= Convert.ToString(row["RouteId"]),
                               RouteName = Convert.ToString(row["RouteName"]),
                               TotalCustomers = Convert.ToInt16(row["TotalCustomers"]),
                               UnappliedAmt = (-1)*Convert.ToDecimal(row["UnappliedAmount"])
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error in ARAgingManager GetUnappliedReceiptsByRoute" + ex.ToString());
                throw;
            }

            return routeAging;
        }

        #endregion 

    }
}