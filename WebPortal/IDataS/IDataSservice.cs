﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace IDataService
{
    [ServiceContract]
    public interface IAppService
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "IsServerReachable")]
        HealthResponse IsServerAvailable();

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "NewDataAvailable/{remoteID}")]
        DataResponse NewDataAvailable(string remoteID);
    }
    [DataContract]
    public class DataResponse
    {
        [DataMember(Name = "IsServerReachable")]
        public bool IsServerReachable
        { get; set; }
        [DataMember(Name = "RemoteID")]
        public string RemoteID
        { get; set; }
        [DataMember(Name = "Payload")]
        public List<string> Payload
        { get; set; }
    }
    [DataContract]
    public class HealthResponse
    {
        [DataMember(Name = "IsServerReachable")]
        public bool IsServerReachable
        { get; set; }
    }
}
