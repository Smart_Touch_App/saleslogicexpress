
#include "stdafx.h"
#include <msclr\marshal.h>
#include "VertexWrapper.h"
#include "VstInvoice.h"
//#include <atlstr.h>
//
//#define  VSTDEMOTITLE   "Vstdemo"
//#define  VSTDEMODEBUG   "vstdebug.txt"
//#define CastToChar (char*)Marshal::StringToHGlobalAnsi
//using namespace System::Runtime::InteropServices;
using namespace VertexWrapper;
using namespace System::Runtime::InteropServices;
using namespace msclr::interop;
String^ VertexApi::SetInvoice(String^ pCompCode,
	String^ pCustomer,
	String^ pCustomerClassCode,
	double pGeoCode,
	String^ pInvoiceNumber,
	String^ pProductSetCode,
	int pItemNumberCount,
	array<String^>^ pItemNumber,
	array<String^>^ pProductCode,
	array<String^>^ pComponentCode,
	array<int>^     pProductQty,
	array<double>^ pExtendedAmount,
	bool *ReturnStatus
	)
{
	bool bRetValue = true;
	String^ opStatus;
	opStatus = "SetInvoice Successful.";

	Console::WriteLine("\n\n***************** Start Set Invoice ***************n\n");

	Console::WriteLine("\n pCompCode		  : {0} ", pCompCode);
	Console::WriteLine("\n pCustomer          : {0} ", pCustomer);
	Console::WriteLine("\n pCustomerClassCode : {0} ", pCustomerClassCode);
	Console::WriteLine("\n pGeoCode           : {0} ", pGeoCode);
	Console::WriteLine("\n pInvoiceNumber     : {0} ", pInvoiceNumber);
	Console::WriteLine("\n pProductSetCode    : {0} ", pProductSetCode);
	for (int index = 0; index < pItemNumberCount; index++)
	{
		Console::WriteLine("\n item index      : {0}", index);
		Console::WriteLine("\n pItemNumber     : {0} ", pItemNumber[index]);
		Console::WriteLine("\n pProductCode    : {0} ", pProductCode[index]);
		Console::WriteLine("\n pProductQty     : {0} ", pProductQty[index]);
		Console::WriteLine("\n pExtendedAmount : {0} ", pExtendedAmount[index]);

	}



	double           lComponentQty;
	tVstTaxblty      lCustTaxability;
	char            *lInvoiceNum;
	tLocGeoCode      lJurisGeocode;
	tVstJurisType    lJurisIdx;
	BOOL             lJurisInCity;
	long             lLineItemIdx;
	long             lLineItemNum;
	double           lProdQty;
	tVstTaxblty      lProdTaxability;
	tVstRegister     lRegWriteInd;
	tVstTaxingJuris  lTaxingJuris;
	double           lTransExtdAmt;
	tVstTaxedGeo     lTransTaxedGeoFlag;
	char			*CustCode;
	char			*CustClassCode;
	char			*CompCode;
	char			*ProductSetCode;
	char			*ProductCode;
	char			*ComponentCode;
	/* Set invoice-level jurisdiction information. */
	int index = 0, maxIndex = 0;
	index = eVstJurisTypeFirstElem;
	maxIndex = eVstJurisTypeNumElems;



	for (index;
		index < maxIndex;
		index++)
	{
		lJurisGeocode = pGeoCode;
		lJurisInCity = TRUE;
		lJurisIdx = (tVstJurisType)index;
		if (VstSetInvJuris(lDataHdl,               /* Tax information handle  */
			lJurisIdx,							   /* Jurisdiction identifier */
			eVstJurisCdGeo,						   /* Rate access method      */
			&lJurisGeocode,						   /* GeoCode                 */
			NULL,								   /* State abbreviation      */
			NULL,								   /* Zip code                */
			NULL,								   /* City name               */
			NULL,								   /* County name             */
			&lJurisInCity) == FALSE)			   /* Within city limits      */
		{
			opStatus = "Can't set invoice-level jurisdiction information.";
			bRetValue = false;
		}
	}

	/* Set invoice-level detail information. */

	lInvoiceNum = (char*)(Marshal::StringToHGlobalAnsi(pInvoiceNumber)).ToPointer();
	printf_s("\n lInvoiceNum : %s", lInvoiceNum);

	CustCode = (char*)(Marshal::StringToHGlobalAnsi(pCustomer)).ToPointer();
	printf_s("\n CustCode : %s", CustCode);

	CustClassCode = (char*)(Marshal::StringToHGlobalAnsi(pCustomerClassCode)).ToPointer();
	printf_s("\n CustClassCode : %s", CustClassCode);

	CompCode = (char*)(Marshal::StringToHGlobalAnsi(pCompCode)).ToPointer();
	printf_s("\n CompCode : %s", CompCode);

	ProductSetCode = (char*)(Marshal::StringToHGlobalAnsi(pProductSetCode)).ToPointer();
	printf_s("\n ProductSetCode : %s", ProductSetCode);

	/* Set customer taxability to be determined by the TDM. */
	lCustTaxability = eVstTaxbltyTDM;

	/* Set register write indicator to write or not write to the register table.
	* Defining VSTDEMOWRITETOREG will cause the sample transaction results
	* to be written to the register table.
	*/

#ifdef VSTDEMOWRITETOREG
	lRegWriteInd = eVstRegisterWrite;
#else
	lRegWriteInd = eVstRegisterDoNotWrite;
#endif

	if (VstSetInvDetail(lDataHdl,                /* Tax information handle   */
		NULL,                    /* Invoice date             */
		NULL,                    /* Invoice gross amount     */
		lInvoiceNum,             /* Invoice number           */
		NULL,                    /* Invoice control number   */
		NULL,                    /* Invoice total tax        */
		CustCode,                /* Customer code            */
		CustClassCode,           /* Customer class code      */
		&lCustTaxability,        /* Customer taxability flag */
		CompCode,                /* Company code             */
		NULL,                    /* Division code            */
		NULL,                    /* Store code               */
		NULL,                    /* G/L account number       */
		&lRegWriteInd) == FALSE) /* Register write indicator */
	{
		opStatus = "Can't set invoice-level detail information.";
		bRetValue = false;

	}

	for (lLineItemIdx = 0; lLineItemIdx < pItemNumberCount; lLineItemIdx++)
	{
		/* The line item index starts at 0, but we want the actual line item
		* numbers to start at 1, for this example.
		*/
		lLineItemNum = lLineItemIdx + 1;

		/* Set line item jurisdiction information.  For these sample transactions,
		* we will simply set the line item jurisdiction information to match the
		* invoice-level jurisdiction information.
		*/
		int innerIndex = 0, innerMaxIndex = 0;
		innerIndex = eVstJurisTypeFirstElem;
		innerMaxIndex = eVstJurisTypeNumElems;

		for (innerIndex;
			innerIndex < innerMaxIndex;
			innerIndex++)
		{
			lJurisGeocode = pGeoCode;
			lJurisInCity = TRUE;
			lJurisIdx = (tVstJurisType)innerIndex;

			if (VstSetLineJuris(lDataHdl,               /* Tax information handle  */
				lLineItemNum,           /* Line item number        */
				lJurisIdx,              /* Jurisdiction identifier */
				eVstJurisCdGeo,         /* Rate access method      */
				&lJurisGeocode,          /* GeoCode                 */
				NULL,                   /* State abbreviation      */
				NULL,                   /* Zip code                */
				NULL,                   /* City name               */
				NULL,                   /* County name             */
				&lJurisInCity) == FALSE) /* Within city limits      */
			{
				opStatus = "Can't set line item jurisdiction information.";
				bRetValue = false;

			}
		}

		/* Set line item detail information. */
		lTransTaxedGeoFlag = eVstTaxedGeoDetermine;
		lTransExtdAmt = pExtendedAmount[lLineItemIdx];
		Console::WriteLine("\n lTransExtdAmt on index : {0} = {1}", index, lTransExtdAmt);

		lTaxingJuris = eVstTaxingJurisPrimary;

		if (VstSetLineDetail(lDataHdl,              /* Tax information handle              */
			lLineItemNum,          /* Line item number                    */
			eVstTransTypeSale,     /* Transaction type                    */
			eVstTransSubTypeNone,  /* Transaction sub-type                */
			eVstTransCdNormal,     /* Transaction code                    */
			NULL,                  /* Transaction date                    */
			&lTransTaxedGeoFlag,    /* Taxing jurisdiction indicator       */
			&lTransExtdAmt,         /* Transaction extended amount         */
			NULL,                  /* Transaction total tax               */
			NULL,                  /* Transaction combined rate           */
			NULL,                  /* User data buffer area               */
			&lTaxingJuris,          /* Taxing Jurisdiction associated with */
			/*   customer exemption certificate    */
			NULL,                  /* Customer exemption certificate      */
			NULL,                  /* Division code                       */
			NULL,                  /* Store code                          */
			NULL) == FALSE)        /* G/Ledger account number             */
		{
			opStatus = "Can't set line item detail information.";
			bRetValue = false;

		}

		/* Set line item product information. */
		lProdTaxability = eVstTaxbltyTDM;
		lProdQty = pProductQty[lLineItemIdx];
		Console::WriteLine("\n lProdQty on index : {0} = {1}", index, lProdQty);

		lComponentQty = pProductQty[lLineItemIdx];
		Console::WriteLine("\n lComponentQty on index : {0} = {1}", index, lComponentQty);

		ProductCode = (char*)(Marshal::StringToHGlobalAnsi(pProductCode[lLineItemIdx])).ToPointer();
		printf_s("\n ProductCode : %s", ProductCode);

		ComponentCode = (char*)(Marshal::StringToHGlobalAnsi(pComponentCode[lLineItemIdx])).ToPointer();
		printf_s("\n ComponentCode : %s", ComponentCode);

		if (VstSetLineProd(lDataHdl,                            /* Tax information handle    */
			lLineItemNum,                        /* Line item number          */
			ProductSetCode,						/* Product set code          */
			&lProdQty,                            /* Product quantity          */
			ProductCode,						 /* Product code              */
			NULL,                                /* Reserved parameter        */
			&lProdTaxability,                     /* Product taxability flag   */
			NULL,                                /* Product reporting code    */
			&lComponentQty,                       /* Component/Transaction qty */
			ComponentCode,								 /* Component code            */
			NULL) == FALSE)                      /* Reserved parameter        */
		{
			opStatus = "Can't set line item product information.";
			bRetValue = false;

		}
	}
	Console::WriteLine("\n\n***************** End Set Invoice ***************\n");
	*ReturnStatus = bRetValue;
	return opStatus;

}

String^ VertexApi::Initialize(bool *ReturnStatus, String^ DataSource){
	bool bRetValue = true;
	String^ opStatus;
	opStatus = "Initialization Successful.";
	DATASOURCE = (char*)(Marshal::StringToHGlobalAnsi(DataSource)).ToPointer();

	lConnHdl = VstCreateConnHdl();
	lDataHdl = VstCreateHdl();

	if ((lConnHdl = VstCreateConnHdl()) != NULL &&
		(lDataHdl = VstCreateHdl()) != NULL)
	{
		if (VstOpenDb(lConnHdl, eVstDbTypeLocation, DATASOURCE, SERVER, USER,
			PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			opStatus = "Error While Opening Location database";
			bRetValue = false;

		}
		if (VstOpenDb(lConnHdl, eVstDbTypeRate, DATASOURCE, SERVER, USER,
			PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			opStatus = "Couldn't open the Rate database.";
			bRetValue = false;

		}

#ifdef VSTDEMOWRITETOREG
		if (VstOpenDb(lConnHdl, eVstDbTypeRegister, DATASOURCE, SERVER, USER,
			PASSWORD, eVstAccessModeReadWrite, NULL, NULL) != eVstDbRetCdNone)
		{
			opStatus = "Couldn't open the Register database for writing.";
			bRetValue = false;

		}
#else
		if (VstOpenDb(lConnHdl, eVstDbTypeRegister, DATASOURCE, SERVER, USER,
			PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			opStatus = "Couldn't open the Register database.";
			bRetValue = false;

		}
#endif

		if (VstOpenDb(lConnHdl, eVstDbTypeTDM, DATASOURCE, SERVER, USER,
			PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			opStatus = "Couldn't open the TDM database.";
			bRetValue = false;

		}

	}
	else
	{
		bRetValue = false;

	}


	*ReturnStatus = bRetValue;
	return opStatus;

}

String^ VertexApi::Cleanup(bool *ReturnStatus)
{
	bool bRetValue = true;
	String^ opStatus;
	opStatus = "Cleanup Successful.";

	/* Close all the database connections. */

	if (VstCloseDb(lConnHdl, eVstDbTypeLocation, NULL, NULL) != eVstDbRetCdNone ||
		VstCloseDb(lConnHdl, eVstDbTypeRate, NULL, NULL) != eVstDbRetCdNone ||
		VstCloseDb(lConnHdl, eVstDbTypeRegister, NULL, NULL) != eVstDbRetCdNone ||
		VstCloseDb(lConnHdl, eVstDbTypeTDM, NULL, NULL) != eVstDbRetCdNone)
	{
		/* Print a warning that an error occurred while closing the databases.
		* Alternately, we could have handled each possible error condition
		* for each of the databases to produce a more detailed message.
		*/
		opStatus = "Error encountered while closing the databases.";
		bRetValue = false;
	}

	/* Release the connection handle. */

	if (VstReleaseConnHdl(lConnHdl) != TRUE)
	{
		opStatus = "Couldn't release connection handle.";
		bRetValue = false;
	}
	*ReturnStatus = bRetValue;
	return opStatus;
}


String^ VertexApi::GetTax(String^ pInvoiceNumber, double* opInvoiceTax, array<double>^ opItemTax, double* opGrossAmt, bool *ReturnStatus)
{
	Console::WriteLine("\n***************** Start GetTax ***************");
	String^ opStatus;
	bool bRetValue = true;
	if (ComputeTax())
	{
		tVstTaxedGeo     lAddtlTaxedGeoFlag;
		char             lCompntCd[VST_COMPONENT_CD_SIZ];
		double           lCompntQty;
		tVstFirstOrNext  lFirstOrNext;
		double           lInvGrossAmt;
		char             lInvNo[VST_INV_NO_SIZ];
		long             lInvNumLineItems;
		double           lInvTotalTax;
		char             lJurisCity[VST_CITY_SIZ];
		tVstJurisType    lJurisIdx;
		tLocGeoCode      lJurisGeo;
		tVstJurisRetCd   lJurisReturnCd;
		char             lJurisState[VST_STATE_CD_SIZ];
		tVstLocAddtlType lLocAddtlIdx;
		tVstDiAppl       lLocalDiApply;
		double           lLocalExmtAmt;
		char             lLocalExmtReasCd[VST_EXEMPT_REA_SIZ];
		double           lLocalNonTaxAmt;
		char             lLocalNonTaxReasCd[VST_NON_TAX_REA_SIZ];
		double           lLocalRate;
		char             lLocalRtEffDate[VST_DATE_SIZ];
		double           lLocalTax;
		tVstTaxblty      lLocalTaxability;
		double           lLocalTaxableAmt;
		tVstTaxIncl      lLocalTaxIncl;
		tVstTaxType      lLocalTaxType;
		char             lProdCd[VST_PROD_CD_SIZ];
		double           lProdQty;
		char             lProdSetCd[VST_PROD_CD_SIZ];
		tVstStLocType    lStLocIdx;
		double           lTransExtdAmt;
		tVstTaxedGeo     lTransTaxedGeoFlag;
		int				 index = 0;
		long             lLineItemNum;        /* Current line item number          */
		double           lTotalExtdAmt = 0.0; /* Total extd amt for all line items */
		double           lTotalTax = 0.0; /* Total tax for all line items      */
		double           lTransTtlTax = 0.0; /* Total tax for current line item   */

		if (VstGetInvDetail(lDataHdl,         /* Tax information handle   */
			NULL,             /* Invoice date             */
			&lInvGrossAmt,     /* Invoice gross amount     */
			lInvNo,           /* Invoice number           */
			NULL,             /* Invoice control number   */
			&lInvNumLineItems, /* Number of line items     */
			&lInvTotalTax,     /* Invoice total tax        */
			NULL,             /* Customer code            */
			NULL,             /* Customer class code      */
			NULL,             /* Customer taxability flag */
			NULL,             /* Company code             */
			NULL,             /* Division code            */
			NULL,             /* Store code               */
			NULL,             /* G/L account number       */
			NULL) == FALSE)   /* Register file indicator  */
		{
			opStatus = "Failed to retrieve invoice detail information.";
			bRetValue = false;
		}

		pInvoiceNumber = marshal_as<String^>(lInvNo);
		Console::WriteLine("pInvoiceNumber : {0}", pInvoiceNumber);

		*opInvoiceTax = lInvTotalTax;
		printf_s("\n opInvoiceTax : %s", &opInvoiceTax);

		*opGrossAmt = lInvGrossAmt;
		printf_s("\n opGrossAmt : %s", &opGrossAmt);


		/* Retrieve the first invoice line item number. */
		lFirstOrNext = eVstFirst;

		while (VstGetLineItemNumber(lDataHdl, lFirstOrNext, &lLineItemNum) == TRUE)
		{

			/* Retrieve line item detail information. */
			if (VstGetLineDetail(lDataHdl,       /* Tax information handle    */
				lLineItemNum,   /* Line item number          */
				NULL,           /* Transaction type          */
				NULL,           /* Transaction sub-type      */
				NULL,           /* Transaction code          */
				NULL,           /* Transaction date          */
				NULL,           /* Taxing juris indicator    */
				&lTransExtdAmt,  /* Transaction extended amt  */
				&lTransTtlTax,   /* Transaction total tax     */
				NULL,           /* Transaction combined rate */
				NULL,           /* User area data buffer     */
				NULL,           /* Transaction status code   */
				NULL,           /* Exempt cert. taxing juris */
				NULL,           /* Exemption cert. number    */
				NULL,           /* Division code             */
				NULL,           /* Store code                */
				NULL) == FALSE) /* G/L account number        */
			{
				opStatus = "Failed to retrieve line item detail information.";
			}
			opItemTax[lLineItemNum - 1] = lTransTtlTax;
			lTotalExtdAmt += lTransExtdAmt;
			/* Set lFirstOrNext to retrieve next line item number. */
			lFirstOrNext = eVstNext;

		}
		if (lInvGrossAmt != (lTotalExtdAmt + lInvTotalTax)){
			Console::WriteLine("\n Gross amount and sum of extnd amt + total tax are not same\n");
		}
	}
	else
	{
		bRetValue = false;
	}

	Console::WriteLine("\n***************** End GetTax ***************\n");
	*ReturnStatus = bRetValue;
	return opStatus;
}

//bool VertexApi::ComputeTax(String^ opStatus)
//{
//	bool bRetValue = true;
//#ifdef VSTDEMODEBUG
//
//	/* Write out data handle information to a debug text file. */
//
//	if (VstDebugHdl(lDataHdl, "vstdebug.txt", "Before VstCalcTax") == FALSE)
//	{
//		opStatus= "VstDebugHdl function failed prior to tax calculation.";
//		
//		bRetValue = false;
//	}
//
//#endif
//
//	/* Perform tax calculations. */
//
//	lCalcRetCd = VstCalcTax(lConnHdl, lDataHdl, NULL, NULL);
//
//	/* Evaluate the calculation return code. */
//
//	switch (lCalcRetCd)
//	{
//	case eVstCalcRetCdNone:
//	case eVstCalcRetCdTdmMtDefault:
//		/* Success. */
//		break;
//
//	default:
//		opStatus ="Tax calculation routine failed.";
//		bRetValue = false;
//		break;
//	}
//
//#ifdef VSTDEMODEBUG
//
//	/* Write out data handle information to a debug text file. */
//
//	if (VstDebugHdl(lDataHdl, "vstdebug.txt", "After VstCalcTax") == FALSE)
//	{
//		opStatus = "VstDebugHdl function failed after tax calculation.";
//	}
//
//#endif
//}

