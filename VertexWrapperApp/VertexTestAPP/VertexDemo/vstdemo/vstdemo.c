/* -------------------------------------------------------------------------
 Copyright 1995-2010 Vertex Inc. 
  
 VSTDEMO.C: Vertex Sales Tax Q Series 4.0 Calculation API Demo Program

 DESCRIPTION:
	This sample program demonstrates basic use of the Vertex Sales Tax Q 
	Series (STQ) 4.0 API functions.  The transaction and jurisdiction
	information used below were selected to highlight new features
	introduced in STQ 2.0.
	
 VERSION: 4.0
---------------------------------------------------------------------------- */



#include <stdio.h>
#include <stdlib.h>

#include "port.h"  /* Resolves porting issues between operating systems. */
#include "vst.h"   /* Sales and Use Tax API datatypes and prototypes.    */
#include "msg.h"   /* Sales and Use Tax messaging header.                */
#include "admn.h"

/* Define connection parameters.  Note that it is possible to set individual
 * connection parameters for each of the four databases.  You may need to
 * modify the parameters below to suit your database environment
 DATASOURCE = "D:\E DRIVE DATA\Abhi_Workspace\BitBucket\TechUnison_Vertex_STQ\Vertex40Database_Jan2015\isamDBR40Mig"
 */

#define  DATASOURCE     "C:\\vertexDB\\"
#define  SERVER         NULL
#define  USER           NULL
#define  PASSWORD       NULL

/* Define the title of the dialog box for warnings and error messages. */

#define  VSTDEMOTITLE   "Vstdemo"
#define  VSTDEMODEBUG   "vstdebug.txt"
/* Define VSTDEMODEBUG if you want to create a sample debug output file
 * named vstdebug.txt in the current directory.
 */

/* #define  VSTDEMODEBUG */

/* Define VSTDEMOWRITETOREG if you would like the sample transactions
 * to be written to the register database.
 */

/* #define VSTDEMOWRITETOREG */

/* Define VSTDEMOMANUALCOMMIT if you want to use the register table
 * manual commit feature to write transaction records to the register
 * pre-returns table.
 */

/* #define  VSTDEMOMANUALCOMMIT */

/* --- Global Data -------------------------------------------------------- */

/* Declare global data.  This data will be used to populate the sample
 * invoices and line items.  The same line item information will appear on
 * each invoice for this example.  The same information is used on each
 * invoice in order to demonstrate how the same line items could be taxed
 * differently in various locations.
 */

#define  INVCOUNT      12   /* Number of sample invoices.               */
#define  TRANSCOUNT     5   /* Number of line items per sample invoice. */

/* Define a convenient structure for storing invoice numbers and
 * jurisdiction info. */

typedef struct
{
	char         sInvNum    [VST_INV_NO_SIZ];
	tLocGeoCode  sInvJuris  [(int) eVstJurisTypeNumElems];
} tInvoiceInfo;

/* Define a convenient structure for storing sample line item info. */

typedef struct
{
	char     sProdSetCd [VST_PROD_CD_SIZ];
	double   sProdQty;
	char     sProdCd    [VST_PROD_CD_SIZ];
	char     sCompntCd  [VST_COMPONENT_CD_SIZ];
	double   sCompntQty;
	double   sExtdAmt;
} tTransInfo;

/* Define invoice numbers and jurisdictional information.  The 
 * jurisdiction information will be used at both the invoice and
 * line-item levels for the invoices in this demo.  With the new API
 * it is possible however to set the jurisdictions separately for each
 * line item on the invoice.
 *
 * Note that the leading zeros have been omitted from some of the
 * GeoCodes below since they are represented as literal long ints.
 *
 * Note also that if an invoice number is not provided for an invoice,
 * then each line item on that invoice will be processed as a separate
 * invoice for purposes of rounding and max tax calculation.
 */

tInvoiceInfo gInvInfo [INVCOUNT] = 
   /* InvNum,   Ship-To,   Ship-From, Order-Acceptance */
	{ "000-01", 390296350, 390296350, 390296350,   /* Berwyn, PA          */
	  "101-01", 310071950, 310071950, 310071950,   /* Pennsauken, NJ      */
	  "001-01",  40910590,  40910590,  40910590,   /* Texarkana, AR       */
	  "001-02",  21220140,  21220140,  21220140,   /* Homer, AK           */
	  "002-01", 100933566, 100933566, 100933566,   /* Basinger, FL        */
	  "003-01",  30211071,  30211071,  30211071,   /* Apache Junction, AZ */
	  "004-01",  30130510,  30130510,  30130510,   /* Gila Bend, AZ       */
	  "005-01",  30130983,  30130983,  30130983,   /* Goodyear, AZ        */
	  "006-01",  30130700,  30130700,  30130700,   /* Surprise, AZ        */
	  "008-01", 350350070, 350350070, 350350070,   /* Grand Forks, ND     */
	  "010-01", 430611783, 430611783, 430611783,   /* Altamont, TN        */
	  "022-01", 430650090, 430650090, 430650090 }; /* Chattanooga, TN     */

/* Define the line item transaction information. */

tTransInfo   gTransInfo [TRANSCOUNT] =
   /* ProdSetCd ProdQty ProdCd      CompntCd      CompntQty ExtdAmt */
	{ "",       2.0,    "COMPSYS",  "MONITOR",    2.0,       1499.90,
	  "",       2.0,    "COMPSYS",  "COMPUTER",   2.0,      15999.90,
	  "",       2.0,    "COMPSYS",  "KYBD/MOUSE", 2.0,        179.50,
	  "",       3.0,    "ART SET",  "PAINTING",   9.0,      22500.00,
	  "",       1.0,    "WALL ART", "",           9.0,      22500.00 };


/* --- Functions ---------------------------------------------------------- */

/* VstdemoMessageCallBack - a function that processes messages from the
 * Sales and Use Tax API.
 */

BOOL POSTFIX VstdemoMessageCallBack (tMsgMessage *pMessage)
{
	BOOL lRetVal = TRUE;

	#if defined(PORT_UNIXANSI)
	/* Format the message and write it to stderr. */
	fprintf(stderr, "\n%s\n\n", pMessage->sText);
	fflush(stderr);

	#elif defined(PORT_AS400)
	char lMessageKey[4],    /* Required for AS/400 messaging. */
		 lErrorCode [4];    /* Required for AS/400 messaging. */

	/* Initialize the AS/400 error code. */
	memset(lErrorCode, 0, 4);

	/* Write message to AS/400 message log. */
	QMHSNDPM("       ",
			 "                    ",
			 pMessage->sText,
			 strlen(pMessage->sText),
			 "*INFO     ",
			 "*EXT      ",
			 0,
			 lMessageKey,
			 lErrorCode);

	#else
	/* Display the error message. */
	MessageBox((HWND) 0,
		pMessage->sText,
		pMessage->sTitle,
			   MB_OK | MB_TASKMODAL);
	
	#endif

	return lRetVal;
} /* VstdemoMessageCallBack() */


/* VstdemoWarningHandler - a function to print a non-fatal warning message.
 * If no message is passed in, then the function simply exits.
 */

void VstdemoWarningHandler (char *pMsg)
{
	tMsgMessage lMsg;

	if (pMsg != NULL)
	{
		lMsg.sSeverity = eMsgSeverityInfo;
		strncpy(lMsg.sTitle, VSTDEMOTITLE, MSG_MAX_TITLE_LEN);
		strncpy(lMsg.sText, pMsg, MSG_MAX_MESSAGE_LEN);
		VstdemoMessageCallBack(&lMsg);
	}
} /* VstdemoWarningHandler() */


/* VstdemoErrorHandler - a function to handle error conditions by closing
 * any open database connections, releasing any allocated handles, and
 * printing an appropriate message.  If no message is passed in, then a
 * generic error message is displayed.
 */

void VstdemoErrorHandler (tVstConnHdl pConnHdl,
						  tVstHdl pDataHdl,
						  char *pMsg)
{
	tMsgMessage lMsg;

	lMsg.sSeverity = eMsgSeverityError;
	strncpy(lMsg.sTitle, VSTDEMOTITLE, MSG_MAX_TITLE_LEN);

	if (pMsg != NULL)
	{
		strncpy(lMsg.sText, pMsg, MSG_MAX_MESSAGE_LEN);
	}
	else
	{
		strcpy(lMsg.sText, "Error encountered.  Exiting ...");
	}

	VstdemoMessageCallBack(&lMsg);

	if (pDataHdl != NULL)
	{
		VstReleaseHdl(pDataHdl);
	}

	if (pConnHdl != NULL)
	{
		VstCloseDb(pConnHdl, eVstDbTypeLocation, NULL, NULL);
		VstCloseDb(pConnHdl, eVstDbTypeRate,     NULL, NULL);
		VstCloseDb(pConnHdl, eVstDbTypeRegister, NULL, NULL);
		VstCloseDb(pConnHdl, eVstDbTypeTDM,      NULL, NULL);
		VstReleaseConnHdl(pConnHdl);
	}

	exit(1);
} /* VstdemoErrorHandler() */


/* VstdemoSetInvoice - a function to populate a data handle with
 * transaction information from a sample invoice.
 */

void VstdemoSetInvoice (tVstHdl pDataHdl, int pInvoiceIdx)
{
	double           lComponentQty;
	tVstTaxblty      lCustTaxability;
	char            *lInvoiceNum;
	tLocGeoCode      lJurisGeocode;
	tVstJurisType    lJurisIdx;
	BOOL             lJurisInCity;
	long             lLineItemIdx;
	long             lLineItemNum;
	double           lProdQty;
	tVstTaxblty      lProdTaxability;
	tVstRegister     lRegWriteInd;
	tVstTaxingJuris  lTaxingJuris;
	double           lTransExtdAmt;
	tVstTaxedGeo     lTransTaxedGeoFlag;

	/* Set invoice-level jurisdiction information. */
	for (lJurisIdx = eVstJurisTypeFirstElem;
		 lJurisIdx < eVstJurisTypeNumElems;
		 lJurisIdx++)
	{
		lJurisGeocode = gInvInfo[pInvoiceIdx].sInvJuris[(int) lJurisIdx];
		lJurisInCity  = TRUE;

		if (VstSetInvJuris(pDataHdl,               /* Tax information handle  */
						   lJurisIdx,              /* Jurisdiction identifier */
						   eVstJurisCdGeo,         /* Rate access method      */
						  &lJurisGeocode,          /* GeoCode                 */
						   NULL,                   /* State abbreviation      */
						   NULL,                   /* Zip code                */
						   NULL,                   /* City name               */
						   NULL,                   /* County name             */
						  &lJurisInCity) == FALSE) /* Within city limits      */
		{
			VstdemoWarningHandler("Can't set invoice-level jurisdiction information.");
		}
	}

	/* Set invoice-level detail information. */
	lInvoiceNum = gInvInfo[pInvoiceIdx].sInvNum;

	/* Set customer taxability to be determined by the TDM. */
	lCustTaxability = eVstTaxbltyTDM;

	/* Set register write indicator to write or not write to the register table.
	 * Defining VSTDEMOWRITETOREG will cause the sample transaction results
	 * to be written to the register table.
	 */
#ifdef VSTDEMOWRITETOREG
	lRegWriteInd = eVstRegisterWrite;
#else
	lRegWriteInd = eVstRegisterDoNotWrite;
#endif

	if (VstSetInvDetail(pDataHdl,                /* Tax information handle   */
						NULL,                    /* Invoice date             */
						NULL,                    /* Invoice gross amount     */
						lInvoiceNum,             /* Invoice number           */
						NULL,                    /* Invoice control number   */
						NULL,                    /* Invoice total tax        */
						NULL,                       /* Customer code            */ // also affects TDM process
						NULL,                    /* Customer class code      */    // Helps in TDM process OR with{custCode}
					   &lCustTaxability,         /* Customer taxability flag */
						NULL,                    /* Company code             */
						NULL,                    /* Division code            */
						NULL,                    /* Store code               */
						NULL,                    /* G/L account number       */
					   &lRegWriteInd) == FALSE)  /* Register write indicator */
	{
		VstdemoWarningHandler("Can't set invoice-level detail information.");
	}

	for (lLineItemIdx = 0; lLineItemIdx < TRANSCOUNT; lLineItemIdx++)
	{
		/* The line item index starts at 0, but we want the actual line item
		 * numbers to start at 1, for this example.
		 */
		lLineItemNum = lLineItemIdx + 1;

		/* Set line item jurisdiction information.  For these sample transactions,
		 * we will simply set the line item jurisdiction information to match the
		 * invoice-level jurisdiction information.
		 */
		for (lJurisIdx = eVstJurisTypeFirstElem;
			 lJurisIdx < eVstJurisTypeNumElems;
			 lJurisIdx++)
		{
			lJurisGeocode = gInvInfo[pInvoiceIdx].sInvJuris[(int) lJurisIdx];
			lJurisInCity  = TRUE;

			if (VstSetLineJuris(pDataHdl,               /* Tax information handle  */
								lLineItemNum,           /* Line item number        */
								lJurisIdx,              /* Jurisdiction identifier */
								eVstJurisCdGeo,         /* Rate access method      */
							   &lJurisGeocode,          /* GeoCode                 */
								NULL,                   /* State abbreviation      */
								NULL,                   /* Zip code                */
								NULL,                   /* City name               */
								NULL,                   /* County name             */
							   &lJurisInCity) == FALSE) /* Within city limits      */
			{
				VstdemoWarningHandler("Can't set line item jurisdiction information.");
			}
		}

		/* Set line item detail information. */
		lTransTaxedGeoFlag = eVstTaxedGeoDetermine;
		lTransExtdAmt      = gTransInfo[lLineItemIdx].sExtdAmt;
		lTaxingJuris       = eVstTaxingJurisPrimary;

		if (VstSetLineDetail(pDataHdl,              /* Tax information handle              */
							 lLineItemNum,          /* Line item number                    */
							 eVstTransTypeSale,     /* Transaction type                    */
							 eVstTransSubTypeNone,  /* Transaction sub-type                */
							 eVstTransCdNormal,     /* Transaction code                    */
							 NULL,                  /* Transaction date                    */
							&lTransTaxedGeoFlag,    /* Taxing jurisdiction indicator       */
							&lTransExtdAmt,         /* Transaction extended amount         */
							 NULL,                  /* Transaction total tax               */
							 NULL,                  /* Transaction combined rate           */
							 NULL,                  /* User data buffer area               */
							&lTaxingJuris,          /* Taxing Jurisdiction associated with */
													/*   customer exemption certificate    */
							 NULL,                  /* Customer exemption certificate      */
							 NULL,                  /* Division code                       */
							 NULL,                  /* Store code                          */
							 NULL) == FALSE)        /* G/Ledger account number             */
		{
			VstdemoWarningHandler("Can't set line item detail information.");
		}

		/* Set line item product information. */
		lProdTaxability = eVstTaxbltyTDM;
		lProdQty        = gTransInfo[lLineItemIdx].sProdQty;
		lComponentQty   = gTransInfo[lLineItemIdx].sCompntQty;

		if (VstSetLineProd(pDataHdl,                            /* Tax information handle    */
						   lLineItemNum,                        /* Line item number          */
						   gTransInfo[lLineItemIdx].sProdSetCd, /* Product set code          */
						  &lProdQty,                            /* Product quantity          */
						   gTransInfo[lLineItemIdx].sProdCd,    /* Product code              */
						   NULL,                                /* Reserved parameter        */
						  &lProdTaxability,                     /* Product taxability flag   */
						   NULL,                                /* Product reporting code    */
						  &lComponentQty,                       /* Component/Transaction qty */
						   gTransInfo[lLineItemIdx].sCompntCd,  /* Component code            */
						   NULL) == FALSE)                      /* Reserved parameter        */
		{
			VstdemoWarningHandler("Can't set line item product information.");
		}
	}
} /* VstdemoSetInvoice() */


/* VstdemoDisplayInvoice - a function to display sample invoice tax
 * calculation results.
 */

void VstdemoDisplayInvoice (tVstHdl pDataHdl)
{
	tVstTaxedGeo     lAddtlTaxedGeoFlag;
	char             lCompntCd          [VST_COMPONENT_CD_SIZ];
	double           lCompntQty;
	tVstFirstOrNext  lFirstOrNext;
	double           lInvGrossAmt;
	char             lInvNo             [VST_INV_NO_SIZ];
	long             lInvNumLineItems;
	double           lInvTotalTax;
	char             lJurisCity         [VST_CITY_SIZ];
	tVstJurisType    lJurisIdx;
	tLocGeoCode      lJurisGeo;
	tVstJurisRetCd   lJurisReturnCd;
	char             lJurisState        [VST_STATE_CD_SIZ];
	tVstLocAddtlType lLocAddtlIdx;
	tVstDiAppl       lLocalDiApply;
	double           lLocalExmtAmt;
	char             lLocalExmtReasCd   [VST_EXEMPT_REA_SIZ];
	double           lLocalNonTaxAmt;
	char             lLocalNonTaxReasCd [VST_NON_TAX_REA_SIZ];
	double           lLocalRate;
	char             lLocalRtEffDate    [VST_DATE_SIZ];
	double           lLocalTax;
	tVstTaxblty      lLocalTaxability;
	double           lLocalTaxableAmt;
	tVstTaxIncl      lLocalTaxIncl;
	tVstTaxType      lLocalTaxType;
	char             lProdCd            [VST_PROD_CD_SIZ];
	double           lProdQty;
	char             lProdSetCd         [VST_PROD_CD_SIZ];
	tVstStLocType    lStLocIdx;
	double           lTransExtdAmt;
	tVstTaxedGeo     lTransTaxedGeoFlag;

	long             lLineItemNum;        /* Current line item number          */
	double           lTotalExtdAmt = 0.0; /* Total extd amt for all line items */
	double           lTotalTax     = 0.0; /* Total tax for all line items      */
	double           lTransTtlTax  = 0.0; /* Total tax for current line item   */

	/* Retrieve invoice detail information. */
	if (VstGetInvDetail(pDataHdl,         /* Tax information handle   */
						NULL,             /* Invoice date             */
					   &lInvGrossAmt,     /* Invoice gross amount     */
						lInvNo,           /* Invoice number           */
						NULL,             /* Invoice control number   */
					   &lInvNumLineItems, /* Number of line items     */
					   &lInvTotalTax,     /* Invoice total tax        */
						NULL,             /* Customer code            */
						NULL,             /* Customer class code      */
						NULL,             /* Customer taxability flag */
						NULL,             /* Company code             */
						NULL,             /* Division code            */
						NULL,             /* Store code               */
						NULL,             /* G/L account number       */
						NULL) == FALSE)   /* Register file indicator  */
	{
		VstdemoWarningHandler("Failed to retrieve invoice detail information.");
	}

	/* Display transaction information. */
	printf("\n\nInvoice: %s\n\n", lInvNo);
	printf("Invoice Gross Amt  Invoice Total Tax\n");
	printf("-----------------  -----------------\n");
	printf("%17.2f  %17.2f\n", lInvGrossAmt, lInvTotalTax);

	printf("\nLine Item Summary\n\n");
	printf("Line ProdSetCd ProdQty ProdCd       CompntCd     CompntQty ExtdAmt   Tax     \n");
	printf("---- --------- ------- ------------ ------------ --------- --------- --------\n");

	/* Retrieve the first invoice line item number. */
	lFirstOrNext = eVstFirst;

	while (VstGetLineItemNumber(pDataHdl, lFirstOrNext, &lLineItemNum) == TRUE)
	{
		/* Retrieve product related information for the current line item. */
		if (VstGetLineProd(pDataHdl,       /* Tax information handle    */
						   lLineItemNum,   /* Line item number          */
						   lProdSetCd,     /* Product set code          */
						  &lProdQty,       /* Product quantity          */
						   lProdCd,        /* Product code              */
						   NULL,           /* Reserved parameter        */
						   NULL,           /* Product taxability flag   */
						   NULL,           /* Product reporting code    */
						  &lCompntQty,     /* Transaction/Component qty */
						   lCompntCd,      /* Component code            */
						   NULL) == FALSE) /* Reserved parameter        */
		{
			VstdemoWarningHandler("Failed to retrieve line item product information.");
		}

		/* Retrieve line item detail information. */
		if (VstGetLineDetail(pDataHdl,       /* Tax information handle    */
							 lLineItemNum,   /* Line item number          */
							 NULL,           /* Transaction type          */
							 NULL,           /* Transaction sub-type      */
							 NULL,           /* Transaction code          */
							 NULL,           /* Transaction date          */
							 NULL,           /* Taxing juris indicator    */
							&lTransExtdAmt,  /* Transaction extended amt  */
							&lTransTtlTax,   /* Transaction total tax     */
							 NULL,           /* Transaction combined rate */
							 NULL,           /* User area data buffer     */
							 NULL,           /* Transaction status code   */
							 NULL,           /* Exempt cert. taxing juris */
							 NULL,           /* Exemption cert. number    */
							 NULL,           /* Division code             */
							 NULL,           /* Store code                */
							 NULL) == FALSE) /* G/L account number        */
		{
			VstdemoWarningHandler("Failed to retrieve line item detail information.");
		}

		printf(" %2ld  %-9.9s ", lLineItemNum, lProdSetCd);
		printf("%7.3f %-12.12s %-12.12s ", lProdQty, lProdCd, lCompntCd);
		printf("%9.3f %9.2f %8.2f\n", lCompntQty, lTransExtdAmt, lTransTtlTax);

		/* Add current line item amounts to running totals. */
		lTotalExtdAmt += lTransExtdAmt;
		lTotalTax += lTransTtlTax;

		/* Set lFirstOrNext to retrieve next line item number. */
		lFirstOrNext = eVstNext;
	}

	/* Display running totals. */
	printf("%68s --------\n", "---------");
	printf("%68.2f %8.2f\n", lTotalExtdAmt, lTotalTax);

	/* Display line item jurisdiction information. */
	printf("\nLine Item Jurisdiction Summary\n\n");
	printf("Line Ship-To      Ship-From    Order-Accept TaxingJuris Name               \n");
	printf("---- ------------ ------------ ------------ ----------- -------------------\n");
	
	/* Retrieve the first invoice line item number again. */
	lFirstOrNext = eVstFirst;

	while (VstGetLineItemNumber(pDataHdl, lFirstOrNext, &lLineItemNum) == TRUE)
	{
		printf(" %2ld  ", lLineItemNum);

		for (lJurisIdx = eVstJurisTypeFirstElem;
			 lJurisIdx < eVstJurisTypeNumElems;
			 lJurisIdx++)
		{
			/* Retrieve line item jurisdiction GeoCode information. */
			if (VstGetLineJuris(pDataHdl,                 /* Tax information handle       */
								lLineItemNum,             /* Line item number             */
								lJurisIdx,                /* Jurisdiction Id              */
								NULL,                     /* Rate access method           */
							   &lJurisGeo,                /* GeoCode                      */
								NULL,                     /* State abbreviation           */
								NULL,                     /* Zip code                     */
								NULL,                     /* City name                    */
								NULL,                     /* County name                  */
								NULL,                     /* Inside/outside city limits   */
							   &lJurisReturnCd) == FALSE) /* Jurisdiction level indicator */
			{
				VstdemoWarningHandler("Failed to retrieve line item jurisdiction information.");
			}

			printf("%09ld    ", lJurisGeo);
		}

		/* Call VstGetLineDetail to retrieve taxing GeoCode flag. */
		if (VstGetLineDetail(pDataHdl,           /* Tax information handle    */
							 lLineItemNum,       /* Line item number          */
							 NULL,               /* Transaction type          */
							 NULL,               /* Transaction sub-type      */
							 NULL,               /* Transaction code          */
							 NULL,               /* Transaction date          */
							&lTransTaxedGeoFlag, /* Taxing juris indicator    */
							 NULL,               /* Transaction extended amt  */
							 NULL,               /* Transaction total tax     */
							 NULL,               /* Transaction combined rate */
							 NULL,               /* User area data buffer     */
							 NULL,               /* Transaction status code   */
							 NULL,               /* Exempt cert. taxing juris */
							 NULL,               /* Exemption cert. number    */
							 NULL,               /* Division code             */
							 NULL,               /* Store code                */
							 NULL) == FALSE)     /* G/L account number        */
		{
			VstdemoWarningHandler("Failed to retrieve line item detail information.");
		}

		/* Map taxed GeoCode flag to a jurisdiction type. */
		switch (lTransTaxedGeoFlag)
		{
		case eVstTaxedGeoOrderAccept:
			lJurisIdx = eVstJurisTypeOrderAccept;
			break;

		case eVstTaxedGeoShipFrom:
			lJurisIdx = eVstJurisTypeShipFrom;
			break;

		default:
			lJurisIdx = eVstJurisTypeShipTo;
			break;
		}

		/* Retrieve GeoCode and name information for taxing jurisdiction. */
		if (VstGetLineJuris(pDataHdl,                 /* Tax information handle       */
							lLineItemNum,             /* Line item number             */
							lJurisIdx,                /* Jurisdiction Id              */
							NULL,                     /* Rate access method           */
						   &lJurisGeo,                /* GeoCode                      */
							lJurisState,              /* State abbreviation           */
							NULL,                     /* Zip code                     */
							lJurisCity,               /* City name                    */
							NULL,                     /* County name                  */
							NULL,                     /* Inside/outside city limits   */
						   &lJurisReturnCd) == FALSE) /* Jurisdiction level indicator */
		{
			VstdemoWarningHandler("Failed to retrieve line item jurisdiction information.");
		}

		printf("%09ld   %15.15s, %2s\n", lJurisGeo, lJurisCity, lJurisState);

		/* Set lFirstOrNext to retrieve next line item number. */
		lFirstOrNext = eVstNext;
	}

	/* Display line item detailed tax information. */
	printf("\nLine Item Tax Detail\n\n");
	printf("Line JurisLevel ExtdAmt   Non-Taxable Exempt     Taxable   Rate    Tax     \n");
	printf("---- ---------- --------- ----------- ---------- --------- ------- --------\n");
	
	/* Reset the running tax total to zero. */
	lTotalTax = 0.0;

	/* Retrieve the first invoice line item number again. */
	lFirstOrNext = eVstFirst;

	while (VstGetLineItemNumber(pDataHdl, lFirstOrNext, &lLineItemNum) == TRUE)
	{
		/* Retrieve line item detail information for extended amt. */
		if (VstGetLineDetail(pDataHdl,       /* Tax information handle    */
							 lLineItemNum,   /* Line item number          */
							 NULL,           /* Transaction type          */
							 NULL,           /* Transaction sub-type      */
							 NULL,           /* Transaction code          */
							 NULL,           /* Transaction date          */
							 NULL,           /* Taxing juris indicator    */
							&lTransExtdAmt,  /* Transaction extended amt  */
							 NULL,           /* Transaction total tax     */
							 NULL,           /* Transaction combined rate */
							 NULL,           /* User area data buffer     */
							 NULL,           /* Transaction status code   */
							 NULL,           /* Exempt cert. taxing juris */
							 NULL,           /* Exemption cert. number    */
							 NULL,           /* Division code             */
							 NULL,           /* Store code                */
							 NULL) == FALSE) /* G/L account number        */
		{
			VstdemoWarningHandler("Failed to retrieve line item detail information.");
		}

		for (lStLocIdx = eVstStLocTypeFirstElem;
			 lStLocIdx < eVstStLocTypeNumElems;
			 lStLocIdx++)
		{
			/* Retrieve jurisdiction information for the current line item. */
			if (VstGetLineLocal(pDataHdl,                 /* Tax information handle        */
								lLineItemNum,             /* Line item number              */
								lStLocIdx,                /* Jurisdiction level            */
							   &lLocalTaxability,         /* Juris taxability flag         */
							   &lLocalTaxType,            /* Juris tax type                */
							   &lLocalTaxIncl,            /* Juris tax inclusion flag      */
								lLocalExmtReasCd,         /* Juris exempt reason code      */
							   &lLocalExmtAmt,            /* Juris exempt amount           */
								lLocalNonTaxReasCd,       /* Juris non-taxable reason code */
							   &lLocalNonTaxAmt,          /* Juris non-taxable amount      */
							   &lLocalRate,               /* Juris rate                    */
								lLocalRtEffDate,          /* Juris rate effective date     */
							   &lLocalTaxableAmt,         /* Juris taxable amount          */
							   &lLocalTax,                /* Juris tax amount              */
							   &lLocalDiApply) == FALSE)  /* Juris district apply flag     */
			{
				VstdemoWarningHandler("Failed to retrieve local jurisdiction information.");
			}

			/* For this example, we only want to show jurisdiction levels
			 * that have collected a tax and also any non-taxable or exempt
			 * amounts.
			 */
			if ((lLocalTax != 0.0) ||
				(lLocalNonTaxAmt != 0.0) ||
				(lLocalExmtAmt != 0.0))
			{
				printf(" %2ld  ", lLineItemNum);

				switch (lStLocIdx)
				{
				case eVstStLocTypeState:
					printf("%-10s ", "State");
					break;

				case eVstStLocTypeCnty:
					printf("%-10s ", "County");
					break;

				case eVstStLocTypeCity:
					printf("%-10s ", "City");
					break;

				case eVstStLocTypeDist:
					printf("%-10s ", "District");
					break;

				default:
					printf("%-10s ", "Unknown");
					break;
				}

				printf("%9.2f ", lTransExtdAmt);
				printf("%10.2f%1s ", lLocalNonTaxAmt, lLocalNonTaxReasCd);
				printf("%9.2f%1s ", lLocalExmtAmt, lLocalExmtReasCd);
				printf("%9.2f %7.5f ", lLocalTaxableAmt, lLocalRate);
				printf("%8.2f\n", lLocalTax);

				lTotalTax += lLocalTax;
			}
		}

		for (lLocAddtlIdx = eVstLocAddtlTypeFirstElem;
			 lLocAddtlIdx < eVstLocAddtlTypeNumElems;
			 lLocAddtlIdx++)
		{
			/* Retrieve additional jurisdiction information for the current line item. */
			if (VstGetLineLocalAddtl(pDataHdl,                 /* Tax information handle        */
									 lLineItemNum,             /* Line item number              */
									 lLocAddtlIdx,             /* Jurisdiction level            */
									&lLocalTaxability,         /* Juris taxability flag         */
									&lLocalTaxType,            /* Juris tax type                */
									&lLocalTaxIncl,            /* Juris tax inclusion flag      */
									 lLocalExmtReasCd,         /* Juris exempt reason code      */
									&lLocalExmtAmt,            /* Juris exempt amount           */
									 lLocalNonTaxReasCd,       /* Juris non-taxable reason code */
									&lLocalNonTaxAmt,          /* Juris non-taxable amount      */
									&lLocalRate,               /* Juris rate                    */
									 lLocalRtEffDate,          /* Juris rate effective date     */
									&lLocalTaxableAmt,         /* Juris taxable amount          */
									&lLocalTax,                /* Juris tax amount              */
									&lAddtlTaxedGeoFlag,       /* Taxing jurisdiction indicator */
									&lLocalDiApply) == FALSE)  /* Juris district apply flag     */
			{
				VstdemoWarningHandler("Failed to retrieve local additional jurisdiction information.");
			}

			/* Again, for this example, we only want to show jurisdiction
			 * levels that have collected a tax and also any non-taxable or
			 * exempt amounts.
			 */
			if ((lLocalTax != 0.0) ||
				(lLocalNonTaxAmt != 0.0) ||
				(lLocalExmtAmt != 0.0))
			{
				printf(" %2ld  ", lLineItemNum);

				switch (lLocAddtlIdx)
				{
				case eVstLocAddtlTypeCnty:
					printf("%-10s ", "Addtl Cnty");
					break;

				case eVstLocAddtlTypeCity:
					printf("%-10s ", "Addtl City");
					break;

				case eVstLocAddtlTypeDist:
					printf("%-10s ", "Addtl Dist");
					break;

				default:
					printf("%-10s ", "Unknown");
					break;
				}

				printf("%9.2f ", lTransExtdAmt);
				printf("%10.2f%1s ", lLocalNonTaxAmt, lLocalNonTaxReasCd);
				printf("%9.2f%1s ", lLocalExmtAmt, lLocalExmtReasCd);
				printf("%9.2f %7.5f ", lLocalTaxableAmt, lLocalRate);
				printf("%8.2f\n", lLocalTax);

				lTotalTax += lLocalTax;
			}
		}

		/* Set lFirstOrNext to retrieve next line item number. */
		lFirstOrNext = eVstNext;
	}

	/* Display running tax total. */
	printf("%75s\n", "--------");
	printf("%75.2f\n", lTotalTax);

} /* VstdemoDisplayInvoice() */


/* main - the primary function of the program.  This function calls the
 * other Vstdemo functions and the STQ 4.0 API functions to establish
 * the connections to the GeoCoder, Rate, Register, and TDM databases.
 * This function calls the other functions in the program to prepare the
 * invoice information, calculate tax, display the results, and finally
 * close the database connections and release the connection and data handles.
 */

int main (void)
{
	tVstConnHdl      lConnHdl = NULL; /* VST connection handle                 */
	tVstHdl          lDataHdl = NULL; /* VST invoice and line item data handle */
	tVstCalcRetCd    lCalcRetCd;      /* Tax calculation function return code  */
	int              lInvoiceIdx;     /* Index for invoice loop                */

	printf("Vertex Sales Tax Q Series API 4.0 Demo Program\n\n");
	printf("Copyright 1995-2010 Vertex Inc.\n\n");

	/* Create connection and data handles. */

	if ((lConnHdl = VstCreateConnHdl()) != NULL &&
		(lDataHdl = VstCreateHdl()) != NULL)
	{
		/* Set message call back function to display VST error messages. */

		if (VstSetMessageCallBack(lConnHdl,
				(tMsgCallBack) VstdemoMessageCallBack) == FALSE)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl, "Can't set message call back function.");
		}

		/* Set attribute to retrieve taxing jurisdiction name information. */

		if (VstSetAttrib(lConnHdl, eVstAttribGetJurisNames, TRUE) == FALSE)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl, "Can't set GetJurisNames attribute.");
		}

	#ifdef VSTDEMOMANUALCOMMIT

		/* Set attribute to manually commit records to the register
		 * pre-returns table.
		 */

		if (VstSetAttrib(lConnHdl, eVstAttribRegManCommit, TRUE) == FALSE)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl, "Can't set RegManCommit attribute.");
		}

	#endif

		/* Open all databases for reading. If we are writing the sample
		 * transactions to the register database, then it needs to be
		 * opened with read/write access. Note: this example assumes that
		 * all four databases reside in the same location/server. It is
		 * possible that the databases may reside in multiple locations
		 * in your environment, in which case the parameters below will
		 * need to be modified to suit your environment. */

		if (VstOpenDb(lConnHdl, eVstDbTypeLocation, DATASOURCE, SERVER, USER,
			 PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl, "Couldn't open the Location database.");

		}

		if (VstOpenDb(lConnHdl, eVstDbTypeRate, DATASOURCE, SERVER, USER,
			 PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl, "Couldn't open the Rate database.");
		}

	#ifdef VSTDEMOWRITETOREG
		if (VstOpenDb(lConnHdl, eVstDbTypeRegister, DATASOURCE, SERVER, USER,
			 PASSWORD, eVstAccessModeReadWrite, NULL, NULL) != eVstDbRetCdNone)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl,
					"Couldn't open the Register database for writing.");
		}
	#else
		if (VstOpenDb(lConnHdl, eVstDbTypeRegister, DATASOURCE, SERVER, USER,
			 PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl, "Couldn't open the Register database.");
		}
	#endif

		if (VstOpenDb(lConnHdl, eVstDbTypeTDM, DATASOURCE, SERVER, USER,
			 PASSWORD, eVstAccessModeReadOnly, NULL, NULL) != eVstDbRetCdNone)
		{
			VstdemoErrorHandler(lConnHdl, lDataHdl, "Couldn't open the TDM database.");
		}

		/* Process multiple sample invoices. */

		for (lInvoiceIdx = 0; lInvoiceIdx < INVCOUNT; lInvoiceIdx++)
		{
			/* Make sure we have a clean invoice handle. */

			if (VstResetHdl(lDataHdl) != TRUE)
			{
				VstdemoErrorHandler(lConnHdl, lDataHdl, "Couldn't reset invoice handle.");
			}

			/* Call demo function to set transaction information for this invoice. */

			VstdemoSetInvoice(lDataHdl, lInvoiceIdx);

		#ifdef VSTDEMODEBUG

			/* Write out data handle information to a debug text file. */

			if (VstDebugHdl(lDataHdl, "vstdebug.txt", "Before VstCalcTax") == FALSE)
			{
				VstdemoWarningHandler("VstDebugHdl function failed prior to tax calculation.");
			}

		#endif

			/* Perform tax calculations. */

			lCalcRetCd = VstCalcTax(lConnHdl, lDataHdl, NULL, NULL);

			/* Evaluate the calculation return code. */

			switch(lCalcRetCd)
			{
				case eVstCalcRetCdNone:
				case eVstCalcRetCdTdmMtDefault:
					/* Success. */
					break;

				default:
					VstdemoErrorHandler(lConnHdl, lDataHdl, 
							"Tax calculation routine failed.");
					break;
			}

		#ifdef VSTDEMODEBUG
			
			/* Write out data handle information to a debug text file. */
		
			if (VstDebugHdl(lDataHdl, "vstdebug.txt", "After VstCalcTax") == FALSE)
			{
				VstdemoWarningHandler("VstDebugHdl function failed after tax calculation.");
			}

		#endif

			/* Call demo function to display transaction information. */
			
			VstdemoDisplayInvoice(lDataHdl);
		
		#ifdef VSTDEMOMANUALCOMMIT

			/* Manually commit transactions to register pre-returns table. */

			if (VstRegisterCommit(lConnHdl, TRUE) == FALSE)
			{
				VstdemoWarningHandler("VstRegisterCommit failed.");
			}

		#endif
		}

		/* Release data handle. */

		if (VstReleaseHdl(lDataHdl) != TRUE)
		{
			VstdemoWarningHandler("Couldn't release data handle.");
		}

		/* Close all the database connections. */

		if (VstCloseDb(lConnHdl, eVstDbTypeLocation, NULL, NULL) != eVstDbRetCdNone ||
			VstCloseDb(lConnHdl, eVstDbTypeRate, NULL, NULL) != eVstDbRetCdNone ||
			VstCloseDb(lConnHdl, eVstDbTypeRegister, NULL, NULL) != eVstDbRetCdNone ||
			VstCloseDb(lConnHdl, eVstDbTypeTDM, NULL, NULL) != eVstDbRetCdNone)
		{
			/* Print a warning that an error occurred while closing the databases.
			 * Alternately, we could have handled each possible error condition
			 * for each of the databases to produce a more detailed message.
			 */
			VstdemoWarningHandler("Error encountered while closing the databases.");
		}

		/* Release the connection handle. */

		if (VstReleaseConnHdl(lConnHdl) != TRUE)
		{
			VstdemoWarningHandler("Couldn't release connection handle.");
		}
	}
	else
	{
		VstdemoErrorHandler(lConnHdl, lDataHdl, "Couldn't create connection or data handle.");
	}

	return 0;
} /* main() */
