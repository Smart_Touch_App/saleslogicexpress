@echo off
setlocal

if not "%sqlany16%"=="" goto have_sqlanyn
set _sa_bin=
goto after_find_sa_bin
:have_sqlanyn
set _sa_bin="%sqlany16%\Bin64\"
if exist %_sa_bin%dbisql.exe goto after_find_sa_bin
set _sa_bin="%sqlany16%\Bin32\"
:after_find_sa_bin

REM Setting variables. . .
if [%1] == [] goto usage
set CONNECTION=%~1
set SQL_FILE=cons_setup.sql

goto after_usage
:usage
REM Display Usage
echo Usage:   %0 CONNECTION
echo.
echo CONNECTION   quoted dbisql connection string for the consolidated database (eg "DSN=my_dsn;UID=my_user;PWD=my_pwd")
goto end
:after_usage

REM Executing commands. . .

echo %_sa_bin%dbisql -nogui -c "%CONNECTION%" READ ENCODING UTF8 '%SQL_FILE%'
%_sa_bin%dbisql -nogui -c "%CONNECTION%" READ ENCODING UTF8 '%SQL_FILE%'

:end
