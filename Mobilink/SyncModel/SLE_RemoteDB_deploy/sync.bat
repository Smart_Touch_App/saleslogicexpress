@echo off
setlocal

if not "%sqlany16%"=="" goto have_sqlanyn
set _sa_bin=
goto after_find_sa_bin
:have_sqlanyn
set _sa_bin="%sqlany16%\Bin64\"
if exist %_sa_bin%dbmlsync.exe goto after_find_sa_bin
set _sa_bin="%sqlany16%\Bin32\"
:after_find_sa_bin

REM Setting variables. . .
if [%1] == [] goto usage
set CONNECTION=%~1
set PROFILE="SLE_RemoteDB_FBM783"
set VERBOSITY=
set LOG_FILE="remote.dbs"
set PARAMS=

goto after_usage
:usage
REM Display Usage
echo Usage:   %0 CONNECTION
echo.
echo CONNECTION   REMOTE DBA connection string for synchronizing the remote database (eg "DBF=my_db.db;UID=my_sync_user;PWD=my_sync_pass") See also "GRANT REMOTE DBA statement" in documentation.
goto end
:after_usage

REM Executing commands. . .
echo %_sa_bin%dbmlsync -c "%CONNECTION%" -sp %PROFILE% %VERBOSITY% -ot %LOG_FILE% %PARAMS%
%_sa_bin%dbmlsync -c "%CONNECTION%" -sp %PROFILE% %VERBOSITY% -ot %LOG_FILE% %PARAMS%

:end
