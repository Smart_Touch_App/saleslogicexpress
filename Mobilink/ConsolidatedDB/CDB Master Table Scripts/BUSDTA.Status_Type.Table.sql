insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'OPEN', 'Open',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'CLSD', 'Closed',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'STTLD', 'Settled',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'USTLD', 'UnSettled',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'CMTD', 'Committed',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'VOID', 'Void',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'VDSTLD', 'VOIDED / SETTLED',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'HOLD', 'Hold',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Status_Type(StatusTypeID, StatusTypeCD, StatusTypeDESC,CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type), 'INPRG', 'In-Progress',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'VERF','Verified',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'REJCT','Rejected',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'NEW','New',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'DLVRD','Delivered',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'ACCEPT','Accepted',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'FAIL','Fail',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'PASS','Pass',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'PHYSCL','Physical',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'WKLY','Weekly',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'RECNT','Recount',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'SUBMIT','Submitted',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'PNDNG','PENDING',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'CMPLT','COMPLETED',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'RTP','READY TO PICK',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'HOP','HOLD ON PICK',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'INPRS','IN PROCESS',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'RELS','RELEASE',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'INFO','Informational Notification',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'NWARN','Warning Notification',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'NOPTN','Optional Notification',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'NMAND','Mandatory Notification',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'UNKNOWN','Unknown Status',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'PIC','PICK COMPLETE',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'SUBJDE','Submitted To JDE',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'INMOBL','In Mobile',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'ERR','Error',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'PROCSD','Processed',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'SALORD','SalesOrder',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


insert into  busdta.Status_Type (StatusTypeId,StatusTypeCD,StatusTypeDesc,CreatedBy,createddatetime,updatedby,updateddatetime)
values( (select isnull(max(StatusTypeID),0)+1 from busdta.Status_Type),'RETORD','ReturnOrder',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())