INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'ABC', 'Scanner Malfunction', 'Manual Pick', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'ACC', 'Always Cash - but this time', 'Charge on Account', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'AS', 'Authorised Signatory Not Available', 'Void Order', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'BNR', 'Bar Code Not Readable', 'Manual Pick', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) VALUES ((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CAB', 'Items still in stock', 'No Sale Reason', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CAC', 'No Credit', 'No Sale Reason', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CAD', 'Too Expensive', 'No Sale Reason', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CNI', 'Customer Closed', 'No Sale Reason', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CNM', 'Cash Not Matched', 'Reject Settlement', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'COC', 'Customer Out Of Cash', 'Void Order', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'ENA', 'Expense Not Available', 'Void Settlement', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'EXI', 'Excess Item', 'Inventory Adjustment', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime, last_modified) VALUES ((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'IMS', 'Item Misplaced', 'Inventory Adjustment', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'LC', 'Low Cash', 'Reject Settlement', CAST(16 AS Numeric(8, 0)), 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'MNA', 'Money Order Not Available', 'Void Settlement', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.ReasonCodeMaster (ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) VALUES ((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'TUV', 'Short of partial cash', 'Charge on Account', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CR', 'Customer Request', 'Void Receipt',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'IAMT', 'Incorrect Amount', 'Void Receipt',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 

INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES ((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'DULRQ', 'Duplicate Request Count', 'Void Cycle Count',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 

INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'INVRQ', 'Invalid Request', 'Void Cycle Count',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 

INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'RVR', 'Restart', 'Void Replenishment',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 

INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'PVR', 'Postpone', 'Void Replenishment',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 



INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CID', 'Items Defect', 'Credit Memo',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'CCD', 'Customer Dissatisfied', 'Credit Memo',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'VCI', 'Item Replaced', 'Void Credit Memo',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'VRI', 'Customer returned items', 'Void Credit Memo',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'VEX', 'Expense Receipt is not available', 'Void Expenses',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'VIE', 'Invalid Expense', 'Void Expenses',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'VIM', 'Invalid Money Order', 'Void MoneyOrder',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 


INSERT INTO BUSDTA.ReasonCodeMaster(ReasonCodeId, ReasonCode, ReasonCodeDescription, ReasonCodeType,CreatedBy,createddatetime,updatedby,updateddatetime) 
VALUES 
((select isnull(max(ReasonCodeId),1)+1 from BUSDTA.ReasonCodeMaster), 'VQA', 'Not qualified amount', 'Void MoneyOrder',
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate()) 
