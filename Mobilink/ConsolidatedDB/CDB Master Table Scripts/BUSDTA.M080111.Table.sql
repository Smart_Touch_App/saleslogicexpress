USE [MobileDataModel]
GO
DELETE FROM [BUSDTA].[M080111]
GO
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(1 AS Numeric(8, 0)), 'PHONE     ', 'FAX  ', 'FAX                                               ', 'xxx       ')
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(2 AS Numeric(8, 0)), 'PHONE     ', 'HOM  ', 'Home                                              ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(3 AS Numeric(8, 0)), 'PHONE     ', 'A/R  ', 'ACCOUNTS RECEIVABLE                               ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(4 AS Numeric(8, 0)), 'PHONE     ', 'O    ', 'Online                                            ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(5 AS Numeric(8, 0)), 'PHONE     ', 'CORP ', '                                                  ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(6 AS Numeric(8, 0)), 'PHONE     ', 'A/P  ', 'ACCOUNTS PAYABLE                                  ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(7 AS Numeric(8, 0)), 'PHONE     ', 'BILL ', '                                                  ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(8 AS Numeric(8, 0)), 'PHONE     ', 'CELL ', '                                                  ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(9 AS Numeric(8, 0)), 'PHONE     ', '     ', 'Business                                          ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(10 AS Numeric(8, 0)), 'PHONE     ', 'BR   ', 'BRANCH                                            ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(11 AS Numeric(8, 0)), 'PHONE     ', 'BUS  ', 'Business                                          ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(12 AS Numeric(8, 0)), 'PHONE     ', 'MOD  ', 'Modem                                             ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(13 AS Numeric(8, 0)), 'PHONE     ', 'TOLL ', '                                                  ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(14 AS Numeric(8, 0)), 'PHONE     ', 'ARFX ', '                                                  ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(15 AS Numeric(8, 0)), 'PHONE     ', 'BUSC ', 'Business                                          ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(16 AS Numeric(8, 0)), 'PHONE     ', 'OFF  ', 'Office                                            ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(17 AS Numeric(8, 0)), 'EMAIL     ', 'E    ', 'Email Address                                     ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(18 AS Numeric(8, 0)), 'EMAIL     ', 'I    ', 'Internet Address (URL)                            ', NULL)
INSERT [BUSDTA].[M080111] ([CTID], [CTTYP], [CTCD], [CTDSC1], [CTTXA1]) VALUES (CAST(19 AS Numeric(8, 0)), 'EMAIL     ', 'O    ', 'Online                                            ', NULL)
