INSERT BUSDTA.Category_Type (CategoryTypeID, CategoryTypeCD, CategoryCodeType, CategoryTypeDESC, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(CategoryTypeID),0)+1 from BUSDTA.Category_Type), 'RPR', 'Expenses', 'Repair', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.Category_Type (CategoryTypeID, CategoryTypeCD, CategoryCodeType, CategoryTypeDESC, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(CategoryTypeID),0)+1 from BUSDTA.Category_Type), 'FUEL', 'Expenses', 'Fuel', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.Category_Type (CategoryTypeID, CategoryTypeCD, CategoryCodeType, CategoryTypeDESC, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(CategoryTypeID),0)+1 from BUSDTA.Category_Type), 'MEALS', 'Expenses', 'Meals(Actual Cost)', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.Category_Type (CategoryTypeID, CategoryTypeCD, CategoryCodeType, CategoryTypeDESC, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(CategoryTypeID),0)+1 from BUSDTA.Category_Type), 'MOCHR', 'Expenses', 'Money Order Charges', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.Category_Type (CategoryTypeID, CategoryTypeCD, CategoryCodeType, CategoryTypeDESC, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(CategoryTypeID),0)+1 from BUSDTA.Category_Type), 'TIRE', 'Expenses', 'Tires', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

INSERT BUSDTA.Category_Type (CategoryTypeID, CategoryTypeCD, CategoryCodeType, CategoryTypeDESC, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime) 
VALUES 
((select isnull(max(CategoryTypeID),0)+1 from BUSDTA.Category_Type), 'PKCHG', 'Expenses', 'Parking Charges', 
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),
(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


