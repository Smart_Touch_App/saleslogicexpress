insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Engine Fluid Levels', 
'Oil, Transmission, Coolant Brake, Etc.',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Paperwork', 
'Registration, Insurance, Accident Kit.',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Brakes', 
'Service, Parking',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Steering Mechanism', 
'',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Horn', 
'',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Belt', 
'',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())


insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Tires and Wheels', 
'Pressure and Condition',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Lights - Head, Tail and Warning, Directional', 
'',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Mirrors', 
'',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())

insert into busdta.Question_Master(QuestionId, QuestionTitle, QuestionDescription, IsMandatory, IsMultivalue, IsDescriptive, CreatedBy, CreatedDatetime, UpdatedBy, UpdatedDatetime)
values
((select isnull(max(QuestionId),0)+1 from busdta.Question_Master),
'Glass/Reflectors', 
'',
1, 0, 0, (select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate(),(select App_user_id from busdta.user_master where App_User = 'SYS001'), getDate())
