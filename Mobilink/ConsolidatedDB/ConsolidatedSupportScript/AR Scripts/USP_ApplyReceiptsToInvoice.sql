SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		WonderBiz Technology Pvt Ltd
-- Create date: 21-August-2015
-- Description:	Applies specified receipts to an invoice
-- =============================================
IF EXISTS(SELECT * FROM sys.objects WHERE name='USP_ApplyReceiptsToInvoice' AND schema_id=SCHEMA_ID('BUSDTA'))
BEGIN 
	DROP PROCEDURE [BUSDTA].[USP_ApplyReceiptsToInvoice];
END
GO

CREATE PROCEDURE [BUSDTA].[USP_ApplyReceiptsToInvoice] 
	@CustomerId int = 0, 
	@RouteId int = 0, 
	@InvoiceId int=0, 
	@ReceiptList varchar(100)='' --Comma separated value of receipt id
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF ISNULL(@CustomerId,0)=0
	BEGIN
		RETURN 'Customer id not provided'; 
	END 

	IF ISNULL(@RouteId,0)=0
	BEGIN
		RETURN 'Route id is not provided'; 
	END 

	IF ISNULL(@ReceiptList,'')=''
	BEGIN
		RETURN 'Please provide atleast one receipt id.'; 
	END 

	IF ISNULL(@InvoiceId,0)=0
	BEGIN
		RETURN 'Invoice id is not provided'; 
	END 

	DECLARE @RemainingReceiptAmount decimal(18,2)=0;
	DECLARE @OpenInvoiceBalance decimal(18,2);
	DECLARE @ReceiptId int=0;
	DECLARE @ReceiptAmt decimal(18,2)=0;
	DECLARE @MaxCustomerLedgerId int = 0;
	DECLARE @Query nvarchar(max)=N'';
	DECLARE @RecordCount int=0; 

	BEGIN TRY 
	
		BEGIN TRANSACTION 

			SELECT @MaxCustomerLedgerId=MAX(CustomerLedgerId)+1 FROM BUSDTA.Customer_Ledger WHERE RouteId=@RouteId; 

			IF (@MaxCustomerLedgerId IS NULL OR @MaxCustomerLedgerId=1)
			BEGIN; 
				THROW 60000, 'No record found in customer_ledger. cannot proceed.',1;
			END;

			--Check if all the provided invoice are processed through EOD 
			SET @Query='SELECT @RecordCount=COUNT(*) FROM BUSDTA.Receipt_Header WHERE ReceiptID IN (' + @ReceiptList + ') AND SettelmentId IS NULL'; 
			
			EXECUTE SP_EXECUTESQL @Query, N'@RecordCount int OUTPUT', @RecordCount = @RecordCount output; 

			IF (@RecordCount<>0)
			BEGIN; 
				THROW 60000, 'All receipts are not processed through EOD, exiting',1; 
			END; 
			
			--Get sum of remaining receipt amoung 
			SET @Query=N'SELECT @RemainingReceiptAmount=SUM(A.Amount) FROM (SELECT RH.ReceiptId, SUM(RH.TransactionAmount - (CASE CL.IsActive WHEN 1 THEN ISNULL(CL.ReceiptAppliedAmt,0) ELSE 0 END)) AS Amount FROM BUSDTA.Customer_Ledger CL';
			SET @Query=@Query + N' INNER JOIN BUSDTA.Receipt_Header RH ON CL.Receiptid=RH.ReceiptId AND CL.RouteId=RH.RouteId AND CL.CustomerId=RH.CustomerId AND RH.SettelmentId IS NOT NULL';
			SET @Query=@Query + N' WHERE LTRIM(RTRIM(CL.ReceiptId)) IN ( ' + @ReceiptList + ') AND CL.CustomerId='+CAST(@CustomerId AS varchar(100)) + ' AND CL.RouteId=' + CAST(@RouteId AS varchar(100));
			SET @Query=@Query + N' GROUP BY RH.ReceiptId) A';

			EXECUTE SP_EXECUTESQL @Query, N'@RemainingReceiptAmount decimal(18,2) OUTPUT', @RemainingReceiptAmount = @RemainingReceiptAmount output; 
		 
			--Get open Invoice balance 
			SET @Query=N'SELECT @OpenInvoiceBalance=(IH.GrossAmt-SUM((CASE CL.IsActive WHEN 1 THEN (ISNULL(CL.ReceiptAppliedAmt,0)+ISNULL(CL.ConcessionAmt,0)) ELSE 0 END))) FROM BUSDTA.Customer_Ledger CL';
			SET @Query=@Query + N' INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.CustomerId=IH.CustomerId AND CL.RouteId=IH.RouteId';
			SET @Query=@Query + N' WHERE IH.InvoiceId = '+ CAST(@InvoiceId AS varchar(100)) + ' AND CL.CustomerId='+CAST(@CustomerId AS varchar(100)) + ' AND CL.RouteId=' + CAST(@RouteId AS varchar(100));
			SET @Query=@Query + N' GROUP BY IH.GrossAmt';

			--PRINT @Query;
			EXECUTE SP_EXECUTESQL @Query, N'@OpenInvoiceBalance decimal(18,2) OUTPUT', @OpenInvoiceBalance = @OpenInvoiceBalance output; 

			/*
			--Compare Invoice balance
			IF(@RemainingReceiptAmount<@openInvoiceBalance)
			BEGIN 
				RETURN 'Receipt amount is less than the invoice amount. cannot apply.'; 
			END 
			*/

			SET @Query=N'DECLARE cursor_ApplyReceiptsToInvoice CURSOR FOR ';
			SET @Query=@Query + N' SELECT RH.ReceiptId, SUM(RH.TransactionAmount - (CASE CL.IsActive WHEN 1 THEN ISNULL(CL.ReceiptAppliedAmt,0) ELSE 0 END)) AS Amount FROM BUSDTA.Customer_Ledger CL';
			SET @Query=@Query + N' INNER JOIN BUSDTA.Receipt_Header RH ON CL.Receiptid=RH.ReceiptId AND CL.CustomerId=RH.CustomerId AND CL.RouteId=RH.RouteId AND RH.SettelmentId IS NOT NULL';
			SET @Query=@Query + N' WHERE LTRIM(RTRIM(CL.ReceiptId)) IN (' + @ReceiptList + ') AND CL.CustomerId='+CAST(@CustomerId AS varchar(100)) + ' AND CL.RouteId=' + CAST(@RouteId AS varchar(100));
			SET @Query=@Query + N' GROUP BY RH.ReceiptID';

			--Print @Query; 
			EXECUTE SP_EXECUTESQL @Query;

			OPEN cursor_ApplyReceiptsToInvoice;

			FETCH NEXT FROM cursor_ApplyReceiptsToInvoice INTO @ReceiptId, @ReceiptAmt;

			WHILE @@FETCH_STATUS = 0 
			BEGIN; 

				IF (@OpenInvoiceBalance>=@ReceiptAmt)
					BEGIN; 
						INSERT INTO BUSDTA.Customer_Ledger (CustomerLedgerId, ReceiptID, RouteId, InvoiceId, CustomerId, IsActive, ReceiptAppliedAmt, DateApplied, UpdatedDateTime, CreatedDatetime)
						SELECT @MaxCustomerLedgerId AS CustomerLedgerId, @ReceiptId AS ReceiptId, @RouteId AS RouteId,  @InvoiceId AS InvoiceId, @CustomerId AS CustomerId, 1 AS IsActive, @ReceiptAmt AS ReceiptAppliedAmt, GETDATE() AS DateApplied, GETDATE() AS UpdatedDateTime, GETDATE() AS CreatedDateTime 
						FROM BUSDTA.Receipt_Header WHERE ReceiptId =@ReceiptId AND RouteId=@RouteId AND CustomerId=@CustomerId;
						
						SET @OpenInvoiceBalance=@OpenInvoiceBalance-@ReceiptAmt;
					END; 
				ELSE
					BEGIN; 
						INSERT INTO BUSDTA.Customer_Ledger (CustomerLedgerId, ReceiptID, RouteId, InvoiceId, CustomerId, IsActive, ReceiptAppliedAmt, DateApplied, UpdatedDateTime, CreatedDatetime)
						SELECT @MaxCustomerLedgerId AS CustomerLedgerId, @ReceiptId AS ReceiptId, @RouteId AS RouteId,  @InvoiceId AS InvoiceId, @CustomerId AS CustomerId, 1 AS IsActive, @OpenInvoiceBalance AS ReceiptAppliedAmt, GETDATE() AS DateApplied, GETDATE() AS UpdatedDateTime, GETDATE() AS CreatedDateTime 
						FROM BUSDTA.Receipt_Header WHERE ReceiptId =@ReceiptId AND RouteId=@RouteId AND CustomerId=@CustomerId;
						
						SET @OpenInvoiceBalance=@OpenInvoiceBalance-@OpenInvoiceBalance;
					END; 

				SET @MaxCustomerLedgerId=@MaxCustomerLedgerId+1;

				FETCH NEXT FROM cursor_ApplyReceiptsToInvoice INTO @ReceiptId, @ReceiptAmt;
			END; 


			IF (@OpenInvoiceBalance=0)
				BEGIN; 
					UPDATE BUSDTA.Invoice_Header SET ARStatusId=2 WHERE InvoiceId =@InvoiceId AND CustomerId=@CustomerId AND RouteId=@RouteId ;
				END; 
			ELSE
				BEGIN; 
					UPDATE BUSDTA.Invoice_Header SET ARStatusId=1 WHERE InvoiceId=@InvoiceId AND CustomerId=@CustomerId AND RouteId=@RouteId ;
				END; 

			CLOSE cursor_ApplyReceiptsToInvoice;

			DEALLOCATE cursor_ApplyReceiptsToInvoice;

		COMMIT;
	END TRY 
	BEGIN CATCH 
		ROLLBACK; 

		RETURN 'Error while applying receipts to invoice, '+ERROR_MESSAGE();
	END CATCH 
END