SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		WonderBiz Technology Pvt Ltd
-- Create date: 21-August-2015
-- Description:	Associate and dis-associate payments from an Invoice, it will automatically update invoice status
-- =============================================
IF EXISTS(SELECT * FROM sys.objects WHERE name='USP_UpdateReceiptInvoiceMappingStatus' AND schema_id=SCHEMA_ID('BUSDTA'))
BEGIN 
	DROP PROCEDURE [BUSDTA].[USP_UpdateReceiptInvoiceMappingStatus];
END
GO

CREATE PROCEDURE [BUSDTA].[USP_UpdateReceiptInvoiceMappingStatus] 
	-- Add the parameters for the stored procedure here
	@CustomerId int = 0, 
	@RouteId int = 0,
	@ReceiptId int = 0, 
	@InvoiceId int = 0,
	@Status bit= 0			-- 0 Disassociate payment, 1- Associate payment
AS
BEGIN;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF ISNULL(@CustomerId,0)=0 
	BEGIN 
		RETURN 'Customer id is not provided.'; 
	END 
	
	IF ISNULL(@RouteId,0)=0 
	BEGIN 
		RETURN 'Route id is not provided.'; 
	END 

	IF ISNULL(@ReceiptId,0)=0 
	BEGIN 
		RETURN 'Receipt is is not provided.'; 
	END 

	IF ISNULL(@InvoiceId,0)=0 
	BEGIN 
		RETURN 'Invoice id is not provided'; 
	END 

	IF @Status IS NULL 
	BEGIN 
		RETURN 'Active status code not provided.'; 
	END 

	DECLARE @OpenInvoiceAmount decimal(10,2)
	
	BEGIN TRY 
		
		BEGIN TRANSACTION;

			--Check is data is processed through EOD 
			IF EXISTS(SELECT * FROM BUSDTA.Receipt_Header WHERE ReceiptID=@ReceiptId AND RouteId=@RouteId AND SettelmentId IS NULL)
			BEGIN; 
				THROW 60000, 'Receipt is not processed through EOD.',1;
			END;	

			UPDATE CL SET CL.IsActive=@Status FROM BUSDTA.Customer_Ledger CL
			INNER JOIN BUSDTA.Receipt_Header RH ON CL.ReceiptId=RH.ReceiptId AND CL.RouteId=RH.RouteId AND RH.SettelmentId IS NOT NULL AND ISNULL(CL.InvoiceId,0)<>0
			WHERE CL.ReceiptId=@ReceiptId AND CL.CustomerId=@CustomerId AND CL.RouteId=@RouteId AND InvoiceId=@InvoiceId;

			SELECT @OpenInvoiceAmount=(IH.GrossAmt-SUM((CASE CL.IsActive WHEN 1 THEN (ISNULL(CL.ReceiptAppliedAmt,0)+ISNULL(CL.ConcessionAmt,0)) ELSE 0 END))) 
			FROM BUSDTA.Customer_Ledger CL INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.RouteId=IH.RouteId
			WHERE CL.RouteId=@RouteId AND CL.CustomerId=@CustomerId AND CL.InvoiceId=@InvoiceId GROUP BY IH.GrossAmt;

			IF (ISNULL(@OpenInvoiceAmount,0)=0)
			BEGIN 
				UPDATE BUSDTA.Invoice_Header SET ARStatusId=2 WHERE InvoiceId IN (SELECT ISNULL(InvoiceID,0) FROM BUSDTA.Customer_Ledger
				WHERE ReceiptId=@ReceiptId AND CustomerId=@CustomerId AND RouteId=@RouteId AND InvoiceId=@InvoiceId AND IsActive=@Status) 
				AND CustomerId=@CustomerId AND RouteId=@RouteId;
			END
			ELSE
			BEGIN;
				UPDATE BUSDTA.Invoice_Header SET ARStatusId=1 WHERE InvoiceId IN (SELECT ISNULL(InvoiceID,0) FROM BUSDTA.Customer_Ledger
				WHERE ReceiptId=@ReceiptId AND CustomerId=@CustomerId AND RouteId=@RouteId AND InvoiceId=@InvoiceId AND IsActive=@Status) 
				AND CustomerId=@CustomerId AND RouteId=@RouteId;
			END;
		COMMIT; 
	END TRY 
	BEGIN CATCH 
		ROLLBACK; 
		RETURN  'Error updating  receipt invoice mapping status: '  + ERROR_MESSAGE(); 
	END CATCH 
END;
