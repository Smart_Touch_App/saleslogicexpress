USE [MobileDataModel_FPS3];

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		WonderBiz Technology Pvt Ltd
-- Create date: 21-August-2015
-- Description:	Applies discount to specified invoice
-- =============================================
IF EXISTS(SELECT * FROM sys.objects WHERE name='USP_GetId' AND schema_id=SCHEMA_ID('BUSDTA'))
BEGIN ;
	DROP PROCEDURE [BUSDTA].[USP_GetId];
END;
GO
CREATE PROCEDURE [BUSDTA].[USP_GetId]
	@RouteName varchar(100), 
	@InvoiceNumber varchar(100), 
	@ReceiptNumber varchar(100),
	@CustomerId varchar(100)
AS
BEGIN; 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @RouteId as bigint=0; 
	DECLARE @InvoiceId as bigint=0;
	DECLARE @ReceiptId as bigint=0;
	DECLARE @Query AS varchar(100)=''; 

	SELECT @RouteId=RouteMasterID FROM BUSDTA.Route_Master WHERE RouteName=@RouteName;
	SELECT @ReceiptId=ReceiptId FROM BUSDTA.Receipt_Header WHERE CustomerId=CAST(@CustomerId AS bigint) AND RouteId=@RouteId AND ReceiptNumber=CAST(@ReceiptNumber AS bigint);
	SELECT @InvoiceId=InvoiceId FROM BUSDTA.Invoice_Header WHERE CustomerId=CAST(@CustomerId AS bigint) AND RouteId=@RouteId AND DeviceInvoiceNumber=CAST(@InvoiceNumber AS bigint);
	
	SELECT @CustomerId AS CustomerId, @RouteId AS RouteId, @InvoiceId AS InvoiceId, @ReceiptId AS ReceiptId;
END;