SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		WonderBiz Technology Pvt Ltd
-- Create date: 21-August-2015
-- Description:	Applies discount to specified invoice
-- =============================================
IF EXISTS(SELECT * FROM sys.objects WHERE name='USP_ApplyDiscountToInvoice' AND schema_id=SCHEMA_ID('BUSDTA'))
BEGIN ;
	DROP PROCEDURE [BUSDTA].[USP_ApplyDiscountToInvoice];
END
GO

CREATE PROCEDURE [BUSDTA].[USP_ApplyDiscountToInvoice] 
	@CustomerId int = 0, 
	@RouteId int = 0,
	@InvoiceId int = 0,
	@DiscountCode int=0,
	@DiscountAmount decimal(18,2)=0.00
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF ISNULL(@CustomerId,0)=0 
	BEGIN 
		RETURN 'Customer id is not provided'; 
	END; 
	
	IF ISNULL(@RouteId,0)=0 
	BEGIN 
		RETURN 'Route id is not provided.'; 
	END; 

	IF ISNULL(@InvoiceId,0)=0 
	BEGIN 
		RETURN 'Invoice id is not provided.'; 
	END; 

	IF ISNULL(@DiscountCode,0)=0 
	BEGIN 
		RETURN 'Discount code is not provided.'; 
	END; 

	IF ISNULL(@DiscountAmount,0)=0 
	BEGIN 
		RETURN 'Discount amount is not provided.'; 
	END; 

	DECLARE @OpenInvoiceBalance decimal(18,2) =0;
	DECLARE @MaxCustomerLedgerId int = 0;
	DECLARE @query varchar(MAX)=''; 

	BEGIN TRY

		BEGIN TRANSACTION;

			SELECT @MaxCustomerLedgerId=MAX(CustomerLedgerId)+1 FROM BUSDTA.Customer_Ledger WHERE RouteId=@RouteId;

			IF (@MaxCustomerLedgerId IS NULL OR @MaxCustomerLedgerId=1)
			BEGIN;
				THROW 60000, 'No record found in customer_ledger. cannot proceed.',1;
			END;

			IF EXISTS(SELECT * FROM BUSDTA.Invoice_Header WHERE CustomerId=@CustomerId AND RouteId=@RouteId AND InvoiceID=@InvoiceId AND ISNULL(NULLIF(ARStatusId,30),1)=2)
			BEGIN; 
				THROW 60000, 'Invoice is already closed. Cannot apply discount/concession.',1;
			END; 
			
			SELECT @OpenInvoiceBalance=(IH.GrossAmt-SUM((CASE CL.IsActive WHEN 1 THEN (ISNULL(CL.ReceiptAppliedAmt,0)+ISNULL(CL.ConcessionAmt,0)) ELSE 0 END))) 
			FROM BUSDTA.Customer_Ledger CL INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.RouteId=IH.RouteId AND CL.CustomerId=IH.CustomerId
			WHERE CL.InvoiceId=@InvoiceId AND CL.RouteId=@RouteId AND CL.CustomerId=@CustomerId GROUP BY IH.GrossAmt;

			IF (@OpenInvoiceBalance<@DiscountAmount)
			BEGIN 
				RETURN 'Discount amount is greater than the open invoice balance.';
			END 

			SET @query='INSERT INTO BUSDTA.Customer_Ledger (CustomerLedgerId, InvoiceId, RouteId, CustomerId, IsActive, ConcessionAmt,';  
			SET @query= @query + ' ConcessionCodeId, DateApplied, UpdatedDateTime, CreatedDateTime)';
			SET @query= @query + ' SELECT CAST(' + CAST(@MaxCustomerLedgerId AS VARCHAR(100))  + ' AS numeric(8,0)) AS CustomerLedgerId, CAST(' + CAST(@InvoiceId AS varchar(max)) + ' AS numeric(8,0)) AS InvoiceId, ';
			SET @query= @query + ' ' + CAST(@RouteId AS varchar(max)) +' AS RouteId, ' + CAST(@CustomerId AS varchar(max)) + ' AS CustomerId, 1 AS IsActive, ' + CAST(@DiscountAmount AS varchar(max)) + ' AS ReceiptAppliedAmt, ';
			SET @query= @query + ' ' +CAST(@DiscountCode AS varchar(max)) + ' AS ConcessionId, getdate() AS DateApplied, getdate() AS UpdatedDateTime' ;
			SET @query= @query + ' , GETDATE() AS CreatedDateTime FROM BUSDTA.Invoice_Header WHERE InvoiceId='+ CAST(@InvoiceId AS varchar(100)) + ' AND RouteId='+ CAST(@RouteId AS varchar(100)) + ' AND CustomerId = ' + CAST(@CustomerId AS varchar(100)); 
 
			EXECUTE(@query);

			SELECT @OpenInvoiceBalance=(IH.GrossAmt-SUM((CASE CL.IsActive WHEN 1 THEN (ISNULL(CL.ReceiptAppliedAmt,0)+ISNULL(CL.ConcessionAmt,0)) ELSE 0 END)))
			FROM BUSDTA.Customer_Ledger CL INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.RouteId=IH.RouteId AND CL.CustomerId=IH.CustomerId
			WHERE (ISNULL(CL.ReceiptID,0)<>0 OR ISNULL(ConcessionCodeId,0)<>0) AND CL.InvoiceId=@InvoiceId AND CL.RouteId=@RouteId AND CL.CustomerId=@CustomerId  GROUP BY IH.GrossAmt;

			IF (@OpenInvoiceBalance=0)
			BEGIN; 
				UPDATE BUSDTA.Invoice_Header SET ARStatusId=2 WHERE InvoiceId =@InvoiceId AND CustomerId=@CustomerId AND RouteId=@RouteId;
			END; 
			ELSE
			BEGIN; 
				UPDATE BUSDTA.Invoice_Header SET ARStatusId=1 WHERE InvoiceId =@InvoiceId AND CustomerId=@CustomerId AND RouteId=@RouteId; 
			END ;

		COMMIT; 

	END TRY
	BEGIN CATCH
		ROLLBACK
		RETURN 'Error while applying discounts '+ ERROR_MESSAGE();
	END CATCH
END