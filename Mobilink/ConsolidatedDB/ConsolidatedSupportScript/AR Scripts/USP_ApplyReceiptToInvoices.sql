SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		WonderBiz Technology Pvt Ltd
-- Create date: 21-August-2015
-- Description:	Applies specified a receipt to multiple invoices
-- =============================================
IF EXISTS(SELECT * FROM sys.objects WHERE name='USP_ApplyReceiptToInvoices' AND schema_id=SCHEMA_ID('BUSDTA'))
BEGIN 
	DROP PROCEDURE [BUSDTA].[USP_ApplyReceiptToInvoices];
END
GO

CREATE PROCEDURE [BUSDTA].[USP_ApplyReceiptToInvoices] 
	@CustomerId int = 0,
	@RouteId int = 0, 
	@ReceiptId int=0, 
	@InvoiceList varchar(100)='' --Comma separated list of invoices 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF ISNULL(@CustomerId,0)=0
	BEGIN
		RETURN 'Customer id not provided'; 
	END 

	IF ISNULL(@RouteId,0)=0
	BEGIN
		RETURN 'Route id is not provided'; 
	END 

	IF ISNULL(@ReceiptId,0)=0
	BEGIN
		RETURN 'Receipt id is not provided'; 
	END 

	IF ISNULL(@InvoiceList,'')=''
	BEGIN
		RETURN 'Please provide atleast one invoice id'; 
	END 

	DECLARE @OpenInvoiceBalance decimal(18,2)=0.00;
	DECLARE @ReceiptUnappliedAmt decimal(18,2)=0.00;
	DECLARE @ReceiptAmt decimal(18,2)=0.00;
	DECLARE @InvoiceAmt decimal(18,2)=0.00;
	DECLARE @InvoiceId int=0;
	DECLARE @MaxCustomerLedgerId int = 0;
	DECLARE @Query nvarchar(max)=N'';

	BEGIN TRY 

		BEGIN TRANSACTION 

		SELECT @MaxCustomerLedgerId=MAX(CustomerLedgerId)+1 FROM BUSDTA.Customer_Ledger WHERE RouteId=@RouteId

		IF (@MaxCustomerLedgerId IS NULL OR @MaxCustomerLedgerId=1)
		BEGIN 
			RETURN 'No record found in customer_ledger. cannot proceed.';
		END

		--SELECT @ReceiptUnappliedAmt=CAST(RH.TransactionAmount - SUM(CASE CL.IsActive WHEN 1 THEN ISNULL(CL.ReceiptAppliedAmt,0) ELSE 0 END) AS decimal(18,2))
		--FROM BUSDTA.Customer_Ledger CL WITH (NOLOCK) INNER JOIN BUSDTA.Receipt_Header RH WITH (NOLOCK) ON CL.Receiptid=RH.Receiptid
		--WHERE RH.CustomerId=@CustomerId AND RH.RouteId=@RouteId 
		--AND RH.ReceiptId=@ReceiptId AND RH.SettelmentId IS NOT NULL GROUP BY RH.TransactionAmount;

		SET @Query=N'SELECT @ReceiptUnappliedAmt=CAST(RH.TransactionAmount - SUM(CASE CL.IsActive WHEN 1 THEN ISNULL(CL.ReceiptAppliedAmt,0) ELSE 0 END) AS decimal(18,2))';
		SET @Query=@Query + N' FROM BUSDTA.Customer_Ledger CL WITH (NOLOCK) INNER JOIN BUSDTA.Receipt_Header RH WITH (NOLOCK) ON CL.Receiptid=RH.Receiptid AND CL.RouteId=RH.RouteId';
		SET @Query=@Query + N' WHERE RH.CustomerId=' + CAST(@CustomerId AS varchar(100)) + ' AND RH.RouteId=' + CAST(@RouteId AS varchar(100)); 
		SET @Query=@Query + N' AND RH.ReceiptId=' + CAST(@ReceiptId AS varchar(100)) + ' AND RH.SettelmentId IS NOT NULL GROUP BY RH.TransactionAmount';

		EXECUTE SP_EXECUTESQL @Query, N'@ReceiptUnappliedAmt decimal(18,2) OUTPUT', @ReceiptUnappliedAmt = @ReceiptUnappliedAmt output; 
		
		SET @Query = N'SELECT @OpenInvoiceBalance=CAST(SUM(A.Amount) AS decimal(18,2)) FROM (SELECT IH.InvoiceId AS InvoiceId, '; 
		SET @Query=@Query + N' (IH.GrossAmt-SUM((CASE CL.IsActive WHEN 1 THEN (ISNULL(CL.ReceiptAppliedAmt,0)+ISNULL(CL.ConcessionAmt,0)) ELSE 0 END))) AS Amount ';
		SET @Query=@Query + N' FROM BUSDTA.Customer_Ledger CL INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.RouteId=IH.RouteId';
		SET @Query=@Query + N' WHERE IH.InvoiceId IN (' + @InvoiceList + ') AND CL.CustomerId='+CAST(@CustomerId AS varchar(100))+' AND CL.RouteId='+CAST(@RouteId AS varchar(100));
		SET @Query=@Query + N' GROUP BY IH.InvoiceId, IH.GrossAmt) A';

		--IF @ReceiptUnappliedAmt <@OpenInvoiceBalance
		--BEGIN 
		--	RETURN 'Open invoice balance is more than available receipt amount. Cannot apply.';
		--END 

		EXECUTE SP_EXECUTESQL @Query, N'@OpenInvoiceBalance decimal(18,2) OUTPUT', @OpenInvoiceBalance = @OpenInvoiceBalance output; 
		
		SET @Query=N'DECLARE cursor_ApplyReceiptToInvoice CURSOR FOR ';
		SET @Query=@Query + N' SELECT IH.InvoiceId AS InvoiceId, ';
		SET @Query=@Query + N' (IH.GrossAmt-SUM((CASE CL.IsActive WHEN 1 THEN (ISNULL(CL.ReceiptAppliedAmt,0)+ISNULL(CL.ConcessionAmt,0)) ELSE 0 END))) AS Amount FROM BUSDTA.Customer_Ledger CL';
		SET @Query=@Query + N' INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.RouteId=IH.RouteId ';
		SET @Query=@Query + N' WHERE IH.InvoiceId IN ( ' + @InvoiceList + ') AND CL.CustomerId=' + CAST(@CustomerId AS varchar(100)) + 'AND CL.RouteId= ' + CAST(@RouteId AS varchar(100));
		SET @Query=@Query + N' GROUP BY IH.InvoiceId, IH.GrossAmt';

		EXECUTE SP_EXECUTESQL @Query;

		OPEN cursor_ApplyReceiptToInvoice;

		FETCH NEXT FROM cursor_ApplyReceiptToInvoice INTO @InvoiceId, @InvoiceAmt;

		WHILE @@FETCH_STATUS = 0 
		BEGIN; 
			--Print 'Invoice Id : ' + CAST(@InvoiceId AS varchar(max));
			--Print 'ReceiptUnappliedAmt: ' + CAST(@ReceiptUnappliedAmt AS varchar(max));
			--Print '@InvoiceAmt  : ' + CAST(@InvoiceAmt AS varchar(max));
			--Get the open balance of invoice, if it is greater than zero, update status to 1 else 2
			SELECT  @InvoiceId=IH.InvoiceId,
			@InvoiceAmt=(IH.GrossAmt-SUM((CASE CL.IsActive WHEN 1 THEN (ISNULL(CL.ReceiptAppliedAmt,0)+ISNULL(CL.ConcessionAmt,0)) ELSE 0 END)))
			FROM BUSDTA.Customer_Ledger CL INNER JOIN BUSDTA.Invoice_Header IH ON CL.InvoiceId=IH.InvoiceId AND CL.RouteId=IH.RouteId
			WHERE IH.InvoiceId =@InvoiceId AND CL.CustomerId=@CustomerId AND CL.RouteId=@RouteId
			GROUP BY IH.InvoiceId, IH.GrossAmt;

			IF(@ReceiptUnappliedAmt>=@InvoiceAmt)
			BEGIN ;
				INSERT INTO BUSDTA.Customer_Ledger (CustomerLedgerId, Receiptid, RouteId, InvoiceId, CustomerId, IsActive, ReceiptAppliedAmt, CreatedDatetime, DateApplied, UpdatedDateTime)
				SELECT @MaxCustomerLedgerId AS CustomerLedgerId, @ReceiptId AS ReceiptId, @RouteId AS RouteId,  InvoiceId, @CustomerId, 1 AS IsActive, @InvoiceAmt AS ReceiptAppliedAmt, GETDATE() AS CreatedDatetime, GETDATE() AS DateApplied, GETDATE() AS UpdatedDateTime 
				FROM BUSDTA.Invoice_Header WHERE InvoiceId =@InvoiceId AND RouteId=@RouteId AND CustomerId=@CustomerId;

				UPDATE BUSDTA.Invoice_Header SET ARStatusId=2 WHERE InvoiceId =@InvoiceId AND CustomerId=@CustomerId AND RouteId=@RouteId ;

				SET @ReceiptUnappliedAmt=@ReceiptUnappliedAmt- @InvoiceAmt;
			END; 
			ELSE
			BEGIN; 
				INSERT INTO BUSDTA.Customer_Ledger (CustomerLedgerId, Receiptid, RouteId, InvoiceId, CustomerId, IsActive, ReceiptAppliedAmt, CreatedDatetime, DateApplied, UpdatedDateTime)
				SELECT @MaxCustomerLedgerId AS CustomerLedgerId, @ReceiptId AS ReceiptId, @RouteId AS RouteId,  InvoiceId, @CustomerId, 1 AS IsActive, @ReceiptUnappliedAmt AS ReceiptAppliedAmt, GETDATE() AS CreatedDatetime, GETDATE() AS DateApplied, GETDATE() AS UpdatedDateTime 
				FROM BUSDTA.Invoice_Header WHERE InvoiceId =@InvoiceId AND RouteId=@RouteId AND CustomerId=@CustomerId;

				UPDATE BUSDTA.Invoice_Header SET ARStatusId=1 WHERE InvoiceId =@InvoiceId AND CustomerId=@CustomerId AND RouteId=@RouteId ;
			END;

			SET @MaxCustomerLedgerId=@MaxCustomerLedgerId+1;

			FETCH NEXT FROM cursor_ApplyReceiptToInvoice INTO @InvoiceId, @InvoiceAmt;
		END 

		CLOSE cursor_ApplyReceiptToInvoice;

		DEALLOCATE cursor_ApplyReceiptToInvoice;

		COMMIT;
	END TRY 
	BEGIN CATCH 
		ROLLBACK;
		RETURN 'Error while applying invoices. ' + ERROR_MESSAGE();
	END CATCH
END
