/*Notes Details */
CREATE TABLE BUSDTA.M0112(
    NDAN8 numeric(8, 0) NOT NULL,
    NDID numeric(8, 0) NOT NULL DEFAULT autoincrement,
    NDDTTM datetime NULL,
    NDDTLS nchar(500) NULL,
    NDTYP nchar(15) NULL,
    NDDFLT bit NULL,
    NDCRBY nchar(50) NULL,
    NDCRDT date NULL,
    NDUPBY nchar(50) NULL,
    NDUPDT date NULL,
 CONSTRAINT PK_M0112 PRIMARY KEY CLUSTERED
(
    NDAN8 ASC,
    NDID ASC
)
)

GO 

/*Queries to insert data into Notes Details*/
insert into busdta.M0112 (NDAN8, NDDTTM, NDDTLS, NDTYP, NDDFLT, NDCRBY, NDCRDT, NDUPBY, NDUPDT)
values (1108911, getdate(), 'Hello World', 'Cust', 1, 'RSR001', getdate(), 'RSR001', getdate());
GO 

insert into busdta.M0112 (NDAN8, NDDTTM, NDDTLS, NDTYP, NDDFLT, NDCRBY, NDCRDT, NDUPBY, NDUPDT)
values (1139845, getdate(), 'Customer asked for additional coffee items.', 'Cust', 1, 'RSR001', getdate(), 'RSR001', getdate());
GO

/*Query to select data from Notes Details*/
select
	NDAN8 as 'CustomerId',
	NDID as 'NoteDetailId',
	NDDTTM as 'NotesDate',
	NDDTLS as 'NoteDetail',
	NDTYP  as 'NoteType',
	NDDFLT as 'NoteDefault',
	NDCRBY as 'CreatedBy',
	NDCRDT as 'CreatedDate',
	NDUPBY as 'UpdatedBy',
	NDUPDT as 'UpdatedDate'
from BUSDTA.M0112
where NDAN8 = 1108911


/*Pricing Tab. Query to display data on Pricing Tab*/
select 
	AIAC27 as 'AlliedDiscountOverallCd', BUSDTA.GetUDCDescription('01','27',AIAC27) as 'AlliedDiscountOverallDesc',
	AIAC15 'AlliedDiscountCategory1Cd', BUSDTA.GetUDCDescription('01','15',AIAC01) as 'AlliedDiscountCategory1Desc',
	AIAC16 'AlliedDiscountCategory2Cd', BUSDTA.GetUDCDescription('01','16',AIAC01) as 'AlliedDiscountCategory2Desc',
	AIAC17 'AlliedDiscountCategory3Cd', BUSDTA.GetUDCDescription('01','17',AIAC01) as 'AlliedDiscountCategory3Desc',
	AIAC18 'AlliedDiscountCategory4Cd', BUSDTA.GetUDCDescription('01','18',AIAC01) as 'AlliedDiscountCategory4Desc',
	AIAC23 'LiquidBracketCd', BUSDTA.GetUDCDescription('01','23',AIAC23) as 'LiquidBracketDesc',
	AIAC24 'PriceProtectionCd', BUSDTA.GetUDCDescription('01','24',AIAC24) as 'PriceProtectionDesc',
	AIAC28 'CoffVolumeCd', BUSDTA.GetUDCDescription('01','28',AIAC28) as 'CoffVolumeDesc',
	AIAC29 'EquipProgPtsCd', BUSDTA.GetUDCDescription('01','29',AIAC29) as 'EquipProgPtsDesc',
	AIAC22 'POSChargeCd', BUSDTA.GetUDCDescription('01','22',AIAC22) as 'POSChargeDesc',
	AIAC30 'CCPBracketCd', BUSDTA.GetUDCDescription('01','30',AIAC30) as 'CCPBracketDesc',
	AIAC11 'TAXGroupCd', BUSDTA.GetUDCDescription('01','11',AIAC11) as 'TAXGroupDesc'
from busdta.M03012 
where AIAN8 = 1108911


/*Info Tab. Query to display data on Info Tab*/
SELECT 
	a.aban8 as 'CustomerId', 
	a.abalph as 'CustomerName', 
	rtrim(d.aladd1) as 'AddressLine1',
	rtrim(d.aladd2) as 'AddressLine2',
	'' as 'AddressLine3',				-- Need to identify which columns to map OR Add new column
	'' as 'AddressLine4',				-- Need to identify which columns to map OR Add new column
	rtrim(d. alcty1) as 'City', 
	rtrim(d.aladds) as 'State', 
	rtrim(d. aladdz) as 'Zip',
	'' as 'MailingName', 			-- Need to identify which columns to map OR Add new column
	'' as 'TaxExemptCertificate',	-- Need to identify which columns to map OR Add new column
	a.ABAT1 as 'SearchType', BUSDTA.GetUDCDescription('01','ST',a.ABAT1) as 'SearchTypeDesc',
	o.MAPA8 as 'Parent', busdta.GetABADescriptionFromABAId(o.MAPA8) as 'ParentDesc',
	a.aban81 as 'BillTo', busdta.GetABADescriptionFromABAId(a.aban81) as 'BillToDesc',
	a.ABAN85 as 'RemitTo', busdta.GetABADescriptionFromABAId(a.ABAN85) as 'RemitToDesc',
	c.AIAC12 as 'KAMNAMStreet', BUSDTA.GetUDCDescription('01','12',c.AIAC12) as 'KAMNAMStreetDesc',
	c.AIAC14 as 'KindOfBizAcct', BUSDTA.GetUDCDescription('01','14',c.AIAC14) as 'KindOfBizAcctDesc',
	c.AIFRTH as 'FreightHandlingCd', BUSDTA.GetUDCDescription('42','FR',c.AIAC14) as 'FreightHandlingDesc',
	c.AIAC02 as 'OperatingUnitCd', BUSDTA.GetUDCDescription('01','02',c.AIAC02) as 'OperatingUnitDesc',
	c.AIAC06 as 'RegionCd', BUSDTA.GetUDCDescription('01','06',c.AIAC06) as 'RegionDesc',
	c.AIAC05 as 'DistrictCd', BUSDTA.GetUDCDescription('01','05',c.AIAC05) as 'DistrictDesc',
	c.AIAC04 as 'BranchCd', BUSDTA.GetUDCDescription('01','04',c.AIAC04) as 'BranchDesc',
	c.AIAC08 as 'ChainCd', BUSDTA.GetUDCDescription('01','08',c.AIAC08) as 'ChainDesc',
	c.AIAC03 as 'RouteCd', BUSDTA.GetUDCDescription('01','03',c.AIAC03) as 'RouteDesc',
	c.AISTOP as 'StopCodeCd', BUSDTA.GetUDCDescription('42','SP',c.AISTOP) as 'StopCodeDesc'
FROM busdta.M0101 a join busdta.F0116 d ON a.aban8 =d.alan8  
	join busdta.F0150 o ON a.ABAN8 = o.MAAN8
	join busdta.M03012 c ON a.ABAN8 = c.AIAN8
WHERE a.aban8 = 1108911

/*Function Which returns the Address Book description from the Address Book Id*/
CREATE FUNCTION [BUSDTA].[GetABADescriptionFromABAId] (@ABAId numeric(8,0))
RETURNS nchar(40)
AS BEGIN
	DECLARE @ABALPH nchar(40);

	select @ABALPH =  ABALPH from BUSDTA.F0101 ab where ab.ABAN8 = @ABAId;

	RETURN @ABALPH;
END;

GO
/*Info Tab. Queries to SAVE customer Information.*/
update  busdta.M0101 set
	ABTXCT='731491215141101' 
where aban8='6280681';

GO

update  busdta.F0116 set 
	aladd1='7408 NORTH MAY AVENUE', 
	aladd2='KASJHDJKAS KL ASK', 
	aladd3='KCAJ KJNJL ALNK', 
	aladd4='LKJASDKJSAKL', 
	alcty1='OKLAHOMA LADNKASJDASJDKJS', 
	aladds='OK', 
	aladdz='73116-3299' 
where alan8='6280681';

GO