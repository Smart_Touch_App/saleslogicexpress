/****** Object:  Table [dbo].[TransactionSyncMaster]    Script Date: 04/16/2015 08:49:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionSyncMaster](
	[ActivityKey] [nchar](30) NOT NULL,
	[SyncDirection] [nchar](10) NULL,
	[SyncPriority] [nchar](10) NULL,
 CONSTRAINT [PK_TransactionSyncMaster] PRIMARY KEY CLUSTERED 
(
	[ActivityKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'AcceptOrder                   ', N'Both      ', N'High      ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'OrderDelivered                ', N'Upload    ', N'High      ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'PickComplete                  ', N'Upload    ', N'High      ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'PickItem                      ', N'Both      ', N'Normal    ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'PickOrder                     ', N'Both      ', N'High      ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'SettlementCancelled           ', N'Upload    ', N'High      ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'SettlementSettlement          ', N'Both      ', N'High      ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'SettlementStarted             ', N'Both      ', N'Normal    ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'SettlementSubmitted           ', N'Upload    ', N'High      ')
INSERT [dbo].[TransactionSyncMaster] ([ActivityKey], [SyncDirection], [SyncPriority]) VALUES (N'SettlementVerified            ', N'Upload    ', N'High      ')
/****** Object:  Table [dbo].[TransactionSyncDetail]    Script Date: 04/16/2015 08:49:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionSyncDetail](
	[SyncID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[ActivityKey] [nchar](15) NULL,
	[Synced] [bit] NOT NULL,
	[SyncTimestamp] [datetime] NULL,
 CONSTRAINT [PK_TransactionSyncDetail] PRIMARY KEY CLUSTERED 
(
	[SyncID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TransactionSyncDetail] ON
INSERT [dbo].[TransactionSyncDetail] ([SyncID], [ActivityID], [ActivityKey], [Synced], [SyncTimestamp]) VALUES (1, 1, NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[TransactionSyncDetail] OFF
/****** Object:  Table [dbo].[TransactionActivityType]    Script Date: 04/16/2015 08:49:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionActivityType](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityKey] [nchar](30) NOT NULL,
	[ActivityType] [nchar](25) NOT NULL,
	[ActivityAction] [nchar](100) NULL,
	[ActionDescription] [nchar](50) NULL,
	[Actor] [nchar](15) NULL,
	[ActivityState] [nchar](15) NULL,
 CONSTRAINT [PK_TransactionActivityType] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC,
	[ActivityKey] ASC,
	[ActivityType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TransactionActivityType] ON
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (1, N'CreateOrder                   ', N'Order                    ', N'Create Order (Template screen place order)                                                          ', NULL, N'User           ', N'Started        ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (2, N'AcceptOrder                   ', N'Order                    ', N'Accept Order (Order acceptance screen)                                                              ', NULL, N'User           ', N'Accepted       ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (3, N'PickOrder                     ', N'Order                    ', N'Pick Order (At the picking screen)                                                                  ', NULL, N'User           ', N'PickStarted    ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (4, N'PickComplete                  ', N'Order                    ', N'Pick Completed (Transition from pick to delivery screen)                                            ', NULL, N'User           ', N'PickCompleted  ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (5, N'OrderDelivered                ', N'Order                    ', N'Delievery Completed                                                                                 ', NULL, N'User           ', N'Completed      ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (6, N'SettlementStarted             ', N'Settlement               ', N'Started                                                                                             ', NULL, N'User           ', N'Started        ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (8, N'SettlementCancelled           ', N'Settlement               ', N'Cancelled                                                                                           ', NULL, N'User           ', N'Cancelled      ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (9, N'SettlementSubmitted           ', N'Settlement               ', N'Settlement	Submitted                                                                                ', NULL, N'User           ', N'Submitted      ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (11, N'SettlementVerified            ', N'Settlement               ', N'Settlement	Verified                                                                                 ', NULL, N'User           ', N'Verified       ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (12, N'SettlementSettlement          ', N'Settlement               ', N'Checks verified                                                                                     ', NULL, N'User           ', N'Started        ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (13, N'PickItem                      ', N'PickOrder                ', N'                                                                                                    ', NULL, N'User           ', N'InProgress     ')
INSERT [dbo].[TransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [ActivityState]) VALUES (14, N'TransactionComplete           ', N'Transaction              ', N'Complete Transaction                                                                                ', NULL, N'User           ', N'Completed      ')
SET IDENTITY_INSERT [dbo].[TransactionActivityType] OFF
/****** Object:  Table [dbo].[TransactionActivityDetail]    Script Date: 04/16/2015 08:49:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionActivityDetail](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[RouteID] [nchar](15) NOT NULL,
	[ActivityTypeKey] [nchar](15) NOT NULL,
	[ActivityDetailClass] [nchar](100) NULL,
	[ActivtyDetails] [nvarchar](max) NULL,
	[ActivityStart] [datetime] NULL,
	[ActivityEnd] [datetime] NULL,
	[StopInstanceID] [nchar](15) NULL,
	[CustomerID] [nchar](15) NULL,
	[SettlementID] [nchar](15) NULL,
	[ParentActivityID] [int] NULL,
 CONSTRAINT [PK_TransactionActivityDetail] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC,
	[RouteID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TransactionActivityDetail] ON
INSERT [dbo].[TransactionActivityDetail] ([ActivityID], [RouteID], [ActivityTypeKey], [ActivityDetailClass], [ActivtyDetails], [ActivityStart], [ActivityEnd], [StopInstanceID], [CustomerID], [SettlementID], [ParentActivityID]) VALUES (1, N'FBM783         ', N'CreateOrder    ', N'SalesLogicExpress.Domain.Order                                                                      ', NULL, CAST(0x0000A47900C8F928 AS DateTime), CAST(0x0000A47900C8FA36 AS DateTime), N'1              ', N'1170897        ', NULL, NULL)
INSERT [dbo].[TransactionActivityDetail] ([ActivityID], [RouteID], [ActivityTypeKey], [ActivityDetailClass], [ActivtyDetails], [ActivityStart], [ActivityEnd], [StopInstanceID], [CustomerID], [SettlementID], [ParentActivityID]) VALUES (2, N'FBM783         ', N'AcceptOrder    ', N'SalesLogicExpress.Domain.Order                                                                      ', NULL, CAST(0x0000A47900C985C8 AS DateTime), CAST(0x0000A47900C9CC18 AS DateTime), N'1              ', N'1170897        ', NULL, NULL)
INSERT [dbo].[TransactionActivityDetail] ([ActivityID], [RouteID], [ActivityTypeKey], [ActivityDetailClass], [ActivtyDetails], [ActivityStart], [ActivityEnd], [StopInstanceID], [CustomerID], [SettlementID], [ParentActivityID]) VALUES (4, N'FBM783         ', N'PickOrder      ', NULL, NULL, CAST(0x0000A47900C985C8 AS DateTime), CAST(0x0000A47900C985C8 AS DateTime), N'1              ', N'1170897        ', NULL, NULL)
INSERT [dbo].[TransactionActivityDetail] ([ActivityID], [RouteID], [ActivityTypeKey], [ActivityDetailClass], [ActivtyDetails], [ActivityStart], [ActivityEnd], [StopInstanceID], [CustomerID], [SettlementID], [ParentActivityID]) VALUES (5, N'FBM783         ', N'PickItem       ', NULL, NULL, CAST(0x0000A47900C985C8 AS DateTime), CAST(0x0000A47900C985C8 AS DateTime), N'1              ', N'1170897        ', NULL, 4)
INSERT [dbo].[TransactionActivityDetail] ([ActivityID], [RouteID], [ActivityTypeKey], [ActivityDetailClass], [ActivtyDetails], [ActivityStart], [ActivityEnd], [StopInstanceID], [CustomerID], [SettlementID], [ParentActivityID]) VALUES (6, N'FBM783         ', N'PickComplete   ', NULL, NULL, CAST(0x0000A47900C985C8 AS DateTime), CAST(0x0000A47900C985C8 AS DateTime), N'1              ', N'1170897        ', NULL, 4)
SET IDENTITY_INSERT [dbo].[TransactionActivityDetail] OFF
/****** Object:  Table [dbo].[NonTransactionActivityType]    Script Date: 04/16/2015 08:49:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NonTransactionActivityType](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityKey] [nchar](30) NOT NULL,
	[ActivityType] [nchar](25) NOT NULL,
	[ActivityAction] [nchar](30) NULL,
	[ActionDescription] [nchar](50) NULL,
	[Actor] [nchar](15) NULL,
	[IsLedgered] [bit] NULL,
 CONSTRAINT [PK_NonTransactionActivityType] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC,
	[ActivityKey] ASC,
	[ActivityType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[NonTransactionActivityType] ON
INSERT [dbo].[NonTransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [IsLedgered]) VALUES (1, N'AppStart                      ', N'Application              ', N'Application Start             ', N'                                                  ', N'System         ', 1)
INSERT [dbo].[NonTransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [IsLedgered]) VALUES (2, N'AppExit                       ', N'Application              ', N'Application Exit              ', NULL, N'System         ', 1)
INSERT [dbo].[NonTransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [IsLedgered]) VALUES (4, N'UserLogin                     ', N'Security                 ', N'User Login                    ', NULL, N'User           ', 1)
INSERT [dbo].[NonTransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [IsLedgered]) VALUES (5, N'UserLogout                    ', N'Security                 ', N'User Logout                   ', NULL, N'User           ', 1)
INSERT [dbo].[NonTransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [IsLedgered]) VALUES (6, N'PasswordIncorrect             ', N'Security                 ', N'Password Incorrect            ', NULL, N'User           ', 1)
INSERT [dbo].[NonTransactionActivityType] ([ActivityID], [ActivityKey], [ActivityType], [ActivityAction], [ActionDescription], [Actor], [IsLedgered]) VALUES (7, N'PasswordChange                ', N'Security                 ', N'Password Change               ', NULL, N'User           ', 1)
SET IDENTITY_INSERT [dbo].[NonTransactionActivityType] OFF
/****** Object:  Table [dbo].[ActivityLedger]    Script Date: 04/16/2015 08:49:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLedger](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityHeaderID] [int] NULL,
	[ActivityTimestamp] [datetime] NOT NULL,
	[ActivityDescription] [nchar](250) NULL,
	[ActivityTypeKey] [nchar](15) NOT NULL,
	[IsTxActivity] [bit] NULL,
 CONSTRAINT [PK_ActivityLedger] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ActivityLedger] ON
INSERT [dbo].[ActivityLedger] ([ActivityID], [ActivityHeaderID], [ActivityTimestamp], [ActivityDescription], [ActivityTypeKey], [IsTxActivity]) VALUES (1, 1, CAST(0x0000A47900C8F928 AS DateTime), NULL, N'AppStart       ', 1)
INSERT [dbo].[ActivityLedger] ([ActivityID], [ActivityHeaderID], [ActivityTimestamp], [ActivityDescription], [ActivityTypeKey], [IsTxActivity]) VALUES (4, NULL, CAST(0x0000A47900C8F5A4 AS DateTime), NULL, N'CreateOrder    ', NULL)
SET IDENTITY_INSERT [dbo].[ActivityLedger] OFF
