USE [MobileDataModel]
GO


/*Delivery Cycle Master*/

CREATE TABLE [BUSDTA].[M56M0002](
	[DCDDC] [nchar](3) NOT NULL,		-- DeliveryDayCode
	[DCDDCD] [nchar](50) NULL,          -- DeliveryDayCodeDesc
	[DCPN] [numeric](4, 0) NOT NULL,    -- PeriodNumber
	[DCWN] [numeric](4, 0) NOT NULL,    -- WeekNumber
	[DCDN] [numeric](4, 0) NOT NULL,    -- DayNumber
	[DCISST] [numeric](4, 0) NULL,      -- IsServiceStop
	[DCCRBY] [nchar](50) NULL,			-- Created By
	[DCCRDT] [date] NULL,				-- Created Date
	[DCUPBY] [nchar](50) NULL,			-- Updated By
	[DCUPDT] [date] NULL,				-- Updated Date
 CONSTRAINT [PK_DeliveryCycleMaster] PRIMARY KEY CLUSTERED 
(
	[DCDDC] ASC,
	[DCPN] ASC,
	[DCWN] ASC,
	[DCDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [BUSDTA].[M56M0002] ADD  CONSTRAINT [DF__DeliveryC__IsSer__74100195]  DEFAULT ((1)) FOR [DCISST]
GO


/*Route Standard Sequence*/
CREATE TABLE [BUSDTA].[M56M0003](
	[RSROUT] [nchar](10) NOT NULL,		-- RouteId
	[RSAN8] [numeric](8, 0) NOT NULL,   -- CustomerID
	[RSWN] [numeric](4, 0) NOT NULL,    -- WeekNumber
	[RSDN] [numeric](4, 0) NOT NULL,    -- DayNumber
	[RSSN] [numeric](4, 0) NULL,        -- SequenceNumber
	[RSCRBY] [nchar](50) NULL,			-- Created By
	[RSCRDT] [date] NULL,               -- Created Date
	[RSUPBY] [nchar](50) NULL,          -- Updated By
	[RSUPDT] [date] NULL,               -- Updated Date
 CONSTRAINT [PK_RouteStandardSequence] PRIMARY KEY CLUSTERED 
(
	[RSROUT] ASC,
	[RSAN8] ASC,
	[RSWN] ASC,
	[RSDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




/*Route Stop Sequence*/
CREATE TABLE [BUSDTA].[M56M0004](
	[RPSTID] [numeric](8, 0) NOT NULL,		-- Stop Id
	[RPROUT] [nchar](10) NOT NULL,          -- RouteID
	[RPAN8] [numeric](8, 0) NOT NULL,       -- CustomerID
	[RPSTDT] [date] NOT NULL,               -- StopDate
	[RPOGDT] [date] NULL,                   -- OriginalDate
	[RPSN] [numeric](4, 0) NULL,            -- SequenceNumber
	[RPVTTP] [nchar](10) NULL,              -- VisiteeType
	[RPSTTP] [nchar](10) NULL,              -- StopType
	[RPRSTID] [numeric](8, 0) NULL,         -- ReferenceStopId
	[RPCRBY] [nchar](50) NULL,				-- Created By
	[RPCRDT] [date] NULL,                   -- Created Date
	[RPUPBY] [nchar](50) NULL,              -- Updated By
	[RPUPDT] [date] NULL,                   -- Updated Date
 CONSTRAINT [PK_RouteStopSequence_1] PRIMARY KEY CLUSTERED 
(
	[RPSTID] ASC,
	[RPROUT] ASC,
	[RPAN8] ASC,
	[RPSTDT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/*This function will return the Period for a given date with respect to BaseDate*/
CREATE FUNCTION [BUSDTA].[GetPeriodForDate] (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int;
	DECLARE @varDiv28 numeric(3,2);
	DECLARE @PeriodNumber numeric(3,2);
	DECLARE @ModNumber numeric(3,2);

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate);
	select @varDiv28 = CEILING (@DateDifference / 28.00);
	select @ModNumber = @varDiv28%3;
	if(@ModNumber = 0)
		select @PeriodNumber = 3;
	else
		select @PeriodNumber = @ModNumber;
	RETURN @PeriodNumber;
END

GO


/*This function will return the Week for a given date with respect to BaseDate*/
CREATE FUNCTION [BUSDTA].[GetWeekForDate] (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int;
	DECLARE @varDiv7 numeric(3,2);
	DECLARE @WeekNumber numeric(3,2);
	DECLARE @ModNumber numeric(3,2);

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate);
	select @varDiv7 = CEILING (@DateDifference / 7.00);
	select @ModNumber = @varDiv7%4;
	if(@ModNumber = 0)
		select @WeekNumber = 4;
	else
		select @WeekNumber = @ModNumber;
	RETURN @WeekNumber;
END

GO

/*This function will return the Day, BaseDate argument can be removed*/
CREATE FUNCTION BUSDTA.GetDayForDate (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int

	select @DateDifference = DATEPART (DW, @CalendarDate)-1

	RETURN @DateDifference
END
GO

GO



/*Following insert command will be run when any of activity takes place
1) On the Date. 2)Traversed to a date. 3) Create Stop on Date. 4) Move Stop. 5) Pre-Order. */
insert into busdta.M56M0004(
    RouteStopId,
    RPROUT,
    RPAN8,
    RPSTDT,
    --RPOGDT, -- This field will be populated when a stop is moved
    RPSN,
    RPVTTP,
    RPSTTP
    --RPRSTID -- This field will be populated when a stop is moved
)
(
    select     2, -- Numbering logic/Autoincrement
    r.RSROUT,
    r.RSAN8,
    GETDATE(),
    r.RSSN,
    'Cust',        -- Customer/Prospect
    'Planned'    -- Planned/UnPlanned/ReScheduled
from BUSDTA.M56M0003 r  , BUSDTA.M56M0002 d
where d.DCWN = BUSDTA.GetWeekForDate(CAST('20150203' as date), CAST('20141229' as date))
    and d.DCDN = BUSDTA.GetDayForDate(CAST('20150203' as date), CAST('20141229'as date))
    and d.DCPN = BUSDTA.GetPeriodForDate(CAST('20150203' as date), CAST('20141229' as date))
    and r.RSROUT = 'FBM783'
    and d.DCPN <> 0
    and d.DCDDC = (select AISTOP from BUSDTA.F03012 where BUSDTA.F03012.AIAN8 = r.RSAN8)
group by r.RSROUT,r.RSAN8, d.DCWN, d.DCDN, r.RSSN
) 


/*Query to select the data to be displayed for Daily Stops.*/

select rss.RPSTID 'StopID',
	rss.RPROUT as 'Route', 
	rss.RPAN8 as 'CustomerId', 
	ab.ABALPH as 'Customer Name', 
	rss.RPSTDT as 'StopDate',
	rss.RPOGDT 'OriginalDate', 
	rtrim(d.aladd1) as 'AddressLine1',
	rtrim(d.aladd2) as 'AddressLine2',
	'' as 'AddressLine3',
	'' as 'AddressLine4',
	busdta.GetDefaultPhone(ab.ABAN8) as 'PhoneNumber',
	rtrim(d. alcty1) as 'City', 
	rtrim(d.aladds) as 'State', 
	rtrim(d. aladdz) as 'Zip',
	rss.RPSN as 'SequenceNumber', 
	rss.RPVTTP as 'VisiteeType', 
	rss.RPSTTP as 'StopType', 
	RPRSTID as 'ReferenceStopId'
from BUSDTA.M56M0004 rss, BUSDTA.F03012 cm, BUSDTA.F0101 ab, busdta.F0116 d
where rss.RPSTDT = '2015-04-09' and	-- Pass Date
rss.RPAN8 = cm.AIAN8 and rss.RPAN8 = ab.ABAN8 


/*Function to select default phone number. This function shall evolve.*/
CREATE OR REPLACE FUNCTION BUSDTA.GetDefaultPhone (@CustomerId numeric(8,0))
RETURNS nchar(25)
AS BEGIN
	DECLARE @PhoneNumber nchar(25)
	Declare @PH1 bit
	Declare @PH2 bit
	Declare @PH3 bit
	Declare @PH4 bit

	select @PH1 = CDDFLTPH1 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	select @PH2 = CDDFLTPH2 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	select @PH3 = CDDFLTPH3 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	select @PH4 = CDDFLTPH4 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	if @PH1 = 1
		select @PhoneNumber = CDAR1+ CDPH1 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	else if @PH2 = 1 
		select @PhoneNumber = CDAR2+ CDPH2 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	else if @PH3 = 1 
		select @PhoneNumber = CDAR3+ CDPH3 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	else if @PH4 = 1 
		select @PhoneNumber = CDAR4+ CDPH4 from BUSDTA.M0111 where CDAN8 = @CustomerId and CDIDLN = 0
	else
		select @PhoneNumber = 'NA'
	RETURN @PhoneNumber
END

GO


